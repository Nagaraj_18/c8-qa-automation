#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" This script is intended to generate sccs-controlled tavern test scripts 
for testing the ClassRelationship endpoint from a requests (https) query on 
a reliable source of domain object names.

Currently the reliable source of domain object names is the APISearch method, 
as that establishes some form of testing for that, but it should eventually
be from an HTTP query on the Swagger page itself, as the Swagger page is an
authoritative document."""

import json
import re
import requests
import yaml
from collections import OrderedDict
import sys

def parse_args_any(args):
    pos = []
    named = OrderedDict({})
    key = None
    for arg in args:
        if key:
            if arg.startswith('--'):
                named[key] = True
                key = arg[2:]
            else:
                named[key] = arg
                key = None
        elif arg.startswith('--'):
            key = arg[2:]
        else:
            pos.append(arg)
    if key:
        named[key] = True
    return (pos, named)


def main(argv):

    args = parse_args_any(argv)

    print(args)

    object_names = args[0]

    payload = args[1]
     
    
    def compose_one_test(object_name, **payload):
        # Params dict should have labels: "begin", "end", "incoming", "max_depth", "normative_paths"
        # Values should be of type        string,  string, boolean,       integer       ,  list     (respectively)
        
        print(OrderedDict({key : eval(payload[key]) for key in payload.keys()}))
        actualized_payload = OrderedDict({key : eval(payload[key]) for key in payload.keys()})
           
        
        serialized_json_payload = "\n        ".join([assignment for assignment in re.split(", *|{|}",json.dumps(actualized_payload)) if assignment])
        
    
        tavern_post_test_text = '''test_name: {0} CRUD status

includes:
  - !include validated_login_and_auth.yaml

marks:
  - posts
  - {0}

stages:
  - name: {0} POST status persistent
    request:
      url: "{{base_url}}/{0}"
      method: POST
      headers:
        content-type: application/json
        cookie: "{{auth_token:s}}"
      json:
        {1}          
    response:
      status_code: 201

  - name: {0} POST status ephemeral for update and delete
    request:
      url: "{{base_url}}/{0}"
      method: POST
      headers:
        content-type: application/json
        cookie: "{{auth_token:s}}"
      json:
        {1}          
    response:
      status_code: 201
      save:
        json:
          {0}_for_update_del_id: id

  - name: {0} GETmany status
    request:
      url: "{{base_url}}/{0}"
      method: GET
      headers:
        content-type: application/json
        accept: application/json
        cookie: "{{auth_token:s}}"
    response:
      status_code: 200

  - name: {0} PUT status 
    request:
      url: "{{base_url}}/{0}/{{{0}_for_update_del_id:s}}"
      method: PUT
      headers:
        content-type: application/json
        cookie: "{{auth_token:s}}"
      json:
        {1}          
    response:
      status_code: 200

  - name: {0} GETsingle status 
    request:
      url: "{{base_url}}/{0}/{{{0}_for_update_del_id:s}}"
      method: GET
      headers:
        content-type: application/json
        cookie: "{{auth_token:s}}"
    response:
      status_code: 200      

  - name: {0} DEL status 
    request:
      url: "{{base_url}}/{0}/{{{0}_for_update_del_id:s}}"
      method: DELETE
      headers:
        content-type: application/json
        cookie: "{{auth_token:s}}"
    response:
      status_code: 200
      '''.format(object_name, serialized_json_payload)
        return tavern_post_test_text      
              
    for object_name in object_names:
        object_post_test_filename = 'test_persistent_{}_CRUD_simple_status.tavern.yaml'.format(object_name)
        object_post_test_contents = compose_one_test(object_name, **payload)
        print("\n\n{0}\n{1}\n".format(object_post_test_filename, object_post_test_contents))
        with open(object_post_test_filename, 'w', encoding='utf-8') as f:
            f.write(object_post_test_contents)

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))




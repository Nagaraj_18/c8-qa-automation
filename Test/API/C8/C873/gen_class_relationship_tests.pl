
my $class_relationship_base_text = <<"HERE";
test_name: class_relationship_GET_simple_status

includes:
  - !include validated_login_and_auth.yaml

marks:
  - gets
  - class_relationship

stages:
  - name: class_relationship_GET_simple_status
    request:
      url: "{proto:s}://{host:s}:{port:d}{base_path:s}/class_relationship?begin=%s&end=%s&incoming=%s&max_depth=%d"
      method: GET
      headers:
        content-type: application/json
        cookie: "{auth_token:s}"
    response:
      status_code: %d
HERE

my $file = 'cr_resource.csv';

open CR_RES_FH, '<', $file;

while (my $line = <CR_RES_FH>) {
    my @fields = split ",", $line or die "\nCould not open $file\n";
    open CR_TEST_OUTPUT, ">", 
      sprintf("test_class_relationship_auto_%s_%s_inc_%s_depth_%d.tavern.yaml", 
        $fields[0], $fields[1], $fields[2], $fields[3]
      );
    print CR_TEST_OUTPUT sprintf $class_relationship_base_text, 
        $fields[0], $fields[1], $fields[2], $fields[3], $fields[4];
    close CR_TEST_OUTPUT;

}
    
   


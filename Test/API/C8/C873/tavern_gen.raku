#!/usr/bin/env perl6

my token target-test-file-name { test .* \.tavern\.yaml }

sub MAIN ( $filename,
           :$left, :$right,
           :$top?,  :$bottom ) {

    say "$left $right $top $bottom $filename";
}

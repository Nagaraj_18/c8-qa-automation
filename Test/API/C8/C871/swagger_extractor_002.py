#!/usr/bin/python
import sys
import requests
from requests.auth import HTTPBasicAuth
from lxml import html
import json
from collections import OrderedDict
import pprint
test_repo  = '/Users/mgipson/scripts/python/API_Wrangling'
sys.path.append(test_repo)

import API_Exerciser_04 as API_Test_Framework
from API_Exerciser_04 import *


aut_server       = 'mg-wf8-sql12.csi.local'
sut_fq_domain    = 'mg-wf8-sql12.centricsoftware.com'
sut_swagger_url  = 'http://%s/csi-requesthandler/api/v2/swagger.json/' % sut_fq_domain
#ba_usr           = 'mgipson'
#ba_pwd           = 'Welcome2csi'


def acquire_swagger_page(aut_server=aut_server, aut_api_version=2, aut_login='Administrator', aut_password='centric8'):
    #endpointdoc_schema = acquire_endpointdoc_html_files(url_ep_docs)
    #print endpointdoc_schema
    
    # Here we get what the swagger page says in an existing C8 installation, if the
    # one specified exists and is accessible. The particular C8 installation is 
    # identified at the start of this script as "aut_server".
    
    swagger_eps_and_attribs=OrderedDict({})
    read_swagger = True 
    try:
        sess_mgr_0 = API_Test_Framework.StartSession('%s' % aut_server, 
          api_version=aut_api_version, 
          login=aut_login, password=aut_password)
    except:
        read_swagger = False
        print "Unable to log into the C8 installation at: %s." % aut_server,
        print "Do you want to continue without the data from that server?"
        response = input()
        match_yes = re.search('^Y(es)?$', response, re.I)
        if match_yes:
            read_swagger = True
    
    if read_swagger:
        #consumable_eps 
        sess_mgr_0.log_in()
        
        swagger_json = sess_mgr_0.get_swagger_json()
        
        #import pdb; pdb.set_trace()
        
        swagger_python = json.loads(swagger_json.text)
        #definitions = swagger_python['definitions']
        raw_paths = swagger_python['paths']
        #raw_endpoints = definitions.keys()

        
        
                
        #endpoints = []
        #for rp in sorted(raw_paths.keys()):
        #    #match = re.search('Apparel', r_ep, re.I)
        #    #if match:
        #    #    import pdb; pdb.set_trace()
        #    #if 'get' in raw_paths[rp]:
        #    #    if raw_paths[rp]['get']['operationId'] == "getAllResources":
        #    #        endpoints.append(rp) 
        #    
        #    endpoints.append(rp)
        #import pdb; pdb.set_trace()
    #return swagger_eps_and_attribs
    
        return swagger_python
        #return paths


swagger_ep_paths_and_attribs = acquire_swagger_page()

print len(swagger_ep_paths_and_attribs)

#for path in swagger_ep_paths:



#    slugified_path = re.sub(r'/', r'', path)
#    curr_test_yaml = open(r'/Users/mgipson/scripts/python/centric_API/isolated_tavern/test_%s_GET_many_status.tavern.yaml' % slugified_path, 'w')
#    curr_test_yaml.write('''
#test_name: %(test_name)s_GET_many_status
#
#includes:
#  - !include validated_login_and_auth.yaml
#
#stages:
#  - name: %(test_name)s GET many status
#    request:
#      url: "{proto:s}://{host:s}:{port:d}{base_path:s}%(ep_path)s"
#      method: GET
#      headers:
#        content-type: application/json
#        cookie: "{test_login_token:s}"
#    response:
#      status_code: 200''' % {'test_name':slugified_path, 'ep_path':path})        
#    curr_test_yaml.close()


print json.dumps(swagger_ep_paths_and_attribs, indent=2, sort_keys=True)


#!/usr/bin/python
# -*- coding: utf-8 -*-

""" This script is intended to generate sccs-controlled tavern test scripts 
for testing the ClassRelationship endpoint from a requests (https) query on 
a reliable source of domain object names.

Currently the reliable source of domain object names is the APISearch method, 
as that establishes some form of testing for that, but it should eventually
be from an HTTP query on the Swagger page itself, as the Swagger page is an
authoritative document."""

import json
import re
import requests
import yaml
from timeit import default_timer as timer
from common_arg_parse import *

with open("start_defaults.yaml", 'r') as defaults_ymlfile:
    yaml_defaults = yaml.load(defaults_ymlfile, Loader=yaml.FullLoader)

object_name_blacklist = ['Log','Version','SharedTeam','SpecLibraryItem',
  'ConfigUpdate','Email','APISearch','License','Count','Job','Localization',
  'ExportImportJob','ObjectTree']

def object_name_corrector(object_name):
    corrected = {"SpecDataSheetSubtype":"SpecificationDataSheetSubtype",
      "ShipmentTerm":"ShipmentTerms",
      "ORQuestionSubSection":"ORQuestionSubsection",
      "SpecLibraryItem":"LookupItem",
      "SharedTeam":"HierarchySecurityGroup"
    }

    if object_name in corrected:
        object_name = corrected[object_name]
    return object_name


headers = {
    'Accept': 'application/json',
}

payload = {    
  "username": session_components['login'],
  "password": session_components['password']
}
security_response = requests.post(
    #'http://mg-wf8-sql12.csi.local:8080/csi-requesthandler/api/v2/session', 
    '{base_url}/session'.format(**session_components), 
    headers=headers, json=payload)

http_token = json.loads(security_response.text)['token']

my_headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Cookie': http_token
}



my_params={"endpoint_names_only":"true","endpoint_skip":0,
  "endpoint_limit":10000, "endpoint_names_only":"false","show_attr":"false"}

api_search_response = requests.get(
    '{base_url}/api_search'.format(**session_components), 
    headers=my_headers,  params=my_params)

_python_struct = json.loads(api_search_response.text)
all_api_search_valid_objects_begin_set = [object_name_corrector(object['name']) \
  for object in _python_struct \
  if object['name'] not in object_name_blacklist]
all_api_search_valid_objects_begin_set.sort()

style_only_end_set = ['Style',]
#import pdb; pdb.set_trace()

def compose_one_test(**params):
    # Params dict should have labels: "begin", "end", "incoming", "max_depth", "normative_paths"
    # Values should be of type        string,  string, boolean,       integer       ,  list     (respectively)

    tavern_path_test_text = '''
test_name: class_relationship GET begin {begin}, end {end}, incoming {incoming}, max_depth {max_depth}

includes:
  - !include validated_login_and_auth.yaml

marks:
  - gets
  - class_relationship

stages:
  - name: class_relationship GET begin {begin}, end {end}, incoming {incoming}, max_depth {max_depth}
    request:
      url: "{{proto:s}}://{{host:s}}:{{port:d}}{{base_path:s}}/class_relationship?begin={begin}&end={end}&incoming={incoming}&max_depth={max_depth}"
      method: GET
      headers:
        content-type: application/json
        cookie: "{{auth_token:s}}"
    response:
      status_code: 200
      verify_response_with:
      - function: utils:all_attribs_are_as_specfd_or_none
        extra_kwargs:
          paths: {normative_paths}

      '''.format(**params)
    return tavern_path_test_text      
              

def run_tests(begin_set, end_set, label, max_depth_limit=1, **session_components):

    my_headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Cookie': http_token
    }
    reqd_tav_test_count = 0
    start = timer()
    for incoming in [False, True]:
        for begin in begin_set:
            for end in end_set:
                for depth in range(1, (max_depth_limit + 1)):
                    my_params = {"begin":begin,"end":end,"incoming":incoming,"max_depth":depth }
                    class_relationship_response = requests.get(
                      '{base_url}/class_relationship'.format(**session_components), 
                      headers=my_headers, params=my_params)
                    path_list = json.loads(class_relationship_response.text)
                    tidy_path_list = json.dumps(path_list, indent=2)
                    if class_relationship_response:
                        my_params["normative_paths"]=json.dumps(path_list['paths'])
                        if len(path_list['paths']) > 0:
                            reqd_tav_test_count += 1
                            if session_components['verbose']:
                                print("\n\n%s: %03d. %s" % (label, reqd_tav_test_count, my_params))
                                if session_components['verbose'] >= 2:
                                    print("Count of paths this query: %d" % len(path_list['paths']))
                                if session_components['verbose'] >= 3:
                                    print(tidy_path_list)
                                # here, form the name of the file and the contents of the 
                                # file

                            filename = "test_persistent_class_relationship_"\
                            + "GET_{begin}_{end}_{incoming}".format(**my_params)\
                            +  "_{}".format(depth)\
                            +  "_status_paths.tavern.yaml"

                            file_contents = compose_one_test(**my_params)
                            if session_components['verbose'] >= 4:
                                print('\n\n{}:\n'.format(filename))
                                print('\n{}\n\n\n\n'.format(file_contents))

                                  
                            if not session_components['suppress_test_gen']:
                                with open(filename, 'w', encoding='utf-8') as f:
                                    f.write(file_contents)
                                
                            if depth < max_depth_limit:
                                break
                        elif depth == max_depth_limit:
                            reqd_tav_test_count += 1
                            if session_components['verbose']:
                                print("\n\n%s: %03d. %s" % (label, reqd_tav_test_count, my_params))
                                if session_components['verbose'] >= 2:
                                    print ("No path found in path depth max this run: %d" % depth)

                            filename = "test_persistent_class_relationship_"\
                            + "GET_Material_Style_true"\
                            +  "_{}".format(depth)\
                            +  "_status_paths.tavern.yaml"
                            file_contents = compose_one_test(**my_params)

                            if not session_components['suppress_test_gen']:
                                with open(filename, 'w', encoding='utf-8') as f:
                                    f.write(file_contents)

                        else:
                            pass

                    else:
                        reqd_tav_test_count += 1
                        if session_components['verbose']:
                            print("\n\n%s: %03d. %s" % (label, reqd_tav_test_count, my_params))
                            if session_components['verbose'] >= 2:
                                print("No response")
                        break
    stop = timer()
    print("Elapsed time for composing {0} tests this run: {1:.2f} seconds.".format(reqd_tav_test_count,(stop - start)))
    

#import pdb;pdb.set_trace()
run_tests(all_api_search_valid_objects_begin_set, style_only_end_set, "trial", max_depth_limit=7, **session_components)

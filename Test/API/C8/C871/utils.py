# utils.py
import urllib
import json
import time
import re
import requests
import yaml

def assert_true(response):
    assert True

def assert_false(response):
    assert False

def test_function(response):
    thingy = urllib.quote(response.json()[0]["id"], safe="")
    return {"marketing_segmentation_enum_list_id": thingy}  
    #return {"marketing_segmentation_enum_list_id":

class Error(Exception):
    """Base class for exceptions in this module."""
    pass

class IntermediatingQueryError(Error):
    """Exception raised for errors in the intermediating query.

    Attributes:
        expression -- input expression in which the error occurred
        message -- explanation of the error
    """
    # see https://docs.python.org/3/tutorial/errors.html
    #def __init__(self, expression, message):
    def __init__(self, message):
        #self.expression = expression
        self.message = message
    

def request_whether_taken(auth_token, path, param_key, param_value, 
  param_value_pad_places=None):
    '''
    At present, this function handles only one key and one value. It will
    probably stay that way until I encounter an POST or PUT endpoint that
    requires more than one unique-constrained value to successfully create
    or update.
    
    This function executes an actual GET, so requires an auth token to 
    use as a header. In internal use from within the "utils" library, 
    calling functions can access the "validated_login_and_auth.yaml" file
    to get the current auth token to provide to this function. But, if you
    are testing this function, say, in an interactive python session, you 
    will need to manually provide a current auth token, most likely by 
    copying it from the "validated_login_and_auth.yaml" file. Any error
    in the authorization should be handled by a special error handler that
    kicks in on anything other than a status code 200. 

    The endpoint method will always be a GET, so that is implicit. Whether
    it is a GET all or a GET single is determined by the path. The path
    Portion of the endpoint is given in the second parameter of the call
    requested param_value as a filter to a GET. 

    At the time of this writing, it is the nature of the REST API that GET all
    is the only type of GET that allows filtering. I am not preventing the
    use of any GET. If the query should fail it will be raised as a specially
    handled exception. 

    The param_key names the attribute the param_value names the value that will
    be queried. 

    If the query succeeds, and a record is found (a fact which indicates that
    the requested value should not be used) then it raises the value by 1 and
    tries again in a reecursive call.

    It continues until it reaches a value for which no record is returned. It
    then returns this value up the calling stack, and that is the value that
    is then proposed for use in the POST or PUT yaml test that presumably calls
    this.

    The final, and only optional formal parameter, is param_value_pad_places,
    which allows specifiying an integer as the width of zero padding. Inclusion
    of zero padding will allow the sorting of records to be the same, whether
    it is numerically or text-sorted.

    The param-value to be tried can be submitted as a string, or an integer. If
    param value is entered as a string, it will be returned as a string. If it
    is entered as an integer without specifying padding, it will be returned as
    an integer. If it is submitted as an integer, with padding specified, it
    will be reurned as a string. I think this will work 
    fine in the real REST API, as long as the string always is "integer-like".
    '''
    
    param_value = int(param_value)
    if param_value_pad_places:
        param_value_pad_places = int(param_value_pad_places)
        param_value = eval('"%%0%dd" %% param_value' % \
          param_value_pad_places)
    else:    
        param_value = "%d" % param_value

    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Cookie': auth_token
    }
    params = {param_key: param_value}
    req = requests.get(path, headers=headers, params=params)
    #if req.status_code == 200 and len(json.loads(req.text)) > 0:
    if req.status_code == 200: 
        if len(json.loads(req.text)) > 0:
            try:
                param_value += 1
            except:
                int_value = int(param_value)
                new_int_value = int_value + 1
                if param_value_pad_places:
                    param_value = eval('"%%0%dd" %% new_int_value' % \
                      param_value_pad_places)
                else:    
                    param_value = "%d" % new_int_value
            
            (param_key, param_value) = request_whether_taken(auth_token, path, 
              param_key, param_value, param_value_pad_places)

        else:
            int_value = int(param_value)
            if param_value_pad_places:
                param_value = eval('"%%0%dd" %% int_value' % \
                  param_value_pad_places)
            else:    
                param_value = "%d" % int_value

    else:
        raise IntermediatingQueryError('The intermdiating query failed. Please'\
          + ' check the values provided for "<path>" and "<key>" in the '\
          + '"<drq>" portion of the extra_kwarg keyword.') 
    return (param_key, param_value)


def differentiated_general_object(**extra_kwargs):
    '''
    Return the payload (passed in extra-kwargs) with values optionally 
    augmented by call-specific unique values of any payload value that
    needs to be different from any that may have gone before.

    Examples are all unique-constrained fields, for example "code" on some
    objects, or "node_name" wherever it is required, or would just be nice
    to have a distinct name, or a name that reflects exactly when the
    object was instantiated or changed. 

    The particular unique value type can be specified from a list of three.
    The user must denote the differentiating value with a tag in angle
    brackets. At present, tag values can be for sormatted date time:
    "<fdt>", for "epoch seconds": "<es>", or for "different required": 
    "<drq.". The angle brackets are not optional, as they keep the tag from
    being confused with any part of an object name. Note that these
    tags are only effective as an easily discernable text pattern. At 
    no time do they ever work like xml tags.

    Formatted date time (<FDT>) is a string with date and time all spelled
    out, e.g: Wednesday_15_July_2020_at_16:07:36

    Epoch seconds (<es>) is a string or number that is the count of 
    seconds since the start of the current epoch (since 1 January 1970).

    Different required (<drq>) is an integer (expressed as an integer, or
    as text) which starts at 1, then is recursively sampled by running an
    actual query against the system under test, until it is found that
    the current value is NOT in use, increasing by 1 when it is found to 
    be in use. At the first instance of an integer found not to be in use,
    the name and value are returned. Requesting this requires passing in
    extra data, structured plainly so as to be analyzable by the following
    regular expression:
    '<(drq)>(<auth_token)>([^<]+)<(url)>([^<]+)<(key)>(.*)'. For
    production tests, the implementor can build the url up as a 
    concatenation of the 

    In the returned payload, The tag will be replaced by the value of the 
    thing it denotes. The user should delimit the 
    angle-bracketed value from any text or number with the single character 
    delimiter, if any, that they want between the uniquefying value and any
    text.  
    '''

    # Create the formatted date time as an array of components so we can give the
    # user a bit of a choice as to whether to rejoin them as separate words or
    # as one long string.
    formatted_date_time_array = time.strftime("%A_%d_%B_%Y_at_%H:%M:%S").split('_')

    epoch_seconds = int(time.time())

    def handle_the_value(diffd_wanted_value, diffn_string, delimiter=''):
        new_value = ""
        if diffd_wanted_value:
            if diffd_wanted_value != "":

                new_value = '%s%s%s' % (diffd_wanted_value, delimiter, diffn_string)
            else:
                new_value = diffn_string
        else:
            new_value = diffn_string
        return new_value
        
        
    payload = {}

    for arg in extra_kwargs:
        # The user must denote the differentiating value with a tag in angle
        # brackets. At present, tag values can be for formatted date time
        # "fdt", for "epoch seconds" "es", or for "different required": "drq".
        # The angle brackets are not 
        # optional as they keep the tag from being confused with
        # any part of an object name. The user should immediately follow the 
        # angle-bracketed value with the delimiter, if any, that they want
        # between the value and any text.
        diffreq_wanted_match = re.match(
          '<(drq)>(<(pad)>(\d+))?<(path)>([^<]+)<(key)>(.*)', arg)

        diffd_wanted_match = re.match('(<(fdt)>( |_)?|<(es)>( |_)?)(.*)', arg)

        if diffreq_wanted_match:
            new_key = diffreq_wanted_match.group(8)
            with open("validated_login_and_auth.yaml", 'r') as login_auth_ymlfile:
                login_auth_yaml = yaml.load(login_auth_ymlfile, Loader=yaml.FullLoader)

            #auth_token = diffreq_wanted_match.group(3)
            auth_token =  login_auth_yaml['variables'][
              'auth_token']
            url = "{proto}://{host}:{port}{base_path}".format(**login_auth_yaml['variables'])
            path = diffreq_wanted_match.group(6)
            url += path

            try:
                padding = int(diffreq_wanted_match.group(4))
            except:
                padding = None

            diffreq_key = diffreq_wanted_match.group(8)
            diffreq_value = extra_kwargs[arg]
            k, v = request_whether_taken(auth_token, url, 
              diffreq_key, diffreq_value, padding)
            payload[k] = v
            
        elif diffd_wanted_match:
            new_key = diffd_wanted_match.group(6)
            diffd_wanted_value = extra_kwargs[arg]
            #delimiter = ''
            if diffd_wanted_match.group(2) == 'fdt':
                diffn_string = " ".join(formatted_date_time_array)
                delimiter = diffd_wanted_match.group(3)
            elif diffd_wanted_match.group(4) == 'es':
                delimiter = diffd_wanted_match.group(5)
                diffn_string = epoch_seconds
            new_value = handle_the_value(diffd_wanted_value, diffn_string, delimiter)
            payload[new_key] = new_value
        else:
            payload[arg]= extra_kwargs[arg]
    return payload

def raw_epoch_seconds():
    '''
    Return a date and time, as raw seconds since epoch start.
    '''
    epoch_time = int(time.time())
    return {"node_name": epoch_time}

def write_file(response, **extra_kwargs):
    '''
    This verification function should be useable with any payload
    to verify that NO attribute, given as a name under
    "extra_kwargs" is the value specified in those arguments
    as the value corresponding that name. It should 
    work whether the response is a list (as under GET many)
    or a dictionary (as under GET single), or no records returned.

    The implementor MUST remember to specify the kwargs 
    assignments in the YAML tests under the subsection 
    (co-subordinate with "function:" to the "response:)
    
    Under this particular function, it may be acceptable for no record to be
    returned, as that could indicate proper filtering under some circumstances,
    but only if the query is accepted as valid. Validity of the query is 
    usually indicated by a 200

    '''
    verbose = True
   
    if response:
        verifiables = json.loads(response.text)
    else:
        verifiables = None
    if verbose:
        f = open("snitch.txt", "w")
        if isinstance(verifiables, list):
            if len(verifiables) == 0:
                f.write("A zero length list was returned by the query.")
            for list_entry in verifiables:
                for key in list_entry.keys():
                    f.write ("\n%s: %s" % (key, list_entry[key]))
                f.write ("\n............................\n")
        elif isinstance(verifiables, dict):
            for key in verifiables.keys():
                f.write("\n%s: %s" % (key, verifiables[key]))
        elif verifiables is None:
            f.write("Nothing (no response body) was returned by the query.")
        f.write("\n--------------------------------------------\n")
    my_pass = True
    for key in extra_kwargs.keys():
        if verbose:
            f.write("\n%s: %s\n" % (key, extra_kwargs[key]))
            f.write("\n-------------------------------------------\n")
        # the try/except is to separate lists from dicts, or in other words,
        # GET many from GET single
        if isinstance(verifiables, list) :
            if len(verifiables) == 0:
                f.write("\nNo records returned is an acceptable outcome for this verification,\n")
                f.write("\nas long as that was what was expected for the overall test.\n")
            else:
                for verifiable in verifiables:
                    if verbose:
                        f.write("\nfrom a list...\n")
                        f.write("\n++++++++\n")
                        f.write("\n%s: %s\n" % (key, verifiable[key])) 
                        f.write("\n%s != %s\n" % (verifiable[key],extra_kwargs[key]))
                    bad_target = extra_kwargs[key]
                    this_target = verifiable[key]
                    if verbose:
                        f.write("\n%s != %s\n" % (this_target, bad_target))
                    if this_target == bad_target:
                        my_pass = False
        elif isinstance(verifiables, dict):
            if verifiables[key] == extra_kwargs[key]:
                if verbose:
                    f.write("\nIn a dict....\n")
                    f.write("\n%s should not equal %s\n" % (verifiables[key], extra_kwargs[key]))
                my_pass = False
        elif verifiables is None:
            if verbose:
                f.write("\nBe sure to check any other verifications, and status code, if any.\n")

    if verbose:
        f.close()
    #assert my_pass != False


def no_attrib_is_as_specfd_or_none(response, **extra_kwargs):
    '''
    This verification function should be useable with any payload
    to verify that NO attribute, given as a name under
    "extra_kwargs" is the value specified in those arguments
    as the value corresponding that name. It should 
    work whether the response is a list (as under GET many)
    or a dictionary (as under GET single), or no records returned.

    The implementor MUST remember to specify the kwargs 
    assignments in the YAML tests under the subsection 
    (co-subordinate with "function:" to the "response:)
    
    Under this particular function, it may be acceptable for no record to be
    returned, as that could indicate proper filtering under some circumstances,
    but only if the query is accepted as valid. Validity of the query is 
    usually indicated by a 200

    '''
    verbose = True
   
    if response:
        verifiables = json.loads(response.text)
    else:
        verifiables = None
    if verbose:
        f = open("snitch.txt", "w")
        if isinstance(verifiables, list):
            if len(verifiables) == 0:
                f.write("A zero length list was returned by the query.")
            for list_entry in verifiables:
                for key in list_entry.keys():
                    f.write ("\n%s: %s" % (key, list_entry[key]))
                f.write ("\n............................\n")
        elif isinstance(verifiables, dict):
            for key in verifiables.keys():
                f.write("\n%s: %s" % (key, verifiables[key]))
        elif verifiables is None:
            f.write("Nothing (no response body) was returned by the query.")
        f.write("\n--------------------------------------------\n")
    my_pass = True
    for key in extra_kwargs.keys():
        if verbose:
            f.write("\n%s: %s\n" % (key, extra_kwargs[key]))
            f.write("\n-------------------------------------------\n")
        # the try/except is to separate lists from dicts, or in other words,
        # GET many from GET single
        if isinstance(verifiables, list) :
            if len(verifiables) == 0:
                f.write("\nNo records returned is an acceptable outcome for this verification,\n")
                f.write("\nas long as that was what was expected for the overall test.\n")
            else:
                for verifiable in verifiables:
                    if verbose:
                        f.write("\nfrom a list...\n")
                        f.write("\n++++++++\n")
                        f.write("\n%s: %s\n" % (key, verifiable[key])) 
                        f.write("\n%s != %s\n" % (verifiable[key],extra_kwargs[key]))
                    bad_target = extra_kwargs[key]
                    this_target = verifiable[key]
                    if verbose:
                        f.write("\n%s != %s\n" % (this_target, bad_target))
                    if this_target == bad_target:
                        my_pass = False
        elif isinstance(verifiables, dict):
            if verifiables[key] == extra_kwargs[key]:
                if verbose:
                    f.write("\nIn a dict....\n")
                    f.write("\n%s should not equal %s\n" % (verifiables[key], extra_kwargs[key]))
                my_pass = False
        elif verifiables is None:
            if verbose:
                f.write("\nBe sure to check any other verifications, and status code, if any.\n")

    if verbose:
        f.close()
    #assert my_pass != False

def no_attrib_is_as_specfd(response, **extra_kwargs):

    '''
    This verification function should be useable with any payload
    to verify that NO attribute, given as a name under
    "extra_kwargs" is the value specified in those arguments
    as the value corresponding that name. It should 
    work whether the response is a list (as under GET many)
    or a dictionary (as under GET single), or no records returned.

    The implementor MUST remember to specify the kwargs 
    assignments in the YAML tests under the subsection 
    (co-subordinate with "function:" to the "response:)
    
    Under this particular function, it is NOT acceptable for no record to be
    returned. 

    '''
    verbose = True
   
    if response:
        verifiables = json.loads(response.text)
    else:
        verifiables = None
    if verbose:
        f = open("snitch.txt", "w")
        if isinstance(verifiables, list):
            if len(verifiables) == 0:
                f.write("A zero length list was returned by the query.")
            for list_entry in verifiables:
                for key in list_entry.keys():
                    f.write ("\n%s: %s" % (key, list_entry[key]))
                f.write ("\n............................\n")
        elif isinstance(verifiables, dict):
            for key in verifiables.keys():
                f.write("\n%s: %s" % (key, verifiables[key]))
        elif verifiables is None:
            f.write("Nothing (no response body) was returned by the query.")
        f.write("\n--------------------------------------------\n")
    my_pass = True
    for key in extra_kwargs.keys():
        if verbose:
            f.write("\n%s: %s\n" % (key, extra_kwargs[key]))
            f.write("\n-------------------------------------------\n")
        # the try/except is to separate lists from dicts, or in other words,
        # GET many from GET single
        if isinstance(verifiables, list) :
            if len(verifiables) == 0:
                f.write("\nNo records returned is not an acceptable outcome for this verification.")
                my_pass = False
            else:
                for verifiable in verifiables:
                    if verbose:
                        f.write("\nfrom a list...\n")
                        f.write("\n++++++++\n")
                        f.write("\n%s: %s\n" % (key, verifiable[key])) 
                        f.write("\n%s != %s\n" % (verifiable[key],extra_kwargs[key]))
                    bad_target = extra_kwargs[key]
                    this_target = verifiable[key]
                    if verbose:
                        f.write("\n%s != %s\n" % (this_target, bad_target))
                    if this_target == bad_target:
                        my_pass = False
        elif isinstance(verifiables, dict):
            if verifiables[key] == extra_kwargs[key]:
                if verbose:
                    f.write("\nIn a dict....\n")
                    f.write("\n%s should not equal %s\n" % (verifiables[key], extra_kwargs[key]))
                my_pass = False
        elif verifiables is None:
            if verbose:
                f.write("\nBe sure to check any other verifications, and status code, if any.\n")

    if verbose:
        f.close()
    #assert my_pass != False


def verify_relationships(response, **extra_kwargs):
    '''
    This verification function should be useable only with
    responses of a class_relationship query.

    It should verify/validate that paths are exactly as given in
    extra_kwargs['paths'], as to specific paths or lack of any,
    and, if any is present, their nodes, but not as to any ordering 
    of the paths.
    
    Both the payload and the "paths" - labeled element of the
    **extra_kwargs dict should be converted to sets before
    comparison so that the order of their elements is not of
    any importance to a valid match. 
    '''
    normative_minimal_path_set = extra_kwargs['paths']
    if normative_minimal_path_set:
        actual_path_set = set(response['paths'])
        normative_minimal_path_set = set(extra_kwargs['paths'])
        assert actual_path_set == normative_minimal_path_set
    else:
        assert response is None
    

    

#def gen_tests_from_api_search_class_relship_all_dom_objects():
    #This 

def all_attribs_are_as_specfd_or_none(response, **extra_kwargs):
    '''
    This verification function should be useable with any payload
    to verify that ALL attributes, given as a name under
    "extra_kwargs" are the value specified in those arguments
    as the value corresponding that name. It should 
    work whether the response is a list (as under GET many)
    or a dictionary (as under GET single) or no records returned.

    The implementor MUST remember to specify the kwargs 
    assignments in the YAML tests under the subsection 
    (co-subordinate with "function:" to the "response:)

    Under this particular function, it may be acceptable for no record to be
    returned, as that could indicate proper filtering under some circumstances,
    but only if the query is accepted as valid. Validity of the query is 
    usually indicated by a 200
    '''
    verbose = True
    
    verifiables = json.loads(response.text)
    #verifiables = json.loads(response)
    if verbose:
        f = open("snitch.txt", "w")
        if len(verifiables) == 0:
                f.write("A zero length list was returned by the query.")
        if isinstance(verifiables, list):
            for list_entry in verifiables:
                for key in list_entry.keys():
                    f.write ("\n%s: %s" % (key, list_entry[key]))
                f.write ("\n............................\n")
        elif isinstance(verifiables, dict):
            for key in verifiables.keys():
                f.write("\n%s: %s" % (key, verifiables[key]))
        elif verifiables is None:
            f.write("Nothing (no response body) returned by query")
        f.write("\n--------------------------------------------\n")
    my_pass = True
    for key in extra_kwargs.keys():
        if verbose:
            f.write("\n%s: %s\n" % (key, extra_kwargs[key]))
            f.write("\n--------------------------------------------\n")
        # the try/except is to separate lists from dicts, or in other words,
        # GET many from GET single
        if isinstance(verifiables, list) :
            if len(verifiables) == 0: 
                f.write("\nNo records returned is an acceptable outcome for this verification,\n")
                f.write("\nas long as that was what was expected for the overall test.\n")
            else:
                for verifiable in verifiables:
                    if verbose:
                        f.write("\nfrom a list...\n")
                        f.write("\n++++++++\n")
                        f.write("\n%s: %s\n" % (key, verifiable[key])) 
                        f.write("\n%s == %s\n" % (verifiable[key],extra_kwargs[key]))
                    bad_target = extra_kwargs[key]
                    this_target = verifiable[key]
                    if verbose:
                        f.write("\n%s != %s\n" % (this_target, bad_target))
                    if this_target != bad_target:
                        my_pass = False
        elif isinstance(verifiables, dict):
            if verifiables[key] != extra_kwargs[key]:
                if verbose:
                    f.write("\nIn a dict....\n")
                    f.write("\n%s should equal %s\n" % (verifiables[key], extra_kwargs[key]))
                my_pass = False
        elif verifiables is None:
            if verbose:
                f.write("\nBe sure to check any other verifications, and status code, if any.\n")

    if verbose:
        f.close()

    assert my_pass != False   

def all_attribs_are_as_specfd(response, **extra_kwargs):
    '''
    This verification function should be useable with any payload
    to verify that ALL attributes, given as a name under
    "extra_kwargs" are the value specified in those arguments
    as the value corresponding that name. It should 
    work whether the response is a list (as under GET many)
    or a dictionary (as under GET single) or no records returned.

    The implementor MUST remember to specify the kwargs 
    assignments in the YAML tests under the subsection 
    (co-subordinate with "function:" to the "response:)

    Under this particular function, it is NOT acceptable for no record to be
    returned.
    '''
    verbose = True
    
    verifiables = json.loads(response.text)

    if verbose:
        f = open("snitch.txt", "w")
        if len(verifiables) == 0:
                f.write("A zero length list was returned by the query.")
        if isinstance(verifiables, list):
            for list_entry in verifiables:
                for key in list_entry.keys():
                    f.write ("\n%s: %s" % (key, list_entry[key]))
                f.write ("\n............................\n")
        elif isinstance(verifiables, dict):
            for key in verifiables.keys():
                f.write("\n%s: %s" % (key, verifiables[key]))
        elif verifiables is None:
            f.write("Nothing (no response body) returned by query")
        f.write("\n--------------------------------------------\n")
    my_pass = True
    for key in extra_kwargs.keys():
        if verbose:
            f.write("\n%s: %s\n" % (key, extra_kwargs[key]))
            f.write("\n--------------------------------------------\n")
        # the try/except is to separate lists from dicts, or in other words,
        # GET many from GET single
        if isinstance(verifiables, list) :
            if len(verifiables) == 0: 
                f.write("\nNo records returned is nor an acceptable outcome for this verification.")
                my_pass = False
            else:
                for verifiable in verifiables:
                    if verbose:
                        f.write("\nfrom a list...\n")
                        f.write("\n++++++++\n")
                        f.write("\n%s: %s\n" % (key, verifiable[key])) 
                        f.write("\n%s == %s\n" % (verifiable[key],extra_kwargs[key]))
                    bad_target = extra_kwargs[key]
                    this_target = verifiable[key]
                    if verbose:
                        f.write("\n%s != %s\n" % (this_target, bad_target))
                    if this_target != bad_target:
                        my_pass = False
        elif isinstance(verifiables, dict):
            if verifiables[key] != extra_kwargs[key]:
                if verbose:
                    f.write("\nIn a dict....\n")
                    f.write("\n%s should equal %s\n" % (verifiables[key], extra_kwargs[key]))
                my_pass = False
        elif verifiables is None:
            if verbose:
                f.write("\nBe sure to check any other verifications, and status code, if any.\n")

    if verbose:
        f.close()

    assert my_pass != False   


#def exo_write(response, **extra_kwargs):
#    '''
#    This helper function writes the specified kwarg(s) and its(their) value to
#    a yaml file on disk, so as to pass the value(s) of the kwarg(s) to later 
#    tests. 
#
#    Later tests will need to reference this yaml file. The references will
#    ideally be distributed across many, many files. To ease maintenance, 
#    this function, and the tests that reference its product, should look to 
#    a central location for the name.
#    '''

def affirm_set_of_attrib_values(response, **extra_kwargs):
    '''
    This helper function compensates for a bug/feature in Tavern, that a json
    list is being compared as to order, even when Tavern strict is turned off,
    whether or not using even the shorthand provided by Tavern's YAML format.

    For example: while this passes:

     1 stages:
     2   - name: authentication_type GETmany status
     3     request:
     4       url: "{proto:s}://{host:s}:{port:d}{base_path:s}/authentication_types"
     5       method: GET
     6       headers:
     7         content-type: application/json
     8         accept: application/json
     9         cookie: "{auth_token:s}"
    10    response:
    11      status_code: 200
    12      strict: false
    13      json: 
    14        - node_name: LDAP
    15        - node_name: Hybrid
    16      save:
    17        json:
    18          one_authentication_type_of_several: "[0].id"

    Reversing the order of of lines 14 and 15 (LDAP and Hybrid) causes it to 
    fail (if the response actually has LDAP higher in the list).

    There is no guarantee that the json being returned is ever going to list
    them in any particular order.

    Actually, experimenting with the above just now leads me to a workaround
    that if I put each node name listing below its own json header, that does
    an apparently separate comparison, so that ordering is no longer an issue:

    10    response:
    11      status_code: 200
    12      stricfft: false
    13      json: 
    14        - node_name: Hybrid
    15      json:
    16        - node_name: LDAP

    
    but it still leaves the issue that there could be other list elements in 
    the response that contain unaccounted-for assignments. In the particular
    test made an example above, this is not an issue, but since this could be
    a possibility in the future.

    So, we will provide for testing for the set (where "set" is a formally 
    defined term) of the named values.

    This function expects, in **kwargs, keys that correspond to a value 
    repeated in each element of the list that is expected to comprise the 
    response. The value of each key is either a set, list, or tuple. If it is
    not exactly a set, it will be converted to one (with loss of the repeti-
    tion of any repeated values).

    Then, it will check that each value appears at least once in the response,
    and if strict is set (or rather not unset, as strict is the default), then
    it will also check that the set of expected values exhausts the set of 
    response values. 
    '''
    strict = True
    if "strict" in extra_kwargs:
        strict = extra_kwargs['strict']
        del extra_kwargs['strict']

    verifiables = json.loads(response.text)

    for key in extra_kwargs.keys():
        desired_value_set = set(extra_kwargs[key])
        tested_value_set = set()
        for verifiable in verifiables:
            tested_value_set.add(verifiable[key])
        if strict:
            # Should be exactly the same set.
            assert(desired_value_set == tested_value_set)
        else:
            # Should be a proper subset.
            assert(desired_value_set <= tested_value_set)
             

    

def affirm_count_records(response, **extra_kwargs):
    '''
    This helper function counts the records in a list. The normal intended use-
    case is to compare to the given number, using the operation mentioned
    '''
    if 'count' in extra_kwargs.keys():
        assert len(response.json()) == extra_kwargs['count']
    # 'equals' is a synonym for 'count'    
    if 'equals' in extra_kwargs.keys():
        assert len(response.json()) == extra_kwargs['equals']
    if 'greater_than' in extra_kwargs.keys():
        assert len(response.json()) > extra_kwargs['greater_than']
    if 'greater_than_or_equal_to' in extra_kwargs.keys():
        assert len(response.json()) >= extra_kwargs['greater_than_or_equal_to']
    if 'less_than' in extra_kwargs.keys():
        assert len(response.json()) < extra_kwargs['less_than']
    if 'less_than_or_equal_to' in extra_kwargs.keys():
        assert len(response.json()) <= extra_kwargs['less_than_or_equal_to']
    if 'not_equals' in extra_kwargs.keys():
        assert len(response.json()) == extra_kwargs['not_equals']




import argparse
import yaml
import re
import psutil

#me = psutil.Process()
#parent = psutil.Process(me.ppid())
#grandparent = psutil.Process(parent.ppid())
#print grandparent.cmdline()

#filename = __file__
#filename = grandparent.cmdline()

#basename = re.sub(r'^(.*?)\.py', r'\1', filename)


with open("start_defaults.yaml", 'r') as defaults_ymlfile:
    yaml_defaults = yaml.load(defaults_ymlfile, Loader=yaml.FullLoader)

#aut_server       = 'mg-wf8-sql12.csi.local:8080'
#sut_fq_domain    = 'mg-wf8-sql12.centricsoftware.com:8080'
#sut_swagger_url  = 'http://%s/csi-requesthandler/api/v2/swagger.json/' % \
#  sut_fq_domain

parser = argparse.ArgumentParser(
  description='This script pulls data from the Swagger page of which the '\
    + 'URL is given, with default or optional command-line setting of all '\
    + 'Swagger url components and login parameters. Then, it parses the '\
    + 'data to report as a tool to extend testing or to create or determine '\
    + 'what tests should be run. Defaults for this test, which are also '\
    + 'common to start.py are settable in the "start_defaults.yaml" file.'\
    + 'Defaults specific to this script are settable in a .yaml file of which'\
    #+ ' the basename is named the same as this script ("%s.yaml") ' % basename\
    + 'So, it is entirely possible to change the name of this script, as '\
    + 'long as the base name of the corresponding .yaml file is changed '\
    + 'to match. If you change the default values there, then this '\
    + 'help message will update to mention the new actual default values.')

yaml_default = yaml_defaults['url settings']['proto']
parser.add_argument('-t', '--proto', '-P', '--transfer', 
  help='Specify the transfer protocol. Default is "%s".' % yaml_default, 
  default=yaml_default
)

yaml_default = yaml_defaults['url settings']['domain']
parser.add_argument('-d', '--domain', '--host', 
  help='Specify the host. The current default is "%s".' % yaml_default,
  default=yaml_default
)

yaml_default = yaml_defaults['url settings']['port']
parser.add_argument('-o', '--port', 
  help='Specify the port. The default is "%s".' % yaml_default,
  default=yaml_default
)

yaml_default_version = yaml_defaults['url settings'][
  'base path before version']
parser.add_argument('-b', '--basepath','--basepath_before_version',
  help='Specify the base path before the version. The default is "%s".' % \
    yaml_default_version,
  default=yaml_default_version
)

yaml_default_version = yaml_defaults['url settings']['rest version']
parser.add_argument('-r', '--rest_version',
  help='Specify the rest version. The default is "%d".' % \
    yaml_default_version,
  default=yaml_default_version
)

yaml_default = yaml_defaults['url settings'][
  'base path after version']
parser.add_argument('-a', '--after_version', '--basepath_after_version',
  help='Specify any base path portion after the rest version. The default ' \
    + 'is "%s".' % yaml_default,
  default=yaml_default
)

yaml_default = yaml_defaults['url settings']['swagger json path']
parser.add_argument('-j', '--swagger_json', '--swagger',
  help='Specify the path to the Swagger json. The default is %s.' % \
  yaml_default,
  default=yaml_default
)

yaml_default = yaml_defaults['operational']['suppress the tests']
parser.add_argument('-s', '--suppress_test_gen', '--suppress', action='store_false',
  help='Suppress test generation to make the run a dry run. The default is: %s.' % \
  yaml_default,
  default=yaml_default
)

yaml_default = yaml_defaults['operational']['verbose']
parser.add_argument('-v', '--verbose', action='count',
  help='Specify the verbosity, increasing with each mention. '\
    + 'The default is %s.' % yaml_default, 
  default=yaml_default
)


yaml_default = False
help_text = 'Receive the complement of tests above and beyond existing '
help_text += 'endpoint tests. The default is %s.' % yaml_default
help_text += 'The tests should be named distinctly from the usual, '
help_text += 'change-controlled set of tests, so they can be purged if '
help_text += 'necessary, by deleting on the wildcard bounded different '
help_text += 'portion of the text.'
parser.add_argument('-T', '--tests', '--Tests', action='store_true',
  help=help_text,
  default=yaml_default
)

yaml_default = False
help_text = 'Pass only a high level summary of the Swagger JSON through '
help_text += 'as a single, pretty-printed '
help_text += 'JSON data-structure. The default is %s.' % yaml_default
parser.add_argument('-S', '--summary', '--Summary', action='store_true',
  help=help_text,
  default=yaml_default
)

yaml_default = False
help_text = 'Pass the Swagger JSON through as a single, pretty-printed '
help_text += 'JSON data-structure. The default is %s.' % yaml_default
parser.add_argument('-R', '--raw', '--Raw', action='store_true',
  help=help_text,
  default=yaml_default
)

yaml_default = False
help_text = 'Analyze and print the analysis for testing expressed as a '
help_text += 'single, pretty printed json data-structure. The default '
help_text += 'is "%s".' % yaml_default
parser.add_argument('-A', '--analysis', '--Analysis', action='store_true',
  help=help_text,
  default=yaml_default
)

#yaml_default = '%s%s%s' % (
#  yaml_defaults['url settings']['base path before version'],
#  yaml_default_version,
#  yaml_defaults['url settings']['after_version']
#)

#yaml_default = yaml_defaults['operational']['verbose']
#parser.add_argument('-v', '--verbose', action='count',
#  help='Specify the verbosity, increasing with each mention. '\
#    + 'The default is %s.' % yaml_default, 
#  default=yaml_default
#)

yaml_default = yaml_defaults['rest api login']['username']
parser.add_argument('-l', '--login', '--username',
  help='Specify the login (username) when it is not the default. '\
    + 'The default is %s.' % yaml_default, 
  default=yaml_default
)

yaml_default = yaml_defaults['rest api login']['password']
parser.add_argument('-p', '--password',
  help='Specify the login (password) when it is not the default. '\
    + 'The default is %s.' % yaml_default, 
  default=yaml_default
)


args = parser.parse_args()

# ( put some data validation here for any values provided by the user ...)
#tokens = {}
##tokens['base_path'] = args.base_path
#tokens['domain']        = args.domain
#tokens['port']          = args.port
#tokens['proto']         = args.proto
#tokens['basepath']      = args.basepath
#tokens['rest_version']  = args.rest_version
#tokens['after_version'] = args.after_version
#tokens['verbose']       = args.verbose
#tokens['login']         = args.login
#tokens['password']      = args.password
#tokens['swagger_json']  = args.swagger_json

session_components = vars(args)
base_url = '{proto}://{domain}'.format(**session_components)
if session_components['port']:
     base_url += ':'
base_url += '{port}{basepath}'.format(**session_components)
base_url += '{rest_version}{after_version}'.format(**session_components)
# I had been forming the url specifically for getting only swagger, but 
# it should be useable for other operations too. Leaving the addition
# of the swagger path in the "swagger_recon.py" script.
session_components['base_url'] = base_url



#!/usr/bin/python
import sys
import argparse
import yaml
import re
import requests
from requests.auth import HTTPBasicAuth
import json
from collections import OrderedDict
import pprint
import os.path
from os import path
import pathlib
import common_arg_parse
from common_arg_parse import *

filename = __file__

basename = re.sub(r'^(.*?)\.py', r'\1', filename)

def acquire_swagger_page(**session_components):
    
    # Here we get what the swagger page says in an existing C8 installation,
    # if the one specified exists and is accessible. 

    swagger_eps_and_attribs=OrderedDict({})
    read_swagger = True 
    
    if read_swagger:
        swagger_url = '{base_url}/{swagger_json}'.format(
          **session_components)
        swagger_json = requests.get(swagger_url)
        swagger_python = json.loads(swagger_json.text)
  
        return swagger_python

swagger_ep_paths_and_attribs = acquire_swagger_page(**session_components)

print(json.dumps(swagger_ep_paths_and_attribs, indent=2, sort_keys=True))

print('count of objects: %s' % len(swagger_ep_paths_and_attribs['tags']))


indep_post = []

# These need to be cycled through for GET's on MerchLevelSubtypes. 
# Actually, I cannot do that so simply yet, as these all must be
# values of the header key: "class_name", and I have not mastered
# passing those in the Tavern file. For now, I will just pull 
# merch_level_subypes out of the usual list of suspects.
# also need to pull out /online_crews
merch_level_subtypes_class_names = ["MerchPlanSubtype",
"MerchProductSubtype", "MerchOptionSubtype", "MerchCollectionSubtype",
"MerchFolderSubtype", "MerchSecondaryPlanSubtype"]

complex_endpoints = re.compile('(/merch|/online_crews)') # Criterion for 
# endpoints too complex to be handled automatically. As soon as possible
# this should be moved into a configuration file. 


placeholder_check = r'\{'  # Criterion of synthetic categories described below.
analysis = OrderedDict()
analysis['Methods'] = OrderedDict()
for mypath in swagger_ep_paths_and_attribs['paths']:
    #import pdb;pdb.set_trace()
    match = re.search(complex_endpoints, mypath)
    if not match:

        # First, handle the "natural" methods. Those: get, post, put, delete,
        # which actually appear, verbatim, in the jason returned by Swagger.
        for method in ('get','post','put','delete'):
            if method in swagger_ep_paths_and_attribs['paths'][mypath]:
                if method not in analysis['Methods']:
                    analysis['Methods'][method] = OrderedDict([('Count',
                      'List'),])
                    analysis['Methods'][method]['Count'] = 0
                    analysis['Methods'][method]['List'] = list()
                analysis['Methods'][method]['Count'] += 1
                analysis['Methods'][method]['List'].append(mypath)
        if 'get' in swagger_ep_paths_and_attribs['paths'][mypath]:  
            # Synthesize categories of methods to establish mass-handleable 
            # groups. Synthetic names should self-document: short but
            # reasonably accurate
            placeholder_match = re.search(placeholder_check, mypath)    
            if not placeholder_match:
                if 'get many, top level' not in analysis['Methods']:
                    analysis['Methods']['get many, top level'] = OrderedDict(
                      [('Count','List'),])
                    analysis['Methods']['get many, top level']['Count'] = 0
                    analysis['Methods']['get many, top level']['List'] = list()

                analysis['Methods']['get many, top level']['Count'] += 1
                analysis['Methods']['get many, top level']['List'].append(
                  mypath)

            else:
                if 'get single, or children' not in analysis['Methods']:
                    analysis['Methods']['get single, or children'] = \
                      OrderedDict([('Count','List'),])
                    analysis['Methods']['get single, or children']['Count'] = \
                      0
                    analysis['Methods']['get single, or children']['List'] = \
                      list()
                    
                analysis['Methods']['get single, or children']['Count'] += 1
                analysis['Methods']['get single, or children']['List'].append(
                  mypath)
        if 'post' in swagger_ep_paths_and_attribs['paths'][mypath]:
            # Synthesize categories of methods to establish mass-handleable 
            # groups. Synthetic names should self-document: short but
            # reasonably accurate
            placeholder_match = re.search(placeholder_check, mypath)    
            if not placeholder_match:
                if 'post, top level' not in analysis['Methods']:
                    analysis['Methods']['post, top level'] = OrderedDict(
                      [('Count','List'),])
                    analysis['Methods']['post, top level']['Count'] = 0
                    analysis['Methods']['post, top level']['List'] = list()

                analysis['Methods']['post, top level']['Count'] += 1
                analysis['Methods']['post, top level']['List'].append(
                  mypath)

            else:
                if 'post children' not in analysis['Methods']:
                    analysis['Methods']['post children'] = \
                      OrderedDict([('Count','List'),])
                    analysis['Methods']['post children']['Count'] = \
                      0
                    analysis['Methods']['post children']['List'] = \
                      list()
                    
                analysis['Methods']['post children']['Count'] += 1
                analysis['Methods']['post children']['List'].append(
                  mypath)
                 
                
if args.raw:
    print(json.dumps(swagger_ep_paths_and_attribs['paths'], indent=2, sort_keys=True))

if args.analysis:
    print(json.dumps(analysis, indent=2, sort_keys=True))

if args.tests:  # "tests is the parameter established or CL parameter requesting complement of tests.
    for my_path in analysis['Methods']['get many, top level']['List']:
        # paths coming from swagger json start with a slash and may contain internal slashes. 
        # Title shold NOT have any slashes. This removes the initial slash
        no_initial_slash_path = re.sub(r'^/(.*)', r'\1', my_path)
        #working_dir = os.getcwd()
        #test_fullpath = os.path.join(working_dir, 'test_%s_GET_many_status.tavern.yaml' % no_initial_slash_path)
        test_fullpath = os.path.join('test_%s_GET_many_status.tavern.yaml' % no_initial_slash_path)
    
        #print("\n\n%s\n\n" % test_fullpath)
        #import pdb;pdb.set_trace()
        #if not pathlib.Path.exists(test_fullpath):
        if not path.exists(test_fullpath):
            deslashed_path = re.sub(r'/', r'_', no_initial_slash_path)
            #path_node_sep = get
            if not os.path.exists('%s/test_%s_GET_many_status.tavern.yaml'):
                test_text = '''
    test_name: %s_GET_many_top_level_status
    
    includes:
      - !include validated_login_and_auth.yaml
    
    stages:
      - name: acceptance_limits GET many status
        request:
          url: "{proto:s}://{host:s}:{port:d}{base_path:s}/%s"
          method: GET
          headers:
            content-type: application/json
            cookie: "{test_login_token:s}"
        response:
          status_code: 200''' % (no_initial_slash_path, no_initial_slash_path)
    
            with open('test_%s_GET_many_top_level_status.tavern.yaml' % deslashed_path, 'w', encoding='utf-8') as f:
                f.write(test_text)


        



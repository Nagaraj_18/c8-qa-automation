#!/usr/bin/python
import sys
import argparse
import yaml
import re
import requests
from requests.auth import HTTPBasicAuth
import json
from collections import OrderedDict
import pprint
import os.path
from os import path
import pathlib

filename = __file__

basename = re.sub(r'^(.*?)\.py', r'\1', filename)


with open("start_defaults.yaml", 'r') as defaults_ymlfile:
    yaml_defaults = yaml.load(defaults_ymlfile, Loader=yaml.FullLoader)

aut_server       = 'mg-wf8-sql12.csi.local:8080'
sut_fq_domain    = 'mg-wf8-sql12.centricsoftware.com:8080'
sut_swagger_url  = 'http://%s/csi-requesthandler/api/v2/swagger.json/' % \
  sut_fq_domain

parser = argparse.ArgumentParser(
  description='This script pulls data from the Swagger page of which the '\
    + 'URL is given, with default or optional command-line setting of all '\
    + 'Swagger url components and login parameters. Then, it parses the '\
    + 'data to report as a tool to extend testing or to create or determine '\
    + 'what tests should be run. Defaults for this test, which are also '\
    + 'common to start.py are settable in the "start_defaults.yaml" file.'\
    + 'Defaults specific to this script are settable in a .yaml file of which'\
    + ' the basename is named the same as this script ("%s.yaml") ' % basename\
    + 'So, it is entirely possible to change the name of this script, as '\
    + 'long as the base name of the corresponding .yaml file is changed '\
    + 'to match. If you change the default values there, then this '\
    + 'help message will update to mention the new actual default values.')

yaml_default = yaml_defaults['url settings']['proto']
parser.add_argument('-t', '--proto', '-P', '--transfer', 
  help='Specify the transfer protocol. Default is "%s".' % yaml_default, 
  default=yaml_default
)

yaml_default = yaml_defaults['url settings']['domain']
parser.add_argument('-d', '--domain', '--host', 
  help='Specify the host. The current default is "%s".' % yaml_default,
  default=yaml_default
)

yaml_default = yaml_defaults['url settings']['port']
parser.add_argument('-o', '--port', 
  help='Specify the port. The default is "%s".' % yaml_default,
  default=yaml_default
)

yaml_default_version = yaml_defaults['url settings'][
  'base path before version']
parser.add_argument('-b', '--basepath','--basepath_before_version',
  help='Specify the base path before the version. The default is "%s".' % \
    yaml_default_version,
  default=yaml_default_version
)

yaml_default_version = yaml_defaults['url settings']['rest version']
parser.add_argument('-r', '--rest_version',
  help='Specify the rest version. The default is "%d".' % \
    yaml_default_version,
  default=yaml_default_version
)

yaml_default = yaml_defaults['url settings'][
  'base path after version']
parser.add_argument('-a', '--after_version', '--basepath_after_version',
  help='Specify any base path portion after the rest version. The default ' \
    + 'is "%s".' % yaml_default,
  default=yaml_default
)

yaml_default = yaml_defaults['url settings']['swagger json path']
parser.add_argument('-s', '--swagger_json', '--swagger',
  help='Specify the path to the Swagger json. The default is %s.' % \
  yaml_default,
  default=yaml_default
)

yaml_default = False
help_text = 'Receive the complement of tests above and beyond existing '
help_text += 'endpoint tests. The default is %s.' % yaml_default
help_text += 'The tests should be named distinctly from the usual, '
help_text += 'change-controlled set of tests, so they can be purged if '
help_text += 'necessary, by deleting on the wildcard bounded different '
help_text += 'portion of the text.'
parser.add_argument('-T', '--tests', '--Tests', action='store_true',
  help=help_text,
  default=yaml_default
)

yaml_default = False
help_text = 'Pass only a high level summary of the Swagger JSON through '
help_text += 'as a single, pretty-printed '
help_text += 'JSON data-structure. The default is %s.' % yaml_default
parser.add_argument('-S', '--summary', '--Summary', action='store_true',
  help=help_text,
  default=yaml_default
)

yaml_default = False
help_text = 'Pass the Swagger JSON through as a single, pretty-printed '
help_text += 'JSON data-structure. The default is %s.' % yaml_default
parser.add_argument('-R', '--raw', '--Raw', action='store_true',
  help=help_text,
  default=yaml_default
)

yaml_default = False
help_text = 'Analyze and print the analysis for testing expressed as a '
help_text += 'single, pretty printed json data-structure. The default '
help_text += 'is "%s".' % yaml_default
parser.add_argument('-A', '--analysis', '--Analysis', action='store_true',
  help=help_text,
  default=yaml_default
)

#yaml_default = '%s%s%s' % (
#  yaml_defaults['url settings']['base path before version'],
#  yaml_default_version,
#  yaml_defaults['url settings']['after_version']
#)

yaml_default = yaml_defaults['operational']['verbose']
parser.add_argument('-v', '--verbose', action='count',
  help='Specify the verbosity, increasing with each mention. '\
    + 'The default is %s.' % yaml_default, 
  default=yaml_default
)

args = parser.parse_args()

# ( put some data validation here for any values provided by the user ...)
tokens = {}
#tokens['base_path'] = args.base_path
tokens['domain']    = args.domain
tokens['port']      = ':%s' % args.port
tokens['proto']     = args.proto
tokens['verbose']   = args.verbose

url_components = vars(args)
url = '{proto}://{domain}'.format(**url_components)
if url_components['port']:
     url += ':'
url += '{port}{basepath}'.format(**url_components)
url += '{rest_version}{after_version}'.format(**url_components)
url += '/{swagger_json}'.format(**url_components)

def acquire_swagger_page(url):
    
    # Here we get what the swagger page says in an existing C8 installation,
    # if the one specified exists and is accessible. 

    swagger_eps_and_attribs=OrderedDict({})
    read_swagger = True 
#    try:
#        sess_mgr_0 = API_Test_Framework.StartSession('%s' % aut_server, 
#          api_version=aut_api_version, 
#          login=aut_login, password=aut_password)
#    except:
#        read_swagger = False
#        print "Unable to log into the C8 installation at: %s." % aut_server,
#        print "Do you want to continue without the data from that server?"
#        response = input()
#        match_yes = re.search('^Y(es)?$', response, re.I)
#        if match_yes:
#            read_swagger = True
    
    if read_swagger:
        swagger_json = requests.get(url)
        #sess_mgr_0.log_in()
        #swagger_json = sess_mgr_0.get_swagger_json()
        swagger_python = json.loads(swagger_json.text)
        #raw_paths = swagger_python['paths']
   
        return swagger_python

swagger_ep_paths_and_attribs = acquire_swagger_page(url)

#print(len(swagger_ep_paths_and_attribs))

#print(json.dumps(swagger_ep_paths_and_attribs, indent=2, sort_keys=True))

print('count of objects: %s' % len(swagger_ep_paths_and_attribs['tags']))


indep_post = []

# These need to be cycled through for GET's on MerchLevelSubtypes. 
# Actually, I cannot do that so simply yet, as these all must be
# values of the header key: "class_name", and I have not mastered
# passing those in the Tavern file. For now, I will just pull 
# merch_level_subypes out of the usual list of suspects.
# also need to pull out /online_crews
merch_level_subtypes_class_names = ["MerchPlanSubtype",
"MerchProductSubtype", "MerchOptionSubtype", "MerchCollectionSubtype",
"MerchFolderSubtype", "MerchSecondaryPlanSubtype"]

complex_endpoints = re.compile('(/merch|/online_crews)') # Criterion for 
# endpoints too complex to be handled automatically. As soon as possible
# this should be moved into a configuration file. 


placeholder_check = r'\{'  # Criterion of synthetic categories described below.
analysis = OrderedDict()
analysis['Methods'] = OrderedDict()
for mypath in swagger_ep_paths_and_attribs['paths']:
    #import pdb;pdb.set_trace()
    match = re.search(complex_endpoints, mypath)
    if not match:

        # First, handle the "natural" methods. Those: get, post, put, delete,
        # which actually appear, verbatim, in the jason returne by Swagger.
        for method in ('get','post','put','delete'):
            if method in swagger_ep_paths_and_attribs['paths'][mypath]:
                if method not in analysis['Methods']:
                    analysis['Methods'][method] = OrderedDict([('Count',
                      'List'),])
                    analysis['Methods'][method]['Count'] = 0
                    analysis['Methods'][method]['List'] = list()
                analysis['Methods'][method]['Count'] += 1
                analysis['Methods'][method]['List'].append(mypath)
        if 'get' in swagger_ep_paths_and_attribs['paths'][mypath]:  
            # Synthesize categories of methods to establish mass-handleable 
            # groups. Synthetic names should self-document: short but
            # reasonably accurate
            placeholder_match = re.search(placeholder_check, mypath)    
            if not placeholder_match:
                if 'get many, top level' not in analysis['Methods']:
                    analysis['Methods']['get many, top level'] = OrderedDict(
                      [('Count','List'),])
                    analysis['Methods']['get many, top level']['Count'] = 0
                    analysis['Methods']['get many, top level']['List'] = list()

                analysis['Methods']['get many, top level']['Count'] += 1
                analysis['Methods']['get many, top level']['List'].append(
                  mypath)

            else:
                if 'get single, or children' not in analysis['Methods']:
                    analysis['Methods']['get single, or children'] = \
                      OrderedDict([('Count','List'),])
                    analysis['Methods']['get single, or children']['Count'] = \
                      0
                    analysis['Methods']['get single, or children']['List'] = \
                      list()
                    
                analysis['Methods']['get single, or children']['Count'] += 1
                analysis['Methods']['get single, or children']['List'].append(
                  mypath)
                 
                
if args.raw:
    print(json.dumps(swagger_ep_paths_and_attribs['paths'], indent=2, sort_keys=True))

if args.analysis:
    print(json.dumps(analysis, indent=2, sort_keys=True))

if args.tests:  # "tests is the parameter established or CL parameter requesting complement of tests.
    for my_path in analysis['Methods']['get many, top level']['List']:
        # paths coming from swagger json start with a slash and may contain internal slashes. 
        # Title shold NOT have any slashes. This removes the initial slash
        no_initial_slash_path = re.sub(r'^/(.*)', r'\1', my_path)
        #working_dir = os.getcwd()
        #test_fullpath = os.path.join(working_dir, 'test_%s_GET_many_status.tavern.yaml' % no_initial_slash_path)
        test_fullpath = os.path.join('test_%s_GET_many_status.tavern.yaml' % no_initial_slash_path)
    
        #print("\n\n%s\n\n" % test_fullpath)
        #import pdb;pdb.set_trace()
        #if not pathlib.Path.exists(test_fullpath):
        if not path.exists(test_fullpath):
            deslashed_path = re.sub(r'/', r'_', no_initial_slash_path)
            #path_node_sep = get
            if not os.path.exists('%s/test_%s_GET_many_status.tavern.yaml'):
                test_text = '''
    test_name: %s_GET_many_top_level_status
    
    includes:
      - !include validated_login_and_auth.yaml
    
    stages:
      - name: acceptance_limits GET many status
        request:
          url: "{proto:s}://{host:s}:{port:d}{base_path:s}/%s"
          method: GET
          headers:
            content-type: application/json
            cookie: "{test_login_token:s}"
        response:
          status_code: 200''' % (no_initial_slash_path, no_initial_slash_path)
    
            with open('test_%s_GET_many_top_level_status.tavern.yaml' % deslashed_path, 'w', encoding='utf-8') as f:
                f.write(test_text)


        



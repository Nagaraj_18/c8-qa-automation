#!/usr/bin/env rakudo
use Getopt::Long;
#use Grammar::Tracer;

#get-options('req|request|q=s');


#grammar OLTavernTest {
#    #token TOP                  { <undo_defs_flags>? <test> }
#    token TOP                  { <test> }
#    
#    #token undo_defs_flags      { (\s*\-{1..2} (no)? <[a..z-]>+) }
#    token test                 { <explicit-marks>? ( <method> <endpoint> <payload>? <url-params>? <explicit-headers>? <response>? )+ }
#    token explicit-marks       { (\s*\-\-marks <-[\- ]>+)? } # under
#    token method               { ([\s*\-\-]:i(post|get|put|delete)) }
#    token endpoint             { (\s+\/<[a..z\/\{\}0..9_]>+) }
#    #token endpoint             { \s+\/<[a..z_]>+ }
#    token url-params           { (\s+ \-\-params (\s+ <permitted-param> )+ )? } 
#    token permitted-param      { ( <pagination>? | <boolean-param>? | <attribute-filter>? | <action>? )? }
#    token pagination           { ( ( skip | limit ) \: \s* \d+)? }
#    token boolean-param        { (\s+ ( <decoded> | <validation> | <log-perf> | <sort> ) \: \s* (true|false))? }
#    token attribute-filter     { ^ <[a..z\/\{\}0..9_]> ('='|'=!'|'=ge'|'=gt'|'=le'|'=lt') \w+ } # Note to self: expressions with "=" must be quoted in a regex now. 
#    token action               { (\s+action\: \s* (APPROVE|REOPEN|CLOSE)) }  
#    token decoded              { (\s+decoded\:    \s* <true-false>) } 
#    token validation           { (\s+validation\: \s* <true-false>) } 
#    token log-perf             { (\s+log_perf\:   \s* <true-false>) } 
#    token sort                 { (\s+sort\:       \s* <true-false>) } 
#    token explicit-headers     { (\s+\-\-eheaders <-[\- ]>)? }  # The security token should always be maintained as an IMplicit header. 
#    token payload              { (\s+ \-\-json \s+ ( <just-the-actual-json> | <ext> )?)? }
#    #token payload              { (\s+\-\-json \s+ ( \w+ | <ext> )) }
#    #token payload              { (\s+\-\-json \s+ ( <[a..z0..9_]>+ \: \s+ <[a..z0..9"'_ ]>+ | <ext> )) }
#    token response             { ( \-\-response \s+ <status>? <response-json>? <verify-response-with>? <save-values>?)? }
#    token status               { ( \-\-status(_code)?\s+(\d\d\d)) }
#    token response-json        { ( ( \-\-strict (true|false))? \s+ <generalized-json> )? }
#    token verify-response-with { ( \-\-verify(_response_with)? \s+ <function-and-args> )? }
#    token save-values          { ( \-\-save \s+ <ext>? <local-saving-json>?)? }           
#    token ext                  { ( \-\-ext\s+ <function-and-args>)? }
#    token function-and-args    { (\s+ \-\-function\s+(<[a..z0..9_]>+\:<[a..z0..9_]>+)\s*\-\-extra_kwargs(\s+<[a..z\/\{\}\<\>0..9_]>+(\:|\s+)<[a..z\/\{\}0..9_ ]>+)+)? }
#    token local-saving-json    { ( <generalized-json> )? }
#    token generalized-json     { ( \-\-json \s+ <just-the-actual-json> )? }
#    token just-the-actual-json { ( <[a..z0..9_]>+ \: \s* <[a..z0..9\"\'_ ]>+ ) }
#    token true-false           { (true|false) }               
#    #token just-the-actual-json { .* }
#
#}

my @tests = [
  '--marks antiflail icecream --post /materials', '--post /materials', '--DELETE /materials/{material}', '--PUT /styles/{style_id} --json node_name: djbouti',
  '--GET /wampum/stompum --params skip:10', '--GET /wampum/stompum --params skip:10 limit:300', '--PUT /wampum/stompum/{step} --json node_name:big_boy_blue --params skip:10 limit:300',
  '--GET /overloads/{whazzzup}/two --json --ext --function utils:get_my_thing --extra_kwargs one1:1',
  '--GET /overloads/{whazzzup}/three --json --ext --function utils:get_my_thing --extra_kwargs one1:1 two2:2',
  '--PUT /sandals/resort/reservation --params limit:10000 --json node_name: quilbert',
  '--PUT /sandals/resort/reservation --json node_name: quilbert --params limit:10000 ',
  '--GET /upsy/downsy/dooly --params limit:10000 --response --status_code 200 --save --json trial_dooly_01_id:id',
  '--POST /seasons --json --ext --function utils:differentiated_general_object --extra_kwargs <fdt>node_name:Trial Season 01 --response --save-values --json season_id:id --POST /seasons/hierarchy',
  '--POST /seasons --json --ext --function utils:differentiated_general_object --extra_kwargs node_name:Trial_Season_01',
  '--POST /seasons --json '

  ];

grammar OLTavernTest {
    #token TOP                  { <undo_defs_flags>? <test> }
    token TOP                  { <test> }
    
    #token undo_defs_flags      { (\s*\-{1..2} (no)? <[a..z-]>+) }
    token test                 { <explicit-marks>? ( <method> \s+ <endpoint> <payload>? <url-params>? <explicit-headers>? <response>? )+ }
    token explicit-marks       { (\s*\-\-marks <-[\- ]>+)? } # under
    token method               { ([\s*]:i(post|get|put|delete)) }
    token endpoint             { (\/<[a..z\/\{\}0..9_]>+) }
    #token endpoint             { \s+\/<[a..z_]>+ }
    token url-params           { (\s+ params ( <permitted-param> )+? ) } 
    #token permitted-param      { (<pagination>? <boolean-param>? <attribute-filter>? <action>?) }
    #regex permitted-param      { ( <pagination>? | <boolean-param>? | <attribute-filter>? | <action>? )  }
    regex permitted-param      { ( <pagination> | <boolean-param> | <attribute-filter> | <action> )+?  }
    #regex permitted-param      { ( <attribute-filter>? | <boolean-param>? | <pagination>? | <action>? )  }
    #token permitted-param      {  <boolean-param> }
    token pagination           { ( ( skip | limit ) \: \s* \d+)? }
    token boolean-param        { ( <decoded>? <validation>? <log-perf>? <sort>? ) }
    #token boolean-param        { <decoded> }
    token attribute-filter     { ( \s+ <[a..z\/\{\}0..9_]>+ ('='|'=!'|'=ge'|'=gt'|'=le'|'=lt') <[a..z\/\{\}0..9_]>+ )+? } # Note to self: expressions with "=" must be quoted in a regex now. 
    #token attribute-filter     { <[a..z\/\{\}0..9_]>+ (up|down|left|right|in|out) <[a..z\/\{\}0..9_]>+ } # Note to self: expressions with "=" must be quoted in a regex now. 
    #regex attribute-filter     { \s+ <[a..z]>+ } # Note to self: expressions with "=" must be quoted in a regex now. 
    #regex attribute-filter     { ( .* ) } # Note to self: expressions with "=" must be quoted in a regex now. 
    token action               { (\s+ action\: \s* (APPROVE|REOPEN|CLOSE)) }  
    #token decoded              { (\s decoded: <true-false>) } 
    token decoded              { (\s+ decoded\: <true-false>) } 
    token validation           { (\s+ validation\: \s* <true-false>) } 
    token log-perf             { (\s+ log_perf\:   \s* <true-false>) } 
    token sort                 { (\s+ sort\:       \s* <true-false>) } 
    token explicit-headers     { (\s+\-\-eheaders <-[\- ]>)? }  # The security token should always be maintained as an IMplicit header. 
    token payload              { (\s+ json \s+ ( <just-the-actual-json> | <ext> )?)? }
    #token payload              { (\s+\-\-json \s+ ( \w+ | <ext> )) }
    #token payload              { (\s+\-\-json \s+ ( <[a..z0..9_]>+ \: \s+ <[a..z0..9"'_ ]>+ | <ext> )) }
    token response             { ( \-\-response \s+ <status>? <response-json>? <verify-response-with>? <save-values>?)? }
    token status               { ( \-\-status(_code)?\s+(\d\d\d)) }
    token response-json        { ( ( \-\-strict (true|false))? \s+ <generalized-json> )? }
    token verify-response-with { ( \-\-verify(_response_with)? \s+ <function-and-args> )? }
    token save-values          { ( \-\-save \s+ <ext>? <local-saving-json>?)? }           
    token ext                  { ( \-\-ext\s+ <function-and-args>)? }
    token function-and-args    { (\s+ \-\-function\s+(<[a..z0..9_]>+\:<[a..z0..9_]>+)\s*\-\-extra_kwargs(\s+<[a..z\/\{\}\<\>0..9_]>+(\:|\s+)<[a..z\/\{\}0..9_ ]>+)+)? }
    token local-saving-json    { ( <generalized-json> )? }
    token generalized-json     { ( \-\-json \s+ <just-the-actual-json> )? }
    token just-the-actual-json { ( <[a..z0..9_]>+ \: \s* <[a..z0..9\"\'_ ]>+ ) }
    token true-false           { (true|false) }               
    #token just-the-actual-json { .* }

}


#sub MAIN(
#    Str :@req where /:i (post|create) | (get|read ) | (put|update) | delete  /
#    ) {
#    say " got:\n\t" ~ join "\n\t", @req;
#}

#for @tests -> $test {
#    put "\n\n$test";
#    my $result= OLTavernTest.parse($test);
#    put $result ?? $result !! 'Failed';
#}

#sub MAIN(
#    Str :@req where /:i (post|create) | (get|read ) | (put|update) | delete  /
#    ) {
#    say " got:\n\t" ~ join "\n\t", @req;
#}

#sub MAIN(
#    Str :@req where OLTavernTest.parse
#    ) {
#    say " got:\n\t" ~ join "\n\t", @req;
#}

sub MAIN(:$list1, *@args, *%flags)
{
    
    #say " got:\n\t" ~ join "\n\t", @*ARGS;
    my $cl-candidate = join " ", @*ARGS;
    put "\n\n$cl-candidate";
    my $result =  OLTavernTest.parse($cl-candidate);
    put $result ?? "Passed:\n$result" !! 'Failed';
    
}




#!/usr/bin/perl 

my @domain_classes_hse_exported = qw{
ReviewColorway
ReviewColorwayConfig
ReviewColorwayRevision
ReviewColorwaySubtype
ReviewSKU
ReviewSKUConfig
ReviewSKURevision
ReviewSKUSubtype
ReviewStyle
ReviewStyleConfig
ReviewStyleRevision
ReviewStyleSubtype
Role
Routing
RoutingConfig
RoutingGroup
RoutingItem
RoutingRevision
RoutingSubtype
SalesCollection
SalesDivision
SalesDivisionCrew
SalesLook
SalesMarket
SalesMarketCrew
SalesOrder
SalesOrderGroup
SalesOrderLineItem
SalesOrderLineItemSKU
SalesPromo
Sample
SampleDimensions
SampleMaterialSpec
SampleStorage
SamplingPlan
SamplingPlanItem
Scene7ImageStore
Scorecard
ScorecardItem
ScorecardReport
Season
SelectSet
SelectSetConfig
SelectSetItem
SelectSetSubtype
Shape
ShapeConfig
ShapeMaster
ShapeSecurityGroup
SharedCrew
Shipment
ShipmentQCIssue
ShipmentTerms
ShippingContainer
ShippingPort
ShippingRate
SiteLibSupplierItem
SitePreferences
SizeChart
SizeChartAdmin
SizeChartConfig
SizeChartDimension
SizeChartReview
SizeChartReviewConfig
SizeChartReviewDimension
SizeChartReviewRevision
SizeChartReviewSize
SizeChartRevision
SizeChartSubSizeRange
SizeChartSubtype
SizeLabel
SizeLabelSize
SizeMap
SizeOnProduct
SizeRange
SizeSpec
SKU
SKUMaterial
SMProduct
SMProductColor
SMProductSKU
SOLineItem
SourcingCapability
SourcingItemCapability
SourcingModule
SourcingViews
SpecificationDataSheet
SpecificationDataSheetConfig
SpecificationDataSheetItem
SpecificationDataSheetRevision
SpecificationDataSheetSubtype
SpecificationItemDefinition
SpecificationSection
SpecificationSectionDefinition
SpecificationSectionItem
SRLineItem
StockOrder
StructItemNameCode
StructureItemDefaults
Style
StyleCopyOptions
StylePushTemplate
StyleType
SubQuestion
SubRouting
SubRoutingConfig
SubRoutingItem
SubRoutingRevision
SubRoutingSubtype
SubSizeRange
Supplier
SupplierConfig
SupplierContractualDocument
SupplierContractualDocumentConfig
SupplierContractualDocumentRevision
SupplierContractualDocumentSubtype
SupplierCrew
SupplierDiscount
SupplierItem
SupplierItemConfig
SupplierItemRevision
SupplierItemSubtype
SupplierRequest
SupplierRequestCrew
SupplierRequestTemplate
SupplierReview
SupplierReviewEvent
SupplierReviewEventConfig
SupplierSubtype
SynchronizedDBExporter
SynchronizedDBExporterParameters
TableColumnReport
Tag
TagSpec
Team
TeamMember
Test
TestRun
TestRunConfig
TestRunRevision
TestRunSection
TestRunSubtype
TestSpec
TestSpecConfig
TestSpecGroup
TestSpecRevision
TestSpecSubtype
Theme
ThemeConfig
ThemeMaster
ThemeMasterConfig
ThemeMasterSubtype
ThemeProductAlternative
ThemeSecurityGroup
TrackActivity
TrackingPattern
User
UserDefaults
UserManager
ViewConfig_Site
Wbs
WbsAdmin
WbsConfig
WbsDependency
WbsElement
WbsRequest
Workflow
WorkflowAdmin
WorkflowConfig
WorkflowHolder
WorkflowNotifiee
WorkflowNotifieeGroup
WorkflowNotifieeRole
WorkflowRevision
WorkflowStep
WorkflowStepParticipant
WorkflowStepParticipantRole
WorkflowStepTransition
WorkflowViews
XMLExporter
XMLExporterParameters
};

sub centric_domain_class_name_pluralize{
    my $domain_class_name = shift;
    if ($domain_class_name =~ /^.*?(?:Colorway)/i){
        $domain_class_name .= 's';
    }
    elsif ($domain_class_name =~ /^(.*?)y$/){
        $domain_class_name = $1 . 'ies';
    }
    elsif ($domain_class_name =~ /^(.*?)ity$/){
        $domain_class_name = $1 . 'ities';
    }
    elsif ($domain_class_name =~ /^(.*?)s$/){
        $domain_class_name = $1 . 's';
    }
    elsif ($domain_class_name =~ /^(.*?)ix$/){
        # $domain_class_name = $1 . 'ixes';      #I think this is the right way. It could also be 'ices', but that depends on the etymology of the word.
        $domain_class_name = $1 . 'ixs';      #Currently, the product does it this way, even if it is wrong:
    }
    else {
        $domain_class_name .= 's';
    }

    return $domain_class_name;
}

sub snakify{
    my $s1 = shift;
    #if ($s1 =~ /^.*?B.*$/){$DB::single = 1;}
    $s1 =~ s/([a-z])BOM/$1_bom/g;            # Proactively handle acronyms and abbreviations: BOM.
    $s1 =~ s/SKU/_sku_/g;            # Proactively handle acronyms and abbreviations: PDF.
    $s1 =~ s/XML/_xml_/g;            # Proactively handle acronyms and abbreviations: XML.
    $s1 =~ s/SO/_so_/g;              # Proactively handle acronyms and abbreviations: SO.
    $s1 =~ s/SM/_sm_/g;              # Proactively handle acronyms and abbreviations: SM.
    $s1 =~ s/SR/_sr_/g;              # Proactively handle acronyms and abbreviations: SR.
    #$s1 =~ s/Scene7Image/_scene7_image/g; #I think this is the conformant way.
    $s1 =~ s/Scene7Image/_scene7image/g; #Currently, the product does it this way, even if it is non-conformant 
    $s1 =~ s/(.)([A-Z][a-z]+)/$1_$2/g;
    $s1 =~ s/([a-z0-9])([A-Z])/$1_$2/g;
    $s1 =~ s/_+/_/g;                         # Condense multiple underscores into just one.
    $s1 =~ s/^_?([^_].*?[^_])_?$/$1/;                         # Trim any straggling underscores.
    $s1 = lc($s1);
    return $s1;
}

%path_ends_by_classname;
for my $classname (@domain_classes_hse_exported){
    my $path_end = snakify($classname);
    $path_end = centric_domain_class_name_pluralize($path_end);
    $path_ends_by_classname{$classname} = $path_end;
    #printf "\n%s: %s", $classname, $path_end;
}

    
for my $classname (keys %path_ends_by_classname){
    $path_end = $path_ends_by_classname{$classname};
    printf("\n/Users/mgipson/scripts/python/REST_API_Tavern_top/isolated_hs_export_tavern/test_%s_GET_many_status.tavern.yaml", $classname);

    $filename = sprintf('/Users/mgipson/scripts/python/REST_API_Tavern_top/isolated_hs_export_tavern/test_%s_(%s)_GET_many_status.tavern.yaml', $classname, $path_end);
    open(my $fh_curr_test_yaml, ">", $filename);
    my $test_content = <<"HERE";
test_name: ${classname}_GET_many_status

includes:
  - !include hse_rest_api_url.yaml

stages:
  - name: ${classname} GET many status
    request:
      url: \"{proto:s}://{host:s}:{port:d}{base_path:s}/${path_end}\"
      method: GET
      headers:
        content-type: application/json
    response:
      status_code: 200
HERE

    print $fh_curr_test_yaml $test_content;

    close $fh_curr_test_yaml;
}

#print json.dumps(swagger_eps_and_attribs, indent=2, sort_keys=True)


#!/usr/bin/python
import sys
import inflect
import re

domain_classes_hse_exported = '''_CS_Job
_CS_Phase
_CS_PreferenceView
_CS_Site
AcceptanceLimit
ActionItem
ActionItemStep
ActionItemStepResponse
Activity
ApparelBOM
ApparelBOMConfig
ApparelBOMRevision
ApparelBOMSubtype
ApparelDimension
Artwork
ArtworkConfig
ArtworkRevision
ArtworkSubtype
AssortmentItem
AttributeValuePusher
Audit
BOMSectionDefinition
BOMSupplierAttributes
BPExpressions
BPMarketPlan
BPMarketProductPlan
BPProductPlan
BuildInAttribute
BusinessCategory
BusinessMarket
BusinessObject
BusinessPlan
Calendar
CalendarDescriptor
CalendarLevel
CalendarRequest
CalendarTrackingType
CamundaDeployment
CamundaProcessDefinition
CamundaProcessInstance
CamundaTask
CamundaTaskDefinition
CanvasTemplate
CareAndComposition
CareAndCompositionConfig
CareAndCompositionRevision
CareAndCompositionSubtype
CareSymbol
Category1
Category2
Classifier0
Classifier1
Classifier2
Classifier3
Collection
CollectionBOM
CollectionColor
CollectionColorConfig
CollectionColorRevision
CollectionMaterial
CollectionMaterialConfig
CollectionMaterialRevision
CollectionMatrix
CollectionMatrixConfig
CollectionMatrixRevision
CollectionMatrixSubtype
CollectionSupplier
ColorColorwayContext
ColorColorwayContextConfig
ColorDataSheet
ColorDataSheetConfig
ColorDataSheetRevision
ColorDataSheetSubtype
ColorMatchingRule
ColorMatchingRuleMaster
ColorMatchingType
ColorMaterial
ColorSpecification
Colorway
ColorwayConfig
Comment
CommentReply
CompanyAnnouncement
CompanyInfo
CompetitiveStyle
Composition
CompositionPlacement
ConfigurableAttributeGroup
ConfigurableValidationRule
ConfigurableValidationRuleGroup
Configuration
Contact
ContactList
ContractualDocument
ContractualDocumentConfig
Country
Language
Style
User
WorkflowNotifieeGroup'''.split("\n")

p = inflect.engine()

def snakify(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()

for classname in domain_classes_hse_exported:
    path_end = p.plural(classname)
    path_end = snakify(path_end)
    print "%s: %s" % (classname, path_end)
    
quit()
#swagger_ep_paths = acquire_swagger_page()
#
#print len(domain_classes_hse_exported)
#
#for path in swagger_ep_paths:
#    slugified_path = re.sub(r'/', r'', path)
#    curr_test_yaml = open(r'/Users/mgipson/scripts/python/REST_API_Tavern_top/isolated_tavern/test_%s_GET_many_status.tavern.yaml' % slugified_path, 'w')
#    curr_test_yaml.write('''
#test_name: %(test_name)s_GET_many_status
#
#includes:
#  - !include validated_login_and_auth.yaml
#
#stages:
#  - name: %(test_name)s GET many status
#    request:
#      url: "{proto:s}://{host:s}:{port:d}{base_path:s}%(ep_path)s"
#      method: GET
#      headers:
#        content-type: application/json
#        cookie: "{test_login_token:s}"
#    response:
#      status_code: 200''' % {'test_name':slugified_path, 'ep_path':path})        
#    curr_test_yaml.close()
#
#
##print json.dumps(swagger_eps_and_attribs, indent=2, sort_keys=True)
#

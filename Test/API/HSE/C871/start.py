#!/usr/bin/var python
import argparse
import yaml
import json
import re
import requests
import subprocess
import sys

#config = ConfigParser.ConfigParser()
#config.read('start.cfg')

with open("start_defaults.yaml", 'r') as defaults_ymlfile:
    yaml_defaults = yaml.load(defaults_ymlfile, Loader=yaml.FullLoader)
#print defaults
#quit()    

parser = argparse.ArgumentParser(
  description='Provide a fresh security token for each test run. Do this '\
    + 'with default or optional command-line setting of all REST API url '\
    + 'components and login parameters. Then, actually login to validate '\
    + 'them, getting the auth token in the process. Finally, write them'\
    + 'to a file that provides them to the pytest/tavern tests.')

yaml_default = yaml_defaults['url settings']['proto']
parser.add_argument('-t', '--proto', '-P', '--transfer', 
  help='Specify the transfer protocol. Default is "%s".' % yaml_default, 
  default=yaml_default
)

yaml_default = yaml_defaults['url settings']['domain']
parser.add_argument('-d', '--domain', '--host', 
  help='Specify the host. The current default is "%s".' % yaml_default,
  default=yaml_default
)

yaml_default = yaml_defaults['url settings']['port']
parser.add_argument('-o', '--port', 
  help='Specify the port. The default is "%s".' % yaml_default,
  default=yaml_default
)

yaml_default_version = yaml_defaults['url settings']['rest version']
parser.add_argument('-r', '--rest_version', 
  help='Specify the rest version. The default is "%d".' % yaml_default_version,
  default=yaml_default_version
)

yaml_default = '%s%d%s' % (
  yaml_defaults['url settings']['base path before version'],
  yaml_default_version,
  yaml_defaults['url settings']['base path after version']
)
parser.add_argument('-b', '--base_path', 
  help='Specify the base path. The default is "%s". ' % yaml_default\
    + 'Note that the default for this is dependent upon the '\
    + 'value chosen for -r/--rest_version (the default of -r is '\
    + 'currently "%s" and is settable in the '%  yaml_default_version\
    + '"start_defaults.yaml" file that accompanies this script).', 
  default=yaml_default
)

yaml_default = yaml_defaults['rest api login']['username']
parser.add_argument('-u', '--username', '--user', 
  help='Specify the C8 login of the user. A default is provided.',
  default=yaml_default
)

yaml_default = yaml_defaults['rest api login']['password']
parser.add_argument('-p', '--password', 
  help='Specify the C8 password of the user. A default is provided.',
  default = yaml_default
)

yaml_default = yaml_defaults['operational']['verbose']
parser.add_argument('-v', '--verbose', action='count',
  help='Specify the verbosity, increasing with each mention. '\
    + 'The default is %s.' % yaml_default, 
  default=yaml_default
)

yaml_file_basename_default = yaml_defaults['operational'][
  'yaml output file basename']
yaml_file_extension_default = yaml_defaults['operational'][
  'yaml output file extension']
yaml_default = '%s.%s' % (yaml_file_basename_default,
  yaml_file_extension_default)
parser.add_argument('-f', '--filename',
  help='Specify the filename for the output. The default is'\
    + ' "%s". So, by specifying an alternative here, ' % yaml_default\
    + ' you can avoid overwriting that'\
    + ' file. But, the tests expect the default name, so when you have'\
    + ' validated the output to an alternative, either change its name'\
    + ' to replace the old "%s", ' % yaml_default\
    + ' or simply rerun this test without the -f/--filename option.',
  default = yaml_default
)

yaml_default = yaml_defaults['operational']['suppress the tests']
parser.add_argument('-s', '--suppress', '--suppress_tests', 
  action='store_false', help='Specify suppression of test kickoff '\
    + 'The default is "%s".' % yaml_default, 
  default=yaml_default
)

yaml_default = yaml_defaults['operational']['test execution']
parser.add_argument('-e', '--execute', '--execute_tests', 
  help='Specify the command to execute the tests. '\
    + 'The default is "%s".' % yaml_default, 
  default=yaml_default
)



args = parser.parse_args()

# ( put some data validation here for any values provided by the user ...)
base_path = args.base_path
domain    = args.domain
execute   = args.execute
filename  = args.filename
password  = args.password
port      = args.port
proto     = args.proto
suppress  = args.suppress
verbose   = args.verbose
username  = args.username

# Make sure the proposed filename has only filename-suitable characters.
slug_case_match = re.match('^[a-zA-Z0-9_.-]+$',filename)
if not slug_case_match:
    print 'The file name "%s" was unuseable. It should  ' % filename\
      + 'consist of only lower and uppercase letters, numbers, '\
      + 'underscores, dashes and (a) dot(s)'
    sys.exit(1)

# Ensure the default yaml file extension is applied (if necessary).
filename = re.sub(
  r'^(.*?[^.])\.?((?i)%s)?$' % yaml_file_extension_default,
  r'\1.%s' % yaml_file_extension_default, filename)



semi_valid_tokens = {}
semi_valid_tokens['base_path'] = base_path
semi_valid_tokens['domain'   ] = domain
semi_valid_tokens['execute'  ] = execute
semi_valid_tokens['filename' ] = filename
semi_valid_tokens['password' ] = password
semi_valid_tokens['port'     ] = port
semi_valid_tokens['proto'    ] = proto
semi_valid_tokens['suppress' ] = suppress
semi_valid_tokens['username' ] = username
semi_valid_tokens['verbose'  ] = verbose

url = '%(proto)s://%(domain)s:%(port)s%(base_path)s/session' % semi_valid_tokens

if args.verbose > 2:
    print url
headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}
payload = {'username': semi_valid_tokens['username'], 
  'password': semi_valid_tokens['password']}
login_valid = False
auth_token  = ''
with requests.Session() as s:
    try:
        r = s.post(url, headers=headers, data=json.dumps(payload))
    except:
        message = ''
        if args.verbose:
            message = "The login attempt did not succeed."
        print(message)
        sys.exit(1)

    if args.verbose > 1:
        print "status: %s" % r.status_code
    if r.status_code == 200:
        login_valid = True
        auth_token = json.loads(r.text)['token']
         
    else:
        message = ''
        if args.verbose:
            message = "The login did not succeed. Status %s " % r.status_code \
              + "was returned."
        print(message)
        sys.exit(1)

# If all lines of defense have been passed, overwrite the filename.

valid_tokens = semi_valid_tokens

valid_tokens['auth_token'] = auth_token

yaml_file_contents = '''# %(filename)s
---

# Each file should have a name and description.
name: Common test information
description: Login for test server and other information

# Variables should just be a mapping of key: value pairs
variables:
    # transfer_proto: the http protocol. this is either "http" or "https"
    proto: %(proto)s
    host: %(domain)s
    # port: 80 is M.S. IIS, 8080 is Undertow
    port: %(port)s  
    base_path: %(base_path)s
    username: %(username)s
    password: %(password)s

    # It is important to understand that the value on the
    # following line of code has a short shelf-life: about 
    # an hour after the last activity that used it. 
    # It also seems to expire a little after midnight, no
    # matter what activity has led up to that time. 
    # The following value can be hand hard-coded for manually 
    # testing, as in script development. For scalability and in
    # production however, it will be critical that this be 
    # written by a script, preferably after the script
    # successfully issues a Session POST. That script should
    # allow intake of the login and password as optional
    # command line parameters (optional because defaults exist). 
    # So, as long as it succeeds in validating the login and 
    # password, and gets the auth token, it can then overwrite
    # this file's values with what succeeded, then kick off the
    # test run. For official recordkeeping the test run should
    # also include a test that simply logs in with the token so
    # acquired.

    test_login_token: %(auth_token)s
''' % valid_tokens

try:
    output_fh = open(valid_tokens['filename'], 'w')
except:
    quit_message = ''
    if args.verbose:
        quit_message = "Could not open output file for writing."
    quit(quit_message)

with output_fh:
    output_fh.write(yaml_file_contents)
    output_fh.close()

if not suppress:
    try:
        print subprocess.Popen('%s' % execute, shell=True, stdout=subprocess.PIPE).stdout.read()
    except Exception, e:
        if verbose:
            print 'Execution of "%s" failed: "%s".' % (execute, e)
        sys.exit(1)





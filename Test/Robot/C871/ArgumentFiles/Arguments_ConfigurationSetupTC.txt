--name  Sanity Configuration Setup Test Cases
..\src\test\robotframework\Configuration\TC_CreateStyleTypeAndAttribute.txt
..\src\test\robotframework\Configuration\TC_CreateStyleTypeDataSheet.txt
..\src\test\robotframework\Configuration\TC_CreateMaterialType.txt
..\src\test\robotframework\Configuration\TC_CreateBOMType.txt
..\src\test\robotframework\Configuration\TC_CreateThemeMasterType.txt
..\src\test\robotframework\Configuration\TC_CreateSpecLibraryItemType.txt
..\src\test\robotframework\Configuration\TC_CreateTestType.txt
..\src\test\robotframework\Configuration\TC_CreateSizeChartType.txt
..\src\test\robotframework\Configuration\TC_CreateContractualDocumentType.txt
..\src\test\robotframework\Configuration\TC_CreateMerchandiseType.txt
..\src\test\robotframework\Configuration\TC_UpdateConfiguration.txt



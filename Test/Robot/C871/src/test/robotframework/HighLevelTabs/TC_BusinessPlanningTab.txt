*** Settings ***

Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt
Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_StylePage.txt 
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../PageObjects/PO_SourcingPage.txt
Resource          ../PageObjects/PO_SpecificationPage.txt
Resource          ../PageObjects/PO_Calendar.txt
Resource          ../Locators/Loc_Documents.txt
Resource          ../Locators/Loc_BusinessPlanning.txt
Resource          ../Locators/Loc_CompanyCurrencyPage.txt

**Variables**
${BusinessCategoryStyleName1}  BP_StyleCategory1
${MaterialBusinessCategoryName1}  BP_MatCategory1
${StyleBusinessPlanName1}  BP_Style
${Currency1}  Yen
*** Test Cases ***
Test Setup        
    DataSetup Create Sourcing Data
Login To Application
	#Data provider: It reads the data from the excel sheet for the test case specified. 
    Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    Wait.DOMready
    DOMready-TabLoads
Create Currency
    Click on Setup Icon
    Wait.DOMready
    DOMready-TabLoads 
    Click Element  ${Loc_Setup-CompanyTab}
    DOMReadyByWaitTime  5 
    Click Element  ${Loc_Currency_Tab}
    DOMReadyByWaitTime  5
    Mouse Over  ${Loc_Setup-CurrencyTab_ActionsLink}
    Click Element  ${Loc_Setup-CurrencyTab_ActionsLink}     
    DOMReadyByWaitTime  4
    Input Text  //div/textarea  ${Currency1}
    Click Element    ${Loc_Currency_Tab}
    DOMReadyByWaitTime  5
    Assert for the Given Element  //a[@class='browse' and text()='${Currency1}']
    DOMReadyByWaitTime  5  
Tabs Under Business Planning Tab
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads
  Click Element     ${Loc_Home-BusinessPlanningTab}
  DOMReadyByWaitTime  5
  Verify the Tab Presence  BusinessPlanningViews-BusinessPlans  Business Plan tab is displayed under Business Planning tab
  Verify the Tab Presence  BusinessPlanningViews-BusinessCategories  Business Categorie tab is displayed under Business Planningtab
Create Style Business Category
   Click Element    ${Loc_Home-BusinessPlanning-BusinessCategoriesTab}
   DOMReadyByWaitTime  5
   Mouse Over  ${Loc_Home-BusinessPlanning-BusinessCategories_ActionsLink}
  #Wait Until Element Is Visible  ${Loc_Home-BusinessPlanning-BusinessCategories_Actions_NewBusinessCategoryLink}
  Click Element   ${Loc_Home-BusinessPlanning-BusinessCategories_ActionsLink}    
  Wait Until Page Contains Element  ${BusinessCategoryDialog} 
    DOMReadyByWaitTime  2
  Click Element  ${NewBusinessCategoryDialog_ProductTypeDDL}
  DOMReadyByWaitTime  2
  Select value from the DDL  Style
  Click Element  ${NewBusinessCategoryDialog_CurrencyDDL}
  DOMReadyByWaitTime  2
  Select value from the DDL  ${Currency1}
  DOMReadyByWaitTime  2
  Click Element  ${NewBusinessCategoryDialog_NodeTextArea}
  DOMReadyByWaitTime  2
  Input Text  ${NewBusinessCategoryDialog_NodeTextArea}  ${BusinessCategoryStyleName1}
  Click on Save Button
  DOMready-PopUpCloses
Create Material Business Category
  Click Element  ${Loc_Home-BusinessPlanning-BusinessCategories_ActionsLink}
  Wait Until Page Contains Element  ${BusinessCategoryDialog} 
  Click Element  ${NewBusinessCategoryDialog_NodeTextArea}
  DOMReadyByWaitTime  2
  Input Text  ${NewBusinessCategoryDialog_NodeTextArea}  ${MaterialBusinessCategoryName1}
  DOMReadyByWaitTime  2
  Click Element  ${NewBusinessCategoryDialog_ProductTypeDDL}
  DOMReadyByWaitTime  2
  Select value from the DDL  Material
  Click Element  ${NewBusinessCategoryDialog_CurrencyDDL}
  DOMReadyByWaitTime  2
  Select value from the DDL  ${Currency1}
  Click on Save Button
  DOMready-PopUpCloses
Create Business Plan
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads    
  Click Element     ${Loc_Home-BusinessPlanningTab}
  DOMReadyByWaitTime  5
  Click Element  ${Loc_Home-BusinessPlanning-BusinessPlansTab}
  DOMReadyByWaitTime  5
  Mouse Over  ${Loc_Home-BusinessPlanning_ActionsLink} 
  Click Element  ${Loc_Home-BusinessPlanning_ActionsLink}  
  Wait Until Page Contains Element  ${BusinessPlanDialog} 
  Click Element  ${NewBusinessPlanDialog_NodeTextArea}
  DOMReadyByWaitTime  2
  Input Text  ${NewBusinessPlanDialog_NodeTextArea}  ${StyleBusinessPlanName1} 
  Click Element  ${NewBusinessPlanDialog_CategoryDDL}
  PO_NewCreationPopUpPage.Select value from the DDL  ${BusinessCategoryStyleName1}
  Click Element  css=[data-csi-act="Finish"]
  DOMready-PopUpCloses
  [Teardown]    Close Browser      
*** Keywords ***
DataSetup Create Sourcing Data

  ${This_DataProvider} =  Data_Provider.DataProvider     Create Sourcing Data
  Set Suite Variable  ${This_DataProvider}
  Log  ${This_DataProvider}
  

  

      
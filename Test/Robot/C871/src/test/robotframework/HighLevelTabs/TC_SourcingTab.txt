*** Settings ***

Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt
Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_StylePage.txt 
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../PageObjects/PO_SourcingPage.txt
Resource          ../PageObjects/PO_SpecificationPage.txt
Resource          ../Locators/Loc_SourcingPage.txt
Resource          ../Locators/Loc_SourcingSupplierPage.txt
Resource          ../Locators/Loc_CollectionManagement.txt

**variables**
${Supplier1}  BSSupplier
${Language1}  Hindi
*** Test Cases ***
Test Setup        
    DataSetup Create Sourcing Data
Login To Application
	#Data provider: It reads the data from the excel sheet for the test case specified. 
    Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    Wait.DOMready
    DOMready-TabLoads

Sourcing Tab
    Click on Home Icon
    DOMready-TabLoads
    Click Element  ${Loc_HomeIcon_SourcingTab}
    DOMready-TabLoads

Verify tabs under Sourcing Tab 
    Click on Home Icon
    DOMready-TabLoads
    Click Element  ${Loc_HomeIcon_SourcingTab}
    DOMready-TabLoads   
    Element Should Be Visible  ${Loc_Home-Sourcing-SupplierPOTab}
    Element Should Be Visible  ${Loc_Home-Sourcing-CustomerPOTab}
    Element Should Be Visible  ${Loc_Home-Sourcing-ShipmentTab}
    Element Should Be Visible  ${Loc_HomeIcon_SourcingTab_SupplierTab}
    Element Should Be Visible  ${Loc_Home-Sourcing-Factory}
    Element Should Be Visible  ${Loc_Sourcing_ReviewsTab}
    Element Should Be Visible  ${Loc_Sourcing_CustomersTab}
    Element Should Be Visible   ${Loc_Sourcing_SalesDivisionsTab}
SupplierTab
    #Navigate to Supplier tab 
    PO_HomePage.Click on Sourcing tab
    DOMready-TabLoads
    PO_SourcingPage.Click on Supplier tab
    DOMready-TabLoads 
Create a Supplier
    Mouse Over  ${Loc_ActionsSupplier}   
    Wait.DOMreadyByWaitTime  1
   Click Element  ${Loc_ActionsSupplier}  
    PO_SetupPage.Verify Dialog Window  New Supplier
    Wait.DOMreadyByWaitTime  2
    Input Text  ${Loc_TextFieldSupplierName}  ${Supplier1} 
	Wait.DOMreadyByWaitTime  1
    Click Element  ${SupplierDlg_IsAgentAttr}
    Wait.DOMreadyByWaitTime  1
    Click Element  ${SupplierDlg_IsSupplierAttr}
    Wait.DOMreadyByWaitTime  1
    PO_NewCreationPopUpPage.Click on Save Button
    Wait.DOMreadyByWaitTime  5    
Create a Customer
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads
  PO_HomePage.Click on Sourcing tab 
  DOMready-TabLoads
  Click Element    	${CustomerTab}
  DOMready-TabLoads
  Click Element    ${CustomerActLink}
  DOMready-PopUpAppears
  Input Text	${PopupCustInput}  Puma
  PO_NewCreationPopUpPage.Click on Save Button
  DOMready-PopUpCloses
  
      
Supplier PO Tab
    Click Element  ${Loc_Home-Sourcing-SupplierPOTab} 
    DOMready-TabLoads
    
Verify tabs under Supplier PO tab
    Element Should Be Visible  ${Loc_Home-Sourcing-SupplierPO-SupplierPOTab} 
    Element Should Be Visible  ${Loc_Home-Sourcing-SupplierPO-OrdersTab}
    Element Should Be Visible  ${Loc_Sourcing_POGroupTab}
    Element Should Be Visible  ${Loc_Sourcing_SupplierPO_SelectSetTab}
    
Verify data creation for Supplier PO tab
  Click Element  ${Loc_Home-Sourcing-SupplierPO-SupplierPOTab}
  DOMready-TabLoads 
  Mouse Over  ${Loc_Home-Sourcing-SupplierPO-SupplierPOTab_ActionsLink}
  Click Element  ${Loc_Home-Sourcing-SupplierPO-SupplierPOTab_ActionsLink}
  DOMready-PopUpAppears
  Assert the Element in popup dialog  Supplier PO
  Assert the Element in popup dialog  Supplier
  Assert the Element in popup dialog  Factory
  Assert the Element in popup dialog  Color Based Ordering
  ${Element} =  Get Required Element  ${Loc_SupplierPOTab_NewSupplierWindow_SupplierPOName}  -1
  Input Text  ${Element}  ${SupplierPO1}
  Click on the DDL button2  field-PurchasedOrder-Form-POSupplier
  DOMreadyByWaitTime  2
  Select Value From the DDL  ${Supplier1} 
  ${Check} =  Get Element Attribute  ${Loc_SupplierPOTab_NewSupplierWindow_ColorBasedOrderingCheckbox}  aria-checked
  Run Keyword If  str('${Check}') == str('true')  Click Element  ${Loc_SupplierPOTab_NewSupplierWindow_ColorBasedOrderingCheckbox}
  Click On Save Button
  DOMready-PopUpCloses 
    
Verify data Creation for PO Group Tab

  Click Element  ${Loc_Sourcing_POGroupTab}
  DOMready-TabLoads 
           
  Mouse Over  ${Loc_POGroup_ActionsButton} 
  Click Element  ${Loc_POGroup_ActionsButton}
  DOMreadyByWaitTime  4  
  Click Element  ${Loc_Sourcing_POGroupTab}
  DOMreadyByWaitTime  4
  Execute Javascript    window.document.evaluate("//a[text()='(Unnamed)']/parent::td", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();
  DOMreadyByWaitTime  4 
   Click Element  //div/textarea
   DOMreadyByWaitTime  1
   Input Text  //div/textarea  ${POGroup1} 
  Click Element  ${Loc_Sourcing_POGroupTab}
  DOMready-TabLoads 
    
Verify data Creation for SelectSets Tab

  Click Element  ${Loc_Sourcing_SupplierPO_SelectSetTab}
  DOMready-TabLoads 
  Mouse Over   ${Loc_SourcingTab_SupplierPOTab_SelectSetTab_Actions} 
  Click Element   ${Loc_SourcingTab_SupplierPOTab_SelectSetTab_Actions}
  DOMready-PopUpAppears
  Input Text  css=[data-csi-automation='field-SelectSet-Form-Node Name'] .dijitInputInner  ${SelectSet1} 
  Click On Save Button
  DOMready-PopUpCloses
  
Customer PO Tab  
  Click Element  ${Loc_Home-Sourcing-CustomerPOTab}
  DOMready-TabLoads
  Element Should be Visible  ${Loc_Home-Sourcing-CustomerPO-CustomerPOTab} 
  Element Should be Visible  ${Loc_Home-Sourcing-CustomerPO-CustomerOrdersTab}
Create the Customer PO
  Mouse Over  ${Loc_Home-Sourcing-CustomerPO-CustomerPOTab_Actions}
  Click Element  ${Loc_Home-Sourcing-CustomerPO-CustomerPOTab_Actions}
  #${Element} =  Get Required Element   ${Loc_Home-Sourcing-CustomerPO-CustomerPOTab_Actions_NewCustomerPOLink}  -1
  #Wait Until Element Is Visible  ${Element}
  #Click Element  ${Element}
  DOMready-PopUpAppears
  Element Should Be Visible  //th[text()='Customer PO']
  Element Should Be Visible  //th[text()='Customer']
  Element Should Be Visible  //th[text()='Color Base Ordering']
  Selenium2Library.Input Text  ${Loc_NewCustomerPO_CustomerPOTextField}  ${CustomerPO1}
  DOMreadyByWaitTime  2
  Click on the DDL button  field-CustomerPurchaseOrder-Form-POCustomer
  DOMreadyByWaitTime  2
  Select Value From the DDL  Puma
  Click on Save Button
  DOMready-PopUpCloses
  Element Should Be Visible  //a[text()='${CustomerPO1}']
Shipment Tab
  Click Element    ${Loc_Home-Sourcing-ShipmentTab}
  DOMready-TabLoads
 
Create A Shipment
  Mouse Over    ${Loc_Home-Sourcing-ShipmentTab_Actions}
  Click Element   ${Loc_Home-Sourcing-ShipmentTab_Actions}
  DOMreadyByWaitTime  4
  Click Element    ${Loc_Home-Sourcing-ShipmentTab}
  DOMready-TabLoads
  Execute Javascript    window.document.evaluate("//a[text()='(Unnamed)']/parent::td", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();
  Click Element  //div/textarea
  Input Text  //div/textarea  ${Shipment1}
  Click Element    ${Loc_Home-Sourcing-ShipmentTab}
  DOMready-TabLoads
  Assert NodeName   ${Shipment1}
Factory Tab
    Click on Home Icon
    DOMready-TabLoads
    Click Element  ${Loc_Home_SourcingTab}
    DOMready-TabLoads
    Click Element  ${Loc_Home-Sourcing-Factory}
    DOMready-TabLoads
Create New Factory
   Mouse Over  ${Loc_ActionsFactory}
   Click Element  ${Loc_ActionsFactory}
   DOMreadyByWaitTime  4
   Click Element  ${Loc_Home-Sourcing-Factory}
   DOMreadyByWaitTime  5
   Execute Javascript    window.document.evaluate("//a[text()='(Unnamed)']/parent::td", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();
   Click Element  //div/textarea
   DOMreadyByWaitTime  1
   Input Text  //div/textarea  ${Factory1}
   Click Element  ${Loc_Home-Sourcing-Factory}
   DOMreadyByWaitTime  4
   Assert NodeName   ${Factory1}
   PO_SourcingPage.Update Suppliers For Required Factory	${Factory1}	 ${Supplier1}
   Wait.DomreadyByWaitTime	1
   Click Element  ${Loc_Home-Sourcing-Factory}
   DOMready-TabLoads 
   
Create Sales Division
        Click on Home Icon
    DOMready-TabLoads
    Click Element  ${Loc_Sourcing_SalesDivisionsTab}
    DOMready-TabLoads 
    Wait Until Page Contains Element   ${Loc_Release}
    Wait Until Page Contains Element  ${Loc_SalesDivisionActionLink}
    Mouse Over  ${Loc_SalesDivisionActionLink}
    Click Element  ${Loc_SalesDivisionActionLink}
    DOMready-PopUpAppears
    Selenium2Library.Input Text    ${Loc_SDPopup_NodeName}    ${SalesDivision1}
    Click Element  ${Loc_SaveButton}
    DOMready-PopUpCloses
Sourcing Reviews tab  
   Click on Home Icon
    Wait.DOMready
    DOMready-TabLoads
    Click Element  ${Loc_Home_SourcingTab}
    DOMready-TabLoads  
    Click Element    ${Loc_Sourcing_ReviewsTab}
    DOMready-TabLoads
   
Factory reviews Tab
    Click Element    ${Loc_Sourcing_Reviews_SupplierReviewTab}
    DOMready-TabLoads 
Supplier Review Tab
    Click Element  ${Loc_Sourcing_Reviews_FactoryReviewTab}
    DOMready-TabLoads    
     
Setup Tab
    #Navigate to Supplier tab 
    PO_HomePage.Click on Sourcing tab
    DOMready-TabLoads
    PO_SourcingPage.Click on Setup tab
    DOMready-TabLoads
    
Create Capability

    Click Element  ${Loc_Home-Sourcing-Setup-Capability}
    DOMready-TabLoads	   
    PO_SetupPage.Wait for the required New Creation options on mouse over-ActionsDDL	${Loc_NewCapability_Link}   
    PO_SetupPage.Click on required option after mouse over-ActionsDDL	${Loc_NewCapability_Link}
    DOMready-TableCellEditableTextArea-Appears
    Input Text  ${Loc_TextAreaCapability}  ${Capability1}
    Click Element  ${Loc_Home-Sourcing-Setup-Capability}
    DOMready-TabLoads	
    
Create Operation group

    Click Element  ${Loc_Home-Sourcing-Setup-OperationGroupTab}
    DOMready-TabLoads	   
    PO_SetupPage.Wait for the required New Creation options on mouse over-ActionsDDL	${Loc_NewOperationGroup}   
    PO_SetupPage.Click on required option after mouse over-ActionsDDL	${Loc_NewOperationGroup}
    Wait.DOMreadyByWaitTime  4 	
    Input Text  ${Loc_Table_InputText}  ${OperationGroup1}
    Click Element  ${Loc_Home-Sourcing-Setup-OperationGroupTab}
    DOMready-TabLoads
Create Sub Routings

    Click Element  ${Loc_Home-Sourcing-Setup-Sub Routings}	
    Wait.DOMready-TabLoads
    Mouse Over  ${Loc_ActionsSubRoutingTab}
   	Click Element  ${Loc_ActionsSubRoutingTab}
   	PO_SetupPage.Verify Dialog Window  New Sub Routing
 	Input Text  ${Loc_TextFieldSubRoutingName}	${SubRouting1}
   	Click On Save Button 
    Wait.DOMreadyByWaitTime  4 	
Create Sales Region Specs

    Click Element  ${Loc_Home-Sourcing-Setup_SalesRegionTab} 
    DOMready-TabLoads
    Click Element  ${Loc_SourcingTab_SalesRegionTab_Actions}
    DOMready-PopUpAppears
    Input Text  ${Loc_TextFieldSalesRegionName}  ${SalesRegionSpecs1}
   	Click On Save Button 
    Wait.DOMreadyByWaitTime  4 
Create Countries

    Click Element  ${Loc_Home-Sourcing-Setup-CountryTab}
    DOMready-TabLoads
    PO_SetupPage.Wait for the required New Creation options on mouse over-ActionsDDL	${Loc_NewCountry}
    PO_SetupPage.Click on required option after mouse over-ActionsDDL	${Loc_NewCountry}
    Wait.DOMreadyByWaitTime  3
    Input Text  ${Loc_Table_InputText}  ${Country1}
    Click Element  ${Loc_Home-Sourcing-Setup-CountryTab}
    DOMready-TabLoads
Create Language

    Click Element  ${Loc_Home-Sourcing-Setup_LanguageTab}
    DOMready-TabLoads
    Click Element  ${Loc_SourcingTab_LanguageTab_Actions}
    DOMready-PopUpAppears
    Input Text  ${Loc_TextFieldLanguageName}  ${Language1}
   	Click On Save Button 
    Wait.DOMreadyByWaitTime  4 
Create HTS Code

    Click Element 	${Loc_Home-Sourcing-Setup-HTSCodeTab}
    DOMready-TabLoads
    Click Element  ${Loc_ActionsSourcingHTSCodeTab}
    Wait.DOMreadyByWaitTime  2 
    #Input Text  ${Loc_TextAreaHTSCode}  ${HTSCode1} 
    Input Text  //div/textarea  ${HTSCode1} 
    Wait.DOMreadyByWaitTime  2
    Click Element 	${Loc_Home-Sourcing-Setup-HTSCodeTab}
    DOMready-TabLoads
Create Shipping Container

    Click Element  ${Loc_Home-Sourcing-Setup-Shipping Container}
    DOMready-TabLoads
    Mouse Over  ${Action_ShippingContainer}
    Click Element   ${Action_ShippingContainer}
    Wait.DOMreadyByWaitTime  2
    #PO_SourcingPage.Enter the Shipping Container value in Text Box	${ShippingContainer1}
    Input Text  //div/textarea  ${ShippingContainer1}
    Wait.DOMreadyByWaitTime  2
    Click Element  ${Loc_Home-Sourcing-Setup-Shipping Container}
    DOMready-TabLoads    
Create Shipping Port

    Click Element  ${Loc_Home-Sourcing-Setup-Shipping Port}	
    DOMready-TabLoads
    Mouse Over   ${Action_ShippingPort}
    Click Element  ${Action_ShippingPort} 
    Wait.DOMreadyByWaitTime  4
    PO_SourcingPage.Enter the Shipping Port value in Text Box	${ShippingPort1}
    Wait.DOMreadyByWaitTime  2
    Click Element  ${Loc_Home-Sourcing-Setup-Shipping Port}	
    Wait.DOMreadyByWaitTime  2
Create Shipping Rate

    Click Element  ${Loc_Home-Sourcing-Setup-Shipping Rate} 
    DOMready-TabLoads
    Mouse Over  ${Action_ShippingRate}
    Click Element  ${Action_ShippingRate}
    PO_SetupPage.Verify Dialog Window  New Shipping Rate
    PO_SourcingPage.Enter the Shipping Rate value in Dialog	${ShippingRate1}
    
   	DOMreadyByWaitTime  2
   	PO_SourcingPage.Update Freight Rate value	1000
    PO_NewCreationPopUpPage.Click on Save Button 
    
 Create Templates
    Click on Home Icon
    Wait.DOMready
    DOMready-TabLoads
    Click Element  ${Loc_Home_SourcingTab}
    DOMready-TabLoads
    Click Element  ${Loc_Home-Sourcing-SetupTab}
    DOMready-TabLoads
    Click Element  ${Loc_Sourcing-Setup-Template}	
    Wait.DOMready-TabLoads
  # creating the Suppler Quote template    
    Click Element  ${Loc_NewSuplierQuoteTemplateLink}
   	PO_SetupPage.Verify Dialog Window  Quote Template
	Input text  ${Loc_TextFieldSupplierQuote}  ${SupplierQuoteTemplate1}
	Wait.DOMreadyByWaitTime  2
	PO_NewCreationPopUpPage.Click on Save Button
   	Wait.DOMready-PopUpCloses
   	Wait.DOMreadyByWaitTime  3               

  # creating the Supplier Request template    
    Click Element  ${Loc_NewSuplierRequestTemplateLink}
   	PO_SetupPage.Verify Dialog Window  New Supplier Request Template
	Input text  ${Loc_TextFieldSupplierRequestName}  ${SupplierRequestTemplate1}
	PO_NewCreationPopUpPage.Click on Save Button
   	Wait.DOMready-PopUpCloses
   	Wait.DOMreadyByWaitTime  3                 
        
   # creating the Routing template    
    Click Element   ${Loc_NewRouting_link}
    Wait.DOMreadyByWaitTime  3  
    Click Element   //div[@class='csi-view-title csi-view-title-SiteLibSupplierItem-RoutingTemplates']
    Wait.DOMreadyByWaitTime  3 
   	Execute Javascript    window.document.evaluate("//a[text()='(Unnamed)']/parent::td", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();
    Click Element  //div/textarea
    Input Text  //div/textarea  ${RoutingTemplate1}
    Wait.DOMreadyByWaitTime  2
	Click Element   //div[@class='csi-view-title csi-view-title-SiteLibSupplierItem-RoutingTemplates']
	Wait.DOMreadyByWaitTime  3  

Reviews Setup Tab	
    Click on Home Icon
    Wait.DOMready
    DOMready-TabLoads
    Click Element  ${Loc_Home_SourcingTab}
    DOMready-TabLoads
    Click Element  ${Loc_Home-Sourcing-SetupTab}
    DOMready-TabLoads
  Click Element  ${Loc_Home-Sourcing-Setup_ReviewSetupTab}
  DOMready-TabLoads
Reviews Templates Tab  
   Click Element  ${Loc_Home-Sourcing-Setup_ReviewTemplatesTab}
   DOMready-TabLoads
  Mouse Over    ${Loc_Home-Sourcing-Setup_ReviewSetupTab_Actions}
  Click Element   ${Loc_Home-Sourcing-Setup_ReviewSetupTab_Actions}
  DOMreadyByWaitTime  4
  Click Element    ${Loc_Home-Sourcing-Setup_ReviewTemplatesTab}
  DOMready-TabLoads
  Execute Javascript    window.document.evaluate("//a[text()='(Unnamed)']/parent::td", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();
  DOMreadyByWaitTime  2
  Click Element  //div/textarea
  DOMreadyByWaitTime  1
  Input Text  //div/textarea   BS_RT
  Click Element    ${Loc_Home-Sourcing-Setup_ReviewTemplatesTab}
  DOMready-TabLoads 

Question SubSectionstab  
      Click on Home Icon
    Wait.DOMready
    DOMready-TabLoads
    Click Element  ${Loc_Home_SourcingTab}
    DOMready-TabLoads
    Click Element  ${Loc_Home-Sourcing-SetupTab}
    DOMready-TabLoads
  Click Element  ${Loc_Home-Sourcing-Setup_ReviewSetupTab}
  DOMready-TabLoads  
  Click Element From List  ${Loc_Home-Sourcing-Setup_QuestionSubsectionsTab}  -1 
   DOMready-TabLoads
  Mouse Over    ${Loc_Home-Sourcing-Setup_QuestionSubsectionsTab_Actions} 
  Click Element   ${Loc_Home-Sourcing-Setup_QuestionSubsectionsTab_Actions}
  DOMreadyByWaitTime  2
   Click Element From List  ${Loc_Home-Sourcing-Setup_QuestionSubsectionsTab}  -1 
   DOMready-TabLoads
  Execute Javascript    window.document.evaluate("//a[text()='(Unnamed)']/parent::td", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();
  DOMreadyByWaitTime  2
  Click Element  //div/textarea
  DOMreadyByWaitTime  1
   Input Text  //div/textarea   BS_QSubSection
  Click Element From List  ${Loc_Home-Sourcing-Setup_QuestionSubsectionsTab}  -1 
  DOMready-TabLoads
Questions Tab
      Click Element  ${Loc_Home-Sourcing-Setup_QuestionsTab}
  DOMready-TabLoads  


  [Teardown]    Close Browser
*** Keywords ***
DataSetup Create Sourcing Data

  ${This_DataProvider} =  Data_Provider.DataProvider     Create Sourcing Data
  Set Suite Variable  ${This_DataProvider}
  Log  ${This_DataProvider}
  
  ${POGroupCollection} =  GetDataProviderColumnValue  Data_POGroupName
  ${POGroup1} =  DataProviderSplitterForMultipleValues  ${POGroupCollection}  1 
  Set Suite Variable  ${POGroup1}
  
  #${SupplierCollection} =  Data_Provider.GetDataProviderColumnValue  Data_SupplierName
  #${Supplier1} =  DataProviderSplitterForMultipleValues  ${SupplierCollection}  1
  #Set Suite Variable  ${Supplier1}  
  
  ${SupplierPOCollection} =  GetDataProviderColumnValue  Data_SupplierPOName
  ${SupplierPO1} =  DataProviderSplitterForMultipleValues  ${SupplierPOCollection}  1 
  Set Suite Variable  ${SupplierPO1}
  
  ${FactoryCollection} =  GetDataProviderColumnValue  Data_FactoryName
  ${Factory1} =  DataProviderSplitterForMultipleValues  ${FactoryCollection}  1 
  Set Suite Variable  ${Factory1} 
   
  ${SalesDivisionCollection} =  GetDataProviderColumnValue  Data_SalesDivision
  ${SalesDivision1} =  DataProviderSplitterForMultipleValues  ${SalesDivisionCollection}  1 
  Set Suite Variable  ${SalesDivision1} 
  
  ${SelectSetCollection} =  GetDataProviderColumnValue  Data_SelectSet
  ${SelectSet1} =  DataProviderSplitterForMultipleValues  ${SelectSetCollection}  1 
  Set Suite Variable  ${SelectSet1}
  
   ${ShipmentCollection} =  Data_Provider.GetDataProviderColumnValue  Data_ShipmentName
  ${Shipment1} =  DataProviderSplitterForMultipleValues  ${ShipmentCollection}  1
  Set Suite Variable  ${Shipment1}
  
   ${CustomerPOCollection} =  Data_Provider.GetDataProviderColumnValue  Data_CustomerPOName
  ${CustomerPO1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${CustomerPOCollection}  1
  Set Suite Variable  ${CustomerPO1}
  
     ${CapabilityCollection} =  Data_Provider.GetDataProviderColumnValue  Data_Capability
  ${Capability1} =  DataProviderSplitterForMultipleValues  ${CapabilityCollection}  1
  Set Suite Variable  ${Capability1}
  
  ${OperationGroupCollection} =  Data_Provider.GetDataProviderColumnValue  Data_OperationGroupName
  ${OperationGroup1} =  DataProviderSplitterForMultipleValues  ${OperationGroupCollection}  1
  Set Suite Variable  ${OperationGroup1}
  
  ${SubRoutingsCollection} =  Data_Provider.GetDataProviderColumnValue  Data_SubRouting
  ${SubRouting1} =  DataProviderSplitterForMultipleValues  ${SubRoutingsCollection}  1
  Set Suite Variable  ${SubRouting1}
  
     ${SalesRegionSpecsCollection} =  Data_Provider.GetDataProviderColumnValue  Data_SalesRegionSpecsName
  ${SalesRegionSpecs1} =  DataProviderSplitterForMultipleValues  ${SalesRegionSpecsCollection}  1
  Set Suite Variable  ${SalesRegionSpecs1}
  
     ${CountriesCollection} =  Data_Provider.GetDataProviderColumnValue  Data_CountryName
  ${Country1} =  DataProviderSplitterForMultipleValues  ${CountriesCollection}  1
  Set Suite Variable  ${Country1}
  
  
  ${HTSCodeCollection} =  Data_Provider.GetDataProviderColumnValue  Data_HTSCodeName
  ${HTSCode1} =  DataProviderSplitterForMultipleValues  ${ShipmentCollection}  1
  Set Suite Variable  ${HTSCode1}
  
  ${ShippingContainerCollection} =  Data_Provider.GetDataProviderColumnValue  Data_ShippingContainerName
  ${ShippingContainer1} =  DataProviderSplitterForMultipleValues  ${ShippingContainerCollection}  1
  Set Suite Variable  ${ShippingContainer1}
  
  ${ShippingPortCollection} =  Data_Provider.GetDataProviderColumnValue  Data_ShippingPortName
  ${ShippingPort1} =  DataProviderSplitterForMultipleValues  ${ShippingPortCollection}  1
  Set Suite Variable  ${ShippingPort1}

  ${ShippingRateCollection} =  Data_Provider.GetDataProviderColumnValue  Data_ShippingRateName
  ${ShippingRate1} =  DataProviderSplitterForMultipleValues  ${ShipmentCollection}  1
  Set Suite Variable  ${ShippingRate1}
  
  ${SupplierQuoteCollection} =  Data_Provider.GetDataProviderColumnValue  Data_SupplierQuoteTemplate
  ${SupplierQuoteTemplate1} =  DataProviderSplitterForMultipleValues  ${SupplierQuoteCollection}  1
  Set Suite Variable  ${SupplierQuoteTemplate1}
  
  ${SupplierRequestCollection} =  Data_Provider.GetDataProviderColumnValue  Data_SupplierRequestTemplate
  ${SupplierRequestTemplate1} =  DataProviderSplitterForMultipleValues  ${SupplierRequestCollection}  1
  Set Suite Variable  ${SupplierRequestTemplate1}
  
  ${RoutingCollection} =  Data_Provider.GetDataProviderColumnValue  Data_RoutingTemplate
  ${RoutingTemplate1} =  DataProviderSplitterForMultipleValues  ${RoutingCollection}  1
  Set Suite Variable  ${RoutingTemplate1}

**

               
       
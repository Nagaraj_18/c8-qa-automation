*** Settings ***
Library           SikuliLibrary     timeout=120.0
Resource		  ../UtilityHelper/SuiteSetUp.txt
Resource          ../Locators/Loc_QualityPage.txt
Resource          ../Locators/Loc_Theme.txt
Resource          ../PageObjects/PO_SetupPage.txt


***Variables***
${DocumentsAndComments_Actlink}		css=[data-csi-automation='plugin-DocumentContainer-Documents-ToolbarNewActions']
${NewDocument}		css=[data-csi-act='NewDocument']
${DocumentsAndCommentsTab}		css=[data-csi-tab='DocumentContainer-DocumentsAndComments']
${DocumentName_Textarea}  css=[data-csi-automation='edit-DocumentContainer-Documents-Node Name:']
${Loc_BrowseImageDlg_ImageDirectoryOption}  //table/tr/td[contains(@class, dgrid-column-0) and text()='Image Directory']/ancestor::div[1]

*** Keywords ***
Set Image Path
   Add Image Path  ${Image_Dir}  
      
Create New Document 
  	[Arguments]  ${DocumentName}
  	Mouse Over   ${DocumentsAndComments_Actlink}
  	Click Element  ${DocumentsAndComments_Actlink}
  	#Click Element   ${NewDocument}
  	Wait Until Element Is Visible  ${DocumentName_Textarea}  
  	Selenium2Library.Input Text   ${DocumentName_Textarea}  ${DocumentName}
  	Click Element    ${DocumentsAndCommentsTab}
  	DOMreadyByWaitTime  2 
	#Click On Docs&Comments tab 

Click on Browse Action in the Table 
  [Arguments]  ${NodeName}  ${HeadingName}
  Click Element  //td[@data-csi-act='Node Name::0']/a[text()='${NodeName}']/following::td[@data-csi-heading='${HeadingName}']
  Wait Until Element Is Visible  //a[text()='Browse Files']  
  Mouse Over  //a[text()='Browse Files']
  Click Element  //a[text()='Browse Files']

Cancel Action to Close the System Dialog 
  Set Image Path
  ${CancelAction}=  Run Keyword And Return Status  SikuliLibrary.Click   CANCEL.png
  Run Keyword If  '${CancelAction}'=='False'  Press Special Key   ESC
  
Upload Image From Toolbar View Icon 
# Organize the Folders as List View 
  [Arguments]  ${AutoId}  ${DocumentImage}
  Set Image Path
  Wait Until Element Is Visible    css=[data-csi-act='BrowseImage']
  Click Element   css=[data-csi-act='BrowseImage']
  Wait Until Element Is Visible   css=[data-csi-act='Upload']
  Upload Image  ${AutoId}    ${DocumentImage}
  
Upload Image
# Organize the Folders as List View 
  [Arguments]  ${AutoId}  ${DocumentImage}
  Set Image Path
  ${Window1Title}=    Get Title
  Log  ${Window1Title}
  Execute Javascript 	window.focus()
  Wait Until Element Is Visible  //td[contains(@class, dgrid-column-0) and text()='Image Directory']
  Click Element   //td[contains(@class, dgrid-column-0) and text()='Image Directory']
  DOMreadyByWaitTime  5
  
  ${IdString} =  Get Element Attribute  ${Loc_BrowseImageDlg_ImageDirectoryOption}  id
  ${IDStringAfterSplit} =  Split String  ${IdString}  red
  ${CUrl} =  Get From List  ${IDStringAfterSplit}  -1
  
  
  #Click Element  //div[@id='dgrid_2-row-centric://fu/${CUrl}/${DocumentImage}'] 
  Click Element  css=[data-csi-url='centric://fu/${CUrl}/${DocumentImage}']
  ${ReqSave}=  Required Element From List  //*[@class='dijitReset dijitInline dijitButtonText' and text()='Save']  -1
  Wait Until Element Is Visible   ${ReqSave}
  Set Focus To Element  ${ReqSave}
  Click Element   ${ReqSave}
  
Upload Image Sikuli
# Organize the Folders as List View 
  [Arguments]  ${AutoId}  ${DocumentImage}
  Set Image Path
  ${Window1Title}=    Get Title
  Log  ${Window1Title}
  Execute Javascript 	window.focus()
  Wait Until Element Is Visible   css=span[data-csi-automation='${AutoId}']
  Click Element   css=span[data-csi-automation='${AutoId}']
  DOMreadyByWaitTime  5
  ${UploadStatus} =  Run Keyword And Return Status   Upload Image  ${DocumentImage}
  Log  ${UploadStatus}
  Run Keyword If	'${UploadStatus}' == 'False'	Cancel Action to Close the System Dialog
  ${ReqSave}=  Required Element From List  //*[@class='dijitReset dijitInline dijitButtonText' and text()='Save']  -1
  Wait Until Element Is Visible   ${ReqSave}
  Click Element   ${ReqSave}
  
Upload SystemData to the Application 
  [Arguments]  ${DocumentImage}  
  Set Image Path
  SikuliLibrary.Wait Until Screen Contain    DESKTOP.png		timeout=30.0
  SikuliLibrary.Click   Desktop.png
  SikuliLibrary.Wait Until Screen Contain    PICTURE REPO.png		timeout=30.0
  SikuliLibrary.Click	PICTURE REPO.png
  SikuliLibrary.Double Click	PICTURE REPO.png
  SikuliLibrary.Wait Until Screen Contain    ${DocumentImage}	timeout=30.0
  SikuliLibrary.Click   ${DocumentImage}
  #samplepic.png
  SikuliLibrary.Wait Until Screen Contain    OPEN.png		timeout=30.0
  SikuliLibrary.Click  OPEN.png
  ${WindowTitle}=    Get Title
  Log  ${WindowTitle}
  DOMreadyByWaitTime  7
    	
Create New Comment 
  [Arguments]  ${Subject}  ${CommentText}
  Set Image Path
  Run Keyword And Return Status  Click Element  css=[data-csi-automation='plugin-CommentContainer-Comments-NewComment']
  Run Keyword And Return Status  Wait Until Page Contains Element  css=.dijitDialogTitleBar 
  Click Element  css=div.fr-wrapper
  Click Element From List  css=div.dijitInputField  -1
  Selenium2Library.Input Text  //div/input[@name='subject']  ${Subject}
  DOMreadyByWaitTime  5 
  Click Element  //div[@class='fr-element fr-view']
  DOMreadyByWaitTime  5
  #${InputStatus}=  Run Keyword And Return Status  SikuliLibrary.Input Text  COMMENT INPUT1.PNG  ${CommentText}
  #Run Keyword If  '${InputStatus}' == 'False'   SikuliLibrary.Input Text  COMMENT INPUT2.PNG  ${CommentText}
  Selenium2Library.Input Text  //div[@class='fr-element fr-view']/p  ${CommentText}
  Click Element From List  //span[@class='dijitReset dijitInline dijitButtonText' and text()='Save']  -1

Canvas Comment 
  [Arguments]  ${CommentText}
  #Set Image Path
  Run Keyword And Return Status  Wait Until Page Contains Element  css=.dijitDialogTitleBar 
  DOMreadyByWaitTime  5 
  Click Element  //div[@class='fr-element fr-view']
  SikuliLibrary.Input Text  COMMENT INPUT1.PNG  ${CommentText}
  Click Element From List  //span[@class='dijitReset dijitInline dijitButtonText' and text()='Save']  -1


Reply Comment 
  [Arguments]  ${ReplyText}
  Click Element  css=css=.galleryComment div.csiActions>a[data-csi-act='CommentReply']
  Wait Until Page Contains Element  css=.dijitDialogTitleBar 
  Click Element  //div[@class='fr-element fr-view']
  SikuliLibrary.Input Text  CommentInput.PNG  ${ReplyText}
  Click Element From List  //span[@class='dijitReset dijitInline dijitButtonText' and text()='Save']	-1


   
Create Canvas
  # Enter the Auto Id for the canvas 
  [Arguments]    ${AutoId}
  #Click Element  css=[data-csi-automation='${AutoId}'] .iconNew
  Click Element  css=[data-csi-automation='${AutoId}'] 
  DOMreadyByWaitTime  2
  Wait Until Page Contains Element   ${Loc_Release}
  Assert for the given Element  ${Loc_IndexFirstCanvasPane}

Add Comments In Canvas
  # Add the Text to be entered for the first Canvas page 
  [Arguments]  ${Text}
  Click Element From List  ${Loc_IndexFirstCanvasPane}  -1
  DOMreadyByWaitTime  2
  Click Element  ${Loc_CanvasAddCommentLink}
  Wait Until Element is Visible  css=.dijitDialogTitle
  DOMreadyByWaitTime  10
  Canvas Comment  ${Text} 
 
Delete Comments In Canvas 
   # Delete the comments from the Canvas
  Click Element From List  ${Loc_IndexFirstCanvasPane}  -1
  DOMreadyByWaitTime  2
  ${Element} =  Required Element From List  ${Loc_DeleteCommentsCanvas}  -1
  Mouse Over  ${Element}
  Wait.DOMreadyByWaitTime  2
  Click Element  ${Element}
  Click Element from List  ${Loc_DeleteCommentsCanvas}  -1
  Wait.DOMreadyByWaitTime  2
  PO_SetupPage.Click Yes No Buttons  Delete
  Wait.DOMreadyByWaitTime  2
  Wait Until Element Is Visible  xpath=//span[text()='Item deleted successfully.']
  
Insert Data Sheet In Canvas
  # Enter Auto id for the first Canvas Page 
  [Arguments]    ${AutoId}
  Click Element From List  xpath=//span[@data-csi-automation='${AutoId}']  -1
  DOMreadyByWaitTime  5
  Wait Until Page Contains Element   ${Loc_Release}
  Assert for the given Element  ${Loc_IndexSecondCanvasPane}
 
Delete the canvas
  # Enter Auto Id for the Second Canvas Page 
  [Arguments]    ${AutoId}
  Click Element  ${Loc_IndexSecondCanvasPane}
  DOMreadyByWaitTime  2
  #Click Element  css=[data-csi-automation='${AutoId}'] .dijitIcon.csiActionIcon.iconDelete
  Click Element  css=[data-csi-automation='${AutoId}']
  Wait.DOMreadyByWaitTime  2
  PO_SetupPage.Click Yes No Buttons  Cancel
  Wait.DOMreadyByWaitTime  2
  Assert for the given Element  ${Loc_IndexSecondCanvasPane}  
  Click Element  ${Loc_IndexSecondCanvasPane}
  DOMreadyByWaitTime  2
  #Click Element  css=[data-csi-automation='${AutoId}'] .dijitIcon.csiActionIcon.iconDelete
  Click Element  css=[data-csi-automation='${AutoId}']
  Wait.DOMreadyByWaitTime  2
  PO_SetupPage.Click Yes No Buttons  Delete
  Wait.DOMreadyByWaitTime  2
  Wait Until Element Is Visible  xpath=//span[text()='Item deleted successfully.']
 
  
Copy The Canvas
  # enter Auto Id for the First Canvas page 
  [Arguments]    ${AutoId}
  Click Element  xpath=//span[@data-csi-automation='${AutoId}']
  DOMreadyByWaitTime  2
  Wait Until Page Contains Element   ${Loc_Release}
  Assert for the given Element  ${Loc_IndexSecondCanvasPane}      
  
Edit Canvas
  # To get the Data Sheet Name put the mouse over the tab 
  #should be able to get the title as 'Canvas - AUT_ThemeMaterial1 - CentricPLM' the Data Sheet Name will be <<AUT_ThemeMaterial1>> Need to pass this value as an argument to the keyword
  [Arguments]  ${DatasheetName}
  Click Element From List  ${Loc_IndexFirstCanvasPane}  -1
  ${ElementFromList} =  Required Element From List  ${Loc_Canvas}  -1
  Click Element  ${ElementFromList}
  Mouse Over   ${ElementFromList}
  DOMreadyByWaitTime  5
  Click Element From List  ${Loc_EditCanvas}  -1
  DOMreadyByWaitTime  5
  # use the JS (document.title) in the browser to get title and add it to the below keyword  
  #${NodeElement}=  Required Element From List  css=div.crumbSearch a  0
  #${NodeName}=  Get Text  ${NodeElement}
  #Select Window  Title=Canvas - ${NodeName}: ${DatasheetName} - Centric PLM
  Select Window  Title=Canvas - ${DatasheetName} - Centric PLM
  ${Window1Title}=    Get Title
  Log  CurrentTitle: ${Window1Title}  console=True
  #Wait Until Element Is Visible    css=span.svgIconImage    10s
  ${Element} =  Required Element From List  xpath=//span[@title='Insert Image']//span[text()='add_a_photo']  -1
  Mouse Over  ${Element}
  DOMreadyByWaitTime  2
  Click Element  ${Element}
  Upload Image  plugin-ImageBrowser-Upload   DOC1.PNG
  DOMreadyByWaitTime  5
  #Click Element From List  css=span.svgIconSave  -1
  Click Element From List  //span[text()='Save']  -1
  DOMreadyByWaitTime  5
  Click Element From List  //span[text()='Save']  0  
  Wait Until Page Contains Element  //span[text()='Canvas saved successfully.']
  Close Window
  
Create Local User	
	# Step 6: Create New User
	${LocalUser_Actions}  Set Variable  css=span[data-csi-automation='plugin-OnlineCrew-CrewUsers-ToolbarNewActions']  div.csi-toolbar-btn-icon-ToolbarNewActions
	${LocalUser_ActionNewUser}  Set Variable  css=tr[data-csi-act='NewUser'] td.dijitMenuItemLabel
	
	Click Element  css=[data-csi-tab='OnlineCrew-CrewUsers']
	DOMready-TabLoads
	# Select the Option 'All'  to put focus on the 'Actions' DDL
	Click Required Link Under Actions  ${LocalUser_Actions}  ${LocalUser_ActionNewUser}
	DOMready-PopUpAppears
	Clear Element Text  ${Loc_NewUser_UserLogin}
	Selenium2Library.Input Text  ${Loc_NewUser_UserLogin}  ${String_NewUser_UserLogin}	
	Wait.DOMreadyByWaitTime  1
	#PO_SetupPage.Enter the New User-Password	
	#Wait.DOMreadyByWaitTime  1
	#PO_SetupPage.Enter the New User-ConfirmPassword
	#Wait.DOMreadyByWaitTime  1
	PO_SetupPage.Click on Active CheckBox
	Wait.DOMreadyByWaitTime  1
	PO_SetupPage.Enter the New User-FirstName
	Wait.DOMreadyByWaitTime  1
	PO_SetupPage.Enter the New User-LastName
	Wait.DOMreadyByWaitTime  1
	${String_NewUser_UserLogin} =  GetDataProviderColumnValue  Data_NewUser-UserName
	Clear Element Text  ${DisplayName_TextBox}
	Selenium2Library.Input Text  ${DisplayName_TextBox}  ${String_NewUser_UserLogin}
	PO_SetupPage.Enter the New User-Email
	Wait.DOMreadyByWaitTime  1
	PO_SetupPage.Click on Save Button at New User PopUp
	DOMready-PopUpCloses  
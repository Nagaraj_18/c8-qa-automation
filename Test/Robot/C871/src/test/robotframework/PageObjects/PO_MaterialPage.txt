*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library           Selenium2Library
Library           String

Resource          ../Locators/Loc_SetupPage.txt
Resource		  ../UtilityHelper/SuiteSetUp.txt
Resource          ../Locators/Loc_MaterialPage.txt

*** Variables ***


*** Keywords ***

Wait for New Material Type option
    Wait Until Element Is Visible  ${Loc_New_Material_Type}

Click on New Material Type option
	Click Element  ${Loc_New_Material_Type}

Wait for New Enumerations option
    Wait Until Element Is Visible  ${Loc_New_Enumerations_Type}

Click on New Enumerations option
	Click Element  ${Loc_New_Enumerations_Type}

Wait for New Enumerations Value option
    Wait Until Element Is Visible  ${Loc_New_Enumerations_Value}

Click on New Enumerations Value option
	Click Element  ${Loc_New_Enumerations_Value}

Get Material type name string
	${Material_type_String} =  Generate Random String  3  [UPPER][NUMBERS]
    Log  ${Material_type_String}
    ${Material_type} =  Catenate  MType${Material_type_String}
    [return]  ${Material_type}

Enter the Material Type in the Pop up window
    [Arguments]    ${Material_type}
    Input Text    ${Loc_NewMaterialtype_TextBox}  ${Material_type}

Click on Material Save Button
	Click Element  ${Loc_Save_Button}

Select the Active Check box to configure the New Material
	[Arguments]    ${Materialtype}
	Execute Javascript var size=document.getElementsByClassName('pendingUpdate').length; for(i=0;i<size;i++){var text=document.getElementsByClassName('pendingUpdate')[i].textContent; if(text.contains('${Materialtype}')){document.getElementsByClassName('pendingUpdate')[i].children[1].children[0].children[0].click();}}
	DOMreadyJSWait

Select the Has Color Check box to configure the New Material
	[Arguments]    ${Materialtype}
	Execute Javascript var size=document.getElementsByClassName('pendingUpdate').length; for(i=0;i<size;i++){var text=document.getElementsByClassName('pendingUpdate')[i].textContent; if(text.contains('${Materialtype}')){document.getElementsByClassName('pendingUpdate')[i].children[3].children[0].children[0].click();}}
	DOMreadyJSWait

Select the Has Size Check box to configure the New Material
	[Arguments]    ${Materialtype}
	Execute Javascript var size=document.getElementsByClassName('pendingUpdate').length; for(i=0;i<size;i++){var text=document.getElementsByClassName('pendingUpdate')[i].textContent; if(text.contains('${Materialtype}')){document.getElementsByClassName('pendingUpdate')[i].children[4].children[0].children[0].click();}}
	DOMreadyJSWait

Select the Has Season Check box to configure the New Material
	[Arguments]    ${Materialtype}
	Execute Javascript var size=document.getElementsByClassName('pendingUpdate').length; for(i=0;i<size;i++){var text=document.getElementsByClassName('pendingUpdate')[i].textContent; if(text.contains('${Materialtype}')){document.getElementsByClassName('pendingUpdate')[i].children[6].children[0].children[0].click();}}
	DOMreadyJSWait

Select the Has Highest Quote for placement Check box to configure the New Material
	[Arguments]    ${Materialtype}
	Execute Javascript var size=document.getElementsByClassName('pendingUpdate').length; for(i=0;i<size;i++){var text=document.getElementsByClassName('pendingUpdate')[i].textContent; if(text.contains('${Materialtype}')){document.getElementsByClassName('pendingUpdate')[i].children[7].children[0].children[0].click();}}
	DOMreadyJSWait

Select the Material BOM Check box to configure the New Material
	[Arguments]    ${Materialtype}
	Execute Javascript var size=document.getElementsByClassName('pendingUpdate').length; for(i=0;i<size;i++){var text=document.getElementsByClassName('pendingUpdate')[i].textContent; if(text.contains('${Materialtype}')){document.getElementsByClassName('pendingUpdate')[i].children[10].children[0].children[0].click();}}
	DOMreadyJSWait

Select the Routing Check box to configure the New Material
	[Arguments]    ${Materialtype}
	Execute Javascript var size=document.getElementsByClassName('pendingUpdate').length; for(i=0;i<size;i++){var text=document.getElementsByClassName('pendingUpdate')[i].textContent; if(text.contains('${Materialtype}')){document.getElementsByClassName('pendingUpdate')[i].children[11].children[0].children[0].click();}}
	DOMreadyJSWait

Select the Spec Data Sheet Check box to configure the New Material
	[Arguments]    ${Materialtype}
	Execute Javascript var size=document.getElementsByClassName('pendingUpdate').length; for(i=0;i<size;i++){var text=document.getElementsByClassName('pendingUpdate')[i].textContent; if(text.contains('${Materialtype}')){document.getElementsByClassName('pendingUpdate')[i].children[12].children[0].children[0].click();}}
	DOMreadyJSWait

Select the Test Run Check box to configure the New Material
	[Arguments]    ${Materialtype}
	Execute Javascript var size=document.getElementsByClassName('pendingUpdate').length; for(i=0;i<size;i++){var text=document.getElementsByClassName('pendingUpdate')[i].textContent; if(text.contains('${Materialtype}')){document.getElementsByClassName('pendingUpdate')[i].children[13].children[0].children[0].click();}}
	DOMreadyJSWait

Select the Diameter Check box to configure the New Material
	[Arguments]    ${Materialtype}
	Execute Javascript var size=document.getElementsByClassName('pendingUpdate').length; for(i=0;i<size;i++){var text=document.getElementsByClassName('pendingUpdate')[i].textContent; if(text.contains('${Materialtype}')){document.getElementsByClassName('pendingUpdate')[i].children[14].children[0].children[0].click();}}
	DOMreadyJSWait

Select the Dimension Check box to configure the New Material
	[Arguments]    ${Materialtype}
	Execute Javascript var size=document.getElementsByClassName('pendingUpdate').length; for(i=0;i<size;i++){var text=document.getElementsByClassName('pendingUpdate')[i].textContent; if(text.contains('${Materialtype}')){document.getElementsByClassName('pendingUpdate')[i].children[15].children[0].children[0].click();}}
	DOMreadyJSWait

Select the Finish Check box to configure the New Material
	[Arguments]    ${Materialtype}
	Execute Javascript var size=document.getElementsByClassName('pendingUpdate').length; for(i=0;i<size;i++){var text=document.getElementsByClassName('pendingUpdate')[i].textContent; if(text.contains('${Materialtype}')){document.getElementsByClassName('pendingUpdate')[i].children[16].children[0].children[0].click();}}
	DOMreadyJSWait

Select the Lenght Check box to configure the New Material
	[Arguments]    ${Materialtype}
	Execute Javascript var size=document.getElementsByClassName('pendingUpdate').length; for(i=0;i<size;i++){var text=document.getElementsByClassName('pendingUpdate')[i].textContent; if(text.contains('${Materialtype}')){document.getElementsByClassName('pendingUpdate')[i].children[17].children[0].children[0].click();}}
	DOMreadyJSWait

Select the TextureEmbrosseRef Check box to configure the New Material
	[Arguments]    ${Materialtype}
	Execute Javascript var size=document.getElementsByClassName('pendingUpdate').length; for(i=0;i<size;i++){var text=document.getElementsByClassName('pendingUpdate')[i].textContent; if(text.contains('${Materialtype}')){document.getElementsByClassName('pendingUpdate')[i].children[18].children[0].children[0].click();}}
	DOMreadyJSWait

Select the Thickness Check box to configure the New Material
	[Arguments]    ${Materialtype}
	Execute Javascript var size=document.getElementsByClassName('pendingUpdate').length; for(i=0;i<size;i++){var text=document.getElementsByClassName('pendingUpdate')[i].textContent; if(text.contains('${Materialtype}')){document.getElementsByClassName('pendingUpdate')[i].children[19].children[0].children[0].click();}}
	DOMreadyJSWait

Select the Tooling Last Check box to configure the New Material
	[Arguments]    ${Materialtype}
	Execute Javascript var size=document.getElementsByClassName('pendingUpdate').length; for(i=0;i<size;i++){var text=document.getElementsByClassName('pendingUpdate')[i].textContent; if(text.contains('${Materialtype}')){document.getElementsByClassName('pendingUpdate')[i].children[20].children[0].children[0].click();}}
	DOMreadyJSWait

Select the Tooling Size Range Check box to configure the New Material
	[Arguments]    ${Materialtype}
	Execute Javascript var size=document.getElementsByClassName('pendingUpdate').length; for(i=0;i<size;i++){var text=document.getElementsByClassName('pendingUpdate')[i].textContent; if(text.contains('${Materialtype}')){document.getElementsByClassName('pendingUpdate')[i].children[21].children[0].children[0].click();}}
	DOMreadyJSWait

Select the Tooling Sizes Check box to configure the New Material
	[Arguments]    ${Materialtype}
	Execute Javascript var size=document.getElementsByClassName('pendingUpdate').length; for(i=0;i<size;i++){var text=document.getElementsByClassName('pendingUpdate')[i].textContent; if(text.contains('${Materialtype}')){document.getElementsByClassName('pendingUpdate')[i].children[22].children[0].children[0].click();}}
	DOMreadyJSWait

Select the Width Check box to configure the New Material
	[Arguments]    ${Materialtype}
	Execute Javascript var size=document.getElementsByClassName('pendingUpdate').length; for(i=0;i<size;i++){var text=document.getElementsByClassName('pendingUpdate')[i].textContent; if(text.contains('${Materialtype}')){document.getElementsByClassName('pendingUpdate')[i].children[23].children[0].children[0].click();}}
	DOMreadyJSWait

Select the Brand Check box to configure the New Material
	[Arguments]    ${Materialtype}
	Execute Javascript var size=document.getElementsByClassName('pendingUpdate').length; for(i=0;i<size;i++){var text=document.getElementsByClassName('pendingUpdate')[i].textContent; if(text.contains('${Materialtype}')){document.getElementsByClassName('pendingUpdate')[i].children[24].children[0].children[0].click();}}
	DOMreadyJSWait

Select the Features Check box to configure the New Material
	[Arguments]    ${Materialtype}
	Execute Javascript var size=document.getElementsByClassName('pendingUpdate').length; for(i=0;i<size;i++){var text=document.getElementsByClassName('pendingUpdate')[i].textContent; if(text.contains('${Materialtype}')){document.getElementsByClassName('pendingUpdate')[i].children[25].children[0].children[0].click();}}
	DOMreadyJSWait

Select the Functions Check box to configure the New Material
	[Arguments]    ${Materialtype}
	Execute Javascript var size=document.getElementsByClassName('pendingUpdate').length; for(i=0;i<size;i++){var text=document.getElementsByClassName('pendingUpdate')[i].textContent; if(text.contains('${Materialtype}')){document.getElementsByClassName('pendingUpdate')[i].children[26].children[0].children[0].click();}}
	DOMreadyJSWait

Click on Material Specification Tab
  Click Element  ${Loc_MaterialSpecificationTab} 

Click On Material Sourcing Tab 
  Click Element  ${Loc_Material-SourcingTab}
  
Click On MaterialMaterials Tab
  Click Element  ${Loc_Material_MaterialTab}

Click On SpecificationLibraries Tab
  Click Element  ${Loc_Specfication_LibrariesTab}

Click On MaterialSecurityGroup Tab
  Click Element  ${Loc_MaterialSecurityGroup}
  
Click On MaterialSamples Tab  
  Click Element  ${Loc_MaterialSamples}



*** Variables ***
${Loc_SBP_Placements_tab}  css=[data-csi-tab='ProductBOMRevision-PartMaterials']
${Loc_SBP_TDS_tab}  css=[data-csi-tab='ApparelBOMRevision-TDS']
${Loc_MultiLevelPlacements_tab}  css=[data-csi-tab='ProductBOMRevision-HierarchicalPartMaterials']
${Loc_SBP_Placemnts_Actions}  css=[data-csi-automation='plugin-ProductBOMRevision-PartMaterials-root'] 
${Loc_SBP_NewFromMaterial_Link}  css=[data-csi-automation='plugin-ProductBOMRevision-PartMaterials-root'] [data-csi-action='AggregateFromMaterial'] .dijitMenuItemLabel
${Loc_SBP_TDS}  css=[data-csi-tab='ApparelBOMRevision-TDS']
${Loc_BOMDimensionsTab}  css=[data-csi-tab='ProductBOMRevision-BOMDimensions']
${Loc_ActionsButtonStyleBOMPlacements}  css=[data-csi-automation='plugin-ProductBOMRevision-PartMaterials-Actions'] .dijitArrowButtonInner
${Loc_SectionsOptionStyleBOMPlacements}  css=[data-csi-automation='plugin-ProductBOMRevision-PartMaterials-Sections'] .dijitMenuItemLabel
${Loc_SelectSectionsOptionStyleBOMPlacements}  css=[data-csi-act="BomSectionMenuSelect"] .dijitMenuItemLabel















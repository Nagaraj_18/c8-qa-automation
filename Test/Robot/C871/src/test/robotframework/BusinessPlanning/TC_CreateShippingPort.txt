*** Settings ***
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt

Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_TablePage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_SourcingPage.txt

**Variables**
${ShippingPort}  ShipPort1
*** Test Cases ***
Create Shipping Port
	[tags]  Sanity_DataLoad
	#Data provider: It reads the data from the excel sheet for the test case specified. 
	${This_DataProvider} =  Data_Provider.DataProvider  Create Shipping Port
	Set Suite Variable  ${This_DataProvider}
	Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    ## Un Comment
    #Check-is the DOM ready
    #Reload
    Wait.DOMready
    DOMready-TabLoads
    Click on Home Icon
    Wait.DOMready
    DOMready-TabLoads  
# Step : Navigate to Home-Sourcing-Setup-Shipping Port
    Click Element  ${Loc_Home_SourcingTab}
    DOMready-TabLoads
    Click Element  ${Loc_Home-Sourcing-SetupTab}
    DOMready-TabLoads
    Click Element  ${Loc_Home-Sourcing-Setup-Shipping Port}	
    DOMready-TabLoads
    Mouse Over   ${Action_ShippingPort}
    Click Element  ${Action_ShippingPort}
    Wait.DOMreadyByWaitTime  4
    PO_SourcingPage.Enter the Shipping Port value in Text Box	${ShippingPort}
    Click Element  ${Loc_Home-Sourcing-Setup-Shipping Port}	
    PO_SourcingPage.Click on Country of Required Shipping Port	${ShippingPort}
    Wait.DOMreadyByWaitTime  2
    Click Element	//div[@data-csi-automation='edit-LibSourcing-ShippingPorts-Country:']/div[@class='dijitReset dijitMenuItem' and text()='India']
	DOMreadyByWaitTime  3
# Step : update Country value for 2nd Shipping Port 
 

  [Teardown]    Close Browser 
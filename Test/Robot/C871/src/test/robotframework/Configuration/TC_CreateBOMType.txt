*** Settings ***
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt

Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_SourcingPage.txt
Resource          ../PageObjects/PO_Calendar.txt

***Variable***
${Loc_ActionsMaterialBOMTypes}   css=[data-csi-automation='plugin-Site-MaterialBOMSubtypes-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
    
*** Test Cases ***
Home-00038
	[tags]  Sanity_DataLoad  CI_TEST
	#Data provider: It reads the data from the excel sheet for the test case specified. 
	${This_DataProvider} =  Data_Provider.DataProvider  Create BOM Type
	Set Test Variable  ${This_DataProvider}
	Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    #Wait Until Page Contains Element   ${Loc_Release}
    #Reload
    Wait.DOMready
    DOMready-TabLoads
    Click on Setup Icon
    DOMready-TabLoads
    #Assertion: Asserting for the 'Configuration' Tab to be Active
    #PO_SetupPage.Verify That This Tab Is Fronted    My Home
   
    # Step 3: Navigate to BOM Type tab	
    #Step Description 
    PO_SetupPage.Click On Configuration tab    
    DOMready-TabLoads
    PO_SetupPage.Click On Type Configuration tab
    DOMready-TabLoads
    PO_SetupPage.Click on Merchandise types tab
    DOMready-TabLoads 
    Wait.DOMready
	Wait For Release Info
    ${ScreenSizeISSmall}=  Run KeyWord And Return Status  Click On Right Arrow Button To View Hidden Tabs  0
    Log  HorizandalScrolbar:${ScreenSizeISSmall}  console=True  
    PO_SetupPage.Click on BOM Type tab
    # Step 4: Create BOM Type
    #Step Description     
    ${Status} =  Run Keyword And Return Status  Wait Until Page Contains Element  css=table[data-csi-automation='plugin-Site-ApparelBOMSubtypes-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
    Run Keyword If  '${Status}' == 'False'  PO_SetupPage.Click on BOM Type tab
	Run Keyword If  '${Status}' == 'False'  DOMready-TabLoads
	Polling Click Action  css=table[data-csi-automation='plugin-Site-ApparelBOMSubtypes-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions   
    Wait.DOMready-PopUpAppears
    
    ${BOMTypeNameCollection} =  GetDataProviderColumnValue  Data_BOMType
    ${BOMTypeName1} =  DataProviderSplitterForMultipleValues  ${BOMTypeNameCollection}   1 
    ${BOMTypeName2} =  DataProviderSplitterForMultipleValues  ${BOMTypeNameCollection}   2
	PO_NewCreationPopUpPage.Enter the Value in the Default Text box	 ${BOMTypeName1}
	Click on Save and New Button
#
	DOMreadyByWaitTime  3
	${Element} =  Required Element From List  css=input[name='Node Name']  -1
	Wait Until Element Is Visible  ${Element}
	Input Text  ${Element}  Trial ApparelBOMSubtype 001
	Click on Save and New Button
	DOMreadyByWaitTime  3
#
	${Element} =  Required Element From List  css=input[name='Node Name']  -1
	Wait Until Element Is Visible  ${Element}
	Input Text  ${Element}  ${BOMTypeName2}
	#PO_NewCreationPopUpPage.Enter the Value in the Default Text box	 ${BOMTypeName2}
	${Element} =  Required Element From List  //div/span[@data-csi-act='Save']/descendant::span[@role='button']  -1
	#PO_NewCreationPopUpPage.Click on Save Button
	Click Element  ${Element}
    DOMready-PopUpCloses
    PO_SetupPage.Click on BOM Type tab
    Click on Active CheckBox-TestType  ${BOMTypeName1}
    Click on Active CheckBox-TestType  ${BOMTypeName2}
    Click on Active CheckBox-TestType  Trial ApparelBOMSubtype 001
    DOMreadyByWaitTime  2
	PO_SetupPage.Click on BOM Type tab
	Click Element  //td[text()='${BOMTypeName1}']//following-sibling::td[@data-csi-heading='LockThemePlacementList:Child:SetupSettings:0']
	DOMreadyByWaitTime  2
	PO_SetupPage.Click on BOM Type tab
	DOMready-TabLoads
	Click Element  //td[text()='${BOMTypeName1}']//following-sibling::td[@data-csi-heading='ExposeBOMSupplierAttributes:Child:SetupSettings:0']
	DOMreadyByWaitTime  2
	Wait Until Page Contains Element  //div[contains(@class,'csiMessage')]//span[contains(@class,'csi-secondary-btn')]//span[text()='OK']
	Click Element  //div[contains(@class,'csiMessage')]//span[contains(@class,'csi-secondary-btn')]//span[text()='OK']
	DOMready-PopUpCloses
	PO_SetupPage.Click on BOM Type tab
	DOMready-TabLoads
	Click Element  //td[text()='${BOMTypeName1}']//following-sibling::td[@data-csi-heading='IsAutomaticPlacementTracking:Child:SetupSettings:0']
	DOMreadyByWaitTime  2
#
    DOMreadyByWaitTime  2
	PO_SetupPage.Click on BOM Type tab
	Click Element  //td[text()='Trial ApparelBOMSubtype 001']//following-sibling::td[@data-csi-heading='LockThemePlacementList:Child:SetupSettings:0']
	DOMreadyByWaitTime  2
	PO_SetupPage.Click on BOM Type tab
	Mouse Over  //td[text()='${BOMTypeName2}']//following-sibling::td[@data-csi-heading='Active:Child:SetupSettings:0']
	DOMreadyByWaitTime  2
	PO_SetupPage.Click on BOM Type tab
	Click Element  //td[text()='${BOMTypeName2}']//following-sibling::td[@data-csi-heading='ExposeBOMSupplierAttributes:Child:SetupSettings:0']
	DOMreadyByWaitTime  2
	Wait Until Page Contains Element  //div[contains(@class,'csiMessage')]//span[contains(@class,'csi-secondary-btn')]//span[text()='OK']
	Click Element  //div[contains(@class,'csiMessage')]//span[contains(@class,'csi-secondary-btn')]//span[text()='OK']
	DOMready-PopUpCloses
	PO_SetupPage.Click on BOM Type tab

Material BomType
   [Tags]  Sanity_DataLoad   CI_TEST
   PO_SetupPage.Click on BOM Type tab 
   DOMready-TabLoads
   Click Element  ${Loc_ActionsMaterialBOMTypes}
   #PO_SetupPage.Click Required Link Under Actions  ${Loc_ActionsMaterialBOMTypes}  ${Loc_ActionsMaterialBOMTypes}   
   Wait.DOMready-PopUpAppears
   PO_NewCreationPopUpPage.Enter the Value in the Default Text box	 Trial MaterialBOMSubtype 001
   Click on Save Button
   DOMready-PopUpCloses
   
   Wait Until Page Contains Element    //td[text()='Trial MaterialBOMSubtype 001']//following-sibling::td[@data-csi-heading='ExposeBOMSupplierAttributes:Child:SetupSettings:0']
   Click Element    //td[text()='Trial MaterialBOMSubtype 001']//following-sibling::td[@data-csi-heading='Active:Child:SetupSettings:0']
   DOMreadyByWaitTime  4
   Mouse Over  //td[text()='Trial MaterialBOMSubtype 001']//following-sibling::td[@data-csi-heading='ValidateBOM:Child:SetupSettings:0']
   Click Element  //div[@class="csi-view-title csi-view-title-Site-MaterialBOMSubtypes"]
   DOMreadyByWaitTime  4
   Click Element    //td[text()='Trial MaterialBOMSubtype 001']//following-sibling::td[@data-csi-heading='ExposeBOMSupplierAttributes:Child:SetupSettings:0']
   Wait Until Page Contains Element  //div[contains(@class,'csiMessage')]//span[contains(@class,'csi-secondary-btn')]//span[text()='OK']
   Click Element  //div[contains(@class,'csiMessage')]//span[contains(@class,'csi-secondary-btn')]//span[text()='OK']
   DOMready-PopUpCloses 	
    # Step 5: Close the Browser Instance.
    Test Step    ${R_Close_Browser}
    [Teardown]    Close Browser

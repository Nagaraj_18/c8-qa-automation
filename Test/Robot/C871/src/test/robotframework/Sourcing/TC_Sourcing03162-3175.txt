*** Settings ***
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Assertions.txt
Resource          ../UtilityHelper/Wait.txt
Resource    	  ../PageObjects/PO_MaterialTypePage.txt
Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_StylePage.txt
Resource          ../PageObjects/PO_TablePage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../PageObjects/PO_SpecificationPage.txt
Resource          ../PageObjects/PO_SourcingPage.txt
Resource          ../Locators/Loc_SourcingSupplierPage.txt
Resource          ../Locators/Loc_CollectionManagement.txt
Resource          ../Locators/Loc_StylePage.txt
Resource          ../Locators/Loc_Calendar.txt


*** Variables *** 
 ${MaterialName}  MaterialPO
 ${Style2}  POStyle1
 ${ColorSpec1}  blue6
 ${ColorSpec2}  red7
 ${Customer}  Myntra
 ${PO_SupplierQuote}  POSQT01
 ${PO_SupplierQuote2}  AUT_SupplierQuote1
 
*** Test Cases ***
Test Setup         
    Setup The Variables
Login-Home-Sourcing- Supplier PO
  Login TestCaseName		SupplierPO
  # Home tab-> Sourcing-> Customers
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads
  PO_HomePage.Click on Sourcing tab 
  DOMready-TabLoads
  Click Element  ${Loc_Home-Sourcing-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  ${Loc_Home-Sourcing-SupplierPO-SupplierPOTab}
  DOMready-TabLoads 
 
Sourcing-03162-Change the state of a Purchase Order from Issued to Closed
  #Home-Sourcing-Supplier PO-Supplier PO- Supplier PO
  #Change the state of a Purchase Order from Issued to Closed
  Click Element From List  //td//a[text()='${SupplierPO2}']//following::td[@data-csi-act='State::0']  0
  Wait Until Element is Visible  //div[text()='Close']
  Select Value From The DDL  Close
  Wait Until Element is Visible  ${Loc_Release} 
Sourcing-03163-Check whether header message, Properties attributes are not editable. Verify that creation links, Actions column link
  #Home-Sourcing-Supplier PO-Supplier PO- Supplier PO
  #Check whether header message, Properties attributes are not editable. Verify that creation links, Actions column link are not displayed. Check whether able to Orders attribute values are not editable. Verify that instructions cannot be modified
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads
  PO_HomePage.Click on Sourcing tab 
  DOMready-TabLoads
  Click Element  ${Loc_Home-Sourcing-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  ${Loc_Home-Sourcing-SupplierPO-SupplierPOTab}
  DOMready-TabLoads
  Click Element From List  //a[text()='${SupplierPO2}']  0
  DOMready-TabLoads
  Click Element  ${Loc_SupplierPONodeName_SupplierPOTab} 
  DOMready-TabLoads 
  Mouse Over   ${Loc_SupplierPONode_Header} 
  DOMreadyByWaitTime  3
  Element Should Not be Visible  css=[data-csi-act='ActionStringRich']
  Click Element  //span[@class="dijitReset dijitInline dijitIcon dijitNoIcon csi-toolbar-btn-icon-htmlToolbars"] 
  DOMreadyByWaitTime  6
  Execute Javascript    window.document.evaluate('//div[@class="csi-view-title csi-view-title-PurchasedOrder-Orders"]', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
  Element Should Not Be Visible  ${Loc_SupplierPONodeName_OrdersSection_ActionsButton}  
  Element Should Not be Visible  //span[@data-csi-act='Delete']
  Element Should Not be Visible  //span[@data-csi-act='Copy']
  Element Should Not be Visible  //span[@data-csi-act='Delete']
  #Element Should Not be Visible  //span[@data-csi-act='MoveUp']
  #Element Should Not be Visible  //span[@data-csi-act='MoveDown']  
  #To verify  the attribute values are not editable 
  ${MatchEleList} =  Get WebElements  ${Loc_OrderTable_DiscountValue}
  ${CountEle} =  Get Length  ${MatchEleList} 
  ${CountEle} =  Evaluate  ${CountEle} + 1
  Log  ${CountEle}
  :FOR  ${i}  IN RANGE  1  ${CountEle} 
  \  Element Should Not Be Visible  //tr[${i}]//td[@data-csi-act='DiscountValue::0' and contains(@class,'Editable')]
  # Intructions are not Editable 
Sourcing-03164-Check whether Properties attribute values are not editable except PO Group attribute
  #Home-Sourcing-Supplier PO-Supplier PO-Properties
  #Check whether Properties attribute values are not editable except PO Group attribute
  Element Should Not Be Visible  //td[@data-csi-heading='Node Name::0' and contains(@class,'Editable')]
  Element Should Not Be Visible  //td[@data-csi-heading='PaymentTerm:Child:PurchaseTerms:0' and contains(@class,'Editable')]
  Element Should Not Be Visible  //td[@data-csi-heading='Comments::0' and contains(@class,'Editable')]
  # Note the PO Group Attribute is also not Editable
  Element Should Not Be Visible  //td[@data-csi-heading='__Parent__::0' and contains(@class,'Editable')]
Sourcing-03165-Check whether creation links, Action column links are not displayed. Verify that unable to create, modify, rearrange, delete orders. 
  #Home-Sourcing-Supplier PO-Supplier PO-Orders-Orders
  #Check whether creation links, Action column links are not displayed. Verify that unable to create, modify, rearrange, delete orders. 
  Click Element  ${PO_Orders_Tab}  
  DOMready-TabLoads
  Click Element  ${Loc_SupplierPONodeName_OrdersTab_OrderPropertiesTab}  
  DOMready-TabLoads
  Element Should Not be Visible  ${SupplierPO_OrderTable_Actions}
  Element Should Not be Visible  //td[text()='${ColorSpec1}']//following::td//div[1]//span[@data-csi-act='Delete']
  #Element Should Not be Visible  //td[text()='${ColorSpec1}']//following::td//div//span[@data-csi-act='MoveUp']
  #Element Should Not be Visible  //td[text()='${ColorSpec1}']//following::td//div//span[@data-csi-act='MoveDown']
Sourcing-03166-Check whether it displays PO Products and PO Colors. Verify that PO Product PO Color values are read only.
  #Home-Sourcing-Supplier PO-Supplier PO-Orders-Summary
  #Check whether it displays PO Products and PO Colors. Verify that PO Product PO Color values are read only.
  Click Element  ${PO_Orders_Tab}  
  DOMready-TabLoads
  Click Element  ${Order_Summart_Tab}
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}
  Assert NodeName     ${ColorSpec1}
  Assert NodeName     ${ColoredMaterial1}
  ${MatchEleList} =  Get WebElements  //td[@data-csi-heading='Node Name:Child:Product:0']
  ${CountEle} =  Get Length  ${MatchEleList} 
  ${CountEle} =  Evaluate  ${CountEle} + 1
  Log  ${CountEle}
  :FOR  ${i}  IN RANGE  1  ${CountEle} 
  \  Element Should Not Be Visible  //tr[${i}]//td[@data-csi-heading='Node Name:Child:Product:0' and contains(@class,'Editable')]	
  ${MatchEleList} =  Get WebElements  //td[@data-csi-heading='Quantity::0']
  ${CountEle} =  Get Length  ${MatchEleList} 
  ${CountEle} =  Evaluate  ${CountEle} + 1
  Log  ${CountEle}
  :FOR  ${i}  IN RANGE  1  ${CountEle} 
  \  Element Should Not Be Visible  //tr[${i}]//td[@data-csi-heading='Quantity::0' and contains(@class,'Editable')]
  ${MatchEleList} =  Get WebElements  //td[@data-csi-heading='Extension::0']
  ${CountEle} =  Get Length  ${MatchEleList} 
  ${CountEle} =  Evaluate  ${CountEle} + 1
  Log  ${CountEle}
  :FOR  ${i}  IN RANGE  1  ${CountEle} 
  \  Element Should Not Be Visible  //tr[${i}]//td[@data-csi-heading='Extension::0' and contains(@class,'Editable')]
  ${MatchEleList} =  Get WebElements  //td[@data-csi-heading='Balance::0']
  ${CountEle} =  Get Length  ${MatchEleList} 
  ${CountEle} =  Evaluate  ${CountEle} + 1
  Log  ${CountEle}
  :FOR  ${i}  IN RANGE  1  ${CountEle} 
  \  Element Should Not Be Visible  //tr[${i}]//td[@data-csi-heading='Balance::0' and contains(@class,'Editable')]
  ${MatchEleList} =  Get WebElements  //td[@data-csi-heading='Node Name:Child:POColors/Child:ProductColor:0{POColors}']
  ${CountEle} =  Get Length  ${MatchEleList} 
  ${CountEle} =  Evaluate  ${CountEle} + 1
  Log  ${CountEle}
  :FOR  ${i}  IN RANGE  1  ${CountEle} 
  \  Element Should Not Be Visible  //tr[${i}]//td[@data-csi-heading='Node Name:Child:POColors/Child:ProductColor:0{POColors}' and contains(@class,'Editable')]
  ${MatchEleList} =  Get WebElements  //td[@data-csi-heading='Quantity:Child:POColors:0{POColors}']
  ${CountEle} =  Get Length  ${MatchEleList} 
  ${CountEle} =  Evaluate  ${CountEle} + 1
  Log  ${CountEle}
  :FOR  ${i}  IN RANGE  1  ${CountEle} 
  \  Element Should Not Be Visible  //tr[${i}]//td[@data-csi-heading='Quantity:Child:POColors:0{POColors}' and contains(@class,'Editable')]  
  ${MatchEleList} =  Get WebElements  //td[@data-csi-heading='FactoryOutBound:Child:POColors:0{POColors}']
  ${CountEle} =  Get Length  ${MatchEleList} 
  ${CountEle} =  Evaluate  ${CountEle} + 1
  Log  ${CountEle}
  :FOR  ${i}  IN RANGE  1  ${CountEle} 
  \  Element Should Not Be Visible  //tr[${i}]//td[@data-csi-heading='FactoryOutBound:Child:POColors:0{POColors}' and contains(@class,'Editable')]  
  ${MatchEleList} =  Get WebElements  //td[@data-csi-heading='Extension:Child:POColors:0{POColors}']
  ${CountEle} =  Get Length  ${MatchEleList} 
  ${CountEle} =  Evaluate  ${CountEle} + 1
  Log  ${CountEle}
  :FOR  ${i}  IN RANGE  1  ${CountEle} 
  \  Element Should Not Be Visible  //tr[${i}]//td[@data-csi-heading='Extension:Child:POColors:0{POColors}' and contains(@class,'Editable')] 
  ${MatchEleList} =  Get WebElements  //td[@data-csi-heading='Balance:Child:POColors:0{POColors}']
  ${CountEle} =  Get Length  ${MatchEleList} 
  ${CountEle} =  Evaluate  ${CountEle} + 1
  Log  ${CountEle}
  :FOR  ${i}  IN RANGE  1  ${CountEle} 
  \  Element Should Not Be Visible  //tr[${i}]//td[@data-csi-heading='Balance:Child:POColors:0{POColors}' and contains(@class,'Editable')]   
Sourcing-03167-Verify that it displays the Samples and check whether the attribute values are read only
  #Home-Sourcing-Supplier PO-Supplier PO-Orders-Samples
  #Verify that it displays the Samples and check whether the attribute values are read only
  Click Element   ${PurchasedOrder-Samples}
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}
  # No Data is been displayed 
 
Sourcing-03168-Check whether only freeze/unfreeze links are displayed for shipments . Check whether unable to create, modify, remove shipment. Verify that able to edit & delete orders. Check whether orders cannot be selected
  #Home-Sourcing-Supplier PO-Supplier PO-Orders-Shipment
  #Check whether only freeze/unfreeze links are displayed for shipments . Check whether unable to create, modify, remove shipment. Verify that able to edit & delete orders. Check whether orders cannot be selected
  Click Element  ${Loc_Home-Sourcing-SupplierPO-IssuesTab}
  DOMready-TabLoads
  ${MatchEleList} =  Get WebElements  //td[@data-csi-heading='Node Name:Child:__Parent__/Child:__Parent__:0']
  ${CountEle} =  Get Length  ${MatchEleList} 
  ${CountEle} =  Evaluate  ${CountEle} + 1
  Log  ${CountEle}
  :FOR  ${i}  IN RANGE  1  ${CountEle} 
  \  Element Should Not Be Visible  //tr[${i}]//td[@data-csi-heading='Node Name:Child:__Parent__/Child:__Parent__:0' and contains(@class,'Editable')]	
  ${MatchEleList} =  Get WebElements  //td[@data-csi-heading='Node Name::0']
  ${CountEle} =  Get Length  ${MatchEleList} 
  ${CountEle} =  Evaluate  ${CountEle} + 1
  Log  ${CountEle}
  :FOR  ${i}  IN RANGE  1  ${CountEle} 
  \  Element Should Not Be Visible  //tr[${i}]//td[@data-csi-heading='Node Name::0' and contains(@class,'Editable')]	
  ${MatchEleList} =  Get WebElements  //td[@data-csi-heading='Owner::0']
  ${CountEle} =  Get Length  ${MatchEleList} 
  ${CountEle} =  Evaluate  ${CountEle} + 1
  Log  ${CountEle}
  :FOR  ${i}  IN RANGE  1  ${CountEle} 
   \  Element Should Not Be Visible  //tr[${i}]//td[@data-csi-heading='Owner::0' and contains(@class,'Editable')]
  
Sourcing-03169-Verify that able to modify Shipment name and check whether able to modify LCL or Air Stuffing location section attribute values
  #Home-Sourcing-Supplier PO-Supplier PO-Orders-Stuffing location
  #Verify that able to modify Shipment name and check whether able to modify LCL or Air Stuffing location section attribute values
  Click Element  ${Loc_POGroup_SupplierPONode_StuffingLocationTab}
  DOMready-TabLoads
  ${Element} =  Get Required Element  //tr//td[@data-csi-heading='Node Name::0' and contains(@class,'Editable')]  0
  Element Should Be Visible  ${Element}
  Execute Javascript    window.document.evaluate('//*[@class="csi-view-title csi-view-title-ShipmentTerms-AlternativeStuffingLocation"]', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
  Click Element  ${Loc_AlternateStuffingLocation_Address}
  Selenium2Library.Input Text  ${Loc_AlternateStuffingLocation_Address_TextArea}  Address3
  Click Element   ${Loc_AlternateStuffingLocation_Section}
  Click Element  ${Loc_AlternateStuffingLocation_Address2}
  Selenium2Library.Input Text  ${Loc_AlternateStuffingLocation_Address2_TextArea}  Address4
  Click Element   ${Loc_AlternateStuffingLocation_Section}
  #Click Element  ${Loc_AlternateStuffingLocation_City}
  #Execute Javascript    window.document.evaluate("//td[@data-csi-act='City:Child:AlternativeStuffingLocation:0']", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();
  #Wait.DOMreadyByWaitTime  3
  #Selenium2Library.Input Text  ${Loc_AlternateStuffingLocation_City_TextArea}  Chicago
  #Click Element   ${Loc_AlternateStuffingLocation_Section} 

Sourcing-03170-Check whether it displays the Issues and verify that the attribute values are read only
  #Home-Sourcing-Supplier PO-Supplier PO-Orders-Issues
  #Check whether it displays the Issues and verify that the attribute values are read only
   Click Element  ${Loc_Home-Sourcing-SupplierPO-IssuesTab}
  DOMready-TabLoads
  ${MatchEleList} =  Get WebElements  //td[@data-csi-heading='Node Name:Child:__Parent__/Child:__Parent__:0']
  ${CountEle} =  Get Length  ${MatchEleList} 
  ${CountEle} =  Evaluate  ${CountEle} + 1
  Log  ${CountEle}
  :FOR  ${i}  IN RANGE  1  ${CountEle} 
  \  Element Should Not Be Visible  //tr[${i}]//td[@data-csi-heading='Node Name:Child:__Parent__/Child:__Parent__:0' and contains(@class,'Editable')]	
  ${MatchEleList} =  Get WebElements  //td[@data-csi-heading='Node Name::0']
  ${CountEle} =  Get Length  ${MatchEleList} 
  ${CountEle} =  Evaluate  ${CountEle} + 1
  Log  ${CountEle}
  :FOR  ${i}  IN RANGE  1  ${CountEle} 
  \  Element Should Not Be Visible  //tr[${i}]//td[@data-csi-heading='Node Name::0' and contains(@class,'Editable')]	
  ${MatchEleList} =  Get WebElements  //td[@data-csi-heading='Owner::0']
  ${CountEle} =  Get Length  ${MatchEleList} 
  ${CountEle} =  Evaluate  ${CountEle} + 1
  Log  ${CountEle}
  :FOR  ${i}  IN RANGE  1  ${CountEle} 
   \  Element Should Not Be Visible  //tr[${i}]//td[@data-csi-heading='Owner::0' and contains(@class,'Editable')]	
Sourcing-03171-Check whether able to modify the Quality Control attributes and verify that it is getting updated properly
  #Home-Sourcing-Supplier PO-Supplier PO-Orders-Quality Control
  #Check whether able to modify the Quality Control attributes and verify that it is getting updated properly
  Click Element  ${Loc_Home-Sourcing-SupplierPO-QualityControlTab}
  DOMready-TabLoads
  Mouse Over  ${SupplierPO-QualityControlTab_QCActions}
  Click Element  ${SupplierPO-QualityControlTab_QCActions}
  #Wait Until Element Is Visible	  ${SupplierPO-QualityControlTab_QCActions_NewQCIssueLink}
  #Assert for the given element    ${SupplierPO-QualityControlTab_QCActions_NewQCIssueLink}
  #Click Element	  ${SupplierPO-QualityControlTab_QCActions_NewQCIssueLink}
  Wait Until Element Is Visible  ${Loc_InputQAText}
  Selenium2Library.Input Text   ${Loc_InputQAText}  QCIssue4
  Wait.DOMreadyByWaitTime  3
  #Wait For the Entered Values in TextArea to GetUpdate  PurchasedOrder-QualityControl  ${Loc_InputQAText}
  Click Element  ${Loc_Home-Sourcing-SupplierPO-QualityControlTab}
  DOMready-TabLoads
 
Sourcing-03172-Check whether Documents cannot be created, modified or deleted. Verify that Comments cannot be created or deleted. Check whether able to modify the last added comment.
  #Home-Sourcing-Supplier PO-Supplier PO- Supplier POHome-Sourcing-Supplier PO-Supplier PO-Orders-Docs & Comments
  #Check whether Documents cannot be created, modified or deleted. Verify that Comments cannot be created or deleted. Check whether able to modify the last added comment.
  Click Element  ${Loc_Home-Sourcing-SupplierPO-Docs&CommentsTab} 
  DOMready-TabLoads
  Element Should Not be Visible  ${Loc_ActionsNewDocumentInMatProduct}
  Element Should Not be visible  ${Loc_NewCommentMarketingMat}
  
Sourcing-03173-Check whether actions column displays only Copy icon. Verify that only PO Group attribute value is editable. Verify that PO is displayed with Closed state.
  #Home-Sourcing-Supplier PO-Supplier PO
  #Check whether actions column displays only Copy icon. Verify that only PO Group attribute value is editable. Verify that PO is displayed with Closed state.
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads
  PO_HomePage.Click on Sourcing tab 
  DOMready-TabLoads
  Click Element  ${Loc_Home-Sourcing-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  ${Loc_Home-Sourcing-SupplierPO-SupplierPOTab}
  DOMready-TabLoads
  Element Should Be Visible  //a[text()='${SupplierPO2}']//following::td[text()='CLOSED']
  Element Should Not Be Visible  //a[text()='${SupplierPO2}']//following::div[1]//span[@data-csi-act='Edit']
  Element Should Not Be Visible  //a[text()='${SupplierPO2}']//following::div[1]//span[@data-csi-act='Delete']
  Element Should Be Visible  //a[text()='${SupplierPO2}']//following::div[1]//span[@data-csi-act='Copy']
  # PO Group attribute is not Editable 
 
Sourcing-03174-Check whether PO cannot be moved to any other state. 
  #Home-Sourcing-Supplier PO-Supplier PO- Supplier PO
  #Check whether PO cannot be moved to any other state. 
  Element Should Be Visible  //td//a[text()='${SupplierPO2}']//following::td[text()='CLOSED']
  Element Should Not Be Visible  //td//a[text()='${SupplierPO2}']//following::td[text()='Closed' and contains(@class,'Editabe')]
Sourcing-03175-Once the PO is in the Closed state, check if the arrow for moving the PO back to the Draft state is available
  #Home-Sourcing-Supplier PO-Supplier PO- Supplier PO
  #Once the PO is in the Closed state, check if the arrow for moving the PO back to the Draft state is available
  Click Element From List  //a[text()='${SupplierPO2}']  0
  DOMready-TabLoads
  Click Element  ${Loc_SupplierPONodeName_SupplierPOTab} 
  DOMready-TabLoads 
  Element Should be Visible  css=[data-csi-act='POClosedToDraft']
  
  SuiteSetUp.Exit Execution  
 **Keywords 
MouseOver Assert and Click
  [Arguments]   ${Loc_MouseOverActLink}    ${Loc_NewElementAftMouseOver}
  Mouse Over   ${Loc_MouseOverActLink}
  Wait Until Element Is Visible	  ${Loc_NewElementAftMouseOver}
  Assert for the given element   ${Loc_NewElementAftMouseOver}
  Click Element	  ${Loc_NewElementAftMouseOver}
  DOMready-PopUpAppears
Enter The Values on Textarea
    [Arguments]   ${Element}  ${InputData}
    DOMreadyByWaitTime  2
    # Press backspace button
    Press Key  ${Element}  \\08
    DOMreadyByWaitTime  2
    Press Key  ${Element}  ${InputData}  
Assert Table Text
  [Arguments]	${TableText}
   Element Should Be Visible  //span[contains(text(),'${TableText}')]  
Assert Popup Table Text
   [Arguments]	${TableText}
   Element Should Be Visible  //*[@class='attrString firstColumn' and contains(text(),'${TableText}')]
Setup The Variables
  ${This_DataProvider} =  Data_Provider.DataProvider  SupplierPO
  Set Suite Variable  ${This_DataProvider}
  Log  ${This_DataProvider}
  
  ${SupplierPOCollection} =  GetDataProviderColumnValue  Data_SupplierPOName
  ${SupplierPO1} =  DataProviderSplitterForMultipleValues  ${SupplierPOCollection}  1 
  ${SupplierPO2} =  DataProviderSplitterForMultipleValues  ${SupplierPOCollection}  2
  ${SupplierPO3} =  DataProviderSplitterForMultipleValues  ${SupplierPOCollection}  3
  Set Suite Variable  ${SupplierPO1}
  Set Suite Variable  ${SupplierPO2}
  Set Suite Variable  ${SupplierPO3}
  
  ${POGroupCollection} =  GetDataProviderColumnValue  Data_POGroupName
  ${POGroup1} =  DataProviderSplitterForMultipleValues  ${POGroupCollection}  1 
  ${POGroup2} =  DataProviderSplitterForMultipleValues  ${POGroupCollection}  2
  Set Suite Variable  ${POGroup1}
  Set Suite Variable  ${POGroup2}

  ${IssueNameCollection} =  GetDataProviderColumnValue  Data_IssueName  
  ${IssueName1} =  DataProviderSplitterForMultipleValues  ${IssueNameCollection}  1 
  ${IssueName2} =  DataProviderSplitterForMultipleValues  ${IssueNameCollection}  2
  Set Suite Variable  ${IssueName1}
  Set Suite Variable  ${IssueName2}  
  
    
  ${ColoredMaterialCollection} =  GetDataProviderColumnValue  Data_ColoredMaterialName
  ${ColoredMaterial1} =  DataProviderSplitterForMultipleValues  ${ColoredMaterialCollection}  1 
  ${ColoredMaterial2} =  DataProviderSplitterForMultipleValues  ${ColoredMaterialCollection}  2
  Set Suite Variable  ${ColoredMaterial1}
  Set Suite Variable  ${ColoredMaterial2}

 ***Comments

  
  
  
  

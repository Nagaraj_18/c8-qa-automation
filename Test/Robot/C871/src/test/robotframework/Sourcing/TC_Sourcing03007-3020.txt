*** Settings ***

Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Assertions.txt
Resource          ../UtilityHelper/Wait.txt
Resource    	  ../PageObjects/PO_MaterialTypePage.txt
Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../PageObjects/PO_DocumentsAndComments.txt
Resource          ../PageObjects/PO_SpecificationPage.txt
Resource          ../PageObjects/PO_SourcingPage.txt
Resource          ../Locators/Loc_SourcingSupplierPage.txt
Resource          ../Locators/Loc_CollectionManagement.txt

***Variable***

${Supplier1}  Laces
${StyleName}  StylePO
${ColorSpec1}  blue6
${ColorSpec2}  red7
${QualityControlIssue1}  QCIssue1
${Style2}  POStyle1
${Country}  Burma

*** Test Cases ***
Test Setup         
    Setup The Variables
Login-Home-Sourcing- Supplier PO
  Login TestCaseName		SupplierPO
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads
  PO_HomePage.Click on Sourcing tab 
  DOMready-TabLoads
  Click Element  ${Loc_Home-Sourcing-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  ${Loc_Home-Sourcing-SupplierPO-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  //a[@class='browse' and text()='${SupplierPO1}']
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}
  Click Element   ${PurchasedOrder-Shipment_Tab}
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}
  Assert NodeName     Shipment1
Sourcing-03007-Verify the tab displays the list of Shipment created/selected in POs-Shipments tab	
 #Home-Sourcing-Supplier PO-Supplier PO-Stuffing Location	
 #Verify the tab displays the list of Shipment created/selected in POs-Shipments tab
  Click Element  //a[text()='Shipment1']
  DOMready-TabLoads 
  Click Element   //*[@data-csi-tab='ShipmentTerms-Stuffing']
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}

Sourcing-03008-Select a Shipment, Verify that four sections Shipment, Container Stuffing Location, Factory Address and LCL or Air Stuffing Location are displayed	
#Home-Sourcing-Supplier PO-Supplier PO-Stuffing Location	
#Select a Shipment, Update the attributes and check if the changes made are displayed in the table. Verify that four sections Shipment, Container Stuffing Location, Factory Address and LCL or Air Stuffing Location are displayed
  Assert for the given element   css=[class='csi-view-title csi-view-title-ShipmentTerms-ShipmentStuffingProperty']
  Assert for the given element   css=[class='csi-view-title csi-view-title-ShipmentTerms-StuffingLocation']
  Assert for the given element   css=[class='csi-view-title csi-view-title-ShipmentTerms-FactoryLocation']
  Assert for the given element   css=[class='csi-view-title csi-view-title-ShipmentTerms-AlternativeStuffingLocation']

#Sourcing-03009-Verify that Container stuffing address information and factory information are automatically updated based on the based on the selected Factory Information	
#Home-Sourcing-Supplier PO-Supplier PO-Stuffing Location	
#Verify that Container stuffing address information and factory information are automatically updated based on the based on the selected Factory Information
  #Element Should Be Visible  //td[@data-csi-heading='Country:Child:PurchasedOrders/Index:0/Child:POFactoryContact:0']//a[@class='browse' and text()='${Country}'] 
  
Sourcing-03010-In the 'LCL or Air Stuffing Location' update attributes and check if the values are displayed correctly	
#Home-Sourcing-Supplier PO-Supplier PO-Stuffing Location	
#In the 'LCL or Air Stuffing Location' update attributes and check if the values are displayed correctly
  Execute Javascript  window.document.evaluate('//td[@data-csi-act="Country:Child:AlternativeStuffingLocation:0"]', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
  Click Element   //*[@class='csi-view-title csi-view-title-ShipmentTerms-AlternativeStuffingLocation']/following::td[@data-csi-act='Country:Child:AlternativeStuffingLocation:0']
  DOMreadyByWaitTime   3
  PO_SpecificationPage.Select Requrired value from DDL-StyleBOM   ${Country}
  Click Element   //*[@class='csi-view-title csi-view-title-ShipmentTerms-AlternativeStuffingLocation']
  Click Element   //*[@class='csi-view-title csi-view-title-ShipmentTerms-AlternativeStuffingLocation']/following::td[@data-csi-act='City:Child:AlternativeStuffingLocation:0']
  Wait Until Element Is Visible   //textarea[@data-csi-automation='edit-ShipmentTerms-AlternativeStuffingLocation-City:Child:AlternativeStuffingLocation']
  Selenium2Library.Input Text    //textarea[@data-csi-automation='edit-ShipmentTerms-AlternativeStuffingLocation-City:Child:AlternativeStuffingLocation']	Chennai
  Click Element   //*[@class='csi-view-title csi-view-title-ShipmentTerms-AlternativeStuffingLocation'] 
  ${VerifyValues3} =  Selenium2Library.Get Text   //*[@class='csi-view-title csi-view-title-ShipmentTerms-AlternativeStuffingLocation']/following::td[@data-csi-act='Country:Child:AlternativeStuffingLocation:0']
  Log  ${VerifyValues3}
  ${VerifyValues4} =  Selenium2Library.Get Text   //*[@class='csi-view-title csi-view-title-ShipmentTerms-AlternativeStuffingLocation']/following::td[@data-csi-act='City:Child:AlternativeStuffingLocation:0']
  Log  ${VerifyValues4}

Sourcing-03011	
   Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads
  PO_HomePage.Click on Sourcing tab 
  DOMready-TabLoads
  Click Element  ${Loc_Home-Sourcing-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  ${Loc_Home-Sourcing-SupplierPO-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  //a[@class='browse' and text()='${SupplierPO1}']
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}
  #Home-Sourcing-Supplier PO-Supplier PO-Issues	
  #Verify that Issues tab displays the list of Issues which belongs to Materials & Styles are displayed based on the selection of Material SKU orders and Style SKU orders 
  Click Element   ${Loc_Home-Sourcing-SupplierPO-IssuesTab} 
  DOMready-TabLoads 
  Assert for the Given Element  //a[@class='browse' and text()='${IssueName1}']
  Assert for the Given Element  //a[@class='browse' and text()='${IssueName2}']
  
#Sourcing-03012	
#Home-Sourcing-Supplier PO-Supplier PO-Issues	
#Verify that it displays correct values and check whether the attribute values are read only
    #### To check the attribute Vaues are not read only

Test Data To Create Supplier Quality Control Issues
  Click Element  ${PurchasedOrder-Shipment_Tab}
  DOMready-TabLoads 
  Mouse Over  ${SupplierPO_Shipment_Actions}
  Click Element  ${SupplierPO_Shipment_Actions}
  #Click Element  ${NewShipment}
  Wait Until Element Is Visible    ${TextArea_Shipment}
  Selenium2Library.Input Text   ${TextArea_Shipment}   ${Shipment2}
  Click Element   ${PurchasedOrder-Shipment_Tab}
  DOMready-TabLoads
  Click Element   //*[@class='browse' and text()='${Shipment2}']
  DOMready-TabLoads 
  Click Element  ${Loc_ShipmentNodeName_QualityControlTab}
  DOMready-TabLoads 
  Mouse Over  ${Loc_ShipmentNodeName_QualityControlTab_IssuesSection_ActionsButton} 
  Click Element  ${Loc_ShipmentNodeName_QualityControlTab_IssuesSection_ActionsButton}
  Wait.DOMreadyByWaitTime  2
  #Click Element  ${Loc_ShipmentNodeName_QualityControlTab_IssuesSection_NewQCIssue} 
  Wait.DOMreadyByWaitTime  4 
  Selenium2Library.InputText  ${Loc_ShipmentNodeName_QualityControlTab_IssuesSection_NewQCIssueTextArea}  ${QualityControlIssue1}
  Click Element  ${Loc_ShipmentNodeName_QualityControlTab}
  DOMready-TabLoads   
 
Sourcing-03013-Home-Sourcing-PO-PO-Quality Control
#Home-Sourcing-PO-PO-Quality Control
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads
  PO_HomePage.Click on Sourcing tab 
  DOMready-TabLoads
  Click Element  ${Loc_Home-Sourcing-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  ${Loc_Home-Sourcing-SupplierPO-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  //a[@class='browse' and text()='${SupplierPO1}']
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release} 	
  Click Element   ${Loc_Home-Sourcing-SupplierPO-QualityControlTab} 
  DOMready-TabLoads 
  Assert for the Given Element  //*[@class='attrString iconEditable firstColumn' and text()='${QualityControlIssue1}']
#Select a Shipment, edit the attributes and check whether it is getting updated properly
  Click Element From List  //a[@class='browse' and text()='${Shipment2}']  -1
  DOMready-TabLoads
  Click Element  ${Loc_ShipmentNodeName_ShipmentTab}
  DOMready-TabLoads 
  Calendar Date Selection Field  ExpectedShipDate::0
  Wait.DOMreadyByWaitTime  4
  ${ExpectedShipmentDateValue} =  Selenium2Library.Get Text  css=[data-csi-act='ExpectedShipDate::0']
  # To Verify the data is upadted for the Quality Issue 
  Click Element  ${Loc_ShipmentNodeName_QualityControlTab}
  DOMready-TabLoads
  Click Element  css=[data-csi-automation='plugin-ShipmentTerms-ShipmentTerm-refresh'] 
  Wait Until Page Contains Element   ${Loc_Release}
  ${Element} =  Get Required Element  //*[@class='attrNumber attrTime iconEditable' and text()='${ExpectedShipmentDateValue}']  -1
  Assert for the Given Element  ${Element}
  
Sourcing-03014-Home-Sourcing-Supplier PO-Supplier PO-Docs & Comment	
#Home-Sourcing-Supplier PO-Supplier PO-Docs & Comments	
  Click Element   ${Loc_Home-Sourcing-SupplierPO-Docs&CommentsTab} 
  DOMready-TabLoads 
  #Create a new document and add a file to the record. Also create a new comment and check if the records are displayed correctly. 
  Mouse Over  ${Loc_ActionsNewDocumentInMatProduct} td.dijitArrowButton
  Click Element  ${Loc_ActionsNewDocumentInMatProduct} td.dijitArrowButton
  Click Element From List  css=[data-csi-act="NewDocument"] td.dijitMenuItemLabel  -1
  Wait.DOMreadyByWaitTime  2   
  Selenium2Library.Input Text  ${Loc_DocumentMarketingMatTextArea}  AUT_Document1
  Wait.DOMreadyByWaitTime  5
  Click Element  ${Loc_Home-Sourcing-SupplierPO-Docs&CommentsTab} 
  Element should be Visible  xpath=//a[@class='browse' and text()='AUT_Document1']
  DOMreadyByWaitTime  2
  Click Element  //a[text()='AUT_Document1']//following::td//div[1]//span[@data-csi-act='Delete']
  Wait.DOMreadyByWaitTime  5
  PO_SetupPage.Click Yes No Buttons  Delete
  Wait.DOMreadyByWaitTime  2
  Wait Until Element is Visible  xpath=//span[text()='Item deleted successfully.']
  Element Should not be visible    //span[text()='AUT_Document1']
  
      
Sourcing-03015-Click on New Supplier PO link and check if a popup is opened. Select the Supplier and its Factory. Update all values, 'Color Base Ordering' and click on Save. Check if a Supplier PO is created with the updated values.	
#Home-Sourcing-Supplier PO-Supplier PO
#Click on New Supplier PO link and check if a popup is opened. Select the Supplier and its Factory. Update all values, 'Color Base Ordering' and click on Save. Check if a Supplier PO is created with the updated values.
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads
  PO_HomePage.Click on Sourcing tab 
  DOMready-TabLoads
  Click Element  ${Loc_Home-Sourcing-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  ${Loc_Home-Sourcing-SupplierPO-SupplierPOTab}
  DOMready-TabLoads
  Mouse Over  ${Loc_Home-Sourcing-SupplierPO-SupplierPOTab_ActionsLink}
  Click Element  ${Loc_Home-Sourcing-SupplierPO-SupplierPOTab_ActionsLink}
  #Click Element  ${Loc_Home-Sourcing-SupplierPO-SupplierPOTab_Actions_NewSupplierPOLink}
  DOMready-PopUpAppears
  Assert the Element in popup dialog  Supplier PO
  Assert the Element in popup dialog  Supplier
  Assert the Element in popup dialog  Factory
  Assert the Element in popup dialog  Color Based Ordering

#Select the Supplier and its Factory. Update all values, do not select 'Color Base Ordering' option and click on Save. Check if a Supplier PO is created with the updated values.
  ${Element} =  Get Required Element  ${Loc_SupplierPOTab_NewSupplierWindow_SupplierPOName}  -1
  Selenium2Library.Input Text  ${Element}  ${SupplierPO2}
  Click on the DDL button  field-PurchasedOrder-Form-POSupplier
  Select Value From the DDL  ${Supplier1} 
  #Click on the DDL button  field-PurchasedOrder-Form-POFactory
  #Select Value From the DDL  ${Factory1}
  ${Check} =  Get Element Attribute  ${Loc_SupplierPOTab_NewSupplierWindow_ColorBasedOrderingCheckbox}  aria-checked
  Run Keyword If  str('${Check}') == str('false')  Click Element  ${Loc_SupplierPOTab_NewSupplierWindow_ColorBasedOrderingCheckbox}
  Click On Save Button
  DOMready-PopUpCloses  
  #Check if a Supplier PO is created with the updated values.
  Assert for the given Element   //a[@class='browse' and text()='${SupplierPO2}']
  #Assert for the given Element   //a[@class='browse' and text()='${Supplier1}']//following::td[@data-csi-act='Factories::0']//a[@class='browse' and text()='{Factory1}']   
Sourcing-03016-Click on the created PO and check for tabs PO, Properties, Orders, Samples, Shipment, Stuffing Location, Quality Control, Documents appearing in the same order. The PO tab should be the default one selected.	
#Home-Sourcing-Supplier PO-Supplier PO	
#Click on the created PO and check for tabs PO, Properties, Orders, Samples, Shipment, Stuffing Location, Quality Control, Documents appearing in the same order. The PO tab should be the default one selected.
  Click Element  //a[@class='browse' and text()='${SupplierPO2}']
  DOMready-TabLoads
  Click Element  ${Loc_SupplierPONodeName_SupplierPOTab}
  DOMready-TabLoads
  Assert the given tab  Supplier PO
  Assert the given tab  Properties
  Assert the given tab  Orders
  Assert the given tab  Samples
  Assert the given tab  Shipment
  Assert the given tab  Stuffing Location
  Assert the given tab  Issues
  Assert the given tab  Quality Control
  Assert the given tab  Docs & Comments

#The PO tab should be the default one selected.'
  ${Check} =  Get Element Attribute  ${Loc_SupplierPONodeName_SupplierPOTab}  aria-selected
  Run Keyword If  str('${Check}') == str('true')  Log  The Default Tab Selected is Supplier PO
  Run Keyword If  str('${Check}') == str('false')  Log  The Default Tab Selected is not Supplier PO

Sourcing-03018-Verify that instead of displaying 'New SKUs' & 'New Material SKUs' links, 'New Colorways' & 'New Colored Materials' selection links are displayed	
#Home-Sourcing-Supplier PO-Supplier PO-Orders-Orders
#Verify that instead of displaying 'New SKUs' & 'New Material SKUs' links, 'New Colorways' & 'New Colored Materials' selection links are displayed
  #Execute Javascript    window.document.evaluate('//span[@data-csi-automation="actions-PurchasedOrder-Orders-root"]', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true)
  Click Element  //span[@class="dijitReset dijitInline dijitIcon dijitNoIcon csi-toolbar-btn-icon-htmlToolbars"]
  DOMreadyByWaitTime  6
  MouseOver  ${SupplierPO_OrderTable_Actions} 
  #Click Element  ${SupplierPO_OrderTable_Actions}
  #Assert for the given element  ${Loc_POSupplierNode_SupplierPOTab_Actions_NewColorwaysOrderLink}
  #Assert for the given element  ${Loc_POSupplierNode_SupplierPOTab_Actions_NewColoredMaterialsOrderLink}
  #Click Element  ${SupplierPO_OrderTable_Actions}
  Click Element  css=[data-csi-automation='plugin-PurchasedOrder-Orders-ToolbarNewActions'] td.dijitArrowButton
  Wait Until Page Contains Element  css=[data-csi-automation='plugin-PurchasedOrder-Orders-ToolbarNewActions'] [data-csi-act='AggregateColorwayOrders'] .dijitMenuItemLabel  
  Click Element  css=[data-csi-automation='plugin-PurchasedOrder-Orders-ToolbarNewActions'] [data-csi-act='AggregateColorwayOrders'] .dijitMenuItemLabel  
  DOMready-PopUpAppears
 
 
Sourcing-03019-Click on New Colorways and check whether New Colorways aggregate view displays the list of all Colorways from all the Styles	
#Home-Sourcing-Supplier PO-Supplier PO-Orders-Orders
#Click on New Colorways and check whether New Colorways aggregate view displays the list of all Colorways from all the Styles
  Assert for the given element   //*[@class='attrString firstColumn' and text()='${ColorSpec1}']
Sourcing-03020-Select few Colorways and click on Save. Check whether the Colorways gets added to the Orders table	
#Home-Sourcing-Supplier PO-Supplier PO-Orders-Orders
#Select few Colorways and click on Save. Check whether the Colorways gets added to the Orders table
  Select Checkbox in popup     ${ColorSpec1}
  DOMreadyByWaitTime  3
  Click Element    ${Loc_SaveButton}
  DOMready-PopUpCloses
  Element Should Be Visible  //td[text()='${ColorSpec1}']   
#######
  SuiteSetUp.Exit Execution
***keywords***
Enter The Values on Textarea
    [Arguments]   ${Element}  ${InputData}
    DOMreadyByWaitTime  2
    # Press backspace button
    Press Key  ${Element}  \\08
    DOMreadyByWaitTime  2
    Press Key  ${Element}  ${InputData}   
Setup The Variables
  ${This_DataProvider} =  Data_Provider.DataProvider  SupplierPO
  Set Suite Variable  ${This_DataProvider}
  Log  ${This_DataProvider}

  ${ShipmentCollection} =  Data_Provider.GetDataProviderColumnValue  Data_ShipmentName
  ${Shipment2} =  DataProviderSplitterForMultipleValues  ${ShipmentCollection}  2
  Set Suite Variable  ${Shipment2}
  
  ${SupplierPOCollection} =  GetDataProviderColumnValue  Data_SupplierPOName
  ${SupplierPO1} =  DataProviderSplitterForMultipleValues  ${SupplierPOCollection}  1 
  ${SupplierPO2} =  DataProviderSplitterForMultipleValues  ${SupplierPOCollection}  2
  Set Suite Variable  ${SupplierPO1}
  Set Suite Variable  ${SupplierPO2}
  
  ${POGroupCollection} =  GetDataProviderColumnValue  Data_POGroupName
  ${POGroup1} =  DataProviderSplitterForMultipleValues  ${POGroupCollection}  1 
  Set Suite Variable  ${POGroup1}
 
  ${DocumentNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_DocumentName
  ${Document1} =  DataProviderSplitterForMultipleValues  ${DocumentNameCollection}  1
  Set Suite Variable  ${Document1}
  
  ${CommentNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_CommentName
  ${Comment1} =  DataProviderSplitterForMultipleValues  ${CommentNameCollection}  1
  Set Suite Variable  ${Comment1}

  ${IssueNameCollection} =  GetDataProviderColumnValue  Data_IssueName  
  ${IssueName1} =  DataProviderSplitterForMultipleValues  ${IssueNameCollection}  1 
  ${IssueName2} =  DataProviderSplitterForMultipleValues  ${IssueNameCollection}  2
  Set Suite Variable  ${IssueName1}
  Set Suite Variable  ${IssueName2}  

 ***Comments
  
Sourcing-03017-Check whether 'Unit Per Pack', 'Sizes' and 'Order Quantity' attribute is editable	
#Home-Sourcing-Supplier PO-Supplier PO
#Check whether 'Unit Per Pack', 'Sizes' and 'Order Quantity' attribute is editable
   ##### Need to add data to verify the Size
  Click Element FRom List  //td[text()='${ColorSpec1}']//following::td[@data-csi-act='Quantity::0']  0
  DOMreadyByWaitTime  3
      Enter The Values on Textarea  //div[contains(@class,'csi-flex-row')]//input[contains(@class,'dijitReset dijitInputInner')]  10
  
  #Selenium2Library.Input Text   css=[data-csi-automation='edit-PurchasedOrder-Orders-Quantity:'] .dijitInputInner  10
  Click Element	  ${Loc_Release}
  
  Element Should Be Visible	//td[text()='${ColorSpec1}']//following::td[@data-csi-act='Quantity::0' and contains(text(),'10')]
  DOMreadyByWaitTime  3
  #Click Element  xpath=//span[@class="dijitReset dijitInline dijitIcon dijitNoIcon csi-toolbar-btn-icon-htmlToolbars"]
  #DOMreadyByWaitTime  6
  #Execute Javascript    window.document.evaluate('//span[@data-csi-automation="plugin-PurchasedOrder-Orders-ToolbarNewActions"]', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
  Click Element  ${Loc_POSupplierNode_SupplierPOTab_OrdersSection_ProductDDL}
  DOMreadyByWaitTime  3
  Select Value from the DDL  ${Style2}
  DOMreadyByWaitTime  6
  
  Click Element From List   //td[text()='${ColorSpec1}']//following::td[@data-csi-act='UnitPerPack::0']  0
  DOMreadyByWaitTime  3
      Enter The Values on Textarea  //div[contains(@class,'csi-flex-row')]//input[contains(@class,'dijitReset dijitInputInner')]  5
  
  #Selenium2Library.Input Text   css=[data-csi-automation='edit-PurchasedOrder-Orders-UnitPerPack:'] .dijitInputInner  5
  Click Element	  ${Loc_Release}
  Element Should Be Visible	//td[text()='${ColorSpec1}']//following::td[@data-csi-act='UnitPerPack::0' and contains(text(),'5')]

  
  Click Element From List   //td[text()='${ColorSpec1}']//following::td[@data-csi-act='QuantityPerSize::0']  0
  DOMreadyByWaitTime  3
      Enter The Values on Textarea  //div[contains(@class,'csi-flex-row')]//input[contains(@class,'dijitReset dijitInputInner')]  5
  
  #Selenium2Library.Input Text   css=[data-csi-automation='edit-PurchasedOrder-Orders-QuantityPerSize:'] .dijitInputInner  5
  Click Element	  ${Loc_Release}
  Element Should Be Visible	//td[text()='${ColorSpec1}']//following::td[@data-csi-act='QuantityPerSize::0']//span[1][contains(text(),'5')]
  Element Should Be Visible	//td[text()='${ColorSpec1}']//following::td[@data-csi-act='QuantityPerSize::0']//span[2][contains(text(),'5')]   
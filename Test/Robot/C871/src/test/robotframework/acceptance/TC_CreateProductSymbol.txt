*** Settings ***
Test Setup			Setup ProductSymbol
Resource      ../UtilityHelper/SuiteSetUp.txt  
Resource      ../UtilityHelper/Report_Properties.txt
Resource      ../UtilityHelper/Wait.txt
Resource      ../UtilityHelper/Assertions.txt

Resource      ../Locators/Loc_MaterialPage.txt  
Resource      ../Locators/Loc_SetupPage.txt

Resource      ../PageObjects/PO_SetupPage.txt  
Resource      ../PageObjects/PO_LoginPage.txt
Resource      ../PageObjects/PO_HomePage.txt
Resource      ../PageObjects/PO_SourcingPage.txt
Resource      ../PageObjects/PO_SpecificationPage.txt
Resource      ../PageObjects/PO_TablePage.txt

***Variable***
@{ReqCareTypeList}  Brand  Feature  Function
${Loc_ProductSymbolNode-ProductSymbolTab}  css=span[data-csi-tab='ProductSymbol-Base']
${Loc_ProductSymbolNode-MaterialsTab}  css=span[data-csi-tab='ProductSymbol-WhereUsedMaterial']
${Loc_ProductSymbolNode-StylesTab}  css=span[data-csi-tab='ProductSymbol-WhereUsedStyle']
${Loc_InputText}	css=[data-csi-automation='edit-ApparelViews-ProductSymbols-Node Name:']
*** Test Cases ***
Create Product Symbol
	[tags]  Sanity_DataLoad
	# Step 1: Open the Application in the browser.
  #Step Description
  Open Browser To Login Page
   
  # Step 2: Enter the login Credentials and Submit.
  PO_LoginPage.Input UserName  Data_UserName
  PO_LoginPage.Input Password  Data_Password
  
  PO_LoginPage.Submit Credentials
  #Check-is the DOM ready
  #Reload
  Wait.DOMready
  Wait For Release Info
  #Assertion: Asserting for the 'Home' Tab to be Active
  #PO_SetupPage.Verify That This Tab Is Fronted  My Home
  Click on Home Icon
  Wait For Release Info
  Wait.DOMready
  #Assertion: Asserting for the 'Configuration' Tab to be Active
  #PO_SetupPage.Verify That This Tab Is Fronted  My Home
   
  # Step 3: Navigate to Home-Sourcing-Setup-HTS Code
  #Step Description 

  PO_HomePage.Click On Material tab
  Wait For Release Info
  DOMready-TabLoads
  #Navigate to care Label tab 
   ${ScreenSizeISSmall}=  Run KeyWord And Return Status  Click On Right Arrow Button To View Hidden Tabs  1
   Log  HorizandalScrolbar:${ScreenSizeISSmall}  console=True 
   
Material-00224   
  Click Element  ${Loc_ProductSymbolTab}
  Wait For Release Info	 
  DOMready-TabLoads  
  
  #Create Care Symbol 
  Mouse Over  ${Loc_ActionsProductSymbol}
  Click Element  ${Loc_ActionsProductSymbol}
  #Wait Until Page Contains Element  ${Loc_ProductSymbolLink}
  #Click Element  ${Loc_ProductSymbolLink}
  DOMreadyByWaitTime  2 
  Click Element  ${Loc_ProductSymbolTab}
  Wait For Release Info	
  DOMready-TabLoads
  Execute Javascript    window.document.evaluate("//a[text()='(Unnamed)']/parent::td", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();
  DOMreadyByWaitTime  4
  #Click Element  //div/textarea
  Input Text  //div/textarea  ${ProductSymbol1}T
  Click Element  ${Loc_ProductSymbolTab}
  DOMready-TabLoads
  #Enter Values in Text Area on a Table  ${Loc_InputText}  Auto_Brand_01
  #Wait For the Entered Values in TextArea to GetUpdate  ApparelViews-ProductSymbols  ${Loc_InputText}   
Material-00225  
  #Update CareType
  Click on the Table Column in Reference to Nodename  ${ProductSymbol1}T  SymbolType::0
  DOMreadyByWaitTime  4
  # Get values from the caretype ddl 
  ${ActualProductSymbolList}=  Get Values From DDL Type 1
  Log List  ${ActualProductSymbolList}	
  Lists Should be Equal  ${ActualProductSymbolList}  ${ReqCareTypeList}
  Select Subtype value  Brand  ${ProductSymbol1}T
  Update Rank  ${ProductSymbol1}T  1

Material-00226
    Click Link  //td[@data-csi-heading='Node Name::0']/a[text()='${ProductSymbol1}T']
    DOMready-TabLoads
    Verify the Element  ${Loc_ProductSymbolNode-ProductSymbolTab}  Product Symbol Tab is Displayed for Product Symbol Node 
    Verify the Element  ${Loc_ProductSymbolNode-MaterialsTab}  Materials Tab is Displayed for Product Symbol Node 
    Verify the Element  ${Loc_ProductSymbolNode-StylesTab}  Styles Tab is Displayed for Product Symbol Node
    Click on Home Icon
    Wait For Release Info
    PO_HomePage.Click On Material tab
    Wait For Release Info
    Click Element  ${Loc_ProductSymbolTab}
    Wait For Release Info 
    
Click Copy Icon
  Click on Action Icon in Table With Preceding Link  ${ProductSymbol1}T  Copy
  DOMreadyByWaitTime  1 
  #Run Keyword And Ignore Error  Enter Values in Text Area on a Table  css=div>textarea[data-csi-automation='edit-SizeChartAdmin-CareSymbols-Node Name:']  Auto_Wash_02
  # Run Keyword And Ignore Error  Clear Element Text  //div/textarea
  Enter Text In TextArea  //div/textarea  ${ProductSymbol2}
  #Run Keyword And Ignore Error  Enter Values in Text Area on a Table  //*[@class='dijitPopup dijitTooltipDialogPopup']//following::input[1]  Auto_Brand_02
  Wait For the Entered Values in TextArea to GetUpdate  ApparelViews-ProductSymbols  ${Loc_InputText}
  Wait.DOMreadyByWaitTime  2
    
Click Delete Icon      
  Click on Action Icon in Table With Preceding Link  ${ProductSymbol2}  Delete
  Wait.DOMreadyByWaitTime  2
  PO_SetupPage.Click Yes No Buttons  Cancel
  Wait.DOMreadyByWaitTime  2
  #
  Click on Action Icon in Table With Preceding Link  ${ProductSymbol2}  Delete
  Wait.DOMreadyByWaitTime  2
  PO_SetupPage.Click Yes No Buttons  Delete
  Wait.DOMreadyByWaitTime  2

Creating the Other Product Symbols
# Creating the Values with the Brand, Feature , Function
  #Create Care Symbol 
  Mouse Over  ${Loc_ActionsProductSymbol}
  Click Element  ${Loc_ActionsProductSymbol}
  #ait Until Page Contains Element  ${Loc_ProductSymbolLink}
  #Click Element  ${Loc_ProductSymbolLink}
  DOMreadyByWaitTime  2 
  Click Element  ${Loc_ProductSymbolTab}
  DOMready-TabLoads	
  Execute Javascript    window.document.evaluate("//a[text()='(Unnamed)']/parent::td", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();
  Click Element  //div/textarea
  Input Text  //div/textarea  ${ProductSymbol1}
  Click Element  ${Loc_ProductSymbolTab}
  DOMready-TabLoads
  #Enter Values in Text Area on a Table  ${Loc_InputText}  ${ProductSymbol1}
  #Wait For the Entered Values in TextArea to GetUpdate  ApparelViews-ProductSymbols  ${Loc_InputText}   
  Click on the Table Column in Reference to Nodename  ${ProductSymbol1}  SymbolType::0
  Select Subtype value  Brand  ${ProductSymbol1}
  Update Rank  ${ProductSymbol1}  1
  
  Mouse Over  ${Loc_ActionsProductSymbol}
  Click Element  ${Loc_ActionsProductSymbol}
  #Wait Until Page Contains Element  ${Loc_ProductSymbolLink}
  #Click Element  ${Loc_ProductSymbolLink}
  DOMreadyByWaitTime  2 
  Click Element  ${Loc_ProductSymbolTab}
  DOMready-TabLoads	
  Execute Javascript    window.document.evaluate("//a[text()='(Unnamed)']/parent::td", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();
  Click Element  //div/textarea
  Input Text  //div/textarea  ${ProductSymbol2}
  Click Element  ${Loc_ProductSymbolTab}
  DOMready-TabLoads
  #Enter Values in Text Area on a Table  ${Loc_InputText}  ${ProductSymbol2}
  #Wait For the Entered Values in TextArea to GetUpdate  ApparelViews-ProductSymbols  ${Loc_InputText}   
  Click on the Table Column in Reference to Nodename  ${ProductSymbol2}  SymbolType::0
  Select Subtype value  Brand  ${ProductSymbol2}
  Update Rank  ${ProductSymbol2}  2
# Creating the Feature Type
  Mouse Over  ${Loc_ActionsProductSymbol}
  Click Element  ${Loc_ActionsProductSymbol}
  #Wait Until Page Contains Element  ${Loc_ProductSymbolLink}
  #Click Element  ${Loc_ProductSymbolLink}
  DOMreadyByWaitTime  2 
  Click Element  ${Loc_ProductSymbolTab}
  DOMready-TabLoads	
  Execute Javascript    window.document.evaluate("//a[text()='(Unnamed)']/parent::td", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();
  Click Element  //div/textarea
  Input Text  //div/textarea  ${ProductSymbol3}
  Click Element  ${Loc_ProductSymbolTab}
  DOMready-TabLoads
  #Enter Values in Text Area on a Table  ${Loc_InputText}  ${ProductSymbol3}
  #Wait For the Entered Values in TextArea to GetUpdate  ApparelViews-ProductSymbols  ${Loc_InputText}   
  Click on the Table Column in Reference to Nodename  ${ProductSymbol3}  SymbolType::0
  Select Subtype value  Feature  ${ProductSymbol3}
  Update Rank  ${ProductSymbol3}  1
  
  Mouse Over  ${Loc_ActionsProductSymbol}
  Click Element  ${Loc_ActionsProductSymbol}
  #Wait Until Page Contains Element  ${Loc_ProductSymbolLink}
  #Click Element  ${Loc_ProductSymbolLink}
  DOMreadyByWaitTime  2 
  Click Element  ${Loc_ProductSymbolTab}
  DOMready-TabLoads	
  Execute Javascript    window.document.evaluate("//a[text()='(Unnamed)']/parent::td", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();
  Click Element  //div/textarea
  Input Text  //div/textarea  ${ProductSymbol4}
  Click Element  ${Loc_ProductSymbolTab}
  DOMready-TabLoads
  #Enter Values in Text Area on a Table  ${Loc_InputText}  ${ProductSymbol4}
  #Wait For the Entered Values in TextArea to GetUpdate  ApparelViews-ProductSymbols  ${Loc_InputText}   
  Click on the Table Column in Reference to Nodename  ${ProductSymbol4}  SymbolType::0
  Select Subtype value  Feature  ${ProductSymbol4}
  Update Rank  ${ProductSymbol4}  2
# Creating the Function Type
  Mouse Over  ${Loc_ActionsProductSymbol}
  Click Element  ${Loc_ActionsProductSymbol}
  #Wait Until Page Contains Element  ${Loc_ProductSymbolLink}
  #Click Element  ${Loc_ProductSymbolLink}
  DOMreadyByWaitTime  2 
  Click Element  ${Loc_ProductSymbolTab}
  DOMready-TabLoads	
  Execute Javascript    window.document.evaluate("//a[text()='(Unnamed)']/parent::td", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();
  Click Element  //div/textarea
  Input Text  //div/textarea  ${ProductSymbol5}
  Click Element  ${Loc_ProductSymbolTab}
  DOMready-TabLoads
  #Enter Values in Text Area on a Table  ${Loc_InputText}  ${ProductSymbol5}
  #Wait For the Entered Values in TextArea to GetUpdate  ApparelViews-ProductSymbols  ${Loc_InputText}   
  Click on the Table Column in Reference to Nodename  ${ProductSymbol5}  SymbolType::0
  Select Subtype value  Function  ${ProductSymbol5}
  Update Rank  ${ProductSymbol5}  1
  
  Mouse Over  ${Loc_ActionsProductSymbol}
  Click Element  ${Loc_ActionsProductSymbol}
  #Wait Until Page Contains Element  ${Loc_ProductSymbolLink}
  #Click Element  ${Loc_ProductSymbolLink}
  DOMreadyByWaitTime  2 
  Click Element  ${Loc_ProductSymbolTab}
  DOMready-TabLoads	
  Execute Javascript    window.document.evaluate("//a[text()='(Unnamed)']/parent::td", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();
  Click Element  //div/textarea
  Input Text  //div/textarea  ${ProductSymbol6}
  Click Element  ${Loc_ProductSymbolTab}
  DOMready-TabLoads
  #Enter Values in Text Area on a Table  ${Loc_InputText}  ${ProductSymbol6}
  #Wait For the Entered Values in TextArea to GetUpdate  ApparelViews-ProductSymbols  ${Loc_InputText}   
  Click on the Table Column in Reference to Nodename  ${ProductSymbol6}  SymbolType::0
  Select Subtype value  Function  ${ProductSymbol6}
  Update Rank  ${ProductSymbol6}  2
  [Teardown]    Close Browser 
 ***Keyword***
Setup ProductSymbol
  ${This_DataProvider} =  Data_Provider.DataProvider  ProductSymbol
  Set Test Variable  ${This_DataProvider}
    
  ${ProductSymbolCollection} =  GetDataProviderColumnValue  Data_ProductSymbol
  ${ProductSymbol1} =  DataProviderSplitterForMultipleValues  ${ProductSymbolCollection}  1 
  ${ProductSymbol2} =  DataProviderSplitterForMultipleValues  ${ProductSymbolCollection}  2
  ${ProductSymbol3} =  DataProviderSplitterForMultipleValues  ${ProductSymbolCollection}  3
  ${ProductSymbol4} =  DataProviderSplitterForMultipleValues  ${ProductSymbolCollection}  4
  ${ProductSymbol5} =  DataProviderSplitterForMultipleValues  ${ProductSymbolCollection}  5
  ${ProductSymbol6} =  DataProviderSplitterForMultipleValues  ${ProductSymbolCollection}  6
  Set Test Variable  ${ProductSymbol1}
  Set Test Variable  ${ProductSymbol2}
  Set Test Variable  ${ProductSymbol3}
  Set Test Variable  ${ProductSymbol4}
  Set Test Variable  ${ProductSymbol5}
  Set Test Variable  ${ProductSymbol6}

Select Subtype value
  [Arguments]  ${SymbolType}  ${SymbolName}
   Click Element  //div[@data-csi-automation='edit-ApparelViews-ProductSymbols-SymbolType:']/following::div[@class='dijitReset dijitMenuItem' and text()='${SymbolType}']
   Assert the Value Updated in Reference to NodeName  ${SymbolName}  SymbolType::0  ${SymbolType}

Update Rank 
  [Arguments]  ${SymbolName}  ${Value}
  Click on the Table Column in Reference to Nodename  ${SymbolName}  Rank::0
  DOMreadyByWaitTime  1 
  #Run Keyword And Ignore Error  Clear Element Text  //div/input
  Enter Text In TextArea  ${Loc_TableColumnInputArea}  ${Value}
  #Enter Values in Text Area on a Table  css=[data-csi-automation='edit-SizeChartAdmin-CareSymbols-Rank:']>div>input    1
  Wait For the Entered Values in TextArea to GetUpdate  ApparelViews-ProductSymbols  ${Loc_InputNo}
  
Enter Text In TextArea
    [Arguments]  ${Element}  ${Input}
    Click Element  ${Element}
    DOMreadyByWaitTime  2
    # Press backspace button
    Press Key  ${Element}  \\1
    Press Key  ${Element}  \\08
    DOMreadyByWaitTime  2
    Press Key  ${Element}  ${Input}     

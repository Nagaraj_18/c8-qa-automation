*** Settings ***
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt

Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_SourcingPage.txt

**Variables**
${Loc_ActionsThemeMasterSubtypes}  css=[data-csi-automation='plugin-Site-ThemeMasterSubtypes-ToolbarNewActions'] 
${Loc_ThemeMasterSubtTypeLink}  css=[data-csi-act="NewThemeMasterSubtype"] .dijitMenuItemLabel

*** Test Cases ***
Home-00038
	[tags]  Sanity_DataLoad
	#Data provider: It reads the data from the excel sheet for the test case specified. 
	${This_DataProvider} =  Data_Provider.DataProvider  Create BOM Type
	Set Test Variable  ${This_DataProvider}
	Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    Wait For Release Info
    #Reload
    Wait.DOMready
    DOMready-TabLoads
    Click on Setup Icon
	Wait For Release Info
    #Assertion: Asserting for the 'Configuration' Tab to be Active
    #PO_SetupPage.Verify That This Tab Is Fronted    My Home
   
    # Step 3: Navigate to BOM Type tab	
    #Step Description 
    PO_SetupPage.Click On Configuration tab
	Wait For Release Info	
    PO_SetupPage.Click On Type Configuration tab
	Wait For Release Info
    #PO_SetupPage.Click on Merchandise types tab 
    Wait.DOMready
    ${ScreenSizeISSmall}=  Run KeyWord And Return Status  Click On Right Arrow Button To View Hidden Tabs  0
    Log  HorizandalScrolbar:${ScreenSizeISSmall}  console=True 
    DOMreadyByWaitTime    4 
ThemeMasterType
    [Tags]  CI_TEST
    Click Element  css=[data-csi-tab='Site-ThemeMasterSubtypes']
    DOMready-TabLoads
    PO_SetupPage.Click Required Link Under Actions  ${Loc_ActionsThemeMasterSubtypes}  ${Loc_ActionsThemeMasterSubtypes}
    DOMreadyByWaitTime    2
    DOMready-PopUpAppears
    Wait Until Page Contains Element  css=div[data-csi-automation='field-ThemeMasterSubtype-Form-Node Name'] div.dijitInputField>input[name='Node Name']
    Click Element  css=div[data-csi-automation='field-ThemeMasterSubtype-Form-Node Name'] div.dijitInputField>input[name='Node Name']
    Input Text    css=div[data-csi-automation='field-ThemeMasterSubtype-Form-Node Name'] div.dijitInputField>input[name='Node Name']  ThemeSubtype_01	
    Wait.DOMreadyByWaitTime  2
    PO_NewCreationPopUpPage.Click on Save Button
    DOMready-PopUpCloses
    Wait Until Page Contains Element  //span[contains(text(),'successfully')]
   	Click Element  //td[text()='ThemeSubtype_01']//following-sibling::td//div[@data-csi-act="Active:Child:SetupSettings:0"]
	DOMreadyByWaitTime  2
	PO_SetupPage.Click on BOM Type tab
	DOMready-TabLoads
    # Step 5: Close the Browser Instance.
    Test Step    ${R_Close_Browser}
    [Teardown]    Close Browser

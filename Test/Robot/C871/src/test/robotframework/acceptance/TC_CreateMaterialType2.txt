*** Settings ***
Test Setup  		DataSetup For Material Types
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt

Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_MaterialTypePage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../PageObjects/PO_TablePage.txt
Resource          ../Locators/Loc_SetupPage.txt    
***Variables***
${Loc_MaterialTypeDDL}   css=[data-csi-automation='field-MaterialType-Form-MaterialUsage'] .dijitMenuItem[item='0']
${MaterialUsageType1}  Standalone
${MaterialUsageType2}  Structure Component
${MaterialUsageType3}  Tool
${HSliderPosition}  //div[contains(@class,'dijitSliderImageHandle')]
#@{MaterialType1Attributes}  data-csi-heading='DefaultHasSeasonAvailability:Child:Config:0'	data-csi-heading='HasSize:Child:Config:0'  data-csi-heading='HasColor:Child:Config:0'	data-csi-heading='DefaultPlacementQuoteToHighestCost:Child:Config:0'  data-csi-heading='DefaultColorSizeAvailability:Child:Config:0'	data-csi-heading='AllowCreateColoredMaterialOnPlacement:Child:Config:0'	data-csi-heading-vk='MaterialBOM'	data-csi-heading-vk='TestRun'	data-csi-heading-vk='Routing'	data-csi-heading-vk='SpecificationDataSheet' 
*** Test Cases ***
Home-00029
	[tags]  DataLoad
    SuiteSetUp.Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    Wait.DOMready
    DOMready-TabLoads
    #Reload        
	Wait.DOMready
    DOMready-TabLoads
    Click on Setup Icon
    Wait.DOMready
    DOMready-TabLoads
    PO_SetupPage.Click On Configuration tab
    #Check-is the DOM ready
    Wait.DOMready
    SuiteSetUp.Test Step    ${R_Click_TypeConfigurationTab}
    PO_SetupPage.Click On Type Configuration tab
    #Check-is the DOM ready
    Wait.DOMready
    SuiteSetUp.Test Step    ${R_Click_MaterialTypesTab}
    PO_SetupPage.Click On Material types tab
    DOMready-TabLoads
    Create CustomView
    Select View  Default    
    #Check-is the DOM ready
    DOMready-TabLoads
    DOMreadyByWaitTime  3
    ${present} =  Run Keyword And Return Status    Element Should Be Visible   //td[@data-csi-heading='Node Name::0'and text()='${MaterialName}']
    Run Keyword If   '${present}' == 'False'  Create StandaloneMaterialType and select Data Sheets      
    Update Data Sheets For StandaloneMaterialType 
    Select View  Default
    PO_SetupPage.Click On Material types tab
    DOMready-TabLoads
    PO_SetupPage.Click On Style types tab
    DOMready-TabLoads
    PO_SetupPage.Click On Material types tab
    DOMready-TabLoads
    Create Structure Material Type
    Select View  Default
    PO_SetupPage.Click On Material types tab
    DOMready-TabLoads
    PO_SetupPage.Click On Style types tab
    DOMready-TabLoads
    PO_SetupPage.Click On Material types tab
    DOMready-TabLoads
    Create Tool Material Type
    Update Data Sheets For StructureMaterialType2  
    Update Data Sheets For ToolMaterialType 

 *** Keywords ***
 Create Structure Material Type
    Polling Click Required Action  ${Actions_MaterialType}  ${Loc_NewMaterialType}  
    DOMready-PopUpAppears
    #Step Description
    SuiteSetUp.Test Step    ${R_EnterNewMaterialType}
    PO_MaterialTypePage.Enter the Material Type in the Pop up window    ${StructureMaterialType2}
	PO_NewCreationPopUpPage.Click on the DDL button  field-MaterialType-Form-MaterialUsage
  #Wait for the DDL to appear by checking on the blank value
    ${DDLClickStatus} =  Run Keyword and Return Status  Wait Until Element Is Visible  ${Loc_MaterialTypeDDL}
    Run Keyword If   '${DDLClickStatus}' == 'False'  Clear Element Text  css=td>div[data-csi-automation='field-MaterialType-Form-MaterialUsage'] div.dijitInputField>input
    Run Keyword If   '${DDLClickStatus}' == 'False'  Input Text  css=td>div[data-csi-automation='field-MaterialType-Form-MaterialUsage'] div.dijitInputField>input  ${MaterialUsageType2}
    Run Keyword If   '${DDLClickStatus}' == 'True'  PO_NewCreationPopUpPage.Select Elemnt from DDL   ${MaterialUsageType2}
	Wait.DOMreadyByWaitTime  2
    Click on Material Save Button
    Select Required Check box on the Material Type    ${StructureMaterialType2}  data-csi-heading='Available:Child:Config:0'
Create Tool Material Type 
    Polling Click Required Action  ${Actions_MaterialType}  ${Loc_NewMaterialType}   
    DOMready-PopUpAppears
    #Step Description
    SuiteSetUp.Test Step    ${R_EnterNewMaterialType}
    PO_MaterialTypePage.Enter the Material Type in the Pop up window    ${ToolMaterialTypeName}
	PO_NewCreationPopUpPage.Click on the DDL button  field-MaterialType-Form-MaterialUsage
  #Wait for the DDL to appear by checking on the blank value
    ${DDLClickStatus} =  Run Keyword and Return Status  Wait Until Element Is Visible  ${Loc_MaterialTypeDDL}
    Run Keyword If   '${DDLClickStatus}' == 'False'  Clear Element Text  css=td>div[data-csi-automation='field-MaterialType-Form-MaterialUsage'] div.dijitInputField>input
    Run Keyword If   '${DDLClickStatus}' == 'False'  Input Text  css=td>div[data-csi-automation='field-MaterialType-Form-MaterialUsage'] div.dijitInputField>input  ${MaterialUsageType1}
    Run Keyword If   '${DDLClickStatus}' == 'True'  PO_NewCreationPopUpPage.Select Elemnt from DDL   ${MaterialUsageType1}
	Wait.DOMreadyByWaitTime  2
    Click on Material Save Button
    Select Required Check box on the Material Type    ${ToolMaterialTypeName}  data-csi-heading='Available:Child:Config:0'
    
Update Data Sheets For StructureMaterialType2    
   @{MaterialType1Attributes}  Create List	7	5	11	6	12	15	13	16	13	19	22	20	23	21	26	24	27	25
    # Step 5: Select the Material type attributes Checkboxes.
    #Step Description
    :FOR  ${I}  IN  @{MaterialType1Attributes}
    #\  @{MatchText} =  Split String  ${I}  '   
    #\  ${MatchValue} =  Get From List  ${MAtchText}  1    
    #\  Run Keyword If  '${MatchValue}' == 'MaterialBOM'  Select View  simplifiedview
	\  Run Keyword If  ${I} == 12  Select View  simplifiedview
    \  DOMreadyByWaitTime    2
    \  Select Required Check box on the Material Type2    ${StructureMaterialType2}  ${I}
    Select View  Default
    PO_SetupPage.Click On Material types tab
    DOMready-TabLoads

Update Data Sheets For ToolMaterialType    
   @{MaterialType1Attributes}  Create List	7	6	11	14	12	15	13	16	19	17	20	23	21	24	22	25	27	26
    # Step 5: Select the Material type attributes Checkboxes.
    #Step Description
    :FOR  ${I}  IN  @{MaterialType1Attributes}
    #\  @{MatchText} =  Split String  ${I}  '   
    #\  ${MatchValue} =  Get From List  ${MAtchText}  1    
    #\  Run Keyword If  '${MatchValue}' == 'MaterialBOM'  Select View  simplifiedview
    \  Run Keyword If  ${I} == 12  Select View  simplifiedview
	\  DOMreadyByWaitTime    2
    \  Select Required Check box on the Material Type2    ${ToolMaterialTypeName}  ${I}
    
    PO_SetupPage.Click On Material types tab
	${ElementList} =  Get WebElements   //td[@class='attrString firstColumn' and text()='${ToolMaterialTypeName}']/following::td[@data-csi-heading='HasColor:Child:Config:0']/div[1]/input
	${Element} =  Get From List  ${ElementList}  0
	Set Focus To Element  ${Element}
	Click Element  ${Element}
	
	Select View  Default
    PO_SetupPage.Click On Material types tab
    DOMready-TabLoads
    
Update Compatible types for First Material 
  PO_MaterialTypePage.Click Compatible Types cell in Material Type table  ${StructureMaterialType1}
  Wait Until Page Contains Element	  //div[@data-csi-automation='edit-Site-MaterialTypes-CompatibleTypes:Child:Config']//div[@class='dijitReset dijitMenuItem']
  Click Element From List  	  //div[@data-csi-automation='edit-Site-MaterialTypes-CompatibleTypes:Child:Config']//div[@class='dijitReset dijitMenuItem']  -1
  DOMready-PopUpAppears
  PO_NewCreationPopUpPage.Wait For PopUp Dialog to Appear  Select Compatible Types
  PO_NewCreationPopUpPage.Select Checkbox in popup    ${StructureMaterialType1}
  DOMreadyByWaitTime  2
   Mouse Over  css=[data-csi-act='Save'] 
  PO_NewCreationPopUpPage.Select Checkbox in popup    ${StructureMaterialType2}
  DOMreadyByWaitTime  2
   Mouse Over  css=[data-csi-act='Save'] 
  PO_NewCreationPopUpPage.Click on Save Button
  DOMready-PopUpCloses
  Wait Until Element Is Visible   css=[data-csi-automation='plugin-Site-MaterialTypes-views']
  PO_SetupPage.Click On Material types tab
  
Update Compatible types for Second Material 
  PO_MaterialTypePage.Click Compatible Types cell in Material Type table  ${StructureMaterialType2}
  Wait Until Page Contains Element	  //div[@data-csi-automation='edit-Site-MaterialTypes-CompatibleTypes:Child:Config']//div[@class='dijitReset dijitMenuItem']
  Click Element From List  	  //div[@data-csi-automation='edit-Site-MaterialTypes-CompatibleTypes:Child:Config']//div[@class='dijitReset dijitMenuItem']  -1
  DOMready-PopUpAppears
  PO_NewCreationPopUpPage.Wait For PopUp Dialog to Appear  Select Compatible Types
  PO_NewCreationPopUpPage.Select Checkbox in popup    ${StructureMaterialType1}
  DOMreadyByWaitTime  2
  Mouse Over  css=[data-csi-act='Save'] 
  PO_NewCreationPopUpPage.Select Checkbox in popup    ${StructureMaterialType2}
  DOMreadyByWaitTime  2
   Mouse Over  css=[data-csi-act='Save'] 
  PO_NewCreationPopUpPage.Select Checkbox in popup    ${ToolMaterialTypeName}
  DOMreadyByWaitTime  2
   Mouse Over  css=[data-csi-act='Save'] 
  PO_NewCreationPopUpPage.Click on Save Button
  DOMready-PopUpCloses
  Wait Until Element Is Visible   css=[data-csi-automation='plugin-Site-MaterialTypes-views']
  PO_SetupPage.Click On Material types tab    

Verify the Attribute 'Allow Colored Material Create on Placement' Enable and Disable Feature
  Assert for the given element		//td[@class='attrString firstColumn' and text()='${ToolMaterialTypeName}']/following::td[@data-csi-heading='AllowCreateColoredMaterialOnPlacement:Child:Config:0']/div/input[@tabindex='-1']	 
  Assert for the given element		//td[@class='attrString firstColumn' and text()='${StructureMaterialType1}']/following::td[@data-csi-heading='AllowCreateColoredMaterialOnPlacement:Child:Config:0']/div/input[@tabindex='0']

Create StandaloneMaterialType and select Data Sheets   
    #Create New Material Types
    Mouse Over  ${Actions_MaterialType}
    #Step Description
    PO_MaterialTypePage.Wait for New Material Type option
    PO_MaterialTypePage.Click on New Material Type option
    #Check-is the DOM ready
    #DOMready-PopUpAppears
    DOMreadyByWaitTime  3
    PO_MaterialTypePage.Enter the Material Type in the Pop up window    ${MaterialName}
    SuiteSetUp.Test Step    ${R_ClickSaveButton}
    PO_MaterialTypePage.Click on Material Save Button
    #Wait Until Element IS Visible  ${Loc_Release}
    Click Element From List  //td[@class='attrString firstColumn' and text()='${MaterialName}']/following::td[@data-csi-heading='Available:Child:Config:0']/div[1]/input  0
	#Select Required Check box on the Material Type2    ${MaterialName}  
 
Dummy Cancel Action to overcome the failure 
    Mouse Over  ${Actions_MaterialType}
    #Step Description
    PO_MaterialTypePage.Wait for New Material Type option
    PO_MaterialTypePage.Click on New Material Type option
    #Check-is the DOM ready
    #DOMready-PopUpAppears
    DOMreadyByWaitTime  3
    ${Status} =  Run Keyword And Return Status  Click Element From List  //span[text()='Cancel']  -1
    Run Keyword If  '${Status}' == 'FAIL'  Reload Page
           
Update Data Sheets For StandaloneMaterialType
    ${MaterialType1Status} =  Run Keyword and Return Status  Assert MaterialType in Table  ${MaterialName}
    Run Keyword If   '${MaterialType1Status}' == 'False'  Create StandaloneMaterialType and select Data Sheets
    #@{MaterialType1Attributes}  Create List	7	3	6	4	11	5	12	15	13	16	14	17	20	19	22	25	21	24	27	26	23
    @{MaterialType1Attributes}  Create List  7	3	6	4	11	5	12	15	13	16	14	17	20	19	22	25	21	24	27	26	23
    # Step 5: Select the Material type attributes Checkboxes.
    #Step Description
    :FOR  ${I}  IN  @{MaterialType1Attributes}
    #\  @{MatchText} =  Split String  ${I}  '   
    #\  ${MatchValue} =  Get From List  ${MAtchText}  1    
    #\  Run Keyword If  '${MatchValue}' == 'MaterialBOM'  Select View  simplifiedview
	\  Run Keyword If  ${I} == 12  Select View  simplifiedview
    \  DOMreadyByWaitTime    2
    \  ${Status} =  Run Keyword And Return Status  Select Required Check box on the Material Type2    ${MaterialName}  ${I}
    \  Run Keyword If  '${Status}' == 'FAIL'  Dummy Cancel Action to overcome the failure
    \  Run Keyword If  '${Status}' == 'FAIL'  Select Required Check box on the Material Type2    ${MaterialName}  ${I}

DataSetup For Material Types
  ${This_DataProvider} =  Data_Provider.DataProvider  Create Materials Types and select Data Sheets
  Set Test Variable  ${This_DataProvider}
  Set Test Variable  ${MaterialName}  Trial MaterialType 001
  Set Test Variable  ${StructureMaterialType2}  Trial MaterialType Usage Not Standalone 001
  Set Test Variable  ${ToolMaterialTypeName}  Trial MaterialType Has-Color False 001
  #Set Test Variable   ${MaterialName}	
  #Set Test Variable   ${StructureMaterialType2}
  #Set Test Variable   ${ToolMaterialTypeName}
  
Navigate To Y Section
  :FOR  ${i}  IN RANGE  0  10
  \  ${Value} =  Get Element Attribute  ${HSliderPosition}@aria-valuenow
  \  Run Keyword IF  ${value} != 27  Click Element From List  css=div.dijitSliderIncrementIconH>span  -1
  #//div[@aria-valuenow='27']
  
Navigate To X Section
  :FOR  ${i}  IN RANGE  0  10
  \  ${Value} =  Get Element Attribute  ${HSliderPosition}@aria-valuenow
  \  Run Keyword IF  ${value} != 18  Click Element From List     css=div.dijitSliderDecrementIconH>span  -1

Create CustomView
    Click Element   css=span[data-csi-automation='plugin-Site-MaterialTypes-views'] >span .dijitButtonText
    Wait Until Element Is Visible  //span[@class='dijitDialogTitle' and text()='Custom Views']
	Click Element  ${Loc_DefaultCustomView}
    Click Element		//*[@title='Copy custom view']
	Clear Element Text  ${Loc_SecurityRoleName}
	Input Text  ${Loc_SecurityRoleName}  simplifiedview
	#@{views} =  Create List  AttributesMap  TDSMap  DefaultPlacementQuoteToHighestCost  DefaultHasSeasonAvailability  DefaultColorSizeAvailability  HasSize  AllowCreateColoredMaterialOnPlacement  HasColor  Available
	@{views} =  Create List  Available    HasColor    AllowCreateColoredMaterialOnPlacement    HasSize    DefaultColorSizeAvailability  DefaultPlacementQuoteToHighestCost  MaterialUsage  CompatibleTypes
	Click Element  //option[@value='Published::0']
	:FOR  ${view}  IN  @{views}
	\    Click Element  //option[@value='${view}:Child:Config:0']
	\    Click Element  //span[text()='< Remove']
	\    DOMreadyByWaitTime    2
	Click on Custom View Save Button
	DOMready-PopUpCloses
	Wait Until Custom View TextArea Is Enabled  Site-MaterialTypes
	Reload
	DOMready-TabLoads

Select View
    [Arguments]  ${CVText}
    Click Element  css=[data-csi-automation='plugin-Site-MaterialTypes-views'] div.dijitDownArrowButton
    Wait Until Page Contains Element  //div[contains(@class,'csi-custom-view-popupPopup') and @role='region']//div[contains(@id,'csi-custom-view-popup')]//div[text()='${CVText}']
    Click Element  //div[contains(@class,'csi-custom-view-popupPopup') and @role='region']//div[contains(@id,'csi-custom-view-popup')]//div[text()='${CVText}']
    Wait Until Custom View TextArea Is Enabled  Site-MaterialTypes	 
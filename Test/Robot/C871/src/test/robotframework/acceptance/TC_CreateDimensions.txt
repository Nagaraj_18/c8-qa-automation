*** Settings ***
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt



Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_SpecificationPage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../Locators/Loc_SpecificationSizeChartPage.txt


*** Test Cases ***

Specification-01086
#Create Dimensions
	[tags]  Sanity_DataLoad
	#Data provider: It reads the data from the excel sheet for the test case specified. 
	${This_DataProvider} =  Data_Provider.DataProvider  Create Dimensions
	Set Test Variable  ${This_DataProvider}
	
    ## Commented area
    # Step 1: Open the Application in the browser.
    #Step Description
    #SuiteSetUp.Test Step    ${R_Open_Browser}
    SuiteSetUp.Open Browser To Login Page
    #Assertion
    #Assertions.Assert the given Title of the Page    Login - Centric PLM
    
    # Step 2: Enter the login Credentials and Submit.
    #Step Description
    #SuiteSetUp.Test Step    ${R_Enter_Credintails}
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    #SuiteSetUp.Test Step    ${R_Click_Login}
    PO_LoginPage.Submit Credentials
    #Wait.DOMready
    #DOMready-TabLoads
    #Check-is the DOM ready
    ## Un Commented area
    #Reload
    Wait.DOMready
    DOMready-TabLoads
    ##
    Click on Home Icon
    Wait.DOMready
    DOMready-TabLoads
    # Step : Navigate to Size Range Tab.
    PO_HomePage.Click On Specification tab 
    DOMready-TabLoads
    Click Element  ${Loc_SizeChartTab}
    DOMready-TabLoads  
    # Step : Create Dimension
    ${DimensionNameCollection} =  GetDataProviderColumnValue  Data_DimensionName
    ${DimensionName1} =  DataProviderSplitterForMultipleValues  ${DimensionNameCollection}  1
    ${DimensionName2} =  DataProviderSplitterForMultipleValues  ${DimensionNameCollection}  2
    ${DimensionName3} =  DataProviderSplitterForMultipleValues  ${DimensionNameCollection}  3
    Mouse Over  ${Action_CreateDimension}
   	#Wait Until Element Is Visible  ${Loc_NewDimension}
    Click Element  ${Action_CreateDimension}
    PO_SetupPage.Verify Dialog Window  New Dimension 
    Input Text  ${Loc_FieldNewDimensionName}  ${DimensionName1}    
    Wait.DOMreadyByWaitTime  2
    PO_NewCreationPopUpPage.Click on Save and New Button
    Wait.DOMreadyByWaitTime  4
    Input Text  ${Loc_FieldNewDimensionName}  ${DimensionName2} 
    Wait.DOMreadyByWaitTime  2
    PO_NewCreationPopUpPage.Click on Save and New Button
    Wait.DOMreadyByWaitTime  4
    Input Text  ${Loc_FieldNewDimensionName}  ${DimensionName3} 
    Wait.DOMreadyByWaitTime  2
    PO_NewCreationPopUpPage.Click on Save Button
    Wait.DOMreadyByWaitTime  4
    #Unselecting the Active Checkbox for the last element 
    Click Element  xpath=//*[@class='browse' and text()='${DimensionName3}']/following::td//input[1][@class='dijitReset dijitCheckBoxInput']
    Wait.DOMreadyByWaitTime  5
         
    # Step : Close the Browser Instance.
    #SuiteSetUp.Test Step    ${R_Close_Browser}
    [Teardown]    Close Browser

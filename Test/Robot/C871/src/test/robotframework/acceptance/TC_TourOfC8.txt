*** Settings ***
Documentation     A read-only tour of Centric 8
Resource          ../PageObjects/C8PageResource.txt
Resource          ../PageObjects/PLM_resource.txt
Resource          ../UtilityHelper/SuiteSetUp.txt  
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt

Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../Locators/Loc_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
*** Variables ***

*** Test Cases ***
	
Login   
	[Tags]  Testx
    SuiteSetUp.Open Browser To Login Page 
    PLM_resource.Input Username    Administrator
    PLM_resource.Input Password    centric8
    DOMreadyByWaitTime    5
    PO_LoginPage.Submit Credentials
    DOMready

Test: Go To Home In Toolbar    
    Run Keyword And Return Status  Wait Until Element Is Not Visible  //img[@src='Images/logo_loader.gif']  10
    Go To Home In Toolbar
    DOMready

Test: Go To My Home
    Go To My Home
    DOMready

Test: Go To Style
    Go To Style
    DOMready

Test: On Home Go To Style
    On Home Go To Style
    DOMready

Test: Go To Style-Seasons
    Go To Style-Seasons
    DOMready

Test: On Style Go To Seasons
    On Style Go To Seasons
    DOMready
Test: Go To Style-Styles
    Go To Style-Styles
    DOMready

Test: On Style Go To Styles
    On Style Go To Styles
    DOMready

Test: Go To Style-Style Gallery
    Go To Style-Style Gallery
    DOMready

Test: On Style Go To Style Gallery
    On Style Go To Style Gallery
    DOMready

Test: Go To Style-Colorways
    Go To Style-Colorways
    DOMready

Test: On Style Go To Colorways
    On Style Go To Colorways
    DOMready

Test: Go To Style-Style Samples
    Go To Style-Style Samples
    DOMready

Test: On Style Go To Style Samples
    On Style Go To Style Samples
    DOMready

Test: Go To Style-Sample Storage
    Go To Style-Sample Storage
    DOMready

Test: On Style Go To Sample Storage
    On Style Go To Sample Storage
    DOMready

Test: Go To Style-Style SKUs
    Go To Style-Style SKUs
    DOMready

Test: On Style Go To Style SKUs
    On Style Go To Style SKUs
    DOMready

Test: Go To Style-Competitive Styles
    Go To Style-Competitive Styles

Test: On Style Go To Competitive Styles
    On Style Go To Competitive Styles

Test: Go To Style-Supplier Requests
    Go To Style-Supplier Requests

Test: On Style Go To Supplier Requests
    On Style Go To Supplier Requests

Test: Go To Material
    Go To Material

Test: On Home Go To Material
    On Home Go To Material

Test: Go To Material-Material
    Go To Material-Material

Test: On Material Go To Material
    On Material Go To Material

Test: Go To Material-Material Security Groups
    Go To Material-Material Security Groups

Test: On Material Go To Material Security Groups
    On Material Go To Material Security Groups

Test: Go To Material-Colored Material
    Go To Material-Colored Material

Test: On Material Go To Colored Material
    On Material Go To Colored Material

Test: Go To Material-Material Samples
    Go To Material-Material Samples

Test: On Material Go To Material Samples
    On Material Go To Material Samples

Test: Go To Material-Material SKUs
    Go To Material-Material SKUs

Test: On Material Go To Material SKUs
    On Material Go To Material SKUs

Test: Go To Material-Sourcing
    Go To Material-Sourcing

Test: On Material Go To Sourcing
    On Material Go To Sourcing

Test: Go To Material-Care Label
    Go To Material-Care Label

Test: On Material Go To Care Label
    On Material Go To Care Label

Test: Go To Material-Composition
    Go To Material-Composition

Test: On Material Go To Composition
    On Material Go To Composition

Test: Go To Material-Placement
    Go To Material-Placement

Test: On Material Go To Placement
    On Material Go To Placement

Test: Go To Material-Product Symbols
    Go To Material-Product Symbols

Test: On Material Go To Product Symbols
    On Material Go To Product Symbols

Test: Go To Material-Language
    Go To Material-Language

Test: On Material Go To Language
    On Material Go To Language

Test: Go To Material-Product Structure
    Go To Material-Product Structure

Test: On Material Go To Product Structure
    On Material Go To Product Structure

Test: Go To Shape and Theme
    Go To Shape and Theme

Test: On Home Go To Shape and Theme
  On Home Go To Shape and Theme

 Test: Go to Shape and Theme - Shape Masters
   Go to Shape and Theme - Shape Masters
   
Test: On Shape and Theme Go to Shape Masters
    On Shape and Theme Go to Shape Masters   
 
Test: Go to Shape and Theme - Shapes
    Go to Shape and Theme - Shapes
    
Test: On Shape and Theme Go to Shapes
    On Shape and Theme Go to Shapes
    
Test:Go to Shape and Theme - Shape Samples
    Go to Shape and Theme - Shape Samples
    
Test: On Shape and Theme Go to Shape Samples
    On Shape and Theme Go to Shape Samples
    
Test: Go to Shape and Theme - Theme Security Groups
    Go to Shape and Theme - Theme Security Groups

Test: On Shape and Theme Go to Theme Security Groups
    On Shape and Theme Go to Theme Security Groups
    
Test: Go to Shape and Theme - Theme Masters
     Go to Shape and Theme - Theme Masters
     
Test: Go to Shape and Theme - Themes
    Go to Shape and Theme - Themes
    
Test: On Shape and Theme Go to Themes
    On Shape and Theme Go to Themes
         
 Test: Go To Specification
    Go To Specification

Test: On Home Go To Specification
    On Home Go To Specification

Test: Go To Specification-Hierarchy
    Go To Specification-Hierarchy

Test: On Specification Go To Hierarchy
    On Specification Go To Hierarchy

Test: Go To Specification-Classifier
    Go To Specification-Classifier

Test: On Specification Go To Classifier
    On Specification Go To Classifier

Test: Go To Specification-Classifier-Classifiers
    Go To Specification-Classifier-Classifiers

Test: On Classifier Go To Classifiers
    On Classifier Go To Classifiers

Test: Go To Specification-Classifier-Classifier3s
    Go To Specification-Classifier-Classifier3s

Test: On Specification Go To
    On Specification Go To

Test: Go To Specification-Color Specification
    Go To Specification-Color Specification

Test: On Specification Go To Color Specification
    On Specification Go To Color Specification

Test: Go To Specification-Sizes
    Go To Specification-Sizes

Test: On Specification Go To Sizes
    On Specification Go To Sizes

Test: Go To Specification-Size Range
    Go To Specification-Size Range

Test: On Specification Go To Size Range
    On Specification Go To Size Range

Test: Go To Specification-Size Map
    Go To Specification-Size Map

Test: On Specification Go To Size Map
    On Specification Go To Size Map

Test: Go To Specification-Size Chart
    Go To Specification-Size Chart

Test: On Specification Go To Size Chart
    On Specification Go To Size Chart

Test: Go To Specification-Size Chart-Dimensions
    Go To Specification-Size Chart-Dimensions

Test: On Size Chart Go To Dimensions
    On Size Chart Go To Dimensions

Test: Go To Specification-Size Chart-Dimension Gallery
    Go To Specification-Size Chart-Dimension Gallery

Test: On Size Chart Go To Dimension Gallery
    On Size Chart Go To Dimension Gallery

Test: Go To Specification-Size Chart-Increments
    Go To Specification-Size Chart-Increments

Test: On Size Chart Go To Increments
    On Size Chart Go To Increments

Test: Go To Specification-Size Chart-Product Group
    Go To Specification-Size Chart-Product Group

Test: On Size Chart Go To Product Group
    On Size Chart Go To Product Group

Test: Go To Specification-Size Label
    Go To Specification-Size Label

Test: On Specification Go To Size Label
    On Specification Go To Size Label

Test: Go To Specification-Spec Items
    Go To Specification-Spec Items

Test: On Specification Go To Spec Items
    On Specification Go To Spec Items

Test: Go To Specification-Product Alternative Specifications
    Go To Specification-Product Alternative Specifications

Test: On Specification Go To Product Alternative Specifications
    On Specification Go To Product Alternative Specifications

Test: Go To Specification-Libraries
    Go To Specification-Libraries

Test: On Specification Go To Libraries
    On Specification Go To Libraries

Test: Go To Specification-Templates
    Go To Specification-Templates

Test: On Specification Go To Templates
    On Specification Go To Templates

Test: Go To Specification-Templates-Data Package
    Go To Specification-Templates-Data Package

Test: On Templates Go To Data Package
    On Templates Go To Data Package

Test: Go To Specification-Templates-Size Chart
    Go To Specification-Templates-Size Chart

Test: On Templates Go To Size Chart
    On Templates Go To Size Chart

Test: Go To Specification-Templates-Spec Data Sheet
    Go To Specification-Templates-Spec Data Sheet

Test: On Templates Go To Spec Data Sheet
    On Templates Go To Spec Data Sheet

Test: Go To Specification-Templates-Style BOM
    Go To Specification-Templates-Style BOM

Test: On Templates Go To Style BOM
    On Templates Go To Style BOM

Test: Go To Sourcing
    Go To Sourcing

Test: On Home Go To Sourcing
    On Home Go To Sourcing

Test: Go To Sourcing-Supplier PO
    Go To Sourcing-Supplier PO

Test: On Sourcing Go To Supplier PO
    On Sourcing Go To Supplier PO

Test: Go To Sourcing-Supplier PO-Supplier PO
    Go To Sourcing-Supplier PO-Supplier PO

Test: On Supplier PO Go To Supplier PO
    On Supplier PO Go To Supplier PO

Test: Go To Sourcing-Supplier PO-Orders
    Go To Sourcing-Supplier PO-Orders

Test: On Supplier PO Go To Orders
    On Supplier PO Go To Orders

Test: Go To Sourcing-Supplier PO-PO Group
    Go To Sourcing-Supplier PO-PO Group

Test: On Supplier PO Go To PO Group
    On Supplier PO Go To PO Group

Test: Go To Sourcing-Supplier PO-Select Sets
    Go To Sourcing-Supplier PO-Select Sets

Test: On Supplier PO Go To Select Sets
    On Supplier PO Go To Select Sets

Test: Go To Sourcing-Customer PO
    Go To Sourcing-Customer PO

Test: On Sourcing Go To Customer PO
    On Sourcing Go To Customer PO

Test: Go To Sourcing-Customer PO-Customer PO
    Go To Sourcing-Customer PO-Customer PO

Test: On Customer PO Go To Customer PO
    On Customer PO Go To Customer PO

Test: Go To Sourcing-Customer PO-Customer Orders
    Go To Sourcing-Customer PO-Customer Orders

Test: On Customer PO Go To Customer Orders
    On Customer PO Go To Customer Orders

Test: Go To Sourcing-Shipment
    Go To Sourcing-Shipment

Test: On Sourcing Go To Shipment
    On Sourcing Go To Shipment

Test: Go To Sourcing-Shipment-Shipment
    Go To Sourcing-Shipment-Shipment

Test: On Shipment Go To Shipment
    On Shipment Go To Shipment

Test: Go To Sourcing-Shipment-Shipment Order
    Go To Sourcing-Shipment-Shipment Order

Test: On Shipment Go To Shipment Order
    On Shipment Go To Shipment Order

Test: Go To Sourcing-Supplier
    Go To Sourcing-Supplier

Test: On Sourcing Go To Supplier
    On Sourcing Go To Supplier

Test: Go To Sourcing-Factory
    Go To Sourcing-Factory

Test: On Sourcing Go To Factory
    On Sourcing Go To Factory

Test: Go To Sourcing-Customers
    Go To Sourcing-Customers

Test: On Sourcing Go To Customers
    On Sourcing Go To Customers

Test: Go To Sourcing-Setup
    Go To Sourcing-Setup

Test: On Sourcing Go To Setup
    On Sourcing Go To Setup

Test: Go To Sourcing-Setup-Capability
    Go To Sourcing-Setup-Capability

Test: On Setup Go To Capability
    On Setup Go To Capability

Test: Go To Sourcing-Setup-Operation Group
    Go To Sourcing-Setup-Operation Group

Test: On Setup Go To Operation Group
    On Setup Go To Operation Group

Test: Go To Sourcing-Setup-Sub Routings
    Go To Sourcing-Setup-Sub Routings

Test: On Setup Go To Sub Routings
    On Setup Go To Sub Routings

Test: Go To Sourcing-Setup-Country
    Go To Sourcing-Setup-Country

Test: On Setup Go To Country
    On Setup Go To Country

Test: Go To Sourcing-Setup-HTS Code
    Go To Sourcing-Setup-HTS Code

Test: On Setup Go To HTS Code
    On Setup Go To HTS Code

Test: Go To Sourcing-Setup-Shipping Container
    Go To Sourcing-Setup-Shipping Container

Test: On Setup Go To Shipping Container
    On Setup Go To Shipping Container

Test: Go To Sourcing-Setup-Shipping Port
    Go To Sourcing-Setup-Shipping Port

Test: On Setup Go To Shipping Port
    On Setup Go To Shipping Port

Test: Go To Sourcing-Setup-Shipping Rate
    Go To Sourcing-Setup-Shipping Rate

Test: On Setup Go To Shipping Rate
    On Setup Go To Shipping Rate
	
Test: Go To Sourcing-Setup-Templates
    Go To Sourcing-Setup-Templates

Test: On Setup Go To Sourcing Templates
    On Setup Go To Sourcing Templates

Test: Go To Sourcing-Sales Divisions
    Go To Sourcing-Sales Divisions

Test: On Sourcing Go To Sales Divisions
    On Sourcing Go To Sales Divisions

Test: Go To Documents
    Go To Documents

Test: On Home Go To Documents
    On Home Go To Documents

Test: Go To Documents-Documents
    Go To Documents-Documents

Test: On Documents Go To Documents
    On Documents Go To Documents

Test: Go To Documents-All Documents
    Go To Documents-All Documents

Test: On Documents Go To All Documents
    On Documents Go To All Documents

Test: Go To Documents-Contractual Documents
    Go To Documents-Contractual Documents

Test: On Documents Go To Contractual Documents
    On Documents Go To Contractual Documents

Test: Go To Documents-Contractual Document Groups
    Go To Documents-Contractual Document Groups

Test: On Documents Go To Contractual Document Groups
    On Documents Go To Contractual Document Groups

Test: Go To Documents-Supplier Contractual Documents
    Go To Documents-Supplier Contractual Documents

Test: On Documents Go To Supplier Contractual Documents
    On Documents Go To Supplier Contractual Documents

Test: Go To Reports
    Go To Reports

Test: On Home Go To Reports
    On Home Go To Reports

Test: Go To Reports-PDF Queue
    Go To Reports-PDF Queue

Test: On Reports Go To PDF Queue
    On Reports Go To PDF Queue

Test: Go To Reports-PDF Services
    Go To Reports-PDF Services

Test: On Reports Go To PDF Services
    On Reports Go To PDF Services

Test: Go To Reports-Image Queue
    Go To Reports-Image Queue

Test: On Reports Go To Image Queue
    On Reports Go To Image Queue

Test: Go To Reports-Email History
    Go To Reports-Email History

Test: On Reports Go To Email History
    On Reports Go To Email History

Test: Go To Reports-Calendar Queue
    Go To Reports-Calendar Queue

Test: On Reports Go To Calendar Queue
    On Reports Go To Calendar Queue

Test: Go To Reports-Reports
    Go To Reports-Reports

Test: On Reports Go To Reports
    On Reports Go To Reports

Test: Go To Select Sets
    Go To Select Sets

Test: On Home Go To Select Sets
    On Home Go To Select Sets

Test: Go To Business Planning
    Go To Business Planning

Test: On Home Go To Business Planning
    On Home Go To Business Planning

Test: Go To Business Planning-Business Plans
    Go To Business Planning-Business Plans

Test: On Business Planning Go To Business Plans
    On Business Planning Go To Business Plans

Test: Go To Business Planning-Business Categories
    Go To Business Planning-Business Categories

Test: On Business Planning Go To Business Categories
    On Business Planning Go To Business Categories

Test: Go To Calendar
    Go To Calendar

Test: On Home Go To Calendar
    On Home Go To Calendar

Test: Go To Calendar-Calendar Templates
    Go To Calendar-Calendar Templates

Test: On Calendar Go To Calendar Templates
    On Calendar Go To Calendar Templates

Test: Go To Quality
    Go To Quality

Test: On Home Go To Quality
    On Home Go To Quality

Test: Go To Quality-Test Runs
    Go To Quality-Test Runs

Test: On Quality Go To Test Runs
    On Quality Go To Test Runs

Test: Go To Quality-Tests
    Go To Quality-Tests

Test: On Quality Go To Tests
    On Quality Go To Tests

Test: Go To Quality-Setup
    Go To Quality-Setup

Test: On Quality Go To Setup
    On Quality Go To Setup

#Test: Go To Quality-Setup-Test Specs
    #Go To Quality-Setup-Test Specs

#Test: On Setup Go To Test Specs
    #On Setup Go To Test Specs

Test: Go To Quality-Setup-Test Groups
    Go To Quality-Setup-Test Groups
	
Test: On Setup Go To Test Groups
    On Setup Go To Test Groups
	
Test: Go To Quality-Setup-Templates
	Go To Quality-Setup-Templates
	
Test: On Setup Go To Quality Templates
    On Setup Go To Quality Templates

Test: Go To Schedule
    Go To Schedule

Test: On Home Go To Schedule
    On Home Go To Schedule

Test: Go To Schedule-WBS Templates
    Go To Schedule-WBS Templates

Test: On Schedule Go To WBS Templates
    On Schedule Go To WBS Templates

Test: Go To Schedule-Milestone Report
    Go To Schedule-Milestone Report

Test: On Schedule Go To Milestone Report
    On Schedule Go To Milestone Report

Test: Go To Schedule-Deliverables Report
    Go To Schedule-Deliverables Report

Test: On Schedule Go To Deliverables Report
    On Schedule Go To Deliverables Report

Test: Go To Collection Management
    Go To Collection Management

Test: On Home Go To Collection Management
    On Home Go To Collection Management

Test: Go To Collection Management-Marketing Collections
    Go To Collection Management-Marketing Collections

Test: On Collection Management Go To Marketing Collections
    On Collection Management Go To Marketing Collections

Test: Go To Collection Management-Marketing Looks
    Go To Collection Management-Marketing Looks

Test: On Collection Management Go To Marketing Looks
    On Collection Management Go To Marketing Looks

Test: Go To Collection Management-Marketing Tools
    Go To Collection Management-Marketing Tools

Test: On Collection Management Go To Marketing Tools
    On Collection Management Go To Marketing Tools

Test: Go To Collection Management-Sales Markets
    Go To Collection Management-Sales Markets

Test: On Collection Management Go To Sales Markets
    On Collection Management Go To Sales Markets

Test: Go To Collection Management-Sales Order
    Go To Collection Management-Sales Order

Test: On Collection Management Go To Sales Order
    On Collection Management Go To Sales Order

Test: Go To Collection Management-Catalog Configuration
    Go To Collection Management-Catalog Configuration

Test: On Collection Management Go To Catalog Configuration
    On Collection Management Go To Catalog Configuration

Test: Go To Merchandising
    Go To Merchandising

Test: On Home Go To Merchandising
    On Home Go To Merchandising

Test: Go To Setup In Toolbar
    Go To Setup In Toolbar

Test: Go To Configuration
    Go To Configuration

Test: On Setup Go To Configuration
    On Setup Go To Configuration

Test: Go To Configuration-Business Objects
    Go To Configuration-Business Objects

Test: On Configuration Go To Business Objects
    On Configuration Go To Business Objects

Test: Go To Configuration-Enumerations
    Go To Configuration-Enumerations

Test: On Configuration Go To Enumerations
    On Configuration Go To Enumerations

Test: Go To Configuration-Shared Directories
    Go To Configuration-Shared Directories

Test: On Configuration Go To Shared Directories
    On Configuration Go To Shared Directories

Test: Go To Configuration-Copy Templates
    Go To Configuration-Copy Templates

Test: On Configuration Go To Copy Templates
    On Configuration Go To Copy Templates

Test: Go To Configuration-Teams
    Go To Configuration-Teams

Test: On Configuration Go To Teams
    On Configuration Go To Teams

Test: Go To Configuration-Teams-Defaults
    Go To Configuration-Teams-Defaults

Test: On Teams Go To Defaults
    On Teams Go To Defaults

Test: Go To Configuration-Teams-Sales Market
    Go To Configuration-Teams-Sales Market

Test: On Teams Go To Sales Market
    On Teams Go To Sales Market

Test: Go To Configuration-Teams-Customer
    Go To Configuration-Teams-Customer

Test: On Teams Go To Customer
    On Teams Go To Customer

Test: Go To Configuration-Teams-Product Supplier
    Go To Configuration-Teams-Product Supplier

Test: On Teams Go To Product Supplier
    On Teams Go To Product Supplier

Test: Go To Configuration-Teams-Supplier PO
    Go To Configuration-Teams-Supplier PO

Test: On Teams Go To Supplier PO
    On Teams Go To Supplier PO

Test: Go To Teams-Sales Division
    Go To Teams-Sales Division

Test: On Teams Go To Sales Division
    On Teams Go To Sales Division

Test: Go To Configuration-Teams-Supplier
    Go To Configuration-Teams-Supplier

Test: On Teams Go To Supplier
    On Teams Go To Supplier

Test: Go To Configuration-Teams-Supplier Request
    Go To Configuration-Teams-Supplier Request

Test: On Teams Go To Supplier Request
    On Teams Go To Supplier Request
    DOMready

Test: Go To Configuration-Type Configuration
    Go To Configuration-Type Configuration
    DOMready

Test: On Configuration Go To Type Configuration
    On Configuration Go To Type Configuration
    DOMready

Test: Go To Configuration-Type Configuration-Style Types
    Go To Configuration-Type Configuration-Style Types

Test: On Type Configuration Go To Style Types
    On Type Configuration Go To Style Types
    DOMready

Test: Go To Configuration-Type Configuration-Style Types-Style Type Attributes
    Go To Configuration-Type Configuration-Style Types-Style Type Attributes

Test: On Style Types Go To Style Type Attributes
    On Style Types Go To Style Type Attributes

Test: Go To Configuration-Type Configuration-Style Types-Style Type Data Sheets
    Go To Configuration-Type Configuration-Style Types-Style Type Data Sheets

Test: On Style Types Go To Style Type Data Sheets
    On Style Types Go To Style Type Data Sheets
    DOMready

Test: Go To Configuration-Type Configuration-Material Types
    Go To Configuration-Type Configuration-Material Types

Test: On Type Configuration Go To Material Types
    On Type Configuration Go To Material Types
    DOMready

Test: Go To Configuration-Type Configuration-Select Set Types
    Go To Configuration-Type Configuration-Select Set Types
    DOMready

Test: On Type Configuration Go To Select Set Types
    On Type Configuration Go To Select Set Types
    DOMready

Test: Go To Configuration-Type Configuration-Spec Library Item Types
    Go To Configuration-Type Configuration-Spec Library Item Types
    DOMready

Test: On Type Configuration Go To Spec Library Item Types
    On Type Configuration Go To Spec Library Item Types
    DOMready

Test: Go To Configuration-Type Configuration-Test Types
    Go To Configuration-Type Configuration-Test Types
    DOMready

Test: On Type Configuration Go To Test Types
    On Type Configuration Go To Test Types
    DOMready

Test: Go To Configuration-Type Configuration-Contractual Document Types
    Go To Configuration-Type Configuration-Contractual Document Types
    DOMready

Test: On Type Configuration Go To Contractual Document Types
    On Type Configuration Go To Contractual Document Types
    DOMready

Test: Go To Configuration-Type Configuration-Merchandise Types
    Go To Configuration-Type Configuration-Merchandise Types
    DOMready

Test: On Type Configuration Go To Merchandise Types
	On Type Configuration Go To Merchandise Types
    DOMready

Test: Go To Configuration-Type Configuration-Merchandise Types-Plan Types
    Go To Configuration-Type Configuration-Merchandise Types-Plan Types
    DOMready

Test: On Merchandise Types Go To Plan Types
    On Merchandise Types Go To Plan Types
    DOMready

Test: Go To Configuration-Type Configuration-Merchandise Types-Folder Types
    Go To Configuration-Type Configuration-Merchandise Types-Folder Types
    DOMready

Test: On Merchandise Types Go To Folder Types
   On Merchandise Types Go To Folder Types
    DOMready

Test: Go To Configuration-Type Configuration-Merchandise Types-Collection Types
    Go To Configuration-Type Configuration-Merchandise Types-Collection Types
    DOMready

Test: On Merchandise Types Go To Collection Types
    On Merchandise Types Go To Collection Types
    DOMready

Test: Go To Configuration-Type Configuration-Merchandise Types-Product Types
    Go To Configuration-Type Configuration-Merchandise Types-Product Types
    DOMready

Test: On Merchandise Types Go To Product Types
    On Merchandise Types Go To Product Types
    DOMready

Test: Go To Configuration-Type Configuration-Merchandise Types-Option Types
    Go To Configuration-Type Configuration-Merchandise Types-Option Types
    DOMready

Test: On Merchandise Types Go To Option Types
    On Merchandise Types Go To Option Types
    DOMready

Test: Go To Configuration-Type Configuration-Merchandise Types-Secondary Types
    Go To Configuration-Type Configuration-Merchandise Types-Secondary Types
    DOMready

Test: On Merchandise Types Go To Secondary Types
    On Merchandise Types Go To Secondary Types
    DOMready

Test: Go To Configuration-Type Configuration-BOM Types
    Go To Configuration-Type Configuration-BOM Types

Test: On Type Configuration Go To BOM Types
    On Type Configuration Go To BOM Types
    DOMready

Test: Go To Configuration-Sourcing
    Go To Configuration-Sourcing

Test: On Configuration Go To Sourcing
    On Configuration Go To Sourcing

Test: Go To Configuration-Size Chart Settings
    Go To Configuration-Size Chart Settings

Test: On Configuration Go To Size Chart Settings
    On Configuration Go To Size Chart Settings
    DOMready

Test: Go To Localization
    Go To Localization

Test: On Setup Go To Localization
    On Setup Go To Localization
    DOMready

Test: Go To Localization-Types
    Go To Localization-Types

Test: On Localization Go To Types
    On Localization Go To Types
    DOMready

Test: Go To Types-Attributes
    Go To Types-Attributes

Test: On Types Go To Attributes
    On Types Go To Attributes
    DOMready

Test: Go To Localization-Enumerations
    Go To Localization-Enumerations

Test: On Localization Go To Enumerations
    On Localization Go To Enumerations
    DOMready

Test: Go To Localization-Resources
    Go To Localization-Resources

Test: On Localization Go To Resources
    On Localization Go To Resources
    DOMready

Test: Go To Localization-Actions
	Go To Localization-Actions
	DOMready
Test: On Localization Go To Actions
	On Localization Go To Actions
    DOMready

Test: Go To Localization-Views
    Go To Localization-Views

Test: On Localization Go To Views
    On Localization Go To Views
    DOMready

Test: Go To Localization-View Resources
    Go To Localization-View Resources

Test: On Localization Go To View Resources
    On Localization Go To View Resources
    DOMready

Test: Go To Localization-Export/Import
    Go To Localization-Export/Import

Test: On Localization Go To Export/Import
    On Localization Go To Export/Import
    DOMready

Test: Go To Service
    Go To Service

Test: On Setup Go To Service
    On Setup Go To Service
    DOMready

Test: Go To Service-Rollup
    Go To Service-Rollup

Test: On Service Go To Rollup
    On Service Go To Rollup
    DOMready

Test: Go To Service-Job Service
    Go To Service-Job Service

Test: On Service Go To Job Service
    On Service Go To Job Service
    DOMready

Test: Go To Service-Image/PDF
    Go To Service-Image/PDF

Test: On Service Go To Image/PDF
    On Service Go To Image/PDF
    DOMready

Test: Go To Service-Slicer Stores
    Go To Service-Slicer Stores

Test: On Service Go To Slicer Stores
    On Service Go To Slicer Stores
    DOMready

Test: Go To Service-Export/Import
    Go To Service-Export/Import

Test: On Service Go To Export/Import
    On Service Go To Export/Import
    DOMready

Test: Go To Service-Export/Import-Export Batches
    Go To Service-Export/Import-Export Batches

Test: On Export/Import Go To Export Batches
   On Export/Import Go To Export Batches
    DOMready

Test: Go To Service-Export/Import-Export Jobs
    Go To Service-Export/Import-Export Jobs

Test: On Export/Import Go To Export Jobs
    On Export/Import Go To Export Jobs
    DOMready

Test: Go To Service-Export/Import-Export Coverage
    Go To Service-Export/Import-Export Coverage

Test: On Export/Import Go To Export Coverage
    On Export/Import Go To Export Coverage
    DOMready

Test: Go To Service-Export/Import-Export Coverage-Classes Covered
    Go To Service-Export/Import-Export Coverage-Classes Covered

Test: On Export Coverage Go To Classes Covered
    On Export Coverage Go To Classes Covered
    DOMready

Test: Go To Service-Export/Import-Export Coverage-Classes Not Covered
    Go To Service-Export/Import-Export Coverage-Classes Not Covered

Test: On Export Coverage Go To Classes Not Covered
    On Export Coverage Go To Classes Not Covered
    DOMready

Test: Go To Service-Export/Import-Import Jobs
   Go To Service-Export/Import-Import Jobs

Test: On Export/Import Go To Import Jobs
    On Export/Import Go To Import Jobs
    DOMready

Test: Go To Service-RestAPI
    Go To Service-RestAPI
    
Test: On Service Go To RestAPI
    On Service Go To RestAPI
    DOMready
        
Test: Go To Service-Labels
    Go To Service-Labels

Test: On Service Go To Labels
   On Service Go To Labels
    DOMready

Test: Go To Service-Reports
	Go To Service-Reports

Test: On Service Go To Reports
    On Service Go To Reports
    DOMready

Test: Go To Service-Reports-Report Templates
    Go To Service-Reports-Report Templates

Test: On Reports Go To Report Templates
    On Reports Go To Report Templates
    DOMready

Test: Go To Service-Reports-Report Jobs
    Go To Service-Reports-Report Jobs

Test: On Reports Go To Report Jobs
   On Reports Go To Report Jobs
    DOMready

Test: Go To Service-Reports-Report Server Setup
    Go To Service-Reports-Report Server Setup

Test: On Reports Go To Report Server Setup
    On Reports Go To Report Server Setup
    DOMready

Test: Go To User Management
    Go To User Management

Test: On Setup Go To User Management
   On Setup Go To User Management
    DOMready

Test: Go To User Management-Users
    Go To User Management-Users

Test: On User Management Go To Users
    On User Management Go To Users
    DOMready

Test: Go To User Management-Groups
    Go To User Management-Groups

Test: On User Management Go To Groups
    On User Management Go To Groups
    DOMready

Test: Go To User Management-Roles
    Go To User Management-Roles

Test: On User Management Go To Roles
    On User Management Go To Roles
    DOMready

Test: Go To User Management-Shared Teams
    Go To User Management-Shared Teams

Test: On User Management Go To Shared Teams
    On User Management Go To Shared Teams
    DOMready

# Venkatesh.Y.M. 10-07-2015 : This Test Case is not applicable for Fresh Build, 
#  Data needs to be created in the parent tab to populate this tabs  
#Test: Go To User Management-Shared Teams-Role's Users
#	Go To User Management-Shared Teams-Role's Users

# Venkatesh.Y.M. 10-07-2015: This Test Case is not applicable for Fresh Build, 
# Data needs to be created in the parent tab to populate this tabs
#Test: On Reports Go To Role's Users
#    On Reports Go To Role's Users
#    DOMready

# Venkatesh.Y.M. 10-07-2015: This Test Case is not applicable for Fresh Build, 
# Data needs to be created in the parent tab to populate this tabs
#Test: Go To User Management-Shared Teams-User's Roles
#    Go To User Management-Shared Teams-User's Roles

# Venkatesh.Y.M. 10-07-2015: This Test Case is not applicable for Fresh Build, 
# Data needs to be created in the parent tab to populate this tabs 
#Test: On Reports Go To User's Roles
#    On Reports Go To User's Roles
#    DOMready



Test: Go To User Management-Shared Teams-Security
    Go To User Management-Shared Teams-Security

Test: On Reports Go To Security
    On Reports Go To Security
    DOMready

Test: Go To User Management-Companies
    Go To User Management-Companies

Test: On User Management Go To Companies
    On User Management Go To Companies
    DOMready

# Venkatesh.Y.M. 10-07-2015: Test case is Not valid 
#Test: Go To User Management-Companies-Users
#    Go To User Management-Companies-Users

# Venkatesh.Y.M. 10-07-2015: Test case is Not valid 
#Test: On Companies Go To Users
#    On Companies Go To Users
#    DOMready

Test: Go To User Management-Announcements
    Go To User Management-Announcements

Test: On User Management Go To Announcements
    On User Management Go To Announcements
    DOMready

Test: Go To User Management-Security
    Go To User Management-Security

Test: On User Management Go To Security
    On User Management Go To Security
    DOMready

Test: Go To User Management-Search
    Go To User Management-Search

Test: On User Management Go To Search
    On User Management Go To Search
    DOMready

Test: Go To User Management-Views
    Go To User Management-Views

Test: On User Management Go To Views
    On User Management Go To Views
    DOMready

Test: Go To User Management-Custom Views
    Go To User Management-Custom Views

Test: On User Management Go To Custom Views
    On User Management Go To Custom Views
    DOMready

Test: Go To User Management-LDAP
    Go To User Management-LDAP

Test: On User Management Go To LDAP
    On User Management Go To LDAP
    DOMready

Test: Go To Company
    Go To Company

Test: On Setup Go To Company
    On Setup Go To Company
    DOMready

Test: Go To Company-Logo and Defaults
    Go To Company-Logo and Defaults

Test: On Company Go To Logo and Defaults
    On Company Go To Logo and Defaults
    DOMready

Test: Go To Company-GTIN Setup
    Go To Company-GTIN Setup

Test: On Company Go To GTIN Setup
    On Company Go To GTIN Setup
    DOMready

Test: Go To Company-Currency
    Go To Company-Currency

Test: On Company Go To Currency
    On Company Go To Currency
    DOMready

Test: Go To Company-Currency Tables
    Go To Company-Currency Tables

Test: On Company Go To Currency Tables
    On Company Go To Currency Tables

Test: Go To Company-Notification Templates
    Go To Company-Notification Templates

Test: On Company Go To Notification Templates
    On Company Go To Notification Templates

Test: Go To Companion Applications
    Go To Companion Applications

Test: On Setup Go To Companion Applications
    On Setup Go To Companion Applications

Test: Go To Companion Applications-Adobe PLM Connect
    Go To Companion Applications-Adobe PLM Connect

Test: On Companion Applications Go To Adobe PLM Connect
    On Companion Applications Go To Adobe PLM Connect
    DOMready

Test: Go To Workflow Management
    Go To Workflow Management

Test: On Setup Go To Workflow Management
    On Setup Go To Workflow Management

Test: Go To Workflow Management-Workflows
    Go To Workflow Management-Workflows

Test: On Workflow Management Go To Workflows
    On Workflow Management Go To Workflows

Test: Go To Workflow Management-Business Objects
	Go To Workflow Management-Business Objects

Test: On Workflow Management Go To Business Objects
    On Workflow Management Go To Business Objects
    DOMready

Test: Go To WBS Management
    Go To WBS Management

Test: On Setup Go To WBS Management
    On Setup Go To WBS Management
    DOMready

Test: Go To Manage Images In Toolbar
    Go To Manage Images In Toolbar

Test: Close Manage Images In Toolbar
    Close Manage Images In Toolbar
    DOMready

Test: Go To Role In Toolbar
    Go To Role In Toolbar

Test: Go To User Icon In Toolbar
    Go To User Icon In Toolbar
    DOMready

Test: Go To User
    Go To User

Test: On User Go To User
    On User Go To User
    DOMready

Test: Go To Global Membership
    Go To Global Membership

Test: On User Go To Global Membership
    On User Go To Global Membership

Test: Go To Team Membership
    Go To Team Membership

Test: On User Go To Team Membership
    On User Go To Team Membership
    DOMready

Test: Go To Favorites
    Go To Favorites

Test: On User Go To Favorites
   On User Go To Favorites
   DOMready

Test: Go To Delegation to Me
   Go To Delegation to Me

Test: On User Go To Delegation to Me
    On User Go To Delegation to Me
    DOMready

Test: Click Tag As Favorite In Toolbar
    Click Tag As Favorite In Toolbar
    DOMready

Test: Click Email In Toolbar
	[Tags]  Testx
    Click Email In Toolbar

Test: Close Email In Toolbar
	[Tags]  Testx
    Close Email In Toolbar
    DOMready

#Test: Click Print In Toolbar
	#[Tags]  Test1
    #Click Print In Toolbar
   # DOMready

Test: Click Roll Up Status In Toolbar
	[Tags]  Test1
    Click Roll Up Status In Toolbar
    DOMready

Test: Click Show Help Pane In Toolbar
	[Tags]  Test1
    Click Show Help Pane In Toolbar
    DOMready

Test: Click Update Configuration In Toolbar
	[Tags]  Test1
    Click Update Configuration In Toolbar
    DOMready

Test: Click Export Configuration In Toolbar
	[Tags]  Test1
    Click Export Configuration In Toolbar
    DOMready

Test: Click Import Configuration In Toolbar
	[Tags]  Test1
    Click Import Configuration In Toolbar
    DOMready

# Venkatesh.Y.M. 10-07-2015: As this icon is removed for C8 5.4 version this Test case is not valid
#Test: Click Validate License In Toolbar
    #Click Validate License In Toolbar
    #DOMready


Test: Click Log Out In Toolbar
	[Tags]  Test1
    Click Log Out In Toolbar

  [Teardown]    Close Browser 
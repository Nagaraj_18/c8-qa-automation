*** Settings ***
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt

Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_MaterialTypePage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt

*** Test Cases ***

Material-00220
    #Create Material Composition
	[tags]  Sanity_DataLoad
	#Data provider: It reads the data from the excel sheet for the test case specified. 
	${This_DataProvider} =  Data_Provider.DataProvider  Create Material Composition
	Set Suite Variable  ${This_DataProvider}
	##Un comment 
    # Step 1: Open the Application in the browser.
    #Step Description
    #SuiteSetUp.Test Step    ${R_Open_Browser}
    Open Browser To Login Page
    #Assertion
    #Assertions.Assert the given Title of the Page    Login - Centric PLM
    # Step 2: Enter the login Credentials and Submit.
    #Step Description
    #SuiteSetUp.Test Step    ${R_Enter_Credintails}
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    #SuiteSetUp.Test Step    ${R_Click_Login}
    PO_LoginPage.Submit Credentials
    #DOMready-TabLoads 
    #Check-is the DOM ready
    #Reload
    Wait.DOMready
    DOMready-TabLoads
    Click on Home Icon
    Wait.DOMready
    DOMready-TabLoads
    #Assertion: Asserting for the "Configuration" Tab to be Active
    #PO_SetupPage.Verify That This Tab Is Fronted    My Home
    # Step 3: Navigate to Test Type tab	
    #Step Description 
    PO_HomePage.Click On Material tab
    Wait.DOMready
    DOMready-TabLoads	
    #PO_MaterialTypePage.Click on Material-Composition tab 
    Click Element  css=[data-csi-tab="SizeChartAdmin-CompositionMaterials"]
    Wait.DOMready
    DOMready-TabLoads
    # Step 4: Create Test Type
    #Step Description
    Mouse Over  ${Actions_Composition}
    DOMReadyByWaitTime  2
    Assert for the given Element  ${Loc_NewComposition}
Material-00221
    
    Mouse Over  ${Actions_Composition}
 	PO_SetupPage.Wait for the required New Creation options on mouse over-ActionsDDL	${Loc_NewComposition}    
    PO_SetupPage.Click on required option after mouse over-ActionsDDL	${Loc_NewComposition}
    Wait.DOMready-PopUpAppears
    ${CompositionNameCollection} =  GetDataProviderColumnValue  Data_Composition
    ${CompositionName} =  DataProviderSplitterForMultipleValues  ${CompositionNameCollection}  1 
	PO_NewCreationPopUpPage.Enter the Value in the Default Text box  ${CompositionName}
	PO_NewCreationPopUpPage.Click on Save Button
    DOMready-PopUpCloses
    Assert for the given element  //*[@data-csi-heading='Node Name::0' and text()='${CompositionName}']
    
    Mouse Over  ${Actions_Composition}
 	PO_SetupPage.Wait for the required New Creation options on mouse over-ActionsDDL	${Loc_NewComposition}    
    PO_SetupPage.Click on required option after mouse over-ActionsDDL	${Loc_NewComposition}
    Wait.DOMready-PopUpAppears
    ${CompositionNameCollection} =  GetDataProviderColumnValue  Data_Composition
    ${CompositionName} =  DataProviderSplitterForMultipleValues  ${CompositionNameCollection}  2
	PO_NewCreationPopUpPage.Enter the Value in the Default Text box  ${CompositionName}
	PO_NewCreationPopUpPage.Click on Save Button
    DOMready-PopUpCloses
    Assert for the given element  //*[@data-csi-heading='Node Name::0' and text()='${CompositionName}']
    # Step 5: Close the Browser Instance.
    #Test Step    ${R_Close_Browser}
    [Teardown]    Close Browser

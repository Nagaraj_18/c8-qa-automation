*** Settings ***
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt

Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_StylePage.txt 
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../PageObjects/PO_SourcingPage.txt
Resource          ../PageObjects/PO_SpecificationPage.txt
Resource          ../PageObjects/PO_Calendar.txt
Resource          ../Locators/Loc_StyleSeasonsPage.txt


***Variables***
${Loc_StyleBOMNode-TDS_ToolbarsButton}  css=span[data-csi-automation='plugin-ApparelBOMRevision-TDS-htmlToolbars'] span.csi-toolbar-btn-icon-htmlToolbars
${Loc_StyleBOMNode-TDS_PlacementsSection_CustomViewButton}  css=span[data-csi-automation='plugin-ProductBOMRevision-PartMaterials-views'] span.dijitButtonContents
${Loc_CV_SetToDefaultButton}  //span[@id='prefSetDefault_label']
*** Test Cases ***
Calendar-01706
#Create Style Hierarchy
	[tags]  Sanity_DataLoad  CI_TEST  test  1a
	#Data provider: It reads the data from the excel sheet for the test case specified. 
	${This_DataProvider} =  Data_Provider.DataProvider  Create Style Type Hierarchy
    Set Suite Variable  ${This_DataProvider}
    Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    #Check-is the DOM ready
    #Reload
    Wait.DOMready
    DOMready-TabLoads
    Click on Setup Icon
    Wait.DOMready
    DOMready-TabLoads
    #Assertion: Asserting for the 'Home' Tab to be Active
    #PO_SetupPage.Verify That This Tab Is Fronted    My Home
    Click on Home Icon
    DOMready-TabLoads
    #Assertion: Asserting for the 'Configuration' Tab to be Active
    #PO_SetupPage.Verify That This Tab Is Fronted    My Home
   
    #Step 3: Click on the Home Icon from the top menu.
    #Step Description
    #SuiteSetUp.Test Step  ${R_Click_HomeIcon}
    #PO_SetupPage.Click on Home Icon
    #DOMready-TabLoads
    #Step 4: Navigate to the Season Tab.
    #Step Description
    SuiteSetUp.Test Step  ${R_Click_StyleTab}
    Click Element  ${HomeStylesTab} 
    DOMready-TabLoads
    Click Element  ${Loc_SeasonTab} 
    DOMready-TabLoads  
    ${SeasonName} =  GetDataProviderColumnValue  Data_SeasonName  
    Set Suite Variable  ${SeasonName}
    ${StyleName} =  GetDataProviderColumnValue  Data_StyleName
    Set Suite Variable  ${StyleName}
    #Step : Create a Season
    # Mouse Over  ${Loc_ActionsStyleSeasonsTab}
    Click Element  ${Loc_NewSeasonLink}
    DOMreadyByWaitTime  4

Calendar-01707
    [Tags]  CI_TEST
    PO_StylePage.Enter the Season in the Pop up window  Data_SeasonName
    PO_NewCreationPopUpPage.Click on Save and Go Button  
    DOMreadyByWaitTime  4
    Run Keyword And Return Status  Navigate To Hierarchy Tab  Season
Specification-01717
    [Tags]  CI_TEST
    #Step : Create a Brand
    Set Focus To Element  ${Loc_NewBrandLink}
    # Mouse Over  ${Loc_ActionsSeasons-BrandsTab}
    DOMreadyByWaitTime  2
    Click Element  ${Loc_NewBrandLink}
    DOMreadyByWaitTime  4
Specification-01719
    [Tags]  CI_TEST
    PO_StylePage.Enter the Brand in the Pop up window  Data_BrandName
    PO_NewCreationPopUpPage.Click on Save and Go Button  
    DOMreadyByWaitTime  4
    Run Keyword And Return Status  Navigate To Hierarchy Tab  Category1
Specification-01720   
    [Tags]  CI_TEST     
    #Step : Create a Department
    DOMreadyByWaitTime  2
    Set Focus To Element  ${Loc_NewDepartmentLink}
    # Mouse Over  ${Loc_ActionsSeasons-DepartmentTab}
    Click Element  ${Loc_NewDepartmentLink}
    DOMreadyByWaitTime  4
Specification-01722
    [Tags]  CI_TEST
    PO_StylePage.Enter the Department in the Pop up window  Data_DepartmentName
    SuiteSetUp.Test Step  ${R_ClickSaveAndGoButton}
    PO_NewCreationPopUpPage.Click on Save and Go Button  
    DOMready-PopUpCloses
    Run Keyword And Return Status  Navigate To Hierarchy Tab  Category2
Specification-01723  
    [Tags]  CI_TEST      
    #Step : Create a Collection
    #Step Description
    DOMreadyByWaitTime  2
    Set Focus To Element  ${Loc_NewCollectionsLink}
    # Mouse Over  ${Loc_ActionsSeasons-CollectionsTab}
    Click Element  ${Loc_NewCollectionsLink}
    DOMreadyByWaitTime  4
Specification-01725
    [Tags]  CI_TEST
    PO_StylePage.Enter the Collection in the Pop up window  Data_CollectionName
    SuiteSetUp.Test Step  ${R_ClickSaveAndGoButton}
    PO_NewCreationPopUpPage.Click on Save and Go Button  
    DOMready-PopUpCloses 
    Run Keyword And Return Status  Navigate To Hierarchy Tab  Collection 
Specification-01727
    [Tags]  CI_TEST
    DOMreadyByWaitTime  2
    Set Focus To Element  ${Loc_NewStylesLink}
    # Mouse Over  ${Loc_ActionsSeasons-StylesTab}
    Click Element  ${Loc_NewStylesLink}
    DOMreadyByWaitTime  4
    #PO_StylePage.Click on Style Type DDLSpecification-01728
Specification-01729
    [Tags]  CI_TEST
    Click Element		//*[@data-csi-automation='field-Style-Form-ProductType']/div[1]
    Wait.DOMreadyByWaitTime  2
    PO_StylePage.Select the Style Type for the style  Data_StyleTypeName
    Wait.DOMreadyByWaitTime  2
Specification-01731
    [Tags]  CI_TEST
    PO_StylePage.Enter the Style in the Pop up window  Data_StyleName
    PO_NewCreationPopUpPage.Click on Save Button  
    DOMready-PopUpCloses
    ${NavigationStatus} =  Run Keyword And Return Status  Navigate To Hierarchy Tab  Style    
    Run Keyword If  '${NavigationStatus}'=='True'  Click Element  css=[data-csi-tab='Style-Properties']
    DOMready-TabLoads
Specification-01758
    [Tags]  CI_TEST
    #Step : Select Size Range for Style
    PO_StylePage.Click on Size Range DDL from properties 
    DOMreadyByWaitTime  4
    PO_SourcingPage.Select Requrired value from DDL  Data_SizeRangeName  1
    DOMreadyByWaitTime  3
Specification-01762
    [Tags]  CI_TEST
    Click Element  //a[@class='browse' and text()='${StyleName}']
    DOMready-TabLoads     
    Click Element  ${Loc_StyleNode-StyleTab}
    DOMready-TabLoads
    #Step : Create ColorWays for the Style
    Click Element  ${Loc_TabStyleProductColors}
    DOMready-TabLoads    
    DOMreadyByWaitTime  2
    Click Element  ${Loc_ActionsStylesColorwaysTab}
    DOMreadyByWaitTime  2
    #Assert for the Given Element  ${Loc_NewColorwaysLink}
    #Assert for the Given Element  ${Loc_NewFromColorSpecificationLink}
Specification-01765
    [Tags]  CI_TEST
    #Click Element  ${Loc_ActionsStylesColorwaysTab}
    DOMreadyByWaitTime  4
    ${ColorWayNameCollection} =  GetDataProviderColumnValue  Data_ColorWayName
    Set Suite Variable  ${ColorWayNameCollection}
    ${ColorWayName1} =  DataProviderSplitterForMultipleValues  ${ColorWayNameCollection}  1
    Set Suite Variable  ${ColorWayName1}
    PO_NewCreationPopUpPage.Enter the Value in the Default Text box  ${ColorWayName1} 
Specification-01767
    [Tags]  CI_TEST
	${ColorNameCollection} =  GetDataProviderColumnValue  Data_ColorSpecificationName
    Set Suite Variable  ${ColorNameCollection}
    ${ColorName1} =  DataProviderSplitterForMultipleValues  ${ColorNameCollection}  1
    Set Suite Variable  ${ColorName1}
    PO_NewCreationPopUpPage.Click on the DDL button  field-Colorway-Form-ColorSpecification
    PO_NewCreationPopUpPage.Select Value from the DDL  ${ColorName1}
    PO_NewCreationPopUpPage.Click on Save Button  
Specification-01763
    [Tags]  CI_TEST  1a 
    # Goto  http://win16sql17-sk:8080/WebAccess/home.html#URL=C4849&RURL=&Tab=__Base__&Tab1=ProductColors
    # Wait For Release Info
    DOMreadyByWaitTime  2
    Click Element  ${Loc_ActionsStylesColorwaysTab} .dijitArrowButton
    Wait Until Element Is Visible  ${Loc_NewFromColorSpecificationLink}
    Click Element From List  ${Loc_NewFromColorSpecificationLink}  -1
Specification-01764
    [Tags]  CI_TEST  
    DOMreadyByWaitTime  4
    ${ColorNameCollection} =  GetDataProviderColumnValue  Data_ColorSpecificationName
    Set Suite Variable  ${ColorNameCollection}
    ${ColorName2} =  DataProviderSplitterForMultipleValues  ${ColorNameCollection}  2
    Set Suite Variable  ${ColorName2}
    PO_NewCreationPopUpPage.Select Single Matching Checkbox elements in the popup-(div-Element)  ${ColorName2}
    PO_NewCreationPopUpPage.Click on Save Button
    DOMreadyByWaitTime  2
    ${ColorWayNameCollection} =  GetDataProviderColumnValue  Data_ColorWayName
    Set Suite Variable  ${ColorWayNameCollection}
    ${ColorWayName2} =  DataProviderSplitterForMultipleValues  ${ColorWayNameCollection}  2
    Set Suite Variable  ${ColorWayName2}
    PO_NewCreationPopUpPage.Enter the Value in the Default Text box  ${ColorWayName2}
    PO_NewCreationPopUpPage.Click on Save Button
    #Step : Assert the Specification level sub tabs
    DOMreadyByWaitTime  2
    Click Element   ${Loc_TabStyleProductSpec}
    DOMready-TabLoads
    
 Rest of Style Hierarchy   
    [Tags]  test 
    #Step : Update Style BOM
    # Goto  http://win16sql17-sk:8080/WebAccess/home.html#URL=C4849&RURL=&Tab=ProductSpec&Tab1=BOMs
    # Wait For Release Info
    Click Element  ${Loc_TabStyleBOM}
    DOMready-TabLoads
    DOMreadyByWaitTime  2
    Click Element  ${Loc_ActionsSpecificationBOMTab} .dijitArrowButton
    DOMreadyByWaitTime  2
    Click Element  ${Loc_NewStyleBOMFromTemplateLink}
    DOMreadyByWaitTime  4
    ${StyleBomTemplateCollection} =  Data_Provider.GetDataProviderColumnValue  Data_StylBOMTemplateName
	${StyleBomTemplate1} =  DataProviderSplitterForMultipleValues  ${StyleBomTemplateCollection}  1
	Set Suite Variable  ${StyleBomTemplate1}
	PO_NewCreationPopUpPage.Select Single Matching Checkbox elements in the popup-(div-Element)  ${StyleBomTemplate1}
	PO_NewCreationPopUpPage.Click on Save Button
    DOMreadyByWaitTime  4
    Click Element From List  //div/span[@data-csi-act='Save']/descendant::span[@role='button']  -1
    DOMready-PopUpCloses
    DOMreadyByWaitTime  3
    PO_SourcingPage.Click on the Node name in table  ${StyleBomTemplate1}  1
    DOMready-TabLoads
    Click Element  css=[data-csi-tab='ApparelBOMRevision-TDS']
    DOMready-TabLoads
    Create CV to Remove BOM Section Collapse Feature
    #Wait Until Custom View Text Area Is Enabled
    DOMreadyByWaitTime  5
    
    ${PlacementNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_PlacementName
	${PlacementName1} =  DataProviderSplitterForMultipleValues  ${PlacementNameCollection}  1
	Set Suite Variable  ${PlacementName1}
    Click Element  css=[data-csi-tab='ProductBOMRevision-PartMaterials']
	DOMready-TabLoads
	Click Element From List  css=[data-csi-automation='plugin-ProductBOMRevision-PartMaterials-views'] div.dijitArrowButton  1
	Wait Until Element Is Visible  //div[@class='dgrid-scroller']//div[text()='AUT_SimpleView']
	Click Element  //div[@class='dgrid-scroller']//div[text()='AUT_SimpleView']
	DOMready-TabLoads

	Run Keyword And Ignore Error   Click Element From List   //*[@class='attrString firstColumn']//span[@data-csi-act='TableHierarchy']  -1
    DOMreadyByWaitTime  5
	#Execute Javascript  window.document.evaluate('//div[@class="plugin-ProductBOMRevision-PartMaterials-centric:"]', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	DOMreadyByWaitTime  3
	#PO_SourcingPage.Click on the Required Cell in BOM TDS table  ${PlacementName1}  13  2
    ${SizeMapName} =  Data_Provider.GetDataProviderColumnValue  Data_SizeMapName
    Set Suite Variable  ${SizeMapName}
    DOMreadyByWaitTime  3
    #PO_SpecificationPage.Select Requrired value from DDL-StyleBOM  ${SizeMapName}
    DOMreadyByWaitTime  3	
    Click Element  css=[class='csi-breadcrumb-view csi-view csi-view-Breadcrumb'] [data-csi-crumb-type='Style']
    DOMready-TabLoads	
    DOMreadyByWaitTime  2 


    #Step : Update Routing
    Click Element  css=[data-csi-tab="Style-ProductSpec"]
    DOMready-TabLoads
    Click Element  ${Loc_StyleRouting_Tab}	 
    DOMready-TabLoads
    #PO_SetupPage.Mouse Over Actions DDL Button  1
    DOMreadyByWaitTime  2
    Mouse Over  ${Loc_ActionsSpecficationRoutingTab} .dijitArrowButton
    Click Element  ${Loc_ActionsSpecficationRoutingTab} .dijitArrowButton
    DOMreadyByWaitTime  2
    Click Element  ${Loc_NewRoutingFromTemplateLink}
    DOMreadyByWaitTime  4
    ${RoutingTemplateCollection} =  Data_Provider.GetDataProviderColumnValue  Data_RoutingTemplate
	${RoutingTemplate1} =  DataProviderSplitterForMultipleValues  ${RoutingTemplateCollection}  1
	Set Suite Variable  ${RoutingTemplate1}
	PO_NewCreationPopUpPage.Select Single Matching Checkbox elements in the popup-(div-Element)  ${RoutingTemplate1}
	PO_NewCreationPopUpPage.Click on Save Button
    DOMready-PopUpCloses
    Click on the Node name in table  ${RoutingTemplate1}  1
    DOMready-TabLoads
    DOMreadyByWaitTime  3
    PO_NewCreationPopUpPage.Click on the Actions button in TDS
    DOMreadyByWaitTime  2
    DOMready-TabLoads
    PO_NewCreationPopUpPage.Select from Actions DDL in TDS  Freeze
    DOMreadyByWaitTime  2
    Click Element  css=[class='csi-breadcrumb-view csi-view csi-view-Breadcrumb'] [data-csi-crumb-type='Style']
    DOMready-TabLoads
    #Step : Update Spec Data Sheet
    Click Element  css=[data-csi-tab="Style-ProductSpec"]
    DOMready-TabLoads
    Click Element  ${Loc_TabStyleSpecDataSheet}
    DOMready-TabLoads
    DOMreadyByWaitTime  2
    Mouse Over  ${Loc_ActionsSpecificationDataSheetTab}
    Click Element  ${Loc_ActionsSpecificationDataSheetTab} .dijitArrowButton
    DOMreadyByWaitTime  2
    Click Element  ${Loc_NewSpecsDataSheetFromTemplateLink} 
    DOMreadyByWaitTime  4
    ${SpecDataSheetTemplateCollection} =  GetDataProviderColumnValue  Data_SpecItemTemplateName
    ${SpecDataSheetTemplate1} =  DataProviderSplitterForMultipleValues  ${SpecDataSheetTemplateCollection}  1
    Set Suite Variable  ${SpecDataSheetTemplate1}
    PO_NewCreationPopUpPage.Select Single Matching Checkbox elements in the popup-(div-Element)  ${SpecDataSheetTemplate1}
	PO_NewCreationPopUpPage.Click on Save Button
    DOMreadyByWaitTime  4
    PO_NewCreationPopUpPage.Click on Save Button
    DOMready-PopUpCloses
    Click on the Node name in table  ${SpecDataSheetTemplate1}  1
    DOMreadyByWaitTime  3
    DOMready-TabLoads
    PO_NewCreationPopUpPage.Click on the Actions button in TDS
    DOMreadyByWaitTime  2
    PO_NewCreationPopUpPage.Select from Actions DDL in TDS  Approve
    DOMreadyByWaitTime  2
    Click Element  css=[class='csi-breadcrumb-view csi-view csi-view-Breadcrumb'] [data-csi-crumb-type='Style']
    DOMready-TabLoads
    # Step Create Data Package for Style
    Click Element  css=[data-csi-tab="Style-ProductSpec"]
    DOMready-TabLoads
    Click Element  ${Loc_TabProductSummary}
    DOMready-TabLoads
    DOMreadyByWaitTime  2
    Mouse Over  ${Loc_ActionsSpecSummaryTab}
    Click Element  ${Loc_ActionsSpecSummaryTab} .dijitArrowButton
    DOMreadyByWaitTime  2
    #PO_SetupPage.Verify the link under Actions  New Data Package...
    #PO_SetupPage.Verify the link under Actions  New from Data Package Template...
    Click Element  ${Loc_NewDataPackageFromTemplateLink}
    DOMreadyByWaitTime  4
    ${StyleDataPackageTemplateNameCollection} =  GetDataProviderColumnValue  Data_StyleDataPackageTemplateName
	${StyleDataPackageTemplateName1} =  DataProviderSplitterForMultipleValues  ${StyleDataPackageTemplateNameCollection}  1  
	Set Suite Variable  ${StyleDataPackageTemplateName1}
	PO_NewCreationPopUpPage.Select Single Matching Checkbox elements in the popup-(div-Element)  ${StyleDataPackageTemplateName1}
	#PO_NewCreationPopUpPage.Click on Save Button
	Click Element  css=[data-csi-act='Finish']
    DOMready-PopUpCloses
    Assert the links in table view  ${StyleDataPackageTemplateName1}  Generate PDF
    Assert the links in table view  ${StyleDataPackageTemplateName1}  Copy
    Assert the links in table view  ${StyleDataPackageTemplateName1}  Delete
    Click on Actions link in the table view  ${StyleDataPackageTemplateName1}  Generate PDF
    DOMreadyByWaitTime  4
    PO_NewCreationPopUpPage.Click on Save Button
    
    DOMreadyByWaitTime  5
    ${DDLElementList} =  Get Web Elements   xpath=//span[@class='dijitReset dijitInline dijitButtonText' and text()='OK']
    ${ListLength} =  Get Length  ${DDLElementList}
    ${Element} =  Get From List  ${DDLElementList}  -1 
    Click Element   ${Element}
  [Teardown]    Close Browser     
***Keywords***
Create CV to Remove BOM Section Collapse Feature
    Wait Until Element Is Visible    ${Loc_StyleBOMNode-TDS_ToolbarsButton}
    Click Element From List  ${Loc_StyleBOMNode-TDS_ToolbarsButton}  0
    Wait Until Element Is Visible  ${Loc_StyleBOMNode-TDS_PlacementsSection_CustomViewButton}
    Click Element  ${Loc_StyleBOMNode-TDS_PlacementsSection_CustomViewButton}
    DOMready-PopUpAppears
    Wait Until Element Is Visible  ${Loc_SecurityRoleName}
    PO_SetupPage.Click on Default Custom View
    PO_SetupPage.Click on Copy button
    Clear Element Text  ${Loc_SecurityRoleName}
    Input Text  ${Loc_SecurityRoleName}  AUT_SimpleView
    Wait.DOMreadyByWaitTime  2
    Click Element  //span[@class='tabLabel' and text()='Group']
    Click Element  css=div>[id='dgrid_2-row-1'] td.dgrid-cell-actions>div span[title='Remove']
    Wait.DOMreadyByWaitTime  2
    Click Element  ${Loc_CV_SetToDefaultButton}
    PO_SetupPage.Click on Custom View Save Button   
    
Navigate To Hierarchy Tab
  [Arguments]  ${Locator}
  Click Element    css=[data-csi-tab='${Locator}-Base']
  Click Element  css=[data-csi-tab='${Locator}-Hierarchy']    
***comments

--name  Calendar Test Cases
#The below Sets should be executed in the same order
#Configuration/Setup Test Cases 
..\src\test\robotframework\Configuration\Setup-01-TC_CreateStyleTypeAndAttribute.txt
..\src\test\robotframework\Configuration\Setup-02-TC_CreateStyleTypeDataSheet.txt
..\src\test\robotframework\Configuration\Setup-03-TC_CreateMaterialType.txt
..\src\test\robotframework\Configuration\Setup-xx-TC_UpdateConfiguration.txt

#Business Scenarios
..\src\test\robotframework\BusinessScenarios\Setup-15-TC_CreateTemplates.txt
..\src\test\robotframework\BusinessScenarios\Setup-14-CalendarTemplateBS.txt
..\src\test\robotframework\BusinessScenarios\Setup-13-CalendarManualTemplates.txt
..\src\test\robotframework\BusinessScenarios\R-Set-001-Hierarchy Level Scenarios.txt
..\src\test\robotframework\BusinessScenarios\R-Set-002-Style Colorway create with various options.txt
..\src\test\robotframework\BusinessScenarios\R-Set-003-Style Multiple SKU and duplicates creation.txt
..\src\test\robotframework\BusinessScenarios\R-Set-004-Style - Issues - All scenarios.txt
..\src\test\robotframework\BusinessScenarios\R-Set-005-Style SKU creation using Use matrix option.txt
..\src\test\robotframework\BusinessScenarios\R-Set-006-Style Samples scenarios using Supplier, Colors, Sizes.txt
..\src\test\robotframework\BusinessScenarios\R-Set-007-Style Samples scenarios using Supplier, Colors, Sizes using Use Matrix feature.txt
..\src\test\robotframework\BusinessScenarios\R-Set-008-Style Hierarchy - Other features for Style samples.txt

..\src\test\robotframework\BusinessScenarios\R-Set-013-Create Issues at all hierarchy levels - Season, Brand, Department and Collection.txt
..\src\test\robotframework\BusinessScenarios\R-Set-014-Create Season Master Calendar and validate.txt
..\src\test\robotframework\BusinessScenarios\R-Set-015-Create MSG Calendar and validate.txt
..\src\test\robotframework\BusinessScenarios\R-Set-016-Create POG Calendar and validate.txt

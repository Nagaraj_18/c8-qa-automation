*** Variables ***
${Loc_ShapeSecurityGroupTab}  //span[@data-csi-tab='ApparelViews-ShapeSecurityGroups']
${Loc_ShapeMasterTab}  //span[@data-csi-tab='ApparelViews-ShapeMasters']
${Loc_ShapesTab}  //span[@data-csi-tab='ApparelViews-Shapes']

${Loc_ActionsShapeSecurityGroup}  //span[@data-csi-automation='plugin-ApparelViews-ShapeSecurityGroups-root']
${Loc_NewShapeSecurityGroupLink}  //tr[@data-csi-act='NewShapeSecurityGroup']/td[contains(@class,'dijitMenuItemLabel')]
${Loc_DeletePopupDeleteButton}  //span[contains(@class,'csi-primary-btn dijitButton')]
${Loc_DeletePopupCancelButton}  //span[contains(@class,'csi-link-btn dijitButton')]

${Loc_ShapeSecurityGroupShapesTab}  //span[@data-csi-tab="ShapeSecurityGroup-Base"]
${Loc_ShapeSecurityGroupShapesMasterTab}  //span[@data-csi-tab='ShapeSecurityGroup-ShapeMasterTab']
${Loc_ShapeSecurityGroupShapeSamplesTab}  //span[@data-csi-tab='ShapeSecurityGroup-ShapeSamples']
${Loc_ShapeSecurityGroupTeamTab}  //span[@data-csi-tab='CrewHolder-Crew']
${Loc_ShapeSecurityGroupShapesTabPropertySection}  //div[@class='propertyView csi-view csi-view-ShapeSecurityGroup-Properties']
${Loc_ShapeSecurityGroupShapesTabShapeSection}  //div[@class='tableViewRoot csi-view csi-view-ShapeSecurityGroup-SecurityGroupShapes']
${Loc_ShapeSecurityGroupShapesTabShapeActionLink}  //span[@data-csi-automation='plugin-ShapeSecurityGroup-SecurityGroupShapes-root']
${ShapeSecurityGroupShapesTabShapeNewShapeLink}  //tr[@data-csi-act='NewShape']/td[contains(@class,'dijitMenuItemLabel')]
${ShapeSecurityGroupShapesTabShapeMoveFromShapeMasterLink}  //tr[@data-csi-act='MoveFromShapeMaster']/td[contains(@class,'dijitMenuItemLabel')]
${Loc_ShapeFormNodeName}  //div[@data-csi-automation='field-Shape-Form-Node Name']//div/input[contains(@class,'dijitInputInner')]
${Loc_ShapeFormShapeSeasons}  //div[@data-csi-automation='field-Shape-Form-ShapeSeasons']//div/input[contains(@class,'dijitArrowButtonInner')]
${Loc_ShapePropertyView}  //div[@class='propertyView csi-view csi-view-Shape-ShapeProperty']
${Loc_ShapePropertyTab}  //span[@data-csi-tab='Shape-ShapeProperty']
${Loc_ShapeStylesTab}  //span[@data-csi-tab='Shape-ShapeStyles']
${Loc_ShapeSamplesTab}  //span[@data-csi-tab='Shape-ShapeSamples']
${Loc_ShapeDocumentsAndCommentsTab}  //span[@data-csi-tab='DocumentContainer-DocumentsAndComments']
${Loc_ShapeDocumentsActionLink}  //span[@data-csi-automation='plugin-DocumentContainer-Documents-root']
${Loc_ShapeDocumentsNewDocumentLink}  //tr[@data-csi-act='NewDocument']
${Loc_ShapeDocumentsNew3DDocumentLink}  //tr[@data-csi-act='New3DDocument']

${Loc_ShapeReferencedDocumentsActionLink}  //span[@data-csi-automation='plugin-DocumentContainer-ReferencedDocuments-root']
${Loc_ShapeReferencedDocumentsSelectDocumentLink}  //tr[@data-csi-act='AggregateReferencedDocs']
${Loc_ShapeReferencedDocumentsGlobalDocumentLink}  //tr[@data-csi-act='AggregateGlobalDocs']

${Loc_ShapeNewCommentLink}  //span[@data-csi-act='NewComment']

${Loc_ShapeSpecificationTab}  //span[@data-csi-tab='Shape-ShapeSpecificationTab']
${Loc_ShapeSizeChartTab}  //span[@data-csi-tab='Shape-SizeChartLayout']
${Loc_ShapeStyleSizeChartTab}  //span[@data-csi-tab='Shape-StyleSizeCharts']
${Loc_ShapeSizeChartActionLink}  //span[@data-csi-automation='plugin-Shape-SizeCharts-root']
${Loc_ShapesNewSizeChartActionLink}  //tr[@data-csi-act='NewSizeChart']
${Loc_ShapesNewFromSizeChartActionLink}  //tr[@data-csi-act='NewFromSizeChart']
${Loc_ShapesSizeChartNewFromTemplateActionLink}  //tr[@data-csi-act='NewFromTemplate']
${Loc_ShapesSizeChartTableRefreshIcon}  //span[@data-csi-automation='plugin-Shape-SizeCharts-refresh']
# Size chart pages sub tabs.
${Loc_ShapesSizeChartTDSTab}  //span[@data-csi-tab='SizeChartRevision-TDS']
${Loc_ShapesSizeChartCanvasTab}  //span[@data-csi-tab='DataSheetRevision-TDSSwitchboard']
${Loc_ShapesSizeChartPropertiesTab}  //span[@data-csi-tab='SizeChartRevision-Properties']
${Loc_ShapesSizeChartDimensionsTab}  //span[@data-csi-tab='SizeChartRevision-Dimensions']
${Loc_ShapesSizeChartDimensionsGalleryTab}  //span[@data-csi-tab='SizeChartRevision-DimensionCards']
${Loc_ShapesSizeChartReviewsTab}  //span[@data-csi-tab='SizeChart-SizeChartReviews']

${Loc_SizeChartRevisionDimensionsActionLink}  //span[@data-csi-automation='plugin-SizeChartRevision-Dimensions-root']
${Loc_SizeChartNewDimension}  //tr[@data-csi-act='NewSizeChartDimension']
${Loc_SizeChartNewFromDimension}  //tr[@data-csi-act='NewFromApparelDimension']
${Loc_SizeChartNewFromSizeChart}  //tr[@data-csi-act='NewFromSizeChart']
${Loc_SizeChartLineDivider}  //tr[@data-csi-act='NewDSLineDivider']

${Loc_TDSSizeChartRevisionDimensionsActions}  //span[@data-csi-automation='plugin-SizeChartRevision-Dimensions-Actions']//span[contains(@class,'dijitDownArrowButton')]
${Loc_TDSDimensionsActionPasteSizeChart}  //tr[@data-csi-act='PasteSizeChart']/td[contains(@class,'dijitMenuItemLabel')]
${Loc_TDSDimensionsActionHideSizeChart}  //tr[@data-csi-act='BlankSizeChart']/td[contains(@class,'dijitMenuItemLabel')]
${Loc_TDSDimensionsActionUnhideSizeChart}  //tr[@data-csi-act='UnblankSizeChart']/td[contains(@class,'dijitMenuItemLabel')]
${Loc_TDSDimensionsActionRestoreIncrements}  //tr[@data-csi-act='RestoreIncrements']/td[contains(@class,'dijitMenuItemLabel')]
${Loc_TDSDimensionsActionRestoreShrinkage}  //tr[@data-csi-act='RestoreShrinkage']/td[contains(@class,'dijitMenuItemLabel')]
${Loc_TDSDimensionsActionResetPatterns}  //tr[@data-csi-act='ResetPatterns']/td[contains(@class,'dijitMenuItemLabel')]
${Loc_TDSDimensionsActionMassUpdateShapeStyles}  //tr[@data-csi-act='MassUpdateShapeStyles']/td[contains(@class,'dijitMenuItemLabel')]

${Loc_ShapesSizeChartTDSToolbarIcon}  //span[@data-csi-automation='plugin-SizeChartRevision-TDS-htmlToolbars']//span[contains(@class,'csi-toolbar-btn-icon-htmlToolbars')]
${Loc_ShapesSizeChartTDSPropertiesView}  //span[@data-csi-automation='plugin-SizeChartRevision-Properties-views']//span[contains(@class,'dijitButtonText')]

${Loc_SizeChartFormNodeName}  //div[@data-csi-automation='field-SizeChart-Form-Node Name']
${Loc_SizeChartFormSubtype}  //div[@data-csi-automation='field-SizeChart-Form-Subtype']

${Loc_ShapeSecurityGroupShapeSamplesTab}  //span[@data-csi-tab='ShapeSecurityGroup-ShapeSamples']
${Loc_ShapeSecurityGroupShapeSamplesTable}  //div[contains(@class,'csi-view-ShapeSecurityGroup-ShapeSamples')]

${Loc_ShapeSecurityGroupTeamTab}  //span[@data-csi-tab='CrewHolder-Crew']
${Loc_ShapeSecurityGroupSharedTeamTab}  //span[@data-csi-tab='CrewHolder-ParentCrews']
${Loc_ShapeSecurityGroupRolesUserTab}  //span[@data-csi-tab='Crew-RoleUsers']
${Loc_ShapeSecurityGroupUsersRoleTab}  //span[@data-csi-tab='Crew-UserRoles']
${Loc_ShapeSecurityGroupSecurityTab}  //span[@data-csi-tab='Crew-Security']

#Team Tab.
${Loc_ShapeSecurityGroupSharedTeamTable}  //div[contains(@class,'csi-view-CrewHolder-ParentCrews')]
${Loc_ShapeSecurityGroupSharedTeamActionLink}  //span[@data-csi-automation='plugin-CrewHolder-ParentCrews-root']
#Role's User Tab
${Loc_ShapeSecurityGroupRolesUserTable}  //div[contains(@class,'csi-view-Crew-RoleUsers')]
${Loc_ShapeSecurityGroupRolesUserGlobalActionIcon}  //span[@data-csi-automation='plugin-Crew-RoleUsers-CrewShowGlobalAssignments']
#Security Tab
${Loc_ShapeSecurityGroupSecurityTable}  //div[contains(@class,'csi-view-Crew-Security')]
${Loc_ShapeSecurityGroupSecurityGlobalActionIcon}  //span[@data-csi-automation='plugin-Crew-Security-CrewShowGlobalAssignments']
#User's Role Tab
${Loc_ShapeSecurityGroupUsersRoleTable}  //div[contains(@class,'csi-view-Crew-UserRoles')]
${Loc_ShapeSecurityGroupUsersRoleGlobalActionIcon}  //span[@data-csi-automation='plugin-Crew-UserRoles-CrewShowGlobalAssignments']

#Shape's Sample Page
${Loc_ShapeSamplesTableView}  //div[contains(@class,'csi-view-Shape-ShapeSamples')]
${Loc_ShapeSamplesActionLink}  //span[@data-csi-automation='plugin-Shape-ShapeSamples-root']
${Loc_ShapeSamplesNewSampleLink}  //tr[@data-csi-act='NewSample']

${Loc_ShapeMasterPropertySection}  //div[@class='propertyView csi-view csi-view-ShapeMaster-ShapeMasterProperty']
${Loc_ShapeMasterShapeSection}  //div[@class='csi-view-title csi-view-title-ShapeMaster-ShapeMasterShapes']

${Loc_ShapeMasterActionLink}  //span[@data-csi-automation='plugin-ApparelViews-ShapeMasters-root']
${Loc_NewShapeMasterLink}  //tr[@data-csi-act='NewShapeMaster']/td[contains(@class,'dijitMenuItemLabel')]
${Loc_ShapeMastersRefresh}  //span[@data-csi-automation='plugin-ApparelViews-ShapeMasters-refresh']

${Loc_ShapeMastersNodeNameFilter}  //span[@data-csi-automation='filter-ApparelViews-ShapeMasters-Node Name']
${Loc_ShapeMastersNodeNameTextarea}  //input[@data-csi-automation='values-ApparelViews-ShapeMasters-Node Name']

#Sample Popup Elements
${Loc_FormTextArea}  //div/input[contains(@class,'dijitInputInner')]
${Loc_FormDDLArrow}  //div/input[contains(@class,'dijitArrowButtonInner')]
${Loc_SampleFormSupplierNodeName}  //div[@data-csi-automation='field-Sample-Form-Supplier']
${Loc_SampleFormSampleNodeName}  //div[@data-csi-automation='field-Sample-Form-Node Name']
${Loc_SampleFormSampleType}  //div[@data-csi-automation='field-Sample-Form-SampleType']
${Loc_SampleFormSampleStatus}  //div[@data-csi-automation='field-Sample-Form-SampleStatus']
${Loc_SampleFormRequestSampleDimensions}  //div[@data-csi-automation='field-Sample-Form-RequestSampleDimensions']
${Loc_SampleFormRequestSampleQuantity}  //div[@data-csi-automation='field-Sample-Form-RequestSampleQuantity']

${Loc_PopupTitleBar}  //div[@class='dijitDialogTitleBar']
${Loc_PopupSaveGoButton}  //span[@data-csi-act='Save_Go']
${Loc_PopupSaveNewButton}  //span[@data-csi-act='Save_New']
${Loc_PopupSaveButton}  //span[@data-csi-act='Save']
${Loc_PopupCancelButton}  //span[@data-csi-act='Cancel']

${Loc_ShapeSecurityGroupBreadcrumb}  //div[@data-csi-crumb-type="ShapeSecurityGroup"]/a[contains(@class,'dijitInline text browse')]

${Loc_ShapeSecurityGroupRefreshIcon}  //span[@data-csi-automation='plugin-ApparelViews-ShapeSecurityGroups-refresh']
${Loc_ShapeSecurityGroupShapeTBRefreshIcon}  //span[@data-csi-automation='plugin-ShapeSecurityGroup-SecurityGroupShapes-refresh']

${Loc_TableInlineArea}  //div[@class='dijitTooltipContainer']//div/textarea
${Loc_ShapeMastersNodeNameSearchBox}  //div[contains(@class,'dijitTooltipDialogPopup')]//input[@data-csi-automation='values-ApparelViews-ShapeMasters-Node Name']

${Loc_DimensionNodeName}  //div[@data-csi-automation='field-ApparelDimension-Form-Node Name']//div/input[contains(@class,'dijitInputInner')]
${Loc_DimensionDescription}  //div[@data-csi-automation='field-ApparelDimension-Form-Description']//div/input[contains(@class,'dijitInputInner')]
${Loc_SizeRangeDimension1Type}  //div[@data-csi-automation="field-SizeRange-Form-Dimension1Type"]//div/input[contains(@class,'dijitInputInner')]

${Loc_FooterStatusPass}  //span[@class='csi-footer-status'][contains(@style,'background-color: lime')]

${Loc_NewIncrement}  //tr[@data-csi-act='NewIncrement']
${Loc_IncrementNodeName}  //div[@data-csi-automation='field-Increment-Form-Node Name']//div/input[contains(@class,'dijitInputInner')]
${Loc_IncrementDescription}  //div[@data-csi-automation='field-Increment-Form-Description']//div/input[contains(@class,'dijitInputInner')]

${Loc_ComboSearchBox}  //div[contains(@class,'dijitTooltipDialogPopup')]//div/input[contains(@class,'dijitInputInner')]

${Loc_ShapeSizeRange}  //td[@data-csi-act='SizeRange::0']
${Loc_ShapeSizeRangeBaseSize}  //td[@data-csi-act='BaseSize:Child:SizeChartSubSizeRanges/Index:0:0']
${Loc_ShapeSizeRangeIncrement}  //td[@data-csi-act='SubrangeIncrement:Child:SizeChartSubSizeRanges/Index:0:0']



*** Variables ***
${Loc_DataSetupLink}  //span[@title="Data Setup"] 
${Loc_ProductSpecificationSetupLink}  css=[data-csi-act="DataSetupSpecification"]
${Loc_ProductSpecificationLink}  //span[@class='MuiTypography-root MuiTypography-body1 MuiTypography-noWrap' and text()='Product Specification Setup']
${Loc_SizesLink}  //span[@class="MuiTypography-root MuiTypography-body1" and text()="Sizes"]
${Loc_SizeRangesLink}  //span[@class="MuiTypography-root MuiTypography-body1" and text()="Size Ranges"]
${Loc_ColorSpecificationLink}  //span[@class="MuiTypography-root MuiTypography-body1" and text()="Color Specifications"]
${Loc_DataPackageTemplatesLink}  //span[@class="MuiTypography-root MuiTypography-body1" and text()="Data Package Templates"]


${Loc_CalendarAndScheduleSetupLink}  css=[data-csi-act="DataSetupSchedule"]
${Loc_CalendarAndScheduleLink}  //span[text()='data-csi-act="Calendar & Schedule Setup']
${Loc_CalendarTemplatesLink}  //span[@class="MuiTypography-root MuiTypography-body1" and text()="Calendar Templates"]


${Loc_HierarchyAndStyleSetupLink}  css=[data-csi-act="DataSetupHierarchy"]
${Loc_HierarchyAndStyleLink}  //span[text()='data-csi-act="Hierarchy & Style Setup']
${Loc_HierarchyItemTemplatesLink}  //span[@class="MuiTypography-root MuiTypography-body1" and text()="Hierarchy Item Templates"]

${Loc_SizesSetupLink}  //span[@class='MuiTypography-root MuiTypography-body1' and text()='Sizes']
${Loc_SizesRangeSetupLink}  //span[@class='MuiTypography-root MuiTypography-body1' and text()='Size Ranges']
${Loc_ColorSpecificationLink}  //span[@class="MuiTypography-root MuiTypography-body1" and text()="Color Specifications"] 
${Loc_BOMSectionsLink}  //span[@class="MuiTypography-root MuiTypography-body1" and text()="BOM Sections"] 
${Loc_PlacementsLink}  //span[@class="MuiTypography-root MuiTypography-body1" and text()="Placements"] 


${Loc_GeneralSetupLink}  css=[data-csi-act="DataSetupGeneral"]
${Loc_CountriesLink}  //span[@class="MuiTypography-root MuiTypography-body1" and text()="Countries"]  
${Loc_CurrenciesLink}  //span[@class="MuiTypography-root MuiTypography-body1" and text()="Currencies"]
${Loc_CurrencyTableLink}  //span[@class="MuiTypography-root MuiTypography-body1" and text()="Currency Tables"]

${Loc_SourcingSetup}  css=[data-csi-act="DataSetupSourcing"]
${Loc_ShippingPortLink}  //span[@class="MuiTypography-root MuiTypography-body1" and text()="Shipping Ports"]   
${Loc_HTSCodeLink}  //span[@class="MuiTypography-root MuiTypography-body1" and text()="HTS Codes"]   
${Loc_SQTLink}  //span[@class="MuiTypography-root MuiTypography-body1" and text()="Supplier Quote Templates"]   
${Loc_SRTLink}  //span[@class="MuiTypography-root MuiTypography-body1" and text()="Supplier Request Templates"]   


${Loc_StyleTemplatesLink}  //span[@class="MuiTypography-root MuiTypography-body1" and text()="Style Templates"]
${Loc_ColorSpecificationTab}  css=[data-csi-tab='ApparelViews-ColorSpecifications']
${Loc_SizesTab}  css=[data-csi-tab='ApparelViews-Sizes']
${Loc_Specification_SizesMapTab}  css=[data-csi-tab='ApparelViews-SizeMaps']
${Loc_HierarchyTab}  css=[data-csi-tab='ApparelViews-StructItemLayout']
${Loc_NewColorSpecification}  css=[data-csi-act='NewColorSpecification']
${Actions_Size}  css=[data-csi-automation='plugin-ApparelViews-Sizes-ToolbarNewActions']

${Loc_NewSizeSpecification}   css=[data-csi-act='NewProductSize']
${Loc_SizeRangeTab}  css=[data-csi-tab='ApparelViews-SizeRanges']
${Loc_SizeChartTab}  css=[data-csi-tab='SizeChartAdmin-SizeChartSpecs']
${Action_CreateDimension}  css=[data-csi-automation='plugin-SizeChartAdmin-Dimensions-ToolbarNewActions']
${Loc_Template_SizeChartTab}  css=[data-csi-tab='SizeChartAdmin-SizeChartTemplates']
${Loc_SizeLabelTab}  css=[data-csi-tab='SizeChartAdmin-SizeLabels']
${Loc_SizeLabelTab_Sub}   css=[data-csi-tab='SizeLabel-Base']
${Loc_SpecItemTab}  css=[data-csi-tab='ApparelViews-SpecificationItemDefinitions']
${Loc_IncrementsTab}  css=[data-csi-tab='SizeChartAdmin-Increments']
${Loc_IncrementTab}  css=[data-csi-tab='Increment-Layout']
# DMG could not find the TDS tab. Kripya Team can you find this and convert
# to CSS? 
${Loc_TDSTab}  css=[data-csi-tab='SizeChartRevision-TDS']
${Loc_ColorWaysTab}  css=[data-csi-tab='ApparelViews-Colorways']
${Loc_ProductGroupTab}  css=[data-csi-tab='SizeChartAdmin-ProductGroups']
${Loc_SubProductGroupTab}   css=[data-csi-tab='ProductGroup-Layout']
${Actions_SizeRange}  css=[data-csi-automation='plugin-ApparelViews-SizeRanges-ToolbarNewActions']
${Loc_NewSizeRange}  css=[data-csi-act='NewSizeRange']
${Loc_SizeMapsTab}  css=[data-csi-tab='SizeRange-SizeMaps']
${Loc_SizeMapTab}  css=[data-csi-tab='SizeMap-SizeMap']
${Loc_NewSizeMap}  css=[data-csi-act='NewSizeMap']
${Loc_NewDimension}  css=[data-csi-act='NewApparelDimension']
${Loc_QualityTab}	css=[data-csi-tab='ProductColor-Quality']
${Loc_DimensionTab}  css=[data-csi-tab='SizeChartAdmin-Dimensions']
${Loc_DimensionActionLink}  css=[data-csi-automation='plugin-SizeChartAdmin-Dimensions-root']

${Action_NewIncrement}  css=[data-csi-automation='plugin-SizeChartAdmin-Increments-ToolbarNewActions']
${Loc_NewIncrements}  css=[data-csi-act='NewIncrement']
${Loc_NewProductGroups}  css=[data-csi-automation='plugin-SizeChartAdmin-ProductGroups-ToolbarNewActions']
${Loc_NewSizeLabel}  //table[@data-csi-automation='plugin-SizeChartAdmin-SizeLabels-ToolbarNewActions']
${Loc_NewSpecItem}  css=[data-csi-act='NewSpecLibraryItem']
${Loc_NewStyleTemplate}  css=[data-csi-act='NewStyleAttributes']
${Loc_NewHierarchyItemTemplate}  css=[data-csi-act='NewStructItemNameCode']
${Loc_NewDataPackageTemplate}  css=[data-csi-act='NewDataPackageTemplate']
${Loc_NewProductAlternativeSpecification}  //table[@data-csi-automation='plugin-ApparelViews-ProductAlternativeSpecifications-ToolbarNewActions']//div[contains(@class,'dijitButtonText')]
${Loc_NewDataSheetTemplate}  css=[data-csi-act='NewDataPackageTemplateSheet']  
${Loc_NewSizeinSizeLabel}  //table[@data-csi-automation='plugin-SizeLabel-Sizes-ToolbarNewActions']
#${Loc_NewSizeinSizeLabel}  css=[data-csi-act='NewProductSize']
${Loc_TemplatesTab}  css=[data-csi-tab='ApparelViews-SpecTemplates']
${Loc_BOMSectionTab}  css=[data-csi-tab='ApparelViews-BOMSectionTemplates']
${Loc_ProductAlternativeSpecificationsTab}  css=[data-csi-tab='ApparelViews-ProductAlternativeSpecifications']
${Loc_DataPackageTab}  css=[data-csi-tab='SizeChartAdmin-DataPackageTemplates']
${Action_SizeChartTemplate}  //table[@data-csi-automation='plugin-SizeChartAdmin-SizeChartTemplates-ToolbarNewActions']//div[contains(@class,'dijitButtonText')]
${Loc_NewSizeChartTemplate}  css=[data-csi-act='NewSizeChart']
${Action_NewDim}  //*[@data-csi-automation='actionRow-SizeChartRevision-Dimensions-root']
${Loc_New}  //table[contains(@data-csi-automation,'SizeChartRevision-Dimensions')][@data-csi-buttonnewactions='true']//div[contains(@class,'dijitButtonText')][text()='New Dimension']
${Loc_NewDivider}  css=[data-csi-act='NewDSLineDivider'] 
${Loc_NewSpecDataSheetTemplate}  css=[data-csi-act='NewSpecDataSheet']
#${Loc_StyleBOM_Tab}  css=[data-csi-tab='Style-BOMs']
${Loc_StyleBOM_Tab}  css=[data-csi-tab='SizeChartAdmin-StyleBOMTemplates'] 
${Loc_MatBOM_Tab}  css=[data-csi-tab='Material-BOMs']
${Loc_NewStyleBOMTemplate_Link}  //table[@data-csi-automation='plugin-SizeChartAdmin-StyleBOMTemplates-ToolbarNewActions']//div[contains(@class,'dijitButtonText')][contains(text(),'New Style BOM Template')]
${Action_NewMaterialBOMItem}  //table[@data-csi-automation='plugin-ProductBOMRevision-PartMaterials-ToolbarNewActionsCreate']//td[contains(@class,'dijitDownArrowButton')]
${Loc_NewOfMaterial_link}  css=[data-csi-act='NewPartMaterialOfMaterial'] .dijitMenuItemLabel
${Loc_NewFromMaterial_link}  css=[data-csi-act='AggregateFromMaterial'] .dijitMenuItemLabel
${Loc_MaterialTab}  css=[data-csi-tab='ApparelViews-Materials']
${Loc_PlacementTab}  css=[data-csi-tab='ApparelViews-AllMaterials']
# DMG could not find the 'New Placement Name...' link. Kripya Team can you find this and convert
# to CSS? 
${Loc_NewPlacement_Link}	//td[@class='dijitReset dijitMenuItemLabel' and (text()='New Placement Name...')]
# DMG could not find the 'Spec Data Sheet' tab. Kripya Team can you find this and convert
# to CSS? 
${Loc_SpecDataSheetTab}  css=[data-csi-tab='ApparelViews-SpecificationDataSheetTemplates']
#${Loc_SpecDataSheetTab}  css=[data-csi-tab='SizeChartAdmin-SpecDSTemplates']
${Loc_NewColorWay_Link}	  css=[data-csi-act='NewColorway']
${Loc_NewFromColorSpec_Link}  css=[data-csi-automation='plugin-Material-ProductColors-ToolbarNewActions'] [data-csi-act='AggregateFromProductColors'] .dijitMenuItemLabel	
${Action_ColoredMaterial}  css=[data-csi-automation='plugin-Material-ProductColors-ToolbarNewActions']
${Loc_NewColoredMaterial_Link}	//table[@data-csi-automation='plugin-Material-ProductColors-ToolbarNewActions']
${Loc_MaterialBOMTab}  css=[data-csi-tab='Material-BOMs']
${Action_NewBOM}  css=[data-csi-automation='plugin-Material-BOMs-ToolbarNewActions']
${Loc_NewMaterialBOM_Link}   //table[@data-csi-automation='plugin-Material-BOMs-ToolbarNewActions']
${Action_MaterialRouting}  css=[data-csi-automation='plugin-Material-Routing-ToolbarNewActions']
${Loc_NewFromTemplates_Link}  css=[data-csi-act='NewFromRoutingTemplates'] .dijitMenuItemLabel
${Loc_NewFromTemplate_Link}  //table[@data-csi-automation='plugin-Material-Routing-ToolbarNewActions']//td[contains(@class,'dijitMenuItemLabel')][contains(text(),'New from Template')]
${Loc_TestRunNewFromTemplate_Link}  css=[data-csi-act='NewFromTemplate'] .dijitMenuItemLabel
${Loc_NewFromRouting_Link}    //table[@data-csi-automation="plugin-Material-Routing-ToolbarNewActions"]//td[contains(text(),'New from Routing...')]
${Action_NewDataPackage}  //table[@data-csi-automation='plugin-Material-Routing-ToolbarNewActions']//td[contains(@class,'dijitMenuItemLabel')][contains(text(),'New from Template')]
${Action_NewDataPackage1}  css=[data-csi-automation='plugin-Product-DataPackages-ToolbarNewActions']
${Loc_NewFromDataPackageTemplate_Link}   //table[@data-csi-automation='plugin-Product-DataPackages-ToolbarNewActions']//td[contains(@class,'dijitMenuItemLabel')][contains(text(),'New from Data Package Template')]
${Loc_StyleRouting_Tab}  css=[data-csi-tab='Style-Routing']
${Loc_StyleSpecs_Tab}   css=[data-csi-tab='Style-SpecDataSheets']
${Loc_Specification-LibraryTab}  css=[data-csi-tab='SizeChartAdmin-Libraries']
${Loc_ActionsCareLabelLibrary}  css=[data-csi-automation='plugin-LibCareSymbol-LibElements-ToolbarNewActions']
${Loc_NewColorSpecificationLibrary_link}  css=[data-csi-act='NewLibColorSpecification']
${Loc_NewSizeLabelLibrary_link}  css=[data-csi-automation='plugin-SizeChartAdmin-Libraries-ToolbarNewActionsCreate'] [data-csi-act='NewLibSizeLabel'] .dijitMenuItemLabel
${Loc_NewCarelabelLibrary_arrowlink}  css=[data-csi-automation='plugin-SizeChartAdmin-Libraries-ToolbarNewActionsCreate'] td.dijitArrowButton
${Loc_NewMaterialLibrary_link}  css=[data-csi-automation='plugin-SizeChartAdmin-Libraries-ToolbarNewActionsCreate'] [data-csi-act='NewLibMaterial'] .dijitMenuItemLabel 
${Loc_NewSizeLabelLibrary_link1}  css=[ata-csi-action='NewLibSizeLabel']
${Loc_ElementsTab}  css=[data-csi-tab='LibMaterial-LibElements']
#${Loc_NewCareLabel_link}  css=[data-csi-act='NewCareSymbol']
${Loc_NewCareLabel_link}   css=[data-csi-automation='plugin-LibCareSymbol-LibElements-ToolbarNewActions']
# ${Loc_NewColorSpecificationLibrary_link}  css=[data-csi-act='NewLibColorSpecification']
${Loc_NewColorSpecification_link}  css=[data-csi-act='NewColorSpecification']
${Loc_LibNewMaterial_link}  //table[@data-csi-automation='plugin-LibMaterial-LibElements-ToolbarNewActions']//div[contains(@class,'dijitButtonText')][contains(text(),'New Material')]
${Loc_SelectElementLink}  //table[@data-csi-automation='plugin-LibMaterial-LibElements-ToolbarNewActions']//td[contains(@class,'dijitMenuItemLabel')][contains(text(),'Select Elements')]
${Loc_NewMaterialActionLinks}  css=[data-csi-automation='plugin-LibMaterial-LibElements-ToolbarNewActions']
${Loc_ActionMaterialLibElements}  css=[data-csi-automation='plugin-LibMaterial-LibElements-ToolbarNewActions']
${Loc_NewMaterial_link}  css=[data-csi-automation='plugin-ApparelViews-AllMaterials-ToolbarNewActions']
# DMG made two tabs for the following single line of xpath. One is new: Properties Data Sheet.
# ${Loc_PropertiesTab}  //span[@class='tabLabel' and (text()='Properties')] 
${Loc_PropertiesTab}  css=[data-csi-tab='Style-Properties']
${Loc_PropertiesDataSheetTab}  css=[data-csi-tab='Style-PropertiesDataSheets']
${Loc_NewColoredMaterial_link}  css=[data-csi-act='NewColorMaterial']
${Loc_NewfromColorSpecification_link}  //table[@data-csi-automation='plugin-Material-ProductColors-ToolbarNewActions']//td[contains(@class,'dijitMenuItemLabel')][contains(text(),'New from Color Specification')]
${Loc_CanvasTab}  css=[data-csi-tab='DataSheetRevision-TDSSwitchboard']
${Loc_NewTestRun_link}  css=[data-csi-automation='plugin-ProductColor-TestRuns-ToolbarNewActions']
${Loc_NewfromTestRun_link}  css=[data-csi-act='NewFromTestRun'] .dijitMenuItemLabel
${Loc_NewfromTemplate_link}  css=[data-csi-act='NewFromTemplate'] .dijitMenuItemLabel
${Loc_NewStyleBOM_Link}  css=[data-csi-automation='plugin-Style-BOMs-ToolbarNewActions']
#${Loc_NewStyleBOM_Link}	 css=[data-csi-act='NewApparelBOM']
${Loc_StyleSpecification_Tab}  css=[data-csi-tab='Style-ProductSpec']
${Loc_NewRouting_Link}  css=[data-csi-automation='plugin-Style-Routing-ToolbarNewActions']
${Loc_ActionsLibrariesTab}  css=[data-csi-automation='plugin-SizeChartAdmin-Libraries-ToolbarNewActionsCreate']
${Loc_ActionsColorSpecificationMaterialLibrariesTab}  css=span[data-csi-automation='plugin-LibMaterial-LibElements-ToolbarNewActions']
${Loc_NewColorSpecificationLibraryLink}  css=[data-csi-act='NewLibColorSpecification'] .dijitMenuItemLabel
${Loc_Home-Specification-ColorSpecificationTab}   css=[data-csi-tab='ApparelViews-ColorSpecificationsTab']
${Loc_ActionsColorSpecificationTab}  css=[data-csi-automation='plugin-ApparelViews-ColorSpecifications-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions 
${Loc_NewColorSpecificationLink}  css=[data-csi-act='NewColorSpecification'] .dijitMenuItemLabel
${Loc_ActionsColorSpecificationElementsTab}  css=[data-csi-automation='plugin-LibColorSpecification-LibElements-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
${Loc_ActionsHierarchyItemTemplates}  css=[data-csi-automation='plugin-ApparelViews-StructItemNameCodes-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
${Loc_NewHierarchyItemTemplateOption}  //table[@data-csi-automation='plugin-ApparelViews-StructItemNameCodes-ToolbarNewActions']
#${Loc_ActionsHierarchyStyleTemplates}  css=[data-csi-automation='plugin-SiteLibStyleTemplate-StyleTemplates-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
${Loc_ActionsHierarchyStyleTemplates}  css=[data-csi-automation='plugin-ApparelViews-StyleTemplates-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
#${Loc_NewHierarchyStyleTemplateOption}  css=[data-csi-act='NewStyleAttributes'] .dijitMenuItemLabel
${Loc_NewHierarchyStyleTemplateOption}  //*[@data-csi-automation='plugin-ApparelViews-StyleTemplates-ToolbarNewActions']
${Loc_ActionsDataPackageTab}  css=[data-csi-automation='plugin-SizeChartAdmin-DataPackageTemplates-ToolbarNewActions'] 
${Loc_NewDataPackageTemplateLink}  //table[@data-csi-automation='plugin-SizeChartAdmin-DataPackageTemplates-ToolbarNewActions']//div[contains(@class,'dijitButtonText')]
${Loc_ActionsDataSheetTemplatesTab}  css=[data-csi-automation='plugin-DataPackageTemplate-DataSheets-ToolbarNewActions']
${Loc_NewDataSheetTemplatesLink}  //table[@data-csi-automation='plugin-DataPackageTemplate-DataSheets-ToolbarNewActions']//div[contains(@class,'dijitButtonText')]
#${Loc_ActionsSpecDataSheetTab}   css=[data-csi-automation='plugin-SizeChartAdmin-SpecDSTemplates-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
#${Loc_NewDataSpecDataSheetTemplateLink}  css=[data-csi-act='NewSpecDataSheet'] .dijitMenuItemLabel
${Loc_ActionsSpecDataSheetTab}   css=[data-csi-automation='plugin-ApparelViews-SpecificationDataSheetTemplates-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
${Loc_NewDataSpecDataSheetTemplateLink}  //table[@data-csi-automation='plugin-ApparelViews-SpecificationDataSheetTemplates-ToolbarNewActions']//div[contains(@class,'dijitButtonText')][contains(text(),'New Spec Data Sheet Template')]
${Loc_ActionsSpecItemsTab}   css=[data-csi-automation='plugin-ApparelViews-SpecificationItemDefinitions-ToolbarNewActions']
${Loc_NewSpecDataSheetTemplateLink}  css=[data-csi-act='NewSpecificationItemDefinition'] .dijitMenuItemLabel
${Loc_ActionsStyleBOMTemplateTab}   css=[data-csi-automation='plugin-SizeChartAdmin-StyleBOMTemplates-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
${Loc_NewStyleBOMTemplateLink}  css=[data-csi-act='NewApparelBOM'] .dijitMenuItemLabel
${Loc_ActionsStyleBOMTemplateNodeTab}   css=[data-csi-automation='plugin-ProductBOMRevision-PartMaterials-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
${Loc_NewOfMaterialLink}  css=[data-csi-act='NewPartMaterialOfMaterial'] .dijitMenuItemLabel
${Loc_NewFromMaterialLink}  css=[data-csi-act='AggregateFromMaterial'] .dijitMenuItemLabel
${Loc_ActionsSpecficationRoutingTab}  css=[data-csi-automation='plugin-Style-Routing-ToolbarNewActions']
${Loc_NewRoutingFromTemplateLink}  css=[data-csi-automation='plugin-Style-Routing-ToolbarNewActions'] [data-csi-act='NewFromRoutingTemplates'] .dijitMenuItemLabel
${Loc_ActionsSpecificationDataSheetTab}  css=[data-csi-automation='plugin-Style-SpecificationDataSheets-ToolbarNewActions']
${Loc_NewSpecsDataSheetFromTemplateLink}  css=[data-csi-automation='plugin-Style-SpecificationDataSheets-ToolbarNewActions'] [data-csi-act='NewSpecificationDataSheetFromTemplate'] .dijitMenuItemLabel
${Loc_ActionsSpecSummaryTab}  css=[data-csi-automation='plugin-Product-DataPackages-ToolbarNewActions']
${Loc_NewDataPackageFromTemplateLink}  css=[data-csi-automation='plugin-Product-DataPackages-ToolbarNewActions'] [data-csi-act='NewDataPackageFromDataPackageTemplate'] .dijitMenuItemLabel

${Loc_SpecificationLibrary}  css=[data-csi-tab='SizeChartAdmin-Libraries'] 
${Actions_ProductSuppliers}  css=[data-csi-automation='plugin-MaterialSourcing-ProductSources-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
${Loc_NewFromSupplier}  css=[data-csi-act='NewProductSourceFromSuppliers'] .dijitMenuItemLabel
${Actions_ProductBlendedCosts}  css=[data-csi-automation='plugin-ProductSourcing-BlendedCosts-ToolbarNewActions']
${Loc_NewProductBlendedCosts}  css=[data-csi-automation='plugin-ProductSourcing-BlendedCosts-ToolbarNewActions']

${Loc_TableBrowseImages}  //*[@data-csi-act='Images::0']

${Loc_NewColorSpecificationActionLink}  //table[@data-csi-automation='plugin-ApparelViews-ColorSpecifications-ToolbarNewActions']//div[contains(@class,'dijitButtonText')]
${Loc_CareLabelSelectElement}  css=[data-csi-act='AggregateFromElements'] .dijitMenuItemLabel

${Loc_ColorSpecificationNewColorSpecificationLink}  //table[@data-csi-automation='plugin-LibColorSpecification-LibElements-ToolbarNewActions']//div[contains(@class,'dijitButtonText')]

${Loc_ToolBarIcon}  //span[@data-csi-automation='plugin-SizeChartRevision-TDS-htmlToolbars']//span[contains(@class,'dijitNoIcon')]
${Loc_MaterialBOMToolBarIcon}  //span[@data-csi-automation='plugin-MaterialBOMRevision-TDS-htmlToolbars']//span[contains(@class,'csi-toolbar-btn-icon-htmlToolbars')]

${Loc_StyleBOMToolBar}  //span[@data-csi-automation='plugin-ApparelBOMRevision-TDS-htmlToolbars']//span[contains(@class,'csi-toolbar-btn-icon-htmlToolbars')]
${Loc_StyleBOMPlacementAction}  //*[@data-csi-automation='plugin-ProductBOMRevision-PartMaterials-ToolbarNewActionsCreate']

${InputSection}  //div[@data-csi-automation='edit-ApparelViews-SizeRanges-Sizes:']//span[@class='dijitPlaceHolder dijitInputField' and text()='Select']/preceding-sibling::input
${Loc_TextBoxColoredMaterialName}  //*[@data-csi-automation='field-ColorMaterial-Form-Node Name']//input[@class='dijitReset dijitInputInner']
${Loc_MaterialName}  css=[data-csi-automation='field-Material-Form-ProductType'] .dijitReset.dijitInputInner
${Loc_NewColoredMaterial_dropdwn}  css=[data-csi-automation='plugin-Material-ProductColors-ToolbarNewActions'] td.dijitArrowButton
${NewFromColorSpecification}  css=[data-csi-act='NewFromColorSpecification'] .dijitMenuItemLabel
${Material_SKU_popup}  css=[data-csi-automation='field-SKUMaterial-Form-Node Name'] .dijitInputInner
${SKU_color}  css=[data-csi-automation='field-SKUMaterial-Form-RealizedColor'] .dijitArrowButton 
${SKU_save}  css=[data-csi-act='Save']
${Size_range_description}  css=[data-csi-automation='field-SizeRange-Form-Description'] .dijitInputInner
${Material_desc}  (//tr//td[@data-csi-act='Description::0'])[1]
${Material_desc_input box}  //div/textarea
${MaterialName_textbox}  css=[data-csi-automation='field-Material-Form-Node Name'] .dijitInputInner
${Loc_FieldNewSpecItemName}  css=[data-csi-automation='field-SpecificationItemDefinition-Form-Node Name'] .dijitInputInner
${Loc_TextAreaNewProductAlternativeSpecification}  css=[data-csi-automation='edit-ApparelViews-ProductAlternativeSpecifications-Node Name:']
${Loc_NewProductAlternativeSpecification}  css=[data-csi-automation='plugin-ApparelViews-ProductAlternativeSpecifications-ToolbarNewActions']
${Loc_TextFieldStyleBOMTemplateName}  css=[data-csi-automation='field-ApparelBOM-Form-Node Name'] .dijitInputInner

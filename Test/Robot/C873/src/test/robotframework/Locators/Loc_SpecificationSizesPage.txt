
*** Variables ***
${Loc_SpecificationTab}  css=[data-csi-tab='ApparelViews-ProductSpec']
${Loc_SizesTab}  css=[data-csi-tab='ApparelViews-Sizes']
${Loc_NewSizeSpecification}   css=[data-csi-act='NewProductSize']
${Loc_FieldSizeName}  css=[data-csi-automation='field-ProductSize-Form-Node Name'] .dijitInputInner
${Loc_FieldSortOrder}  css=[data-csi-automation='field-ProductSize-Form-SizeCode'] .dijitInputInner


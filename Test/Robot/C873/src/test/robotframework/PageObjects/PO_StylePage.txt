*** Settings ***

Resource          ../Locators/Loc_StylePage.txt
Resource          ../Locators/Loc_NewCreationPopUpPage.txt
Resource		  ../UtilityHelper/SuiteSetUp.txt

*** Keywords ***

Click On Season tab	
	Click Element   ${Loc_SeasonTab}

Check if the Season Name already available
	[Arguments]    ${Data_SeasonName}
	${SeasonNameCount} =  Get Matching Xpath Count  //*[@class='tableView'][1]//a[@class='browse']	
	${Check} =  Set Variable  False
	:FOR  ${i}  IN RANGE  0  ${SeasonNameCount}
		\  Log  ${i}
		\  ${TextString} =  Execute Javascript  return document.getElementsByClassName('tableView')[0].getElementsByClassName('browse')[${i}].text;
		\  Log  ${TextString}
		\  ${Check} =  Evaluate  '${Data_SeasonName}' == '${TextString}'
		\  Log  ${Check}
		\  Log  str(${Check})
		\  Run Keyword If  str(${Check}) == 'True'  Exit For Loop
	[return]  	${Check}
	
Wait for New Season option
    Wait Until Element Is Visible  ${Loc_NewSeason}

Click on New Season option
	Click Element  ${Loc_NewSeason}

Enter the Season in the Pop up window
    [Arguments]    ${Data_SeasonName}
    ${SeasonName} =  GetDataProviderColumnValue  ${Data_SeasonName}
    Input Text    ${Loc_PopUpNameTextBox}  ${SeasonName}
	
Wait for New Brand option
    Wait Until Element Is Visible  ${Loc_NewBrand}

Click on New Brand option
	Click Element  ${Loc_NewBrand}
	
Enter the Brand in the Pop up window
    [Arguments]    ${Data_BrandName}
    ${BrandName} =  GetDataProviderColumnValue  ${Data_BrandName}
    Input Text    ${Loc_PopUpNameTextBox}  ${BrandName}
    
Wait for New Collection option
    Wait Until Element Is Visible  ${Loc_NewCollection}

Click on New Collection option
	Click Element  ${Loc_NewCollection}

Enter the Collection in the Pop up window
    [Arguments]    ${Data_CollectionName}
    ${CollectionName} =  GetDataProviderColumnValue  ${Data_CollectionName}
    Input Text    ${Loc_PopUpNameTextBox}  ${CollectionName}

Wait for New Department option
    Wait Until Element Is Visible  ${Loc_NewDepartment}

Click on New Department option
	Click Element  ${Loc_NewDepartment}

Enter the Department in the Pop up window
    [Arguments]    ${Data_DepartmentName}
    ${DepartmentName} =  GetDataProviderColumnValue  ${Data_DepartmentName}
    Input Text    ${Loc_PopUpNameTextBox}  ${DepartmentName}

Wait for New Style option
    Wait Until Element Is Visible  ${Loc_NewStyle}

Click on New Style option
	Click Element  ${Loc_NewStyle}
	
Click on Style Type DDL
	${DDl_List} =  Get WebElements  //*[@class='dijitDialogPaneContent dijitDialogSingleChild']//input[@class='dijitReset dijitInputField dijitArrowButtonInner']	
	${StyleTypeDDL} =  Get From List  ${DDl_List}  0
	Click Element  ${StyleTypeDDL}


Select the Style Type for the style
	[Arguments]    ${Data_StyleTypeName}
	${StyleTypeName} =  GetDataProviderColumnValue  ${Data_StyleTypeName}
	Click Element  //*[@class='dijitReset dijitMenu dijitComboBoxMenu']//div[@class='dijitReset dijitMenuItem' and text()='${StyleTypeName}']

Enter the Style in the Pop up window
    [Arguments]    ${Data_StyleName}
    ${StyleName} =  GetDataProviderColumnValue  ${Data_StyleName}
    Input Text    ${Loc_PopUpNameTextBox}  ${StyleName}
       
Click on Size Range DDL from properties
	Click Element  css=[data-csi-act='ActualSizeRange:Child:Attributes:0']
	      
## SizeChart Functionality
Get Increment_or_Edited Size value in Table Cell
  [Arguments]  ${CellString}  ${ReqEle}
  ${CellSplitedList} =  Split Size value in Table Cell   ${CellString}  /
  ${CellValue} =  Get From List  ${CellSplitedList}  ${ReQEle}
  ${NoValue} =  Convert To Number  ${CellValue}
  ${SizeValue} =  Convert To Integer  ${NoValue}
  [Return]  ${SizeValue}
  
Split Size value in Table Cell
  [Arguments]  ${String}  ${Seperator}
  ${SplitedList} =  Split String  ${String}  ${Seperator}
  [Return]  ${SplitedList}
 
Update the Dimension Matrix Value
  [Arguments]  ${Dimension}  ${ClassName}  ${Position1}  ${Value}
  Click Element From List  //a[text()='${Dimension}']//following::td[@class='${ClassName}']  ${Position1}
  Wait Until Element is Visible  css=[data-csi-automation='edit-SizeChartRevision-Dimensions-Increments:{SizeChartDimensionSummaryMatrix}']
  Input text  css=[data-csi-automation='edit-SizeChartRevision-Dimensions-Increments:{SizeChartDimensionSummaryMatrix}'] .dijitInputInner  ${Value} 
  Click Element  //div[@class='csi-view-title csi-view-title-SizeChartRevision-Dimensions' and text()='Dimensions']

Verify the Dimension Matrix Value
  [Arguments]  ${Dimension}  ${ClassName}  ${Position1}  ${Value}
  ${Element} =  Get Text  //a[text()='${Dimension}']//following::td[@class='${ClassName}']//span[1]
  Run Keyword If 	${Element} == ${Value}  Log  The values for ${Dimension} are retained 
  Run Keyword If 	${Element} != ${Value}  Log  The values for ${Dimension} are not retained 
  Wait for Setup Icon 
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads
  #Navigate to Home-Sourcing Tab 
  Click Element  ${HomeStylesTab}
  DOMready-TabLoads
  PO_HomePage.Click On Styles tab

  Click on the Node in table  ${Style1}
  Wait For Page Load
  Click Element  ${Loc_Style_SpecificationTab}
  Wait Until Page Contains Element   xpath=//span[text()='Loading...']
  Wait Until Page Contains Element   ${Loc_Release}
  Click Element  ${Loc_Style-Specification-SizeChartTab}
  Wait For Page Load
 
Update inline values
  [Arguments]  ${Locator}  ${Value}
  Wait Until Page Contains Element  ${Locator} 
  #Repated Clicks  5
  Clear Element Text  //div//input
  Input Text  //div//input  ${Value} 
  
Repated Clicks
  [Arguments]  ${Locator}
  :FOR  ${i}  IN RANGE  0  5
  \  ${Editable} =    Run Keyword And Return Status  Wait For Input Field
  \  Exit For Loop If  '${Editable}' == 'True'
  \  ${ClickStatus} =  Run Keyword And Return Status  Click Element From List  ${Locator}  1
  
Wait For Input Field
  Wait Until Element Is Visible  //div//input  
  
Just Click
  [Arguments]  ${Locator}  ${Index}
  :FOR  ${i}  IN RANGE  0  5
  \  ${ClickStatus} =  Run Keyword And Return Status  Click Element From List  ${Locator}  ${Index}
  \  Exit For Loop If  '${ClickStatus}' == 'True'
  
Switch ON Compare Increment Radio Button 
  # != true
  [Arguments]    ${TDSrDIM}
  ${CmpIncSwitchCurrentStatus} =  Get Element Attribute  ${CmpIncrmntToolBar}  aria-pressed
  Run Keyword If  '${CmpIncSwitchCurrentStatus}' != 'true'  Click Element From List   css=span.csi-toolbar-btn-icon-ToggleIncrementValues  ${TDSrDIM}
  Run Keyword And Ignore Error  Wait Until Page Contains Element  //span[text()='Processing...']
  Wait For Release Info
  PO_TablePage.Wait Until Custom View TextArea Is Enabled  SizeChartRevision-Dimensions
  :FOR  ${i}  IN RANGE  0  5
  \  Run Keyword And Ignore Error  Mouse Over  css=span[data-csi-automation='plugin-SizeChartRevision-Dimensions-ToolbarNewActions']  div.csi-toolbar-btn-icon-ToolbarNewActions
  \  ${Status} =  Run Keyword And Return Status  Wait Until Element Is Visible  css=tr[data-csi-act='NewSizeChartDimension'] td.dijitMenuItemLabel
  \  Exit For Loop If  '${Status}' == 'True'
    
Switch OFF Compare Increment Radio Button 
  #== true
  [Arguments]    ${TDSrDIM}
  ${CmpIncSwitchCurrentStatus} =  Get Element Attribute  ${CmpIncrmntToolBar}  aria-pressed
  Run Keyword If  '${CmpIncSwitchCurrentStatus}' == 'true'  Click Element From List   css=span.csi-toolbar-btn-icon-ToggleIncrementValues  ${TDSrDIM}
  Run Keyword And Ignore Error  Wait Until Page Contains Element  //span[text()='Processing...']
  Wait For Release Info
  PO_TablePage.Wait Until Custom View TextArea Is Enabled  SizeChartRevision-Dimensions
  :FOR  ${i}  IN RANGE  0  5
  \  Run Keyword And Ignore Error  Mouse Over  css=span[data-csi-automation='plugin-SizeChartRevision-Dimensions-ToolbarNewActions']  div.csi-toolbar-btn-icon-ToolbarNewActions
  \  ${Status} =  Run Keyword And Return Status  Wait Until Element Is Visible  css=tr[data-csi-act='NewSizeChartDimension'] td.dijitMenuItemLabel
  \  Exit For Loop If  '${Status}' == 'True'
  
Get Increment Value From Base Size
  [Arguments]  ${Dim}
  ${PreBaseV1} =  Get Text  //a[text()='${Dim}']/parent::span/parent::td/following-sibling::td[contains(@class,'PatternBase')]/child::span[@class='increment']
  Log  ${PreBaseV1}  console=True
  [Return]  ${PreBaseV1}
  
Get Increment Value From First Size
  [Arguments]  ${Dim}
  #${V1} =  Get Text  //a[text()='${Dim}']/parent::span/parent::td/following-sibling::td[contains(@class,'csi-table-matrix-column-first')]/child::span
  ${FV1} =  Get Text  //a[text()='${Dim}']/parent::span/parent::td/following-sibling::td[contains(@class,'csi-table-matrix-column-first')]
  Log  ${FV1}  console=True  
  [Return]  ${FV1}
    
Get Increment Value From Last Size
  [Arguments]  ${Dim}
  #${V2} =  Get Text  //a[text()='${Dim}']/parent::span/parent::td/following-sibling::td[contains(@class,'csi-table-matrix-column-last')]/child::span
  ${LV1} =  Get Text  //a[text()='${Dim}']/parent::span/parent::td/following-sibling::td[contains(@class,'csi-table-matrix-column-last')]
  Log  ${LV1}  console=True
  [Return]  ${LV1}

Edit Or Over Key Value for Base Size
  [Arguments]    ${TDSrDIM}  ${Value}
  :FOR  ${Item}  IN  @{DimensionsList}
  \  Click On Size Attribute  ${Item}  PatternBase  0
  \  Execute JavaScript  window.document.evaluate("//div[@data-csi-automation='edit-SizeChartRevision-Dimensions-Increments:{SizeChartDimensionSummaryMatrix}']//div//input[contains(@class,'dijitInputInner')]",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.value=${Value};
  \  Click Element  css=[data-csi-tab='SizeChartRevision-${TDSrDIM}']
  Click Element From List  css=span[data-csi-automation='plugin-SizeChartRevision-Dimensions-refresh'] span.csi-toolbar-btn-icon-refresh  0
  Wait For Release Info
  PO_TablePage.Wait Until Custom View TextArea Is Enabled  SizeChartRevision-Dimensions
  DOMreadyByWaitTime  2

Click On Size Attribute
# class name to click respective sizes, PatternBase - to click base size, first size - csi-table-matrix-column-first, last size - csi-table-matrix-column-last.
  [Arguments]    ${Dim}  ${PartialClassName}  ${TDSrDIM}
  :FOR  ${Item}  IN  0  5
  \  Click Element From List  //a[text()='${Dim}']/parent::span/parent::td/following-sibling::td[contains(@class,'${PartialClassName}')]  0
  \  ${Status} =  Run Keyword And Return Status  Wait Until Page Contains Element  //div[@data-csi-automation='edit-SizeChartRevision-Dimensions-Increments:{SizeChartDimensionSummaryMatrix}']//div//input[contains(@class,'dijitInputInner')]
  \  Exit For Loop If  '${Status}' == 'True'
  
Edit Or Over Key Value for First Size
  [Arguments]    ${TDSrDIM}  ${Value}
  :FOR  ${Item}  IN  @{DimensionsList}
  \  Click On Size Attribute  ${Item}  csi-table-matrix-column-first  0
  \  Execute JavaScript  window.document.evaluate("//div[@data-csi-automation='edit-SizeChartRevision-Dimensions-Increments:{SizeChartDimensionSummaryMatrix}']//div//input[contains(@class,'dijitInputInner')]",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.value=${Value};
  \  Click Element  css=[data-csi-tab='SizeChartRevision-${TDSrDIM}']
  Click Element From List  css=span[data-csi-automation='plugin-SizeChartRevision-Dimensions-refresh'] span.csi-toolbar-btn-icon-refresh  0
  Wait For Release Info
  PO_TablePage.Wait Until Custom View TextArea Is Enabled  SizeChartRevision-Dimensions
  DOMreadyByWaitTime  2
  
Edit Or Over Key Value for Last Size
  #TDS, Dimensions
  [Arguments]    ${TDSrDIM}  ${Value}
  :FOR  ${Item}  IN  @{DimensionsList}
  \  Click On Size Attribute  ${Item}  csi-table-matrix-column-last  0
  \  Execute JavaScript  window.document.evaluate("//div[@data-csi-automation='edit-SizeChartRevision-Dimensions-Increments:{SizeChartDimensionSummaryMatrix}']//div//input[contains(@class,'dijitInputInner')]",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.value=${Value};
  \  Click Element  css=[data-csi-tab='SizeChartRevision-${TDSrDIM}']
  Click Element From List  css=span[data-csi-automation='plugin-SizeChartRevision-Dimensions-refresh'] span.csi-toolbar-btn-icon-refresh  0
  Wait For Release Info
  PO_TablePage.Wait Until Custom View TextArea Is Enabled  SizeChartRevision-Dimensions
  DOMreadyByWaitTime  2  
	
	

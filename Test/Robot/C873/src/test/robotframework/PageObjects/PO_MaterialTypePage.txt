*** Settings ***
Resource          ../Locators/Loc_MaterialTypePage.txt
Resource		  ../UtilityHelper/SuiteSetUp.txt
Resource		  ../PageObjects/PO_SetupPage.txt

*** Keywords ***

Wait for New Material Type option
    Wait Until Element Is Visible  ${Actions_MaterialType}

Click on New Material Type option
	Click Element  ${Actions_MaterialType}

Get Material type name string
	${Material_type_String} =  Generate Random String  3  [UPPER][NUMBERS]
    Log  ${Material_type_String}
    ${Material_type} =  Catenate  MType${Material_type_String}
    [return]  ${Material_type}

Enter the Material Type in the Pop up window
    [Arguments]    ${Data_MaterialTypeName}
    #${MaterialTypeName} =  GetDataProviderColumnValue  ${Data_MaterialTypeName}
    Wait Until Page Contains Element  ${Loc_NewMaterialTypeTextBox} 
    Input Text    ${Loc_NewMaterialTypeTextBox}  ${Data_MaterialTypeName}
#YM needs....
Click on DDL in the Pop up window
   [Arguments]    ${LabelName}
   Click Element  //tr/th[text()='${LabelName}']/following::input[@class='dijitReset dijitInputField dijitArrowButtonInner']
#YM needs.... 
Select value in DDL 
  [Arguments]    ${DDLText}
  Sleep  3
  Click Element  //div[text()='${DDLText}']
  
Click on Material Save Button
	Click Element  ${Loc_SaveButton}
	Run KeyWord And Return Status  Wait Until Element Is Not Visible  //span[@class='dijitDialogTitle' and text()='New Material Type']
    Wait Until Element Is Visible  //span[text()='Material Type created successfully.']
    #Element Should Be Visible  css=[data-csi-automation='plugin-Site-MaterialTypes-views']
    DOMreadyByWaitTime  5
    
Check if the Material Name already Exist 
	[Arguments]    ${MaterialName}
	${MaterialTypeCount} =  Get Matching Xpath Count  ${Loc_MaterialTypeCount}	
	${Check} =  Set Variable  False
	:FOR  ${i}  IN RANGE  0  ${MaterialTypeCount}
		\  Log  ${i}
		\  ${TextString} =  Execute Javascript  return document.getElementsByClassName('postUpdate')[${i}].children[0].textContent;
		\  ${Check} =  Evaluate  '${MaterialName}' == '${TextString}'
		\  Log  str(${Check})
		\  Run Keyword If  str(${Check}) == 'True'  Exit For Loop
	[return]  	${Check}
	
Select Required Check box on the Material Type
	[Arguments]    ${MaterialType}  ${Position}
	PO_SetupPage.Click On Material types tab
	${ElementList} =  Get WebElements   //td[text()='${MaterialType}']/following::td[@${Position}]//div/input
	${Element} =  Get From List  ${ElementList}  0
	Set Focus To Element  ${Element}
	Select Checkbox  ${Element}
	Capture Page Screenshot
	#${Status} =  Run Keyword And Return Status  Click Element  ${Element}
	#${ExeStatus1} =  Split String   ${ExeStatus}  ,
	#${Status} =  Get From List  ${ExeStatus1}  0
	#Run Keyword If  '${Status}' == 'FAIL'  Mouse Over  css=[data-csi-automation='plugin-Site-MaterialTypes-views']
    #Run Keyword If  '${Status}' == 'FAIL'  Click Element  ${Element}
    	
Select Required Check box on the Material Type2
	[Arguments]    ${MaterialType}  ${Position}
	PO_SetupPage.Click On Material types tab
	${ElementList} =  Get WebElements   //td[text()='${MaterialType}']/following::td[${Position}]/div[1]/input
	${Element} =  Get From List  ${ElementList}  0
	Set Focus To Element  ${Element}
	Select Checkbox  ${Element}
	Capture Page Screenshot
	#${Status} =  Run Keyword And Return Status  Click Element  ${Element}
	#${ExeStatus1} =  Split String   ${ExeStatus}  ,
	#${Status} =  Get From List  ${ExeStatus1}  0
	#Run Keyword If  '${Status}' == 'FAIL'  Mouse Over  css=[data-csi-automation='plugin-Site-MaterialTypes-views']
    #Run Keyword If  '${Status}' == 'FAIL'  Click Element  ${Element}
	
Select the Active Check box to configure the New Material
	[Arguments]    ${MaterialType}
	${ElementList} =  Get WebElements  //tr[@class='pendingUpdate']//td[text()='${MaterialType}']/following::td[1]/div[1]/input
	${Element} =  Get From List  ${ElementList}  0
	Select Checkbox  ${Element}

Select the Has Color Check box to configure the New Material
	[Arguments]    ${MaterialType}
	${ElementList} =  Get WebElements  //tr[@class='pendingUpdate']//td[text()='${MaterialType}']/following::td[3]/div[1]/input
	${Element} =  Get From List  ${ElementList}  0
	Select Checkbox  ${Element}

Select the Has Size Check box to configure the New Material
	[Arguments]    ${MaterialType}
	${ElementList} =  Get WebElements  //tr[@class='pendingUpdate']//td[text()='${MaterialType}']/following::td[4]/div[1]/input
	${Element} =  Get From List  ${ElementList}  0
	Select Checkbox  ${Element}

Select the Has Season Check box to configure the New Material
	[Arguments]    ${MaterialType}
	${ElementList} =  Get WebElements  //tr[@class='pendingUpdate']//td[text()='${MaterialType}']/following::td[6]/div[1]/input
	${Element} =  Get From List  ${ElementList}  0
	Select Checkbox  ${Element}

Select the Has Highest Quote for placement Check box to configure the New Material
	[Arguments]    ${MaterialType}
	${ElementList} =  Get WebElements  //tr[@class='pendingUpdate']//td[text()='${MaterialType}']/following::td[7]/div[1]/input
	${Element} =  Get From List  ${ElementList}  0
	Select Checkbox  ${Element}

Select the Material BOM Check box to configure the New Material
	[Arguments]    ${MaterialType}
	${ElementList} =  Get WebElements  //tr[@class='pendingUpdate']//td[text()='${MaterialType}']/following::td[10]/div[1]/input
	${Element} =  Get From List  ${ElementList}  0
	Select Checkbox  ${Element}

Select the Routing Check box to configure the New Material
	[Arguments]    ${MaterialType}
	${ElementList} =  Get WebElements  //tr[@class='pendingUpdate']//td[text()='${MaterialType}']/following::td[11]/div[1]/input
	${Element} =  Get From List  ${ElementList}  0
	Select Checkbox  ${Element}

Select the Spec Data Sheet Check box to configure the New Material
	[Arguments]    ${MaterialType}
	${ElementList} =  Get WebElements  //tr[@class='pendingUpdate']//td[text()='${MaterialType}']/following::td[12]/div[1]/input
	${Element} =  Get From List  ${ElementList}  0
	Select Checkbox  ${Element}

Select the Test Run Check box to configure the New Material
	[Arguments]    ${MaterialType}
	${ElementList} =  Get WebElements  //tr[@class='pendingUpdate']//td[text()='${MaterialType}']/following::td[13]/div[1]/input
	${Element} =  Get From List  ${ElementList}  0
	Select Checkbox  ${Element}

Select the Diameter Check box to configure the New Material
	[Arguments]    ${MaterialType}
	${ElementList} =  Get WebElements  //tr[@class='pendingUpdate']//td[text()='${MaterialType}']/following::td[14]/div[1]/input
	${Element} =  Get From List  ${ElementList}  0
	Select Checkbox  ${Element}

Select the Dimension Check box to configure the New Material
	[Arguments]    ${MaterialType}
	${ElementList} =  Get WebElements  //tr[@class='pendingUpdate']//td[text()='${MaterialType}']/following::td[15]/div[1]/input
	${Element} =  Get From List  ${ElementList}  0
	Select Checkbox  ${Element}

Select the Finish Check box to configure the New Material
	[Arguments]    ${MaterialType}
	${ElementList} =  Get WebElements  //tr[@class='pendingUpdate']//td[text()='${MaterialType}']/following::td[16]/div[1]/input
	${Element} =  Get From List  ${ElementList}  0
	Select Checkbox  ${Element}

Select the Length Check box to configure the New Material
	[Arguments]    ${MaterialType}
	${ElementList} =  Get WebElements  //tr[@class='pendingUpdate']//td[text()='${MaterialType}']/following::td[17]/div[1]/input
	${Element} =  Get From List  ${ElementList}  0
	Select Checkbox  ${Element}

Select the TextureEmbossRef Check box to configure the New Material
	[Arguments]    ${MaterialType}
	${ElementList} =  Get WebElements  //tr[@class='pendingUpdate']//td[text()='${MaterialType}']/following::td[18]/div[1]/input
	${Element} =  Get From List  ${ElementList}  0
	Select Checkbox  ${Element}

Select the Thickness Check box to configure the New Material
	[Arguments]    ${MaterialType}
	${ElementList} =  Get WebElements  //tr[@class='pendingUpdate']//td[text()='${MaterialType}']/following::td[19]/div[1]/input
	${Element} =  Get From List  ${ElementList}  0
	Select Checkbox  ${Element}

Select the Tooling Last Check box to configure the New Material
	[Arguments]    ${MaterialType}
	${ElementList} =  Get WebElements  //tr[@class='pendingUpdate']//td[text()='${MaterialType}']/following::td[20]/div[1]/input
	${Element} =  Get From List  ${ElementList}  0
	Select Checkbox  ${Element}

Select the Tooling Size Range Check box to configure the New Material
	[Arguments]    ${MaterialType}
	${ElementList} =  Get WebElements  //tr[@class='pendingUpdate']//td[text()='${MaterialType}']/following::td[21]/div[1]/input
	${Element} =  Get From List  ${ElementList}  0
	Select Checkbox  ${Element}

Select the Tooling Sizes Check box to configure the New Material
	[Arguments]    ${MaterialType}
	${ElementList} =  Get WebElements  //tr[@class='pendingUpdate']//td[text()='${MaterialType}']/following::td[22]/div[1]/input
	${Element} =  Get From List  ${ElementList}  0
	Select Checkbox  ${Element}

Select the Width Check box to configure the New Material
	[Arguments]    ${MaterialType}
	${ElementList} =  Get WebElements  //tr[@class='pendingUpdate']//td[text()='${MaterialType}']/following::td[23]/div[1]/input
	${Element} =  Get From List  ${ElementList}  0
	Select Checkbox  ${Element}

Select the Brand Check box to configure the New Material
	[Arguments]    ${MaterialType}
	${ElementList} =  Get WebElements  //tr[@class='pendingUpdate']//td[text()='${MaterialType}']/following::td[24]/div[1]/input
	${Element} =  Get From List  ${ElementList}  0
	Select Checkbox  ${Element}

Select the Features Check box to configure the New Material
	[Arguments]    ${MaterialType}
	${ElementList} =  Get WebElements  //tr[@class='pendingUpdate']//td[text()='${MaterialType}']/following::td[25]/div[1]/input
	${Element} =  Get From List  ${ElementList}  0
	Select Checkbox  ${Element}
Select the Functions Check box to configure the New Material
	[Arguments]    ${MaterialType}
	${ElementList} =  Get WebElements  //tr[@class='pendingUpdate']//td[text()='${MaterialType}']/following::td[26]/div[1]/input
	${Element} =  Get From List  ${ElementList}  0
	Select Checkbox  ${Element}

Click Compatible Types cell in Material Type table 
  [Arguments]  ${RefItem} 
  Click Element  //tr/td[text()='${RefItem}']/following-sibling::td[@class='attrReflist attrContainer iconEditable']
  
Select Compatible Material types  
  [Arguments]  ${CompatibleMaterialType}
  Wait Until Element Is Visible  //*[@class='dijitMenuItemLabel' and text()='${CompatibleMaterialType}']/preceding::div[1]/input[1]
  Select Checkbox   //*[@class='dijitMenuItemLabel' and text()='${CompatibleMaterialType}']/preceding::div[1]/input[1]
    
Click on Material-Composition tab
  Click Element	  ${Loc_Composition_Tab}
  
Click on Material-ProductSymbols tab
  Click Element		${Loc_ProductSymbols_Tab}	

    
    
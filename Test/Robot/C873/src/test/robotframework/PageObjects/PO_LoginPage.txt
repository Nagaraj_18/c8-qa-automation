*** Settings ***

Resource          ../Locators/Loc_LoginPage.txt
Resource          ../UtilityHelper/SuiteSetUp.txt 

*** Keywords ***

Input UserName 
    [Arguments]    ${Data_UserName}     
    ${UserName} =  GetDataProviderColumnValue  ${Data_UserName}
    Wait Until Element Is Visible      ${Loc_UserName}
   Selenium2Library.Input Text    ${Loc_UserName}    ${UserName}

Input Password
    [Arguments]    ${Data_Password}   
    ${Password} =  GetDataProviderColumnValue  ${Data_Password} 
    Selenium2Library.Input Text    ${Loc_Password}    ${Password}

Submit Credentials
    Wait Until Element Is Visible  css=div.csiActions span.csi-primary-btn
    Click Element  xpath=//span[@id = "loginButton_label"]
    # Wait Until Page does not Contain Element  id=forgotPasswordLink
    # ${LoaderStatus} =  Run Keyword And Return Status  Wait For Loader Icon To Appear 

Wait For Login Process
    Wait Until Page Does Not Contain Element   //img[@src='Generated/Logo.png']
    Wait Until Element Is Not Visible   //img[@src='Images/logo_loader.gif']
    Wait Until Element IS Visible  css=[data-csi-automation='plugin-User-HeaderToolbar-PageSetup']
    
Login TestCaseName		
	[Tags]  Sanity, Material
	[Arguments]    ${TestCaseName}
	#Data provider: It reads the data from the excel sheet for the test case specified. 
	${This_DataProvider} =  Data_Provider.DataProvider    ${TestCaseName}
	Set Suite Variable  ${This_DataProvider}
	SuiteSetUp.Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    Wait For Release Info    
    
Add Needed Image Path
    Add Image Path    ${IMAGE_DIR}    
    
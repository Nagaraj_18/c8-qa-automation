 *** Settings ***

Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt
Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_MaterialTypePage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../PageObjects/PO_TablePage.txt
Resource          ../Locators/Loc_SetupPage.txt   
***Variables***
${Loc_MaterialTypeDDL}   css=[data-csi-automation='field-MaterialType-Form-MaterialUsage'] .dijitMenuItem[item='0']
${MaterialUsageType1}  Standalone
${MaterialUsageType2}  Structure Component
${MaterialUsageType3}  Tool
${HSliderPosition}  //div[contains(@class,'dijitSliderImageHandle')]
@{MaterialType1Attributes}  data-csi-heading-vk='TestRun'  data-csi-heading='DefaultPlacementQuoteToHighestCost:Child:Config:0'  data-csi-heading='HasColor:Child:Config:0'  data-csi-heading-vk='TestRun'  data-csi-heading-vk='SpecificationDataSheet'  data-csi-heading-vk='Routing'	   data-csi-heading-vk='MaterialBOM'  data-csi-heading='DefaultHasSeasonAvailability:Child:Config:0'  data-csi-heading='DefaultColorSizeAvailability:Child:Config:0'  data-csi-heading='HasSize:Child:Config:0'  data-csi-heading='AllowCreateColoredMaterialOnPlacement:Child:Config:0'    
@{MaterialType2Attributes}  data-csi-heading-vk='TestRun'  data-csi-heading-vk='SpecificationDataSheet'  data-csi-heading-vk='Routing'	   data-csi-heading-vk='MaterialBOM'  data-csi-heading='DefaultPlacementQuoteToHighestCost:Child:Config:0'  data-csi-heading='DefaultHasSeasonAvailability:Child:Config:0'  data-csi-heading='HasSize:Child:Config:0' 	 
@{MaterialType3Attributes}  data-csi-heading-vk='TestRun'  data-csi-heading='HasColor:Child:Config:0'  data-csi-heading-vk='SpecificationDataSheet'  data-csi-heading-vk='Routing'	   data-csi-heading-vk='MaterialBOM'  data-csi-heading='DefaultPlacementQuoteToHighestCost:Child:Config:0'  data-csi-heading='DefaultHasSeasonAvailability:Child:Config:0'  data-csi-heading='DefaultColorSizeAvailability:Child:Config:0'  data-csi-heading='AllowCreateColoredMaterialOnPlacement:Child:Config:0'  
@{MaterialType4Attributes}  data-csi-heading-vk='TestRun'  data-csi-heading-vk='SpecificationDataSheet'  data-csi-heading-vk='Routing'	   data-csi-heading-vk='MaterialBOM'  data-csi-heading='DefaultHasSeasonAvailability:Child:Config:0' 

*** Test Cases ***
Test Setup
    [Tags]  Setup  		
    DataSetup For Material Types
Create Material Types
    [Tags]  Setup
    SuiteSetUp.Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    Wait.DOMready
    DOMready-TabLoads
    #Reload
	Wait.DOMready
    DOMready-TabLoads
    #Click on Setup Icon
    #Wait.DOMready
    #DOMready-TabLoads
    #Click On the System Configuration Link
    #DOMready-TabLoads
    PO_SetupPage.Click On Configuration tab
    #Check-is the DOM ready
    Wait.DOMready
    SuiteSetUp.Test Step    ${R_Click_TypeConfigurationTab}
    PO_SetupPage.Click On Type Configuration tab
    #Check-is the DOM ready
    Wait.DOMready
    SuiteSetUp.Test Step    ${R_Click_MaterialTypesTab}
    PO_SetupPage.Click On Material types tab
    DOMready-TabLoads
    #Create CustomView
    ##Select View  Default    
    #Check-is the DOM ready
    DOMready-TabLoads
    DOMreadyByWaitTime  3
    Create View
    DOMreadyByWaitTime  3
Choose Data Sheets for Material Type
    [Tags]  Setup
    ${present} =  Run Keyword And Return Status    Element Should Be Visible   //td[@data-csi-heading='Node Name::0'and text()='${MaterialName1}']
    Run Keyword If   '${present}' == 'False'  Create StandaloneMaterialType and select Data Sheets      
    Update Data Sheets For StandaloneMaterialType
    
    Create HasSizeNoColorMaterialType and select Data Sheets      
    Update Data Sheets For HasSizeNoColorMaterialType
    #Select View  Default
    #Create Tool Material Type
    #Click Element From List  //span[@class='dijitSliderButtonInner']  0
    DOMreadyByWaitTime  3
    Create NoSizeHasColorMaterialType and select Data Sheets      
    Update Data Sheets For NoSizeHasColorMaterialType
    #Update Data Sheets For ToolMaterialType  
    DOMreadyByWaitTime  3 
    Create NoSizeNoColorMaterialType and select Data Sheets      
    Update Data Sheets For NoSizeNoColorMaterialType
    # Step 7: Close the Browser Instance.
    SuiteSetUp.Test Step    ${R_Close_Browser}
    #[Teardown]    Close Browser 
 *** Keywords ***
 
Create View
  Click Element From List  css=[data-csi-automation='plugin-Site-MaterialTypes-CustomViewActions'] .dijitButtonText  -1
  Wait.DOMreadyByWaitTime  2
  Click Element From List  css=[data-csi-automation='plugin-Site-MaterialTypes-CustomViewManage'] .dijitMenuItemLabel  -1
  Wait.DOMreadyByWaitTime  5
  Wait.DOMreadyByWaitTime  2	
  PO_SetupPage.Click on Default Custom View
  PO_SetupPage.Click on Copy button
  Clear Element Text  ${Loc_SecurityRoleName}
  Selenium2Library.Input Text  ${Loc_SecurityRoleName}  CV
  Wait.DOMreadyByWaitTime  2
  #Click Element		//div[@class='csiPreferenceDisplay']//div[@class='dijitReset dijitRight dijitButtonNode dijitArrowButton dijitDownArrowButton dijitArrowButtonContainer']         
  #Click Element          //div[@class='dijitReset dijitMenuItem' and text()='Material']      
#Add the  attribute to the view 
  Click Element 		css=.csiPreferenceSelect [value='EnableSKUSources:Child:Config:0']
  Wait.DOMreadyByWaitTime  2
  Click Element         //span[text()='< Remove']     
#Add the attribute to the view
  Wait.DOMreadyByWaitTime  2
  Click Element 		css=.csiPreferenceSelect [value='MaterialUsage:Child:Config:0']
  Wait.DOMreadyByWaitTime  2
  Click Element         //span[text()='< Remove']
  Wait.DOMreadyByWaitTime  2
    Click Element 		css=.csiPreferenceSelect [value='CompatibleTypes:Child:Config:0']
  Wait.DOMreadyByWaitTime  2
  Click Element         //span[text()='< Remove']
  Click Element From List  Xpath=//span[text()='Save']  -1 
  Wait.DOMreadyByWaitTime  5 
Create StandaloneMaterialType and select Data Sheets   
    #Create New Material Types
    Mouse Over  ${Actions_MaterialType}
    #Step Description
    PO_MaterialTypePage.Wait for New Material Type option
    PO_MaterialTypePage.Click on New Material Type option
    #Check-is the DOM ready
    DOMready-PopUpAppears
    DOMreadyByWaitTime  3
    PO_MaterialTypePage.Enter the Material Type in the Pop up window    ${MaterialName1}
    SuiteSetUp.Test Step    ${R_ClickSaveButton}
    PO_MaterialTypePage.Click on Material Save Button
    #Wait Until Element IS Visible  ${Loc_Release}
    Select Required Check box on the Material Type    ${MaterialName1}  data-csi-heading='Available:Child:Config:0'
 
Dummy Cancel Action to overcome the failure 
    Mouse Over  ${Actions_MaterialType}
    #Step Description
    PO_MaterialTypePage.Wait for New Material Type option
    PO_MaterialTypePage.Click on New Material Type option
    #Check-is the DOM ready
    DOMready-PopUpAppears
    DOMreadyByWaitTime  3
    ${Status} =  Run Keyword And Return Status  Click Element From List  //span[text()='Cancel']  -1
    Run Keyword If  '${Status}' == 'FAIL'  Reload Page
           
Update Data Sheets For StandaloneMaterialType
    ${MaterialType1Status} =  Run Keyword and Return Status  Assert MaterialType in Table  ${MaterialName1}
    Run Keyword If   '${MaterialType1Status}' == 'False'  Create StandaloneMaterialType and select Data Sheets
    #@{MaterialType1Attributes}  Create List	7	3	6	4	11	5	12	15	13	16	14	17	20	19	22	25	21	24	27	26	23
    #@{MaterialType1Attributes}  Create List	7	3	6	4	11	5	12	15	13	16	14	17	20	19	22	25	21	24	27	26	23
    # Step 5: Select the Material type attributes Checkboxes.
    #Step Description
    :FOR  ${I}  IN  @{MaterialType1Attributes}
    \  @{MatchText} =  Split String  ${I}  '   
    \  ${MatchValue} =  Get From List  ${MAtchText}  1    
    #\  Run Keyword If  '${MatchValue}' == 'MaterialBOM'  Select View  simplifiedview
    \  DOMreadyByWaitTime    2
    \  ${Status} =  Run Keyword And Return Status  Select Required Check box on the Material Type    ${MaterialName1}  ${I}
    \  DOMreadyByWaitTime    2

Create HasSizeNoColorMaterialType and select Data Sheets   
    #Create New Material Types
    Mouse Over  ${Actions_MaterialType}
    #Step Description
    PO_MaterialTypePage.Wait for New Material Type option
    PO_MaterialTypePage.Click on New Material Type option
    #Check-is the DOM ready
    DOMready-PopUpAppears
    DOMreadyByWaitTime  3
    PO_MaterialTypePage.Enter the Material Type in the Pop up window    ${MaterialName2}
    SuiteSetUp.Test Step    ${R_ClickSaveButton}
    PO_MaterialTypePage.Click on Material Save Button
    #Wait Until Element IS Visible  ${Loc_Release}
    Select Required Check box on the Material Type    ${MaterialName2}  data-csi-heading='Available:Child:Config:0'
 
       
Update Data Sheets For HasSizeNoColorMaterialType
    ${MaterialType1Status} =  Run Keyword and Return Status  Assert MaterialType in Table  ${MaterialName2}
    Run Keyword If   '${MaterialType1Status}' == 'False'  Create HasSizeNoColorMaterialType and select Data Sheets
    #@{MaterialType1Attributes}  Create List	7	3	6	4	11	5	12	15	13	16	14	17	20	19	22	25	21	24	27	26	23
    #@{MaterialType1Attributes}  Create List	7	3	6	4	11	5	12	15	13	16	14	17	20	19	22	25	21	24	27	26	23
    # Step 5: Select the Material type attributes Checkboxes.
    #Step Description
    :FOR  ${I}  IN  @{MaterialType2Attributes}
    \  DOMreadyByWaitTime    2
    \  ${Status} =  Run Keyword And Return Status  Select Required Check box on the Material Type    ${MaterialName2}  ${I}
    \   DOMreadyByWaitTime    2
Create NoSizeHasColorMaterialType and select Data Sheets   
    #Create New Material Types
    Mouse Over  ${Actions_MaterialType}
    #Step Description
    PO_MaterialTypePage.Wait for New Material Type option
    PO_MaterialTypePage.Click on New Material Type option
    #Check-is the DOM ready
    DOMready-PopUpAppears
    DOMreadyByWaitTime  3
    PO_MaterialTypePage.Enter the Material Type in the Pop up window    ${MaterialName3}
    SuiteSetUp.Test Step    ${R_ClickSaveButton}
    PO_MaterialTypePage.Click on Material Save Button
    #Wait Until Element IS Visible  ${Loc_Release}
    Select Required Check box on the Material Type    ${MaterialName3}  data-csi-heading='Available:Child:Config:0'
 
       
Update Data Sheets For NoSizeHasColorMaterialType
    ${MaterialType1Status} =  Run Keyword and Return Status  Assert MaterialType in Table  ${MaterialName3}
    Run Keyword If   '${MaterialType1Status}' == 'False'  Create NoSizeHasColorMaterialType and select Data Sheets
    #@{MaterialType1Attributes}  Create List	7	3	6	4	11	5	12	15	13	16	14	17	20	19	22	25	21	24	27	26	23
    #@{MaterialType1Attributes}  Create List	7	3	6	4	11	5	12	15	13	16	14	17	20	19	22	25	21	24	27	26	23
    # Step 5: Select the Material type attributes Checkboxes.
    #Step Description
    :FOR  ${I}  IN  @{MaterialType3Attributes}
    \  @{MatchText} =  Split String  ${I}  '   
    \  ${MatchValue} =  Get From List  ${MAtchText}  1    
    #\  Run Keyword If  '${MatchValue}' == 'MaterialBOM'  Select View  simplifiedview
    \  DOMreadyByWaitTime    2
    \  ${Status} =  Run Keyword And Return Status  Select Required Check box on the Material Type    ${MaterialName3}  ${I}
    \  DOMreadyByWaitTime    2
Create NoSizeNoColorMaterialType and select Data Sheets   
    #Create New Material Types
    Mouse Over  ${Actions_MaterialType}
    #Step Description
    PO_MaterialTypePage.Wait for New Material Type option
    PO_MaterialTypePage.Click on New Material Type option
    #Check-is the DOM ready
    DOMready-PopUpAppears
    DOMreadyByWaitTime  3
    PO_MaterialTypePage.Enter the Material Type in the Pop up window    ${MaterialName4}
    SuiteSetUp.Test Step    ${R_ClickSaveButton}
    PO_MaterialTypePage.Click on Material Save Button
    #Wait Until Element IS Visible  ${Loc_Release}
    Select Required Check box on the Material Type    ${MaterialName4}  data-csi-heading='Available:Child:Config:0'
 
       
Update Data Sheets For NoSizeNoColorMaterialType
    ${MaterialType1Status} =  Run Keyword and Return Status  Assert MaterialType in Table  ${MaterialName4}
    Run Keyword If   '${MaterialType1Status}' == 'False'  Create NoSizeNoColorMaterialType and select Data Sheets
    #@{MaterialType1Attributes}  Create List	7	3	6	4	11	5	12	15	13	16	14	17	20	19	22	25	21	24	27	26	23
    #@{MaterialType1Attributes}  Create List	7	3	6	4	11	5	12	15	13	16	14	17	20	19	22	25	21	24	27	26	23
    # Step 5: Select the Material type attributes Checkboxes.
    #Step Description
    :FOR  ${I}  IN  @{MaterialType4Attributes}
    \  @{MatchText} =  Split String  ${I}  '   
    \  ${MatchValue} =  Get From List  ${MAtchText}  1    
    #\  Run Keyword If  '${MatchValue}' == 'MaterialBOM'  Select View  simplifiedview
    \  DOMreadyByWaitTime    2
    \  ${Status} =  Run Keyword And Return Status  Select Required Check box on the Material Type    ${MaterialName4}  ${I}
    \  DOMreadyByWaitTime    2
DataSetup For Material Types
  ${This_DataProvider} =  Data_Provider.DataProvider  Create Materials Types and select Data Sheets
  Set Suite Variable  ${This_DataProvider}
  ${MaterialNameList} =  Data_Provider.GetDataProviderColumnValue  Data_MaterialTypeName
  ${MaterialName1} =  DataProviderSplitterForMultipleValues  ${MaterialNameList}  1
  ${MaterialName2} =  DataProviderSplitterForMultipleValues  ${MaterialNameList}  2
  ${MaterialName3} =  DataProviderSplitterForMultipleValues  ${MaterialNameList}  3
  ${MaterialName4} =  DataProviderSplitterForMultipleValues  ${MaterialNameList}  4

  Set Suite Variable   ${MaterialName1}	
  Set Suite Variable   ${MaterialName2}
  Set Suite Variable   ${MaterialName3}
  Set Suite Variable   ${MaterialName4}
  
Navigate To Y Section
  :FOR  ${i}  IN RANGE  0  10
  \  ${Value} =  Get Element Attribute  ${HSliderPosition}@aria-valuenow
  \  Run Keyword IF  ${value} != 27  Click Element From List  css=div.dijitSliderIncrementIconH>span  -1
  #//div[@aria-valuenow='27']
  
Navigate To X Section
  :FOR  ${i}  IN RANGE  0  10
  \  ${Value} =  Get Element Attribute  ${HSliderPosition}@aria-valuenow
  \  Run Keyword IF  ${value} != 18  Click Element From List     css=div.dijitSliderDecrementIconH>span  -1

Create CustomView
    Click Element   css=span[data-csi-automation='plugin-Site-MaterialTypes-views'] >span .dijitButtonText
    Wait Until Element Is Visible  //span[@class='dijitDialogTitle' and text()='Custom Views']
	Click Element  ${Loc_DefaultCustomView}
    Click Element		//*[@title='Copy custom view']
	Clear Element Text  ${Loc_SecurityRoleName}
	Input Text  ${Loc_SecurityRoleName}  simplifiedview
	#@{views} =  Create List  AttributesMap  TDSMap  DefaultPlacementQuoteToHighestCost  DefaultHasSeasonAvailability  DefaultColorSizeAvailability  HasSize  AllowCreateColoredMaterialOnPlacement  HasColor  Available
	@{views} =  Create List  Available    HasColor    AllowCreateColoredMaterialOnPlacement    HasSize    DefaultColorSizeAvailability  DefaultPlacementQuoteToHighestCost  MaterialUsage  CompatibleTypes
	Click Element  //option[@value='Published::0']
	:FOR  ${view}  IN  @{views}
	\    Click Element  //option[@value='${view}:Child:Config:0']
	\    Click Element  //span[text()='< Remove']
	\    DOMreadyByWaitTime    2
	Click on Custom View Save Button
	DOMready-PopUpCloses
	Wait Until Custom View TextArea Is Enabled  Site-MaterialTypes

Select View
    [Arguments]  ${CVText}
    Click Element  css=[data-csi-automation='plugin-Site-MaterialTypes-views'] div.dijitDownArrowButton
    Wait Until Page Contains Element  //div[contains(@class,'dijitMenuItem')]//span[text()='${CVText}']
    Click Element  //div[contains(@class,'dijitMenuItem')]//span[text()='${CVText}']
    Wait Until Custom View TextArea Is Enabled  Site-MaterialTypes	
Create Tool Material Type 
    Polling Click Required Action  ${Actions_MaterialType}  ${Actions_MaterialType}  
    DOMready-PopUpAppears
    #Step Description
    SuiteSetUp.Test Step    ${R_EnterNewMaterialType}
    PO_MaterialTypePage.Enter the Material Type in the Pop up window    ${ToolMaterialTypeName}
	PO_NewCreationPopUpPage.Click on the DDL button  field-MaterialType-Form-MaterialUsage
  #Wait for the DDL to appear by checking on the blank value
    ${DDLClickStatus} =  Run Keyword and Return Status  Wait Until Element Is Visible  ${Loc_MaterialTypeDDL}
    Run Keyword If   '${DDLClickStatus}' == 'False'  Clear Element Text  css=td>div[data-csi-automation='field-MaterialType-Form-MaterialUsage'] div.dijitInputField>input
    Run Keyword If   '${DDLClickStatus}' == 'False'  Input Text  css=td>div[data-csi-automation='field-MaterialType-Form-MaterialUsage'] div.dijitInputField>input  ${MaterialUsageType3}
    Run Keyword If   '${DDLClickStatus}' == 'True'  PO_NewCreationPopUpPage.Select Elemnt from DDL   ${MaterialUsageType3}
	Wait.DOMreadyByWaitTime  2
    Click on Material Save Button
    Wait.DOMreadyByWaitTime  4
    #Mouse Over  css=[data-csi-heading='Available:Child:Config:0']
    Wait.DOMreadyByWaitTime  2
    Select Required Check box on the Material Type    ${ToolMaterialTypeName}  data-csi-heading='Available:Child:Config:0'
Update Data Sheets For ToolMaterialType    
   #@{MaterialType1Attributes}  Create List	7	6	11	14	12	15	13	16	19	17	20	23	21	24	22	25	27	26
    # Step 5: Select the Material type attributes Checkboxes.
    #Step Description
    :FOR  ${I}  IN  @{MaterialType1Attributes}
    \  @{MatchText} =  Split String  ${I}  '   
    \  ${MatchValue} =  Get From List  ${MAtchText}  1    
    #\  Run Keyword If  '${MatchValue}' == 'MaterialBOM'  Select View  simplifiedview
    \  DOMreadyByWaitTime    2
    \  ${Status} =  Run Keyword And Return Status  Select Required Check box on the Material Type     ${ToolMaterialTypeName}  ${I}
    \  Run Keyword If  '${Status}' == 'FAIL'  Dummy Cancel Action to overcome the failure
    \  Run Keyword If  '${Status}' == 'FAIL'  Select Required Check box on the Material Type     ${ToolMaterialTypeName}  ${I}
    
    #Select View  Default
    PO_SetupPage.Click On Material types tab
    DOMready-TabLoads     
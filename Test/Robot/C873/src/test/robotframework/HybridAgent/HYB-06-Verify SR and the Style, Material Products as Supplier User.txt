***Settings***
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt

Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../Locators/Loc_HomePage.txt
Resource          ../Locators/Loc_StylePage.txt
Resource          ../Locators/Loc_SourcingPage.txt
Resource          ../Locators/Loc_MaterialPage.txt
Resource          ../Locators/Loc_MaterialLibrary.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt

***Variables***
@{OnlineSupplierUsers}  h1  i1  f1  g1
${OnlineSupplierPwd}  centric8

*** Test Cases ***
Test Setup  		
    DataSetup For New Enumeration
        
Login as h1 User and Verify Supplier Request Name, Type and State for Style
    [Tags]  Portal User Business Cases
    Open Browser To Login Page
    ${UserName} =  Get From List  ${OnlineSupplierUsers}  0
    Selenium2Library.Input Text    ${Loc_UserName}    ${UserName}
    Selenium2Library.Input Text    ${Loc_Password}    ${OnlineSupplierPwd}
    PO_LoginPage.Submit Credentials
    Wait.DOMready
    DOMready-TabLoads
    Click on Home Icon
    DOMready-TabLoads
    Click Element    ${Loc_NewUserLogin_StylesTab}
    DOMready-TabLoads
    Click Element    css=[data-csi-tab-name='Supplier Requests']
    DOMready-TabLoads
    Element Should be Visible    //td[a[text()='${SupplierRequestName1}']]/following-sibling::td[ text()='ISSUED']
    Element Should be Visible    //td[@data-csi-heading='RequestType::0'and text()='Style']
    Element Should be Visible    //td[@data-csi-heading='Node Name::0']//a[text()='${SupplierRequestName1}']
    Wait.DOMreadyByWaitTime  2
    
Verify the SR withing Setup tab for h1 user style
    [Tags]  Portal User Business Cases
    Click Element   //td[@data-csi-heading='Node Name::0']//a[text()='${SupplierRequestName1}']
    DOMready-TabLoads
    Click Element    css=[data-csi-tab-name='Supplier Quotes']
    DOMready-TabLoads
    Element Should be Visible    //a[text()='${StyleName1} - ${SupplierRequestName1}']//following::a[text()='Huckleberry-${SupplierQuotesName1}']
    Click Element    css=[data-csi-tab-name='Setup']
    DOMready-TabLoads
    Element Should be Visible    //td[@data-csi-heading='Node Name::0']//a[text()='${StyleName1} - ${SupplierRequestName1}']
    Wait.DOMreadyByWaitTime  2
    # Element Should be Visible    //td[a[text()='AUT_SR_002']]/following-sibling::td[ text()='ISSUED']
Verify Supplier Quotes Name and State, also Verify Styles Product on SR Name for h1 user
    [Tags]  Portal User Business Cases
    Click on Home Icon
    DOMready-TabLoads
    Click Element    ${Loc_NewUserLogin_StylesTab}
    DOMready-TabLoads
    Click Element    ${Loc_NewUserLogin_StylesTab_StylesTab_SupplierQuoteTab}
    DOMready-TabLoads
    Element Should be Visible    //td[@data-csi-heading='Node Name::0']//a[text()='Huckleberry-${SupplierQuotesName1}']
    Element Should be Visible    //td[a[text()='Huckleberry-${SupplierQuotesName1}']]/following-sibling::td//div[ text()='DRAFT']
    Wait.DOMreadyByWaitTime  2
    Click Element    (//span[@data-csi-tab-name='Styles'])[2]
    DOMready-TabLoads
    Element Should be Visible    //td[@data-csi-heading='Node Name:Child:SRLineItems:0{SRLineItems}']//a[text()='${StyleName1} - ${SupplierRequestName1}']
    Wait.DOMreadyByWaitTime  2
    Close Browser


Login as i1 User and Verify Supplier Request Name, Type and State for Style
    [Tags]  Portal User Business Cases
    SuiteSetUp.Open Browser To Login Page
    ${UserName} =  Get From List  ${OnlineSupplierUsers}  1
    Selenium2Library.Input Text    ${Loc_UserName}    ${UserName}
    Selenium2Library.Input Text    ${Loc_Password}    ${OnlineSupplierPwd}
    PO_LoginPage.Submit Credentials
    Wait.DOMready
    DOMready-TabLoads
    Click on Home Icon
    DOMready-TabLoads
    Click Element    ${Loc_NewUserLogin_StylesTab}
    DOMready-TabLoads
    Click Element    css=[data-csi-tab-name='Supplier Requests']
    DOMready-TabLoads
    Element Should be Visible    //td[a[text()='${SupplierRequestName2}']]/following-sibling::td[ text()='ISSUED']
    Element Should be Visible    //td[@data-csi-heading='RequestType::0'and text()='Style']
    Element Should be Visible    //td[@data-csi-heading='Node Name::0']//a[text()='${SupplierRequestName2}']
    Wait.DOMreadyByWaitTime  2
    
Verify the SR withing Setup tab for i1 user style
    [Tags]  Portal User Business Cases
    Click Element   //td[@data-csi-heading='Node Name::0']//a[text()='${SupplierRequestName2}']
    DOMready-TabLoads
    Click Element    css=[data-csi-tab-name='Supplier Quotes']
    DOMready-TabLoads
    Element Should be Visible    //a[text()='${StyleName1} - ${SupplierRequestName2}']//following::a[text()='Iceplant-${SupplierQuotesName2}']
    Wait.DOMreadyByWaitTime    2
    Click Element    css=[data-csi-tab-name='Setup']
    DOMready-TabLoads
    Element Should be Visible    //td[@data-csi-heading='Node Name::0']//a[text()='${StyleName1} - ${SupplierRequestName2}']
    Wait.DOMreadyByWaitTime  2
       
Verify Supplier Quotes Name and State, also Verify Styles Product on SR Name for i1 user
    [Tags]  Portal User Business Cases
    Click on Home Icon
    DOMready-TabLoads
    Click Element    ${Loc_NewUserLogin_StylesTab}
    DOMready-TabLoads
    Click Element    ${Loc_NewUserLogin_StylesTab_StylesTab_SupplierQuoteTab}
    DOMready-TabLoads
    Element Should be Visible    //td[@data-csi-heading='Node Name::0']//a[text()='Iceplant-${SupplierQuotesName2}']
    Element Should be Visible    //td[a[text()='Iceplant-${SupplierQuotesName2}']]/following-sibling::td//div[ text()='DRAFT']
    Click Element    (//span[@data-csi-tab-name='Styles'])[2]
    DOMready-TabLoads
    Element Should be Visible    //td[@data-csi-heading='Node Name:Child:SRLineItems:0{SRLineItems}']//a[text()='${StyleName1} - ${SupplierRequestName2}']
    Wait.DOMreadyByWaitTime  2
    Close Browser    

    
Login as h1 User and Verify Supplier Request Name, Type and State for Material
    [Tags]  Portal User Business Cases
    Open Browser To Login Page
    ${UserName} =  Get From List  ${OnlineSupplierUsers}  0
    Selenium2Library.Input Text    ${Loc_UserName}    ${UserName}
    Selenium2Library.Input Text    ${Loc_Password}    ${OnlineSupplierPwd}
    PO_LoginPage.Submit Credentials
    Wait.DOMready
    DOMready-TabLoads
    Click on Home Icon
    DOMready-TabLoads
    Click Element    ${Loc_NewUserLogin_MaterialsTab}
    DOMready-TabLoads
    Click Element    css=[data-csi-tab-name="Supplier Requests"]
    DOMready-TabLoads
    Element Should be Visible    //td[a[text()='${SupplierRequestName3}']]/following-sibling::td[ text()='ISSUED']
    Element Should be Visible    //td[@data-csi-heading='RequestType::0'and text()='Material']
    Element Should be Visible    //td[@data-csi-heading='Node Name::0']//a[text()='${SupplierRequestName3}']
    Wait.DOMreadyByWaitTime  2
    
Verify the SR withing Setup tab for h1 user Material
    [Tags]  Portal User Business Cases
    Click Element   //td[@data-csi-heading='Node Name::0']//a[text()='${SupplierRequestName3}']
    DOMready-TabLoads
    Click Element    css=[data-csi-tab-name='Supplier Quotes']
    DOMready-TabLoads
    Element Should be Visible    //a[text()='${MaterialName1} - ${SupplierRequestName3}']//following::a[text()='Huckleberry-${SupplierQuotesName3}']
    Wait.DOMreadyByWaitTime  2
    Click Element    css=[data-csi-tab-name='Setup']
    DOMready-TabLoads
    Element Should be Visible    //td[@data-csi-heading='Node Name::0']//a[text()='${MaterialName1} - ${SupplierRequestName3}']
    Wait.DOMreadyByWaitTime  2

Verify Supplier Quotes Name and State, also Verify Material Product on SR Name for h1 user    
    [Tags]  Portal User Business Cases
    Click on Home Icon
    DOMready-TabLoads
    Click Element    ${Loc_NewUserLogin_MaterialsTab}
    DOMready-TabLoads
    Click Element  css=[data-csi-tab-name="Supplier Quotes"] 
    DOMready-TabLoads
    Element Should be Visible    //td[@data-csi-heading='Node Name::0']//a[text()='Huckleberry-${SupplierQuotesName3}']
    Close Browser

    
Login as i1 User and Verify Supplier Request Name, Type and State for Material
    [Tags]  Portal User Business Cases
    Open Browser To Login Page
    ${UserName} =  Get From List  ${OnlineSupplierUsers}  1
    Selenium2Library.Input Text    ${Loc_UserName}    ${UserName}
    Selenium2Library.Input Text    ${Loc_Password}    ${OnlineSupplierPwd}
    PO_LoginPage.Submit Credentials
    Wait.DOMready
    DOMready-TabLoads
    Click on Home Icon
    DOMready-TabLoads
    Click Element    ${Loc_NewUserLogin_MaterialsTab}
    DOMready-TabLoads
    Click Element    css=[data-csi-tab-name="Supplier Requests"]
    DOMready-TabLoads
    Element Should be Visible    //td[a[text()='${SupplierRequestName4}']]/following-sibling::td[ text()='ISSUED']
    Element Should be Visible    //td[@data-csi-heading='RequestType::0'and text()='Material']
    Element Should be Visible    //td[@data-csi-heading='Node Name::0']//a[text()='${SupplierRequestName4}']
    Wait.DOMreadyByWaitTime  2
    
Verify the SR withing Setup tab for i1 user Material
    [Tags]  Portal User Business Cases
    Click Element    //td[@data-csi-heading='Node Name::0']//a[text()='${SupplierRequestName4}']
    DOMready-TabLoads
    Click Element    css=[data-csi-tab-name='Supplier Quotes']
    DOMready-TabLoads
    Element Should be Visible    //a[text()='${MaterialName2} - ${SupplierRequestName4}']//following::a[text()='Iceplant-${SupplierQuotesName4}']
    Wait.DOMreadyByWaitTime  2
    Click Element    css=[data-csi-tab-name='Setup']
    DOMready-TabLoads
    Element Should be Visible    //td[@data-csi-heading='Node Name::0']//a[text()='${MaterialName2} - ${SupplierRequestName4}']
    Wait.DOMreadyByWaitTime  2

Verify Supplier Quotes Name and State, also Verify Material Product on SR Name for i1 user
    [Tags]  Portal User Business Cases
    Click on Home Icon
    DOMready-TabLoads
    Click Element    ${Loc_NewUserLogin_MaterialsTab}
    DOMready-TabLoads
    Click Element  css=[data-csi-tab-name="Supplier Quotes"] 
    DOMready-TabLoads 
    Element Should be Visible    //td[@data-csi-act='Node Name::0']//a[text()='Iceplant-${SupplierQuotesName4}']
    Wait.DOMreadyByWaitTime  2
    Close Browser

*** Keywords ***

DataSetup For New Enumeration  
	${This_DataProvider} =  Data_Provider.DataProvider  Create a New Enumeration
    Set Suite Variable  ${This_DataProvider}
    
    ${StyleNameList} =  Data_Provider.GetDataProviderColumnValue  Data_StyleName
    ${StyleName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${StyleNameList}  1
    Set Suite Variable  ${StyleName1}
    
    ${MaterialNameList} =  Data_Provider.GetDataProviderColumnValue  Data_MaterialName
    ${MaterialName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MaterialNameList}  1
    ${MaterialName2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MaterialNameList}  2
    Set Suite Variable  ${MaterialName1}
    Set Suite Variable  ${MaterialName2}
    
    ${SupplierRequestList} =  Data_Provider.GetDataProviderColumnValue  Data_SupplierRequestName
    ${SupplierRequestName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SupplierRequestList}  1
    ${SupplierRequestName2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SupplierRequestList}  2 
    ${SupplierRequestName3} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SupplierRequestList}  3
    ${SupplierRequestName4} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SupplierRequestList}  4    
    Set Suite Variable    ${SupplierRequestName1}
    Set Suite Variable    ${SupplierRequestName2}
    Set Suite Variable    ${SupplierRequestName3}
    Set Suite Variable    ${SupplierRequestName4}
    
    ${SupplierQuotesList} =  Data_Provider.GetDataProviderColumnValue  Data_SupplierQuoteName
    ${SupplierQuotesName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SupplierQuotesList}  1
    ${SupplierQuotesName2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SupplierQuotesList}  2 
    ${SupplierQuotesName3} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SupplierQuotesList}  3
    ${SupplierQuotesName4} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SupplierQuotesList}  4     
    Set Suite Variable    ${SupplierQuotesName1}
    Set Suite Variable    ${SupplierQuotesName2}
    Set Suite Variable    ${SupplierQuotesName3}
    Set Suite Variable    ${SupplierQuotesName4}
     


    
    



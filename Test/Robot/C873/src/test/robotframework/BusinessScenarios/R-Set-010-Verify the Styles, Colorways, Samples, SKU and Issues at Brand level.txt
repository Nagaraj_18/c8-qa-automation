*** Settings ***

Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt
Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_StylePage.txt 
Resource          ../PageObjects/PO_BusinessObjectPage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../PageObjects/PO_SourcingPage.txt
Resource          ../PageObjects/PO_SpecificationPage.txt
Resource          ../PageObjects/PO_Calendar.txt
Resource          ../Locators/Loc_MaterialPage.txt
Resource          ../Locators/Loc_StyleSeasonsPage.txt
Resource          ../Locators/Loc_SpecificationSizeRangePage.txt
Resource          ../Locators/Loc_SpecificationSizesPage.txt

***Variables***
${Issue1}  AUT_Issue1
${Issue2}  AUT_Issue2

*** Test Cases ***
#1.	Create Season Hierarchy, Create Style, Add Size Range, Create Colorway and SKU for style"
#"2  Create Material, Add size range to it, Create Colored Material and SKU for material

Test Setup1  
    [Tags]  Brand Hierarchy      
    DataSetup CreateStyleBS

Login To Application
    [Tags]  Brand Hierarchy
	#Data provider: It reads the data from the excel sheet for the test case specified. 
	${This_DataProvider} =  Data_Provider.DataProvider  Create Style BS
    Set Suite Variable  ${This_DataProvider}
    Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    Wait.DOMready
    DOMready-TabLoads

Verify the Styles at Brand level
    [Tags]  Brand Hierarchy
    PO_SetupPage.Click on Site Search Field and Hit Search  Season   ${SeasonName1}
    DOMreadyByWaitTime  4 
    PO_SetupPage.Click on Required Value From Site Search Result  1
    Wait.DOMready-TabLoads
    Click Element  css=[data-csi-tab-name="Season"]
    DOMready-TabLoads  
    Execute Javascript    window.document.evaluate('//div[@class="csi-view-title csi-view-title-Season-Category1s"]', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);  
    Click Element  //a[@class='browse' and text()='${BrandName1}']
    DOMready-TabLoads
    # Click Element  css=[data-csi-tab-name="Category1-Base"]
    # DOMready-TabLoads    
    Click Element  css=[data-csi-tab-name="Styles"]
    DOMready-TabLoads
    Element Should Be Visible  //a[@class='browse' and text()='${StyleName1}']
Verify the Colorways at Brand level  
    [Tags]  Brand Hierarchy  
    Click Element  css=[data-csi-tab-name="Colorways"]
    DOMready-TabLoads
    Element Should Be Visible  //a[@class='browse' and text()='${ColorWay1}']
    Element Should Be Visible  //a[@class='browse' and text()='${ColorWay2}'] 
    Element Should Be Visible  //a[@class='browse' and text()='${ColorWay3}'] 
     
Verify the Issues at Brand level 
    [Tags]  Brand Hierarchy   
    Click Element  css=[data-csi-tab-name="Issues"]
    DOMready-TabLoads
    Element Should Be Visible  //a[@class='browse' and text()='${Issue1}-Copy']
    ${Element} =  Get Required Element  //a[@class='browse' and text()='${Issue2}']  0  
    Element Should Be Visible  ${Element}     
Verify SKU at Brand level
    [Tags]  Brand Hierarchy
    Click Element  css=[data-csi-tab-name="Style SKUs"]
    DOMready-TabLoads
    Click Element  //*[@data-csi-automation='filter-StructureItem-AllSKUs-LocalizedName*/en:Child:RealizedSize']  
    Wait.DOMreadyByWaitTime  4
    Click On the Search Value  A 
    #Press Key  //input[@class="dijitReset dijitInputInner"]  \\08
    Wait.DOMreadyByWaitTime  3
    #Click Checkbox Item in DDL Within a PopUp  All
    #Click Checkbox Item in DDL Within a PopUp  ${Sizes1} 
    Click Element  //label[@class='dijitMenuItemLabel' and contains(text(),'${Sizes1}')]/preceding-sibling::div/input
    Wait.DOMreadyByWaitTime  3
    Click Element  css=[data-csi-tab-name="Style SKUs"]
    DOMready-TabLoads
    Element Should Be Visible  //a[@class='browse' and contains(text(),'-${ColorWay1}-${Sizes1}')] 
    Element Should Be Visible  //a[@class='browse' and contains(text(),'-${ColorWay2}-${Sizes1}')] 
    Element Should Be Visible  //a[@class='browse' and contains(text(),'-${ColorWay3}-${Sizes1}')]
    
    Click Element  //*[@data-csi-automation='filter-StructureItem-AllSKUs-LocalizedName*/en:Child:RealizedSize']  
    Wait.DOMreadyByWaitTime  2
    Click On the Search Value  All 
    #Press Key  //input[@class="dijitReset dijitInputInner"]  \\08
    Wait.DOMreadyByWaitTime  3
    Click Checkbox Item in DDL Within a PopUp  All
    Wait.DOMreadyByWaitTime  2
    Click On the Search Value  A
    Wait.DOMreadyByWaitTime  2
    #Click Checkbox Item in DDL Within a PopUp  ${Sizes1} 
    Click Element  //label[@class='dijitMenuItemLabel' and contains(text(),'${Sizes2}')]/preceding-sibling::div/input
    Wait.DOMreadyByWaitTime  3
    Click Element  css=[data-csi-tab-name="Style SKUs"]
    DOMready-TabLoads
    Element Should Be Visible  //a[@class='browse' and contains(text(),'-${ColorWay1}-${Sizes2}')] 
    Element Should Be Visible  //a[@class='browse' and contains(text(),'-${ColorWay2}-${Sizes2}')] 
    Element Should Be Visible  //a[@class='browse' and contains(text(),'-${ColorWay3}-${Sizes2}')]
    
    Click Element  //*[@data-csi-automation='filter-StructureItem-AllSKUs-LocalizedName*/en:Child:RealizedSize']  
    Wait.DOMreadyByWaitTime  2
    Click On the Search Value  All
    Wait.DOMreadyByWaitTime  2
    Click Checkbox Item in DDL Within a PopUp  All
    Wait.DOMreadyByWaitTime  2
    Click On the Search Value  A
    Wait.DOMreadyByWaitTime  2
    #Click Checkbox Item in DDL Within a PopUp  ${Sizes1} 
    Click Element  //label[@class='dijitMenuItemLabel' and contains(text(),'${Sizes3}')]/preceding-sibling::div/input
    Wait.DOMreadyByWaitTime  3
    Click Element  css=[data-csi-tab-name="Style SKUs"]
    DOMready-TabLoads
    Element Should Be Visible  //a[@class='browse' and contains(text(),'-${ColorWay1}-${Sizes3}')] 
    Element Should Be Visible  //a[@class='browse' and contains(text(),'-${ColorWay2}-${Sizes3}')] 
    Element Should Be Visible  //a[@class='browse' and contains(text(),'-${ColorWay3}-${Sizes3}')]
Test Setup2  
    [Tags]  Brand Hierarchy      
    DataSetup CreateStyleSamples 
Verify the Samples at Brand Level  
    [Tags]  Brand Hierarchy      
    PO_SetupPage.Click on Site Search Field and Hit Search  Season   ${SeasonName1}
    DOMreadyByWaitTime  4 
    PO_SetupPage.Click on Required Value From Site Search Result  1
    Wait.DOMready-TabLoads
    Click Element  css=[data-csi-tab-name="Season"]
    DOMready-TabLoads  
    Execute Javascript    window.document.evaluate('//div[@class="csi-view-title csi-view-title-Season-Category1s"]', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);  
    Click Element  //a[@class='browse' and text()='${BrandName1}']
    DOMready-TabLoads
    # Click Element  css=[data-csi-tab-name="Category1-Base"]
    # DOMready-TabLoads
    Click Element  css=[data-csi-tab-name="Samples"]
    DOMready-TabLoads
    Click Element  //*[@data-csi-automation='filter-StructureItem-AllSamples-LocalizedName*/en:Child:ProductSize']  
    Wait.DOMreadyByWaitTime  2
    Click On the Search Value  A
    Wait.DOMreadyByWaitTime  2
    #Click Checkbox Item in DDL Within a PopUp  All
    Wait.DOMreadyByWaitTime  2
    #Click Checkbox Item in DDL Within a PopUp  ${Sizes1} 
    Click Element  //label[@class='dijitMenuItemLabel' and contains(text(),'${Sizes1}')]/preceding-sibling::div/input
    Wait.DOMreadyByWaitTime  3
    Click Element  css=[data-csi-tab-name="Samples"]
    DOMready-TabLoads
    Element Should Be Visible  //a[@class='browse' and text()='Sample2-${Supplier1}-${Sizes1}-1'] 
    Element Should Be Visible  //a[@class='browse' and text()='Sample2-${Supplier1}-${Sizes1}-2']
    
    Element Should Be Visible  //a[@class='browse' and text()='Sample2-${Supplier1}-${ColorWay1}-${Sizes1}-1'] 
    Element Should Be Visible  //a[@class='browse' and text()='Sample2-${Supplier1}-${ColorWay2}-${Sizes1}-2']
    Element Should Be Visible  //a[@class='browse' and text()='Sample2-${Supplier1}-${ColorWay1}-${Sizes1}-1']
    Element Should Be Visible  //a[@class='browse' and text()='Sample2-${Supplier1}-${ColorWay2}-${Sizes1}-2']
    
    Click Element  //*[@data-csi-automation='filter-StructureItem-AllSamples-LocalizedName*/en:Child:ProductSize']  
    Wait.DOMreadyByWaitTime  2
    Click On the Search Value  All
    Wait.DOMreadyByWaitTime  2
    Click Checkbox Item in DDL Within a PopUp  All
    Wait.DOMreadyByWaitTime  2
    Click On the Search Value  A
    Wait.DOMreadyByWaitTime  2
    #Click Checkbox Item in DDL Within a PopUp  ${Sizes1} 
    Click Element  //label[@class='dijitMenuItemLabel' and contains(text(),'${Sizes2}')]/preceding-sibling::div/input
    Wait.DOMreadyByWaitTime  3
    Click Element  css=[data-csi-tab-name="Samples"]
    DOMready-TabLoads
    Element Should Be Visible  //a[@class='browse' and text()='Sample2-${Supplier1}-${Sizes2}-1'] 
    Element Should Be Visible  //a[@class='browse' and text()='Sample2-${Supplier1}-${Sizes2}-2']
    
    Element Should Be Visible  //a[@class='browse' and text()='Sample2-${Supplier1}-${ColorWay1}-${Sizes2}-1'] 
    Element Should Be Visible  //a[@class='browse' and text()='Sample2-${Supplier1}-${ColorWay2}-${Sizes2}-2']
    Element Should Be Visible  //a[@class='browse' and text()='Sample2-${Supplier1}-${ColorWay1}-${Sizes2}-1']
    Element Should Be Visible  //a[@class='browse' and text()='Sample2-${Supplier1}-${ColorWay2}-${Sizes2}-2']
    
  [Teardown]    Close Browser 
*** Keywords ***
DataSetup CreateStyleBS
  ${This_DataProvider} =  Data_Provider.DataProvider    Create Style BS
  Set Suite Variable  ${This_DataProvider}
  Log  ${This_DataProvider}
 
  ${StyleNameList} =  Data_Provider.GetDataProviderColumnValue  Data_StyleName
  ${StyleName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${StyleNameList}  1
  Set Suite Variable  ${StyleName1}

  ${SeasonList} =  Data_Provider.GetDataProviderColumnValue  Data_SeasonName
  ${SeasonName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SeasonList}  1    
  Set Suite Variable  ${SeasonName1}
  
  ${BrandList} =  Data_Provider.GetDataProviderColumnValue  Data_BrandName
  ${BrandName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${BrandList}  1    
  Set Suite Variable  ${BrandName1}
  
  ${DepartmentList} =  Data_Provider.GetDataProviderColumnValue  Data_DepartmentName
  ${DepartmentName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${DepartmentList}  1    
  Set Suite Variable  ${DepartmentName1}
  
  ${CollectionList} =  Data_Provider.GetDataProviderColumnValue  Data_CollectionName
  ${CollectionName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${CollectionList}  1    
  Set Suite Variable  ${CollectionName1}
  
  ${ColorWayList} =  Data_Provider.GetDataProviderColumnValue  Data_ColorWayName
  ${ColorWay1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${ColorWayList}  1 
  ${ColorWay2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${ColorWayList}  2  
  ${ColorWay3} =  Data_Provider.DataProviderSplitterForMultipleValues  ${ColorWayList}  3 
  Set Suite Variable  ${ColorWay1}
  Set Suite Variable  ${ColorWay2}
  Set Suite Variable  ${ColorWay3}
  
  ${SizesList} =  Data_Provider.GetDataProviderColumnValue  Data_SizeName
  ${Sizes1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SizesList}  1 
  ${Sizes2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SizesList}  2 
  ${Sizes3} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SizesList}  3  
  Set Suite Variable  ${Sizes1}
  Set Suite Variable  ${Sizes2}
  Set Suite Variable  ${Sizes3}
  
DataSetup CreateStyleSamples
  ${This_DataProvider} =  Data_Provider.DataProvider    Create Style Samples
  Set Suite Variable  ${This_DataProvider}
  Log  ${This_DataProvider}
 
  ${StyleNameList} =  Data_Provider.GetDataProviderColumnValue  Data_StyleName
  ${StyleName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${StyleNameList}  1
  Set Suite Variable  ${StyleName1}

  ${SeasonList} =  Data_Provider.GetDataProviderColumnValue  Data_SeasonName
  ${SeasonName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SeasonList}  1    
  Set Suite Variable  ${SeasonName1} 
  
  ${BrandList} =  Data_Provider.GetDataProviderColumnValue  Data_BrandName
  ${BrandName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${BrandList}  1    
  Set Suite Variable  ${BrandName1}
 
  ${ColorWayList} =  Data_Provider.GetDataProviderColumnValue  Data_ColorWayName
  ${ColorWay1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${ColorWayList}  1 
  ${ColorWay2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${ColorWayList}  2  
  Set Suite Variable  ${ColorWay1}
  Set Suite Variable  ${ColorWay2}
 
  ${SizesList} =  Data_Provider.GetDataProviderColumnValue  Data_SizeName
  ${Sizes1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SizesList}  1 
  ${Sizes2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SizesList}  2   
  Set Suite Variable  ${Sizes1}
  Set Suite Variable  ${Sizes2}
  
  ${SupplierList} =  Data_Provider.GetDataProviderColumnValue  Data_SupplierName
  ${Supplier1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SupplierList}  1 
  ${Supplier2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SupplierList}  2   
  Set Suite Variable  ${Supplier1}
  Set Suite Variable  ${Supplier2}

*** Settings ***
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../Locators/Loc_SourcingSetupPage.txt
Resource          ../Locators/Loc_MaterialPage.txt
# Resource          ../Locators/Loc_Routing.txt
# Resource          ../Locators/Loc_SourcingSetupPage.txt
# Resource          ../Locators/Loc_MaterialLibrary.txt
Resource          ../Locators/Loc_StyleSeasonsPage.txt
Resource          ../Locators/Loc_SpecificationSizeRangePage.txt
Resource          ../Locators/Loc_SpecificationSizesPage.txt
Resource          ../PageObjects/PO_SourcingPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt

***Variables***
${Loc_Specification_SpecTab}    css=[data-csi-tab-name='Spec']
${Loc_Routing_AddCapabilityDownButton}    css=[data-csi-automation='plugin-RoutingRevision-Items-ToolbarNewActions'] .dijitArrowButton
${Loc_Specification_TemplateTab}    css=[data-csi-tab-name='ApparelViews-SpecTemplates']
${Loc_SpecSectionDefinition}    css=[data-csi-tab-name='ApparelViews-SpecificationSectionDefinitions']
${Loc_Action_New_SpecSectionDefinition}    css=[data-csi-automation='plugin-ApparelViews-SpecificationSectionDefinitions-ToolbarNewActions']
${Loc_Action_New_Spec_DataSheet}    css=[data-csi-automation='plugin-Material-SpecificationDataSheets-ToolbarNewActions']
${Loc_Spec_DataSheet_CreateAction}    css=[data-csi-automation='plugin-SpecificationDataSheetRevision-Items-ToolbarNewActionsCreate']
*** Test Cases ***
Test Setup        
    DataSetup Create Operation Group
    
Create Specification
    [Tags]  Material Specification
    Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    Wait.DOMready
    DOMready-TabLoads
    Click on Home Icon
    DOMready-TabLoads
    Click Element  //span[@title="Data Setup"]
    DOMreadyByWaitTime    2
    Click Element  css=[data-csi-act="DataSetupSpecification"]
    DOMready-TabLoads
    Click Element  //span[@class='MuiTypography-root MuiTypography-body1 MuiTypography-noWrap' and text()='Product Specification Setup']
    DOMreadyByWaitTime    3
    Click Element from List    //span[text()='RF_CareLabel']    1
    DOMready-TabLoads
    Wait.DOMreadyByWaitTime  5
    # # Click Element    ${Loc_SpecificationTab}
    # # DOMready-TabLoads
    # # ${ScreenSizeISSmall}=  Run KeyWord And Return Status  Click On Right Arrow Button To View Hidden Tabs  1
    # # Log  HorizandalScrolbar:${ScreenSizeISSmall}  console=True
    # # Wait.DOMreadyByWaitTime  5
    # # Click Element    ${Loc_Specification_TemplateTab}
    # # DOMready-TabLoads
    # # Click Element    ${Loc_SpecSectionDefinition}
    # # DOMready-TabLoads
    Click Element    ${Loc_Action_New_SpecSectionDefinition}
    DOMready-TabLoads
    PO_SetupPage.Verify Dialog Window  New Spec Section Definition
    Wait.DOMreadyByWaitTime  5
    # # Click Element    css=[data-csi-automation='field-SpecificationSectionDefinition-Form-Subtype'] .dijitArrowButton
    # # Wait.DOMreadyByWaitTime  5
    # # Select value from the DDL    ${SpecsItemName1} 
    # # Wait.DOMreadyByWaitTime  5
    # # Click Element    css=[data-csi-automation="field-SpecificationSectionDefinition-Form-Node Name"] .dijitInputInner
    # Wait.DOMreadyByWaitTime  5  
    Input Text    ${Loc_PopUpNameTextBox}    ${SpecItemTemplateName1}
    # Wait.DOMreadyByWaitTime  5
    Click on Save Button
    Wait.DOMready-PopUpCloses
    Click Element from List    //span[text()='RF_Packaging']    1
    DOMready-TabLoads
    Wait.DOMreadyByWaitTime  5
    Click Element    ${Loc_Action_New_SpecSectionDefinition}
    DOMready-TabLoads
    PO_SetupPage.Verify Dialog Window  New Spec Section Definition
    Wait.DOMreadyByWaitTime  5
    # Wait.DOMreadyByWaitTime  5
    # Click Element    css=[data-csi-automation='field-SpecificationSectionDefinition-Form-Subtype'] .dijitArrowButton
    # Wait.DOMreadyByWaitTime  5
    # Select value from the DDL    ${SpecsItemName2} 
    # Wait.DOMreadyByWaitTime  5
    # Click Element    css=[data-csi-automation="field-SpecificationSectionDefinition-Form-Node Name"] .dijitInputInner
    # Wait.DOMreadyByWaitTime  5
    Input Text    ${Loc_PopUpNameTextBox}       ${SpecItemTemplateName2}  
    # Wait.DOMreadyByWaitTime  5
    Click On Save Button
    Wait.DOMready-PopUpCloses
    # Close Browser
    
Enter into Existing Material
    [Tags]  Material Specification    
    # Open Browser To Login Page
    # PO_LoginPage.Input UserName    Data_UserName
    # PO_LoginPage.Input Password    Data_Password
    # PO_LoginPage.Submit Credentials
    # Wait.DOMready
    DOMready-TabLoads
    Click on Home Icon
    DOMready-TabLoads
    Click Element    ${Loc_MaterialTab}
    DOMready-TabLoads
    Click Element    ${Loc_Material_MaterialTab}
    DOMready-TabLoads
    Click Element    //td[@data-csi-act='Node Name::0']//a[contains(text(),'${MaterialName1}')]
    DOMready-TabLoads
Navigate to Specification tab-Spec
    [Tags]  Material Specification
    Click Element    ${Loc_MaterialSpecificationTab}
    DOMready-TabLoads
    Click Element    ${Loc_Specification_SpecTab}
    DOMready-TabLoads 
    Click Element    ${Loc_Action_New_Spec_DataSheet}
    DOMready-TabLoads
    
Creating Data Sheet for Carelabel type
    [Tags]  Material Specification
    PO_SetupPage.Verify Dialog Window  New Spec Data Sheet
    Wait.DOMreadyByWaitTime  5
    Click Element    css=[data-csi-automation='field-SpecificationDataSheet-Form-Subtype'] .dijitArrowButton  
    Wait.DOMreadyByWaitTime  5
    Select value from the DDL    ${SpecsItemName1}  
    Wait.DOMreadyByWaitTime  5
    Click Element  css=[data-csi-automation='field-SpecificationDataSheet-Form-Node Name'] .dijitInputInner
    Wait.DOMreadyByWaitTime  5
    Input Text    ${Loc_PopUpNameTextBox}   SpecItems1
    Wait.DOMreadyByWaitTime  5
    Click On Save Button
    Wait.DOMready-PopUpCloses
    # Wait.DOMreadyByWaitTime  5 
    # Click Element   //td[@data-csi-act='Node Name::0']//a[contains(text(),'SpecItems1')]    
    # DOMready-TabLoads 
    Close Browser
    
Click on Select Spec Section and Choose CL Section
    [Tags]  Material Specification
    Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    Wait.DOMready
    DOMready-TabLoads
    Click on Home Icon
    DOMready-TabLoads
    Click Element    ${Loc_MaterialTab}
    DOMready-TabLoads
    Click Element    ${Loc_Material_MaterialTab}
    DOMready-TabLoads
    Click Element    //td[@data-csi-act='Node Name::0']//a[contains(text(),'${MaterialName1}')]
    DOMready-TabLoads
    Click Element    ${Loc_MaterialSpecificationTab}
    DOMready-TabLoads
    Click Element    ${Loc_Specification_SpecTab}
    DOMready-TabLoads 
    Click Element from List   //td[@data-csi-act='Node Name::0']//a[contains(text(),'SpecItems1')]    -1    
    Wait.DOMready
    Click Element    ${Loc_Spec_DataSheet_CreateAction} 
    Wait.DOMreadyByWaitTime  5
    Click Element From List    css=[data-csi-act='AggregateSpecificationSectionDefinition'] .dijitMenuItemLabel    -1
    Wait.DOMreadyByWaitTime  5
    PO_SetupPage.Verify Dialog Window  Select Spec Section
    Wait.DOMreadyByWaitTime  5
    Select Single Matching Checkbox elements in the popup-(div-Element)  ${SpecItemTemplateName1}
    Wait.DOMreadyByWaitTime  5
    Click On Save Button
    Wait.DOMready-PopUpCloses   
     
Create a Custom Spec section 'Custom Spec Section' 
    [Tags]  Material Specification
    Click Element    ${Loc_Spec_DataSheet_CreateAction}
    Wait.DOMreadyByWaitTime  5
    Click Element From List    css=[data-csi-act="NewSpecificationSection"] .dijitMenuItemLabel    -1
    Wait.DOMreadyByWaitTime  5
    PO_SetupPage.Verify Dialog Window  Create Custom Spec Section
    Wait.DOMreadyByWaitTime  5
    Click Element    css=[data-csi-automation='field-SpecificationSection-Form-Node Name'] .dijitInputInner
    Wait.DOMreadyByWaitTime  5
    Input Text    ${Loc_PopUpNameTextBox}    Custom
    Wait.DOMreadyByWaitTime  5
    Click On Save Button
    Wait.DOMReady-PopUpCloses
    Wait.DOMreadyByWaitTime  2

Click on create -> New from Spec item and Choose Spec Item of care label type into both the sections
    [Tags]  Material Specification
    Click Element    ${Loc_Spec_DataSheet_CreateAction}
    Wait.DOMreadyByWaitTime  5
    Click Element From List    css=[data-csi-act='NewFromSpecificationItemDefinition']    -1
    PO_SetupPage.Verify Dialog Window  New from Spec Item
    Wait.DOMreadyByWaitTime  5
    Click Element    css=[data-csi-automation='field-SpecificationDataSheetRevision-SpecItemSelectionForm-SpecificationSection'] .dijitArrowButton
    Wait.DOMreadyByWaitTime    5
    Select value from the DDL    ${SpecItemTemplateName1}
    Wait.DOMreadyByWaitTime  2
    PO_NewCreationPopUpPage.Click on the DDL button  field-SpecificationDataSheetRevision-SpecItemSelectionForm-SpecItems
    Wait.DOMreadyByWaitTime  5
    Click Element    //label[@class='dijitMenuItemLabel' and text()='${SpecItemName1}']/preceding-sibling::div/input
    Wait.DOMreadyByWaitTime  5
    Click Element    css=[data-csi-automation='field-SpecificationDataSheetRevision-SpecItemSelectionForm-SpecItems'] .dijitArrowButton
    Wait.DOMreadyByWaitTime  5
    # Click Element    css=[data-csi-automation='field-SpecificationDataSheetRevision-SpecItemSelectionForm-SpecItems'] .dijitArrowButton
    # Wait.DOMreadyByWaitTime  5
    # Click Element    //label[@class='dijitMenuItemLabel' and text()='${SpecItemName1}']/preceding-sibling::div/input
    # Wait.DOMreadyByWaitTime  5
    # Click Element    css=[data-csi-automation='field-SpecificationDataSheetRevision-SpecItemSelectionForm-SpecItems'] .dijitArrowButton
    # Wait.DOMreadyByWaitTime    2
    Click On Save Button
    Wait.DOMReady-PopUpCloses
    Wait.DOMreadyByWaitTime    2
    Click Element    ${Loc_Spec_DataSheet_CreateAction}
    Wait.DOMreadyByWaitTime  5
    Click Element From List    css=[data-csi-act='NewFromSpecificationItemDefinition']    -1
    PO_SetupPage.Verify Dialog Window  New from Spec Item
    Wait.DOMreadyByWaitTime  5
    Click Element    css=[data-csi-automation='field-SpecificationDataSheetRevision-SpecItemSelectionForm-SpecificationSection'] .dijitArrowButton
    Wait.DOMreadyByWaitTime    5
    Select value from the DDL    Custom  
    Wait.DOMreadyByWaitTime  5
    PO_NewCreationPopUpPage.Click on the DDL button  field-SpecificationDataSheetRevision-SpecItemSelectionForm-SpecItems
    Wait.DOMreadyByWaitTime  5
    Click Element    //label[@class='dijitMenuItemLabel' and text()='${SpecItemName1}']/preceding-sibling::div/input
    Wait.DOMreadyByWaitTime  5
    Click Element    css=[data-csi-automation='field-SpecificationDataSheetRevision-SpecItemSelectionForm-SpecItems'] .dijitArrowButton
    Wait.DOMreadyByWaitTime  5
    # Click Element    css=[data-csi-automation='field-SpecificationDataSheetRevision-SpecItemSelectionForm-SpecItems'] .dijitArrowButton
    # Wait.DOMreadyByWaitTime  5
    # Click Element    //label[@class='dijitMenuItemLabel' and text()='${SpecItemName1}']/preceding-sibling::div/input
    # Wait.DOMreadyByWaitTime  5
    # Click Element    css=[data-csi-automation='field-SpecificationDataSheetRevision-SpecItemSelectionForm-SpecItems'] .dijitArrowButton
    # Wait.DOMreadyByWaitTime    2
    Click On Save Button
    Wait.DOMReady-PopUpCloses
    Wait.DOMreadyByWaitTime  2
    Close Browser
 Create a Data Sheet of Packaging type
    [Tags]  Material Specification
    # Click Element    //div[@class='crumb crumbSearch']//a[text()='Material_01']
    Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    Wait.DOMready
    DOMready-TabLoads
    Click on Home Icon
    DOMready-TabLoads
    Click Element    ${Loc_MaterialTab}
    DOMready-TabLoads
    Click Element    ${Loc_Material_MaterialTab}
    DOMready-TabLoads
    Click Element    //td[@data-csi-act='Node Name::0']//a[contains(text(),'${MaterialName1}')]
    DOMready-TabLoads
    DOMready-TabLoads
    Click Element    ${Loc_MaterialSpecificationTab}
    DOMready-TabLoads
    Click Element    ${Loc_Specification_SpecTab}
    DOMready-TabLoads
    Click Element    ${Loc_Action_New_Spec_DataSheet} 
    DOMready-TabLoads
    PO_SetupPage.Verify Dialog Window  New Spec Data Sheet    
    Wait.DOMreadyByWaitTime  5
    Click Element    css=[data-csi-automation='field-SpecificationDataSheet-Form-Subtype'] .dijitArrowButton  
    Wait.DOMreadyByWaitTime  5
    Select value from the DDL    ${SpecsItemName2}  
    Click Element  css=[data-csi-automation='field-SpecificationDataSheet-Form-Node Name'] .dijitInputInner
    Wait.DOMreadyByWaitTime  5
    Input Text    ${Loc_PopUpNameTextBox}   SpecItems2
    Wait.DOMreadyByWaitTime  5
    Click On Save Button
    Wait.DOMreadyByWaitTime  2
    Wait.DOMReady-PopUpCloses
    Click Element From List    //td[@data-csi-act='Node Name::0']//a[contains(text(),'SpecItems2')]    -1
    DOMready-TabLoads 
    
Click on Select Spec Section and Choose PK Section
    [Tags]  Material Specification
    Click Element    ${Loc_Spec_DataSheet_CreateAction} 
    Wait.DOMreadyByWaitTime  5
    Click Element From List    css=[data-csi-act='AggregateSpecificationSectionDefinition'] .dijitMenuItemLabel    -1
    Wait.DOMreadyByWaitTime  5
    PO_SetupPage.Verify Dialog Window  Select Spec Section
    Wait.DOMreadyByWaitTime  5
    Select Single Matching Checkbox elements in the popup-(div-Element)  ${SpecItemTemplateName2}
    Wait.DOMreadyByWaitTime  5
    Click On Save Button
    Wait.DOMready-PopUpCloses    

Create a Custome Spec Section 'Custom Spec Section'    
    [Tags]  Material Specification
    Click Element    ${Loc_Spec_DataSheet_CreateAction}
    Wait.DOMreadyByWaitTime  5
    Click Element From List    css=[data-csi-act="NewSpecificationSection"] .dijitMenuItemLabel    -1
    Wait.DOMreadyByWaitTime  5
    PO_SetupPage.Verify Dialog Window  Create Custom Spec Section
    Wait.DOMreadyByWaitTime  5
    Click Element    css=[data-csi-automation='field-SpecificationSection-Form-Node Name'] .dijitInputInner
    Wait.DOMreadyByWaitTime  5
    Input Text    ${Loc_PopUpNameTextBox}    Custom
    Wait.DOMreadyByWaitTime  5
    Click On Save Button
    Wait.DOMReady-PopUpCloses
    Click Element    ${Loc_Spec_DataSheet_CreateAction}
    Wait.DOMreadyByWaitTime  2
    
Click on create-> New from Spec Item and Choose Spec Item of packaging type into both the sections 
    [Tags]  Material Specification
    Click Element From List    css=[data-csi-act='NewFromSpecificationItemDefinition']    -1
    Wait.DOMreadyByWaitTime  5
    PO_SetupPage.Verify Dialog Window  New from Spec Item
    Wait.DOMreadyByWaitTime  2
    Click Element    css=[data-csi-automation='field-SpecificationDataSheetRevision-SpecItemSelectionForm-SpecificationSection'] .dijitArrowButton
    Wait.DOMreadyByWaitTime    5
    Select value from the DDL    ${SpecItemTemplateName2}
    Wait.DOMreadyByWaitTime  2
    Click Element    css=[data-csi-automation='field-SpecificationDataSheetRevision-SpecItemSelectionForm-SpecItems'] .dijitArrowButton
    Wait.DOMreadyByWaitTime  5
    Click Element    //label[@class='dijitMenuItemLabel' and text()='${SpecItemName2}']/preceding-sibling::div/input
    Wait.DOMreadyByWaitTime  5
    Click Element    css=[data-csi-automation='field-SpecificationDataSheetRevision-SpecItemSelectionForm-SpecItems'] .dijitArrowButton
    Wait.DOMreadyByWaitTime  5
    # Click Element    css=[data-csi-automation='field-SpecificationDataSheetRevision-SpecItemSelectionForm-SpecItems'] .dijitArrowButton
    # Wait.DOMreadyByWaitTime  5
    # Click Element    //label[@class='dijitMenuItemLabel' and text()='${SpecItemName2}']/preceding-sibling::div/input
    # Wait.DOMreadyByWaitTime  2
    # Click Element    css=[data-csi-automation='field-SpecificationDataSheetRevision-SpecItemSelectionForm-SpecItems'] .dijitArrowButton
    # Wait.DOMreadyByWaitTime    5
    Click On Save Button
    Wait.DOMReady-PopUpCloses
    Wait.DOMreadyByWaitTime    2
    Click Element    ${Loc_Spec_DataSheet_CreateAction}
    Wait.DOMreadyByWaitTime  5
    Click Element From List    css=[data-csi-act='NewFromSpecificationItemDefinition']    -1
    Wait.DOMreadyByWaitTime  5
    PO_SetupPage.Verify Dialog Window  New from Spec Item
    Wait.DOMreadyByWaitTime  5
    Click Element    css=[data-csi-automation='field-SpecificationDataSheetRevision-SpecItemSelectionForm-SpecificationSection'] .dijitArrowButton
    Wait.DOMreadyByWaitTime    5
    Select value from the DDL    Custom
    Wait.DOMreadyByWaitTime  5
    Click Element    css=[data-csi-automation='field-SpecificationDataSheetRevision-SpecItemSelectionForm-SpecItems'] .dijitArrowButton
    Wait.DOMreadyByWaitTime  5
    Click Element    //label[@class='dijitMenuItemLabel' and text()='${SpecItemName2}']/preceding-sibling::div/input
    Wait.DOMreadyByWaitTime  5
    Click Element    css=[data-csi-automation='field-SpecificationDataSheetRevision-SpecItemSelectionForm-SpecItems'] .dijitArrowButton
    Wait.DOMreadyByWaitTime  5
    # Click Element    css=[data-csi-automation='field-SpecificationDataSheetRevision-SpecItemSelectionForm-SpecItems'] .dijitArrowButton
    # Wait.DOMreadyByWaitTime  5
    # Click Element    //label[@class='dijitMenuItemLabel' and text()='${SpecItemName2}']/preceding-sibling::div/input
    # Wait.DOMreadyByWaitTime  5
    # Click Element    css=[data-csi-automation='field-SpecificationDataSheetRevision-SpecItemSelectionForm-SpecItems'] .dijitArrowButton
    # Wait.DOMreadyByWaitTime    2
    Click On Save Button
    Wait.DOMReady-PopUpCloses
    Close Browser 

    
*** Keywords ***
DataSetup Create Operation Group
  ${This_DataProvider} =  Data_Provider.DataProvider    Create Operation Group
  Set Suite Variable  ${This_DataProvider}
  Log  ${This_DataProvider}
  
  ${MaterialNameList} =  Data_Provider.GetDataProviderColumnValue  Data_MaterialName
  ${MaterialName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MaterialNameList}  1
  Set Suite Variable    ${MaterialName1}
  ${SpecsItemTypeList} =  Data_Provider.GetDataProviderColumnValue    Data_SpecLibraryItemType
  ${SpecsItemName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SpecsItemTypeList}  1
  ${SpecsItemName2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SpecsItemTypeList}  2
  Set Suite Variable    ${SpecsItemName1}
  Set Suite Variable    ${SpecsItemName2}
  ${SpecItemTemplateList} =    Data_Provider.GetDataProviderColumnValue    Data_SpecItemTemplateName
  ${SpecItemTemplateName1} =    Data_Provider.DataProviderSplitterForMultipleValues  ${SpecItemTemplateList}  1
  ${SpecItemTemplateName2} =    Data_Provider.DataProviderSplitterForMultipleValues  ${SpecItemTemplateList}  2
  Set Suite Variable    ${SpecItemTemplateName1}
  Set Suite Variable    ${SpecItemTemplateName2}
  ${SpecItemTypeList} =   Data_Provider.GetDataProviderColumnValue    Data_SpecItemName    
  ${SpecItemName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SpecItemTypeList}  1
  ${SpecItemName2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SpecItemTypeList}  2
  Set Suite Variable    ${SpecItemName1}
  Set Suite Variable    ${SpecItemName2}

*** Settings ***
Test Setup         Setup The Variables
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt

Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_TablePage.txt
Resource          ../PageObjects/PO_SourcingPage.txt
Resource          ../PageObjects/PO_ExtendedFunctionsPage.txt
Resource		  ../Locators/Loc_SourcingFactoryPage.txt
Resource          ../Locators/Loc_CollectionManagement.txt

*** Variables ***
${Factory1ID}  F-01
${Factory1State}  Pending
${Factory2ID}  F-02
${Factory2State}  Approved
${Factory3ID}  F-03
${Factory3State}  Inactive

${DeleteConfirmation}  Item deleted successfully.
${Contact1}  AUT_Contact1
${Contact2}  AUT_Contact2
${Contact3}  AUT_Contact2-COPY
${ComplianceDocument1}  AUT_CompDoc1
${ComplianceDocument2}  AUT_CompDoc2
${Loc_TextArea-FactoryName}  css=[data-csi-automation="edit-LibSourcing-Factories-Node Name:"]
*** Test Cases ***
Sourcing-00388
	[tags]  Sanity_DataLoad
	#Data provider: It reads the data from the excel sheet for the test case specified. 
	${This_DataProvider} =  Data_Provider.DataProvider  Create Factory
	Set Suite Variable  ${This_DataProvider}
	Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    #Reload
    Wait.DOMready
    DOMready-TabLoads
    Click on Home Icon
    DOMready-TabLoads
    Click Element  ${Loc_Home_SourcingTab}
    DOMready-TabLoads
    Click Element  ${Loc_Home-Sourcing-Factory}
    DOMready-TabLoads	
    # Step : Create Factory
    :FOR  ${i}  IN RANGE  0  4
    \   #PO_SetupPage.Mouse Over Actions DDL Button	1
    \   Mouse Over  ${Loc_ActionsFactory}
    \   #PO_SetupPage.Wait for the required New Creation options on mouse over-ActionsDDL	${Loc_NewFactory_Link}  
    \   #PO_SetupPage.Click on required option after mouse over-ActionsDDL	${Loc_NewFactory_Link}
    \   Click Element  ${Loc_ActionsFactory}
    \	Wait.DomreadyByWaitTime	4
    \	${FactoryNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_FactoryName
    \   ${Index} =  Evaluate  ${i} + 1
    \   ${FactoryName} =  DataProviderSplitterForMultipleValues  ${FactoryNameCollection}  ${Index}
    \   PO_SourcingPage.Enter the Factory value in Text Box	 ${FactoryName}
    \   Wait For the Entered Values in TextArea to GetUpdate  LibSourcing-Factories  ${Loc_TextArea-FactoryName}
    \  Click Element  ${Loc_Home-Sourcing-Factory}
    \  DOMready-TabLoads
  
    PO_SourcingPage.Update ID For Required Factory	${FactoryName1}	 ${Factory1ID}	
    Wait.DomreadyByWaitTime	1
    Click Element  ${Loc_Home-Sourcing-Factory}
    DOMready-TabLoads
    PO_SourcingPage.Update State For Required Factory	${FactoryName1}	 ${Factory1State}	
    Wait.DomreadyByWaitTime	1
    Click Element  ${Loc_Home-Sourcing-Factory}
    DOMready-TabLoads
    PO_SourcingPage.Update Country of Origin For Required Factory	${FactoryName1}  ${CountryName1}
    Wait.DomreadyByWaitTime	1
    Click Element  ${Loc_Home-Sourcing-Factory}
    DOMready-TabLoads
    PO_SourcingPage.Update Suppliers For Required Factory	${FactoryName1}	 ${SupplierName1}
    Wait.DomreadyByWaitTime	1
    Click Element  css=[data-csi-tab='LibSourcing-PurchasedOrderTab']
    DOMready-TabLoads
    Click Element  ${Loc_Home-Sourcing-Factory}
    DOMready-TabLoads
    PO_SourcingPage.Update Port of Origin For Required Factory	${FactoryName1}  ${ShippingPortName3}	
    Wait.DomreadyByWaitTime	1
    Click Element  ${Loc_Home-Sourcing-Factory}
    DOMready-TabLoads
        
    # Step 6: update values for 2nd Factory        
    PO_SourcingPage.Update ID For Required Factory	${FactoryName2} 	${Factory2ID}	
    Wait.DomreadyByWaitTime	1
    Click Element  ${Loc_Home-Sourcing-Factory}
    DOMready-TabLoads
    PO_SourcingPage.Update State For Required Factory	${FactoryName2} 	${Factory2State}	
    Wait.DomreadyByWaitTime	1
    Click Element  ${Loc_Home-Sourcing-Factory}
    DOMready-TabLoads
    PO_SourcingPage.Update Country of Origin For Required Factory	${FactoryName2}  ${CountryName1}
    Wait.DomreadyByWaitTime	1
    Click Element  ${Loc_Home-Sourcing-Factory}
    DOMready-TabLoads
    PO_SourcingPage.Update Suppliers For Required Factory	${FactoryName2}  ${SupplierName3}
    Wait.DomreadyByWaitTime	1
    #Click Element  ${Loc_Home-Sourcing-Factory}
    Click Element  css=[data-csi-tab='LibSourcing-PurchasedOrderTab']
    DOMready-TabLoads
    Click Element  ${Loc_Home-Sourcing-Factory}
    DOMready-TabLoads
    PO_SourcingPage.Update Port of Origin For Required Factory	${FactoryName2}  ${ShippingPortName2}	
    Wait.DomreadyByWaitTime	1
    Click Element  ${Loc_Home-Sourcing-Factory}
    DOMready-TabLoads
    
    # Step 7: update values for 3rd Factory    
    PO_SourcingPage.Update ID For Required Factory	${FactoryName3}  ${Factory3ID}	
    Wait.DomreadyByWaitTime	1 
    Click Element  css=[data-csi-tab='LibSourcing-PurchasedOrderTab']
    DOMready-TabLoads
    Click Element  ${Loc_Home-Sourcing-Factory}
    DOMready-TabLoads
    PO_SourcingPage.Update State For Required Factory	${FactoryName3}  ${Factory3State}	
    Wait.DomreadyByWaitTime	1
    Click Element  css=[data-csi-tab='LibSourcing-PurchasedOrderTab']
    DOMready-TabLoads
    Click Element  ${Loc_Home-Sourcing-Factory}
    DOMready-TabLoads
    PO_SourcingPage.Update Country of Origin For Required Factory	${FactoryName3}  ${CountryName2}
    Wait.DomreadyByWaitTime	1
    Click Element  css=[data-csi-tab='LibSourcing-PurchasedOrderTab']
    DOMready-TabLoads
    Click Element  ${Loc_Home-Sourcing-Factory}
    DOMready-TabLoads
    PO_SourcingPage.Update Port of Origin For Required Factory	${FactoryName3}  ${ShippingPortName1}	
    Wait.DomreadyByWaitTime	1
    Click Element  //div[contains(@class,'csi-view-LibSourcing-Factories')]//div[contains(text(),'Displaying')]
	Wait.DomreadyByWaitTime	2
	Click Element  ${Loc_Home-Sourcing-Factory}
    DOMready-TabLoads
    Wait.DomreadyByWaitTime	3

Sourcing-00389
#delete icon validation
#Check for the delete icon; hit the icon and check if the user is prompted for a confirmation to delete the record. Hit Yes and check if the record is deleted.
  Mouse Over  ${Loc_ActionsFactory}
  #PO_SetupPage.Wait for the required New Creation options on mouse over-ActionsDDL	${Loc_NewFactory_Link}  
  #PO_SetupPage.Click on required option after mouse over-ActionsDDL	${Loc_NewFactory_Link}
  Click Element  ${Loc_ActionsFactory}
  Wait.DomreadyByWaitTime	4
  PO_SourcingPage.Enter the Factory value in Text Box	Test Factory
  Click Element  ${Loc_Home-Sourcing-Factory}
  DOMready-TabLoads
  Mouse Over  //td//a[text()="Test Factory"]//following::td//span[@data-csi-act="Delete"]
  Click Element     //td//a[text()="Test Factory"]//following::td//span[@data-csi-act="Delete"]
  Wait Until Page Contains Element  //span[@class='dijitDialogTitle' and text()='Delete Factory?']
  Click Yes No Buttons  Delete
  Wait Until Page Contains Element  //span[text()='${DeleteConfirmation}']
  Element Should Not Be Visible  //a[text()="Test Factory"]
Sourcing-00390
# click on fzctory and verofy tabs 
#Click on a Factory and check for seven tabs Properties, Suppliers, Capabilities, Contractual Documents, Docs & Comments, Where Used and Audit, with the Properties tab being the default

  Click Element  //a[text()="${FactoryName4}"]
   DOMready-TabLoads
  Element Should Be visible   ${Loc_FactoryNode_PropertiesTab}  
  Element Should Be visible   ${Loc_FactoryNode_SuppliersTab}  
  Element Should Be visible   ${Loc_FactoryNode_CapabilitiesTab}  
  Element Should Be visible   ${Loc_FactoryNode_ContractualDocumentTab}  
  Element Should Be visible   ${Loc_FactoryNode_FactoryReviewsTab} 
  # Thsi Tab is not visible now
  #Element Should Be visible   ${Loc_FactoryNode_ReviewReportTab}  
  Element Should Be visible   ${Loc_FactoryNode_Docs&CommentsTab}  
  Element Should Be visible   ${Loc_FactoryNode_WhereUsedTab}  
  Element Should Be visible   ${Loc_FactoryNode_AuditTab}  
  Element Should Be Visible   ${Loc_FactoryNode_PropertiesTab_DefaultSelected}
  
Sourcing-00391
#Quickly verify that the Properties section displays the attributes and the correct values. Edit the value for the 'State' field and check if the dropdown displays entries Pending, Approved and Inactive

  Element Should Be Visible  //td[@data-csi-act="Node Name::0" and text()="${FactoryName4}"]
  #Element Should Be Visible  //td[@data-csi-heading="Country::0"]//a[text()="${CountryName1}"]
  #Element Should Be Visible  //td[@data-csi-act="Suppliers::0"]//a[text()="${SupplierName1}"]
  Mouse Over  ${Loc_FactoryNode_PropertiesTab_StateField}
  Click Element  ${Loc_FactoryNode_PropertiesTab_StateField}
  Wait.DomreadyByWaitTime	3
  @{StateAttributes} =  Create List  Pending  Approved    
  ${Index} =  Set Variable  0
  :FOR  ${ListItems}  IN  @{StateAttributes}
  \  Element Should Be visible  //div[@class="dijitReset dijitMenuItem" and text()="${ListItems}"]
  Element Should Be visible  //div[@class="dijitReset dijitMenuItem" and text()="Inactive"]
  Click Element  ${Loc_FactoryNode_PropertiesTab} 
  


Sourcing-00392
# create contact 
#Also create a Contact and update the necessary attributes. Check if the up/down arrows, copy and delete icon are functioning properly

  Mouse Over  ${Loc_FactoryNode_PropertiesTab_Actions}  
  #Wait Until Element is Visible  ${Loc_FactoryNode_PropertiesTab_Actions_NewContactLink}
  Click Element  ${Loc_FactoryNode_PropertiesTab_Actions}
   DOMreadyByWaitTime  4
   #Selenium2Library.Input Text  //div//following::textrarea[@data-csi-automation="edit-SourcingItem-Contacts-Node Name:"]  ${Contact1}
  
   Enter Values in Text Area on a Table  ${Loc_InputContactsText}  ${Contact1}
  Wait For the Entered Values in TextArea to GetUpdate2  SourcingItem-Contacts  SourcingItem-GeneralInfo  ${Loc_InputContactsText} 
  Click Element  ${Loc_FactoryNode_PropertiesTab} 
  
  Mouse Over  ${Loc_FactoryNode_PropertiesTab_Actions} 
  #Wait Until Element is Visible  ${Loc_FactoryNode_PropertiesTab_Actions_NewContactLink}
  Click Element  ${Loc_FactoryNode_PropertiesTab_Actions}  
   DOMreadyByWaitTime  4
   #Selenium2Library.Input Text  //div//following::textrarea[@data-csi-automation="edit-SourcingItem-Contacts-Node Name:"]  ${Contact2}
  Enter Values in Text Area on a Table  ${Loc_InputContactsText}  ${Contact2}
  #Wait For the Entered Values in TextArea to GetUpdate2  SourcingItem-Contacts  SourcingItem-GeneralInfo  ${Loc_InputContactsText}  
  #Click Element  ${Loc_FactoryNode_PropertiesTab} 
  DOMreadyByWaitTime  4
  Click Element  //div[@class="csi-view-title csi-view-title-SourcingItem-Contacts"]
  DOMreadyByWaitTime  2
  Mouse Over  //td//a[text()="${Contact2}"]//following::td//span[@data-csi-act="Copy"]
  Click Element     //td//a[text()="${Contact2}"]//following::td//span[@data-csi-act="Copy"]
  DOMreadyByWaitTime  4
  #Clear Element Text  ${Loc_InputContactsText} 
  #Selenium2Library.Input Text  //div//following::textrarea[@data-csi-automation="edit-SourcingItem-Contacts-Node Name:"]  ${Contact3}
  #Enter Values in Text Area on a Table  ${Loc_InputContactsText}  ${Contact3}
  #Wait For the Entered Values in TextArea to GetUpdate2  SourcingItem-Contacts  SourcingItem-GeneralInfo  ${Loc_InputContactsText}  
  Click Element  ${Loc_FactoryNode_PropertiesTab} 
  
  Mouse Over  //td//a[text()="${Contact3}"]//following::td//span[@data-csi-act="Delete"]
  Click Element     //td//a[text()="${Contact3}"]//following::td//span[@data-csi-act="Delete"]
  Wait Until Page Contains Element  //span[@class='dijitDialogTitle' and text()='Delete Contact?']
  Click Yes No Buttons  Delete
  Wait Until Page Contains Element  //span[text()='${DeleteConfirmation}']
  Element Should Not Be Visible  //a[text()="${Contact3}"]
   
  Click Element  xpath=//a[text()='${Contact2}']//following::span[@data-csi-act='MoveUp']
  DOMreadyByWaitTime  4
  Element Should Be Visible  xpath=//table//tbody[2]//tr[1]//a[text()='${Contact2}']
  Click Element  xpath=//a[text()='${Contact2}']//following::span[@data-csi-act='MoveDown']
  DOMreadyByWaitTime  4
  Element Should Be Visible  xpath=//table//tbody[2]//tr[2]//a[text()='${Contact2}']
   
Sourcing-00393
# click on suppliers tab and verfiy links 
#Under this Suppliers tab and check if the table displays a link 'Select Suppliers'.
  Click Element  ${Loc_FactoryNode_SuppliersTab} 
  DOMready-TabLoads
  Set Focus To Element  ${Loc_FactoryNode_SuppliersTab_Actions}
  #Mouse Over   ${Loc_FactoryNode_SuppliersTab_Actions}
  #Wait Until Element is Visible  ${Loc_FactoryNode_SuppliersTab_Actions_SelectSuppliersLink}
  #Click Element  ${Loc_FactoryNode_SuppliersTab_Actions} 

Sourcing-00394
# select suppliers 
#Click on the 'Select Supplier' link and check if Supplier marked as 'Is Supplier' and both 'Is Supplier' and 'Is Agent' are displayed for selection. Select multiple records and hit Save. Check if the selected Suppliers are displayed in the table
  Click Element  ${Loc_FactoryNode_SuppliersTab} 
  DOMready-TabLoads
  Mouse Over   ${Loc_FactoryNode_SuppliersTab_Actions}
  #Wait Until Element is Visible  ${Loc_FactoryNode_SuppliersTab_Actions_SelectSuppliersLink}
  Click Element  ${Loc_FactoryNode_SuppliersTab_Actions} 
  DOMready-PopUpAppears 
  Element Should Be visible  //td[text()="${SupplierName1}"]
  Element Should Be visible  //td[text()="${SupplierName3}"]
  Click Element  //input[@title="Toggle selections of all displayed items"]
  Click On Save Button
  DOMready-PopUpCloses
  Element Should Be visible  //a[text()="${SupplierName1}"]
  Element Should Be visible  //a[text()="${SupplierName3}"]
Sourcing-00395 
# select capabilities
#Click on the 'Select Capabilities' link and check if the Capabilities created for 'Factory' are displayed for selection is a dialog box that allows multiple selection of records. Select a few records and hit Save; check if the table displays the entries correctly
   Click Element  ${Loc_FactoryNode_CapabilitiesTab} 
   DOMready-TabLoads
   Mouse Over   ${Loc_FactoryNode_CapabilitiesTab_Actions}
   #Wait Until Element is Visible  ${Loc_FactoryNode_CapabilitiesTab_Actions_NewFromCapabilityLink}
   Click Element  ${Loc_FactoryNode_CapabilitiesTab_Actions}
  
   DOMready-PopUpAppears 
  Element Should Be visible  //td[text()="${CapabilityName1}"]
  Click Element  //input[@title="Toggle selections of all displayed items"]
  Click On Save Button
  DOMready-PopUpCloses
   Element Should Be visible  //a[text()="${CapabilityName1}"]

Sourcing-00396 
# contractual documents
#Click on the Contractual Documents tabs, make sure 2 sections 'Factory Compliance Items' and Supplier Contractual Documents are displayed 

   Click Element  ${Loc_FactoryNode_ContractualDocumentTab} 
   DOMready-TabLoads
   Element Should Be Visible  ${Loc_FactoryNode_ContractualDocumentTab_FactoryComplianceItemsSection}
   Element Should Be Visible  ${Loc_FactoryNode_ContractualDocumentTab_SupplierContractualDocumentsSection}
   


Sourcing-00397 
#Create a new Factory Compliance by hitting the link. Update editable attributes and check if they are displayed correctly. 
  Click Element  ${Loc_FactoryNode_ContractualDocumentTab} 
  DOMready-TabLoads
  Mouse Over   ${Loc_FactoryNode_ContractualDocumentTab_FactoryComplianceItemsSection_Actions}
  #Wait Until Element is Visible  ${Loc_FactoryNode_ContractualDocumentTab_FactoryComplianceItemsSection_Actions_NewFactoryComplianceLink}

  Click Element  ${Loc_FactoryNode_ContractualDocumentTab_FactoryComplianceItemsSection_Actions}
  DOMreadyByWaitTime  2
   Enter Values in Text Area on a Table  ${Loc_InputContractualDocumentText}  ${ComplianceDocument1}
  Wait For the Entered Values in TextArea to GetUpdate2  Factory-Compliances  Factory-ContractualDocuments  ${Loc_InputContractualDocumentText} 
  Click Element  ${Loc_FactoryNode_ContractualDocumentTab} 
  Element Should Be Visible  //a[text()="${ComplianceDocument1}"]

Sourcing-00398 
# Check for the copy and delete icons in the Actions column. Click on each of them and check if they work correctly.
   
  Mouse Over  //td//a[text()="${ComplianceDocument1}"]//following::td//span[@data-csi-act="Copy"]
  Click Element  //td//a[text()="${ComplianceDocument1}"]//following::td//span[@data-csi-act="Copy"]
  DOMreadyByWaitTime  4
  #Enter Values in Text Area on a Table  ${Loc_InputContractualDocumentText}  ${ComplianceDocument2}
  #Wait For the Entered Values in TextArea to GetUpdate2  Factory-Compliances  Factory-ContractualDocuments  ${Loc_InputContractualDocumentText} 
  Click Element  ${Loc_FactoryNode_ContractualDocumentTab}  
  DOMreadyByWaitTime  4
  Mouse Over  //td//a[text()="${ComplianceDocument1}-COPY"]//following::td//span[@data-csi-act="Delete"]
  Click Element     //td//a[text()="${ComplianceDocument1}-COPY"]//following::td//span[@data-csi-act="Delete"]
  #Mouse Over  //td//a[text()="${ComplianceDocument2}"]//following::td//span[@data-csi-act="Delete"]
  #Click Element     //td//a[text()="${ComplianceDocument2}"]//following::td//span[@data-csi-act="Delete"]
  Wait Until Page Contains Element  //span[@class='dijitDialogTitle' and text()='Delete Factory Compliance Item?']
  Click Yes No Buttons  Delete
  Wait Until Page Contains Element  //span[text()='${DeleteConfirmation}']
  Element Should Not Be Visible  //a[text()="${ComplianceDocument2}"]
   

Sourcing-00399
# Click on compliance
#Click on the Compliance and check if the Properties and Docs & Comments tabs are displayed. Also check if the Actions dropdown display Revise, Approve, Purge Revisions and Purge and Reset Revisions. 
   Click Element  //a[text()="${ComplianceDocument1}"]
   DOMready-TabLoads
     Element Should Be Visible  ${Loc_ComplianceDocumentNode_PropertiesTab}
  Element Should Be Visible  ${Loc_ComplianceDocumentNode_Docs&DCommentsTab}
  Click Element  ${Loc_ComplianceDocumentNode_PropertiesTab}
   DOMready-TabLoads
   Click Element  ${Loc_ComplianceDocumentNode_ActionsDDL} 
  DOMreadyByWaitTime  4
  
  @{StateAttributes} =  Create List  Revise  Approve  Purge Revisions  Purge and Reset Revisions
  ${Index} =  Set Variable  0
  :FOR  ${ListItems}  IN  @{StateAttributes}
  \  Element Should Be visible  //td[@class="dijitReset dijitMenuItemLabel" and text()="${ListItems}"]
  Click Element  ${Loc_ComplianceDocumentNode_PropertiesTab}
  
Sourcing-00400
#doc & comme
#Check if the Docs & Comments feature are working as expected 
  Click Element  ${Loc_ComplianceDocumentNode_Docs&DCommentsTab}
   DOMready-TabLoads
   Create New Document   ${Document1}
  Wait.DOMreadyByWaitTime  5
  Element should be Visible  xpath=//a[@class='browse' and text()='${Document1}']
  DOMreadyByWaitTime  3

  
  #Create New Comment  NewComment  ${Comment1}
  #Mouse Over  ${Loc_NewCommentMarketingMat}
  Click Element From List  css=[data-csi-automation='plugin-CommentContainer-Comments-ToolbarNewActions']  0
  Wait Until Element is Visible  css=.dijitDialogTitle
  #DOMready-PopUpAppears
  DOMreadyByWaitTime  4
  Selenium2Library.Input Text  css=.dijitReset.dijitInputInner[name='subject']  AUT_Comment1
  Click Element  xpath=//*[@class='dijitReset dijitInline dijitButtonText' and text()='Save']
  DOMready-PopUpCloses
  Element should be Visible  xpath=//span[text()='AUT_Comment1']
   


#Sourcing-00401
#contractual doc
#Change to the Supplier Contractual Documents Section and note that the table will be empty for now. This tab will be tested a little later


Sourcing-00402
#REVIEWS 13 
#Click on the tab and verify it displays 'Reviews;Issues;Events'
   Click on Home Icon
    DOMready-TabLoads
    Click Element  ${Loc_Home_SourcingTab}
    DOMready-TabLoads
    Click Element  ${Loc_Home-Sourcing-Factory}
    DOMready-TabLoads
   Click Element  //a[text()="${FactoryName4}"]
   DOMready-TabLoads  
   
  Click Element  ${Loc_FactoryNode_FactoryReviewsTab} 
  DOMready-TabLoads
  Element Should Be Visible  ${Loc_FactoryNode_FactoryReviewsTab_ReviewTab}
  Element Should Be Visible  ${Loc_FactoryNode_FactoryReviewsTab_IssuesTab}
  Element Should Be Visible  ${Loc_FactoryNode_FactoryReviewsTab_EventsTab}    


  [Teardown]    Close Browser 
**Keywords 
Setup The Variables
     ${This_DataProvider} =  Data_Provider.DataProvider  Create Factory
  Set Suite Variable  ${This_DataProvider}
  Log  ${This_DataProvider}
    ${FactoryNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_FactoryName
    ${FactoryName1} =  DataProviderSplitterForMultipleValues  ${FactoryNameCollection}  1
    ${FactoryName2} =  DataProviderSplitterForMultipleValues  ${FactoryNameCollection}  2
    ${FactoryName3} =  DataProviderSplitterForMultipleValues  ${FactoryNameCollection}  3
    ${FactoryName4} =  DataProviderSplitterForMultipleValues  ${FactoryNameCollection}  4
    Set Suite Variable  ${FactoryName1}
    Set Suite Variable  ${FactoryName2}
    Set Suite Variable  ${FactoryName3}
    Set Suite Variable  ${FactoryName4}
    ${SupplierNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_SupplierName
    ${SupplierName1} =  DataProviderSplitterForMultipleValues  ${SupplierNameCollection}  1
    ${SupplierName3} =  DataProviderSplitterForMultipleValues  ${SupplierNameCollection}  3
    Set Suite Variable  ${SupplierName1}
    Set Suite Variable  ${SupplierName3}
    ${ShippingPortNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_ShippingPortName
    ${ShippingPortName1} =  DataProviderSplitterForMultipleValues  ${ShippingPortNameCollection}  1
    ${ShippingPortName2} =  DataProviderSplitterForMultipleValues  ${ShippingPortNameCollection}  2
    ${ShippingPortName3} =  DataProviderSplitterForMultipleValues  ${ShippingPortNameCollection}  3
    Set Suite Variable  ${ShippingPortName1}
    Set Suite Variable  ${ShippingPortName2}
    Set Suite Variable  ${ShippingPortName3}
    ${CountryNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_CountryName
    ${CountryName1} =  DataProviderSplitterForMultipleValues  ${CountryNameCollection}  1
    ${CountryName2} =  DataProviderSplitterForMultipleValues  ${CountryNameCollection}  2
    Set Suite Variable  ${CountryName1}
    Set Suite Variable  ${CountryName2}
    
    ${CapabilityNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_Capability
    ${CapabilityName1} =  DataProviderSplitterForMultipleValues  ${CapabilityNameCollection}  1
    Set Suite Variable  ${CapabilityName1}
    
    ${DocumentNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_DocumentName
    ${Document1} =  DataProviderSplitterForMultipleValues  ${DocumentNameCollection}  1
    Set Test Variable  ${Document1}
  
    ${CommentNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_CommentName
    ${Comment1} =  DataProviderSplitterForMultipleValues  ${CommentNameCollection}  1
  Set Test Variable  ${Comment1}

Wait For the Entered Values in TextArea to GetUpdate2 
  [Arguments]  ${Locator}  ${TabLocator}  ${InputType} 
   Click Element  css=[data-csi-tab='${TabLocator}']
  ${CvEnabled}=  Run Keyword And Return Status  Element Should Be Enabled  css=[data-csi-automation='plugin-${Locator}-views'] div.dijitInputField>input[role='textbox']
  Run Keyword If  '${CVEnabled}'=='False' 	Press Key	${InputType}  \\9
  Run Keyword If  '${CVEnabled}'=='False' 	Click Element  css=[data-csi-tab='${TabLocator}']
  ${CvEnabled2}=  Run Keyword And Return Status  Element Should Be Enabled  css=[data-csi-automation='plugin-${Locator}-views'] div.dijitInputField>input[role='textbox']
  Run Keyword If  '${CVEnabled2}'=='False' 	Reload	 
 

  
 
    

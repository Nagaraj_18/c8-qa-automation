*** Settings ***
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt
Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_SpecificationPage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../Locators/Loc_SpecificationTemplatesPage.txt

***variables***
${Loc_DPTemplateDialogNodename_InputTextarea}  div[data-csi-automation='field-DataPackageTemplate-Form-Node Name'] div.dijitInputField>input[name='Node Name']

*** Test Cases ***


Create Material Data Package Template

	[tags]  Sanity_DataLoad
	#Data provider: It reads the data from the excel sheet for the test case specified. 
	${This_DataProvider} =  Data_Provider.DataProvider  Create Material Data Package Template

	Set Test Variable  ${This_DataProvider}

    Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    #Reload
    Wait.DOMready
    DOMready-TabLoads
    Click on Setup Icon
    Wait.DOMready
    DOMready-TabLoads

    PO_SetupPage.Click on Home Icon
    Wait.DOMready
   
    #Step : Navigate to Specification-Templates-Spec Data Sheet Templates tab. 
    Click Element  ${Loc_SpecificationTab}
    DOMready-TabLoads
    Click Element  ${Loc_ProductAlternativeSpecificationsTab}
    DOMready-TabLoads
    ${ScreenSizeISSmall}=  Run KeyWord And Return Status  Click On Right Arrow Button To View Hidden Tabs  1
    Log  HorizandalScrolbar:${ScreenSizeISSmall}  console=True  
    Click Element  ${Loc_TemplatesTab}
    DOMready-TabLoads  
    Click Element  ${Loc_DataPackageTab}
    DOMready-TabLoads 
	
	#Step : Create a Material Data Package Template.
	# Mouse Over  ${Loc_ActionsDataPackageTab}
    Click Element  ${Loc_NewDataPackageTemplateLink}
	PO_SetupPage.Verify Dialog Window  New Data Package Template
	${MaterialDataPackageTemplateName} =  GetDataProviderColumnValue  Data_MaterialDataPackageTemplateName
	Input Text  ${Loc_TextFieldDataPackageTemplate}  ${MaterialDataPackageTemplateName}
	Wait.DOMreadyByWaitTime  1
	PO_NewCreationPopUpPage.Click on the DDL button  field-DataPackageTemplate-Form-TemplateType
	Wait.DOMreadyByWaitTime  1
	PO_NewCreationPopUpPage.Select value from the DDL  Material
	Wait.DOMreadyByWaitTime  1
	PO_NewCreationPopUpPage.Click on Save Button
	DOMready-PopUpCloses
			
	#Step : Update the Data Package Template.
	DOMreadyByWaitTime  3	
	PO_SpecificationPage.Click on the Data Package Template  ${MaterialDataPackageTemplateName}
	DOMreadyByWaitTime  3
	DOMready-TabLoads
    Mouse Over  ${Loc_ActionsDataSheetTemplatesTab} 
    Click Element  ${Loc_NewDataSheetTemplatesLink}
	PO_SetupPage.Verify Dialog Window  New Data Sheet Template
	DOMreadyByWaitTime  1
	PO_NewCreationPopUpPage.Click on the DDL button  field-DataPackageTemplateSheet-Form-Type
	Wait.DOMreadyByWaitTime  1
	PO_NewCreationPopUpPage.Select value from the DDL  Routing
	Wait.DOMreadyByWaitTime  2
	PO_NewCreationPopUpPage.Click on the DDL button  field-DataPackageTemplateSheet-Form-Phase
	Wait.DOMreadyByWaitTime  1
	PO_NewCreationPopUpPage.Select value from the DDL  Sample
	Wait.DOMreadyByWaitTime  2
	PO_NewCreationPopUpPage.Click on the DDL button  field-DataPackageTemplateSheet-Form-State
	Wait.DOMreadyByWaitTime  1
	PO_NewCreationPopUpPage.Select value from the DDL  PENDING
	Wait.DOMreadyByWaitTime  2
	Click Element  ${Loc_CheckboxDataPackageLatestOnly}
	Wait.DOMreadyByWaitTime  1
	PO_NewCreationPopUpPage.Click on Save and New Button
	DOMreadyByWaitTime  4
	PO_NewCreationPopUpPage.Click on the DDL button  field-DataPackageTemplateSheet-Form-Type
	Wait.DOMreadyByWaitTime  1
	PO_NewCreationPopUpPage.Select value from the DDL  Material BOM
	Wait.DOMreadyByWaitTime  2
	PO_NewCreationPopUpPage.Click on the DDL button  field-DataPackageTemplateSheet-Form-State
	Wait.DOMreadyByWaitTime  1
	PO_NewCreationPopUpPage.Select value from the DDL  APPROVED
	Wait.DOMreadyByWaitTime  2
	PO_NewCreationPopUpPage.Click on Save Button
	DOMready-PopUpCloses
	        
  
  Wait Until Page Contains Element  //span[contains(text(),'successfully')]
  Click on Home Icon
  DOMready-TabLoads
  #//div[@data-csi-automation="field-DataPackageTemplate-Form-Node Name"]//dfollowing::div[Contains(@class

  Wait Until Page Contains Element  //a[text()='${MaterialDataPackageTemplateName}']//parent::td
  Click Element  //a[text()='${MaterialDataPackageTemplateName}']//parent::td//following-sibling::td[contains(@class,'actionsColumn')]//div//span[@data-csi-act='Copy']
  DOMready-PopUpAppears
  Wait Until Page Contains Element  ${Loc_TextFieldDataPackageTemplate}
  Click Element  ${Loc_TextFieldDataPackageTemplate}
  Clear Element Text  ${Loc_TextFieldDataPackageTemplate}
  Input Text  ${Loc_TextFieldDataPackageTemplate}  Trial DataPackageTemplate Material 001
  Click on Save Button
  DOMready-PopUpCloses

    # Step : Close the Browser Instance.
    SuiteSetUp.Test Step    ${R_Close_Browser}
    [Teardown]    Close Browser

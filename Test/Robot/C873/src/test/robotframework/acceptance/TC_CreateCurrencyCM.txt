*** Settings ***
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt
Test Setup 			Suite Setup Currency
Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_SourcingPage.txt
Resource          ../Locators/Loc_CompanyCurrencyPage.txt
Resource          ../Locators/Loc_BusinessPlanning.txt

*** Test Cases ***

Calendar-02787-Create Currency

#Create Currency
    Open Browser To Login Page
    #Assertions.Assert the given Title of the Page    Login - Centric PLM
    # Step 2: Enter the login Credentials and Submit.
    #Step Description
    #SuiteSetUp.Test Step    ${R_Enter_Credintails}
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    Wait Until Page Contains Element   ${Loc_Release}
    Reload
    Wait.DOMready
    Click on Setup Icon
	DOMready-TabLoads
    Click Element  ${Loc_Company_Tab}
    DOMready-TabLoads  
	Click Element  ${Loc_Currency_Tab}
	DOMready-TabLoads
#Navigate to Create and Updated Details  
    # Step 4: Create Currency Type
    #Step Description    
    Mouse Over  ${Currency_Action} 
 	PO_SetupPage.Wait for the required New Creation options on mouse over-ActionsDDL	${Loc_NewCurrency_Link}    
    PO_SetupPage.Click on required option after mouse over-ActionsDDL	${Loc_NewCurrency_Link}
    DOMreadyByWaitTime  4
Business Planning-02788
    #Update editable attributes and check if the entry gets updated properly
    Input Text  ${Loc_TextAreaCurrencyName}  ${Currency1}
    DOMreadyByWaitTime  5
    Click Element  ${Loc_Currency_Tab}
    Assert for the Given Element  //td[@data-csi-heading="Node Name::0" and text()="${Currency1}"]
    Click Element From List  //td[text()='${Currency1}']/following::td[@data-csi-act='Symbol::0']  0
    Wait.DOMreadyByWaitTime  1
	Input Text 	${Loc_TextAreaSymbolName}  ${Symbol1} 
	Wait.DOMreadyByWaitTime  5
   Click Element  ${Loc_Currency_Tab}
    
Business Planning-02789
    #Check if the Active field is editable. Mark is as true/false and check if it is displayed correctly
     Click Element  //td[@data-csi-heading="Node Name::0" and text()="${Currency1}"]//following::td[2][@data-csi-heading="Active::0"]//div//input
 
    DOMReadyByWaitTime  2
    ${Check} =  Get Element Attribute  //td[@data-csi-heading="Node Name::0" and text()="${Currency1}"]//following::td[2][@data-csi-heading="Active::0"]//div//input  aria-checked
    Run Keyword If  str('${Check}') == str('False')  Log the Active Check Box is Un-selected for ${Currency1} 
    ${Check} =  Get Element Attribute  //td[@data-csi-heading="Node Name::0" and text()="${Currency1}"]//following::td[2][@data-csi-heading="Active::0"]//div//input  araia-checked
    Run Keyword If  str('${Check}') == str('true')  Log the Active Check Box is selected for ${Currency1} by Default
    
    
   Click Element  //td[@data-csi-heading="Node Name::0" and text()="${Currency1}"]//following::td[2][@data-csi-heading="Active::0"]//div//input
 
    DOMReadyByWaitTime  2
    ${Check} =  Get Element Attribute  //td[@data-csi-heading="Node Name::0" and text()="${Currency1}"]//following::td[2][@data-csi-heading="Active::0"]//div//input  araia-checked
    Run Keyword If  str('${Check}') == str('true')  Log the Active Check Box is selected for ${Currency1}
    ${Check} =  Get Element Attribute  //td[@data-csi-heading="Node Name::0" and text()="${Currency1}"]//following::td[2][@data-csi-heading="Active::0"]//div//input  aria-checked
    Run Keyword If  str('${Check}') == str('False')  Log the Active Check Box is not selected for ${Currency1}
    
Business Planning-02790
    #Create several entries and mark a few as Active=False
     Mouse Over  ${Currency_Action} 
 	PO_SetupPage.Wait for the required New Creation options on mouse over-ActionsDDL	${Loc_NewCurrency_Link}    
    PO_SetupPage.Click on required option after mouse over-ActionsDDL	${Loc_NewCurrency_Link}
    DOMreadyByWaitTime  4
    DOMreadyByWaitTime  2
    Input Text  ${Loc_TextAreaCurrencyName}  ${Currency2}
    DOMreadyByWaitTime  5
    Click Element  ${Loc_Currency_Tab}
    ## Update symbols 
    Click Element From List  //td[text()='${Currency2}']/following::td[@data-csi-act='Symbol::0']  0
    Wait.DOMreadyByWaitTime  1
	Input Text 	${Loc_TextAreaSymbolName}	${Symbol2} 
	Wait.DOMreadyByWaitTime  5
	Click Element  ${Loc_Currency_Tab}
	
	Mouse Over  ${Currency_Action} 
 	PO_SetupPage.Wait for the required New Creation options on mouse over-ActionsDDL	${Loc_NewCurrency_Link}    
    PO_SetupPage.Click on required option after mouse over-ActionsDDL	${Loc_NewCurrency_Link}
    DOMreadyByWaitTime  2
    Input Text  ${Loc_TextAreaCurrencyName}  ${Currency3}
    DOMreadyByWaitTime  5
    Click Element  ${Loc_Currency_Tab}
    ## Update symbols 
    Click Element From List  //td[text()='${Currency3}']/following::td[@data-csi-act='Symbol::0']  0
    Wait.DOMreadyByWaitTime  1
	Input Text 	${Loc_TextAreaSymbolName}	${Symbol3} 
	Wait.DOMreadyByWaitTime  2
	Click Element  ${Loc_Currency_Tab}
	# Making the Currency -Europe inactive
	Click Element  //td[@data-csi-heading="Node Name::0" and text()="${Currency3}"]//following::td[2][@data-csi-heading="Active::0"]//div//input
	
	
	Mouse Over  ${Currency_Action} 
 	PO_SetupPage.Wait for the required New Creation options on mouse over-ActionsDDL	${Loc_NewCurrency_Link}    
    PO_SetupPage.Click on required option after mouse over-ActionsDDL	${Loc_NewCurrency_Link}
    DOMreadyByWaitTime  2
    Input Text  ${Loc_TextAreaCurrencyName}  ${Currency4}
    DOMreadyByWaitTime  2
    Click Element  ${Loc_Currency_Tab}
    ## Update symbols 
    Click Element From List  //td[text()='${Currency4}']/following::td[@data-csi-act='Symbol::0']  0
    Wait.DOMreadyByWaitTime  1
	Input Text 	${Loc_TextAreaSymbolName}	${Symbol4} 
	Wait.DOMreadyByWaitTime  2
	Click Element  ${Loc_Currency_Tab}
	
Business Planning-02791
    #Check if the Actions column has the delete icon. Click on the delete icon and check if the user is prompted for a confirmation to delete the record
    #Deleting the Currency-Japan
    ${Element} =  Required Element From List  //td[@data-csi-heading="Node Name::0" and text()="${Currency4}"]//following::td//div[1]//span[1]  0
    Assert for the Given Element  ${Element} 
    Click Element  ${Element} 
    Wait Until Element is Visible  //span[text()="Delete Currency?"]
Business Planning-02792
    #Hit No and confirm that the record does not get deleted. Click on the delete icon and this time hit Yes.
    Click Element  ${Dialog_CancelButton}
    Element Should Be visible  //td[@data-csi-heading="Node Name::0" and text()="${Currency4}"]
Business Planning-02793
    #Check if the selected row gets removed and is not displayed in the table any more.
    ${Element} =  Required Element From List  //td[@data-csi-heading="Node Name::0" and text()="${Currency4}"]//following::td//div[1]//span[1]  0
    Assert for the Given Element  ${Element} 
    Click Element  ${Element} 
    Wait Until Element is Visible  //span[text()="Delete Currency?"]
    Click Element  ${Dialog_DeleteButton}
    Wait Until Element is Visible  //span[text()="Item deleted successfully."]
    Element Should Not Be visible  //td[@data-csi-heading="Node Name::0" and text()="${Currency4}"]

Exit Close Browser Execution
    SuiteSetUp.Exit Execution		
		
    # Step 5: Close the Browser Instance.
    #Test Step    ${R_Close_Browser}
    [Teardown]    Close Browser

***Keywords***
Suite Setup Currency 
  ${This_DataProvider} =  Data_Provider.DataProvider  Create Currency
  Set Test Variable  ${This_DataProvider}
  ${Currency_Collection} =  GetDataProviderColumnValue  Data_Currency
  ${Currency1} =  DataProviderSplitterForMultipleValues  ${Currency_Collection}  1
  Set Test Variable  ${Currency1}
  ${Currency2} =  DataProviderSplitterForMultipleValues  ${Currency_Collection}  2
  Set Test Variable  ${Currency2}
    ${Currency3} =  DataProviderSplitterForMultipleValues  ${Currency_Collection}  3
  Set Test Variable  ${Currency3}
    ${Currency4} =  DataProviderSplitterForMultipleValues  ${Currency_Collection}  4
  Set Test Variable  ${Currency4}
  ${Symbol_Collection} =  GetDataProviderColumnValue  Data_Symbol
  ${Symbol1} =  DataProviderSplitterForMultipleValues  ${Symbol_Collection}  1
  Set Test Variable  ${Symbol1}
  ${Symbol2} =  DataProviderSplitterForMultipleValues  ${Symbol_Collection}  2
  Set Test Variable  ${Symbol2}
    ${Symbol3} =  DataProviderSplitterForMultipleValues  ${Symbol_Collection}  3
  Set Test Variable  ${Symbol3}
    ${Symbol4} =  DataProviderSplitterForMultipleValues  ${Symbol_Collection}  4
  Set Test Variable  ${Symbol4}
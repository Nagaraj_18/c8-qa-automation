*** Settings ***
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt

Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_SpecificationPage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../Locators/Loc_SpecificationSizeRangePage.txt
Resource          ../Locators/Loc_SpecificationSizesPage.txt
***Variables***
${Loc_NewSizeRangeDlg-DimensionType_Textbox}  css=[data-csi-automation='field-ProductSize-Form-DimensionType'] div.dijitInputField>input[type='text']
${Loc_NewSizeRangeDlg-TwoDimensional_Checkbox}  //div//input[@name='IsTwoDim']
${Loc_NewSizeRangeDlg-Dimension1Type_Textbox}  css=div[data-csi-automation='field-SizeRange-Form-Dimension1Type'] div.dijitInputField>input[type='text']
${Loc_NewSizeRangeDlg-Dimension2Type_Textbox}  css=div[data-csi-automation='field-SizeRange-Form-Dimension2Type'] div.dijitInputField>input[type='text']
${Loc_NewSizeDlg-TwoDimensional_Checkbox}  css=[data-csi-automation='field-ProductSize-Form-IsTwoDim']>input
${Loc_NewSizeDlg-Dimension1_TextArea}  css=div[data-csi-automation='field-ProductSize-Form-Dimension1Size'] div.dijitInputField>input[type='text']
${Loc_NewSizeDlg-Dimension2_TextArea}  css=div[data-csi-automation='field-ProductSize-Form-Dimension2Size'] div.dijitInputField>input[type='text']
${Loc_NewSizeDlg-Dimension1_DDL}  css=div[data-csi-automation='field-ProductSize-Form-Dimension1Size'] div.dijitDownArrowButton
${Loc_NewSizeDlg-Dimension2_DDL}  css=div[data-csi-automation='field-ProductSize-Form-Dimension2Size'] div.dijitDownArrowButton

${NewSizeDlg_DimensionTypeText}  css=[data-csi-automation='field-ProductSize-Form-DimensionType']>div.dijitInputField>input[type='text']

${InputSection}  //div[@data-csi-automation='edit-ApparelViews-SizeRanges-Sizes:']//span[@class='dijitPlaceHolder dijitInputField' and text()='Select']/preceding-sibling::input

*** Test Cases ***
Create Size Range
#Create Size Range
  [Tags]  Testw  CI_TEST
  #Data provider: It reads the data from the excel sheet for the test case specified. 
  ${This_DataProvider} =  Data_Provider.DataProvider  Create Size Range
  Set Suite Variable  ${This_DataProvider}
  # Step 1: Open the Application in the browser.
  #Step Description
  #SuiteSetUp.Test Step    ${R_Open_Browser}
  SuiteSetUp.Open Browser To Login Page
  #Assertion
  #Assertions.Assert the given Title of the Page    Login - Centric PLM
  # Step 2: Enter the login Credentials and Submit.
  #Step Description
  #SuiteSetUp.Test Step    ${R_Enter_Credintails}
  PO_LoginPage.Input UserName    Data_UserName
  PO_LoginPage.Input Password    Data_Password
  #SuiteSetUp.Test Step    ${R_Click_Login}
  PO_LoginPage.Submit Credentials
  #Wait.DOMready
  #DOMready-TabLoads
  #Check-is the DOM ready
  #Reload
  Wait.DOMready
  DOMready-TabLoads    
   Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads    
  #Step : Navigate to the Size Tab.
  #Step Description
  Click Element  ${Loc_SpecificationTab} 
  DOMready-TabLoads

  Click Element  ${Loc_SizesTab}
  DOMready-TabLoads
  

  
Create Sizes
  [Tags]  Test1  CI_TEST
  ${SizeList} =  Create List  Auto_S  Auto_M  Auto_L  Auto_XL  Auto_XXL  Auto_XXXL
  Log  ${SizeList}
  ${ListSize} =  Get Length  ${SizeList}

  :FOR  ${i}  IN RANGE  0  ${ListSize}
  \  Mouse Over  ${Actions_Size}
  \  Click Element  ${Actions_Size}
  \  #Wait Until Element Is Visible   ${Loc_NewSizeSpecification}
  \  #Click Element  ${Loc_NewSizeSpecification} 
  \  PO_SetupPage.Verify Dialog Window  New Size
  \  Wait.DOMreadyByWaitTime  2
  \  ${StringSize} =  Get From List  ${SizeList}  ${i}
  \  Input Text  ${Loc_FieldSizeName}  ${StringSize}
  \  Wait.DOMreadyByWaitTime  2
  \  Click on the DDL button  field-ProductSize-Form-DimensionType
  \  Click Element   ${NewSizeDlg_DimensionTypeText}
  \  Input Text   ${NewSizeDlg_DimensionTypeText}  Size
  #\  Wait.DOMreadyByWaitTime  2
  #\   Select value from the DDL  Size
  \  Wait.DOMreadyByWaitTime  2
  \  ${SizeSortOrder} =  Evaluate  ${i} + 300
  \  Input Text  ${Loc_FieldSortOrder}  ${SizeSortOrder}
  \  Wait.DOMreadyByWaitTime  2

  \  Run Keyword And Ignore Error  PO_NewCreationPopUpPage.Click on Save Button
  \  DOMready-PopUpCloses
Specification-03988
  [Tags]  Testw  CI_TEST
	#Step 6: Create 3 Size Ranges
    #Step Description
    Click Element  ${Loc_SizeRangeTab}
    DOMready-TabLoads 
    #PO_SetupPage.Mouse Over Actions DDL Button  3
    Set Focus To Element   ${Actions_SizeRange}
    Mouse Over  ${Actions_SizeRange}
    Click Element  ${Actions_SizeRange}
    #Wait Until Element Is Visible   css=[data-csi-act='NewSizeRange'] td.dijitMenuItemLabel
    #Click Element   css=[data-csi-act='NewSizeRange'] td.dijitMenuItemLabel
    PO_SetupPage.Verify Dialog Window  New Size Range
    :FOR  ${i}  IN RANGE  0  3
    \  ${SizeRangeNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_SizeRangeName
    \  ${Index} =  Evaluate  ${i} + 1
    \  ${SizeRangeName} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SizeRangeNameCollection}  ${Index}
    \  Click Element    ${Loc_FieldSizeRangeName}
    \  DOMreadyByWaitTime    2 
    \  Input Text  ${Loc_FieldSizeRangeName}  ${SizeRangeName}
    \   Click on the DDL button2  field-SizeRange-Form-Dimension1Type
    \   DOMreadyByWaitTime    2 
    \   Select value from the DDL  Size
    \  Wait.DOMreadyByWaitTime  2
    \  Run Keyword If  ${i} == 2  Click On Save Button
    \  Run Keyword If  ${i} == 2  Exit For Loop
    \  PO_NewCreationPopUpPage.Click on Save and New Button
    \  DOMready-WithInPopUp
    #Click Element From List  //div/span[@data-csi-act='Cancel']/descendant::span[@role='button']  -1
    #PO_NewCreationPopUpPage.Click on Cancel Button
    DOMready-PopUpCloses

	
    #Step 7: Setup the 1st Size Range 
    ${SizeRangeNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_SizeRangeName
    ${SizeRangeName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SizeRangeNameCollection}  1
    PO_SpecificationPage.Click on Sizes for the Size Range created  ${SizeRangeName1}  
    #PO_SetupPage.Verify Dialog WindowTe  Select Size Chart Size Ranges
    Wait.DOMreadyByWaitTime  3
    ${ReqEle} =  Required Element From List  ${InputSection}  -1
    Wait Until Element Is Visible  ${ReqEle}
    #Run Keyword And Ignore Error  Double Click Element  ${ReqEle}
    Input Text    ${ReqEle}  Auto_
    Wait Until Page Contains Element  //div[@class='dijitReset dijitMenuItem']//label[text()='S']
    Click Element  //div[@class='dijitReset dijitMenuItem']//label[text()='S']
    Wait.DOMreadyByWaitTime  2
    Click Element  //div[@class='dijitReset dijitMenuItem']//label[text()='M']
    Wait.DOMreadyByWaitTime  2
    Click Element  //div[@class='dijitReset dijitMenuItem']//label[text()='L']
    Wait.DOMreadyByWaitTime  2
    Click Element  ${Loc_SizeRangeTab}
    DOMready-TabLoads 
      
    PO_SpecificationPage.Click on OK FOR MATERIAL for the Size Range created  ${SizeRangeName1}
    Wait.DOMreadyByWaitTime  5
    Click Element  //a[text()='${SizeRangeName1}']/parent::td/following::td[@data-csi-act='BaseSize::0']
    Wait Until Element Is Visible  //div[text()='Auto_M']
    Click Element    //div[text()='Auto_M']
    
    #Step 8: Setup the 2nd Size Range 
    ${SizeRangeName2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SizeRangeNameCollection}  2
    PO_SpecificationPage.Click on Sizes for the Size Range created  ${SizeRangeName2}  
    Wait.DOMreadyByWaitTime  2
    
    ${ReqEle} =  Required Element From List  ${InputSection}  -1
    Run Keyword And Ignore Error    Double Click Element  ${ReqEle}
    Input Text    ${ReqEle}  Auto_
    
    Wait Until Page Contains Element  //div[@class='dijitReset dijitMenuItem']//label[text()='XL']
    Click Element  //div[@class='dijitReset dijitMenuItem']//label[text()='XL']
    Wait.DOMreadyByWaitTime  2
    Wait Until Page Contains Element  //div[@class='dijitReset dijitMenuItem']//label[text()='XXL']
    Click Element  //div[@class='dijitReset dijitMenuItem']//label[text()='XXL']
    Wait.DOMreadyByWaitTime  2
    Wait Until Page Contains Element  //div[@class='dijitReset dijitMenuItem']//label[text()='XXXL']
    Click Element  //div[@class='dijitReset dijitMenuItem']//label[text()='XXXL']
    Wait.DOMreadyByWaitTime  2
    Click Element  ${Loc_SizeRangeTab}
    DOMready-TabLoads
    Click Element  //a[text()='${SizeRangeName2}']/parent::td/following::td[@data-csi-act='BaseSize::0']
    Wait Until Element Is Visible  //div[text()='Auto_XXL']
    Click Element    //div[text()='Auto_XXL']
    Click Element  ${Loc_SizeRangeTab}
    DOMready-TabLoads
    PO_SpecificationPage.Click on OK FOR STYLE for the Size Range created  ${SizeRangeName2}
    Wait.DOMreadyByWaitTime  5    

    #Step 9: Setup the 3nd Size Range 
    ${SizeRangeName3} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SizeRangeNameCollection}  3
    PO_SpecificationPage.Click on Select Size Chart Size Ranges for the Size Range created  ${SizeRangeName3}
    DOMready-PopUpAppears
    DOMreadyByWaitTime  2
    PO_SpecificationPage.Select Sizes for the Size Range  ${SizeRangeName1}
    Wait.DOMreadyByWaitTime  2
    PO_SpecificationPage.Select Sizes for the Size Range  ${SizeRangeName2}
    Wait.DOMreadyByWaitTime  2
    PO_NewCreationPopUpPage.Click on Save Button
    DOMready-PopUpCloses

Specification-00501
  [Tags]  Test
  Click Element  ${Loc_SpecificationTab} 
  DOMready-TabLoads
  Click Element  ${Loc_SizesTab}
  DOMready-TabLoads
  Set Focus To Element   ${Actions_Size}
  Mouse Over  ${Actions_Size}
  Click Element  ${Actions_Size}
  #Wait Until Element Is Visible   ${Loc_NewSizeSpecification}
  #Click Element  ${Loc_NewSizeSpecification} 
  PO_SetupPage.Verify Dialog Window  New Size
  DOMreadyByWaitTime  2
  #Click on the DDL button  field-ProductSize-Form-DimensionType
  Click Element  ${NewSizeDlg_DimensionTypeText} 
  DOMreadyByWaitTime  3
  @{DimensionTypes} =  Create List  Size  Length  Waist
  :FOR  ${Items}  IN  @{DimensionTypes}
  \  Element Should Be Visible  //div[text()='${Items}']
  Element Should Be Visible  //th[text()='Sort Order']//following-sibling::td[@class='csiFormViewSeparator']
  Input Text  ${NewSizeDlg_DimensionTypeText}   Size
  Clear Element Text  ${Loc_FieldSortOrder}
  Input Text  ${Loc_FieldSizeName}  VerfiySize
  Click Element  //div/span[@data-csi-act='Save']/descendant::span[@role='button']
  DOMreadyByWaitTime  4
  #Page Should Contain Element  //div[text()='There were problems with the entered data. Please try again.']
  Element Should Be Visible  //div[text()='This value is required.']

Specification-00502
  [Tags]  Test
  Click Element  ${Loc_NewSizeDlg-TwoDimensional_Checkbox}
  Wait.DOMreadyByWaitTime  2
  Page Should Contain Element  ${Loc_NewSizeDlg-Dimension1_DDL}
  Page Should Contain Element  ${Loc_NewSizeDlg-Dimension2_DDL}
Specification-00503
  [Tags]  Test
  Input Text  ${Loc_FieldSizeName}  Verify Mandatory Field
  Clear Element Text  ${Loc_NewSizeDlg-Dimension1_TextArea}
  Click Element    ${Loc_FieldSizeName}
  Clear Element Text  ${Loc_NewSizeDlg-Dimension2_TextArea}
  Click Element    ${Loc_FieldSizeName}
  Log  Verify Dimensiontypes are mandatory, Try Save the dialog without entering both dimensiontype values  console=True 
  Click Element  //div/span[@data-csi-act='Save']/descendant::span[@role='button']
  DOMreadyByWaitTime  2
  Page Should Contain Element  //div/span[@data-csi-act='Save']
      
  Click Element  ${Loc_NewSizeDlg-Dimension1_TextArea}
  Input Text  ${Loc_NewSizeDlg-Dimension1_TextArea}  Auto_S (Size)
  Log  Verify both Dimensiontypes are mandatory, enter 1 dimensiontype value and click Save; confirm the dialog does not close   console=True 
  Click Element  //div/span[@data-csi-act='Save']/descendant::span[@role='button']
  DOMreadyByWaitTime  2
  Page Should Contain Element  //div/span[@data-csi-act='Save']
  Click on Cancel Button
  DOMready-PopUpCloses 
  
    @{Sizes} =  Create List  A  B  C  10  20  30
  @{DimTypes} =  Create List  Waist  Waist  Waist  Length  Length  Length
  ${order} =  Set Variable  ${777}    
  :FOR  ${i}  ${j}  IN ZIP  ${Sizes}  ${DimTypes}
  \  #Execute Javascript    window.document.evaluate('//span[@data-csi-automation="plugin-ApparelViews-Sizes-root"]', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
  \  Click Element  ${Actions_Size}
  \  PO_SetupPage.Verify Dialog Window  New Size
  \  Wait.DOMreadyByWaitTime  2
  \  ${Check} =  Get Element Attribute  //div[@data-csi-automation="field-ProductSize-Form-IsTwoDim"]//input[1]  aria-checked
  \  Run Keyword If  str("${Check}") == "false"  Click Element  ${Loc_NewSizeDlg-TwoDimensional_Checkbox}
  \  Click Element  ${Loc_NewSizeDlg-Dimension1_TextArea}
  \  Input Text  ${Loc_NewSizeDlg-Dimension1_TextArea}  ${j}
  \  Click Element   //div[@data-csi-automation='field-ProductSize-Form-IsTwoDim']//input
  \  Input Text  ${Loc_FieldSizeName}  ${i}
  \  Click Element  ${Loc_NewSizeRangeDlg-DimensionType_Textbox}
  \  Input Text  ${Loc_NewSizeRangeDlg-DimensionType_Textbox}  ${j}
  \  Wait.DOMreadyByWaitTime  2  
  \  ${Order} =  Evaluate  ${order} + 1
  \  Input Text  ${Loc_FieldSortOrder}  ${Order}
  \  Click on Save Button
  \  DOMready-PopUpCloses
  DOMreadyByWaitTime  2

      
Creation Of Sizes For 2D 
  [Tags]  Test

  @{2DSizes} =  Create List  X  Y  Z  X1  Y1
  @{Dimension1Sizes} =  Create List  A (Waist)  B (Waist)  C (Waist)  B (Waist)  C (Waist)
  @{Dimension2Sizes} =  Create List  10 (Length)   10 (Length)  10 (Length)  20 (Length)  30 (Length)
  ${items} =  Set Variable  0
  :FOR  ${i}  IN RANGE  0  5
  \  #PO_SetupPage.Click Required Link Under Actions  ${Actions_Size}  ${Loc_NewSizeSpecification}
  \  Click Element  ${Actions_Size}
  \  PO_SetupPage.Verify Dialog Window  New Size
  \  Wait.DOMreadyByWaitTime  2
  \  ${Check} =  Get Element Attribute  //div[@data-csi-automation="field-ProductSize-Form-IsTwoDim"]//input[1]  aria-checked
  \  Run Keyword If  str("${Check}") == "false"  Click Element  ${Loc_NewSizeDlg-TwoDimensional_Checkbox}
  \  ${2DSize} =  Get From List  ${2DSizes}  ${items}
  \  ${Dimension1Size} =  Get From List  ${Dimension1Sizes}  ${items}
  \  ${Dimension2Size} =  Get From List  ${Dimension2Sizes}  ${items}
  \  ${X} =  Required Element From List  ${Loc_NewSizeDlg-Dimension1_TextArea}  -1
  \  Click Element  ${X}
  \  Input Text  ${X}  ${Dimension1Size}
  \  Click Element    ${Loc_FieldSizeName}
  \  Capture Page Screenshot
  \  ${Y} =  Required Element From List  ${Loc_NewSizeDlg-Dimension2_TextArea}  -1
  \  Click Element  ${Y}
  \  Input Text  ${Y}  ${Dimension2Size}
  \  Click Element   ${Loc_FieldSizeName}
  \  Capture Page Screenshot  
  \  Input Text  ${Loc_FieldSizeName}  ${2DSize}
  \  Click On Save Button  
  \  DOMready-PopUpCloses
  \  DOMreadyByWaitTime  2
  \  ${items} =  Evaluate  ${items} + 1
    DOMreadyByWaitTime  3


Specification-03989
  [Tags]  Test
  Reload
  Wait.DOMready
  DOMready-TabLoads
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads    
   Click Element  ${Loc_SpecificationTab} 
  DOMready-TabLoads
  Click Element  ${Loc_SizeRangeTab}
  DOMready-TabLoads
  Set Focus To Element   ${Actions_SizeRange}
  Mouse Over  ${Actions_SizeRange}
  Click Element  ${Actions_SizeRange}
  #Wait Until Element Is Visible   css=[data-csi-act='NewSizeRange'] td.dijitMenuItemLabel
  #Click Element   css=[data-csi-act='NewSizeRange'] td.dijitMenuItemLabel
  PO_SetupPage.Verify Dialog Window  New Size Range

  DOMreadyByWaitTime  2
  Input Text  ${Loc_FieldSizeRangeName}  2DSizeRange(Waist&Length)
  ${Check} =  Get Element Attribute  ${Loc_NewSizeRangeDlg-TwoDimensional_Checkbox}  aria-checked
  Run Keyword If  str("${Check}") == "false"  Click Element  ${Loc_NewSizeRangeDlg-TwoDimensional_Checkbox}
  Click Element  ${Loc_NewSizeRangeDlg-Dimension1Type_Textbox}
  Input Text  ${Loc_NewSizeRangeDlg-Dimension1Type_Textbox}  Waist     
  Click Element  ${Loc_FieldSizeRangeName}
  Click Element  ${Loc_NewSizeRangeDlg-Dimension2Type_Textbox}
  Input Text  ${Loc_NewSizeRangeDlg-Dimension2Type_Textbox}  Length
  Click Element  ${Loc_FieldSizeRangeName}
  PO_NewCreationPopUpPage.Click on Save Button
  DOMready-PopUpCloses
  #Execute Javascript    window.document.evaluate('//span[@data-csi-automation="plugin-ApparelViews-SizeRanges-root"]', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
  PO_SpecificationPage.Click on Sizes for the Size Range created  2DSizeRange(Waist&Length)  
  Wait.DOMreadyByWaitTime  2
  Select the Sizes for the Size Range  X
  Wait.DOMreadyByWaitTime  2
  Select the Sizes for the Size Range  Y
  Wait.DOMreadyByWaitTime  2
  Select the Sizes for the Size Range  Z
  Wait.DOMreadyByWaitTime  2    
    Select the Sizes for the Size Range  X1
  Wait.DOMreadyByWaitTime  2    
  Select the Sizes for the Size Range  Y1
  Wait.DOMreadyByWaitTime  2  
  Click Element  ${Loc_SizeRangeTab}
  DOMready-TabLoads 
               
    #Step 10: Close the Browser Instance.
    #SuiteSetUp.Test Step    ${R_Close_Browser}
    [Teardown]    Close Browser

 ***comments

*** Settings ***
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt

Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_SpecificationPage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../PageObjects/PO_TablePage.txt
Resource          ../PageObjects/PO_ExtendedFunctionsPage.txt
Resource          ../PageObjects/PO_MaterialTypePage.txt

*** Test Cases ***
Specification-00542

#Create Care Label Libraries
	[tags]  Sanity_DataLoad
	#Data provider: It reads the data from the excel sheet for the test case specified. 
	${This_DataProvider} =  Data_Provider.DataProvider  Create Care Label Libraries
	Set Suite Variable  ${This_DataProvider}
	
    # Step 1: Open the Application in the browser.
    #Step Description
    #SuiteSetUp.Test Step    ${R_Open_Browser}
    Open Browser To Login Page
    #Assertion
    #Assertions.Assert the given Title of the Page    Login - Centric PLM
    # Step 2: Enter the login Credentials and Submit.
    #Step Description
    #SuiteSetUp.Test Step    ${R_Enter_Credintails}
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    #SuiteSetUp.Test Step    ${R_Click_Login}
    PO_LoginPage.Submit Credentials
    #Check-is the DOM ready
    #Reload
    Wait.DOMready
    DOMready-TabLoads
	Wait Until Page Contains Element   ${Loc_Release}
    #Click on Setup Icon
    #Wait.DOMready
    #DOMready-TabLoads
    #Assertion: Asserting for the 'Configuration' Tab to be Active
    #PO_SetupPage.Verify That This Tab Is Fronted    My Home
    # Step 3: Navigate to Specification-Library tab	
    #Step Description 
    PO_SetupPage.Click on Home Icon
    Wait.DOMready
    Click On Specification tab
    DOMready-TabLoads
    #PO_SpecificationPage.Click On Color Specification tab	
    Click Element  ${Loc_ProductAlternativeSpecificationsTab}
    DOMready-TabLoads
    ${ScreenSizeISSmall}=  Run KeyWord And Return Status  Click On Right Arrow Button To View Hidden Tabs  1
    Log  ${ScreenSizeISSmall}
	PO_SpecificationPage.Click On Specification-Library tab
    DOMready-TabLoads
    # Step 4: Create New Care Label Library
    #Step Description    
  
    Click Element  ${Loc_ActionsLibrariesTab} td.dijitArrowButton
    DOMreadyByWaitTime  3
    Click Element  ${Loc_ActionsLibrariesTab} [data-csi-act="NewLibCareSymbol"] .dijitMenuItemLabel
    # Assert for the Element in Actions Link  New Care Label Library...
    # Assert for the Element in Actions Link  New Color Specification Library...
    # Assert for the Element in Actions Link  New Material Library...
    # Assert for the Element in Actions Link  New Size Label Library...
    #Assert for the given element    ${Loc_NewCareLabelLibrary_link}
	#PO_SetupPage.Wait for the required New Creation options on mouse over-ActionsDDL	${Loc_NewCareLabelLibrary_link}    
    #PO_SetupPage.Click on required option after mouse over-ActionsDDL	${Loc_NewCareLabelLibrary_link}
    
    DOMready-TableCellEditableTextArea-Appears
    DOMreadyByWaitTime  3
    ${CareLabelLibraryNameCollection} =  GetDataProviderColumnValue  Data_CareLabelLibrary
    ${CareLabelLibraryName1} =  DataProviderSplitterForMultipleValues  ${CareLabelLibraryNameCollection}  1 
	Selenium2Library.Input Text  //div/textarea  ${CareLabelLibraryName1}  
    DOMreadyByWaitTime  2
    PO_SpecificationPage.Click On Specification-Library tab   
    DOMreadyByWaitTime  2 
    #Create 2nd Care Label Library at Home Level 
  
    Click Element  ${Loc_ActionsLibrariesTab} td.dijitArrowButton
    DOMreadyByWaitTime  3
    Click Element From List  ${Loc_ActionsLibrariesTab} [data-csi-act="NewLibCareSymbol"] .dijitMenuItemLabel  -1
    #PO_SetupPage.Wait for the required New Creation options on mouse over-ActionsDDL	${Loc_NewCareLabelLibrary_link}    
    #PO_SetupPage.Click on required option after mouse over-ActionsDDL	${Loc_NewCareLabelLibrary_link}
    DOMready-TableCellEditableTextArea-Appears
    DOMreadyByWaitTime  3
    ${CareLabelLibraryName2} =  DataProviderSplitterForMultipleValues  ${CareLabelLibraryNameCollection}  2 
	Selenium2Library.Input Text  //div/textarea  ${CareLabelLibraryName2}  
    DOMreadyByWaitTime  2
    PO_SpecificationPage.Click On Specification-Library tab   
    DOMreadyByWaitTime  2 
    Click Element  //*[@class='browse' and text()='${CareLabelLibraryName1}']
    DOMready-TabLoads

 Specification-00545
    
    # Step 5: Create New Care Labels for the Care Label Library
    # Mouse Over  ${Loc_ActionsCareLabelLibrary}
    Mouse Over  ${Loc_NewCareLabel_link}
    Click Element  ${Loc_NewCareLabel_link}
    #PO_SetupPage.Wait for the required New Creation options on mouse over-ActionsDDL	${Loc_NewCareLabel_link}    
    #PO_SetupPage.Click on required option after mouse over-ActionsDDL	${Loc_NewCareLabel_link}
    DOMreadyByWaitTime  3
    DOMready-TableCellEditableTextArea-Appears
    ${CareLabelNameCollection} =  GetDataProviderColumnValue  Data_LibraryElements
    ${CareLabelName1} =  DataProviderSplitterForMultipleValues  ${CareLabelNameCollection}  1 
	Selenium2Library.Input Text  //div/textarea  ${CareLabelName1} 
	DOMreadyByWaitTime  2
	PO_SpecificationPage.Click on Library-Elements tab  LibCareSymbol-LibElements
	DOMreadyByWaitTime  2
	Mouse Over  //*[@class='browse' and text()='${CareLabelName1}']//following::td[@data-csi-heading='Images::0']
	DOMreadyByWaitTime  3
	# Click Element  //*[@data-csi-act='Images::0']
	
	# DOMreadyByWaitTime  5	
	# # Upload Image  plugin-ImageBrowser-UploadOne  DOC1.PNG
	# DOMreadyByWaitTime  4 
	

	
 Specification-00544
    Reload
    DOMready-TabLoads
	Mouse Over  ${Loc_NewCareLabel_link}
	Click Element  ${Loc_NewCareLabel_link}
    #PO_SetupPage.Wait for the required New Creation options on mouse over-ActionsDDL	${Loc_NewCareLabel_link}    
    #PO_SetupPage.Click on required option after mouse over-ActionsDDL	${Loc_NewCareLabel_link}
    DOMreadyByWaitTime  3
    ${EditableAreaVisible} =  Run Keyword And Return Status  DOMready-TableCellEditableTextArea-Appears
	Run Keyword If  '${EditableAreaVisible}'=='False'  Create Carelabel
	${CareLabelNameCollection} =  GetDataProviderColumnValue  Data_LibraryElements
	${CareLabelName2} =  DataProviderSplitterForMultipleValues  ${CareLabelNameCollection}  2 
	Selenium2Library.Input Text  //div/textarea  ${CareLabelName2} 
	DOMreadyByWaitTime  2
    
	PO_SpecificationPage.Click on Library-Elements tab  LibCareSymbol-LibElements
	DOMreadyByWaitTime  2
	
	Mouse Over  ${Loc_NewCareLabel_link}
    Click Element  ${Loc_NewCareLabel_link}
    #PO_SetupPage.Wait for the required New Creation options on mouse over-ActionsDDL	${Loc_NewCareLabel_link}    
    #O_SetupPage.Click on required option after mouse over-ActionsDDL	${Loc_NewCareLabel_link}
    DOMreadyByWaitTime  3
    ${EditableAreaVisible} =  Run Keyword And Return Status  DOMready-TableCellEditableTextArea-Appears
	Run Keyword If  '${EditableAreaVisible}' == 'False'  Create Carelabel
	${CareLabelNameCollection} =  GetDataProviderColumnValue  Data_LibraryElements
	${CareLabelName1} =  DataProviderSplitterForMultipleValues  ${CareLabelNameCollection}  1
	${CareLabelName2} =  DataProviderSplitterForMultipleValues  ${CareLabelNameCollection}  2 
    ${CareLabelName3} =  DataProviderSplitterForMultipleValues  ${CareLabelNameCollection}  3 
	Selenium2Library.Input Text  //div/textarea  ${CareLabelName3} 
	DOMreadyByWaitTime  2
	PO_SpecificationPage.Click on Library-Elements tab  LibCareSymbol-LibElements
	DOMreadyByWaitTime  2
	#Make the CareLabel In-Active
	Click On Active Checkbox With Preceding Link  ${CareLabelName3}
    #Click on Select Elements and validate the available care labels
    Click Element  ${Loc_NewCareLabel_link} td.dijitArrowButton
    
    DOMreadyByWaitTime  2
    Click Element From List  ${Loc_CareLabelSelectElement}  -1
    DOMready-PopUpAppears
    Assert for the given element  //*[@class='attrString firstColumn' and text()='${CareLabelName1}']
    Assert for the given element  //*[@class='attrString firstColumn' and text()='${CareLabelName2}']
    Click on Required Button in the New Creation Pop Up  Cancel
    DOMreadyByWaitTime  3
Specification-00546
    ${CareLabelNameCollection} =  GetDataProviderColumnValue  Data_LibraryElements
	${CareLabelName1} =  DataProviderSplitterForMultipleValues  ${CareLabelNameCollection}  1
    Click on Action Icon in Table With Preceding Link  ${CareLabelName1}  Move
    DOMready-PopUpAppears
    Wait Until Element Is Visible  css=[data-csi-automation='plugin-SizeChartAdmin-Libraries-views']
    #Todo    
    ${ListEle} =  Get WebElements   //td[@data-csi-heading='Node Name::0']/preceding-sibling::td/div/input
    ${ReqEle} =  Get From List  ${ListEle}  0 
    Click Element  ${ReqEle} 
    #Click Required Element From List 
    Click on Required Button in the New Creation Pop Up  Save
    DOMreadyByWaitTime  3
    Close All Browsers 
    # Step 5: Close the Browser Instance.
    #Test Step    ${R_Close_Browser}
    [Teardown]    Close Browser
	
***Keywords***

Create Carelabel 
    PO_SpecificationPage.Click on Library-Elements tab  LibCareSymbol-LibElements
	DOMreadyByWaitTime  2
	# Mouse Over  ${Loc_ActionsCareLabelLibrary}
    PO_SetupPage.Wait for the required New Creation options on mouse over-ActionsDDL	${Loc_NewCareLabel_link}    
    PO_SetupPage.Click on required option after mouse over-ActionsDDL	${Loc_NewCareLabel_link}
    DOMreadyByWaitTime  3
	DOMready-TableCellEditableTextArea-Appears
    

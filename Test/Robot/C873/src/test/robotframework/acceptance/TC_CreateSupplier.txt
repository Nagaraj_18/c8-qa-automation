*** Settings ***
Test Setup			Setup SupplierData
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt

Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_SourcingPage.txt
Resource          ../PageObjects/PO_SpecificationPage.txt
Resource	  ../Locators/Loc_SourcingSupplierPage.txt


*** Test Cases ***
Sourcing-00334
	[tags]  Sanity_DataLoad
	Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    #Reload
    Wait.DOMready
    DOMready-TabLoads
    Click on Home Icon
    DOMready-TabLoads
    Click Element  ${Loc_Home_SourcingTab}
    DOMready-TabLoads
    PO_SourcingPage.Click on Supplier tab
    DOMready-TabLoads	   
    # Step 3: Navigate to Supplier tab 
    PO_HomePage.Click on Sourcing tab
    DOMready-TabLoads
    PO_SourcingPage.Click on Supplier tab
    DOMready-TabLoads	
    @{SupplierList1} =  Create List  ${Supplier1}  ${Supplier2}  ${Supplier3}  ${Supplier4}
     # Create 4 Suppliers on different combination 
    :FOR  ${ListItem}  IN  @{SupplierList1}
    \  Log  ${ListItem}
    \  Mouse Over  ${Loc_ActionsSupplier}   
    \  Wait.DOMreadyByWaitTime  1
    \  Click Element  ${Loc_ActionsSupplier}  
   	\  #Click Element From List  ${Loc_NewSupplierLink}  -1
   	\  PO_SetupPage.Verify Dialog Window  New Supplier
    \  Input Text  ${Loc_TextFieldSupplierName}  ${ListItem}
	\  Wait.DOMreadyByWaitTime  1
    \  Click Element  ${SupplierDlg_IsAgentAttr}
    \  Wait.DOMreadyByWaitTime  1
    \  Click Element  ${SupplierDlg_IsSupplierAttr}
    \  Wait.DOMreadyByWaitTime  1
    \  PO_NewCreationPopUpPage.Click on Save Button
    \  Wait.DOMreadyByWaitTime  5
    \  Wait Until Element Is Enabled  ${Loc_ActionsSupplier}  120 
    
Update Agent 
    # Step 4: Create Agent only 
    Mouse Over  ${Loc_ActionsSupplier} 
    DOMreadyByWaitTime  2
    #Unset Is Supplier checkbox 
    PO_SourcingPage.Click checkbox in table view  ${Supplier2}  5
    Mouse Over  ${Loc_ActionsSupplier}
    DOMreadyByWaitTime  2
    #Update ID
    PO_SourcingPage.Update ID For Required Supplier	${Supplier2}  S-02	
    Wait.DomreadyByWaitTime	2
	Click Element  ${Loc_Home-Sourcing-SupplierTab}
    DOMready-TabLoads
    #Update Status
    Mouse Over  ${Loc_ActionsSupplier} 
    PO_SourcingPage.Update State For Required Supplier	${Supplier2}  Pending
    Wait.DomreadyByWaitTime	2
	Click Element  ${Loc_Home-Sourcing-SupplierTab}
    DOMready-TabLoads
    #Update Country
    Mouse Over  ${Loc_ActionsSupplier} 
    PO_SourcingPage.Update Country For Required Supplier  ${Supplier2}  Data_CountryName  1
    Wait.DomreadyByWaitTime	2
	Click Element  ${Loc_Home-Sourcing-SupplierTab}
    DOMready-TabLoads
 	#Update Shipping Port
    Mouse Over  ${Loc_ActionsSupplier} 
    PO_SourcingPage.Update Port of Origin For Required Supplier  ${Supplier2}  Data_ShippingPortName  2
    Wait.DomreadyByWaitTime	2
Sourcing-00339
    # Verify the subtabs for Agent Only
    PO_SourcingPage.Click on the Required Supplier  Data_SupplierName  2
    DOMready-TabLoads
    DOMreadyByWaitTime  5
    @{AgentTabs} =  Create List  Properties  Suppliers  Capabilities  Contractual Documents  Where Used  Audit
    :FOR   ${ListItems}  IN  @{AgentTabs}
    \  Assertions.Assert the given tab  ${ListItems}  
Sourcing-00335
    Click on Home Icon
    DOMready-TabLoads
    Click Element  ${Loc_Home_SourcingTab}
    DOMready-TabLoads
	Click Element  ${Loc_Home-Sourcing-SupplierTab}
    DOMready-TabLoads
	DOMreadyByWaitTime  5
    Mouse Over  ${Loc_ActionsSupplier} 
    DOMreadyByWaitTime  2
    #Unset Agent 
    PO_SourcingPage.Click checkbox in table view  ${Supplier3}  4
    Mouse Over  ${Loc_ActionsSupplier} 
    DOMreadyByWaitTime  2
    #Update ID
    PO_SourcingPage.Update ID For Required Supplier	${Supplier3}  S-03	
    Wait.DomreadyByWaitTime	2
	Click Element  ${Loc_Home-Sourcing-SupplierTab}
    DOMready-TabLoads
    #Update Status
    Mouse Over  ${Loc_ActionsSupplier} 
    PO_SourcingPage.Update State For Required Supplier	${Supplier3}  Approved
    Wait.DomreadyByWaitTime	2
	Click Element  ${Loc_Home-Sourcing-SupplierTab}
    DOMready-TabLoads
    #Update Country
    Mouse Over  ${Loc_ActionsSupplier} 
    PO_SourcingPage.Update Country For Required Supplier  ${Supplier3}  Data_CountryName  2
    Wait.DomreadyByWaitTime	2
	Click Element  ${Loc_Home-Sourcing-SupplierTab}
    DOMready-TabLoads
    #Update Shipping Port
    Mouse Over  ${Loc_ActionsSupplier} 
    PO_SourcingPage.Update Port of Origin For Required Supplier  ${Supplier3}  Data_ShippingPortName  1
    
    # Verify the subtabs for Supplier only
    PO_SourcingPage.Click on the Required Supplier  Data_SupplierName  3
    DOMready-TabLoads
    DOMreadyByWaitTime  5
    @{SupplierTabs} =  Create List  Properties  Factories  Capabilities  Contractual Documents  Discounts  Docs & Comments  Where Used  Audit
    :FOR   ${ListItems}  IN  @{SupplierTabs}
    \  Assertions.Assert the given tab  ${ListItems}
            
Sourcing-00379
#Update Warehouse 
    Click on Home Icon
    DOMready-TabLoads
    Click Element  ${Loc_Home_SourcingTab}
    DOMready-TabLoads
	Click Element  ${Loc_Home-Sourcing-SupplierTab}
    DOMready-TabLoads
    #Unset both supplier agent and set warehouse to true
    PO_SourcingPage.Click checkbox in table view  ${Supplier4}  4
    #Mouse Over  ${Loc_Home-Sourcing-SupplierTab}
    Mouse Over  ${Loc_ActionsSupplier}
    Wait.DOMreadyByWaitTime  2
    PO_SourcingPage.Click checkbox in table view  ${Supplier4}  5
    #Mouse Over  ${Loc_Home-Sourcing-SupplierTab}
    Mouse Over  ${Loc_ActionsSupplier}
    Wait.DOMreadyByWaitTime  2
    PO_SourcingPage.Click checkbox in table view  ${Supplier4}  6
    #Update Id 
    Mouse Over  ${Loc_ActionsSupplier} 
    DOMreadyByWaitTime  2
    PO_SourcingPage.Update ID For Required Supplier	${Supplier4}  S-04	
    Wait.DomreadyByWaitTime	2
	Click Element  ${Loc_Home-Sourcing-SupplierTab}
    DOMready-TabLoads
    #Update State
    Mouse Over  ${Loc_ActionsSupplier} 
    PO_SourcingPage.Update State For Required Supplier	${Supplier4}  Inactive
    Wait.DomreadyByWaitTime	2
	Click Element  ${Loc_Home-Sourcing-SupplierTab}
    DOMready-TabLoads
Sourcing-00381
    #Update Country
    Mouse Over  ${Loc_ActionsSupplier} 
    PO_SourcingPage.Update Country For Required Supplier  ${Supplier4}  Data_CountryName  2
    Wait.DomreadyByWaitTime	2
	Click Element  ${Loc_Home-Sourcing-SupplierTab}
    DOMready-TabLoads
    #Update Shipping Port
    Mouse Over  ${Loc_ActionsSupplier} 
    PO_SourcingPage.Update Port of Origin For Required Supplier  ${Supplier4}  Data_ShippingPortName  1
Sourcing-00338
    # Verify the subtabs for Warehouse only 
    PO_SourcingPage.Click on the Required Supplier  Data_SupplierName  4
    DOMready-TabLoads
    DOMreadyByWaitTime  5
    @{WarehouseTabs} =  Create List  Properties  Capabilities  Docs & Comments  Where Used  Audit  Stock Orders
    :FOR   ${ListItems}  IN  @{WarehouseTabs}
    \  Assertions.Assert the given tab  ${ListItems}
Sourcing-00336
#Update Supplier and Set all Options to True      
    Click on Home Icon
    DOMready-TabLoads
    Click Element  ${Loc_Home_SourcingTab}
    DOMready-TabLoads
	Click Element  ${Loc_Home-Sourcing-SupplierTab}
    DOMready-TabLoads
    Mouse Over  ${Loc_ActionsSupplier} 
    DOMreadyByWaitTime  2
    PO_SourcingPage.Click checkbox in table view  ${Supplier1}  6
    #Update Id 
    Mouse Over  ${Loc_ActionsSupplier} 
    PO_SourcingPage.Update ID For Required Supplier	${Supplier1}  S-01	
    Wait.DomreadyByWaitTime	2
	Click Element  ${Loc_Home-Sourcing-SupplierTab}
    DOMready-TabLoads
    #Update Status
    Mouse Over  ${Loc_ActionsSupplier} 
    PO_SourcingPage.Update State For Required Supplier	${Supplier1}  Approved
    Wait.DomreadyByWaitTime	2
	Click Element  ${Loc_Home-Sourcing-SupplierTab}
    DOMready-TabLoads
    #Verify Country 
    Mouse Over  ${Loc_ActionsSupplier} 
    PO_SourcingPage.Verify Country For Required Supplier  ${Supplier1}  Data_CountryName  1
	Click Element  ${Loc_Home-Sourcing-SupplierTab}
    DOMready-TabLoads
    #Update Country 
    Mouse Over  ${Loc_ActionsSupplier} 
    PO_SourcingPage.Update Country For Required Supplier  ${Supplier1}  Data_CountryName  1
    Wait.DomreadyByWaitTime	2
	Click Element  ${Loc_Home-Sourcing-SupplierTab}
    DOMready-TabLoads
    #Update Port of Origin
    Mouse Over  ${Loc_ActionsSupplier} 
    PO_SourcingPage.Update Port of Origin For Required Supplier  ${Supplier1}  Data_ShippingPortName  3
    # Verify subtabs of the Supplier  
    PO_SetupPage.Click on Home Icon
    DOMready-TabLoads
    Click Element  ${Loc_HomeIcon_SourcingTab}
    DOMready-TabLoads
	Click Element  ${Loc_Home-Sourcing-SupplierTab}
    DOMready-TabLoads
    
Sourcing-00340
    PO_SourcingPage.Click on the Required Supplier  Data_SupplierName  1
    DOMready-TabLoads
    DOMreadyByWaitTime  5
    @{AllSupplierTabNames} =  Create List  Properties  Suppliers  Factories  Capabilities  Contractual Documents  Discounts  Docs & Comments  Where Used  Audit  Stock Orders
    @{AllSupplierTabs} =  Create List  SourcingItem-GeneralInfo  Supplier-Suppliers  Supplier-Factories  SourcingItem-Capabilities  Supplier-ContractualDocuments  Supplier-Discounts  DocumentContainer-DocumentsAndComments  Supplier-WhereUsed  Auditable-Audit  Supplier-StockOrdersLayout 
  	Set Suite Variable  @{AllSupplierTabs}
  	Set Suite Variable  @{AllSupplierTabNames}
  	:FOR  ${SubTab}  ${SubTabNames}  IN ZIP    ${AllSupplierTabs}    ${AllSupplierTabNames}
  	\  Verify the Tab Presence  ${SubTab}  ${SubTabNames} Subtab is present on Supplier node
        
Sourcing-00344
    #Create Factory for the Supplier 
    PO_SourcingPage.Click on Factories tab
    DOMready-TabLoads
    Mouse Over  ${Loc_ActionsSourcingFactory}  
   	Click Element  ${Loc_ActionsSourcingFactory} 
   	Wait.DOMreadyByWaitTime  3
    Input Text	css=[data-csi-automation='edit-Supplier-Factories-Node Name:']  Supplier_Factory
    Wait.DOMreadyByWaitTime  2
    PO_SourcingPage.Click on Factories tab
    #Step 10: Select Supplier 
	Click Element  ${Loc_SupplierSuppliersTab}
    DOMready-TabLoads
    Click Element  css=[data-csi-automation='plugin-Supplier-Suppliers-ToolbarNewActions'] td.dijitArrowButton
    Wait.DOMreadyByWaitTime  2
    Click Element  css=[data-csi-automation='plugin-Supplier-Suppliers-ToolbarNewActions'] [data-csi-act='AggregateFromSuppliers'] td.dijitMenuItemLabel
    PO_SetupPage.Verify Dialog Window  Select Suppliers
    Assertions.Assert the Element in popup dialog  ${Supplier1}
    Assertions.Assert the Element in popup dialog  ${Supplier3}
    PO_NewCreationPopUpPage.Select Checkbox in popup  ${Supplier3}
    PO_NewCreationPopUpPage.Click on Save Button
    DOMready-PopUpCloses
    
Sourcing-00346
   #Update Capabilities for the Supplier 
    PO_SourcingPage.Click on Capabilities tab  
    Wait.DomreadyByWaitTime	2 
    #Polling Click Required Action    ${Loc_Capabilities_ActionLink}    ${Loc_NewFromCapabilities_Link}
    Click Element  ${Loc_Capabilities_ActionLink}
    ${Capability_Collection} =  Data_Provider.GetDataProviderColumnValue  Data_Capability
    ${Capability1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${Capability_Collection}  1
    PO_SetupPage.Verify Dialog Window  New from Capability
    Assertions.Assert the Element in popup dialog  ${Capability1}
    PO_NewCreationPopUpPage.Select Checkbox in popup  ${Capability1}
    PO_NewCreationPopUpPage.Click on Save Button
    Wait.DOMready-PopUpCloses
    
Sourcing-00369
    # Step 12: Update Discounts for the Supplier 
    Reload
    Wait.DOMready
    DOMready-TabLoads
    Click Element From List  ${Loc_TabSupplierDiscounts}  -1
    Wait.DOMreadyByWaitTime  5
    DOMready-TabLoads
    
Sourcing-00370
    Mouse Over  ${Loc_ActionsDiscounts}   
   	Click Element  ${Loc_ActionsDiscounts}
    Wait.DOMreadyByWaitTime  1
    #PO_SourcingPage.Click on New Supplier Discount option
    Wait.DOMreadyByWaitTime  2
    #PO_SourcingPage.Enter the Supplier Discount Name  Discount-01
    Input Text  ${Loc_TextAreaSupplierDiscounts}  Discount-01
    Wait.DOMreadyByWaitTime  2
    Click Element From List  ${Loc_TabSupplierDiscounts}  -1 
    DOMready-TabLoads
    #PO_SourcingPage.Click on the Required cell in the table  Discount-01  3
    Click Element  //td/a[text()='Discount-01']/following::td[@data-csi-act='DiscountPct::0']
    Wait.DOMreadyByWaitTime  2
    Enter the number value in table  10
    Click Element From List  ${Loc_TabSupplierDiscounts}  -1 
    DOMready-TabLoads
    #PO_SourcingPage.Click checkbox in table view  Discount-01  4
    Click Element  //td[a[text()='Discount-01']]/following::td[@data-csi-heading='Active::0']/div/Input[@class='dijitReset dijitCheckBoxInput']	
    
    Wait.DOMreadyByWaitTime  3
    # Create another discount 
    Click Element From List  ${Loc_TabSupplierDiscounts}  -1
    Mouse Over  ${Loc_ActionsDiscounts}   
   	Click Element  ${Loc_ActionsDiscounts}
   	Wait.DOMreadyByWaitTime  3
    Input Text  ${Loc_TextAreaSupplierDiscounts}  Discount-02
    Wait.DOMreadyByWaitTime  2
    Click Element From List  ${Loc_TabSupplierDiscounts}  -1 
    DOMready-TabLoads
    #PO_SourcingPage.Click on the Required cell in the table  Discount-02  3
    Click Element  //td/a[text()='Discount-02']/following::td[@data-csi-act='DiscountPct::0']
    Wait.DOMreadyByWaitTime  2
    Enter the number value in table  7
    Click Element From List  ${Loc_TabSupplierDiscounts}  -1
    Wait.DOMreadyByWaitTime  2
    #PO_SourcingPage.Click checkbox in table view  Discount-02  4
    Click Element  //td[a[text()='Discount-02']]/following::td[@data-csi-heading='Active::0']/div/Input[@class='dijitReset dijitCheckBoxInput']	
    
    Wait.DOMreadyByWaitTime  3
    Click Element From List  ${Loc_TabSupplierDiscounts}  -1
  [Teardown]    Close Browser 
***Keyword***
Setup SupplierData
  ${This_DataProvider} =  Data_Provider.DataProvider  Create Supplier
  Set Test Variable  ${This_DataProvider}
  ${SupplierCollection} =  Data_Provider.GetDataProviderColumnValue  Data_SupplierName
  ${Supplier1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SupplierCollection}  1
  ${Supplier2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SupplierCollection}  2
  ${Supplier3} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SupplierCollection}  3
  ${Supplier4} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SupplierCollection}  4
  @{SupplierList} =  Create List  ${Supplier1}  ${Supplier2}  ${Supplier3}
  Set Test Variable  ${Supplier1}
  Set Test Variable  ${Supplier2}
  Set Test Variable  ${Supplier3}
  Set Test Variable  ${Supplier4}
  
  :FOR  ${ListItem}  IN  @{SupplierList}
  \  Log  ${ListItem}
  \  Set Test Variable  ${ListItem}   
  


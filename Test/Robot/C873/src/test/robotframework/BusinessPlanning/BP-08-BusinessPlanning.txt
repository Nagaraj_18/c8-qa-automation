*** Settings ***

Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_SetupPage.txt
Resource          ../PageObjects/PO_SourcingPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_EnumerationPage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../Locators/Loc_SetupPage.txt    
Resource          ../Locators/Loc_BusinessPlanning.txt
Resource          ../Locators/Loc_SourcingSupplierPage.txt
Resource          ../PageObjects/PO_TablePage.txt
Resource          ../Locators/Loc_MaterialPage.txt
Resource          ../Locators/Loc_MaterialLibrary.txt

***Variable***
${Var_Volume}  3000
${Var_Margin}  5
${Var_Percentage}  100
${Msg_CreationProductPlan}  Product Plan created successfully.
${Var_Placeholder1}  Placeholder1
${Var_Placeholder2}  Placeholder2
${Var_ActualVolume}  100
${Var_ActualMargin}  100
${StyleName1}  BP_Style
${StyleSizes}  BP_Size1
${ColorWayName1}  BP_StyleColorway1
${StyleSKUName1}  BP_Style_SKU1 
${Supplier1}  Iceplant
${Currency1}  US
${BPSeason}  BP_Season
${BPBrand}  RF_Brand 
${BPDepartment}  RF_Dept
${BPCollection}  RF_Collec
${StyleBusinessPlanName2}  StyleBusinessPlan2
#${StyleType1}  Apparel - Color and Size
${StyleType1}  AUT_Style_ST1


*** Test Cases ***  
Test Setup    
    [tags]  Business Planning        
  Setup The Variables 
Create Business Planning 
       
  SuiteSetUp.Open Browser To Login Page
  PO_LoginPage.Input UserName    NewUser_UserName
  PO_LoginPage.Input Password    NewUser_Password
  PO_LoginPage.Submit Credentials
  Wait For Release Info
  #Reload
  Wait.DOMready
  DOMready-TabLoads

Create Suppliers at Home->Sourcing Level
    [tags]  Business Planning
    Click on Home Icon
    DOMready-TabLoads
    Click Element  ${Loc_Home_SourcingTab}
    DOMready-TabLoads
    PO_SourcingPage.Click on Supplier tab
    DOMready-TabLoads	   

    @{SupplierList1} =  Create List  ${Supplier1}  
     # Create 4 Suppliers on different combination 
    :FOR  ${ListItem}  IN  @{SupplierList1}
    \  Log  ${ListItem}
    \  Mouse Over  ${Loc_ActionsSupplier}   
    \  Wait.DOMreadyByWaitTime  1
    \  Click Element  ${Loc_ActionsSupplier}  
   	\  PO_SetupPage.Verify Dialog Window  New Supplier
   	\  Wait.DOMreadyByWaitTime  3
    \  Input Text  ${Loc_TextFieldSupplierName}  ${ListItem}
	\  Wait.DOMreadyByWaitTime  1
    \  Click Element  ${SupplierDlg_IsSupplierAttr}
    \  Wait.DOMreadyByWaitTime  1
    \  PO_NewCreationPopUpPage.Click on Save Button
    \  Wait.DOMreadyByWaitTime  5
    \  Wait Until Element Is Enabled  ${Loc_ActionsSupplier}  120  
Verify the column names
    [tags]  Business Planning
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads    
  Click Element     ${Loc_Home-BusinessPlanningTab}
  DOMready-TabLoads
  Verify the Element  //span[text()='Business Plan']    Business Plan Column is displayed in Button Business Plans Table
  Verify the Element  //span[text()='Category']    Category Column is displayed in Button Business Plans Table
  Verify the Element  //span[text()='Currency Table']    Currency Table Column is displayed in Button Business Plans Table
  Verify the Element  //span[text()='Business Market Plans']    Business Market Plan Column is displayed in Button Business Plans Table
  Verify the Element  //span[text()='Product Filters']    Product Filters Column is displayed in Button Business Plans Table
  Verify the Element  //span[text()='Target Volume']    Target Volume Column is displayed in Button Business Plans Table
  Verify the Element  //span[text()='Target Margin (%)']    Target Margin (%) Column is displayed in Button Business Plans Table
 verify the Business Plans dialog
      
  Click Element  ${Loc_Home-BusinessPlanning-BusinessPlansTab}
  DOMready-TabLoads
  Mouse Over  ${Loc_Home-BusinessPlanning_ActionsLink} 
  Click Element  ${Loc_Home-BusinessPlanning_ActionsLink}  
  Wait Until Page Contains Element  ${BusinessPlanDialog} 
  @{BusinessPlanDialogAtrributes} =  Create List  ${NewBusinessPlanDialog_CurrencyTableDDL}  ${NewBusinessPlanDialog_SupplierDDL}  ${NewBusinessPlanDialog_CategoryDDL}  ${NewBusinessPlanDialog_NodeTextArea}
  ${Index} =  Set Variable  0
  :FOR  ${ListItems}  IN  @{BusinessPlanDialogAtrributes}
  \  Page Should Contain Element  ${ListItems}
  \  Run Keyword If  str('${Index}') == str('1')  Page Should Contain Element  //tr[${Index}]/following::td[1][text()='*']
  \  ${Index} =  Evaluate  ${Index} + 1   
Entering the value in the dialog window
    [tags]  Business Planning
  Click Element  ${NewBusinessPlanDialog_NodeTextArea}
  Input Text  ${NewBusinessPlanDialog_NodeTextArea}  ${StyleBusinessPlanName1} 
  Click Element  ${NewBusinessPlanDialog_CategoryDDL}
  Page Should Contain Element  //div[text()='${BusinessCategoryStyleName1}']
  Page Should Contain Element  //div[text()='${MaterialBusinessCategoryName2}']
  
Entering the value in the dialog window-select currency
    [tags]  Business Planning       
  PO_NewCreationPopUpPage.Select value from the DDL  ${BusinessCategoryStyleName1}
  Click Element  ${NewBusinessPlanDialog_CurrencyTableDDL}
  Select value from the DDL   ${CurrencyTable1}
  
Entering the value in the dialog window-select supplier
    [tags]  Business Planning         
  Click Element  ${NewBusinessPlanDialog_SupplierDDL}
  Select value from the DDL  ${Supplier1}
  
Entering the value in the dialog window-select season
    [tags]  Business Planning       
  Click Element  ${NewBusinessPlanDialog_CategoryDDL}
  Select value from the DDL  ${BusinessCategoryStyleName1}
  DOMreadyByWaitTime  2
  Wait Until Page Contains Element  //th[text()='Season']
Entering the value in the dialog window-select brand
     [tags]  Business Planning    
  Click Element  ${NewBusinessPlanDialog_SeasonDDL}
  Select Value from the DDL  ${BPSeason}
  DOMreadyByWaitTime  2
  Wait Until Page Contains Element  //th[text()='Brand']
Entering the value in the dialog window-select Category
    [tags]  Business Planning      
  Click Element  ${NewBusinessPlanDialog_BrandDDL}
  Select Value from the DDL  ${BPBrand}
  DOMreadyByWaitTime  2
  Wait Until Page Contains Element  //th[text()='Department']
Entering the value in the dialog window-select Dpt
    [tags]  Business Planning      
  Click Element  ${NewBusinessPlanDialog_DepartmentDDL}
  Select Value from the DDL  ${BPDepartment}
  DOMreadyByWaitTime  2
  Wait Until Page Contains Element  //th[text()='Department']
Entering the value in the dialog window-select collection
    [tags]  Business Planning      
  Click Element  ${NewBusinessPlanDialog_CollectionDDL}
  Select Value from the DDL  ${BPCollection}
  DOMreadyByWaitTime  2  
       
  Page Should Not Contain Element  //tr/th[text()='Department']/following-sibling::td[text()='*']

Entering the value in the dialog window-select Business market
    [tags]  Business Planning      
  Click on Next Button
  Wait Until Page Contains Element  css=[data-csi-act='Finish']  
  @{BusinessMarket} =  Create List  ${BusinessMarketName1}  ${BusinessMarketName2}  
  Set Suite Variable  @{BusinessMarket}  
  :FOR  ${Listitems}  IN  @{BusinessMarket}
  \  Wait Until Keyword Succeeds  2 Min    5 Sec  Page Should Contain Element  //*[text()='${Listitems}']/preceding::td[1]/div/input[@checked='checked']
  Click on Finish Button
  DOMready-PopUpCloses
  Page Should Contain Element  //a[text()='${StyleBusinessPlanName1}']/parent::td[1]
     
  Page Should Contain Element  //a[text()='${StyleBusinessPlanName1}']/parent::td[1]/following::td[@data-csi-heading='Children::0']/span[text()='${BusinessMarketName2}']
  :FOR  ${Listitems}  IN  @{BusinessMarket}
  \  Page Should Contain Element  //a[text()='${StyleBusinessPlanName1}']/parent::td[1]/following::td[@data-csi-heading='Children::0']/span[text()='${Listitems}']
  
Select the Business Plan Name-Verify the tabs
     [tags]  Business Planning     
  Click Link  //td/a[text()='${StyleBusinessPlanName1}']
   DOMready-TabLoads
  Page Should Contain Element  ${Loc_BusinessPlanNode-PlanTab}
  Page Should Contain Element  ${Loc_BusinessPlanNode-ProductPlansTab}
  Page Should Contain Element  ${Loc_BusinessPlanNode-MarketProductPlansTab}
  Page Should Contain Element  ${Loc_BusinessPlanNode-CurrencyExchangeRatesTab}
  # Page Should Contain Element  //span[@aria-selected='true' and @data-csi-tab='BusinessPlan-BusinessPlan'] 
  
Verify the sections
    [tags]  Business Planning       
  Page Should Contain Element  //div[@data-csi-template-rule='Rule1']/div[text()='Properties']
  Page Should Contain Element  //div[@data-csi-template-rule='Rule2']/div[text()='Business Market Plans']

Edit the values in properties
    [tags]  Business Planning 
  Click Element From List  ${Loc_BusinessPlanNode-Plan-TargetVolumeAttr}  0
  DOMreadyByWaitTime  2
  Enter The Values on Textarea  ${Loc_EditTargetVolumeValuePropertiesTab}  ${Var_Volume}
  Click Element  ${Loc_BusinessPlanNode-PlanTab}
  Click Element From List  ${Loc_BusinessPlanNode-Plan-TargetMarginAttr}  0
  DOMreadyByWaitTime  2
  Enter The Values on Textarea  ${Loc_EditTargetMarginValuePropertiesTab}  ${Var_Margin}
  Click Element  ${Loc_BusinessPlanNode-PlanTab}
  
#Business Planning-02863-selecting the BM using the GO to dialog option
  #Mouse Over    ${Loc_BusinessPlanNode-Plan-BusinessMarketPlansAttr}
  #Click Element  ${Loc_BusinessPlanNode-Plan-BusinessMarketPlansAttr}
  #Wait Until Element Is Visible    //div[contains(@class,'dijitMenuItem')]//span[text()='Go to Dialog...']
  #Click Element      //div[contains(@class,'dijitMenuItem')]//span[text()='Go to Dialog...']
  #Wait Until Page Contains Element  ${SelectBusinessMarketDialog}
  
#Business Planning-02864
  #:FOR  ${Listitems}  IN  @{BusinessMarket}
  #\  Page Should Contain Element  //td[@data-csi-heading='Node Name::0' and text()='${Listitems}']/preceding::td[1]/div/input
#Business Planning-02865
  #Page Should Contain Element  //th[contains(@class,'csi-table-selection-column')]/div/input
  #Page Should Contain Element  //span[text()='Save']
  #Click on Cancel Button

  :FOR  ${Listitems}  IN  @{BusinessMarket}
  \  Page Should Contain Element  //td[@data-csi-heading='Node Name::0' and text()='${Listitems}']
  \  Page Should Not Contain Element  //td[@data-csi-heading='Node Name::0' and text()='${Listitems}']/following-sibling::td[contains(@class,'actionsColumn')]
  


  Click Element  //td[@data-csi-heading='Node Name::0' and text()='${BusinessMarketName1}']/following::td[1]
  DOMreadyByWaitTime  2
  Enter The Values on Textarea  ${Loc_EditSplitValueBusinessMarketPlanNode}  40
  Click Element  //div[@data-csi-template-rule='Rule2']/div[text()='Business Market Plans']
  
  Click Element  //td[@data-csi-heading='Node Name::0' and text()='${BusinessMarketName2}']/following::td[1]
  DOMreadyByWaitTime  2
  Enter The Values on Textarea  ${Loc_EditSplitValueBusinessMarketPlanNode}  60
  Click Element  //div[@data-csi-template-rule='Rule2']/div[text()='Business Market Plans']

Verify the product plans tab
    [tags]  Business Planning
  Click Element     ${Loc_BusinessPlanNode-ProductPlansTab}
  DOMready-TabLoads
  Verify the Element  //span[text()='Product Plan']    Product Plan Column is displayed in Button Product Plans Table
  Verify the Element  //span[text()='Description']    Description Column is displayed in Product Plans Table
  Verify the Element  //span[text()='Target Volume']    Target Volume Column is displayed in Product Plans Table
  Verify the Element  //span[text()='Target Margin (%)']    Target Margin (%) Column is displayed in Product Plans Table
  Verify the Element  //span[text()='Active']    Active Column is displayed in Product Plans Table
  
Verify the action links in the product plans tab
    [tags]  Business Planning
  Mouse Over  ${Loc_BusinessPlanNode-ProductPlans_ActionsLink}
  Click Element  ${Loc_BusinessPlanNode-ProductPlans_ActionsLink}
  #Wait Until Element Is Visible  ${Loc_BusinessPlanNode-ProductPlans_Actions_NewPlaceHolderLink}
  Mouse Over  ${Loc_BusinessPlanNode-ProductPlans_Actions_NewPlaceHolderLink}
  Mouse Over  ${Loc_BusinessPlanNode-ProductPlans_Actions_NewFromStylesLink}
  Mouse Over  ${Loc_BusinessPlanNode-ProductPlans_Actions_NewFromColorwaysLink}
  Mouse Over  ${Loc_BusinessPlanNode-ProductPlans_Actions_NewFromSKULink}
  Mouse Over  ${Loc_BusinessPlanNode-ProductPlans_Actions_NewFromSizesLink}
  
Create the product plans using the placeholder option and selecting the options select style and new style options
    [tags]  Business Planning
  Click Element  ${Loc_BusinessPlanNode-ProductPlans_Actions_NewPlaceHolderLink}
  Wait Until Element Is Visible  //span[text()='${Msg_CreationProductPlan}']
  DOMreadyByWaitTime  2
  Input Text  //div/textarea  ${Var_Placeholder1}
  Click Element     ${Loc_BusinessPlanNode-ProductPlansTab}
  Wait For Save Icon To Appear

Verify the options under placeholder
    [tags]  Business Planning
  ${PlaceHolder1URL} =  Get Element Attribute  //td[text()='${Var_Placeholder1}']  data-csi-url 
  Set Suite Variable  ${PlaceHolder1URL}
  Mouse Over    css=[data-csi-act='Copy'][data-csi-url='${PlaceHolder1URL}']
  Mouse Over    css=[data-csi-act='Delete'][data-csi-url='${PlaceHolder1URL}']
  Click Element  css=[data-csi-automation='actions-BusinessPlan-BPProductPlans-${PlaceHolder1URL}']
  Wait Until Element Is Visible  //td[text()='Select Style']
  Mouse Over    //td[text()='Select Colorway']
  Mouse Over    //td[text()='Select Style SKU']
  Mouse Over    //td[text()='Select Size']
  Mouse Over    //td[text()='New Style']
  Mouse Over    //td[text()='Select Style']

  Click Element    //td[text()='Select Style']
  DOMready-PopUpAppears
  PO_SetupPage.Verify Dialog Window  Select Style
  #Wait Until Element Is Visible  //span[@class='dijitDialogTitle' and text()='Select Style']
  Click Element  //*[text()='${StyleName1}']
  Page Should Contain Element  css=[data-csi-act='Save']
  Page Should Contain Element  css=[data-csi-act='Cancel']
  
   Click Element From List  css=[data-csi-act='Save']  -1
    DOMreadyByWaitTime  5
    Wait Until Page Contains Element  ${Loc_Release}
    Page Should Contain Element  //td[@data-csi-act='Node Name::0']/a[text()='${StyleName1}']
    Page Should Not Contain Element  //td[text()='${Var_Placeholder1}']/following::td[5]/div/span[contains(@class,'dijitDropDownButton')]
    Page Should Contain Element  css=[data-csi-act='Delete'][data-csi-url='${PlaceHolder1URL}']
    Page Should Contain Element  css=[data-csi-act='Copy'][data-csi-url='${PlaceHolder1URL}']
    

  Mouse Over  ${Loc_BusinessPlanNode-ProductPlans_ActionsLink}
  Click Element  ${Loc_BusinessPlanNode-ProductPlans_ActionsLink}
  Wait Until Element Is Visible  ${Loc_BusinessPlanNode-ProductPlans_Actions_NewPlaceHolderLink}
  Click Element  ${Loc_BusinessPlanNode-ProductPlans_Actions_NewPlaceHolderLink}
  Wait Until Element Is Visible  //span[text()='${Msg_CreationProductPlan}']
  DOMreadyByWaitTime  2
  Input Text  //div/textarea  ${Var_Placeholder2}
  DOMreadyByWaitTime  2
  Wait Until Page Contains Element  ${Loc_Release}
  ${ClickStatus} =  Run Keyword And Return Status  Click Tab After Entering Value For Inline Row  BusinessPlan-BPProductPlans
  Run Keyword IF  '${ClickStatus}'=='false'  Alternate - Tab name Click  //div/textarea  BusinessPlan-BPProductPlans

  Click Element     ${Loc_BusinessPlanNode-ProductPlansTab}  
  ${PlaceHolder2URL} =  Get Element Attribute  //td[text()='${Var_Placeholder2}']  data-csi-url 
  Set Suite Variable  ${PlaceHolder2URL}
  Click Element  css=[data-csi-automation='actions-BusinessPlan-BPProductPlans-${PlaceHolder2URL}']
  Wait Until Page Contains Element  //td[text()='Select Style']
  Click Element From List    //td[contains(text(),'New Style')]  -1
  DOMready-PopUpAppears
  PO_SetupPage.Verify Dialog Window  New Style
  #Wait Until Element Is Visible  //div[@class='dijitDialogTitleBar']/span[text()='New Style']
  ###
Verify the fields
    [tags]  Business Planning      
  @{NewProductDialog2Attributes} =  Create List  Style Type    Template    Season    Brand    Department    Collection    Theme  Style  
  :FOR   ${ListItems}  IN  @{NewProductDialog2Attributes}
  \  Element Should Be Visible  //th[text()='${ListItems}']
Update season hierarchy values in the aggregate  
    [tags]  Business Planning    
  # Update season hierarchy values in the aggregate
    @{SeasonAttributes} =  Create List  ${NewProductDialog_StyleTypeDDL}  ${NewProductDialog_SeasonDDL}   ${NewProductDialog_BrandDDL}  ${NewProductDialog_DepartmentDDL}  ${NewProductDialog_CollectionDDL}
    ${SeasonAttributeValues} =  Create List   ${StyleType1}  ${BPSeason}  ${BPBrand}  ${BPDepartment}  ${BPCollection}
    ${Index} =  Set Variable  0
    Click Element  ${NewProductDialog_StyleTypeDDL}
    Select value from the DDL   ${StyleType1}
   :FOR  ${Iterator}  IN  @{SeasonAttributes}
    \  Click Element  ${Iterator}
    \  ${ReqElement} =  Get From List  ${SeasonAttributeValues}  ${Index}
    \  Select value from the DDL  ${ReqElement}
    \  ${Index} =  Evaluate  ${Index} + 1
    
  Input text  css=[data-csi-automation="field-Style-Form-Node Name"] .dijitInputInner  ${Var_Placeholder2}      
    

      
  Click Element From List  css=[data-csi-act='Save']  -1 
  DOMreadyByWaitTime  5  
  Wait Until Page Contains Element  //span[text()='${Msg_ConfirmationStyleCreation}']
  Element Should Be Visible  //td[@data-csi-act='Node Name::0']/a[text()='${Var_Placeholder2}']
        
Select option new from styles   
    [tags]  Business Planning  
  Reload
  Wait.DOMready
  DOMready-TabLoads
  Mouse Over  ${Loc_BusinessPlanNode-ProductPlans_ActionsLink}
  Click Element  ${Loc_BusinessPlanNode-ProductPlans_ActionsLink}
  Wait Until Page Contains Element  ${Loc_BusinessPlanNode-ProductPlans_Actions_NewPlaceHolderLink}    
  Click Element From List  ${Loc_BusinessPlanNode-ProductPlans_Actions_NewFromStylesLink}  -1
  DOMready-PopUpAppears
  Element Should Be Visible  //td[text()='${StyleName1}']
  Click Element  //td[text()='${StyleName1}']
  Click Element From List  css=[data-csi-act='Save']  -1 
  DOMready-PopUpCloses  
  Wait Until Page Contains Element  //td[@data-csi-heading='Node Name::0']/a[text()='${StyleName1}']
  
Select option new from colorways 
    [tags]  Business Planning     
  Mouse Over  ${Loc_BusinessPlanNode-ProductPlans_ActionsLink}
  Click Element  ${Loc_BusinessPlanNode-ProductPlans_ActionsLink}
  Wait Until Page Contains Element  ${Loc_BusinessPlanNode-ProductPlans_Actions_NewPlaceHolderLink}
  Click Element From List  ${Loc_BusinessPlanNode-ProductPlans_Actions_NewFromColorwaysLink}  -1
  DOMready-PopUpAppears
  Element Should Be Visible  //td[text()='${ColorWayName1}']
  Click Element  //td[text()='${ColorWayName1}']
  Click Element From List  css=[data-csi-act='Save']  -1 
  DOMready-PopUpCloses  
  Wait Until Page Contains Element  //td[@data-csi-heading='Node Name::0']/a[text()='${StyleName1}']
  
Select option new from SKU 
    [tags]  Business Planning     
  Mouse Over  ${Loc_BusinessPlanNode-ProductPlans_ActionsLink}
  Click Element  ${Loc_BusinessPlanNode-ProductPlans_ActionsLink}
  Wait Until Page Contains Element  ${Loc_BusinessPlanNode-ProductPlans_Actions_NewPlaceHolderLink}
  DOMreadyByWaitTime  3
  Click Element From List  ${Loc_BusinessPlanNode-ProductPlans_Actions_NewFromSKULink}  -1
  DOMready-PopUpAppears
  Element Should Be Visible  //td[@data-csi-heading='Node Name::0']/preceding::td[1]/div/input
  Click Element  //td[@data-csi-heading='Node Name::0']/preceding::td[1]/div/input
  Click Element From List  css=[data-csi-act='Save']  -1 
  DOMready-PopUpCloses  
  
Select option new from Sizes
    [tags]  Business Planning
  Mouse Over  ${Loc_BusinessPlanNode-ProductPlans_ActionsLink}
  Click Element  ${Loc_BusinessPlanNode-ProductPlans_ActionsLink}
  Wait Until Page Contains Element  ${Loc_BusinessPlanNode-ProductPlans_Actions_NewPlaceHolderLink}
  Click Element From List  ${Loc_BusinessPlanNode-ProductPlans_Actions_NewFromSizesLink}  -1
  DOMready-PopUpAppears
  Element Should Be Visible  //td[@data-csi-heading='Node Name::0']/preceding::td[1]/div/input
  Click Element  //td[text()='${StyleName1}']
  Click on Next Button 
  #Wait Until Page Contains Element    css=[data-csi-act='Finish']  
  Wait Until Keyword Succeeds  2 Min    5 Sec  Click Element  //td[text()='Size']/preceding::td[3]/div/input
  Click on Finish Button
  DOMready-PopUpCloses 
#Business Planning-02893
      
  # Each of the column names are returning close to 4 elements
  #Click Element  ${Loc_BusinessPlanNode-MarketProductPlansTab}
  #Wait Until Page Contains Element  ${Loc_Release}
   #DOMready-TabLoads
  #@{MarketProductPlansTabAttributes} =  Create List  Product Plan  Target Volume  Target Margin (%)  Forecast Volume  Forecast Margin (%)  Committed Volume  Committed Margin (%)  Actual Volume  Actual Margin (%)
  #:FOR  ${Iterator}  IN  @{MarketProductPlansTabAttributes}
  #\  Element Should Be Visible  //div[@class='csi-table-header-container']//following::span[@class='csi-table-header-content' and text()='${Iterator}']
  
#Business Planning-02894  

Create View
    [tags]  Business Planning
  Click Element  ${Loc_BusinessPlanNode-MarketProductPlansTab}
  Wait Until Page Contains Element  ${Loc_Release}
  DOMready-TabLoads
  Wait.DOMreadyByWaitTime  5 
  Click Element From List  css=[data-csi-automation='plugin-BusinessPlan-MarketProductPlans-CustomViewActions'] .dijitButtonText  -1
  Wait.DOMreadyByWaitTime  2
  Click Element From List  css=[data-csi-automation='plugin-BusinessPlan-MarketProductPlans-CustomViewManage'] .dijitMenuItemLabel  -1
  Wait.DOMreadyByWaitTime  5
  #Wait Until Page Contains Element  xpath=//span[text()='Add >']
  Wait.DOMreadyByWaitTime  2	
# Copy the Default CV and create a new CV
  PO_SetupPage.Click on Default Custom View
  PO_SetupPage.Click on Copy button
  Clear Element Text  ${Loc_SecurityRoleName}
  Selenium2Library.Input Text  ${Loc_SecurityRoleName}  ProdPlanView
  Wait.DOMreadyByWaitTime  2
  #Click Element		//div[@class='csiPreferenceDisplay']//div[@class='dijitReset dijitRight dijitButtonNode dijitArrowButton dijitDownArrowButton dijitArrowButtonContainer']         
  #Click Element          //div[@class='dijitReset dijitMenuItem' and text()='Material']      
#Add the  attribute to the view 
  Click Element 		css=.csiPreferenceSelect [value='Volume:Child:Forecast:0']
  Wait.DOMreadyByWaitTime  2
  Click Element         //span[text()='< Remove']     
#Add the attribute to the view
  Wait.DOMreadyByWaitTime  2
  Click Element 		css=.csiPreferenceSelect [value='MarginPct:Child:Forecast:0']
  Wait.DOMreadyByWaitTime  2
  Click Element         //span[text()='< Remove']
  Wait.DOMreadyByWaitTime  2
  Click Element 		css=.csiPreferenceSelect [value='Volume:Child:Committed:0']
  Wait.DOMreadyByWaitTime  2
  Click Element         //span[text()='< Remove']
  Wait.DOMreadyByWaitTime  2
  Click Element 		css=.csiPreferenceSelect [value='MarginPct:Child:Committed:0']
  Wait.DOMreadyByWaitTime  2
  Click Element         //span[text()='< Remove']
  Wait.DOMreadyByWaitTime  2
  Wait.DOMreadyByWaitTime  2
  Click Element From List  Xpath=//span[text()='Save']  -1 
  Wait.DOMreadyByWaitTime  5  

Editing the Volume,Actual Margin value on the Market Product Plans Tab    
    [tags]  Business Planning      
  Click Element  ${Loc_BusinessPlanNode-MarketProductPlansTab}
  Wait Until Page Contains Element  ${Loc_Release}
  DOMready-TabLoads
  Click Element  //tr[2]/td[@data-csi-act='Volume:Child:Actual:0']  
  Wait until Element Is Visible  //div/input[@value='0']
  Enter The Values on Textarea  //div/input[@value='0']  ${Var_ActualVolume}
  Click Element  ${Loc_BusinessPlanNode-MarketProductPlansTab}
  DOMreadyByWaitTime  2
  
  Click Element  //tr[2]/td[@data-csi-act='MarginPct:Child:Actual:0']  
  Wait until Element Is Visible  //div/input[@value='0']
  Enter The Values on Textarea  //div/input[@value='0']  ${Var_ActualMargin}
  Click Element  ${Loc_BusinessPlanNode-MarketProductPlansTab}
  DOMreadyByWaitTime  2
  
Editing the Target currency
    [tags]  Business Planning     
  Click Element  ${Loc_BusinessPlanNode-CurrencyExchangeRatesTab}
  DOMready-TabLoads
  Element Should Be Visible  //span[text()='Target']    
   
  Element Should Be Visible  //*[text()='${Currency1}']
  #Element Should Be Visible  //td[@data-csi-heading='Target::0' and text()='${Currency2}']


Creating another Style Business Plan 
    [tags]  Business Planning
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads    
  Click Element     ${Loc_Home-BusinessPlanningTab}
  DOMready-TabLoads 
    Click Element  ${Loc_Home-BusinessPlanning-BusinessPlansTab}
  DOMready-TabLoads
  Mouse Over  ${Loc_Home-BusinessPlanning_ActionsLink} 
  Click Element  ${Loc_Home-BusinessPlanning_ActionsLink}  
    
  Click Element  ${NewBusinessPlanDialog_NodeTextArea}
  Input Text  ${NewBusinessPlanDialog_NodeTextArea}  ${StyleBusinessPlanName2} 
  Click Element  ${NewBusinessPlanDialog_CategoryDDL}
       
  PO_NewCreationPopUpPage.Select value from the DDL  ${BusinessCategoryStyleName1}
  Click Element  ${NewBusinessPlanDialog_CurrencyTableDDL}
  Select value from the DDL   ${CurrencyTable1}
 
  Click Element  ${NewBusinessPlanDialog_SupplierDDL}
  Select value from the DDL  ${Supplier1}

       
  Click Element  ${NewBusinessPlanDialog_CategoryDDL}
  Select value from the DDL  ${BusinessCategoryStyleName1}
  DOMreadyByWaitTime  2
  Wait Until Page Contains Element  //th[text()='Season']
     
  Click Element  ${NewBusinessPlanDialog_SeasonDDL}
  Select value from the DDL  ${BPSeason}
  DOMreadyByWaitTime  2
  Wait Until Page Contains Element  //th[text()='Brand']

  #Click Element  ${NewBusinessPlanDialog_BrandDDL}
  Click On the DDL button2  field-BusinessPlan-Form-BPCategory1
  DOMreadyByWaitTime  2
  Select value from the DDL  ${BPBrand}
  DOMreadyByWaitTime  2
  Wait Until Page Contains Element  //th[text()='Department']
      
  Click Element  ${NewBusinessPlanDialog_DepartmentDDL}
  DOMreadyByWaitTime  2
  Select value from the DDL  ${BPDepartment}
  DOMreadyByWaitTime  2
  Wait Until Page Contains Element  //th[text()='Department']
      
  Click Element  ${NewBusinessPlanDialog_CollectionDDL}
  DOMreadyByWaitTime  2
  Select value from the DDL  ${BPCollection}
  DOMreadyByWaitTime  2  

  Click on Next Button
  Wait Until Page Contains Element  css=[data-csi-act='Finish']  
  @{BusinessMarket} =  Create List  ${BusinessMarketName1}  ${BusinessMarketName2}  
  Set Suite Variable  @{BusinessMarket}  
  :FOR  ${Listitems}  IN  @{BusinessMarket}
  \  Wait Until Keyword Succeeds  2 Min    5 Sec  Page Should Contain Element  //*[text()='${Listitems}']/preceding::td[1]/div/input[@checked='checked']
  Click on Finish Button
  DOMready-PopUpCloses

  Click Link  //td/a[text()='${StyleBusinessPlanName2}']
  DOMready-TabLoads
Business Planning- Properties and Business Market Plans on the Plan tab
    [tags]  Business Planning
    #Check for two sections Properties and Business Market Plans on the Plan tab
  Click Element  ${Loc_BusinessPlanNode-PlanTab}
  DOMready-TabLoads
  Assert for the Given Element  ${Loc_BusinessPlan-PropertiesSection} 
  Assert for the Given Element  ${Loc_BusinessPlan-BusinessMarketPlanSection}
Business Planning-Edit the values in properties 
    [tags]  Business Planning
  Click Element From List  ${Loc_BusinessPlanNode-Plan-TargetVolumeAttr}  0
  DOMreadyByWaitTime  2
  Enter The Values on Textarea  ${Loc_EditTargetVolumeValuePropertiesTab}  ${Var_Volume}
  Click Element  ${Loc_BusinessPlanNode-PlanTab}
  Click Element From List  ${Loc_BusinessPlanNode-Plan-TargetMarginAttr}  0
  DOMreadyByWaitTime  2
  Enter The Values on Textarea  ${Loc_EditTargetMarginValuePropertiesTab}  ${Var_Margin}
  Click Element  //tr[1]//following::td[1][@data-csi-act='SplitPct:Child:Target:0'] 
  Enter The Values on Textarea  ${Loc_BusinessPlanTab-EditTargetSplitColumn}  40
  Click Element  ${Loc_BusinessPlanNode-PlanTab}
 
  Click Element  //tr[2]//following::td[1][@data-csi-act='SplitPct:Child:Target:0'] 
  Enter The Values on Textarea  ${Loc_BusinessPlanTab-EditTargetSplitColumn}  60
  Click Element  ${Loc_BusinessPlanNode-PlanTab}
Adding Data under Product Plans Tab  
    [tags]  Business Planning
  Click Element  ${Loc_BusinessPlanNode-PlanTab}     
  Click Element     ${Loc_BusinessPlanNode-ProductPlansTab}
  DOMready-TabLoads 
#Add a placeholder and Add style to it
  Mouse Over  ${Loc_BusinessPlanNode-ProductPlans_ActionsLink}
  Click Element  ${Loc_BusinessPlanNode-ProductPlans_ActionsLink}
  #Wait Until Element Is Visible  ${Loc_BusinessPlanNode-ProductPlans_Actions_NewPlaceHolderLink}

  Click Element  ${Loc_BusinessPlanNode-ProductPlans_Actions_NewPlaceHolderLink}
  Wait Until Element Is Visible  //span[text()='${Msg_CreationProductPlan}']
  DOMreadyByWaitTime  2
  Input Text  //div/textarea  ${Var_Placeholder1}
  Click Element     ${Loc_BusinessPlanNode-ProductPlansTab}
  ${PlaceHolder1URL} =  Get Element Attribute  //td[text()='${Var_Placeholder1}']  data-csi-url 
  Set Suite Variable  ${PlaceHolder1URL}
  Click Element  css=[data-csi-automation='actions-BusinessPlan-BPProductPlans-${PlaceHolder1URL}']
  Wait Until Element Is Visible  //td[text()='Select Style']
  Click Element    //td[text()='Select Style']
  DOMready-PopUpAppears
  PO_SetupPage.Verify Dialog Window  Select Style
  #Wait Until Element Is Visible  //span[@class='dijitDialogTitle' and text()='Select Style']
  Click Element  //td[text()='${StyleName1}']
  Click Element From List  css=[data-csi-act='Save']  -1
  DOMready-PopUpCloses
    
#    [Teardown]    Close Browser         
***Keywords***
Setup The Variables
  ${This_DataProvider} =  Data_Provider.DataProvider  Business Planning
  Set Suite Variable  ${This_DataProvider}
  Log  ${This_DataProvider}
  
  ${CurrencyTableCollection} =  Data_Provider.GetDataProviderColumnValue  Data_CurrencyTableName
  ${CurrencyTable1} =  DataProviderSplitterForMultipleValues  ${CurrencyTableCollection}  1
  ${CurrencyTable2} =  DataProviderSplitterForMultipleValues  ${CurrencyTableCollection}  2
  Set Suite Variable  ${CurrencyTable1}
  Set Suite Variable  ${CurrencyTable2}
   
  ${BusinessMarketNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_BusinessMarketName
  ${BusinessMarketName1} =  DataProviderSplitterForMultipleValues  ${BusinessMarketNameCollection}  1
  ${BusinessMarketName2} =  DataProviderSplitterForMultipleValues  ${BusinessMarketNameCollection}  2
  ${BusinessMarketName3} =  DataProviderSplitterForMultipleValues  ${BusinessMarketNameCollection}  3  
  Set Suite Variable  ${BusinessMarketName1} 
  Set Suite Variable  ${BusinessMarketName2}
  Set Suite Variable  ${BusinessMarketName3}
  ${BusinessCategoryStyleNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_BusinessCategoryStyleName
  ${MaterialBusinessCategoryNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_MaterialBusinessCategoryName
  ${BusinessCategoryStyleName1} =  DataProviderSplitterForMultipleValues  ${BusinessCategoryStyleNameCollection}  1
  Set Suite Variable  ${BusinessCategoryStyleName1}
  
  ${MaterialBusinessCategoryName1} =  DataProviderSplitterForMultipleValues  ${MaterialBusinessCategoryNameCollection}  1
  ${MaterialBusinessCategoryName2} =  DataProviderSplitterForMultipleValues  ${MaterialBusinessCategoryNameCollection}  2
  ${MaterialBusinessCategoryName3} =  DataProviderSplitterForMultipleValues  ${MaterialBusinessCategoryNameCollection}  3
  Set Suite Variable  ${MaterialBusinessCategoryName1}
  Set Suite Variable  ${MaterialBusinessCategoryName2}
  Set Suite Variable  ${MaterialBusinessCategoryName3}
  
  ${BusinessMarketNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_BusinessMarketName
  ${BusinessMarketName1} =  DataProviderSplitterForMultipleValues  ${BusinessMarketNameCollection}  1
  ${BusinessMarketName2} =  DataProviderSplitterForMultipleValues  ${BusinessMarketNameCollection}  2
  ${BusinessMarketName3} =  DataProviderSplitterForMultipleValues  ${BusinessMarketNameCollection}  3
  Set Suite Variable  ${BusinessMarketName1}
  Set Suite Variable  ${BusinessMarketName2}
  Set Suite Variable  ${BusinessMarketName3}
  
  ${StyleBusinessPlanNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_StyleBusinessPlanName
  ${StyleBusinessPlanName1} =  DataProviderSplitterForMultipleValues  ${StyleBusinessPlanNameCollection}  1
  Set Suite Variable  ${StyleBusinessPlanName1}
 
Enter The Values on Textarea
    [Arguments]   ${Element}  ${InputData}
    DOMreadyByWaitTime  2
    # Press backspace button
    Press Key  ${Element}  \\08
    DOMreadyByWaitTime  2
    Press Key  ${Element}  ${InputData}

    
***

     
  
*** Settings ***

Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt
Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_StylePage.txt 
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../Locators/Loc_StyleSeasonsPage.txt
Resource          ../Locators/Loc_StylePage.txt



*** Test Cases ***
Test Setup        
    DataSetup CreateStyleSamples
    
Login To Application 
    [Tags]    Product Customization
    Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    Wait.DOMready
    DOMready-TabLoads
    
Navigate to style and verify reference document should not visible
    [Tags]    Product Customization
    Click on Home Icon
    DOMready-TabLoads
    Click Element  ${HomeStylesTab} 
    DOMready-TabLoads
    Click Element  css=[data-csi-tab='ApparelViews-AllStyles']
    DOMready-TabLoads
    Click Element    //a[text()='AUT_Style_S']
    DOMready-TabLoads
    Click Element    css=[data-csi-tab='Style-Properties']
    DOMready-TabLoads
    Click Element    css=[data-csi-tab='DocumentContainer-DocumentsView']
    DOMready-TabLoads
    Element Should Not be Visible    css=[data-csi-automation='plugin-DocumentContainer-ReferencedDocuments-ToolbarNewActions']
    Wait.DOMreadyByWaitTime  5


Navigate to Material and verify reference document should not visible
    [Tags]    Product Customization
    Click on Home Icon
    DOMready-TabLoads
    Click Element    ${Loc_MaterialTab}
    DOMready-TabLoads
    Click Element   //a[text()='Material_02']  
    DOMready-TabLoads
    Click Element    css=[data-csi-tab='DocumentContainer-DocumentsView']
    DOMready-TabLoads
    Element Should Not be Visible    css=[data-csi-automation='plugin-DocumentContainer-ReferencedDocuments-ToolbarNewActions']
    Wait.DOMreadyByWaitTime  5

Navigate to Colored Material and verify reference document should not visible
    [Tags]    Product Customization
    Click Element    css=[data-csi-tab='Material-MaterialProperties']
    DOMready-TabLoads
    Click Element    //a[text()='Red_Color']
    DOMready-TabLoads
    Click Element    css=[data-csi-tab='DocumentContainer-DocumentsView']
    DOMready-TabLoads
    Element Should Not be Visible    css=[data-csi-automation='plugin-DocumentContainer-ReferencedDocuments-ToolbarNewActions']
    Wait.DOMreadyByWaitTime  5
    Close Browser


*** Keywords ***
DataSetup CreateStyleSamples
  ${This_DataProvider} =  Data_Provider.DataProvider    Create Style Samples
  Set Suite Variable  ${This_DataProvider}
  Log  ${This_DataProvider}
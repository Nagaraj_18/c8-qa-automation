*** Settings ***
Library           ../UtilityHelper/Utility1.py
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt
Resource          ../PageObjects/PO_SetupPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_TablePage.txt
Resource          ../PageObjects/PO_SourcingPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_EnumerationPage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../Locators/Loc_SetupPage.txt    
Resource          ../Locators/Loc_BusinessPlanning.txt

***Variables***
${BusinessMarketCode1}  BM-N01
${BusinessMarketCode2}  BM-S02
${MaterialName1}  AUT_MaterialBP
${ShippingPort}  ShipPort1
${BusinessMarketNameMat1}  CENTRAL
${BusinessMarketNameMat2}  WEST
${BusinessMarketNameMat3}  NORTHEAST
*** Test Cases *** 
Test Setup           
     Setup The Variables  
Create Business Category
  [Tags]  02   
  SuiteSetUp.Open Browser To Login Page
  PO_LoginPage.Input UserName    NewUser_UserName
  PO_LoginPage.Input Password    NewUser_Password
  PO_LoginPage.Submit Credentials
  Wait For Release Info
  #Reload
  Click on Setup Icon
  Wait.DOMready
  DOMready-TabLoads
    
Verify the Business Planing tab
  Click Element  ${Loc_Company_Tab}
  Wait For Release Info
  Verify the Tab Presence  Data-Currency  Currency tab is displayed under Company tab
  Verify the Tab Presence  Data-CurrencyTables  CurrencyTables tab is displayed under Company tab
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads
  Click Element     ${Loc_Home-BusinessPlanningTab}
  DOMready-TabLoads
  Verify the Tab Presence  BusinessPlanningViews-BusinessPlans  Business Plan tab is displayed under Business Planning tab
  Verify the Tab Presence  BusinessPlanningViews-BusinessCategories  Business Categorie tab is displayed under Business Planningtab
	
Verify the Business Category Tab
  Click Element    ${Loc_Home-BusinessPlanning-BusinessCategoriesTab}
  DOMready-TabLoads
  #Wait Until Page Contains Element  ${Loc_Home-BusinessPlanning-BusinessCategories_ActionsLink}
  #Business category, Type, Currency, Markets, Product Filters
  Verify the Element  //span[text()='Business Category']    Business Category Column is displayed in Button Business Categories Table 
  Verify the Element  //span[text()='Product Type']    Product Type Column is displayed in Button Business Categories Table
  Verify the Element  //span[text()='Currency']    Currency Column is displayed in Button Business Categories Table
  Verify the Element  //span[text()='Markets']    Markets Column is displayed in Button Business Categories Table
  Verify the Element  //span[text()='Product Filters']    Product Filters Column is displayed in Button Business Categories Table

Verify the action links
  Mouse Over  ${Loc_Home-BusinessPlanning-BusinessCategories_ActionsLink}
  #Wait Until Element Is Visible  ${Loc_Home-BusinessPlanning-BusinessCategories_Actions_NewBusinessCategoryLink}
  Click Element   ${Loc_Home-BusinessPlanning-BusinessCategories_ActionsLink} 
  DOMreadyByWaitTime  5  
  PO_SetupPage.Verify Dialog Window  New Business Category
  #Wait Until Page Contains Element  ${BusinessCategoryDialog} 
  @{BusinessCategoryDialogAtrributes} =  Create List  ${NewBusinessCategoryDialog_CurrencyDDL}  ${NewBusinessCategoryDialog_ProductTypeDDL}  ${NewBusinessCategoryDialog_NodeTextArea}
  ${Index} =  Set Variable  0
  :FOR  ${ListItems}  IN  @{BusinessCategoryDialogAtrributes}
  \  Page Should Contain Element  ${ListItems}
  \  Run Keyword If  str('${Index}') > str('0')  Page Should Contain Element  //tr[${Index}]/following::td[1][text()='*']
   \  ${Index} =  Evaluate  ${Index} + 1
    
veriy the action button on the popUp window
  Verify the Element  ${Dialog_Save&GoButton}    Save&Go Button on New Plan Dialog is Displayed 
  Verify the Element  ${Dialog_SaveButton}   Save Button on New Plan Dialog is Displayed 
  Verify the Element  ${Dialog_Save&NewButton}    Save&New Button on New Plan Dialog is Displayed 
  Verify the Element  ${Dialog_CancelButton}   Cancel Button on New Plan Dialog is Displayed 

Verify the popup dialog fields
  Click Element  ${NewBusinessCategoryDialog_ProductTypeDDL}
  Assert for the given element  //div[text()='Style']
  Assert for the given element  //div[text()='Material']
  Select value from the DDL  Style
  
Verify the popup dialog fields
  Click Element  ${NewBusinessCategoryDialog_CurrencyDDL}
  Assert for the given element  //div[text()='${Currency1}']
  Assert for the given element  //div[text()='${Currency2}']
  Page Should Not Contain Element  //div[text()='${Currency3}']
  Select value from the DDL  ${Currency2}
  
Enter the values
  Click Element  ${NewBusinessCategoryDialog_NodeTextArea}
  Input Text  ${NewBusinessCategoryDialog_NodeTextArea}  ${BusinessCategoryStyleName1}
  Click on Save Button
  DOMready-PopUpCloses
  Wait Until Page Contains Element  //span[text()='${BCCreationMSG}']
  Wait Until Element Is Visible   ${Loc_Home-BusinessPlanning-BusinessCategories_ActionsLink}

Enter the values to create Material Business Category
  Click Element  ${Loc_Home-BusinessPlanning-BusinessCategories_ActionsLink}
  Wait Until Page Contains Element  ${BusinessCategoryDialog} 
  Click Element  ${NewBusinessCategoryDialog_NodeTextArea}
  Input Text  ${NewBusinessCategoryDialog_NodeTextArea}  ${MaterialBusinessCategoryName1}
  Click Element  ${NewBusinessCategoryDialog_ProductTypeDDL}
  Select value from the DDL  Material
  Click Element  ${NewBusinessCategoryDialog_CurrencyDDL}
  Select value from the DDL  ${Currency2}
  Click on Save Button
  DOMready-PopUpCloses

Verify the options for a business category Edt,Copy,Delete
  @{TableIcons} =  Create List  Edit  Copy  Delete
  Set Suite Variable  @{TableIcons}
  :FOR  ${ListItems}  IN  @{TableIcons}
  \  Page Should Contain Element  //td/a[text()='${MaterialBusinessCategoryName1}']/following::td[5]/div/span[@data-csi-act='${ListItems}']
    
Verify the Edit option
  Click Element     //td/a[text()='${MaterialBusinessCategoryName1}']/following::td[5]/div/span[@data-csi-act='Edit']
  DOMready-PopUpAppears
  PO_SetupPage.Verify Dialog Window  Edit
  #Wait Until Page Contains Element  //span[@class='dijitDialogTitle' and text()='Edit']
  Page Should Contain Element  //div[contains(@class,'dijitTextBoxDisabled') and @data-csi-automation='field-BusinessCategory-Form-FilterType']
  Page Should Contain Element  //div[contains(@class,'dijitTextBoxDisabled') and @data-csi-automation='field-BusinessCategory-Form-BusinessCurrency']
  Verify the Element  ${Dialog_SaveButton}   Save Button on Edit Dialog is Displayed 
  Verify the Element  ${Dialog_CancelButton}   Cancel Button on Edit Dialog is Displayed 
  Click Element  ${NewBusinessCategoryDialog_NodeTextArea}
  Clear Element Text  ${NewBusinessCategoryDialog_NodeTextArea}
  Input Text  ${NewBusinessCategoryDialog_NodeTextArea}  ${MaterialBusinessCategoryName2}
  Click on Save Button
  DOMready-PopUpCloses
  
verify the Copy option
  #validation of Copy icon
  Click Element     //td/a[text()='${MaterialBusinessCategoryName2}']/following::td[5]/div/span[@data-csi-act='Copy']
  DOMready-PopUpAppears
  PO_SetupPage.Verify Dialog Window  Copy
  #Wait Until Page Contains Element  //span[@class='dijitDialogTitle' and text()='Copy']
  Clear Element Text  ${NewBusinessCategoryDialog_NodeTextArea}
  Input Text  ${NewBusinessCategoryDialog_NodeTextArea}  ${MaterialBusinessCategoryName3}
  Click on Save Button
  DOMready-PopUpCloses
  #validation of Delete icon
  Click Element     //td/a[text()='${MaterialBusinessCategoryName3}']/following::td[5]/div/span[@data-csi-act='Delete']
  DOMreadyByWaitTime  5
  #PO_SetupPage.Verify Dialog Window  Delete Business Category?
  Wait Until Page Contains Element  //span[@class='dijitDialogTitle' and text()='Delete Business Category?']
  Click Yes No Buttons  Delete
  Wait Until Page Contains Element  //span[text()='${DeleteConfirmation}']
  
Click on the Style -business category and verify the tabs
  Click Element  ${Loc_Home-BusinessPlanning-BusinessCategoriesTab}
  Click Link   //td/a[text()='${BusinessCategoryStyleName1}']
  Wait Until Element Is Visible  ${Loc_Release}
  Page Should Contain Element  //div[@data-csi-template-rule='Rule1']/div[text()='Properties']
  Page Should Contain Element  //div[@data-csi-template-rule='Rule2']/div[text()='Business Markets']
  
Verify the columns
    #Business category, Type, Currency, Product Filters
  Verify the Element  //div[text()='Business Category']    Business Category Attribute is displayed on Business Categories Properties Section 
  Verify the Element  //div[text()='Product Type']    Product Type Attribute is displayed on Business Categories Properties Section
  Verify the Element  //th[text()='Currency']    Currency Attribute is displayed on Business Categories Properties Section
  Verify the Element  //th[text()='Product Filters']    Product Filters Attribute is displayed on Business Categories Properties Section

Verify the column names
  Verify the Element  //th[text()='Business Market']    Business Market Column is displayed in Business Categories Table
  Verify the Element  //th[text()='Code']    Code Column is displayed in Business Categories Table
  Verify the Element  //th[text()='Currency']    Currency Column is displayed in Business Categories Table
  Verify the Element  //th[text()='Split (%)']    Split % Column is displayed in Business Categories Table
  Verify the Element  //th[text()='Countries']    Countries Column is displayed in Business Markets Table of Business Categories Node
  Verify the Element  //th[text()='Default Country']    Default Country Column is displayed in Business Markets Table of Business Categories Node
  Verify the Element  //th[text()='Shipping Ports']    Shipping Port Column is displayed in Business Markets Table of Business Categories Node
  Verify the Element  //th[text()='Default Shipping Port']  Default Shipping Port Column is displayed Business Markets Table of Business Categories Node
  Verify the Element  //th[text()='HTS Code Applicable']  HTS Code Applicable Column is displayed in Business Markets Table of Business Categories Node
  Verify the Element  //th[text()='Shipping Rate Applicable']  Shipping Rate Applicable Column is displayed in Business Markets Table of Business Categories Node
  Verify the Element  //th[text()='Active']  Active Column is displayed in Business Markets Table of Business Categories Node
Create business market in a business category
  CLick Element  ${Loc_BusinessCategorieNode-BusinessMarket_ActionsLink}
  DOMreadyByWaitTime  5
  PO_SetupPage.Verify Dialog Window  New Business Market
  #Wait Until Page Contains Element  ${BusinessMarketDialog} 
  @{BusinessMarketDialogAtrributes} =  Create List  ${NewBusinessMarketDialog_CodeTextArea}  ${NewBusinessMarketDialog_NodeTextArea}  ${NewBusinessMarketDialog_CurrencyDDL}
  ${Index} =  Set Variable  0
  :FOR  ${ListItems}  IN  @{BusinessMarketDialogAtrributes}
    \  Page Should Contain Element  ${ListItems}
    \  Run Keyword If  str('${Index}') > str('0')  Page Should Contain Element  //tr/th[text()='${ListItems}']/following-sibling::td[text()='*']
    
  Verify the Element  ${Dialog_Save&GoButton}    Save&Go Button on Business Market Dialog is Displayed 
  Verify the Element  ${Dialog_SaveButton}   Save Button on Business Market Dialog is Displayed 
  Verify the Element  ${Dialog_Save&NewButton}    Save&New Button on Business Market Dialog is Displayed 
  Verify the Element  ${Dialog_CancelButton}   Cancel Button on Business Market Dialog is Displayed 

Verify the new business market dialog
  DOMreadyByWaitTime  5
  PO_SetupPage.Verify Dialog Window  New Business Market
  #Wait Until Page Contains Element  ${BusinessMarketDialog}
  Click Element From List  ${NewBusinessMarketDialog_CurrencyDDL}  -1
  DOMreadyByWaitTime  2
  Select value from the DDL  ${Currency2}
  DOMreadyByWaitTime  2
  Click Element  ${NewBusinessMarketDialog_NodeTextArea}
  Input Text  ${NewBusinessMarketDialog_NodeTextArea}  ${BusinessMarketName1}
  Click Element  ${NewBusinessMarketDialog_CodeTextArea}
  Input Text  ${NewBusinessMarketDialog_CodeTextArea}  ${BusinessMarketCode1}
   DOMreadyByWaitTime  2
  #Click Element From List  ${NewBusinessMarketDialog_CurrencyDDL}  -1

  Click Element  ${Dialog_SaveButton}
  Wait Until Page Contains Element  //span[text()='${BMCreationMSG}']
  DOMreadyByWaitTime  4
Verify the new business market dialog
  CLick Element From List  ${Loc_BusinessCategorieNode-BusinessMarket_ActionsLink}  -1
  DOMreadyByWaitTime  5
  PO_SetupPage.Verify Dialog Window  New Business Market
  #Wait Until Page Contains Element  ${BusinessMarketDialog} 
  #Wait Until Page Contains Element  ${NewBusinessMarketDialog_NodeTextArea}
  
  #Click Element From List  ${NewBusinessMarketDialog_CurrencyDDL}  -1
  #Click Element From List  ${NewBusinessMarketDialog_CurrencyDDL}  -1
  DOMreadyByWaitTime  2
  #Select value from the DDL  ${Currency1}
  
  DOMreadyByWaitTime  2
  
  Click Element  ${NewBusinessMarketDialog_NodeTextArea}
  Input Text  ${NewBusinessMarketDialog_NodeTextArea}  ${BusinessMarketName2}
  Click Element  ${NewBusinessMarketDialog_CodeTextArea}
  Input Text  ${NewBusinessMarketDialog_CodeTextArea}  ${BusinessMarketCode2}
  DOMreadyByWaitTime  2
  Click on Save Button
  DOMready-PopUpCloses
  Page Should Contain Element  //td/a[text()='${BusinessMarketName1}']
  Page Should Contain Element  //td/a[text()='${BusinessMarketName2}']
  ###Click Element
  DOMreadyByWaitTime  5

Verify the copy,delete options
  ${NodeUrl} =  Get Element Attribute  //a[text()='${BusinessMarketName1}']/parent::td  data-csi-url
  Page Should Contain Element  //td/div/span[@data-csi-act='Copy' and @data-csi-url='${NodeUrl}']
  Page Should Contain Element  //td/div/span[@data-csi-act='Delete' and @data-csi-url='${NodeUrl}']
  # Mouse Over  //a[text()='${BusinessMarketName1}']
  DOMreadyByWaitTime  5
  #Click Element  css=[data-csi-automation="plugin-BusinessCategory-BusinessMarkets-refresh"]
  #Wait Until Page contains Element  ${Loc_Release}
  #Wait Until Element Is Visible  //a[@class='browse' and text()='${BusinessMarketName1}']
  Click Element  //td/div/span[@data-csi-act='Copy' and @data-csi-url='${NodeUrl}']
  DOMready-PopUpAppears
  DOMreadyByWaitTime  5
  PO_SetupPage.Verify Dialog Window  Copy
  #Wait Until Page Contains Element  //span[@class='dijitDialogTitle' and text()='Copy']
  Clear Element Text  ${NewBusinessMarketDialog_NodeTextArea}
  Input Text  ${NewBusinessMarketDialog_NodeTextArea}  ${BusinessMarketName3}
  Click on Save Button
  DOMready-PopUpCloses
  Mouse Over  //a[text()='${BusinessMarketName1}']
  Click Element  ${Loc_BusinessCategpryTab}
  Click Element  //td/div/span[@data-csi-act='Delete' and @data-csi-url='${NodeUrl}']
  DOMready-PopUpAppears
  Wait Until Page Contains Element  //span[@class='dijitDialogTitle' and text()='Delete Business Market?']
  Click Yes No Buttons  Cancel   
Creating a view for Business Market
    Wait.DOMreadyByWaitTime  5 
    Click Element From List  css=[data-csi-automation='plugin-BusinessCategory-BusinessMarkets-CustomViewActions'] .dijitButtonText  -1
  Wait.DOMreadyByWaitTime  2
  Click Element From List  css=[data-csi-automation='plugin-BusinessCategory-BusinessMarkets-CustomViewManage'] .dijitMenuItemLabel  -1
  Wait.DOMreadyByWaitTime  5
  #Wait Until Page Contains Element  xpath=//span[text()='Add >']
  Wait.DOMreadyByWaitTime  2	
# Copy the Default CV and create a new CV
  PO_SetupPage.Click on Default Custom View
  PO_SetupPage.Click on Copy button
  Clear Element Text  ${Loc_SecurityRoleName}
  Selenium2Library.Input Text  ${Loc_SecurityRoleName}  BMCV
  Wait.DOMreadyByWaitTime  2
  #Click Element		//div[@class='csiPreferenceDisplay']//div[@class='dijitReset dijitRight dijitButtonNode dijitArrowButton dijitDownArrowButton dijitArrowButtonContainer']         
  #Click Element          //div[@class='dijitReset dijitMenuItem' and text()='Material']      
#Add the  attribute to the view 
  Click Element 		css=.csiPreferenceSelect [value='Code::0']
  Wait.DOMreadyByWaitTime  2
  Click Element         //span[text()='< Remove']     
#Add the attribute to the view
  Wait.DOMreadyByWaitTime  2
  Click Element 		css=.csiPreferenceSelect [value='MarketCurrency::0']
  Wait.DOMreadyByWaitTime  2
  Click Element         //span[text()='< Remove']
  Wait.DOMreadyByWaitTime  2
  Click Element 		css=.csiPreferenceSelect [value='Countries::0']
  Wait.DOMreadyByWaitTime  2
  Click Element         //span[text()='< Remove']
  Wait.DOMreadyByWaitTime  2
  Click Element 		css=.csiPreferenceSelect [value='DefaultCountry::0']
  Wait.DOMreadyByWaitTime  2
  Click Element         //span[text()='< Remove']
  Wait.DOMreadyByWaitTime  2
    Click Element 		css=.csiPreferenceSelect [value='DefaultPort::0']
  Wait.DOMreadyByWaitTime  2
  Click Element         //span[text()='< Remove']
  Wait.DOMreadyByWaitTime  2
  
  #Click Element 		css=.csiPreferenceSelect [value='Remarks:Child:PurchaseTerms:0']
  #Click Element         //span[text()='< Remove']
  Wait.DOMreadyByWaitTime  2
  #Click Element 		css=.csiPreferenceSelect [value='Comments::0']
  #Click Element         //span[text()='< Remove']
  Wait.DOMreadyByWaitTime  2
  Click Element From List  Xpath=//span[text()='Save']  -1 
  Wait.DOMreadyByWaitTime  5
Verify the active option
  Page Should Contain Element  //a[text()='${BusinessMarketName1}']/parent::td/following::div[1][@data-csi-act='DutyApplicable::0']/input[@aria-checked='true']
  Page Should Contain Element  //a[text()='${BusinessMarketName1}']/parent::td/following::div[2][@data-csi-act='FreightApplicable::0']/input[@aria-checked='true']
  Page Should Contain Element  //a[text()='${BusinessMarketName1}']/parent::td/following::div[3][@data-csi-act='Active::0']/input[@aria-checked='true']
  DOMReadyByWaitTime  3
  Click Element  //a[text()='${BusinessMarketName3}']/parent::td/following::div[3][@data-csi-act='Active::0']/input[@aria-checked='true']
  Page Should Contain Element  //a[text()='${BusinessMarketName3}']/parent::td/following::div[3][@data-csi-act='Active::0']/input[@aria-checked='false']
         

Entering the Business Market Split % and the Shipping port
    Click Element  //a[text()='${BusinessMarketName1}']//following::td[1][@data-csi-heading='SplitPct::0']
    DOMReadyByWaitTime  3
    #Input Text  //div[@data-csi-automation='edit-BusinessCategory-BusinessMarkets-SplitPct:']//div[2]//input  20
    #Input Text  //div/input[@value='0']  20
    Enter The Values on Textarea  //div[contains(@class,'csi-flex-row')]//input[contains(@class,'dijitReset dijitInputInner')]  20
  Click Element  ${Loc_BusinessCategpryTab}
  DOMready-TabLoads
    Click Element  //a[text()='${BusinessMarketName2}']//following::td[1][@data-csi-heading='SplitPct::0']
    DOMReadyByWaitTime  3
    #Input Text  //div[@data-csi-automation='edit-BusinessCategory-BusinessMarkets-SplitPct:']//div[2]//input  40
    #Input Text  //div/input[@value='0']  40
    Enter The Values on Textarea  //div[contains(@class,'csi-flex-row')]//input[contains(@class,'dijitReset dijitInputInner')]  40
  Click Element  ${Loc_BusinessCategpryTab}
  DOMready-TabLoads
    Click Element  //a[text()='${BusinessMarketName3}']//following::td[1][@data-csi-heading='SplitPct::0']
    DOMReadyByWaitTime  3
    #Input Text  //div[@data-csi-automation='edit-BusinessCategory-BusinessMarkets-SplitPct:']//div[2]//input  40
    #Input Text  //div/input[@value='0']  40
    Enter The Values on Textarea  //div[contains(@class,'csi-flex-row')]//input[contains(@class,'dijitReset dijitInputInner')]  40
  Click Element  ${Loc_BusinessCategpryTab}
  DOMready-TabLoads
Select the Shipping Ports
    Click Element From List  //a[text()='${BusinessMarketName1}']/following::td[@data-csi-heading='ShippingPorts::0']  0
   DOMReadyByWaitTime  3
    Click Checkbox Item in DDL Within a PopUp  ${ShippingPort} 
   Click Element  ${Loc_BusinessCategpryTab}
   DOMready-TabLoads
   Click Element From List  //a[text()='${BusinessMarketName2}']/following::td[@data-csi-heading='ShippingPorts::0']  0
   DOMReadyByWaitTime  3
    Click Checkbox Item in DDL Within a PopUp  ${ShippingPort}
   Click Element  ${Loc_BusinessCategpryTab}
   DOMready-TabLoads
    Click Element From List  //a[text()='${BusinessMarketName3}']/following::td[@data-csi-heading='ShippingPorts::0']  0
    DOMReadyByWaitTime  3
    Click Checkbox Item in DDL Within a PopUp  ${ShippingPort}
   Click Element  ${Loc_BusinessCategpryTab}
   DOMready-TabLoads   
go to the business market and verify the tabs
  Click Link  //a[text()='${BusinessMarketName1}']
  DOMready-TabLoads
  Page Should Contain Element  ${Loc_BusinessMarketNode-BusinessMarketTab}
  Page Should Contain Element  ${Loc_BusinessMarketNode-PropertiesTab}
  Page Should Contain Element  ${Loc_BusinessMarketNode-WhereUsedTab}
On properties tab edit the values
  [Tags]  02
  Click Element  ${Loc_BusinessMarketNode-PropertiesTab}
  Wait Until Page contains Element  ${Loc_Release}
  Click Element  ${Loc_BusinessMarketNode-PropertiesTab}
  DOMready-TabLoads
  Click Element  //td[@data-csi-act='Countries::0']
  Wait Until Element is Visible  //label[text()='${CountryName1}']/preceding::div[1]/input
  Click Element  //label[text()='${CountryName1}']/preceding::div[1]/input
  Click Element  ${Loc_BusinessMarketNode-PropertiesTab}
  DOMready-TabLoads

  
Verify the presense of where used tab
   Click Element  ${Loc_BusinessMarketNode-WhereUsedTab}
   Wait Until Page contains Element  ${Loc_Release}  
The steps to add the Business Market% for Material Business Category  
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads
  Click Element     ${Loc_Home-BusinessPlanningTab}
  DOMready-TabLoads
  Click Link   //td/a[text()='${MaterialBusinessCategoryName2}']
  DOMready-TabLoads
  Wait Until Element Is Visible  ${Loc_Release}
Business Planning-Create business market in a Material business category
  CLick Element  ${Loc_BusinessCategorieNode-BusinessMarket_ActionsLink}
  Wait Until Page Contains Element  ${BusinessMarketDialog} 
  #Click Element From List  ${NewBusinessMarketDialog_CurrencyDDL}  -1
  DOMreadyByWaitTime  2
  #Select value from the DDL  ${Currency2}
  DOMreadyByWaitTime  2
  Click Element  ${NewBusinessMarketDialog_NodeTextArea}
  Input Text  ${NewBusinessMarketDialog_NodeTextArea}  ${BusinessMarketNameMat1}
  Click Element  ${NewBusinessMarketDialog_CodeTextArea}
  Input Text  ${NewBusinessMarketDialog_CodeTextArea}  BM-N03
   DOMreadyByWaitTime  2
  #Click Element From List  ${NewBusinessMarketDialog_CurrencyDDL}  -1

  Click Element  ${Dialog_SaveButton}
  Wait Until Page Contains Element  //span[text()='${BMCreationMSG}']
  DOMreadyByWaitTime  4
  CLick Element From List  ${Loc_BusinessCategorieNode-BusinessMarket_ActionsLink}  -1
  Wait Until Page Contains Element  ${BusinessMarketDialog} 

  DOMreadyByWaitTime  2
  
  Click Element  ${NewBusinessMarketDialog_NodeTextArea}
  Input Text  ${NewBusinessMarketDialog_NodeTextArea}  ${BusinessMarketNameMat2}
  Click Element  ${NewBusinessMarketDialog_CodeTextArea}
  Input Text  ${NewBusinessMarketDialog_CodeTextArea}  BM-N04
  DOMreadyByWaitTime  2
  Click on Save Button
  DOMready-PopUpCloses
  DOMreadyByWaitTime  5

  ${NodeUrl} =  Get Element Attribute  //a[text()='${BusinessMarketNameMat1}']/parent::td  data-csi-url
   DOMreadyByWaitTime  5
  Click Element  //td/div/span[@data-csi-act='Copy' and @data-csi-url='${NodeUrl}']
  DOMready-PopUpAppears
  DOMreadyByWaitTime  5
  PO_SetupPage.Verify Dialog Window  Copy
  #Wait Until Page Contains Element  //span[@class='dijitDialogTitle' and text()='Copy']
  Clear Element Text  ${NewBusinessMarketDialog_NodeTextArea}
  Input Text  ${NewBusinessMarketDialog_NodeTextArea}  ${BusinessMarketNameMat3}
  Click on Save Button
  DOMready-PopUpCloses  
Entering the Business Market Split % and the Shipping port for Material Business Category
    Click Element  //a[text()='${BusinessMarketNameMat1}']//following::td[1][@data-csi-heading='SplitPct::0']
    DOMReadyByWaitTime  3
    #Input Text  //div[@data-csi-automation='edit-BusinessCategory-BusinessMarkets-SplitPct:']//div[2]//input  20
    #Input Text  //div/input[@value='0']  20
    Enter The Values on Textarea  //div[contains(@class,'csi-flex-row')]//input[contains(@class,'dijitReset dijitInputInner')]  20
  Click Element  ${Loc_BusinessCategpryTab}
  DOMready-TabLoads
    Click Element  //a[text()='${BusinessMarketNameMat2}']//following::td[1][@data-csi-heading='SplitPct::0']
    DOMReadyByWaitTime  3
    #Input Text  //div[@data-csi-automation='edit-BusinessCategory-BusinessMarkets-SplitPct:']//div[2]//input  40
    #Input Text  //div/input[@value='0']  40
    Enter The Values on Textarea  //div[contains(@class,'csi-flex-row')]//input[contains(@class,'dijitReset dijitInputInner')]  40
  Click Element  ${Loc_BusinessCategpryTab}
  DOMready-TabLoads
    Click Element  //a[text()='${BusinessMarketNameMat3}']//following::td[1][@data-csi-heading='SplitPct::0']
    DOMReadyByWaitTime  3
    #Input Text  //div[@data-csi-automation='edit-BusinessCategory-BusinessMarkets-SplitPct:']//div[2]//input  40
    #Input Text  //div/input[@value='0']  40
    Enter The Values on Textarea  //div[contains(@class,'csi-flex-row')]//input[contains(@class,'dijitReset dijitInputInner')]  40
  Click Element  ${Loc_BusinessCategpryTab}
  DOMready-TabLoads
Select the Shipping Ports for Material Business Category Business markets
    Click Element From List  //a[text()='${BusinessMarketNameMat1}']/following::td[@data-csi-heading='ShippingPorts::0']  0
   DOMReadyByWaitTime  3
    Click Checkbox Item in DDL Within a PopUp  ${ShippingPort} 
   Click Element  ${Loc_BusinessCategpryTab}
   DOMready-TabLoads
   Click Element From List  //a[text()='${BusinessMarketNameMat2}']/following::td[@data-csi-heading='ShippingPorts::0']  0
   DOMReadyByWaitTime  3
    Click Checkbox Item in DDL Within a PopUp  ${ShippingPort}
   Click Element  ${Loc_BusinessCategpryTab}
   DOMready-TabLoads
    Click Element From List  //a[text()='${BusinessMarketNameMat3}']/following::td[@data-csi-heading='ShippingPorts::0']  0
    DOMReadyByWaitTime  3
    Click Checkbox Item in DDL Within a PopUp  ${ShippingPort}
   Click Element  ${Loc_BusinessCategpryTab}
   DOMready-TabLoads    
 
Test Data To Create a MSG for Material
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads
  Run Keyword And Ignore error  Click On Required Arrow Button To View Hidden Tabs  left
  DOMready-TabLoads
  PO_HomePage.Click On Material Tab
  DOMready-TabLoads
  Click Element  css=[data-csi-tab='ApparelViews-MaterialSecurityGroups']
  DOMready-TabLoads
  #Wait Until Custom View Text Area Is Enabled   ApparelViews-MaterialSecurityGroups
  Click Element  css=[data-csi-automation='plugin-ApparelViews-MaterialSecurityGroups-ToolbarNewActions']
  DOMreadyByWaitTime    2
  Polling Click Tab  css=[data-csi-tab='ApparelViews-MaterialSecurityGroups']
  DOMreadyByWaitTime    2
  Click Element From List  //a[text()='(Unnamed)']  -1
  DOMready-TabLoads
  Click Element  //td[@data-csi-heading='Node Name::0']     
  Input Text  //div//textarea  ${SecurityGroup1}  
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads
  PO_HomePage.Click On Material Tab
  DOMready-TabLoads
  Click Element  css=[data-csi-tab='ApparelViews-MaterialSecurityGroups']
  DOMready-TabLoads

Add Materials To Msg
  [Tags]  msg1
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads
  PO_HomePage.Click On Material Tab
  DOMready-TabLoads
  Click Element  css=[data-csi-tab='ApparelViews-MaterialSecurityGroups']
  DOMready-TabLoads
  #Wait Until Custom View Text Area Is Enabled   ApparelViews-MaterialSecurityGroups
  Click Link   //td[@data-csi-heading='Node Name::0']/a[text()='${SecurityGroup1}']
  DOMready-TabLoads
  Click Element  css=[data-csi-automation='plugin-MaterialSecurityGroup-SecurityGroupMaterials-ToolbarNewActions'] td.dijitArrowButton   
  DOMReadyByWaitTime  4
  Click Element From List  css=[data-csi-automation='plugin-MaterialSecurityGroup-SecurityGroupMaterials-ToolbarNewActions'] [data-csi-act='AggregateMaterials'] .dijitMenuItemLabel  -1
  
  #Click Element From List  css=[data-csi-automation='plugin-MaterialSecurityGroup-SecurityGroupMaterials-ToolbarNewActions'] [data-csi-act='MoveFromMaterial'] .dijitMenuItemLabel  -1
  DOMready-PopUpAppears
  Wait Until Page Contains Element   //*[text()='${MaterialName1}']
  DOMReadyByWaitTime  4
  Click Element  //*[text()='${MaterialName1}']
  DOMReadyByWaitTime  2
  Click on Save Button
  DOMready-PopUpCloses
  DOMready-TabLoads   
  
  [Teardown]    Close Browser 
     

***Keywords***
Setup The Variables
  ${This_DataProvider} =  Data_Provider.DataProvider  Business Planning
  Set Suite Variable  ${This_DataProvider}
  Log  ${This_DataProvider}
  ${CurrencyTableCollection} =  Data_Provider.GetDataProviderColumnValue  Data_CurrencyTableName
  ${CurrencyTable1} =  DataProviderSplitterForMultipleValues  ${CurrencyTableCollection}  1
  ${CurrencyTable2} =  DataProviderSplitterForMultipleValues  ${CurrencyTableCollection}  2
  Set Suite Variable  ${CurrencyTable1}
  Set Suite Variable  ${CurrencyTable2}
  ${CurrencyCollection} =  Data_Provider.GetDataProviderColumnValue  Data_Currency
  ${Currency1} =  DataProviderSplitterForMultipleValues  ${CurrencyCollection}  1
  ${Currency2} =  DataProviderSplitterForMultipleValues  ${CurrencyCollection}  2
  ${Currency3} =  DataProviderSplitterForMultipleValues  ${CurrencyCollection}  3
  Set Suite Variable  ${Currency1}
  Set Suite Variable  ${Currency2}
  Set Suite Variable  ${Currency3}
  ${ShippingPortNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_ShippingPortName
  ${ShippingPortName1} =  DataProviderSplitterForMultipleValues  ${ShippingPortNameCollection}  1
  ${ShippingPortName3} =  DataProviderSplitterForMultipleValues  ${ShippingPortNameCollection}  3
  Set Suite Variable  ${ShippingPortName1}
  Set Suite Variable  ${ShippingPortName3}
  ${CountryNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_CountryName
  ${CountryName1} =  DataProviderSplitterForMultipleValues  ${CountryNameCollection}  1
  ${CountryName2} =  DataProviderSplitterForMultipleValues  ${CountryNameCollection}  2
  Set Suite Variable  ${CountryName1} 
  Set Suite Variable  ${CountryName2}  
  ${BusinessMarketNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_BusinessMarketName
  ${BusinessMarketName1} =  DataProviderSplitterForMultipleValues  ${BusinessMarketNameCollection}  1
  ${BusinessMarketName2} =  DataProviderSplitterForMultipleValues  ${BusinessMarketNameCollection}  2
  ${BusinessMarketName3} =  DataProviderSplitterForMultipleValues  ${BusinessMarketNameCollection}  3  
  Set Suite Variable  ${BusinessMarketName1} 
  Set Suite Variable  ${BusinessMarketName2}
  Set Suite Variable  ${BusinessMarketName3}
  ${BusinessCategoryStyleNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_BusinessCategoryStyleName
  ${BusinessCategoryStyleName1} =  DataProviderSplitterForMultipleValues  ${BusinessCategoryStyleNameCollection}  1
  Set Suite Variable  ${BusinessCategoryStyleName1}
  ${MaterialBusinessCategoryNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_MaterialBusinessCategoryName
  ${MaterialBusinessCategoryName1} =  DataProviderSplitterForMultipleValues  ${MaterialBusinessCategoryNameCollection}  1
  ${MaterialBusinessCategoryName2} =  DataProviderSplitterForMultipleValues  ${MaterialBusinessCategoryNameCollection}  2
  ${MaterialBusinessCategoryName3} =  DataProviderSplitterForMultipleValues  ${MaterialBusinessCategoryNameCollection}  3
  Set Suite Variable  ${MaterialBusinessCategoryName1}
  Set Suite Variable  ${MaterialBusinessCategoryName2}
  Set Suite Variable  ${MaterialBusinessCategoryName3}
  ${SecurityGroupCollection} =  Data_Provider.GetDataProviderColumnValue  Data_SecurityGroup
  ${SecurityGroup1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SecurityGroupCollection}  1
  Set Suite Variable  ${SecurityGroup1}
    
  #${MaterialNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_MaterialName
  #${MaterialName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MaterialNameCollection}  1
  #Set Suite Variable  ${MaterialName1}
StaleElementTest
    [Arguments]   ${Element}
    Click Element  //td[@data-csi-act='SplitPct::0']
    Wait Until Element Is Enabled  ${Element}
    Click Element  ${Element}
    DOMreadyByWaitTime  2
    # Press backspace button
    Press Key  ${Element}/input  \\08
    DOMreadyByWaitTime  2
    Press Key  ${Element}/input  50
Enter The Values on Textarea
    [Arguments]   ${Element}  ${InputData}
    DOMreadyByWaitTime  2
    # Press backspace button
    Press Key  ${Element}  \\08
    DOMreadyByWaitTime  2
    Press Key  ${Element}  ${InputData}
***comments

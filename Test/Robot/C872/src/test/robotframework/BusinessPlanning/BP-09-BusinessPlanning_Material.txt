*** Settings ***

Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_SetupPage.txt
Resource          ../PageObjects/PO_SourcingPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_EnumerationPage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../Locators/Loc_SetupPage.txt    
Resource          ../Locators/Loc_BusinessPlanning.txt
Resource          ../Locators/Loc_MaterialPage.txt
Resource          ../Locators/Loc_MaterialLibrary.txt

    
***Variables**
${Var_Volume}  3000
${Var_Margin}  5
${BusinessMarketCode1}  BM-N01
${BusinessMarketCode2}  BM-S02 
${SupplierName1}  Iceplant
${MaterialBusinessPlanName3}  AUT_MaterialBusinessPlan3
${BusinessMarketNameMat1}  CENTRAL
${BusinessMarketNameMat2}  WEST
${BusinessMarketNameMat3}  NORTHEAST


*** Test Cases *** 
Test Setup   
    [tags]  Business Planning         
    Setup The Variables  
Create Business Planning 
    [tags]  Business Planning
  SuiteSetUp.Open Browser To Login Page
  PO_LoginPage.Input UserName    NewUser_UserName
  PO_LoginPage.Input Password    NewUser_Password
  PO_LoginPage.Submit Credentials
  Wait For Release Info
  #Reload
  DOMready-TabLoads 
 



Verify the Business Plan Table
    [tags]  Business Planning
#Once a category created for a Material is selected in the dialog box, check for the following - 
  #Reload
  # Wait.DOMready
  Click on Home Icon
  Wait For Release Info    
  Click Element     ${Loc_Home-BusinessPlanningTab}
  DOMready-TabLoads
  Click Element  ${Loc_Home-BusinessPlanning-BusinessPlansTab}
  DOMready-TabLoads
  Verify the Element  //th[text()='Business Plan']    Business Plan Column is displayed in Button Business Plans Table
  Verify the Element  //th[text()='Category']    Category Column is displayed in Button Business Plans Table
  Verify the Element  //th[text()='Currency Table']    Currency Table Column is displayed in Button Business Plans Table
  Verify the Element  //th[text()='Business Market Plans']    Business Market Plan Column is displayed in Button Business Plans Table
  Verify the Element  //th[text()='Product Filters']    Product Filters Column is displayed in Button Business Plans Table
  Verify the Element  //th[text()='Target Volume']    Target Volume Column is displayed in Button Business Plans Table
  Verify the Element  //th[text()='Target Margin (%)']    Target Margin (%) Column is displayed in Button Business Plans Table
 
Verify the Material Security Group field
    [tags]  Business Planning
#Check if a Material Security Group attribute appears in the dropdown. Check if all the MSGs created in the application are available in the dropdown
  Mouse Over  ${Loc_Home-BusinessPlanning_ActionsLink}
  #Wait Until Element Is Visible  ${Loc_Home-BusinessPlanning_Actions_NewBusinessPlanLink}
  Click Element  ${Loc_Home-BusinessPlanning_ActionsLink}  
  Wait Until Page Contains Element  ${BusinessPlanDialog} 
  @{BusinessPlanDialogAtrributes} =  Create List  ${NewBusinessPlanDialog_CurrencyTableDDL}  ${NewBusinessPlanDialog_SupplierDDL}  ${NewBusinessPlanDialog_CategoryDDL}  ${NewBusinessPlanDialog_NodeTextArea}
  :FOR  ${ListItems}  IN  @{BusinessPlanDialogAtrributes}
  \  Page Should Contain Element  ${ListItems}

  # Selecting the category
  #Click Element  ${NewBusinessPlanDialog_CategoryDDL}
  DOMreadyByWaitTime  4
  Click On the DDL Button2  field-BusinessPlan-Form-Category
  Page Should Contain Element  //div[text()='${BusinessCategoryStyleName1}']
  Page Should Contain Element  //div[text()='${MaterialBusinessCategory2}']
  PO_NewCreationPopUpPage.Select value from the DDL  ${MaterialBusinessCategory2}
  # Select Currency 
  Click Element  ${NewBusinessPlanDialog_CurrencyTableDDL}
  Select value from the DDL   ${CurrencyTable1}
  # Select Supplier  
  Click Element  ${NewBusinessPlanDialog_SupplierDDL}
  Select value from the DDL  ${SupplierName1}
  # Assert for the Material Security Group 
  Click On the DDL Button  field-BusinessPlan-Form-BPMSG
  DOMreadyByWaitTime  2
  Assert for the Element in Table DDL   ${SecurityGroup1}
  Select value from the DDL  ${SecurityGroup1}
   
Confirm that the Material Security Group attribute is not marked as mandatory
    [tags]  Business Planning
  #Confirm that the Material Security Group attribute is not marked as mandatory
  Page Should Not Contain Element  //tr/th[text()='Material Security Group']/following-sibling::td[text()='*']
  Click Element  ${NewBusinessPlanDialog_NodeTextArea}
  Input Text  ${NewBusinessPlanDialog_NodeTextArea}  ${MaterialBusinessPlan1} 
  
Now click Next, check if the Business markets created under the said Category are appearing for selection. Check if all the entries are marked as true and are appearing correctly in the table. Now click on Finish and check if the Business Plan gets created.
    [tags]  Business Planning
  #Now click Next, check if the Business markets created under the said Category are appearing for selection. Check if all the entries are marked as true and are appearing correctly in the table. Now click on Finish and check if the Business Plan gets created.
  Click on Next Button
  DOMready-PopUpAppears  
  DOMreadyByWaitTime  4
  @{BusinessMarket} =  Create List  ${BusinessMarketNameMat1}  ${BusinessMarketNameMat2}  ${BusinessMarketNameMat3}
  Set Suite Variable  @{BusinessMarket}  
  :FOR  ${Listitems}  IN  @{BusinessMarket}
  \  Page Should Contain Element  //*[text()='${Listitems}']/preceding::td[1]/div/input[@aria-checked='true']
  Click Element  //td[text()='${BusinessMarketNameMat3}']/preceding::td/div[1]/input[@class='dijitReset dijitCheckBoxInput']
  Click on Finish Button
  DOMready-PopUpCloses
  Page Should Contain Element  //a[text()='${MaterialBusinessPlan1}']/parent::td[1]
  DOMreadyByWaitTime  4
#Business Planning-02902-Check if the Business Markets selected are appearing in the Business Plan table  
  #Check if the Business Markets selected are appearing in the Business Plan table
  #Page Should Contain Element  //a[text()='${MaterialBusinessPlan1}']/parent::td[1]/following::td[@data-csi-heading='Children::0']/span[text()='${BusinessMarketNameMat2}']
  #Page Should Contain Element  //a[text()='${MaterialBusinessPlan1}']/parent::td[1]/following::td[@data-csi-heading='Children::0']/span[text()='${BusinessMarketNameMat1}']
  
Click on the Business Plan created and check for four other tabs Plan, Products, Market Product Plans and Currency Exchange Rates. Check if the Plan tab is selected by default
    [tags]  Business Planning
    #Click on the Business Plan created and check for four other tabs Plan, Products, Market Product Plans and Currency Exchange Rates. Check if the Plan tab is selected by default
  Click Element  //a[text()='${MaterialBusinessPlan1}']
  DOMready-TabLoads
  Verify the Tab Presence  BusinessPlan-BusinessPlan  Business Plan Tab is displayed 
  Verify the Tab Presence  BusinessPlan-BPProductPlans  Business Plan Product Plan Tab is displayed 
  Verify the Tab Presence  BusinessPlan-MarketProductPlans  Business Plan Market Product Plan Tab is displayed
  Verify the Tab Presence  BusinessPlan-ExchangeRates  Business Plan Exchange Rate Tab is displayed

Check for two sections Properties and Business Market Plans on the Plan tab
    [tags]  Business Planning
    #Check for two sections Properties and Business Market Plans on the Plan tab
  Click Element  ${Loc_BusinessPlanNode-PlanTab}
  DOMready-TabLoads
  Assert for the Given Element  ${Loc_BusinessPlan-PropertiesSection} 
  Assert for the Given Element  ${Loc_BusinessPlan-BusinessMarketPlanSection}
Edit the values in Business Planning properties 
    [tags]  Business Planning
  Click Element From List  ${Loc_BusinessPlanNode-Plan-TargetVolumeAttr}  0
  DOMreadyByWaitTime  2
  Enter The Values on Textarea  ${Loc_EditTargetVolumeValuePropertiesTab}  ${Var_Volume}
  Click Element  ${Loc_BusinessPlanNode-PlanTab}
  Click Element From List  ${Loc_BusinessPlanNode-Plan-TargetMarginAttr}  0
  DOMreadyByWaitTime  2
  Enter The Values on Textarea  ${Loc_EditTargetMarginValuePropertiesTab}  ${Var_Margin}
  Click Element  ${Loc_BusinessPlanNode-PlanTab}   
Check if the Business Plan related attributes are displayed in the Properties section. Edit these values and check if the correct attributes are displayed
    [tags]  Business Planning
    #Check if the Business Plan related attributes are displayed in the Properties section. Edit these values and check if the correct attributes are displayed
  Assert for the Given Element  //div[@class='csiHeadingCellLabel' and text()='Business Plan']//following::td[@class='attrString iconEditable' and text()='${MaterialBusinessPlan1}']
  Assert for the Given Element  //div[@class='csiHeadingCellLabel' and text()='Category']//following::td//a[@class='browse' and text()='${MaterialBusinessCategory2}']
  Assert for the Given Element  //div[@class='csiHeadingCellLabel' and text()='Currency Table']//following::td//a[@class='browse' and text()='${CurrencyTable1}']
  Assert for the Given Element  //div[@class='csiHeadingCellLabel' and text()='Business Market Plans']//following::td//span[text()='${BusinessMarketNameMat1}'] 
  Assert for the Given Element  //div[@class='csiHeadingCellLabel' and text()='Business Market Plans']//following::td//span[text()='${BusinessMarketNameMat2}'] 
  Click Element  css=[data-csi-act='Node Name::0']
  Enter The Values on Textarea  css=[data-csi-automation='edit-BusinessPlan-Properties-Node Name:']  ${MaterialBusinessPlan2}
  Click Element  ${Loc_BusinessPlanNode-PlanTab}
  Assert for the Given Element  //div[@class='csiHeadingCellLabel' and text()='Business Plan']//following::td[@class='attrString iconEditable' and text()='${MaterialBusinessPlan2}']
  
In the Properties section edit the business market field
    [tags]  Business Planning
    #In the Properties section, hover the mouse pointer on the Business Market Plans field. Check if a link 'Select Business Markets' appears. Click on the link and check if a dialog box appears
  Click Element  css=[data-csi-act='Children::0']
  Wait Until Element Is Visible  //label[@class='dijitMenuItemLabel' and text()='None']
Check if the active Business Markets created under the said Business Category are appearing for selection. 
    #Check if the active Business Markets created under the said Business Category are appearing for selection. Check if the already added markets are appearing selected in the dialog box.
   #DOMready-PopUpAppears
 
  #:FOR  ${Listitems}  IN  @{BusinessMarket}
  #\  Page Should Contain Element  //td[@data-csi-heading='Node Name::0' and text()='${Listitems}']/preceding::td[1]/div/input
   Click Checkbox Item in DDL Within a PopUp  None
   DOMreadyByWaitTime  2
   Click Checkbox Item in DDL Within a PopUp  ${BusinessMarketNameMat1}
   DOMreadyByWaitTime  2
   Click Checkbox Item in DDL Within a PopUp  ${BusinessMarketNameMat2}
   DOMreadyByWaitTime  2
   Click Checkbox Item in DDL Within a PopUp  ${BusinessMarketNameMat3}
   DOMreadyByWaitTime  2
   ${Index} =  Set Variable  0
  :FOR  ${ListItems}  IN  @{BusinessMarket}
  \  Run Keyword If  str('${Index}') == str('0')  Page Should Contain Element  //label[@class="dijitMenuItemLabel" and text()='${Listitems}']
  \  Run Keyword If  str('${Index}') == str('1')  Page Should Contain Element  //label[@class="dijitMenuItemLabel" and text()='${Listitems}']
  \  Run Keyword If  str('${Index}') == str('2')  Page Should Contain Element  //label[@class="dijitMenuItemLabel" and text()='${Listitems}']
  \  ${Index} =  Evaluate  ${Index} + 1 
  Click Element  ${Loc_BusinessPlanNode-PlanTab}
  DOMready-TabLoads
On the Business Market Plans table, check if the Business Markets selected are appearing in a table. Check and confirm that the table does not have any link and also confirm that the Actions column is not available
    [tags]  Business Planning
    #On the Business Market Plans table, check if the Business Markets selected are appearing in a table. Check and confirm that the table does not have any link and also confirm that the Actions column is not available
  :FOR  ${Listitems}  IN  @{BusinessMarket}
  \  Page Should Contain Element  //td[@class='attrString iconEditable firstColumn' and text()='${Listitems}']    
   :FOR  ${Listitems}  IN  @{BusinessMarket}
  \  Page Should Not Contain Element  //a[@class='browse' and text()='${Listitems}'] 
 
  Page Should Not Contain Element  //th[@class='sort lastColumn' and text()='Actions'] 
  

Check if the attributes in the table are editable. Make changes and check if they are retained correctly
    [tags]  Business Planning
    #Check if the attributes in the table are editable. Make changes and check if they are retained correctly

  Click Element  //tr[1]//following::td[1][@data-csi-act='SplitPct:Child:Target:0'] 
  Enter The Values on Textarea  ${Loc_BusinessPlanTab-EditTargetSplitColumn}  20
  Click Element  ${Loc_BusinessPlanNode-PlanTab}
 
  Click Element  //tr[2]//following::td[1][@data-csi-act='SplitPct:Child:Target:0'] 
  Enter The Values on Textarea  ${Loc_BusinessPlanTab-EditTargetSplitColumn}  40
  Click Element  ${Loc_BusinessPlanNode-PlanTab}
     
  Click Element  //tr[3]//following::td[1][@data-csi-act='SplitPct:Child:Target:0'] 
  Enter The Values on Textarea  ${Loc_BusinessPlanTab-EditTargetSplitColumn}  40
  Click Element  ${Loc_BusinessPlanNode-PlanTab}
  DOMready-TabLoads
In the Product Plans table, check for a table with attributes Product Plan, Description, Target Volume, Target Margin(%), Active and Size Range
    [tags]  Business Planning
    #In the Product Plans table, check for a table with attributes Product Plan, Description, Target Volume, Target Margin(%), Active and Size Range
  Click Element  ${Loc_BusinessPlanNode-ProductPlansTab}
  DOMready-TabLoads
  Verify the Element  //div[@class='csi-table-header-container']//following::span[@class='csi-table-header-content' and text()='Product Plan']    Product Plan Column is displayed Product Plans Table
  Verify the Element  //div[@class='csi-table-header-container']//following::span[@class='csi-table-header-content' and text()='Description']    Description Column is displayed in Product Plans Table
  ${Element} =  Required Element From List  //div[@class='csi-table-header-container']//following::span[@class='csi-table-header-content' and text()='Target Volume']  -1
  Page Should Contain Element   ${Element}
  ${Element} =  Required Element From List  //div[@class='csi-table-header-container']//following::span[@class='csi-table-header-content' and text()='Target Margin (%)']  -1
  Page Should Contain Element   ${Element}
  ${Element} =  Required Element From List  //div[@class='csi-table-header-container']//following::span[@class='csi-table-header-content' and text()='Active']  -1
  Page Should Contain Element   ${Element}
Creating second Material Business Plan
    [tags]  Business Planning
  Click on Home Icon
  Wait For Release Info    
  Click Element     ${Loc_Home-BusinessPlanningTab}
  DOMready-TabLoads
  Click Element  ${Loc_Home-BusinessPlanning-BusinessPlansTab}
  DOMready-TabLoads
  Mouse Over  ${Loc_Home-BusinessPlanning_ActionsLink}
  #Wait Until Element Is Visible  ${Loc_Home-BusinessPlanning_Actions_NewBusinessPlanLink}
  Click Element  ${Loc_Home-BusinessPlanning_ActionsLink}  
  Wait Until Page Contains Element  ${BusinessPlanDialog} 
  DOMreadyByWaitTime  4
  Click On the DDL Button2  field-BusinessPlan-Form-Category
  DOMreadyByWaitTime  3
  #Page Should Contain Element  //div[text()='${BusinessCategoryStyleName1}']
  #Page Should Contain Element  //div[text()='${MaterialBusinessCategory2}']
  PO_NewCreationPopUpPage.Select value from the DDL  ${MaterialBusinessCategory2}
  # Select Currency 
  Click Element  ${NewBusinessPlanDialog_CurrencyTableDDL}
  Select value from the DDL   ${CurrencyTable1}
  # Select Supplier  
  Click Element  ${NewBusinessPlanDialog_SupplierDDL}
  Select value from the DDL  ${SupplierName1}
  # Assert for the Material Security Group 
  Click On the DDL Button  field-BusinessPlan-Form-BPMSG
  DOMreadyByWaitTime  2
  Assert for the Element in Table DDL   ${SecurityGroup1}
  Select value from the DDL  ${SecurityGroup1}
  Click Element  ${NewBusinessPlanDialog_NodeTextArea}
  Input Text  ${NewBusinessPlanDialog_NodeTextArea}  ${MaterialBusinessPlanName3} 

  Click on Next Button
  DOMready-PopUpAppears  
  @{BusinessMarket} =  Create List  ${BusinessMarketNameMat1}  ${BusinessMarketNameMat2}  ${BusinessMarketNameMat3}
  Set Suite Variable  @{BusinessMarket}  
  :FOR  ${Listitems}  IN  @{BusinessMarket}
  \  Page Should Contain Element  //*[text()='${Listitems}']/preceding::td[1]/div/input[@aria-checked='true']
  Click Element  //td[text()='${BusinessMarketNameMat3}']/preceding::td/div[1]/input[@class='dijitReset dijitCheckBoxInput']
  Click on Finish Button
  DOMready-PopUpCloses
  Page Should Contain Element  //a[text()='${MaterialBusinessPlanName3}']/parent::td[1]  
Business Planning- Properties and Business Market Plans on the Plan tab
    [tags]  Business Planning
    #Check for two sections Properties and Business Market Plans on the Plan tab
  Click Element  //a[text()='${MaterialBusinessPlanName3}']
  DOMready-TabLoads
  Click Element  ${Loc_BusinessPlanNode-PlanTab}
  DOMready-TabLoads
  Assert for the Given Element  ${Loc_BusinessPlan-PropertiesSection} 
  Assert for the Given Element  ${Loc_BusinessPlan-BusinessMarketPlanSection}
Business Planning-Edit the values in properties-Material 
    [tags]  Business Planning
  Click Element From List  ${Loc_BusinessPlanNode-Plan-TargetVolumeAttr}  0
  DOMreadyByWaitTime  2
  Enter The Values on Textarea  ${Loc_EditTargetVolumeValuePropertiesTab}  ${Var_Volume}
  Click Element  ${Loc_BusinessPlanNode-PlanTab}
  Click Element From List  ${Loc_BusinessPlanNode-Plan-TargetMarginAttr}  0
  DOMreadyByWaitTime  2
  Enter The Values on Textarea  ${Loc_EditTargetMarginValuePropertiesTab}  ${Var_Margin}
  Click Element  ${Loc_BusinessPlanNode-PlanTab}   
    [Teardown]    Close Browser

 ***Keywords***
Setup The Variables
  ${This_DataProvider} =  Data_Provider.DataProvider  Business Planning
  Set Suite Variable  ${This_DataProvider}
  
  ${MaterialPlanCollection} =  Data_Provider.GetDataProviderColumnValue  Data_MaterialBusinessPlanName
  ${MaterialBusinessPlan1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MaterialPlanCollection}  1
  ${MaterialBusinessPlan2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MaterialPlanCollection}  2
  Set Suite variable  ${MaterialBusinessPlan1}
  Set Suite variable  ${MaterialBusinessPlan2}
  
  ${MaterialBusinessCategoryCollection} =  Data_Provider.GetDataProviderColumnValue  Data_MaterialBusinessCategoryName
  ${MaterialBusinessCategory1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MaterialBusinessCategoryCollection}  1
  ${MaterialBusinessCategory2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MaterialBusinessCategoryCollection}  2
  Set Suite variable  ${MaterialBusinessCategory1}
  Set Suite variable  ${MaterialBusinessCategory2}
  
  ${BusinessCategoryStyleNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_BusinessCategoryStyleName
  ${BusinessCategoryStyleName1} =  DataProviderSplitterForMultipleValues  ${BusinessCategoryStyleNameCollection}  1
  Set Suite variable  ${BusinessCategoryStyleName1}

  ${CurrencyTableCollection} =  Data_Provider.GetDataProviderColumnValue  Data_CurrencyTableName
  ${CurrencyTable1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${CurrencyTableCollection}  1
  Set Suite variable  ${CurrencyTable1}
  
  #${SupplierNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_SupplierName
  #${SupplierName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SupplierNameCollection}  1
  #Set Suite variable  ${SupplierName1}

  ${BusinessMarketNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_BusinessMarketName
  ${BusinessMarketName1} =  DataProviderSplitterForMultipleValues  ${BusinessMarketNameCollection}  1
  ${BusinessMarketName2} =  DataProviderSplitterForMultipleValues  ${BusinessMarketNameCollection}  2
  ${BusinessMarketName3} =  DataProviderSplitterForMultipleValues  ${BusinessMarketNameCollection}  3
  Set Suite variable  ${BusinessMarketName1}
  Set Suite variable  ${BusinessMarketName2}
  Set Suite variable  ${BusinessMarketName3}

  ${CurrencyCollection} =  Data_Provider.GetDataProviderColumnValue  Data_Currency
  ${Currency1} =  DataProviderSplitterForMultipleValues  ${CurrencyCollection}  1
  ${Currency2} =  DataProviderSplitterForMultipleValues  ${CurrencyCollection}  2
  ${Currency3} =  DataProviderSplitterForMultipleValues  ${CurrencyCollection}  3
  Set Suite variable  ${Currency1}
  Set Suite variable  ${Currency2}
  Set Suite variable  ${Currency3}
 
  ${SecurityGroupCollection} =  Data_Provider.GetDataProviderColumnValue  Data_SecurityGroup
  ${SecurityGroup1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SecurityGroupCollection}  1
  Set Suite variable  ${SecurityGroup1}
  
Enter The Values on Textarea
    [Arguments]   ${Element}  ${InputData}
    DOMreadyByWaitTime  2
    # Press backspace button
    Press Key  ${Element}  \\08
    DOMreadyByWaitTime  2
    Press Key  ${Element}  ${InputData}  

**comments**
Adding Business Markets to the Business Category Material
  Click on Home Icon
  Wait For Release Info    
  Click Element     ${Loc_Home-BusinessPlanningTab}
  DOMready-TabLoads
  Click Element    ${Loc_Home-BusinessPlanning-BusinessCategoriesTab}
  DOMready-TabLoads
  Click Element From List  //a[text()='${MaterialBusinessCategory2}']  -1
  DOMready-TabLoads
  Mouse Over  ${Loc_BusinessCategorieNode-BusinessMarket_ActionsLink}	
  #Wait Until Element Is Visible  ${Loc_BusinessCategorieNode-BusinessMarket_Actions_BusinessMarketLink}
  Click Element  ${Loc_BusinessCategorieNode-BusinessMarket_Actions_BusinessMarketLink}
  Wait Until Page Contains Element  ${BusinessMarketDialog}
  DOMreadyByWaitTime  4
  Click Element From List  ${NewBusinessMarketDialog_CurrencyDDL}  -1
  DOMreadyByWaitTime  2
  Select value from the DDL  ${Currency2}
  DOMreadyByWaitTime  2
  Click Element  ${NewBusinessMarketDialog_NodeTextArea}
  Input Text  ${NewBusinessMarketDialog_NodeTextArea}  ${BusinessMarketNameMat1}
  Click Element  ${NewBusinessMarketDialog_CodeTextArea}
  Input Text  ${NewBusinessMarketDialog_CodeTextArea}  ${BusinessMarketCode1}
  DOMreadyByWaitTime  2
  #Click Element From List  ${NewBusinessMarketDialog_CurrencyDDL}  -1

  Click Element  ${Dialog_Save&NewButton}
  Wait Until Page Contains Element  //span[text()='${BMCreationMSG}']
  
  Wait Until Page Contains Element  ${BusinessMarketDialog}
  DOMreadyByWaitTime  4
  Click Element From List  ${NewBusinessMarketDialog_CurrencyDDL}  -1
  DOMreadyByWaitTime  2
  Select value from the DDL  ${Currency2}
  DOMreadyByWaitTime  2
  Click Element  ${NewBusinessMarketDialog_NodeTextArea}
  Input Text  ${NewBusinessMarketDialog_NodeTextArea}  ${BusinessMarketName2}
  Click Element  ${NewBusinessMarketDialog_CodeTextArea}
  Input Text  ${NewBusinessMarketDialog_CodeTextArea}  ${BusinessMarketCode2}
   DOMreadyByWaitTime  2
  #Click Element From List  ${NewBusinessMarketDialog_CurrencyDDL}  -1

  Click Element  ${Dialog_Save&NewButton}
  Wait Until Page Contains Element  //span[text()='${BMCreationMSG}']
  
  Wait Until Page Contains Element  ${BusinessMarketDialog}
  DOMreadyByWaitTime  4
  Click Element From List  ${NewBusinessMarketDialog_CurrencyDDL}  -1
  DOMreadyByWaitTime  2
  Select value from the DDL  ${Currency2}
  DOMreadyByWaitTime  2
  Click Element  ${NewBusinessMarketDialog_NodeTextArea}
  Input Text  ${NewBusinessMarketDialog_NodeTextArea}  ${BusinessMarketName3}
  Click Element  ${NewBusinessMarketDialog_CodeTextArea}
  Input Text  ${NewBusinessMarketDialog_CodeTextArea}  ${BusinessMarketCode2}
  DOMreadyByWaitTime  2
  #Click Element From List  ${NewBusinessMarketDialog_CurrencyDDL}  -1

  Click Element  ${Dialog_SaveButton}
  Wait Until Page Contains Element  //span[text()='${BMCreationMSG}']
  DOMreadyByWaitTime  5

*** Variables ***
${Doc_Documents}	  css=[data-csi-tab='DocumentContainer-Documents']
${Doc_AllDocuments}	  css=[data-csi-tab='Data-AllDocuments']
${Doc_AllContractualDocuments}	css=[data-csi-tab='Data-AllContractualDocuments']
${NewDocument_ActLink}  css=table[data-csi-automation=plugin-DocumentContainer-Documents-ToolbarNewActions]
${Doc_ContractualDocumentGroupsSwitchboard}	 css=[data-csi-tab='Data-ContractualDocumentGroupsSwitchboard']
${Doc_SupplierContractualDocuments}	 css=[data-csi-tab='Data-SupplierContractualDocuments']
${NewContractualDocument_ActLink}		css=table[data-csi-automation='plugin-Data-AllContractualDocuments-ToolbarNewActions']
${NewContractualDocumentGroup_ActLink}		css=table[data-csi-automation='plugin-Data-ContractualDocumentGroupsSwitchboard-ToolbarNewActions']
${NewContractualDocument}		css=[data-csi-act='NewContractualDocument']
${NewContractualDocumentGroup}		css=[data-csi-act='NewContractualDocumentGroup']
${NewFromTestRunTemplate}		css=[data-csi-act='NewFromTestRunTemplate']
${Popup_SourceTypeDDLArrow}		css=[data-csi-automation='field-ContractualDocument-Form-Subtype']>div.dijitArrowButton
${ContractualDocuments-root_ActLink}		css=[data-csi-automation='actions-ContractualDocumentGroup-ContractualDocuments-root']
${SelectContractualDocument} 	  css=[data-csi-act='NewFromContractualDocuments']
${Loc_CDOCpopup_BrowseButton}  css=[name='SourceFile']
${Loc_CDOCpopup_DescriptionAttr}  css=[name='Description']
${Loc_CDOCpopup_NodenameAttr}  css=[name='Node Name']
${Table_ActionsCol_SpanDots}   //*[@class='browse' and text()='Web']//following::td[16]/div/span[1]/span
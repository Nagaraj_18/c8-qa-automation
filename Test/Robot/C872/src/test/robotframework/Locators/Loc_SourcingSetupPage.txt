
*** Variables ***
${Loc_Home_SourcingTab}  css=[data-csi-tab='LibSourcing-Sourcing'] 
${Loc_Home-Sourcing-SetupTab}  css=[data-csi-tab='LibSourcing-SourcingSetup']

#Operation Group
${Loc_Home-Sourcing-Setup-OperationGroupTab}  css=[data-csi-tab='LibSourcing-RoutingGroups']
${Loc_NewOperationGroup}  css=[data-csi-act='NewRoutingGroup']
${Loc_Table_InputText_OG}		css=[data-csi-automation='edit-LibSourcing-RoutingGroups-Node Name:']
${Loc_Sourcing-Setup-OperationGroupTab_CustomViewTextArea}  css=[data-csi-automation='plugin-LibSourcing-RoutingGroups-views'] div.dijitInputField>input[role='textbox']


#Capability 
${Loc_Home-Sourcing-Setup-Capability}	css=[data-csi-tab='LibSourcing-SourcingCapabilities']
${Loc_NewCapability_Link}	//td[@class='dijitReset dijitMenuItemLabel' and (text()='New Capability...')]
${Loc_TextAreaCapability}  css=[data-csi-automation='edit-LibSourcing-SourcingCapabilities-Node Name:']

#Country
${Loc_Home-Sourcing-Setup-CountryTab}	css=[data-csi-tab='LibSourcing-Countries']
${Loc_TextAreaCountry}  //div/textarea[@data-csi-automation='edit-LibSourcing-Countries-Node Name:']
${Loc_Table_InputTextCountry}  	css=[data-csi-automation='edit-LibSourcing-Countries-Node Name:']
${Loc_Table_InputText_COR}	css=[data-csi-automation='edit-LibSourcing-Countries-Region:']
${Loc_Table_InputText_COC}	css=[data-csi-automation='edit-LibSourcing-Countries-Code:']
${Loc_ButtonYesOnDialog}  xpath=//span[@class='dijitReset dijitInline dijitButtonText' and  text()='Yes']
${Loc_CountryNode-Province/StateTab_ActionsLink}  css=[data-csi-automation='plugin-Country-ProvinceState-ToolbarNewActions']
${Loc_CountryNode-Province/StateTab_Actions_NewProvinceLink}  css=tr[data-csi-act='NewProvinceState'] td.dijitMenuItemLabel
${Loc_CountryNode-CountryTab}  css=[data-csi-tab='Country-Country']
${Loc_CountryNode-Province/StateTab}  css=[data-csi-tab='Country-ProvinceState']
${Loc_Province/StateNodeNameTextArea}  css=[data-csi-automation='edit-Country-ProvinceState-Node Name:']
${Loc_CountryNode-Province/StateTab_CustomViewTextArea}  css=[data-csi-automation='plugin-Country-ProvinceState-CustomViewSelect'] div.dijitInputField>input[role='textbox']
${Loc_Sourcing-Setup-CountriesTab_CustomViewTextArea}  css=[data-csi-automation='plugin-LibSourcing-Countries-CustomViewSelect'] div.dijitInputField>input[role='textbox']
	

#HTS Code
${Loc_Home-Sourcing-Setup-HTSCodeTab}	css=[data-csi-tab='SourcingViews-DutyRates']
${Loc_TextAreaHTSCode}   css=[data-csi-automation='edit-SourcingViews-DutyRates-Node Name:']
${Loc_ActionsSourcingHTSCodeTab}  css=[data-csi-automation='plugin-SourcingViews-DutyRates-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
${Loc_SourcingSetupNewHTSCodeLink}  css=[data-csi-act='NewDutyRate'] .dijitMenuItemLabel
${Loc_TextAreaDuty%}	css=[data-csi-automation='edit-SourcingViews-DutyRates-DutyPct:'] .dijitInputInner
${Loc_FromCountry_AUT_HTS-01_Text}	xpath=//td[@class='attrString iconEditable firstColumn'and text()='AUT_HTS-01']/following-sibling::td[4]
${Loc_FromCountry_AUT_HTS-02_Text}	xpath=//td[@class='attrString iconEditable firstColumn'and text()='AUT_HTS-02']/following-sibling::td[4]
${Loc_ToCountry_AUT_HTS-01_Text}	xpath=//td[@class='attrString iconEditable firstColumn'and text()='AUT_HTS-01']/following-sibling::td[5]
${Loc_ToCountry_AUT_HTS-02_Text}	xpath=//td[@class='attrString iconEditable firstColumn'and text()='AUT_HTS-02']/following-sibling::td[5]

#Shipping Port 
${Loc_Home-Sourcing-Setup-Shipping Port}	css=[data-csi-tab='LibSourcing-ShippingPorts']
${Action_ShippingPort}  css=[data-csi-automation='plugin-LibSourcing-ShippingPorts-ToolbarNewActions']
${Loc_NewShippingPort_Link}		css=[data-csi-automation='plugin-LibSourcing-ShippingPorts-ToolbarNewActions']
${Loc_TextAreaShippingPort}   css=[data-csi-automation='edit-LibSourcing-ShippingPorts-Node Name:']
${LocShippingPortNode-Properties-Contacts_ActionsLink}  css=[data-csi-automation='plugin-SourcingItem-Contacts-ToolbarNewActions']
${LocShippingPortNode-Properties-Contacts_Actions_NewContactLink}   css=tr[data-csi-act='NewContact'] td.dijitMenuItemLabel
${Loc_ShippingPortNode-DocumentsAndCommentsTab}  css=[data-csi-tab='DocumentContainer-DocumentsAndComments']
${Loc_ShippingPortNode-DocumentsAndComments-Documents_ActionLink}  css=[data-csi-automation='plugin-DocumentContainer-Documents-ToolbarNewActions']
${Loc_ShippingPortNode-DocumentsAndComments-Documents_Action_NewDocumentLink}  css=[data-csi-act='NewDocument'] td.dijitMenuItemLabel
${Loc_ShippingPortNode-AuditTab}  css=[data-csi-tab='Auditable-Audit']
${Loc_ShippingPortNode-CapabilityTab}  css=[data-csi-tab='SourcingItem-Capabilities']
${Loc_ShippingPortNode-Capability_ActionsLink}  css=[data-csi-automation='plugin-SourcingItem-Capabilities-ToolbarNewActions']  div.csi-toolbar-btn-icon-ToolbarNewActions
${Loc_ShippingPortNode-Capability_Actions_NewFromCapabilityLink}  css=[data-csi-act='AggregateFromCapabilities'] td.dijitMenuItemLabel

# Shiping Container 
${Loc_Home-Sourcing-Setup-Shipping Container} 	css=[data-csi-tab='SourcingViews-ShippingContainers']
${Action_ShippingContainer}  css=[data-csi-automation='plugin-SourcingViews-ShippingContainers-ToolbarNewActions']
${Loc_NewShippingContainer}  css=[data-csi-act='NewShippingContainer']
${Loc_TextAreaShippingContainer}  css=[data-csi-automation='edit-SourcingViews-ShippingContainers-Node Name:']
${Loc_TextFieldMaxVolumeField}  css=[data-csi-automation='edit-SourcingViews-ShippingContainers-MaxVolumeCBM:'] .dijitInputInner
${Loc_TextFieldMaxWeightField}  css=[data-csi-automation='edit-SourcingViews-ShippingContainers-MaxWeightLbs:'] .dijitInputInner
${Loc_TextFieldPackingEfficiencyField}  css=[data-csi-automation='edit-SourcingViews-ShippingContainers-PackingEfficiency:'] .dijitInputInner

#Shipping Rate

${Loc_Home-Sourcing-Setup-Shipping Rate} 	css=[data-csi-tab='SourcingViews-ShippingRates']
${Action_ShippingRate}  css=[data-csi-automation='plugin-SourcingViews-ShippingRates-root']
${Loc_NewShippingRate_link}  css=[data-csi-act='NewShippingRate']
${Loc_TextBoxShippingName}  css=[data-csi-automation='field-ShippingRate-Form-Node Name'] .dijitInputInner
${Loc_ShipmentType_DDL}  css=[data-csi-automation='field-ShippingRate-Form-ShipmentType']>div>input.dijitArrowButtonInner
#${Loc_ShipmentType_DDL}  xpath=//div[@data-csi-automation='field-ShippingRate-Form-ShipmentType']//input
${Loc_FrightRate_text}	css=[data-csi-automation='field-ShippingRate-Form-Rate'] .dijitInputInner

#Sub Routing

${AddCapabilityDialog_Views}  css=[data-csi-automation='plugin-LibSourcing-SourcingCapabilities-views']
${LocRouting_Tab}  //span[text()='Sub Routings']
${Loc_Home-Sourcing-Setup-Sub Routings}  css=[data-csi-tab='ApparelViews-SubRoutings'] 
${Loc_ActionsSubRoutingTab}   css=[data-csi-automation='plugin-ApparelViews-SubRoutings-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
${Loc_NewSubRoutingLink}  css=[data-csi-act='NewSubRouting'] .dijitMenuItemLabel
${Loc_TextFieldSubRoutingName}  css=[data-csi-automation='field-SubRouting-Form-Node Name'] .dijitInputInner
${Loc_ActionsSubRoutingTDSTab}   css=[data-csi-automation='plugin-SubRoutingRevision-Items-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
${Loc_AddCapabilityLink}  css=[data-csi-act='NewFromCapability'] .dijitMenuItemLabel
${Loc_AddFromSubRoutingLink}  css=[data-csi-act='NewFromSubRouting'] .dijitMenuItemLabel
${Loc_AddSubRoutingLink}  css=[data-csi-act='NewFromSubRouting'] .dijitMenuItemLabel
${Loc_AddCopiedSubRoutingLink}  css=[data-csi-act='NewFromCopiedSubRouting'] .dijitMenuItemLabel
${Loc_NewFromSubRoutingLink}  css=[data-csi-act='NewFromNewSubRouting'] .dijitMenuItemLabel
${Loc_NewDividerLink}  css=[data-csi-act='NewDSLineDivider'] .dijitMenuItemLabel
${Loc_NewSpecialLink}  css=[data-csi-act='NewSpecial'] .dijitMenuItemLabel
${Loc_TextFieldNewSubRoutingName}  css=[data-csi-automation='field-SubRouting-Form-Node Name'] .dijitInputInner
${Loc_TextAreaDivider}  css=[data-csi-automation='edit-SubRoutingRevision-Items-__TextSpan__:'] 
${Loc_SubRoutingTDSTab}  css=[data-csi-tab='SubRoutingRevision-TDS']
${Loc_TextAreaSpecial}  css=[data-csi-automation='edit-SubRoutingRevision-Items-Node Name:Child:Actual'] 

#templates 
${Loc_Sourcing-Setup-Template}  css=[data-csi-tab='SiteLibSupplierItem-SupplierItemSetup']
${Action_RoutingTemplate}  css=[data-csi-automation='plugin-SiteLibSupplierItem-RoutingTemplates-root']
${Loc_NewRouting_link}  css=[data-csi-act='NewRouting']
${Action_NewOperation}  css=[data-csi-automation='plugin-RoutingRevision-Items-root']
${Loc_AddCapability_link}  css=[data-csi-act='NewFromCapability']
${Loc_TextAreaRoutingTemplate}  css=[data-csi-automation='edit-SiteLibSupplierItem-RoutingTemplates-Node Name:']
${Loc_AddSubRouting_link}  css=[data-csi-act='NewFromSubRouting']
${Loc_AddCopiedSubRouting_link}  css=[data-csi-act='NewFromCopiedSubRouting']
${Loc_NewDivider_link}  css=[data-csi-act='NewDSLineDivider']
${Loc_TabRoutingTDS}  css=[data-csi-tab='RoutingRevision-TDS']
${Loc_TabRoutingProperties}  css=[data-csi-tab='RoutingRevision-Properties']
${Loc_TabRoutingItems}  css=[data-csi-tab='RoutingRevision-Items']
${Loc_TabRoutingWhereUsed}  css=[data-csi-tab='RoutingRevision-WhereUsed']


${Loc_ActionsSuplierQuoteTemplates}   css=[data-csi-automation='plugin-SiteLibSupplierItem-SupplierItemTemplates-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
${Loc_NewSuplierQuoteTemplateLink}  css=[data-csi-act='NewSupplierItem'] .dijitMenuItemLabel
${Loc_TextFieldSupplierQuote}  css=[data-csi-automation='field-SupplierItem-SupplierQuoteTemplateForm-Node Name'] .dijitInputInner
${Loc_TabCostScenario}  css=[data-csi-tab='SupplierItemRevision-CostScenario']
${Loc_TabSupplierQuote}  css=[data-csi-tab='SupplierItemRevision-SupplierItem']
${Loc_AddHTSCodesLink}  //table[@data-csi-automation='plugin-SupplierItemRevision-DutyPerUsage-ToolbarNewActions']
${Loc_TextAreaFOB%}  css=[data-csi-automation='edit-SupplierItemRevision-DutyPerUsage-Percentage:'] .dijitInputInner
${Loc_TextAreaDutySupplierQuote}  css=[data-csi-automation='edit-SupplierItemRevision-DutyPerUsage-DutyPct:'] .dijitInputInner

${Loc_TextFieldSupplierRequestName}  css=[data-csi-automation='field-SupplierRequestTemplate-Form-Node Name'] .dijitInputInner
${Loc_ActionsSuplierRequestTemplates}   css=[data-csi-automation='plugin-SiteLibSupplierItem-SupplierRequestTemplates-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
${Loc_NewSuplierRequestTemplateLink}  css=[data-csi-act='NewSupplierRequestTemplate'] .dijitMenuItemLabel
${Loc_ButtonNext}  css=[data-csi-act='NextPage']
${Loc_ButtonFinish}  css=[data-csi-act='Finish']



*** Variables ***
${Loc_PopUpNameTextBox}  css=input[name='Node Name']
# The following replaces the prior xpath general locator, but is deprecated. Instead, scripts should use a locator or function that resolves to a locator that is in a specific dialog, and a specific ddl.
${1srDDLDownArrowButton}  css=.csiFormView [value='? ']
${Loc_DefaultTextBox}  css=[id='Node Name']
# The following replaces the prior xpath general locator, but is deprecated. Instead, the script should use a locator or function that resolves to a locator that is in a specific dialog, and a specific checkbox.
${CheckBox}  css=.csiFormViewCheckbox [type='checkbox']
${Loc_DescriptionTextBox}  css=input[name='Description']
# I was not able to find an example of this in the product. Maybe someone on the Kripya team can convert this to css.
${Loc_CommentsTextBox}  //*[@class='csiDialog dijitLayoutContainer dijitDialog dijitDialogError dijitError']//input[@class='dijitReset dijitInputInner']  
${Loc_ColorName}  css=[data-csi-automation='field-ColorSpecification-Form-Node Name'] 
${Loc_AttributeNodeName}  css=[data-csi-automation='field-ConfigurableAttribute-Form-Node Name']  
${Loc_CalendarPopUpCurDateField}  css=tbody.dijitCalendarBodyContainer tr.dijitCalendarWeekTemplate>td.dijitCalendarCurrentDate

#dijitCalendarEnabledDate dijitCalendarCurrentDate dijitCalendarCurrentMonth dijitCalendarDateTemplate
${Loc_CalendarPopUpCurDateFieldSpan}  css=tbody.dijitCalendarBodyContainer tr.dijitCalendarWeekTemplate>td.dijitCalendarCurrentDate>span
${Loc_CalendarPopUpNxtYrField}  css=div.dijitCalendarYearLabel>span.dijitCalendarNextYear

 
  #Wait Until Element Is Visible   //*[@class='dijitCalendarDateLabel' and text()='19']
  #Click Element  //*[@class='dijitCalendarDateLabel' and text()='19']
  










*** Variables ***
${Loc_ActivityBO}  css=[data-csi-url='centric://REFLECTION/BusinessObject/Activity'] .browse
${Loc_NewCustomAttribute}  css=[data-csi-act='NewCustomAttribute']
# There are several specific controls depending on this repeated general locator (same in xpath and CSS)
# This approach is deprecated in favor of giving the context.
${Loc_NewCustomAttributeTextBox}  css=input[id='Node Name']
${Loc_SaveButton}  css=[data-csi-act='Save']
${Loc_AttributeGroupTextBox}    css=[data-csi-automation='edit-BusinessObject-AttributeGroups-Node Name:']
${Loc_AttributeGroupTab}    css=[data-csi-tab='BusinessObject-AttributeGroups']
${Loc_AttributeTab}    css=[data-csi-tab='BusinessObject-Attributes']

*** Variables ***
 
${HomeStylesTab}  css=[data-csi-tab='ApparelViews-Style']  
${Loc_SeasonTab}  css=[data-csi-tab='ApparelViews-Seasons']
${Loc_NewStyle}  css=[data-csi-act='CreateStyle']
${Loc_NewBrand}  css=[data-csi-act='NewCategory1']
${Loc_NewSeason}  css=[data-csi-act='NewSeason']
${Loc_NewCollection}  css=[data-csi-act='NewCollection']
${Loc_NewDepartment}  css=[data-csi-act='NewCategory2']
${Loc_ActionsStyleSeasonsTab}   css=[data-csi-automation='plugin-ApparelViews-Seasons-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
${Loc_NewSeasonLink}  css=[data-csi-act='NewSeason'] .dijitMenuItemLabel
${Loc_ActionsSeasons-BrandsTab}   css=[data-csi-automation='plugin-Season-Category1s-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
${Loc_NewBrandLink}  css=[data-csi-act='NewCategory1'] .dijitMenuItemLabel
${Loc_ActionsSeasons-DepartmentTab}   css=[data-csi-automation='plugin-Category1-Category2s-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
${Loc_NewDepartmentLink}  css=[data-csi-act='NewCategory2'] .dijitMenuItemLabel
${Loc_ActionsSeasons-CollectionsTab}   css=[data-csi-automation='plugin-Category2-Collections-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
${Loc_NewCollectionsLink}  css=[data-csi-act='NewCollection'] .dijitMenuItemLabel
${Loc_ActionsSeasons-StylesTab}   css=[data-csi-automation='plugin-Collection-Styles-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
${Loc_NewStylesLink}  css=[data-csi-act='NewStyle'] .dijitMenuItemLabel
${Loc_TabStyleProductColors}  css=[data-csi-tab='Style-ProductColors']
${Loc_StyleNode-StyleTab}  css=[data-csi-tab='Style-Base']

${Loc_ActionsStylesColorwaysTab}   css=[data-csi-automation='plugin-Style-ProductColors-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
${Loc_NewColorwaysLink}  css=[data-csi-act='NewColorway'] .dijitMenuItemLabel
${Loc_NewFromColorSpecificationLink}  css=[data-csi-act='AggregateFromProductColors'] .dijitMenuItemLabel
${Loc_TabStyleProductSpec}  css=[data-csi-tab='Style-ProductSpec']
${Loc_TabStyleBOM}  css=[data-csi-tab='Style-BOMs'] 
${Loc_ActionsSpecificationBOMTab}   css=[data-csi-automation='plugin-Style-BOMs-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
${Loc_NewStyleBOMFromTemplateLink}  css=[data-csi-act='NewFromBOMTemplate'] .dijitMenuItemLabel
${Loc_StyleRouting_Tab}  css=[data-csi-tab='Style-Routing']
${Loc_ActionsSpecficationRoutingTab}  css=[data-csi-automation='plugin-Style-Routing-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
${Loc_NewRoutingFromTemplateLink}  css=[data-csi-act='NewFromRoutingTemplates'] .dijitMenuItemLabel
${Loc_TabStyleSpecDataSheet}  css=[data-csi-tab='Style-SpecificationDataSheets']


${Loc_ActionsSpecificationDataSheetTab}  css=[data-csi-automation='plugin-Style-SpecDataSheets-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
${Loc_NewSpecsDataSheetFromTemplateLink}  css=[data-csi-act='NewSpecDataSheetFromTemplate'] .dijitMenuItemLabel
${Loc_TabProductSummary}  css=[data-csi-tab='Product-ProductSpecSummary']
${Loc_ActionsSpecSummaryTab}  css=[data-csi-automation='plugin-Product-DataPackages-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
${Loc_NewDataPackageFromTemplateLink}  css=[data-csi-act='AggregateFromPackages'] .dijitMenuItemLabel

#
${Loc_SeasonNode-SeasonTab}  css=[data-csi-tab='Season-Base']
${Loc_SeasonNode-Season-HierarchyTab}  css=[data-csi-tab='Season-Hierarchy']
${Loc_SeasonHierarchy-CalendarTab}  css=[data-csi-tab='CalendarContainer-Calendar']
${Loc_SeasonHierarchy-Calendar_Actions}  css=table[data-csi-automation='plugin-CalendarContainer-Calendar-ToolbarNewActions']
${Loc_SeasonHierarchy-Calendar_Actions_NewFromMasterCalendarLink}  css=tr[data-csi-act='AggregateFromCalendar'] td.dijitMenuItemLabel
${Loc_BrandNode-BrandTab}  css=[data-csi-tab='Category1-Base']
${Loc_BrandNode-Brand-HierarchyTab}  css=[data-csi-tab='Category1-Hierarchy']
${Loc_DepartmentNode-DepartmentTab}  css=[data-csi-tab='Category2-Base']
${Loc_DepartmentNode-Department-HierarchyTab}  css=[data-csi-tab='Category2-Hierarchy']
${Loc_CollectionNode-CollectionTab}  css=[data-csi-tab='Collection-Base'] 
${Loc_CollectionNode-Collection-HierarchyTab}  css=[data-csi-tab='Collection-Hierarchy'] 

*** Settings ***
Test Setup            Setup MerchandisingData
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt
Resource          ../PageObjects/PO_SetupPage.txt
Resource          ../PageObjects/PO_SourcingPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_EnumerationPage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../Locators/Loc_SetupPage.txt    
Resource          ../Locators/Loc_MerchandiseType.txt
Resource          ../Locators/Loc_SpecificationSizeRangePage.txt
Resource          ../Locators/Loc_SpecificationSizesPage.txt

*** Variable ***
${Save_btn}  css=[data-csi-act='Save']
${PreviousSeason_btn}  css=[data-csi-automation='field-MerchPlan-NewSeasonForm-Previous'] .dijitArrowButton
${MerchStyle2}  AUT_MerchStyle2
${MerchStyle1}  AUT_MerchStyle1
${MerchProductType}  AUT-Product-01

*** Test Cases ***
Login To Application
    [Tags]  Merchandising
	SuiteSetUp.Open Browser To Login Page
	PO_LoginPage.Input UserName    NewUser_UserName
	PO_LoginPage.Input Password    NewUser_Password
	PO_LoginPage.Submit Credentials
	DOMready-TabLoads 

  	Click on Home Icon
    DOMready-TabLoads
    ${ScreenSizeISSmall}=  Run KeyWord And Return Status  Click On Right Arrow Button To View Hidden Tabs
    Log  ${ScreenSizeISSmall}
    DOMReadyByWaitTime  2
    Click Element  ${Loc_Home-CollectionManagementTab}
    DOMready-TabLoads

Navigate to Merch Plan
    [Tags]  Merchandising
    PO_SetupPage.Click on Site Search Field and Hit Search  Plan   ${MerchPlan}
    DOMreadyByWaitTime  4 
    PO_SetupPage.Click on Required Value From Site Search Result  1
    Wait.DOMready-TabLoads
    Click Element  ${Loc_MerchPlanNode-PlanTab}
    DOMready-TabLoads  
    DOMreadyByWaitTime    2

Click in Secondaries in Merch Plan
    [Tags]  Merchandising
   Click Element  ${Loc_MerchPlanNode-SecondaryTab}
    DOMready-TabLoads
Create custom view to show secondaries (Parent and Child) in Merch Plan
    [Tags]  Merchandising
     Create a View  plugin-MerchSeason-TargetSecondaries-CustomViewActions  plugin-MerchSeason-TargetSecondaries-CustomViewManage  Plan
Change the version and verify the display of secondaries in Merch Plan
    [Tags]  Merchandising
    Click Element  ${Loc_MerchPlanNode-PlanTab_VersionTypesToolbar_DDL}
    DOMreadyByWaitTime    2
    Select value from the DDL  ${MerchSeason1}
    DOMreadyByWaitTime    2
    Click Element  ${Loc_MerchPlanNode-SecondaryTab}
    DOMready-TabLoads
    ${Element} =  Required Element From List  //td[@data-csi-heading="Children::1"]/a[text()='${MerchSeason1}']  -1
    Element Should Be Visible  ${Element}

    
    Click Element  ${Loc_MerchPlanNode-PlanTab_VersionTypesToolbar_DDL}
    DOMreadyByWaitTime    2
    Select value from the DDL  ${MerchSeason2}
    DOMreadyByWaitTime    2
    Click Element  ${Loc_MerchPlanNode-SecondaryTab}
    DOMready-TabLoads
    ${Element} =  Required Element From List  //td[@data-csi-heading="Children::1"]/a[text()='${MerchSeason2}']  0
    Element Should Be Visible  ${Element}
  
    
Navigate to Folder-Region Level
    [Tags]  Merchandising
    PO_SetupPage.Click on Site Search Field and Hit Search  Folder   ${Folder1Name}
    DOMreadyByWaitTime  4 
    PO_SetupPage.Click on Required Value From Site Search Result  1
    Wait.DOMready-TabLoads
    Click Element  ${Loc_Folder1Node-PlanTab}
    DOMready-TabLoads  
    DOMreadyByWaitTime    2

Click in Secondaries in Folder-Region Level
    [Tags]  Merchandising
   Click Element  ${Loc_Folder2Node-SecondaryTab}
   DOMready-TabLoads
Create custom view to show secondaries (Parent and Child) in Folder-Region Level
    [Tags]  Merchandising
     Create a View  plugin-MerchFolderVersion-TargetSecondaries-CustomViewActions  plugin-MerchFolderVersion-TargetSecondaries-CustomViewManage  F1

Change the version and verify the display of secondaries in Folder-Region Level
    [Tags]  Merchandising
    Click Element  ${Folder1Node_SecondariesTab_VersionsDDL}
    DOMreadyByWaitTime    2
    Select value from the DDL  ${MerchSeason1}
    DOMreadyByWaitTime    2
    Click Element  ${Loc_Folder2Node-SecondaryTab}
    DOMready-TabLoads
    ${Element} =  Required Element From List  //td[@data-csi-heading="Children::1"]/a[text()='${MerchSeason1}']  -1
    Element Should Be Visible  ${Element}

    
    Click Element  ${Folder1Node_SecondariesTab_VersionsDDL}
    DOMreadyByWaitTime    2
    Select value from the DDL  ${MerchSeason2}
    DOMreadyByWaitTime    2
    Click Element  ${Loc_Folder2Node-SecondaryTab}
    DOMready-TabLoads
    ${Element} =  Required Element From List  //td[@data-csi-heading="Children::1"]/a[text()='${MerchSeason2}']  -1
    Element Should Be Visible  ${Element}

    

Navigate to Folder-Dpt Level
    [Tags]  Merchandising
    PO_SetupPage.Click on Site Search Field and Hit Search  Folder   ${Folder2Name}
    DOMreadyByWaitTime  4 
    PO_SetupPage.Click on Required Value From Site Search Result  1
    Wait.DOMready-TabLoads
    Click Element  ${Loc_Folder1Node-PlanTab}
    DOMready-TabLoads  
    DOMreadyByWaitTime    2

Click in Secondaries in Folder-Dpt Level
    [Tags]  Merchandising
   Click Element  ${Loc_Folder2Node-SecondaryTab}
   DOMready-TabLoads
Create custom view to show secondaries (Parent and Child) in Folder-Dpt Level
    [Tags]  Merchandising
     Create a View  plugin-MerchFolderVersion-TargetSecondaries-CustomViewActions  plugin-MerchFolderVersion-TargetSecondaries-CustomViewManage  F2

Change the version and verify the display of secondaries in Folder-Dpt Level
    [Tags]  Merchandising
    Click Element From List  ${Folder1Node_SecondariesTab_VersionsDDL}  -1
    DOMreadyByWaitTime    2
    Select value from the DDL  ${MerchSeason1}
    DOMreadyByWaitTime    2
    Click Element  ${Loc_Folder2Node-SecondaryTab}
    DOMready-TabLoads
    ${Element} =  Required Element From List  //td[@data-csi-heading="Children::1"]/a[text()='${MerchSeason1}']  -1
    Element Should Be Visible  ${Element}

    
    Click Element From List  ${Folder1Node_SecondariesTab_VersionsDDL}  -1
    DOMreadyByWaitTime    2
    Select value from the DDL  ${MerchSeason2}
    DOMreadyByWaitTime    2
    Click Element  ${Loc_Folder2Node-SecondaryTab}
    DOMready-TabLoads
    ${Element} =  Required Element From List  //td[@data-csi-heading="Parents::1"]/a[text()='${MerchSeason2}']  -1
    Element Should Be Visible  ${Element}

Navigate to Folder-Gender Level
    [Tags]  Merchandising
    PO_SetupPage.Click on Site Search Field and Hit Search  Folder   ${Folder3Name}
    DOMreadyByWaitTime  4 
    PO_SetupPage.Click on Required Value From Site Search Result  1
    Wait.DOMready-TabLoads
  
    DOMreadyByWaitTime    2

Click in Secondaries in Folder-Gender Level
    [Tags]  Merchandising
   Click Element  ${Loc_Folder2Node-SecondaryTab}
   DOMready-TabLoads
Create custom view to show secondaries (Parent and Child) in Folder-Gender Level
    [Tags]  Merchandising
     Create a View  plugin-MerchFolderVersion-TargetSecondaries-CustomViewActions  plugin-MerchFolderVersion-TargetSecondaries-CustomViewManage  F3

Change the version and verify the display of secondaries in Folder-Gender Level
    [Tags]  Merchandising
    Click Element From List  ${Folder1Node_SecondariesTab_VersionsDDL}  -1
    DOMreadyByWaitTime    2
    Select value from the DDL  ${MerchSeason1}
    DOMreadyByWaitTime    2
    Click Element  ${Loc_Folder2Node-SecondaryTab}
    DOMready-TabLoads
    ${Element} =  Required Element From List  //td[@data-csi-heading="Children::1"]/a[text()='${MerchSeason1}']  -1
    Element Should Be Visible  ${Element}

    
    Click Element From List  ${Folder1Node_SecondariesTab_VersionsDDL}  -1
    DOMreadyByWaitTime    2
    Select value from the DDL  ${MerchSeason2}
    DOMreadyByWaitTime    2
    Click Element  ${Loc_Folder2Node-SecondaryTab}
    DOMready-TabLoads
    ${Element} =  Required Element From List  //td[@data-csi-heading="Parents::1"]/a[text()='${MerchSeason2}']  -1
    Element Should Be Visible  ${Element}

  


Navigate to Folder-Collection Level
    [Tags]  Merchandising
    PO_SetupPage.Click on Site Search Field and Hit Search  Merch Collection   ${MerchCollection1Name}
    DOMreadyByWaitTime  4 
    PO_SetupPage.Click on Required Value From Site Search Result  1
    Wait.DOMready-TabLoads
    Click Element  ${Loc_MerchCollectionNode-PlanTab}
    DOMready-TabLoads  
    DOMreadyByWaitTime    2
Click in Secondaries in Folder-Collection Level
    [Tags]  Merchandising
   Click Element  ${Loc_CollectionsNode-SecondaryTab}
   DOMready-TabLoads
Create custom view to show secondaries (Parent and Child) in Folder-Collection Level
    [Tags]  Merchandising
     Create a View  plugin-MerchCollectionVersion-TargetSecondaries-CustomViewActions  plugin-MerchCollectionVersion-TargetSecondaries-CustomViewManage  C

Change the version and verify the display of secondaries in Folder-Collection Level
    [Tags]  Merchandising
    Click Element From List  ${CollectionNode_SecondariesTab_VersionsTypeDDL}  -1
    DOMreadyByWaitTime    2
    Select value from the DDL  ${MerchSeason1}
    DOMreadyByWaitTime    2
    Click Element  ${Loc_CollectionsNode-SecondaryTab}
    DOMready-TabLoads
    Element Should Be Visible  //td[@data-csi-heading="Children::1"]/a[text()='${MerchSeason1}']
    
    Click Element From List  ${CollectionNode_SecondariesTab_VersionsTypeDDL}  -1
    DOMreadyByWaitTime    2
    Select value from the DDL  ${MerchSeason2}
    DOMreadyByWaitTime    2
    Click Element  ${Loc_CollectionsNode-SecondaryTab}
    DOMready-TabLoads
    Element Should Be Visible  //td[@data-csi-heading="Parents::1"]/a[text()='${MerchSeason2}']
  

Navigate to Folder-Product Level
    [Tags]  Merchandising
    PO_SetupPage.Click on Site Search Field and Hit Search  Product   ${MerchProduct1}
    DOMreadyByWaitTime  4 
    PO_SetupPage.Click on Required Value From Site Search Result  1
    Wait.DOMready-TabLoads
    Click Element  ${Loc_MerchProductNode-PlanTab}
    DOMready-TabLoads  
    DOMreadyByWaitTime    2
Click in Secondaries in Folder-Product Level
    [Tags]  Merchandising
    Click Element  ${Loc_ProductNode-SecondaryTab}
    DOMready-TabLoads
Create custom view to show secondaries (Parent and Child) in Folder-Product Level
    [Tags]  Merchandising
     Create a View  plugin-MerchProductVersion-TargetSecondaries-CustomViewActions  plugin-MerchProductVersion-TargetSecondaries-CustomViewManage  P

Change the version and verify the display of secondaries in Folder-Product Level
    [Tags]  Merchandising
    Click Element From List  ${ProductNode_SecondariesTab_VersionsTypeDDL}  -1
    DOMreadyByWaitTime    2
    Select value from the DDL  ${MerchSeason1}
    DOMreadyByWaitTime    2
    Click Element  ${Loc_ProductNode-SecondaryTab}
    DOMready-TabLoads
    Element Should Be Visible  //td[@data-csi-heading="Children::1"]/a[text()='${MerchSeason1}']
    


Navigate to Folder-Option Level
    [Tags]  Merchandising
    PO_SetupPage.Click on Site Search Field and Hit Search  Option   ${Var_Option1Name}
    DOMreadyByWaitTime  4 
    PO_SetupPage.Click on Required Value From Site Search Result  1
    Wait.DOMready-TabLoads
    DOMreadyByWaitTime    2
Click in Secondaries in Folder-Option Level
    [Tags]  Merchandising
   Click Element  ${Loc_OptionsNode-SecondaryTab}
   DOMready-TabLoads
Create custom view to show secondaries (Parent and Child) in Folder-Option Level
    [Tags]  Merchandising
     Create a View  plugin-MerchOptionVersion-TargetSecondaries-CustomViewActions  plugin-MerchOptionVersion-TargetSecondaries-CustomViewManage  Op
     DOMreadyByWaitTime    2
Change the version and verify the display of secondaries in Folder-Option Level
    [Tags]  Merchandising
    Click Element From List  ${OptionsNode_SecondariesTab_VersionsTypeDDL}  -1
    DOMreadyByWaitTime    2
    Select value from the DDL  ${MerchSeason1}
    DOMreadyByWaitTime    2
    Click Element  ${Loc_OptionsNode-SecondaryTab}
    DOMready-TabLoads
    ${Element} =  Required Element From List  //td[@data-csi-heading="Parents::1"]/a[text()='${MerchSeason1}']  -1
    Element Should Be Visible  ${Element}


    #[Teardown]    Close Browser   
     
***Keywords***
Setup MerchandisingData
  ${This_DataProvider} =  Data_Provider.DataProvider  Merchandising
  Set Test Variable  ${This_DataProvider}
  ${StyleType} =  Data_Provider.GetDataProviderColumnValue  Data_StyleTypeName
  ${MerchSeason_Collection} =  Data_Provider.GetDataProviderColumnValue  Data_SeasonName
  ${MerchSeason} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MerchSeason_Collection}  1
  ${MerchBrand_Collection} =  Data_Provider.GetDataProviderColumnValue   Data_BrandName
  ${MerchBrand} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MerchBrand_Collection}  1
  ${MerchCol_Collection} =  Data_Provider.GetDataProviderColumnValue   Data_CollectionName
  ${MerchCollection} =   Data_Provider.DataProviderSplitterForMultipleValues  ${MerchCol_Collection}  1
  ${MerchDepartment_Collection} =  Data_Provider.GetDataProviderColumnValue   Data_DepartmentName
  ${MerchDepartment} =   Data_Provider.DataProviderSplitterForMultipleValues  ${MerchDepartment_Collection}  1
  Set Test Variable  ${StyleType}
  Set Test Variable  ${MerchSeason}
  Set Test Variable  ${MerchBrand}
  Set Test Variable  ${MerchDepartment}
  Set Test Variable  ${MerchCollection}
  #Set Test Variable  ${MerchTheme}
  

        
Click On Right Arrow Button To View Hidden Tabs    
   Wait Until Element Is Visible  ${Loc_Home_ScrollRightToViewOtherTabsButton}
   Click Element  ${Loc_Home_ScrollRightToViewOtherTabsButton}
   Wait Until Element Is Not Visible    ${Loc_Home_ScrollRightToViewOtherTabsButton}    

Create A View
    
  [Arguments]  ${Actions}  ${ManageActions}  ${i} 
  Click Element From List  css=[data-csi-automation='${Actions}'] .dijitButtonText  -1
  Wait.DOMreadyByWaitTime  2
  Click Element From List  css=[data-csi-automation='${ManageActions}'] .dijitMenuItemLabel  -1
  Wait.DOMreadyByWaitTime  5
  #Wait Until Page Contains Element  xpath=//span[text()='Add >']
  Wait.DOMreadyByWaitTime  2   
# Copy the Default CV and create a new CV
  PO_SetupPage.Click on Default Custom View
  PO_SetupPage.Click on Copy button
  Clear Element Text  ${Loc_SecurityRoleName}
  Selenium2Library.Input Text  ${Loc_SecurityRoleName}  ChildView${i}
  Wait.DOMreadyByWaitTime  2
  #Click Element        //div[@class='csiPreferenceDisplay']//div[@class='dijitReset dijitRight dijitButtonNode dijitArrowButton dijitDownArrowButton dijitArrowButtonContainer']        
  #Click Element          //div[@class='dijitReset dijitMenuItem' and text()='Material']     
#Add the  attribute to the view
  Click Element         css=.csiPreferenceSelect [value='Parents::0']
  Wait.DOMreadyByWaitTime  2
  Click Element         //span[text()='Add >']    
#Add the attribute to the view
  Wait.DOMreadyByWaitTime  2
  Click Element         css=.csiPreferenceSelect [value='Children::0']
  Wait.DOMreadyByWaitTime  2
  Click Element         //span[text()='Add >']
  Wait.DOMreadyByWaitTime  2
  Click Element From List  Xpath=//span[text()='Save']  -1
  DOMreadyByWaitTime  4

  	
  	
  	
  	
  	
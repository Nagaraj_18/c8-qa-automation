*** Settings ***
Test Setup            Setup MerchandisingData
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt
Resource          ../PageObjects/PO_SetupPage.txt
Resource          ../PageObjects/PO_SourcingPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_EnumerationPage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../Locators/Loc_SetupPage.txt    
Resource          ../Locators/Loc_MerchandiseType.txt
Resource          ../Locators/Loc_SpecificationSizeRangePage.txt
Resource          ../Locators/Loc_SpecificationSizesPage.txt

*** Variable ***
${Save_btn}  css=[data-csi-act='Save']
${PreviousSeason_btn}  css=[data-csi-automation='field-MerchPlan-NewSeasonForm-Previous'] .dijitArrowButton
${MerchStyle2}  AUT_MerchStyle2
${MerchStyle1}  AUT_MerchStyle1
${MerchProductType}  AUT-Product-01

*** Test Cases ***
Create Merchandising Plan
    [Tags]  Merchandising
	SuiteSetUp.Open Browser To Login Page
	PO_LoginPage.Input UserName    NewUser_UserName
	PO_LoginPage.Input Password    NewUser_Password
	PO_LoginPage.Submit Credentials
	DOMready-TabLoads 

  	Click on Home Icon
    DOMready-TabLoads
    ${ScreenSizeISSmall}=  Run KeyWord And Return Status  Click On Right Arrow Button To View Hidden Tabs
    Log  ${ScreenSizeISSmall}
    DOMReadyByWaitTime  2
    Click Element  ${Loc_Home-CollectionManagementTab}
    DOMready-TabLoads

Create Merch Plan 
    [Tags]  Merchandising
    Click Element  ${Loc_Home-MerchandisingTab}
    Wait For Release Info
    Wait Until Element Is Visible  ${Loc_Home-MerchandisingTab_ActionsLink}
    Click Element  ${Loc_Home-MerchandisingTab_ActionsLink}
    DOMready-PopUpAppears
    Verify the Element  ${Merchandising_NewPlanDialog}    New PlanDialog is Displayed
    Input Text  css=div[data-csi-automation='field-MerchPlanConfig-NewPlanForm-Name'] .dijitInputInner  ${MerchPlan}
    Click Element  ${NewPlanDialog_SubtypeDDL}
    Select value from the DDL  ${NewPlanDialog_SubtypeValue}
    Input Text  css=div[data-csi-automation='field-MerchPlanConfig-NewPlanForm-Season'] .dijitInputInner  ${MerchSeason1}
    Click Element  ${Save_btn}
    DOMready-PopUpCloses
    Wait Until Element IS Visible   ${Loc_Home-MerchandisingTab_ActionsLink}

Navigate to Merchandising Plan created
    [Tags]  Merchandising
    Click on the Node in table  ${MerchPlan}
    DOMready-TabLoads
    Wait Until Element Is Visible  ${Loc_Release}
 

Setup Secondaries for the plan with both the types
    [Tags]  Merchandising
    Click Element  ${Loc_MerchPlanNode-SecondarySetupTab}
    DOMready-TabLoads
    Click Element From List   ${Loc_MerchPlanNode-SecondarySetup_SecondaryTypesToolbar_DDL}  -1
    DOMreadyByWaitTime    2 
    Select value from the DDL  ${SegmentType1}
    DOMreadyByWaitTime    2 
    Click Element  ${Loc_MerchPlanNode-SecondarySetupTab}
    DOMready-TabLoads
    :FOR  ${i}  IN RANGE  1  3
    \  Click Element From List  ${Loc_MerchPlanNode-SecondarySetup_Actions}  -1
    \  Wait Until Element Is Visible  //span[text()='Secondary Plan created successfully.']
    \  ${ActionCommentVisibility} =  Run Keyword And Return Status  Wait Until Element Is Visible  css=[data-csi-act='ActionComment']  timeout=5 seconds
    \  Log  ${ActionCommentVisibility}
    \  Input Text  //div/textarea  MonthlyValue-0${i}
    \  Click Element  ${Loc_MerchPlanNode-SecondarySetupTab}
	\  DOMready-TabLoads
      	\  Click Element  //td[text()='MonthlyValue-0${i}']//following::div[1]/input
    #\  Wait Until Element Is Enabled  ${Loc_PlanNode-SecondarySetupTab_CV_InputArea}
    # *Create a Secondary plan for the Seconday Type Segment
    Click Element From List   ${Loc_MerchPlanNode-SecondarySetup_SecondaryTypesToolbar_DDL}  -1
    DOMreadyByWaitTime    2 
    Select value from the DDL  ${SegmentType2}
    DOMreadyByWaitTime    2 
    Click Element  ${Loc_MerchPlanNode-SecondarySetupTab}
	DOMready-TabLoads
	:FOR  ${i}  IN RANGE  1  3
    \  Click Element From List  ${Loc_MerchPlanNode-SecondarySetup_Actions}  -1
    \  Wait Until Element Is Visible  //span[text()='Secondary Plan created successfully.']
    \  ${ActionCommentVisibility} =  Run Keyword And Return Status  Wait Until Element Is Visible  css=[data-csi-act='ActionComment']  timeout=5 seconds
    \  Log  ${ActionCommentVisibility}
    \  Input Text  //div/textarea  SegementValue-0${i}
    \  Click Element  ${Loc_MerchPlanNode-SecondarySetupTab}
	\  DOMready-TabLoads

Create New Season 
    [Tags]  Merchandising
    Click Element   ${Loc_MerchPlanNode-VersionsTab}
    DOMready-TabLoads
    Wait Until Element IS Visible   ${Loc_Release}
    #Element Should Be Visible  //td[@class='attrString firstColumn' and text()='${MerchSeason1}']
    Wait Until Element Is Visible  ${Loc_MerchPlanNode-VersionsTab_Actions_NewSeasonLink}
    Click Element  ${Loc_MerchPlanNode-VersionsTab_Actions_NewSeasonLink}
    DOMready-PopUpAppears
    Input Text  ${Loc_NewSeasonDialog_Name}  ${MerchSeason2}
    DOMreadyByWaitTime    2
    Click Element  ${PreviousSeason_btn}
    DOMreadyByWaitTime    2
    Select value From the DDL  ${MerchSeason1}
    Wait For Element And Click  ${Save_btn}
    DOMready-PopUpCloses
Mapping It to Real Season 
    [Tags]  Merchandising
    Click Element  //td[@class='attrString firstColumn' and text()='${MerchSeason2}']/following::td[2]
    DOMreadyByWaitTime    2
    Select value from the DDL  ${MerchSeason2}
    DOMreadyByWaitTime    2
    Click Element   ${Loc_MerchPlanNode-VersionsTab}
    DOMready-TabLoads
    
    Click Element  //td[@class='attrString firstColumn' and text()='${MerchSeason1}']/following::td[2]
    DOMreadyByWaitTime    2
    Select value from the DDL  ${MerchSeason1}
    DOMreadyByWaitTime    2
    Click Element   ${Loc_MerchPlanNode-VersionsTab}
    DOMready-TabLoads
Create two secondaries for the Plan using New from Secondary Plan for season 2016
    [Tags]  Merchandising
    Click Element  ${Loc_MerchPlanNode-SecondaryTab}
    DOMready-TabLoads
    DOMreadyByWaitTime    2
    Click Element  css=[data-csi-automation="plugin-MerchSeason-TargetSecondaries-refresh"]
    DOMready-TabLoads

    Click Element From List  ${Loc_MerchPlanNode-PlanTab_VersionTypesToolbar_DDL}  -1
    DOMreadyByWaitTime    2
    Element Should Be Visible  //div[@class='dijitReset dijitMenuItem' and text()='${MerchSeason2}']
    Select value from the DDL  ${MerchSeason2}
    Click Element  ${Loc_MerchPlanNode-SecondaryTab}
    DOMready-TabLoads
    Click Element From List  ${Loc_MerchPlanNode-PlanTab_SecondaryTypesToolbar_DDL}  -1
    DOMreadyByWaitTime    2
    ${Element1} =  Required Element From List  //div[@class='dijitReset dijitMenuItem' and text()='${SegmentType1}']  -1
    Element Should Be Visible  ${Element1}
    ${Element2} =  Required Element From List  //div[@class='dijitReset dijitMenuItem' and text()='${SegmentType2}']  -1
    Element Should Be Visible  ${Element2}
    Click Element  ${Element1}
    #Select value from the DDL  ${Element1}
    Click Element  ${Loc_MerchPlanNode-SecondaryTab}
    DOMready-TabLoads
    Click Element  ${Loc_NewFromSecondaryPlan}
    DOMready-PopUpAppears
    Click Element  //td[text()='MonthlyValue-01']
    Click Element  //td[text()='MonthlyValue-02']
    DOMreadyByWaitTime    2
    Wait For Element And Click  ${Save_btn}
    DOMready-PopUpCloses
Create two secondaries for the Plan using New from Secondary Plan for season 2015
    [Tags]  Merchandising
    Click Element  ${Loc_MerchPlanNode-SecondaryTab}
    DOMready-TabLoads
    DOMreadyByWaitTime    2
    Click Element  css=[data-csi-automation="plugin-MerchSeason-TargetSecondaries-refresh"]
    DOMready-TabLoads

    Click Element From List  ${Loc_MerchPlanNode-PlanTab_VersionTypesToolbar_DDL}  -1
    DOMreadyByWaitTime    2
    Element Should Be Visible  //div[@class='dijitReset dijitMenuItem' and text()='${MerchSeason1}']
    Select value from the DDL  ${MerchSeason1}
    Click Element  ${Loc_MerchPlanNode-SecondaryTab}
    DOMready-TabLoads
    Click Element From List  ${Loc_MerchPlanNode-PlanTab_SecondaryTypesToolbar_DDL}  -1
    DOMreadyByWaitTime    2
    ${Element1} =  Required Element From List  //div[@class='dijitReset dijitMenuItem' and text()='${SegmentType1}']  -1
    Element Should Be Visible  ${Element1}
    ${Element2} =  Required Element From List  //div[@class='dijitReset dijitMenuItem' and text()='${SegmentType2}']  -1
    Element Should Be Visible  ${Element2}
    Click Element  ${Element1}
    #Select value from the DDL  ${Element1}
    Click Element  ${Loc_MerchPlanNode-SecondaryTab}
    DOMready-TabLoads
    Click Element  ${Loc_NewFromSecondaryPlan}
    DOMready-PopUpAppears
    Click Element  //td[text()='MonthlyValue-01']
    Click Element  //td[text()='MonthlyValue-02']
    DOMreadyByWaitTime    2
    Wait For Element And Click  ${Save_btn}
    DOMready-PopUpCloses    
Create Folder for Region Level & navigate in to it
    [Tags]  Merchandising
    Click Element  ${Loc_MerchPlanNode-PlanTab}
    Wait For Release Info
    DOMready-TabLoads
    Polling Mouse Over  ${Loc_MerchPlanNode-PlanTab_ActionsLink}
    Mouse Over  ${Loc_MerchPlanNode-PlanTab_ActionsLink}
    Element Should Be Visible  ${Loc_MerchPlanNode-PlanTab_ActionsLink}
    #*Create Folder at Plan Level - Amreica and verify Delete icon is Updated
    Create Folder1
    Element Should be Visible  //tr/td/span/a[text()='${Folder1Name}']/following::td[contains(@class, actionsColumn)]/div/span[@data-csi-act='Delete']
  	Click Link  ${Loc_PlanNode-PlanTab_Folder1NameLink}
    DOMready-TabLoads    
    DOMreadyByWaitTime    2
Add version for Region  
    [Tags]  Merchandising
    Click Element   ${Loc_FolderNode-VersionsTab}
    DOMready-TabLoads
    Wait Until Element IS Visible   ${Loc_Release}
    #Element Should Be Visible  //td[@class='attrString firstColumn' and text()='${MerchSeason1}']
    Wait Until Element Is Visible  ${Loc_FolderNode-VersionsTab_NewFolderVersionActions}
    Click Element  ${Loc_FolderNode-VersionsTab_NewFolderVersionActions}
    DOMready-PopUpAppears
    DOMreadyByWaitTime    2
    Click Element From List  //*[text()='${MerchSeason2}']  -1
    DOMreadyByWaitTime    2
    Wait For Element And Click  ${Save_btn}
    DOMready-PopUpCloses    
Create Folder for Department Level & navigate in to it
    [Tags]  Merchandising
  #Reload
  #DOMready-TabLoads  
  #PO_SetupPage.Click on Site Search Field and Hit Search  Folder   ${Folder1Name}
  #DOMreadyByWaitTime  4 
  #PO_SetupPage.Click on Required Value From Site Search Result  1
  #Wait.DOMready-TabLoads
    Click Element  ${Loc_Folder1Node-PlanTab}
    DOMready-TabLoads
  
    Click Element From List  ${Loc_MerchPlanNode-SubFolder_PlanTab_VersionTypesToolbar_DDL}  -1
    DOMreadyByWaitTime    2
    Element Should Be Visible  //div[@class='dijitReset dijitMenuItem' and text()='${MerchSeason2}']
    Select value from the DDL  ${MerchSeason1}
    Click Element  ${Loc_Folder1Node-PlanTab}
    DOMready-TabLoads
    Click Element From List  ${Loc_MerchPlanNode-SubFolder_PlanTab_SecondaryTypesToolbar_DDL}  -1
    DOMreadyByWaitTime    2
    ${Element1} =  Required Element From List  //div[@class='dijitReset dijitMenuItem' and text()='${SegmentType1}']  -1
    Element Should Be Visible  ${Element1}
    Click Element  ${Element1}
  
  #Polling Mouse Over  ${Loc_Folder1Node-PlanTab_ActionsLink}
  DOMreadyByWaitTime    4
  #* Create Folder Level 2 - ChildrensWear  
  Click Element From List  ${Loc_Folder1Node-PlanTab_ActionsLink}  -1
  DOMreadyByWaitTime    2
  Create Folders  ${Folder2Name}  MerchFolderVersion-TargetFolders
  DOMready-TabLoads
  Element Should be Visible  //tr/td/span/a[text()='${Folder2Name}']/following::td[contains(@class, actionsColumn)]/div/span[@data-csi-act='Delete']
 
  Click Link  ${Loc_PlanNode-PlanTab_Folder2NameLink}
  DOMready-TabLoads    
  DOMreadyByWaitTime    2
Add version for Department  
    [Tags]  Merchandising
    Click Element   ${Loc_FolderNode-VersionsTab}
    DOMready-TabLoads
    Wait Until Element IS Visible   ${Loc_Release}
    #Element Should Be Visible  //td[@class='attrString firstColumn' and text()='${MerchSeason1}']
    Wait Until Element Is Visible  ${Loc_FolderNode-VersionsTab_NewFolderVersionActions}
    Click Element  ${Loc_FolderNode-VersionsTab_NewFolderVersionActions}
    DOMready-PopUpAppears
    DOMreadyByWaitTime    2
    Click Element From List  //*[text()='${MerchSeason2}']  -1
    DOMreadyByWaitTime    2
    Wait For Element And Click  ${Save_btn}
    DOMready-PopUpCloses      
Create Folder for Gender Level & navigate in to it
    [Tags]  Merchandising
  Click Element  ${Loc_Folder1Node-PlanTab}
  DOMready-TabLoads  
  DOMreadyByWaitTime    2
  Wait Until Element Is Visible  ${Loc_Folder1Node-PlanTab_ActionsLink}
  Click Element From List  ${Loc_Folder1Node-PlanTab_ActionsLink}  -1
  DOMreadyByWaitTime    2
    #* Create Folder Level 3 - Boys 
  Create Folders  ${Folder3Name}  MerchFolderVersion-TargetFolders
  DOMready-TabLoads
  Element Should be Visible  //tr/td/span/a[text()='${Folder3Name}']/following::td[contains(@class, actionsColumn)]/div/span[@data-csi-act='Delete']
  
  Click Link  ${Loc_Folder1Node-PlanTab_Folder3NameLink}
  DOMready-TabLoads    
  DOMreadyByWaitTime    2
Add version for Gender  
    [Tags]  Merchandising
    Click Element   ${Loc_FolderNode-VersionsTab}
    DOMready-TabLoads
    Wait Until Element IS Visible   ${Loc_Release}
    #Element Should Be Visible  //td[@class='attrString firstColumn' and text()='${MerchSeason1}']
    Wait Until Element Is Visible  ${Loc_FolderNode-VersionsTab_NewFolderVersionActions}
    Click Element  ${Loc_FolderNode-VersionsTab_NewFolderVersionActions}
    DOMready-PopUpAppears
    DOMreadyByWaitTime    2
    Click Element From List  //*[text()='${MerchSeason2}']  -1
    DOMreadyByWaitTime    2
    Wait For Element And Click  ${Save_btn}
    DOMready-PopUpCloses
Create Merch Collection & navigate in to it
    [Tags]  Merchandising
  Click Element  ${Loc_Folder3Node-PlanTab}
  DOMready-TabLoads  
  DOMreadyByWaitTime    2 
  Wait Until Element Is Visible  ${Loc_Folder3Node-PlanTab_ActionsLink} 
  Click Element  ${Loc_Folder3Node-PlanTab_ActionsLink}
  Wait.DOMreadyByWaitTime  2
  Create Collection  ${MerchCollection1Name}  MerchFolderVersion-TargetCollections
  DOMready-TabLoads  
  # Verfiy Created Collection Name 
  Element Should Be Visible  ${Loc_Folder3Node-PlanTab_MerchCollection1NameLink}  
  # Verify the Delete icon is Updated for the Collection 
  Page Should Contain Element  //td[@data-csi-act='Node Name:Child:__Parent__:0']/following::td[2]/div/span[@title='Delete']
  Click Link  ${Loc_Folder3Node-PlanTab_MerchCollection1NameLink}
  DOMready-TabLoads    
  DOMreadyByWaitTime    2
Add version for Collection  
    [Tags]  Merchandising
    Click Element   ${Loc_CollectionNode-VersionsTab}
    DOMready-TabLoads
    Wait Until Element IS Visible   ${Loc_Release}
    #Element Should Be Visible  //td[@class='attrString firstColumn' and text()='${MerchSeason1}']
    Wait Until Element Is Visible  ${Loc_CollectionNode-VersionsTab_NewFolderVersionActions}
    Click Element  ${Loc_CollectionNode-VersionsTab_NewFolderVersionActions}
    DOMready-PopUpAppears
    DOMreadyByWaitTime    2
    Click Element From List  //*[text()='${MerchSeason2}']  -1
    DOMreadyByWaitTime    2
    Wait For Element And Click  ${Save_btn}
    DOMready-PopUpCloses   
Create Merch Product & navigate in to it
    [Tags]  Merchandising
    Click Element  ${Loc_MerchCollectionNode-PlanTab}
    DOMready-TabLoads  
    DOMreadyByWaitTime    2
    Wait Until Element Is Visible  ${Loc_MerchCollectionNode-plan_ActionsLink}
    Click Element      ${Loc_MerchCollectionNode-plan_ActionsLink}
    DOMready-PopUpAppears  
    Click Element  ${NewProductDialog_ProductTextArea}
    Clear Element Text    ${NewProductDialog_ProductTextArea}
    Input Text  ${NewProductDialog_ProductTextArea}  ${MerchProduct1}
    Wait.DOMreadyByWaitTime  2
     # Update values in New Product Dialog 
    Click On the DDL Button  field-MerchProduct-NewProductVersionForm-Subtype
    Wait.DOMreadyByWaitTime  2
    Select value from the DDL  ${MerchProductType}
    Wait.DOMreadyByWaitTime  2 
    ${CreateStyleCheckboxStatus} =  Get Element Attribute  ${NewProductDialog_CreateStyleCheckbox}  aria-checked
    Run Keyword If  '${CreateStyleCheckboxStatus}'=='false'  Click Element  ${NewProductDialog_CreateStyleCheckbox}
    Wait.DOMreadyByWaitTime  5
    Click Element From List  css=[data-csi-act='NextPage']  -1
    # Update values in second section of New Product Dialog 
    @{NewProductDialog2Attributes} =  Create List  Create as Candidate    Style Type    Template    Season    Brand    Department    Collection    Theme
    :FOR   ${ListItems}  IN  @{NewProductDialog2Attributes}
    \  Element Should Be Visible  //th[text()='${ListItems}']
    # Available buttons validation 
    Page Should Contain Element  //span[text()='Back']
    Page Should Contain Element  //span[text()='Finish']
    ${ReqElement} =  Required Element From List  //span[text()='Cancel']  -1 
    Page Should Contain Element  ${ReqElement}   
    # Update values in New Product Dialog
    ${CandidateCheckboxStatus} =  Get Element Attribute  ${NewProductDialog_CreateAsCandidateCheckbox}  aria-checked
    Run Keyword If  '${CandidateCheckboxStatus}'=='false'  Click Element  ${NewProductDialog_CreateAsCandidateCheckbox}
    Wait.DOMreadyByWaitTime  5
    # Update season hierarchy values in the aggregate
    @{SeasonAttributes} =  Create List  ${NewProductDialog_StyleTypeDDL}  ${NewProductDialog_SeasonDDL}   ${NewProductDialog_BrandDDL}  ${NewProductDialog_DepartmentDDL}  ${NewProductDialog_CollectionDDL}
    ${SeasonAttributeValues} =  Create List   ${StyleType}  ${MerchSeason}  ${MerchBrand}  ${MerchDepartment}  ${MerchCollection}
    ${Index} =  Set Variable  0
    :FOR  ${Iterator}  IN  @{SeasonAttributes}
    \  Click Element  ${Iterator}
    \  ${ReqElement} =  Get From List  ${SeasonAttributeValues}  ${Index}
    \  Select value from the DDL  ${ReqElement}
    \  ${Index} =  Evaluate  ${Index} + 1
    Click Element From List  css=[data-csi-act='Finish']  -1 
    DOMready-PopUpCloses
    DOMreadyByWaitTime    5
    Click Element From List  //a[text()='${MerchProduct1}']  0
    DOMready-TabLoads    
    DOMreadyByWaitTime    2
Add version for Product  
    [Tags]  Merchandising
    Click Element   ${Loc_ProductsNode-VersionsTab} 
    DOMready-TabLoads
    Wait Until Element IS Visible   ${Loc_Release}
    #Element Should Be Visible  //td[@class='attrString firstColumn' and text()='${MerchSeason1}']
    Wait Until Element Is Visible  ${Loc_ProductNode-VersionsTab_NewFolderVersionActions}
    Click Element  ${Loc_ProductNode-VersionsTab_NewFolderVersionActions}
    DOMready-PopUpAppears
    DOMreadyByWaitTime    2
    Click Element From List  //*[text()='${MerchSeason2}']  -1
    DOMreadyByWaitTime    2
    Wait For Element And Click  ${Save_btn}
    DOMready-PopUpCloses     
    
Create Merch Option
    [Tags]  Merchandising
    Click Element  ${Loc_MerchProductNode-PlanTab}
    DOMready-TabLoads  
    DOMreadyByWaitTime    2
    Wait Until Element Is Visible  ${Loc_MerchProductNode-PlanTab-Actions}
    Mouse Over  ${Loc_MerchProductNode-PlanTab-Actions}
	Click Element  ${Loc_MerchProductNode-PlanTab-Actions}
	DOMReadyByWaitTime  2
	#Click Element  ${Loc_MerchProductNode-PlanTab-Actions_OptionLink}
	#DOMReadyByWaitTime  2
	Create Option  ${Var_Option1Name}  MerchProductVersion-Target
	DOMready-TabLoads
	# Verfiy Created Option Created 
    Element Should Be Visible  //span[@class='attrPrimary']/a[text()='${Var_Option1Name}']
    Click Element From List  //a[text()='${Var_Option1Name}']  0
    DOMready-TabLoads    
    DOMreadyByWaitTime    2
Add version for Option  
    [Tags]  Merchandising
    Click Element   ${Loc_OptionsNode-VersionsTab} 
    DOMready-TabLoads
    Wait Until Element IS Visible   ${Loc_Release}
    #Element Should Be Visible  //td[@class='attrString firstColumn' and text()='${MerchSeason1}']
    Wait Until Element Is Visible  ${Loc_OptionNode-VersionsTab_NewFolderVersionActions}
    Click Element  ${Loc_OptionNode-VersionsTab_NewFolderVersionActions}
    DOMready-PopUpAppears
    DOMreadyByWaitTime    2
    Click Element From List  //*[text()='${MerchSeason2}']  -1
    DOMreadyByWaitTime    2
    Wait For Element And Click  ${Save_btn}
    DOMready-PopUpCloses                
  [Teardown]    Close Browser

***Keywords***
Setup MerchandisingData
  ${This_DataProvider} =  Data_Provider.DataProvider  Merchandising
  Set Test Variable  ${This_DataProvider}
  ${StyleType} =  Data_Provider.GetDataProviderColumnValue  Data_StyleTypeName
  ${MerchSeason_Collection} =  Data_Provider.GetDataProviderColumnValue  Data_SeasonName
  ${MerchSeason} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MerchSeason_Collection}  1
  ${MerchBrand_Collection} =  Data_Provider.GetDataProviderColumnValue   Data_BrandName
  ${MerchBrand} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MerchBrand_Collection}  1
  ${MerchCol_Collection} =  Data_Provider.GetDataProviderColumnValue   Data_CollectionName
  ${MerchCollection} =   Data_Provider.DataProviderSplitterForMultipleValues  ${MerchCol_Collection}  1
  ${MerchDepartment_Collection} =  Data_Provider.GetDataProviderColumnValue   Data_DepartmentName
  ${MerchDepartment} =   Data_Provider.DataProviderSplitterForMultipleValues  ${MerchDepartment_Collection}  1
  Set Test Variable  ${StyleType}
  Set Test Variable  ${MerchSeason}
  Set Test Variable  ${MerchBrand}
  Set Test Variable  ${MerchDepartment}
  Set Test Variable  ${MerchCollection}
  #Set Test Variable  ${MerchTheme}
  
Create Folder1
    Polling Click  ${Loc_MerchPlanNode-PlanTab_ActionsLink}
    Wait Until Element IS Visible  //span[text()='Folder created successfully.']
    ${ActionCommentVisibility} =  Run Keyword And Return Status  Wait Until Element Is Visible  css=[data-csi-act='ActionComment']  timeout=5 seconds
    Log  ${ActionCommentVisibility}
    Input Text  //div/textarea  ${Folder1Name}
    Click Element  ${Loc_MerchPlanNode-PlanTab}

Create Folders
    [Arguments]  ${FolderName}  ${CurrentPlanTab}
    Wait Until Element IS Visible  //span[text()='Folder created successfully.']
    ${ActionCommentVisibility} =  Run Keyword And Return Status  Wait Until Element Is Visible  css=[data-csi-act='ActionComment']  timeout=5 seconds
    Log  ${ActionCommentVisibility}
    Input Text  //div/textarea  ${FolderName}
    Click Element   css=[data-csi-tab='${CurrentPlanTab}']

Create Collection
    [Arguments]  ${CollectionName}  ${CurrentPlanTab}
    Wait Until Element IS Visible  //span[text()='Merch Collection created successfully.']
    ${ActionCommentVisibility} =  Run Keyword And Return Status  Wait Until Element Is Visible  css=[data-csi-act='ActionComment']  timeout=5 seconds
    Log  ${ActionCommentVisibility}
    Input Text  //div/textarea  ${CollectionName}
    Click Element   css=[data-csi-tab='${CurrentPlanTab}']

Create Option
    [Arguments]  ${OptionName}  ${CurrentTab}
    Wait Until Element IS Visible  //span[text()='Option created successfully.']
    ${ActionCommentVisibility} =  Run Keyword And Return Status  Wait Until Element Is Visible  css=[data-csi-act='ActionComment']  timeout=5 seconds
    Log  ${ActionCommentVisibility}
    Input Text  //div/textarea  ${OptionName}
    Click Element   css=[data-csi-tab='${CurrentTab}']
        
Click On Right Arrow Button To View Hidden Tabs    
   Wait Until Element Is Visible  ${Loc_Home_ScrollRightToViewOtherTabsButton}
   Click Element  ${Loc_Home_ScrollRightToViewOtherTabsButton}
   Wait Until Element Is Not Visible    ${Loc_Home_ScrollRightToViewOtherTabsButton}    
   


Polling Mouse Over
  [Arguments]  ${ActionLink}
  :FOR  ${i}  IN RANGE  0  5
  \  ${MouseOver} =  Run Keyword And Return Status  Mouse Over  ${ActionLink}
  \  Exit For Loop If  '${MouseOver}' == 'True'

Polling Click
  [Arguments]  ${ActionLink}
  :FOR  ${i}  IN RANGE  0  5
  \  ${Click} =  Run Keyword And Return Status  Click Element  ${ActionLink}
  \  Exit For Loop If  '${Click}' == 'True'


                  
*** Comments ***

  	
  	
  	
  	
  	
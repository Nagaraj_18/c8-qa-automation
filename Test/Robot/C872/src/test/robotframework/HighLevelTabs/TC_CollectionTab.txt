*** Settings ***
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt
Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../Locators/Loc_CollectionManagement.txt
Resource          ../Locators/Loc_BusinessPlanning.txt
Resource          ../Locators/Loc_CompanyCurrencyPage.txt

***Variable***
${MarketinCollection_name}  NewMarketingCollection
${Currency_name}  Euro
${InputField}  //div/textarea
${MarketLook_Name}  NewMarketLook
${MarketTool_Name}  NewMarkeTool
${NewSales_Name}  NewSalesMarket
${SalesOrder_Name}  NewSalesOrder
${SalesOrderGroup_Name}  NewSalesOrderGroup
${Currency1}  Euro


*** Test Cases ***
Login To Application
	#Data provider: It reads the data from the excel sheet for the test case specified.
	${This_DataProvider} =  Data_Provider.DataProvider  Create BOM Type
	Set Test Variable  ${This_DataProvider}
    Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    DOMready-TabLoads

Create Currency
    Click on Setup Icon
    Wait.DOMready
    DOMready-TabLoads 
    Click Element  ${Loc_Setup-CompanyTab}
    DOMready-TabLoads 
    Click Element  ${Loc_Currency_Tab}
    DOMready-TabLoads
    Mouse Over  ${Loc_Setup-CurrencyTab_ActionsLink}
    Click Element  ${Loc_Setup-CurrencyTab_ActionsLink}     
    DOMReadyByWaitTime  2
    Input Text  //div/textarea  ${Currency1}
    Click Element    ${Loc_Currency_Tab}
    DOMreadyByWaitTime  5
    Assert for the Given Element  //a[@class='browse' and text()='${Currency1}']
    DOMready-TabLoads     


Create New Marketing Collection
    Click on Home Icon
    DOMready-TabLoads
    #Click on the right arrow button in the Home Page
    ${ScreenSizeISSmall}=  Run KeyWord And Return Status  Click On Right Arrow Button To View Hidden Tabs  0
    Log  ${ScreenSizeISSmall}
    DOMReadyByWaitTime  3
    Click Element  ${Loc_CollectionsMgtTab}
    DOMreadyByWaitTime  5
    Click Element  ${Loc_MarketingCollectionsTab}
    DOMreadyByWaitTime  5
    Click Element  ${Loc_ActionsNewMarketingCollections}
    Wait.DOMreadyByWaitTime  2
    Input Text  ${Loc_MarketingCollectionsTextbox}  ${MarketinCollection_name}
    Wait.DOMreadyByWaitTime  2
    Click Element  ${CurrencyList}
    Wait.DOMreadyByWaitTime  1
    Select Elemnt from DDL  ${Currency_name}
    Wait.DOMreadyByWaitTime  1
    PO_NewCreationPopUpPage.Click on Save Button
    DOMreadyByWaitTime  5
Create Marketing Looks
    Click Element  ${Loc_MarketingLooksTab}
    DOMreadyByWaitTime  5
    Click Element  ${Loc_ActionsNewMarketingLook}
    Wait.DOMreadyByWaitTime  2    
    Input Text  ${InputField}  ${MarketLook_Name}
    Wait.DOMreadyByWaitTime  1
    
Create Marketing Tool
    Click Element  ${Loc_MarketingToolsTab}
    DOMreadyByWaitTime  5
    Click Element  ${Loc_ActionsNewMarketingTool}
    Wait.DOMreadyByWaitTime  2
    Input Text  ${InputField}  ${MarketTool_Name}
    Wait.DOMreadyByWaitTime  1
    
Create Sales Markets
    Click Element  ${Loc_MarketingSalesMarketsTab}
    DOMready-TabLoads
    Click Element  ${Loc_ActionSalesMarket}
    Wait.DOMreadyByWaitTime  2    
    Input Text  ${Loc_MCSalesMarketsFormNodeName}  ${NewSales_Name}
    Click Element  ${Loc_MCSalesMarketsFormCurrency}
    Wait.DOMreadyByWaitTime  1
    Select Elemnt from DDL  ${Currency1}
    Wait.DOMreadyByWaitTime  1
    PO_NewCreationPopUpPage.Click on Save Button
    DOMreadyByWaitTime  5
Create Sales Order
    Click Element  ${Loc_MarketingSalesOrderTab}
    DOMreadyByWaitTime  5
    Click Element  ${Loc_ActionsSalesOrder}
    Wait.DOMreadyByWaitTime  2
    Input Text  ${NewSalesOrder_Name}  ${SalesOrder_Name}
    Wait.DOMreadyByWaitTime  1
    Click On the DDL Button  field-SalesOrder-Form-Market
    Wait.DOMreadyByWaitTime  2
    Select value from the DDL  ${NewSales_Name}
    Wait.DOMreadyByWaitTime  2
    PO_NewCreationPopUpPage.Click on Save Button
    DOMreadyByWaitTime  5
Create Sales Order Group
    Click Element  ${Loc_CMSaleOrderGroup}
    DOMready-TabLoads
    Click Element  ${Loc_ActionSalesOrderGroup}
    Wait.DOMreadyByWaitTime  2
    Input Text  ${NewSalesOrderGroup_Name}  ${SalesOrderGroup_Name}
    Wait.DOMreadyByWaitTime  1
    PO_NewCreationPopUpPage.Click on Save Button
    DOMreadyByWaitTime  5
      [Teardown]    Close Browser
    
*** Settings ***
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt

Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_MaterialTypePage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../Locators/Loc_SetupPage.txt    
Resource          ../Locators/Loc_ContractualDocumentTypePage.txt 
Resource          ../PageObjects/PO_ContractualDocumentTypePage.txt

*** Test Cases ***
Home-00035
	[tags]  ContractualDocuments Sanity_DataLoad
	#Data provider: It reads the data from the excel sheet for the test case specified. 
	${This_DataProvider} =  Data_Provider.DataProvider  Create Contractual Document Types
	Set Test Variable  ${This_DataProvider}
	
	# Step 1: Open the Application in the browser.
    #Step Description
    #SuiteSetUp.Test Step    ${R_Open_Browser}
    SuiteSetUp.Open Browser To Login Page
    # Step 2: Enter the login Credentials and Submit.
    #Step Description
    #SuiteSetUp.Test Step    ${R_Enter_Credentials}
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    #Step Description
    #SuiteSetUp.Test Step    ${R_Click_Login}
    PO_LoginPage.Submit Credentials
    ##DOMreadyByWaitTime  5
    #Reload
    Wait.DOMready
    DOMready-TabLoads
    PO_SetupPage.Wait for Setup Icon
    PO_SetupPage.Click on Setup Icon
    Wait.DOMready
    DOMready-TabLoads
    # Step 3: Navigate to Style Type Attributes tab.
    #Step Description
    PO_SetupPage.Click On Configuration tab
    #Check-is the DOM ready
    Wait.DOMready
    PO_SetupPage.Click On Type Configuration tab
    #Check-is the DOM ready
    Wait.DOMready
    PO_SetupPage.Click on Contractual Document types tab
    #Check-is the DOM ready
    DOMready-TabLoads
    DOMreadyByWaitTime  3
    
    # Get the Contractual Document Names from Excel
    ${ContractualDocumentTypeCollection} =  Data_Provider.GetDataProviderColumnValue  Data_ContractualDocumentTypeName
	# Values for the Contractual Document is extracted from the specified column '
	# Get the Count of Contractual Documents from Excel 
	${Count} =  DataProviderSplitterValues-Total  ${ContractualDocumentTypeCollection}
	    
    #Step Description
    #Step 1: Create Contractual Document Types     
    :FOR  ${i}  IN RANGE  0  ${Count} 
    \  Mouse Over   ${Action_ContractualDocumentType}
    \  Wait Until Element Is Visible  ${Loc_LinkNewContractualDocument}
    \  Click Element  ${Loc_LinkNewContractualDocument}
    \  DOMreadyByWaitTime  4
    \  ${index} =  Evaluate  ${i} + 1
    \  ${ContractualDocumentType} =  DataProviderSplitterForMultipleValues  ${ContractualDocumentTypeCollection}  ${index}
    \  Input Text  ${Loc_TextBoxContractualDocumentType}  ${ContractualDocumentType}
    \  PO_NewCreationPopUpPage.Click on Required Button in the New Creation Pop Up  Save
    \  DOMready-PopUpCloses
    \  DOMreadyByWaitTime  2    
    \  PO_SetupPage.Click on Contractual Document types tab
    
	#Step 2: Update First Contractual Document Types and select Attributes
	${ContractualDocumentTypeCollection} =  Data_Provider.GetDataProviderColumnValue  Data_ContractualDocumentTypeName
	${ContractualDocumentType1} =  DataProviderSplitterForMultipleValues  ${ContractualDocumentTypeCollection}  1
	#Sub Step 1: Click on Active check box attribute
	Click on the Checkbox in CDOC tab  ${ContractualDocumentType1}  1
	DOMreadyByWaitTime    4 
    #Sub Step 2: Click on Requires Acknowledgement check box attribute
	Click on the Checkbox in CDOC tab  ${ContractualDocumentType1}  3
	DOMreadyByWaitTime    4  
    #Sub Step 3: Click on Has Effectivity Dates check box attribute
	Click on the Checkbox in CDOC tab  ${ContractualDocumentType1}  4
	DOMreadyByWaitTime    4 
    #Sub Step 4: Click on Has Renewal Period check box attribute
	Click on the Checkbox in CDOC tab  ${ContractualDocumentType1}  5
	DOMreadyByWaitTime    4
    #Sub Step 5: Click on Renewal Period Begins on Receipt check box attribute
	Click on the Checkbox in CDOC tab  ${ContractualDocumentType1}  6
	DOMreadyByWaitTime    4
	    
	#Step 3: Update Second Contractual Document Types and select Attributes
	${ContractualDocumentTypeCollection} =  Data_Provider.GetDataProviderColumnValue  Data_ContractualDocumentTypeName
	${ContractualDocumentType2} =  DataProviderSplitterForMultipleValues  ${ContractualDocumentTypeCollection}  2
	#Sub Step 1: Click on Active check box attribute
	Click on the Checkbox in CDOC tab  ${ContractualDocumentType2}  1
	DOMreadyByWaitTime    4 
    #Sub Step 2: Click on Requires Acknowledgement check box attribute
	Click on the Checkbox in CDOC tab  ${ContractualDocumentType2}  3
	DOMreadyByWaitTime    4  
    #Sub Step 3: Click on Has Effectivity Dates check box attribute
	Click on the Checkbox in CDOC tab  ${ContractualDocumentType2}  4
	DOMreadyByWaitTime    4  
    #Sub Step 4: Click on Has Renewal Period check box attribute
	Click on the Checkbox in CDOC tab  ${ContractualDocumentType2}  5
	DOMreadyByWaitTime    4 
 
	#Step 4: Update Third Contractual Document Types and select Attributes
	${ContractualDocumentTypeCollection} =  Data_Provider.GetDataProviderColumnValue  Data_ContractualDocumentTypeName
	${ContractualDocumentType3} =  DataProviderSplitterForMultipleValues  ${ContractualDocumentTypeCollection}  3
	#Sub Step 1: Click on Active check box attribute
	Click on the Checkbox in CDOC tab  ${ContractualDocumentType3}  1
	DOMreadyByWaitTime    4 
    #Sub Step 2: Click on Requires Acknowledgement check box attribute
	Click on the Checkbox in CDOC tab  ${ContractualDocumentType3}  3
	DOMreadyByWaitTime    4  
    #Sub Step 3: Click on Has Effectivity Dates check box attribute
	Click on the Checkbox in CDOC tab  ${ContractualDocumentType3}  4  
	DOMreadyByWaitTime    4

	#Step 5: Update Fourth Contractual Document Types and select Attributes
	${ContractualDocumentTypeCollection} =  Data_Provider.GetDataProviderColumnValue  Data_ContractualDocumentTypeName
	${ContractualDocumentType4} =  DataProviderSplitterForMultipleValues  ${ContractualDocumentTypeCollection}  4
	#Sub Step 1: Click on Active check box attribute
	Click on the Checkbox in CDOC tab  ${ContractualDocumentType4}  1 
    DOMreadyByWaitTime    4
    #Sub Step 2: Click on Requires Acknowledgement check box attribute
	Click on the Checkbox in CDOC tab  ${ContractualDocumentType4}  3  
	DOMreadyByWaitTime    4
	#Step 6: Update Fifth Contractual Document Types and select Attributes
	${ContractualDocumentTypeCollection} =  Data_Provider.GetDataProviderColumnValue  Data_ContractualDocumentTypeName
	${ContractualDocumentType5} =  DataProviderSplitterForMultipleValues  ${ContractualDocumentTypeCollection}  5
	#Sub Step 1: Click on Active check box attribute
	Click on the Checkbox in CDOC tab  ${ContractualDocumentType5}  1 
  
	    # Step 7: Close the Browser Instance.
    SuiteSetUp.Test Step    ${R_Close_Browser}
    [Teardown]    Close Browser
*** Settings ***
Test Setup         Setup The Variables
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Assertions.txt
Resource          ../UtilityHelper/Wait.txt

Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_MaterialTypePage.txt
Resource          ../PageObjects/PO_StyleTypePage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt

Resource          ../PageObjects/PO_EnumerationPage.txt
Resource          ../PageObjects/PO_BusinessObjectPage.txt
Resource          ../PageObjects/PO_MerchandiseType.txt

Resource          ../Locators/Loc_ConfigurationBusinessObjectsPage.txt
Resource          ../Locators/Loc_SetupPage.txt 
Resource          ../Locators/Loc_MerchandiseType.txt

*** Test Cases ***
Create Custom Attribute on a Business Objects
	[tags]  Merch
	#Data provider: It reads the data from the excel sheet for the test case specified. 
	${This_DataProvider} =  Data_Provider.DataProvider  Merchandising
	Set Suite Variable  ${This_DataProvider}
    SuiteSetUp.Open Browser To Login Page
    PO_LoginPage.Input UserName    NewUser_UserName
    PO_LoginPage.Input Password    NewUser_Password
    PO_LoginPage.Submit Credentials
    Wait Until Page Contains Element   ${Loc_Release}
    #Reload
    Click on Setup Icon
    Wait.DOMready
    DOMready-TabLoads
    Assert for the given element  ${Loc_SetupConfigurationIcon}
    Click Element  ${Loc_SetupConfigurationIcon}
    Wait For Release Info
    DOMready-TabLoads
    Click Element  ${Loc_SetupTab_ConfigurationTab}
    
Home-00069
#Disable Restrict Edit for Style Carryover Attribute 
    # Navigate to Style BO Node 
	[tags]  Merch
    Click Element  ${Loc_SetupConfigurationIcon}
    Wait For Release Info
    Click Element  ${Loc_SetupTab_ConfigurationTab}
    DOMready-TabLoads
    Click Element  ${Loc_BusinessObject}
    DOMready-TabLoads
    Click Element  ${Loc_Setup-BusinessObject_PageSizeSelectorDDL}
    Select value from the DDL   All
    DOMready-TabLoads
    Wait For Release Info
    BO Navigation WorkAround  Style  5
    #Focus  ${Loc_StyleLink}
    #Click On BO  Style
    DOMready-TabLoads
    Wait Until Page Contains Element   ${Loc_Release}
    # Set Restrict Edit to false 
    Click Element  ${BONode_CarryOverAttribute_EditIcon}
    Click Element  ${BOAttributeDialog_CopyRootType}
    Set Focus To Element  ${BOAttributeDialog_RestrictEdits}
    Click Element  ${BOAttributeDialog_RestrictEdits}
    PO_NewCreationPopUpPage.Click on Save Button
    DOMready-PopUpCloses   
    
Home-00072
#Add Carryover Attribute to Style Form Definition
    [tags]  Merch
    Click Element    ${Loc_StyleBONode-FormDefinitionTab}
    DOMready-TabLoads
    Mouse Over  ${Loc_StyleBONode-FormDefinition_ActionLink}
    Click Element  ${Loc_StyleBONode-FormDefinition_ActionLink}
    Set Focus To Element  ${Loc_SelectAttributesDlg_CarryoverCheckbox}
    DOMready-TabLoads
    Select Checkbox In Popup  Style/CarryOver
    PO_NewCreationPopUpPage.Click on Save Button
    DOMready-PopUpCloses   

Custom Attribute Creation On Merchandise Folder 
	[tags]  Merch1
	BO Navigation WorkAround  Merchandise Folder  0
    DOMready-TabLoads
    Click On Action Link For Custom BO Attribute Creation
    Wait Until Page Contains Element  ${NewAttributeDialog_AttributeNameTextBox}
    Input Text  ${NewAttributeDialog_AttributeNameTextBox}  ${CustomBoAttributeName3}
    Create and Update Enum value for Custom Attribute  ${NewEnum3}
    Set Focus To Element  ${Loc_NewAttributeDialog_OptionalCheckBox}
    Mouse Over    ${Loc_NewAttributeDialog_OptionalCheckBox}
    Click Checkbox  ${Loc_NewAttributeDialog_OptionalCheckBox}
    PO_NewCreationPopUpPage.Click on Save Button
    DOMready-PopUpCloses
  [Teardown]    Close Browser 	
***Comments****
Update Required Enum
	[tags]  Merchx
	Click Element  ${Loc_SetupConfigurationIcon}
    Wait Until Page Contains Element   ${Loc_Release}
    Click Element  ${Loc_SetupTab_ConfigurationTab}
    Wait Until Page Contains Element   ${Loc_Release}
    PO_SetupPage.Click On Enumerations tab 
    DOMready-TabLoads
    Wait Until Page Contains Element   ${Loc_Release}
    
    # Create 1st Enum 
    Create a Enumeration  ${NewEnum3}
    DOMreadyByWaitTime  2
    Create Enumeration Value  ${NewEnum3Value1}
    DOMreadyByWaitTime  3   
    Create Enumeration Value  ${NewEnum3Value2}
    # Create 2nd Enum 
    Reload
    DOMready-TabLoads
    Create a Enumeration  ${NewEnum1}
    DOMreadyByWaitTime  2
    Create Enumeration Value  ${NewEnum1Value1}
    DOMreadyByWaitTime  2
    Create Enumeration Value  ${NewEnum1Value2}
    DOMreadyByWaitTime  2
    Create Enumeration Value  ${NewEnum1Value3}
    
    # Create 3rd Enum 
    Reload
    DOMready-TabLoads
    Create a Enumeration  ${NewEnum2}
    DOMreadyByWaitTime  2
    Create Enumeration Value  ${NewEnum2Value1}
    DOMreadyByWaitTime  2
    Create Enumeration Value  ${NewEnum2Value2}
    DOMreadyByWaitTime  2
    Create Enumeration Value  ${NewEnum2Value3} 
    DOMreadyByWaitTime  2

****comments***
#orginal
Custom Attribute Creation IdGender 
	[tags]  Merch
	BO Navigation WorkAround  Style Attributes  0
    Click On Action Link For Custom BO Attribute Creation
    Wait Until Page Contains Element  ${NewAttributeDialog_AttributeNameTextBox}
    Input Text  ${NewAttributeDialog_AttributeNameTextBox}  ${CustomBoAttributeName1}
    Create and Update Enum value for Custom Attribute  ${NewEnum1}
    Focus  ${NewAttributeDialog_EvaluateOnClientCheckBox}
    Mouse Over  ${NewAttributeDialog_EvaluateOnClientCheckBox}
    Click Element  ${NewAttributeDialog_EvaluateOnClientCheckBox}
    Focus    ${NewAttributeDialog_HiddenCheckBox}
    Mouse Over      ${NewAttributeDialog_HiddenCheckBox}
    #Click Element  ${NewAttributeDialog_HiddenCheckBox}
    PO_NewCreationPopUpPage.Click on Save Button
    DOMready-PopUpCloses
    
Custom Attribute Creation IdPricePoint 
	[tags]  Merch
    Reload
    Click On Action Link For Custom BO Attribute Creation
    Wait Until Page Contains Element  ${NewAttributeDialog_AttributeNameTextBox}
    Input Text  ${NewAttributeDialog_AttributeNameTextBox}  ${CustomBoAttributeName2}
    Create and Update Enum value for Custom Attribute  ${NewEnum2}
    Focus  ${NewAttributeDialog_EvaluateOnClientCheckBox}
    Mouse Over  ${NewAttributeDialog_EvaluateOnClientCheckBox}  
    Click Element  ${NewAttributeDialog_EvaluateOnClientCheckBox}
    Focus  ${NewAttributeDialog_HiddenCheckBox}
    Mouse Over    ${NewAttributeDialog_HiddenCheckBox}
    #Click Element  ${NewAttributeDialog_HiddenCheckBox}
    PO_NewCreationPopUpPage.Click on Save Button 
    DOMready-PopUpCloses   

Custom Attribute Creation On Merchandise Folder 
	[tags]  Merch
	BO Navigation WorkAround  Merchandise Folder  0
    DOMready-TabLoads
    Click On Action Link For Custom BO Attribute Creation
    Wait Until Page Contains Element  ${NewAttributeDialog_AttributeNameTextBox}
    Input Text  ${NewAttributeDialog_AttributeNameTextBox}  ${CustomBoAttributeName3}
    Create and Update Enum value for Custom Attribute  ${NewEnum3}
    Focus  ${Loc_NewAttributeDialog_OptionalCheckBox}
    Mouse Over    ${Loc_NewAttributeDialog_OptionalCheckBox}
    Click Element  ${Loc_NewAttributeDialog_OptionalCheckBox}
    PO_NewCreationPopUpPage.Click on Save Button
    DOMready-PopUpCloses
    
Custom Attribute Creation On Merchandise Product Version
	[tags]  Merch
    BO Navigation WorkAround  Merchandise Product Version  0
    Click On Action Link For Custom BO Attribute Creation
    Wait Until Page Contains Element  ${NewAttributeDialog_AttributeNameTextBox}
    Input Text  ${NewAttributeDialog_AttributeNameTextBox}  ${CustomBoAttributeName4}
    Create and Update Enum value for Custom Attribute  ${NewEnum2}
    Focus  ${NewAttributeDialog_EvaluateOnClientCheckBox}
    Mouse Over    ${NewAttributeDialog_EvaluateOnClientCheckBox}
    Click Element  ${NewAttributeDialog_EvaluateOnClientCheckBox}
    Focus  ${NewAttributeDialog_HiddenCheckBox}
    Mouse Over    ${NewAttributeDialog_HiddenCheckBox}
    Click Element  ${NewAttributeDialog_HiddenCheckBox}
    PO_NewCreationPopUpPage.Click on Save Button 
    DOMready-PopUpCloses   

 
*** Keywords ***  
Setup The Variables
  ${This_DataProvider} =  Data_Provider.DataProvider  Create Custom Attribute on a Business Objects
  Set Suite Variable  ${This_DataProvider}
  Log  ${This_DataProvider}
  
  ${BOAttributeCollection} =  Data_Provider.GetDataProviderColumnValue  Data_CustomBOAttributeName
  ${BOAttributeName} =  DataProviderSplitterForMultipleValues  ${BOAttributeCollection}  1
  Set Test Variable  ${BOAttributeName} 
        
 
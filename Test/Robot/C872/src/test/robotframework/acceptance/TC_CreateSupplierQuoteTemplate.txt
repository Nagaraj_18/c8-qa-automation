*** Settings ***
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt

Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_SourcingPage.txt
Resource		  ../PageObjects/PO_NewCreationPopUpPage.txt
Resource		  ../Locators/Loc_SourcingSetupPage.txt


*** Test Cases ***
Create Supplier Quote Template 
	[tags]  Sanity_DataLoad
	#Data provider: It reads the data from the excel sheet for the test case specified. 
	${This_DataProvider} =  Data_Provider.DataProvider  Create Supplier Quote Template
	Set Suite Variable  ${This_DataProvider}
	
    # Step 1: Open the Application in the browser.
    #Step Description
    #SuiteSetUp.Test Step    ${R_Open_Browser}
    Open Browser To Login Page
    #Assertion
    #Assertions.Assert the given Title of the Page    Login - Centric PLM
    
    # Step 2: Enter the login Credentials and Submit.
    #Step Description
    #SuiteSetUp.Test Step    ${R_Enter_Credintails}
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    
    #SuiteSetUp.Test Step    ${R_Click_Login}
    PO_LoginPage.Submit Credentials
    #Wait.DOMready
    #DOMready-TabLoads
    #Reload
    Wait.DOMready
    DOMready-TabLoads
    Click on Setup Icon
    Wait.DOMready
    DOMready-TabLoads
    #Assertion: Asserting for the 'Home' Tab to be Active
    #PO_SetupPage.Verify That This Tab Is Fronted    My Home
    Click on Home Icon
    Wait.DOMready
    DOMready-TabLoads
    #Assertion: Asserting for the 'Home' Tab to be Active
    #PO_SetupPage.Verify That This Tab Is Fronted    My Home
    
    #Step 3: Click on the Home Icon from the top menu.
    #Step Description
    #SuiteSetUp.Test Step  ${R_Click_HomeIcon}
    #PO_SetupPage.Click on Home Icon
    #Wait.DOMreadyByWaitTime  3
    
    # Step 3: Navigate to Home-Sourcing-Setup-Templates
    #Step Description 
    Click Element  ${Loc_Home_SourcingTab}
    DOMready-TabLoads
    Click Element  ${Loc_Home-Sourcing-SetupTab}
    DOMready-TabLoads
    Click Element  ${Loc_Sourcing-Setup-Template}	
    Wait.DOMready-TabLoads
  	 	
    # Step : Create Supplier Quote Template
   	# Mouse Over  ${Loc_ActionsSuplierQuoteTemplates}
   	Click Element  ${Loc_NewSuplierQuoteTemplateLink}
   	PO_SetupPage.Verify Dialog Window  Quote Template
   	${QuoteTemplateCollection} =  Data_Provider.GetDataProviderColumnValue  Data_SupplierQuoteTemplate
	${QuoteTemplate1} =  DataProviderSplitterForMultipleValues  ${QuoteTemplateCollection}  1
	Input text  ${Loc_TextFieldSupplierQuote}  ${QuoteTemplate1}
	PO_NewCreationPopUpPage.Click on Save Button
   	Wait.DOMready-PopUpCloses
   	Wait.DOMreadyByWaitTime  3
   	PO_SourcingPage.Click on the Node name in table  ${QuoteTemplate1}  1
   	
   	#Step : Update 1st Supplier Quote Template
   	#substep 1: Update HTS Code 
   	Wait.DOMready-TabLoads
   	Click Element  ${Loc_SQTNode_SQTTab}
   	Wait.DOMready-TabLoads
   	# Mouse Over  ${Loc_ActionAddHTSCode}
	# Click Element  ${Loc_TabSupplierQuote}  
    Click Element  ${Loc_AddHTSCodesLink}
   	PO_SetupPage.Verify Dialog Window  New from HTS Code
   	PO_NewCreationPopUpPage.Select All in popup
    Wait.DOMreadyByWaitTime  3
   	PO_NewCreationPopUpPage.Click on Save Button
    Wait.DOMready-PopUpCloses
   	Wait.DOMreadyByWaitTime  3
   	Click Element  ${Loc_EditFOB_Text}
   	Wait.DOMreadyByWaitTime  2
   	#Input Text  ${Loc_TextAreaFOB%}  65
   	Input Text  //input[@class='dijitReset dijitInputInner']  65
   	Click Element  ${Loc_TabSupplierQuote}
   	Wait.DOMreadyByWaitTime  2
   	Click Element  css=[data-csi-act='DutyPct::0']
   	Wait.DOMreadyByWaitTime  3
   	#Input Text  ${Loc_TextAreaDutySupplierQuote}   35
   	Input Text  //input[@class='dijitReset dijitInputInner']  35
   	Click Element  ${Loc_TabSupplierQuote}
   	Wait.DOMreadyByWaitTime  2
   	Assertions.Assert the given tab  Cost Scenario 
   	Assertions.Assert the given tab  Docs & Comments
   	
   	#substep 2: Update Cost Scenario 
   	Click Element  ${Loc_TabCostScenario} 
   	Wait.DOMready-TabLoads
   	Click Element  css=[data-csi-act='CreateCostScenario'] span.dijitNoIcon
   	DOMreadyByWaitTime  5
   	PO_SetupPage.Verify Dialog Window  Manage Cost Scenario
   	PO_NewCreationPopUpPage.Click on the DDL button  field-CostScenario-Form-QuoteRouting
   	${RoutingCollection} =  Data_Provider.GetDataProviderColumnValue  Data_RoutingTemplate
	${Routing1} =  DataProviderSplitterForMultipleValues  ${RoutingCollection}  1
	DOMreadyByWaitTime  2
	PO_NewCreationPopUpPage.Select Value from the DDL  ${Routing1}
	DOMreadyByWaitTime  2
	PO_NewCreationPopUpPage.Click on Save Button 
	Wait.DOMready-PopUpCloses
	DOMreadyByWaitTime  3
   	 
Update 2nd Supplier Quote Template
    Reload
    Wait.DOMready
    DOMready-TabLoads
    Click on Home Icon
    Wait.DOMready
    DOMready-TabLoads
    Click Element  ${Loc_Home_SourcingTab}
    DOMready-TabLoads
    Click Element  ${Loc_Home-Sourcing-SetupTab}
    DOMready-TabLoads
    Click Element  ${Loc_Sourcing-Setup-Template}	
    Wait.DOMready-TabLoads
   	# Mouse Over  ${Loc_ActionsSuplierQuoteTemplates}
   	Click Element  ${Loc_NewSuplierQuoteTemplateLink}
   	PO_SetupPage.Verify Dialog Window  New Supplier Quote Template
   	${QuoteTemplateCollection} =  Data_Provider.GetDataProviderColumnValue  Data_SupplierQuoteTemplate
	${QuoteTemplate2} =  DataProviderSplitterForMultipleValues  ${QuoteTemplateCollection}  2
	Input text  ${Loc_TextFieldSupplierQuote}  ${QuoteTemplate2}
	PO_NewCreationPopUpPage.Click on Save Button
   	Wait.DOMready-PopUpCloses
   	DOMreadyByWaitTime  4
   	Click Element  xpath=//a[@class='browse' and text()='${QuoteTemplate2}']
   	Wait.DOMready-TabLoads
   	
   	#Step : Update 2nd Supplier Quote Template
   	Click Element  ${Loc_TabSupplierQuote}
   	Wait.DOMready-TabLoads
   	#${Val} =  Get Element Attribute  //span[@data-csi-automation='actions-SupplierItemRevision-DutyPerUsage-root']//following::span[1][@class='dijitReset dijitInline dijitButtonText' and text()='Actions...']  id
    #Execute Javascript  document.getElementById("${Val}").scrollIntoView(); 
	
   	# Mouse Over  ${Loc_ActionAddHTSCode}
   	# Click Element  ${Loc_ActionAddHTSCode}
    Click Element  ${Loc_AddHTSCodesLink}
   	PO_SetupPage.Verify Dialog Window  New from HTS Code
   	PO_NewCreationPopUpPage.Select All in popup
    Wait.DOMreadyByWaitTime  2
   	PO_NewCreationPopUpPage.Click on Save Button
   	DOMready-PopUpCloses
   	DOMreadyByWaitTime  3
   	Click Element  ${Loc_EditFOB_Text}
   	Wait.DOMreadyByWaitTime  4
   	#Input Text   ${Loc_TextAreaFOB%}  80
   	Input Text  //input[@class='dijitReset dijitInputInner']  65
   	Click Element  ${Loc_TabSupplierQuote}
    #Click Element  css=[data-csi-act='DutyPct::0']
   	Wait.DOMreadyByWaitTime  2
   	#Input Text  ${Loc_TextAreaDutySupplierQuote}  20
   	Input Text  //input[@class='dijitReset dijitInputInner']  35
   	Click Element  ${Loc_TabSupplierQuote}
   	Wait.DOMreadyByWaitTime  2
   	Assertions.Assert the given tab  Cost Scenario 
   	Assertions.Assert the given tab  Docs & Comments
   	
   	#substep : Update Cost Scenario 
   	PO_SourcingPage.Click on Cost Scenario tab
   	Wait.DOMready-TabLoads
   	Click Element  css=[data-csi-act='CreateCostScenario'] span.dijitNoIcon
   	DOMreadyByWaitTime  5
   	PO_SetupPage.Verify Dialog Window  Manage Cost Scenario
   	PO_NewCreationPopUpPage.Click on the DDL button  field-CostScenario-Form-QuoteRouting
   	Wait.DOMreadyByWaitTime  2
   	${RoutingCollection} =  Data_Provider.GetDataProviderColumnValue  Data_RoutingTemplate
	${Routing3} =  DataProviderSplitterForMultipleValues  ${RoutingCollection}  3
	PO_NewCreationPopUpPage.Select Value from the DDL  ${Routing3}
	Wait.DOMreadyByWaitTime  2
	PO_NewCreationPopUpPage.Click on Save Button 
	
    # Step 9: Close the Browser Instance.
    #Test Step    ${R_Close_Browser}
    [Teardown]    Close Browser
****comments

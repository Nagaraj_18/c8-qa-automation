*** Settings ***
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt

Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_SpecificationPage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../PageObjects/PO_ExtendedFunctionsPage.txt

*** Test Cases ***
Specification-00551
	[tags]  Sanity_DataLoad
	#Data provider: It reads the data from the excel sheet for the test case specified. 
	${This_DataProvider} =  Data_Provider.DataProvider  Create Color Specification Libraries
	Set Test Variable  ${This_DataProvider}
	
    # Step 1: Open the Application in the browser.
    #Step Description
    #SuiteSetUp.Test Step    ${R_Open_Browser}
    Open Browser To Login Page
    #Assertion
    #Assertions.Assert the given Title of the Page    Login - Centric PLM
    
    # Step 2: Enter the login Credentials and Submit.
    #Step Description
    #SuiteSetUp.Test Step    ${R_Enter_Credintails}
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    
    #SuiteSetUp.Test Step    ${R_Click_Login}
    PO_LoginPage.Submit Credentials
    #Check-is the DOM ready
    #Reload
    Wait.DOMready
    DOMready-TabLoads
    Wait For Page Load
    Click on Setup Icon
    DOMready-TabLoads
    #Assertion: Asserting for the 'Configuration' Tab to be Active
    #PO_SetupPage.Verify That This Tab Is Fronted    My Home
   
    # Step 3: Navigate to Specification-Library tab	
    SuiteSetUp.Test Step  ${R_Click_HomeIcon}
    PO_SetupPage.Click on Home Icon
    Wait.DOMready
    DOMready-TabLoads
    #Step Description 
    Click On Specification tab
     Wait.DOMready
	DOMready-TabLoads
    Click Element  ${Loc_ProductAlternativeSpecificationsTab}
     Wait.DOMready
    DOMready-TabLoads
    ${ScreenSizeISSmall}=  Run KeyWord And Return Status  Click On Right Arrow Button To View Hidden Tabs  1
    Log  ${ScreenSizeISSmall}
    #PO_SpecificationPage.Click On Color Specification tab	
	PO_SpecificationPage.Click On Specification-Library tab
    DOMready-TabLoads
    # Step 4: Create New Color Specification Library
    Mouse Over  ${Loc_ActionsLibrariesTab}
    
    Click Element  ${Loc_ActionsLibrariesTab} td.dijitArrowButton
    DOMreadyByWaitTime  3
    Click Element From List  ${Loc_ActionsLibrariesTab} [data-csi-act="NewLibColorSpecification"] .dijitMenuItemLabel  -1
    

    #Click Element  ${Loc_NewColorSpecificationLibrary_link}
    #Click Element  ${Loc_ActionsColorSpecificationLibrariesTab}
    DOMreadyByWaitTime  2
    DOMready-TableCellEditableTextArea-Appears
    Wait Until Page Contains Element  xpath=//span[text()='Color Specification Library created successfully.']
    ${ColorSpecificationLibraryNameCollection} =  GetDataProviderColumnValue  Data_ColorSpecificationLibrary
    ${ColorSpecificationLibraryName1} =  DataProviderSplitterForMultipleValues  ${ColorSpecificationLibraryNameCollection}  1 
    Selenium2Library.Input Text  //div/textarea  ${ColorSpecificationLibraryName1} 
    DOMreadyByWaitTime  2
    PO_SpecificationPage.Click On Specification-Library tab    
    DOMreadyByWaitTime  2
    Click Element  //*[@class='browse' and text()='${ColorSpecificationLibraryName1}']
    DOMready-TabLoads
    Wait Until Element Is Enabled  css=[data-csi-automation='plugin-LibColorSpecification-LibElements-views']
    
    # Go Back
    # Wait For Page Load
Specification-00552
    [tags]  Sanity_DataLoad    
    # Step 5: Create New Care Labels for the Color Specification Library
    Click Element  ${Loc_ColorSpecificationNewColorSpecificationLink}
    DOMreadyByWaitTime  5
    # Click Element  ${Loc_NewColorSpecificationActionLink}
    ${This_DataProvider} =  Data_Provider.DataProvider  Create Color Specification Libraries
	Set Test Variable  ${This_DataProvider}
    ${ColorSpecificationNameCollection} =  GetDataProviderColumnValue  Data_LibraryElements
    ${ColorSpecificationName1} =  DataProviderSplitterForMultipleValues  ${ColorSpecificationNameCollection}  1 
    ${ColorSpecificationName2} =  DataProviderSplitterForMultipleValues  ${ColorSpecificationNameCollection}  2 
    ${ColorSpecificationName3} =  DataProviderSplitterForMultipleValues  ${ColorSpecificationNameCollection}  3 
     
    DOMreadyByWaitTime  2
	Selenium2Library.Input Text  //div/textarea  ${ColorSpecificationName1} 

    PO_SpecificationPage.Click on Library-Elements tab  LibColorSpecification-LibElements 
	DOMreadyByWaitTime  2
	#To Modify with a valid locator not with index name 
	#Mouse Over  css=[data-csi-automation='actions-LibColorSpecification-LibElements-root']
    #Click Element  css=[data-csi-act='NewColorSpecification']
    #Wait Until Element Is Visible  css=.dijitDialogTitleBar
    #Input Text  css=[data-csi-automation='field-ColorSpecification-Form-Node Name']  RX
    #Click Element  css=[data-csi-act='Save']
    #Wait Until Element Is Not Visible  css=.dijitDialogTitleBar
    #DOMreadyByWaitTime  2
    Mouse Over  //*[@class='browse' and text()='${ColorSpecificationName1}']//following::td[7]
    DOMreadyByWaitTime  3
    
	#Wait Until Element Is Visible    ${Loc_TableBrowseImages}
	#Click Element     ${Loc_TableBrowseImages}
	DOMreadyByWaitTime  5	
    # Upload Image  plugin-ImageBrowser-UploadOne  DOC1.PNG
      [Teardown]    Close Browser 
***Settings***
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt

Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../Locators/Loc_HomePage.txt
Resource          ../Locators/Loc_StylePage.txt
Resource          ../Locators/Loc_SourcingPage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt

***Variables***
@{OnlineSupplierUsers}  h1  i1  f1  g1
${OnlineSupplierPwd}  centric8

*** Test Cases ***
Test Setup  		
    DataSetup For New Enumeration
    
Login To Application 
    [Tags]  Portal User Business Cases

    Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    Wait.DOMready
    DOMready-TabLoads

Create Style Supplier Request for h1 user
    [Tags]  Portal User Business Cases
    Click on Home Icon
    DOMready-TabLoads
    Click Element  ${HomeStylesTab} 
    DOMready-TabLoads
    Click Element    ${Loc_StylesTab}
    DOMready-TabLoads
    Click Element    //a[text()='${StyleName1}']
    DOMready-TabLoads
    Click Element    ${Loc_Style-SourcingTab}
    DOMready-TabLoads
    Click Element    ${Loc_Style_SourcingTab_SupplierRequestTab}
    DOMready-TabLoads
    Click Element    css=[data-csi-automation='plugin-Style-SupplierRequests-ToolbarNewActions']
    DOMready-PopUpAppears
    Click Element    css=[data-csi-automation='field-SupplierRequest-Form-Node Name']
    Input Text    css=[data-csi-automation='field-SupplierRequest-Form-Node Name'] .dijitInputInner    ${SupplierRequestName1}
    Wait.DOMreadyByWaitTime  2
    Click Element    css=[data-csi-automation='field-SupplierRequest-Form-SRSuppliers']>div.dijitArrowButton
    Wait.DOMreadyByWaitTime  2
    # PO_NewCreationPopUpPage.Click on the DDL button2  field-SupplierRequest-Form-SRSuppliers    
    Click Checkbox Item in DDL Within a PopUp  Huckleberry
    DOMreadyByWaitTime  3
    PO_NewCreationPopUpPage.Click on the DDL button2  field-SupplierRequest-Form-SRSuppliers
    PO_NewCreationPopUpPage.Click on Save Button
    DOMreadyByWaitTime  3
    DOMready-PopUpCloses
 
Create Style Supplier Quotes and Issued for h1 user
    [Tags]  Portal User Business Cases
    Click Element    //a[text()='AUT_SR_001']
    DOMready-TabLoads
    Click Element    css=[data-csi-automation='plugin-SupplierRequest-StyleQuotes-ToolbarNewActions']
    DOMready-PopUpAppears
    Click Element    css=[data-csi-automation='field-SupplierItem-Form-Node Name']
    Wait.DOMreadyByWaitTime  2
    Input Text    css=[data-csi-automation='field-SupplierItem-Form-Node Name'] .dijitInputInner    ${SupplierQuotesName1}
    Wait.DOMreadyByWaitTime  2
    PO_NewCreationPopUpPage.Click on Save Button
    DOMreadyByWaitTime  3
    DOMready-PopUpCloses
    Click Element    css=[data-csi-act='IssueRequest']
    DOMreadyByWaitTime  3
    Element Should be Visible    css=[data-csi-act='CloseRequest']
 
Create Style Supplier Request for i1 user
    [Tags]  Portal User Business Cases
    Click on Home Icon
    DOMready-TabLoads
    Click Element  ${HomeStylesTab} 
    DOMready-TabLoads
    Click Element    ${Loc_StylesTab}
    DOMready-TabLoads
    Click Element    //a[text()='AUT_Style_S']
    DOMready-TabLoads
    Click Element    ${Loc_Style-SourcingTab}
    DOMready-TabLoads
    Click Element    ${Loc_Style_SourcingTab_SupplierRequestTab}
    DOMready-TabLoads
    Click Element    css=[data-csi-automation='plugin-Style-SupplierRequests-ToolbarNewActions']
    DOMready-PopUpAppears
    Click Element    css=[data-csi-automation='field-SupplierRequest-Form-Node Name']
    Input Text    css=[data-csi-automation='field-SupplierRequest-Form-Node Name'] .dijitInputInner    ${SupplierRequestName2}
    Wait.DOMreadyByWaitTime  2
    Click Element    css=[data-csi-automation='field-SupplierRequest-Form-SRSuppliers']>div.dijitArrowButton
    Wait.DOMreadyByWaitTime  2
    # PO_NewCreationPopUpPage.Click on the DDL button2  field-SupplierRequest-Form-SRSuppliers    
    Click Checkbox Item in DDL Within a PopUp  Iceplant
    DOMreadyByWaitTime  3
    PO_NewCreationPopUpPage.Click on the DDL button2  field-SupplierRequest-Form-SRSuppliers
    PO_NewCreationPopUpPage.Click on Save Button
    DOMreadyByWaitTime  3
    DOMready-PopUpCloses
    
Create Style Supplier Quotes and Issued for i1 user
    [Tags]  Portal User Business Cases
    Click Element    //a[text()='AUT_SR_003']
    DOMready-TabLoads
    Click Element    css=[data-csi-automation='plugin-SupplierRequest-StyleQuotes-ToolbarNewActions']
    DOMready-PopUpAppears
    Click Element    css=[data-csi-automation='field-SupplierItem-Form-Node Name']
    Wait.DOMreadyByWaitTime  2
    Input Text    css=[data-csi-automation='field-SupplierItem-Form-Node Name'] .dijitInputInner    ${SupplierQuotesName2}
    Wait.DOMreadyByWaitTime  2
    PO_NewCreationPopUpPage.Click on Save Button
    DOMreadyByWaitTime  3
    DOMready-PopUpCloses
    Click Element    css=[data-csi-act='IssueRequest']
    DOMreadyByWaitTime  3
    Element Should be Visible    css=[data-csi-act='CloseRequest']
    Close Browser
   
*** Keywords ***

DataSetup For New Enumeration  
	${This_DataProvider} =  Data_Provider.DataProvider  Create a New Enumeration
    Set Suite Variable  ${This_DataProvider}
    ${StyleNameList} =  Data_Provider.GetDataProviderColumnValue  Data_StyleName
    ${StyleName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${StyleNameList}  1
    Set Suite Variable  ${StyleName1}
    
    ${SupplierRequestList} =  Data_Provider.GetDataProviderColumnValue  Data_SupplierRequestName
    ${SupplierRequestName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SupplierRequestList}  1
    ${SupplierRequestName2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SupplierRequestList}  2 
    Set Suite Variable    ${SupplierRequestName1}
    Set Suite Variable    ${SupplierRequestName2}
    
    ${SupplierQuotesList} =  Data_Provider.GetDataProviderColumnValue  Data_SupplierQuoteName
    ${SupplierQuotesName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SupplierQuotesList}  1
    ${SupplierQuotesName2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SupplierQuotesList}  2 
    Set Suite Variable    ${SupplierQuotesName1}
    Set Suite Variable    ${SupplierQuotesName2}
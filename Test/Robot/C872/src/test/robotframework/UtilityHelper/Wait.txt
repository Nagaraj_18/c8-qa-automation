*** Settings ***
Library           Selenium2Library	timeout=90.0	run_on_failure=Capture Page Screenshot
Library           ../UtilityHelper/ExtendedFunctions.py
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_SetupPage.txt

*** Variables ***
${Loc_Release}    xpath=//span[text()='${Release}']
${Loc_HomeIcon}  css=[data-csi-automation='plugin-User-HeaderToolbar-PageHome']
${Loc_LoaderIcon}    xpath=//div[@id='splash']/img[@src='Images/logo_loader.gif']

*** Keywords ***

#increasing the timeout time to support execution on latest ff and chrome browsers
#Below keyword waits until the web page completely loads. 
#This waits upto 120 seconds. 
DOMready    
    Wait For Condition  return document.readyState=='complete';  timeout=180
    Run Keyword And Ignore Error  Wait For Release Info

#Below keyword waits until all the tabs of the current page loads.
#This waits upto 120 seconds. 
DOMready-TabLoads
	${PageLoadStatus} =  Run Keyword And Return Status  Wait For Condition  return document.getElementById('pageBottom').children[0].children[1].getAttribute('style')=='';  timeout=240
    Run Keyword If  '${PageLoadStatus}'=='False'  Message to User in the bottom of the Page 
    Run Keyword And Ignore Error  Wait For Release Info
Message to User in the bottom of the Page    
    Wait For Condition  return document.getElementById('pageBottom').children[0].children[1].getAttribute('style')=='display: none;';
    
#This Keyword waits till the popup appears.
DOMready-PopUpAppears
 	#Wait For Condition  return document.getElementsByClassName('csiDialog dijitDialog dijitDialogError dijitError').length=='1';
	Sleep  3
	Wait Until Page Contains Element  css=.dijitDialogTitleBar

PopupWait
    [Arguments]  ${SpecifcClass}
    Wait Until Page Contains Element  //div[@class='dijitDialogTitleBar']//following::div[contains(@class,'csi-view-${SpecifcClass}')]
#Use this within the PopUp after Save and New 
#(or) any other possible/Similar scenarios.  
DOMready-WithInPopUp	
	 :FOR  ${i}  IN RANGE  0  240    
 	 \  Log  ${i}
  	 \  Sleep  .5
	 \  ${Check} =  Execute Javascript  return document.getElementsByClassName('csiActions ')[0].hasAttribute('disabled'); 
	 \  Run Keyword If  str(${Check}) == 'False'  Exit For Loop
	
#Use this for the pop up to close. This will wait until
#the newly created data gets displayed in the application.  
DOMready-PopUpCloses
	#Wait For Condition  return document.getElementsByClassName('viewDialog csiDialog dijitDialog').length=='0';  1200
	Wait Until Page Does Not Contain Element  css=.dijitDialogTitleBar  timeout=180
	DOMreadyByWaitTime  3
	
	
DOMready-TableCellEditableTextArea-Appears
	Capture Page Screenshot
	Wait For Condition  return document.getElementsByClassName('dijitReset dijitTextBox dijitTextArea dijitExpandingTextArea').length=='1';  120
	
#Below keyword waits for the given number 
#of seconds which is passed as parameter 
#to it. 
DOMreadyByWaitTime	 
	[Arguments]   ${Seconds}
	Sleep  ${Seconds}

#Below keyword can be used with Javascript Executors.
#It waits for 2 seconds by default.	
DOMreadyJSWait	 	
	Sleep  2	
	
#Below keyword reloads the web page.	
Reload
  Reload Page
  DOMready
  Sleep  4s
  Run Keyword And Return Status  Wait For Loader Icon To Disappear
  Run Keyword And Return Status  Wait For Element to be Visible  ${Loc_HomeIcon}
  Run Keyword And Return Status  Wait For Element to be Visible  ${Loc_Release}
  ${SessionStatus} =  Run Keyword And Return Status  Session Check
  Run Keyword If  '${SessionStatus}' == 'True'  Login After Session Expiry
  DOMready-TabLoads  
  
Login After Session Expiry 
    PO_LoginPage.Input UserName    Administrator
    PO_LoginPage.Input Password    Centric8
    PO_LoginPage.Submit Credentials
    Wait Until Page Contains Element   ${Loc_Release}
    
Session Check 
    Page Should Contain Element  //div[text()='Your session has expired. Please login again.']
    
#Below keyword waits until the given element loads into the DOM.
Wait For Element to be Visible
	[Arguments]   ${Element_Locator}
	Wait Until Page Contains Element  ${Element_Locator}  timeout=120

Wait for Update Configuration PopUp   
   DOMreadyByWaitTime    5    
   #${ClickStatus} =  Run Keyword And Return Status  Click Element From List  css=Input[name='headerSearchText']  -1
   ${ClickStatus} =  Run Keyword And Return Status  Click Element From List  css=Input[name='UpdateConfiguration']  -1
   Run Keyword If  '${ClickStatus}' == 'False'  Close Popup
   ##Capture Page Screenshot
   ${Value} =  Execute JavaScript  return document.getElementById('pageBottom').textContent
   ${Status} =  stringMatch  ${Value}
   ${Status1} =  Run Keyword And Return Status  Should Be Equal  ${Status}  True
   DOMreadyByWaitTime    2
   ##Capture Page Screenshot  
   [return]  ${Status1}   

Close Popup
    ${AlertStatus} =  Run Keyword And Return Status  Handle Alert  action=ACCEPT
    Run Keyword If  '${AlertStatus}' == 'True'  Log  Handled Alert.  console=True
      

#Keyword will wait for Loading icon to disappear 
Wait For Loader Icon To Appear  
  Wait Until Element Is Not Visible  ${Loc_LoaderIcon}  timeout=2m 

#Keyword will wait for Loading icon to disappear    
Wait For Loader Icon To Disappear  
  Wait Until Element Is Not Visible  ${Loc_LoaderIcon}  timeout=4m    

# Keyword to Wait until Loading Details in the Bottom of the Page
Wait For Page Load
  Wait Until Page Contains Element   xpath=//span[text()='Loading...']
  Wait Until Page Contains Element   ${Loc_Release}
  
Wait For Save Icon To Appear 
  Run Keyword And Ignore Error  Wait Until Element Is Visible  css=.dirty   
  
Wait For Save Icon To Disappear
  Wait Until Element Is Not Visible  css=.dirty  
  
Wait For Release Info
  ${Element}=  Required Element From List  ${Loc_Release}  -1
  Wait Until Element Is Visible  ${Element}
  DOMreadyByWaitTime  3
  
Wait For Element And Click
    [Arguments]  ${Element}
    Wait Until Page Contains Element  ${Element}
    Click Element  ${Element}
    DOMreadyByWaitTime  3   
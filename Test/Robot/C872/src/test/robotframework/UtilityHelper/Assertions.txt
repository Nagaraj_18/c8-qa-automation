
*** Keywords ***

#Checks for the page's title.
Assert the given Title of the Page
    [Arguments]   ${Title}
    Title Should Be  ${Title}   
     
#Checks for the given element to be present in the DOM    
Assert for the given element
    [Arguments]   ${Element}
    Sleep  3
    ${Status} =	Run Keyword And Return Status	Check Element Should Be Visible  ${Element}
    Log  ${Status}  console=True

Verify the Element 
    [Arguments]   ${Element}  ${MSG}
    ${Status} =	Run Keyword And Return Status	Check Element Should Be Visible  ${Element}
    Log  ${MSG} : ${Status}  console=True

Verify the Tab Presence 
    [Arguments]   ${Element}  ${MSG}
    ${Status} =	Run Keyword And Return Status	Page Should contain Element  css=[data-csi-tab='${Element}']
    Log  ${MSG} : ${Status}  console=True    
    
Assert for the Attributes in dialog box
    [Arguments]   ${Element}
    ${Status} =	Run Keyword And Return Status	Check Element Should Be Visible  xpath=//th[@class='csiFormViewLabel' and text()=${Element}]
    Log  ${Status}   

#Checks if the given element is present in the dropdown
Assert for the Element in Table DDL
 	[Arguments]	${DDL_Element}
 	Element Should Be Visible  //div[@class='dijitReset dijitMenu dijitComboBoxMenu']/div[@class='dijitReset dijitMenuItem' and contains(text(),'${DDL_Element}')]
 	
Assert the Table DDL Element from list 
 	[Arguments]	@{DDL_List}
 	:FOR  ${list_Value}  IN  @{DDL_List}
    \    
    # \  ${Node_Name}  Selenium2Library.Get Text  ${list_Value}
    \  ${Status} =	Run Keyword And Return Status	Element Should Be Visible  //div[@class='dijitReset dijitMenu dijitComboBoxMenu']/div[@class='dijitReset dijitMenuItem' and contains(text(),'${list_Value}')] 	
    \  Log  ${list_Value} - ${Status}  console=True
    \  
    Log  Assertion completed.
 	 

Assert for the Element in Actions Link
 	[Arguments]	${Element}
 	Element Should Be Visible  //td[@class='dijitReset dijitMenuItemLabel' and (text()='${Element}')]


#Checks if the given tab is present in the page 
Assert the given tab 	
	[Arguments]  ${Tab_Locator}
	Element Should Be Visible  //span[text()='${Tab_Locator}']
	
#Checks if the given element is present in the pop up dialog 
Assert the Element in popup dialog
	[Arguments]  ${popupvalue}
	${Status} =	Run Keyword And Return Status	Element Should Be Visible  //div[contains(@class,'dijitDialog')]//th[text()='${popupvalue}']
    Log  ${Status}
    
Verify if the Element is not present in popup dialog
	[Arguments]  ${popupvalue}
	${Status} =	Run Keyword And Return Status	//td[@data-csi-heading='Node Name::0' and text()='${popupvalue}']
    Log  ${Status}
    
Assert the links in table view 
	[Arguments]  ${ReferenceElement}  ${LinkTitle}
	${Status} =	Run Keyword And Return Status	Element Should Be Visible  //td[a[text()='${ReferenceElement}']]/following-sibling::td/div/a[@title='${LinkTitle}']
    Log  ${Status}
    
Assert the links in TDS Action column	
	[Arguments]  ${LinkTitle}
	Element Should Be Visible  //td[@class='attrRef' and string-length() > 1]/following-sibling::td/div/a[@title='${LinkTitle}']

Verify if the icon is not present in Specific placement 
	[Arguments]  ${PlacementRef}  ${LinkTitle}
	Element Should Not Be Visible  //td[@class='${PlacementRef}' and string-length() = 1]/following-sibling::td/div/a[@title='${LinkTitle}']
    
Assert the Column Heading
    [Arguments]	 ${Element}
    Element Should Be Visible  xpath=//span[text()='${Element}']


Assert for the Field Names
    [Arguments]	 ${Element}    
    Element Should Be Visible  xpath=//th[text()='${Element}']

Assert for the Attributes
    [Arguments]	 ${Element}    
    Element Should Be Visible  xpath=//th[@class='csiFormViewLabel' and text()='${Element}']
    
Assert for Mandatory Field
   [Arguments]  ${Element}
   Element Should Be Visible  xpath=//th[@class='csiFormViewLabel' and text()='${Element}']//following::div[@class='iconRequired csiActionIcon']

Assert for the Active Tab
    [Arguments]	 ${Element}    
    Element Should Be Visible  ${Element}
    
Assert Flowchart
    [Arguments]	 ${Element}
    Element Should Be Visible  css=.stateWrapper.'${Element}'.stateOther

Assert for Action Icons in the Table View 
  [Arguments]  ${ReferenceItem}  ${ActName}
  Assert for the given element	//*[@class='attrString iconEditable firstColumn']/a[text()='${ReferenceItem}']/following::td/div/a[@data-csi-act='${ActName}']

Assert for the Node in Table 
  [Arguments]  ${NodeName}
  Assert for the given element	//td[@data-csi-act='Node Name::0']/a[text()='${NodeName}']

Assert for the Link in Actions Column
  [Arguments]  ${NodeName}  ${Title}
   Assert for the given Element  //*[@class='browse' and text()='${NodeName}']//following::td[2]/div/span[@title='${Title}']
  
Assert MaterialType in Table 
  [Arguments]  ${NodeName}
  Assert for the given element	//td[@data-csi-heading='Node Name::0'and text()='${NodeName}']
    
Assert for Placements Link in Actions Column
  [Arguments]  ${NodeName}  ${Title}
   Assert for the given Element  //*[text()='${NodeName}']//following::td[17]/div/span[@title='${Title}']
  
Assert Popup Page Button
  [Arguments]  ${ButtonName}
  Assert for the given element	 //span[@title='${ButtonName}'] 
  
Assert Popup Action Button
  [Arguments]  ${ButtonName}
  Assert for the given element	 //span[@data-csi-act='${ButtonName}']  

Assert the Value Updated in Reference to NodeName
  [Arguments]	${NodeValue}  ${heading}  ${ValidationValue}
  Element Should Be Visible  //td[@data-csi-act='Node Name::0']/a[text()='${NodeValue}']/following::td[@data-csi-heading='${heading}' and text()='${ValidationValue}'] 
Assert NodeName
  [Arguments]  ${NodeText}
  Assert for the given element	 //*[@class='browse' and text()='${NodeText}']

Assert Element With Index  
  [Arguments]    ${ListDuplicateLastElementtLoc}	${Index}
  ${List} =  Get Web Elements  ${ListDuplicateLastElementtLoc}
  ${ReqEle} =  Get From List  ${List}  ${Index}     
  Element Should Be Visible  ${ReqEle} 
Assert for the Element In Section
    [Arguments]  ${LabelName}
  Assert for the given element	 //div[@class='csiHeadingCellLabel' and text()='${LabelName}']
  
Assert the given list
    [Arguments]  @{list}
    :FOR  ${list_Value}  IN  @{list}
    \    
    \  ${Node_Name}  Selenium2Library.Get Text  ${list_Value}
    \  ${Status} =	Run Keyword And Return Status	Check Element Should Be Visible  ${list_Value}
    \  Log  ${Node_Name} - ${Status}  console=True
    \  
    Log  Assertion completed.
    
Assert Popup Page Buttons
    @{popup_Buttons_list}  Create List  ${Loc_SaveButton}  ${Loc_Save&NewButton}  ${Loc_Save&GoButton}  ${Loc_CancelButton}
  
    :FOR  ${buttons}  IN  ${popup_Buttons_list}
    \    
    \  Assert for the given element  ${buttons}
    \ 
    Log  Popup buttons assertion completed.
    
Assert for the given element not present
    [Arguments]   ${Element}  ${MSG}   
    ${Status} =	Run Keyword And Return Status	Element Should Not Be Visible  ${Element}
    Log  ${MSG} : ${Status}  console=True  
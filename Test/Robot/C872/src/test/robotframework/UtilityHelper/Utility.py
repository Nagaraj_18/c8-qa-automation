#Wait comment for Stale Element Exception
import selenium.webdriver.support.ui

def waitForElement(Element):
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Element)))
    return Element

# def _wait_until_search_field_appears(self.webdriver, max_wait=5, Element):
#         """Waits until the search input field can be located for the current search engine
# 
#         Args:
#             max_wait: How long to wait maximally before returning False.
# 
#         Returns: False if the search input field could not be located within the time
#                 or the handle to the search input field.
#         """
# 
#         def find_visible_search_input(driver):
#             input_field = driver.find_element(Element)
#             return input_field
# 
#         try:
#             search_input = WebDriverWait(self.webdriver, max_wait).until(find_visible_search_input)
#             return search_input
#         except TimeoutException as e:
#             logger.error('{}: TimeoutException waiting for search input field: {}'.format(self.name, e))
#             return False 
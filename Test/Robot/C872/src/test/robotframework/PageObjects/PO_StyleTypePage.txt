*** Settings ***

Resource          ../Locators/Loc_SetupPage.txt
Resource		  ../UtilityHelper/SuiteSetUp.txt
Resource          ../Locators/Loc_StyleTypePage.txt

*** Keywords ***

Wait for New Style Type option
    Wait Until Element Is Visible  ${Loc_NewStyleType}

Click on New Style Type option
	Click Element  ${Loc_NewStyleType}

Get Style type name string
	#${Style_type_String} =  Generate Random String  3  [UPPER][NUMBERS]
    #Log  ${Style_type_String}
    #${Style_type} =  Catenate  SType${Style_type_String}
    [return]  ${Style_type}

Enter the Style Type in the Pop up window
    [Arguments]    ${Data_StyleTypeName}
    Input Text    ${Loc_NewStyleTypeTextBox}   ${Data_StyleTypeName}

Click on Style Save Button
	Click Element  ${Loc_Save_Button}
	Run KeyWord And Return Status  Wait Until Element Is Not Visible  //span[@class='dijitDialogTitle' and text()='New Style Type']
    Wait Until Element Is Visible  //span[text()='Style Type created successfully.']
	#Element Should Be Visible  css=[data-csi-automation='plugin-BusinessObject-StyleTypeAttributes-views']
    
	
Check if the Style Name already Exist 
	[Arguments]    ${StyleName}
	${StyleTypeCount} =  Get Element Count  ${Loc_StyleTypeCount}
	${Check} =  Set Variable  False	
	:FOR  ${i}  IN RANGE  0  ${StyleTypeCount}
		\  Log  ${i}
		\  ${Path} =  Required Element From List  //span[contains(@data-csi-automation,'plugin-BusinessObject-StyleTypeAttributes-C')]//ancestor::td//preceding-sibling::td[@data-csi-heading='Node Name::0']  ${i}
		\  ${TextString} =  Selenium2Library.Get Text  ${Path} 
		\  ${Check} =  Evaluate  '${StyleName}' == '${TextString}'
		\  Log  str(${Check})
		\  Run Keyword If  str(${Check}) == 'True'  Exit For Loop
	[return]  	${Check}
	
Check if the Style Name already Exist and not Updated 
	[Arguments]    ${StyleName}
	${StyleTypeCount} =  Get Element Count  //tr[@class='pendingUpdate']
	${Check} =  Set Variable  False
	:FOR  ${i}  IN RANGE  0  ${StyleTypeCount}
		\  Log  ${i}
		\  ${TextString} =  Execute Javascript  return document.getElementsByClassName('pendingUpdate')[${i}].getElementsByTagName('td')[0].textContent;
		\  ${Check} =  Evaluate  '${StyleName}' == '${TextString}'
		\  Log  str(${Check})
		\  Run Keyword If  str(${Check}) == 'True'  Exit For Loop
	[return]  	${Check}		
	
Select the Active Check box to configure the New Style
	[Arguments]    ${StyleType}
	${Element} =  Set Variable  //td[text()='${StyleType}']/following-sibling::td[@data-csi-heading='Available:Child:Config:0']/div//input
	${ElementList} =  Get WebElements  //td[text()='${StyleType}']/following-sibling::td[@data-csi-heading='Available:Child:Config:0']/div//input
	${Element1} =  Get From List  ${ElementList}  0
	#Execute JavaScript  window.document.evaluate("${Element}",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Wait.DOMreadyByWaitTime  4
	Click Element  ${Element1}
	
Select the Published Check box to configure the New Style
	[Arguments]    ${StyleType}
	${ElementList} =  Get WebElements  //td[text()='${StyleType}']/following-sibling::td[@data-csi-heading='Published::0']//div//input
	${Element} =  Get From List  ${ElementList}  0
	Click Element  ${Element}
Select the Has Color Check box to configure the New Style
	[Arguments]    ${StyleType}
	${Element} =  Set Variable  //td[text()='${StyleType}']/following-sibling::td[@data-csi-heading='HasColor:Child:Config:0']//div//input
	${ElementList} =  Get WebElements   //td[text()='${StyleType}']/following-sibling::td[@data-csi-heading='HasColor:Child:Config:0']//div//input
	${Element1} =  Get From List  ${ElementList}  0
	#Execute JavaScript  window.document.evaluate("${Element}",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element  ${Element1}
Select the Has Size Check box to configure the New Style
	[Arguments]    ${StyleType}
	${Element} =  Set Variable  //td[text()='${StyleType}']/following-sibling::td[@data-csi-heading='HasSize:Child:Config:0']//div//input
	${ElementList} =  Get WebElements   //td[text()='${StyleType}']/following-sibling::td[@data-csi-heading='HasSize:Child:Config:0']//div//input
	${Element1} =  Get From List  ${ElementList}  0
	#Execute JavaScript  window.document.evaluate("${Element}",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element  ${Element1}
Select the Is Assortment Check box to configure the New Style
	 [Arguments]    ${StyleType}
	${Element} =  Set Variable  //td[text()='${StyleType}']/following-sibling::td[@data-csi-heading='IsAssortment:Child:Config:0']//div//input
	${ElementList} =  Get WebElements   //td[text()='${StyleType}']/following-sibling::td[@data-csi-heading='IsAssortment:Child:Config:0']//div//input
	${Element1} =  Get From List  ${ElementList}  0
	#Execute JavaScript  window.document.evaluate("${Element}",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element  ${Element1}
Select the In Assortment Check box to configure the New Style
	[Arguments]    ${StyleType}
	${Element} =  Set Variable  //td[text()='${StyleType}']/following-sibling::td[@data-csi-heading='InAssortment:Child:Config:0']//div//input
	${ElementList} =  Get WebElements   //td[text()='${StyleType}']/following-sibling::td[@data-csi-heading='InAssortment:Child:Config:0']//div//input
	${Element1} =  Get From List  ${ElementList}  0
	#Execute JavaScript  window.document.evaluate("${Element}",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element  ${Element1}
Select the AC SKU Check box to configure the New Style
	[Arguments]    ${StyleType}
	${Element} =  Set Variable  //td[text()='${StyleType}']/following-sibling::td[@data-csi-heading='AutoCreateSKU:Child:Config:0']//div//input
	${ElementList} =  Get WebElements  //td[text()='${StyleType}']/following-sibling::td[@data-csi-heading='AutoCreateSKU:Child:Config:0']//div//input
	${Element1} =  Get From List  ${ElementList}  0
	#Execute JavaScript  window.document.evaluate("${Element}",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element  ${Element1}
Select the Validate MCM Check box to configure the New Style
	[Arguments]    ${StyleType}
	${Element} =  Set Variable  //td[text()='${StyleType}']/following::td[@data-csi-heading='ValidateMCMaterialColors:Child:Config:0']//div//input
	${ElementList} =  Get WebElements   //td[text()='${StyleType}']/following::td[@data-csi-heading='ValidateMCMaterialColors:Child:Config:0']//div//input
	${Element1} =  Get From List  ${ElementList}  0
	#Execute JavaScript  window.document.evaluate("${Element}",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element  ${Element1}
Select the Brands Check box to configure the New Style
	[Arguments]    ${StyleType}
	${Element} =  Set Variable  //td[text()='${StyleType}']/following-sibling::td[@data-csi-heading-vk='ProductAttributes/Brands']//div//input  
	${ElementList} =  Get WebElements  //td[text()='${StyleType}']/following-sibling::td[@data-csi-heading-vk='ProductAttributes/Brands']//div//input
	${Element1} =  Get From List  ${ElementList}  0
	#Execute JavaScript  window.document.evaluate("${Element}",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element  ${Element1}
Select the Features Check box to configure the New Style
	[Arguments]    ${StyleType}
	${Element} =  Set Variable  //td[text()='${StyleType}']/following-sibling::td[@data-csi-heading-vk='ProductAttributes/Features']//div//input
	${ElementList} =  Get WebElements  //td[text()='${StyleType}']/following-sibling::td[@data-csi-heading-vk='ProductAttributes/Features']//div//input
	${Element1} =  Get From List  ${ElementList}  0
	#Execute JavaScript  window.document.evaluate("${Element}",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element  ${Element1}
Select the Functions Check box to configure the New Style
	[Arguments]    ${StyleType}
	${Element} =  Set Variable  //td[text()='${StyleType}']/following-sibling::td[@data-csi-heading-vk='ProductAttributes/Functions']/div//input
	${ElementList} =  Get WebElements  //td[text()='${StyleType}']/following-sibling::td[@data-csi-heading-vk='ProductAttributes/Functions']/div//input
	${Element1} =  Get From List  ${ElementList}  0
	#Execute JavaScript  window.document.evaluate("${Element}",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element  ${Element1}
Select the Last Check box to configure the New Style
	[Arguments]    ${StyleType}
	${Element} =  Set Variable  //td[text()='${StyleType}']/following-sibling::td[@data-csi-heading='ReferenceDefaultImageOnColorwayCreate:Child:Config:0']/div//input
	${ElementList} =  Get WebElements  //td[text()='${StyleType}']/following-sibling::td[@data-csi-heading='ReferenceDefaultImageOnColorwayCreate:Child:Config:0']/div//input
	${Element1} =  Get From List  ${ElementList}  0
	#Execute JavaScript  window.document.evaluate("${Element}",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element  ${Element1}
Click on Refresh Icon
	Click Element  css=.iconRefresh
	DOMreadyJSWait
Select the Artwork Check box to configure the New Style
	[Arguments]    ${StyleType}
	${Element} =  Set Variable  //td[text()='${StyleType}']/following::td[3]/div[1]/input
	${ElementList} =  Get WebElements  //td[text()='${StyleType}']/following::td[3]/div[1]/input
	${Element1} =  Get From List  ${ElementList}  1
	#Execute JavaScript  window.document.evaluate("${Element}",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element  ${Element1}
Select the Allow Color Spec to configure the New Style
	[Arguments]    ${StyleType}
	${Element} =  Set Variable  //td[text()='${StyleType}']/following-sibling::td[@data-csi-heading='AllowCreateColorSpecOnColorway:Child:Config:0']/div//input
	${ElementList} =  Get WebElements   //td[text()='${StyleType}']/following-sibling::td[@data-csi-heading='AllowCreateColorSpecOnColorway:Child:Config:0']/div//input
	${Element1} =  Get From List  ${ElementList}  0
	#Execute JavaScript  window.document.evaluate("${Element}",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Set Focus To Element   ${Element1}
	Click Element  ${Element1}	
Select the Style BOM Check box to configure the New Style
	[Arguments]    ${StyleType}
	${Element} =  Set Variable  //td[text()='${StyleType}']/following::td[@data-csi-heading='TDSMap:Child:Config:0']/div[1]/input
	${ElementList} =  Get WebElements    //td[text()='${StyleType}']/following::td[@data-csi-heading='TDSMap:Child:Config:0']/div[1]/input
	Get Length  ${ElementList}
	${Element1} =  Get From List  ${ElementList}  1
	#Execute JavaScript  window.document.evaluate("${Element}",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element  ${Element1}
	#Reload
	#Wait Until Element Is Enabled  css=[data-csi-automation='plugin-Site-StyleTDS-ToolbarNewActions'] div.csi-toolbar-btn-icon-ToolbarNewActions
	#Click Element From List  //tr[@class='pendingUpdate']//td[text()='${StyleType}']/following::td[@data-csi-heading='TDSMap:Child:Config:0']/div[1]/input  0
	
Select the Image Data Sheet Check box to configure the New Style
	[Arguments]    ${StyleType}
	${Element} =  Set Variable  //td[text()='${StyleType}']/following::td[5]/div[1]/input
	${ElementList} =  Get WebElements  //td[text()='${StyleType}']/following::td[5]/div[1]/input
	Get Length  ${ElementList}
	${Element1} =  Get From List   ${ElementList}   1
	#Execute JavaScript  window.document.evaluate("${Element}",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Select Checkbox   ${Element1}
	
Select the Property data Sheet Check box to configure the New Style
	[Arguments]    ${StyleType}
	${Element} =  Set Variable  //td[text()='${StyleType}']/following::td[6]/div[1]/input  
	${ElementList} =  Get WebElements  //td[text()='${StyleType}']/following::td[6]/div[1]/input
	${Element1} =  Get From List  ${ElementList}  1
	#Execute JavaScript  window.document.evaluate("${Element}",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element  ${Element1}
	
Select the Style review Check box to configure the New Style
	[Arguments]    ${StyleType}
	${Element} =  Set Variable  //td[text()='${StyleType}']/following::td[7]/div[1]/input  
	${ElementList} =  Get WebElements  //td[text()='${StyleType}']/following::td[7]/div[1]/input
	${Element1} =  Get From List  ${ElementList}  1
	#Execute JavaScript  window.document.evaluate("${Element}",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element  ${Element1}
	
Select the Routing Check box to configure the New Style
	[Arguments]    ${StyleType}
	${Element} =  Set Variable  //td[text()='${StyleType}']/following::td[8]/div[1]/input
	${ElementList} =  Get WebElements  //td[text()='${StyleType}']/following::td[8]/div[1]/input
	${Element1} =  Get From List  ${ElementList}  1
	#Execute JavaScript  window.document.evaluate("${Element}",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element  ${Element1}
	
Select the Size Chart Check box to configure the New Style
	[Arguments]    ${StyleType}
	${Element} =  Set Variable  //td[text()='${StyleType}']/following::td[9]/div[1]/input  
	${ElementList} =  Get WebElements  //td[text()='${StyleType}']/following::td[9]/div[1]/input
	${Element1} =  Get From List  ${ElementList}  1
	#Execute JavaScript  window.document.evaluate("${Element}",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element  ${Element1}
	
Select the Size Label data Sheet Check box to configure the New Style
	[Arguments]    ${StyleType}
	${Element} =  Set Variable  //td[text()='${StyleType}']/following::td[10]/div[1]/input
    ${ElementList} =  Get WebElements  //td[text()='${StyleType}']/following::td[10]/div[1]/input
	${Element1} =  Get From List  ${ElementList}  1
	#Execute JavaScript  window.document.evaluate("${Element}",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element  ${Element1}
	
Select the Spec Data Sheet Check box to configure the New Style
	[Arguments]    ${StyleType}
	${Element} =  Set Variable  //td[text()='${StyleType}']/following::td[11]/div[1]/input  
	${ElementList} =  Get WebElements  //td[text()='${StyleType}']/following::td[11]/div[1]/input
	${Element1} =  Get From List  ${ElementList}  1
	#Execute JavaScript  window.document.evaluate("${Element}",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element  ${Element1}

Select the Care & Composition check box to configure the New Style
    [Arguments]    ${StyleType}
	${Element} =  Set Variable  //td[text()='${StyleType}']/following::td[12]/div[1]/input  
	${ElementList} =  Get WebElements  //td[text()='${StyleType}']/following::td[12]/div[1]/input
	${Element1} =  Get From List  ${ElementList}  1
	#Execute JavaScript  window.document.evaluate("${Element}",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element  ${Element1}	
	
Select the Price List Check box to configure the New Style
    [Arguments]    ${StyleType}
	${Element} =  Set Variable  //td[text()='${StyleType}']/following::td[13]/div[1]/input  
	${ElementList} =  Get WebElements  //td[text()='${StyleType}']/following::td[13]/div[1]/input
	${Element1} =  Get From List  ${ElementList}  1
	#Execute JavaScript  window.document.evaluate("${Element}",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element  ${Element1}
	
Select the Enable Style Supplier Attributes Check box to configure the New Style
 
	[Arguments]    ${StyleType}
	${Element} =  Set Variable  //td[text()='${StyleType}']/following::td[@data-csi-heading='EnableStyleSupplierAttributes:Child:Config:0']/div[1]/input  
	${ElementList} =  Get WebElements  //td[text()='${StyleType}']/following::td[@data-csi-heading='EnableStyleSupplierAttributes:Child:Config:0']/div[1]/input
	${Element1} =  Get From List  ${ElementList}  0
	#Execute JavaScript  window.document.evaluate("${Element}",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element  ${Element1}
	
Select the Enable SKU Source Business Object Check box to configure the New Style
 
	[Arguments]    ${StyleType}
	${Element} =  Set Variable  //td[text()='${StyleType}']/following::td[@data-csi-heading='EnableSKUSources:Child:Config:0']/div[1]/input  
	${ElementList} =  Get WebElements  //td[text()='${StyleType}']/following::td[@data-csi-heading='EnableSKUSources:Child:Config:0']/div[1]/input
	${Element1} =  Get From List  ${ElementList}  0
	#Execute JavaScript  window.document.evaluate("${Element}",document,null,XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
	Click Element  ${Element1}	
	
*** Settings ***

Resource          ../Locators/Loc_HomePage.txt
Resource          ../UtilityHelper/SuiteSetUp.txt

*** Keywords ***

Click On Style tab    
	Click Element   ${HomeStylesTab}
	
Click On Material tab    
	Click Element   ${Loc_MaterialTab}
	
Click On Specification tab	
	Click Element   ${Loc_SpecificationTab}

Click On Libraries tab	
	Click Element	${LibrariesTab}
	
Click On Material's BOM tab	
	Click Element  ${Loc_MaterialBOMTab}
	

Wait for New Material option
    Wait Until Element Is Visible  ${Loc_NewMaterial}

Click on New Material option
	Click Element  ${Loc_NewMaterial}

Click on Placement tab
	Click Element  ${Loc_Home-Material-Placement}
	
Click on Styles tab    
	Click Element   ${Loc_StylesTab}
	
Click on Documents tab
    Click Element	${Loc_DocumentsTab}
		
Enter the Color in the Text Box
    [Arguments]    ${Data_ColorName}
    Execute Javascript   document.getElementsByClassName('dijitReset dijitTextBox dijitTextArea dijitExpandingTextArea')[0].value='${Data_ColorName}';
    
Click on Material Type DDL
	${DDl_List} =  Get WebElements  css=[data-csi-automation='field-Material-Form-ProductType']>div>input.dijitInputField	
	#${DDl_List} =  Get WebElements  css=[data-csi-automation='field-Material-Form-ProductType']>div>[role='presentation']
	${MaterialTypeDDL} =  Get From List  ${DDl_List}  -1
	Click Element  ${MaterialTypeDDL}
        
Select the Material Type for the Material

	[Arguments]    ${Data_MaterialTypeName}
	${MaterialTypeName} =  GetDataProviderColumnValue  ${Data_MaterialTypeName}
	Wait Until Element Is Visible  //*[@data-csi-automation='field-Material-Form-ProductType']//div[@class='dijitReset dijitMenuItem' and text()='${MaterialTypeName}']
	Click Element  //*[@data-csi-automation='field-Material-Form-ProductType']//div[@class='dijitReset dijitMenuItem' and text()='${MaterialTypeName}']
	
	
Enter the Material in the Pop up window       
	[Arguments]    ${Data_MaterialName}
    ${MaterialName} =  GetDataProviderColumnValue  ${Data_MaterialName}
    Input Text    ${Loc_PopUpTextBox}  ${MaterialName} 		

Click on Sourcing tab
	Click Element	${Loc_Home_SourcingTab}

Click on Refresh icon on simple table view	
	Click Element  //span[@class='dijitReset dijitInline dijitIcon csiActionIcon iconRefresh']
	
Click on Supplier Requests tab 
  Click Element  css=[data-csi-tab='Material-SupplierRequests']

Click on LzSupplier Requests tab 
  Click Element  //span[text()='Vendor Requests'] 

Click On Collection Management tab
  Click Element  ${Loc_CollectionManagementTab} 
  
Click On Theme Tab
  Click Element  ${Loc_HomeThemeTab}
  
Click On Shape And Theme Tab
  Click Element  ${Loc_ShapeAndThemeTab}
 
Click On Right Arrow Button To View Hidden Tabs
    [Arguments]    ${Index}
   ${MyEle} =  Required Element From List    ${Loc_Home_ScrollRightToViewOtherTabsButton}  ${Index}
   Wait Until Element Is Visible  ${MyEle}
   Click Element  ${MyEle}
   Wait Until Element Is Not Visible    ${Loc_Home_ScrollRightToViewOtherTabsButton} 
   
Click On Left Arrow Button To View Hidden Tabs
    [Arguments]    ${Index}
   ${MyEle} =  Required Element From List    ${Loc_Home_ScrollLeftToViewOtherTabsButton}  ${Index}
   Click Element  ${MyEle} 
   #${MyEle} =  Required Element From List    ${Loc_Home_ScrollLeftToViewOtherTabsButton}  ${Index}
   #Wait Until Element Is Visible  ${MyEle}
   #Click Element  ${MyEle}
   #Wait Until Element Is Not Visible    ${Loc_Home_ScrollLeftToViewOtherTabsButton} 
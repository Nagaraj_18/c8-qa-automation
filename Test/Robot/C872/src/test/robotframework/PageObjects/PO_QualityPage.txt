*** Settings ***
Resource          ../Locators/Loc_QualityPage.txt
Resource          ../Locators/Loc_NewCreationPopUpPage.txt
Resource  		  ../UtilityHelper/Data_Provider.txt
Resource  		  ../UtilityHelper/Wait.txt
Resource  		  ../UtilityHelper/SuiteSetUp.txt



*** Keywords ***

Click On Quality tab    
	Click Element   ${Loc_QualityTab}	
	
Click On Quality-Setup tab    
	Click Element   ${Loc_QualitySetupTab}	
	
Click On Quality-TestSpecs tab    
	Click Element	${Loc_TestSpecsTab}
	
Click on Revisions Actions DDL
   	Click Element  //*[@class='phaseView csiView']//span[@class='dijitReset dijitStretch dijitButtonContents dijitDownArrowButton']
   
Select from Revisions Actions DDL
	[Arguments]    ${State} 
	Click Element  //*[@class='dijitReset dijitMenuItemLabel' and text()='${State}']   	
	
Click On Quality-Properties tab    
	Click Element	${Loc_PropertiesTab}		
	
Click On Quality-Test Groups tab    
	Click Element	${Loc_TestGroupsTab}

Click On Docs&Comments tab    
	Click Element	${Loc_Docs&CommentsTab}		
	

*** Settings ***
Resource          ../Locators/Loc_SourcingPage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource		  ../UtilityHelper/Assertions.txt
Resource		  ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../Locators/Loc_SourcingSetupPage.txt

*** Keywords ***

Click on Setup tab
	Click Element	${Loc_Home-Sourcing-SetupTab}
Click on Country tab
	Click Element	${Loc_Home-Sourcing-Setup-CountryTab}
Click on Operation Group tab
	Click Element	${Loc_Home-Sourcing-Setup-OperationGroupTab}
Click on HTS Code tab	
	Click Element	${Loc_Home-Sourcing-Setup-HTSCodeTab}
Click on Shipping Port tab
	Click Element	${Loc_Home-Sourcing-Setup-Shipping Port}
Click on Capability tab 
	Click Element	${Loc_Home-Sourcing-Setup-Capability}
Click on Shipping Container tab
	Click Element	${Loc_Home-Sourcing-Setup-Shipping Container}
Click on Shipping Rate tab
	Click Element	${Loc_Home-Sourcing-Setup-Shipping Rate}
Click on Factory tab 
	Click Element	${Loc_Home-Sourcing-Factory}
Click on Supplier tab 
	Click Element	${Loc_Home-Sourcing-SupplierTab}
Click on Templates tab
	Click Element  ${Loc_Sourcing-Setup-Template}
Click on Supplier Quote Template tab on Supplier Quote Template
	Click Element  ${Loc_Home-SupplierQuoteTemplate-SupplierQuoteTemplate}
Click on Sub Routings tab
	Click Element  ${Loc_Home-Sourcing-Setup-Sub Routings}
Click on Factories tab 
	# loactor has been given explictly as this tab is specific to a node
	Click Element   //span[text()='Factories']
Click on Capabilities tab 
	# loactor has been given explictly as this tab is specific to a node
	Click Element   //span[text()='Capabilities']
Click on Discounts tab 
	Click Element   //span[text()='Discounts']
Click on Suppliers tab 
	# loactor has been given explictly as this tab is specific to a node
	Click Element   //span[text()='Suppliers']
Click on TDS tab 
	Click Element  css=[data-csi-tab='MaterialBOMRevision-TDS']  						    

Click on Routing TDS tab
    Click Element  css=[data-csi-tab='RoutingRevision-TDS']
     
Click on Cost Scenario tab
#Updated the locator with respect to style-supplierQuote Cost Scenario, not sure if it works on other areas 
	Click Element  css=[data-csi-tab='SupplierItemRevision-CostScenario']

Click on Create Cost Scenario Button 
  Click Element 	css=[data-csi-act='CreateCostScenario'] span.dijitNoIcon
  
Click on Routing tab
	Click Element  ${Loc_Material-Specification-Routing}
	
Click on Summary tab
	Click Element  ${Loc_Material-Specification-Summary}
	
# to delete after confirmation, has been moved to PO_Homepage 
Click on Placement tab
	Click Element  ${Loc_Home-Material-Placement}
	
Enter the Country value in Text Box
	[Arguments]    ${Data_CountryName}
    Input Text  ${Loc_Table_InputTextCountry}	${Data_CountryName}
        
Enter the Code value for Required Country
	[Arguments]	  ${ReqCountry}	 ${CountryCode}	
	DOMreadyByWaitTime  4
	#Wait Until Page Contains Element  //a[text()='${ReqCountry}']/../following-sibling::td[@data-csi-heading='Code::0']
	#Click Element  //a[text()='${ReqCountry}']/../following-sibling::td[@data-csi-heading='Code::0']
	Click Element From List  //a[text()='${ReqCountry}']//following::td[@data-csi-heading='Code::0']  0
	#Execute Javascript	var size=document.getElementsByClassName('tableView')[2].getElementsByClassName('attrString iconEditable firstColumn').length;for(i=0;i<size;i++){var TextString=document.getElementsByClassName('tableView')[2].getElementsByClassName('attrString iconEditable firstColumn')[i].getElementsByTagName('a')[0].text;if(TextString=='${ReqCountry}'){document.getElementsByClassName('tableView')[2].getElementsByClassName('attrString iconEditable firstColumn')[i].nextSibling.click()}}
	DOMreadyByWaitTime  2
	#Input Text  ${Loc_Table_InputText_COC}  ${CountryCode}
	Input Text  //input[@ class='dijitReset dijitInputInner']  ${CountryCode}

Enter the Region value for Required Country 
	[Arguments]	  ${ReqCountry}	 ${Region}
	Click Element	//a[text()='${ReqCountry}']/../following-sibling::td[@data-csi-act='Region::0']	
	DOMreadyByWaitTime  3
	#Input Text  ${Loc_Table_InputText_COR}  ${Region}
	Input Text  //input[@ class='dijitReset dijitInputInner']  ${Region}
	
Copy Country India
	Click Element 	${Loc_CopyCountry_India_Icon}	

Delete Country	
	Click Element 	${Loc_DeleteCountry_WithTextCopy_Icon}
	DOMreadyByWaitTime  3
	Click Element	${Loc_DeleteDialog_YesButton}
	
		
Enter the Operation Group value in Text Box
	[Arguments]  ${Data_OperationGroupName}  ${OperationGroup_Index}
	${OperationGroupNameCollection} =	 GetDataProviderColumnValue	${Data_OperationGroupName}
	${OperationGroupName} =	 DataProviderSplitterForMultipleValues	${OperationGroupNameCollection}  ${OperationGroup_Index}
	Input Text  ${Loc_Table_InputText_OG}	${OperationGroupName}

Enter the HTS Code value in Text Box
	[Arguments]    ${HTSCodeName1}
	Input Text  ${Loc_Table_InputText}	${HTSCodeName1}  
   	
Update Duty% for HTS Code
 	[Arguments]	 ${Data_HTSCode1Duty}	${ReqHTS}
 	${HTS1DutyValueString} =  Convert To String  ${Data_HTSCode1Duty}
	${WordAfterSplit} =  Split String  ${HTS1DutyValueString}  .
	${HTSDutyValue} =  Get From List  ${WordAfterSplit}  0
 	Click Element 	//td[@class='attrString iconEditable firstColumn'and text()='${ReqHTS}']/following-sibling::td[@data-csi-act='DutyPct::0']
 	DOMreadyByWaitTime  1
 	#Input Text   ${Loc_Table_Input_Number}	${HTSDutyValue}
 	Input Text  //input[@class='dijitReset dijitInputInner']  ${HTSDutyValue}
 	
Copy HTS Required Code
 	[Arguments]	 ${ReqdHTSToCopy}
 	Click Element 	//td[text()='${ReqdHTSToCopy}']/following::td[8]/div/span[@data-csi-act='Copy']
 	DOMreadyByWaitTime  3
 	
Enter the Shipping Port value in Text Box
	[Arguments]    ${ShippingPortName}
   	Input Text  ${Loc_Table_InputText}	${ShippingPortName}  	

Click on From Country of Required HTS Code
	[Arguments]	 ${HTS_FromCountry}
	Click Element	${HTS_FromCountry}
	
Click on To Country of Required HTS Code
 	[Arguments]	 ${HTS_ToCountry}
	Click Element	${HTS_ToCountry}
	
	
Click on Country of Required Shipping Port
	[Arguments]	 ${ShippingPort_Country}
	Click Element	//td[a[text()='${ShippingPort_Country}']]/following::td[1]		
		

Select From Country
	[Arguments] 	${Data_HTSCodeFromCountry}  ${Country_Index}
	${HTSCodeFromCountryCollection} =	GetDataProviderColumnValue	${Data_HTSCodeFromCountry}
	${HTSCodeFromCountry} =  DataProviderSplitterForMultipleValues  ${HTSCodeFromCountryCollection}  ${Country_Index}
 	Click Element	//div[@class='dijitReset dijitMenu dijitComboBoxMenu']/div[@class='dijitReset dijitMenuItem' and text()='${HTSCodeFromCountry}']

Select To Country
	[Arguments] 	${Data_HTSCodeToCountry}  ${Country_Index}
	${HTSCodeToCountryCollection} =	GetDataProviderColumnValue	${Data_HTSCodeToCountry}
	${HTSCodeToCountry} =  DataProviderSplitterForMultipleValues  ${HTSCodeToCountryCollection}  ${Country_Index}
 	Click Element	//div[@class='dijitReset dijitMenu dijitComboBoxMenu']/div[@class='dijitReset dijitMenuItem' and text()='${HTSCodeToCountry}']
	

Click on Country of the Required Shipping Port 
	[Arguments] 	${Data_ShippingPort1Country}
	${ShippingPortCountry} =	GetDataProviderColumnValue	${Data_ShippingPort1Country}
	Click Element	${ShippingPortCountry}
	DOMreadyByWaitTime  7
 
Select Country for Shipping Port
	[Arguments]  ${Data_ShippingPortCountry}  ${Country_Index}
	${ShippingPortCountryCollection} =	GetDataProviderColumnValue	${Data_ShippingPortCountry}
	${ShippingPortCountry} =  DataProviderSplitterForMultipleValues  ${ShippingPortCountryCollection}  ${Country_Index}
	DOMreadyByWaitTime  7
 	Click Element	//div[@data-csi-automation='edit-LibSourcing-ShippingPorts-Country:']/div[@class='dijitReset dijitMenuItem' and text()='${ShippingPortCountry}']
	DOMreadyByWaitTime  3
	
Enter the Capability value in Text Box
	[Arguments]    ${CapabilityName}
   	Input Text  ${Loc_Table_InputText}	${CapabilityName} 
	
Click on Capability For Required Capability
	[Arguments]	 ${CapabilityForLoc}
	Click Element	${CapabilityForLoc}
		
Select Capability Type
	[Arguments]	 ${Data_CapabilityFor}  ${CapabilityType_Index}
	${CapabilityForValueCollection} =	GetDataProviderColumnValue	${Data_CapabilityFor}
	${CapabilityForValue} =	DataProviderSplitterForMultipleValues	${CapabilityForValueCollection}  ${CapabilityType_Index}
	Click Element	//div[@class='dijitReset dijitMenu dijitComboBoxMenu']/div[@class='dijitReset dijitMenuItem' and text()='${CapabilityForValue}']
 	 	
Click on Operation Group of Required Capability
	[Arguments]	 ${CapabilityOperationGroupLoc}
	Click Element	${CapabilityOperationGroupLoc}
		
Select Operation Group
	[Arguments]  ${Data_CapabilityOperationGroupValue}  ${OperationGroup_Index}
	${OperationGroupValueCollection} =	GetDataProviderColumnValue	${Data_CapabilityOperationGroupValue}
	${OperationGroupValue} =	DataProviderSplitterForMultipleValues	${OperationGroupValueCollection}  ${OperationGroup_Index}
	Click Element	//div[@class='dijitReset dijitMenu dijitComboBoxMenu']/div[@class='dijitReset dijitMenuItem' and text()='${OperationGroupValue}']
 	
Copy Required Capability
 	[Arguments]	 ${CapabilityCopyIcon}
 	Click Element 	${CapabilityCopyIcon}
 		
Enter the Shipping Container value in Text Box
	[Arguments]    ${ShippingContainerName}
   	Input Text  ${Loc_Table_InputText}	${ShippingContainerName}
   	
Click on Max Volume of Required Shipping Container
	[Arguments]	 ${MaxVolumeLoc}
	Click Element	//td[@data-csi-act='Node Name::0' and text()='${MaxVolumeLoc}']/following-sibling::td[@data-csi-act='MaxVolumeCBM::0']

Update Max Volume
	[Arguments]	 ${DataMaxVolume}	
	${MaxVolumeString} =  Convert To String  ${DataMaxVolume}
	${WordAfterSplit} =  Split String  ${MaxVolumeString}  .
	${MaxVolumeValue} =  Get From List  ${WordAfterSplit}  0
	#Clear Element Text	${Loc_Table_Input_Number1}
	#Input Text   ${Loc_Table_Input_Number1}	 ${MaxVolumeValue}
	Input Text  //input[@class='dijitReset dijitInputInner']  ${MaxVolumeValue}
	
Click on Max Weight of Required Shipping Container
	[Arguments]	 ${MaxWeightLoc}
	Click Element	//td[@data-csi-act='Node Name::0' and text()='${MaxWeightLoc}']/following-sibling::td[@data-csi-act='MaxWeightLbs::0']
	


Update Max Weight
	[Arguments]	 ${MaxWeightValue}
	${MaxWeightString} =  Convert To String  ${MaxWeightValue}
	${WordAfterSplit} =  Split String  ${MaxWeightString}  .
	${MaxWeightValueUpdated} =  Get From List  ${WordAfterSplit}  0
	#Clear Element Text	//*[@data-csi-automation='edit-SourcingViews-ShippingContainers-MaxWeightLbs:']//div//input[@class='dijitReset dijitInputInner']
	Input Text  //input[@class='dijitReset dijitInputInner']	${MaxWeightValueUpdated}
	

Click on Packaging Efficiency of Required Shipping Container
	[Arguments]	 ${PackagingEfficencyLoc}
	Click Element	//td[@data-csi-act='Node Name::0' and text()='${PackagingEfficencyLoc}']/following-sibling::td[@data-csi-act='PackingEfficiency::0']
  
Update Packaging Efficiency
	[Arguments]	 ${PackagingEfficencyValue}
	${PackagingEfficencyString} =  Convert To String  ${PackagingEfficencyValue}
	${WordAfterSplit} =  Split String  ${PackagingEfficencyString}  .
	${PackagingEfficencyValueUpdated} =  Get From List  ${WordAfterSplit}  0
	#Clear Element Text	//*[@data-csi-automation='edit-SourcingViews-ShippingContainers-PackingEfficiency:']//div//input[@class='dijitReset dijitInputInner']
	#Input Text  //*[@data-csi-automation='edit-SourcingViews-ShippingContainers-PackingEfficiency:']//div//input[@class='dijitReset dijitInputInner']	${PackagingEfficencyValueUpdated}
	Input Text  //input[@class='dijitReset dijitInputInner']	${PackagingEfficencyValueUpdated}

Copy Required Shipping Container
	[Arguments]	 ${ShippingcontainertoCopy}
	Click Element 	//td[@class='attrString iconEditable firstColumn'and text()='${ShippingcontainertoCopy}']/following-sibling::td[5]/div/span[1]

Enter the Shipping Rate value in Dialog
	[Arguments]	 ${ShippingRateName}
	Input Text	${Loc_PopUpNameTextBox}	${ShippingRateName}	

Update Shipment Type in Dialog
	[Arguments]	 ${ShipmentTypeValue}
	Wait Until Element Is Visible  ${Loc_ShipmentType_dropdown}
	Click Element	${Loc_ShipmentType_dropdown}
	Wait Until Element Is Visible  //div[@data-csi-automation='field-ShippingRate-Form-ShipmentType']/div[@class='dijitReset dijitMenuItem' and text()='${ShipmentTypeValue}']
	Click Element	//div[@data-csi-automation='field-ShippingRate-Form-ShipmentType']/div[@class='dijitReset dijitMenuItem' and text()='${ShipmentTypeValue}']
	
Update Freight Rate value 
	[Arguments]	 ${DataFreightValue}
	${FreightValueString} =  Convert To String  ${DataFreightValue}
	${WordAfterSplit} =  Split String  ${FreightValueString}  .
	${FreightValue} =  Get From List  ${WordAfterSplit}  0
	Click Element  ${Loc_FrightRate_text}
	#Clear Element Text	${Loc_FrightRate_text}
	Input Text	${Loc_FrightRate_text}  ${FreightValue}

Update Port of Origin
 	[Arguments]	 ${forShippingRate}	${PortofOrigin}  ${Index}
 	${PortofOriginCollection} =  GetDataProviderColumnValue  ${PortofOrigin}
 	${PortofOriginValue} = 	DataProviderSplitterForMultipleValues	${PortofOriginCollection}  ${Index}	
 	Click Element 	//td[@data-csi-act='Node Name::0' and text()='${forShippingRate}']/following-sibling::td[@data-csi-act='PortOfOrigin::0']
	Wait.DOMreadyByWaitTime  3
	Click Element  //div[@data-csi-automation='edit-SourcingViews-ShippingRates-PortOfOrigin:']//div[@class='dijitReset dijitMenuItem' and text()='${PortofOriginValue}']

Update Port of Destination
 	[Arguments]  ${forShippingRate}  ${PortofDestination}  ${Index}
 	${PortofDestinationValueCollection} = 	GetDataProviderColumnValue	${PortofDestination}	
 	${PortofDestinationValue} = 	DataProviderSplitterForMultipleValues	${PortofDestinationValueCollection}  ${Index}
 	Click Element 	//td[@data-csi-act='Node Name::0' and text()='${forShippingRate}']/following-sibling::td[@data-csi-act='PortOfDestination::0']
	
	Wait.DOMreadyByWaitTime  3
	Click Element  //div[@data-csi-automation='edit-SourcingViews-ShippingRates-PortOfDestination:']//div[@class='dijitReset dijitMenuItem' and text()='${PortofDestinationValue}']
	
	
Update Container for ShipmentContainer
 	[Arguments]	 ${forShippingRate}  ${ShippingRateContainer}  ${Index}
 	${ShippingRateContainerValueCollection} = 	GetDataProviderColumnValue	${ShippingRateContainer}
 	${ShippingRateContainerValue} = 	DataProviderSplitterForMultipleValues	${ShippingRateContainerValueCollection}  ${Index}	
 	Click Element 	//td[@data-csi-act='Node Name::0'and text()='${forShippingRate}']/following-sibling::td[@data-csi-act='Container::0']
	Wait.DOMreadyByWaitTime  1
	Click Element  //div[@data-csi-automation='edit-SourcingViews-ShippingRates-Container:']/div[@class='dijitReset dijitMenuItem' and text()='${ShippingRateContainerValue}']
    
Enter the Factory value in Text Box
	[Arguments]	 ${FactoryNameValue}	
	Selenium2Library.Input Text	${Loc_Table_InputText}	${FactoryNameValue}

Update ID For Required Factory
	[Arguments]	 ${forFactory}	${ID}
 	Click Element 	//a[text()='${forFactory}']/../following-sibling::td[@data-csi-act='SupplierNumber::0']
	Wait.DOMreadyByWaitTime  1
	Selenium2Library.Input Text 	${Loc_Table_InputText}	${ID} 

Update State For Required Factory
	[Arguments]	 ${forFactory}	${State}
 	Click Element 	//a[text()='${forFactory}']/../following-sibling::td[@data-csi-act='State::0']
	Wait.DOMreadyByWaitTime  1
	Click Element		//div[@data-csi-automation='edit-LibSourcing-Factories-State:']/div[@class='dijitReset dijitMenuItem' and text()='${State}']
#Thiru	
Update Country of Origin For Required Factory
	[Arguments]  ${forFactory}	${CountryofOrigin}
	Click Element 	//a[text()='${forFactory}']/../following-sibling::td[@data-csi-act='Country::0']
	Wait.DOMreadyByWaitTime  1
	Click Element		//div[@data-csi-automation='edit-LibSourcing-Factories-Country:']/div[@class='dijitReset dijitMenuItem' and text()='${CountryofOrigin}']
	
Update Suppliers For Required Factory
	[Arguments]  ${forFactory}	${SupplierValue}  
 	Click Element 	//a[text()='${forFactory}']/../following-sibling::td[@data-csi-act='Suppliers::0']
	Wait.DOMreadyByWaitTime  1
	Wait Until Element Is Visible	//div[*]/label[text()='${SupplierValue}']
	Click Element		//div[*]/label[text()='${SupplierValue}']
	
Update Port of Origin For Required Factory
	[Arguments]  ${forFactory}	${PortofOrigin}
	Click Element 	//a[text()='${forFactory}']/../following-sibling::td[@data-csi-act='PortOfOrigin::0']
	Wait.DOMreadyByWaitTime  2
	Click Element		//div[@data-csi-automation='edit-LibSourcing-Factories-PortOfOrigin:']/div[@class='dijitReset dijitMenuItem' and text()='${PortofOrigin}']

Create Supplier
	[Arguments]  ${SupplierName}
	PO_NewCreationPopUpPage.Enter the value in First Text box  ${SupplierName}
	Wait.DOMreadyByWaitTime  1
	# will be removing the below commented code if the supplier checkbox works fine 
	#Click Element  //input[@class='dijitReset dijitCheckBoxInput' and @name='IsSupplier']
	#Click Element  //input[@class='dijitReset dijitCheckBoxInput' and @name='IsAgent']
	PO_NewCreationPopUpPage.Click on Save Button
	
Update ID For Required Supplier
	[Arguments]	${forSupplier}	${ID}
	Click Element 	//a[text()='${forSupplier}']/../following-sibling::td[@data-csi-act='SupplierNumber::0']	
	Wait.DOMreadyByWaitTime  3
	Input Text 	${Loc_Table_InputText}	${ID} 

Update State For Required Supplier
	[Arguments]	${forFactory}	${State}
	Click Element  //a[text()='${forFactory}']/../following-sibling::td[@data-csi-act='State::0']
	Wait.DOMreadyByWaitTime  1
	Click Element  //div[@class='dijitReset dijitMenu dijitComboBoxMenu']/div[@class='dijitReset dijitMenuItem' and text()='${State}']

Update Country For Required Supplier
	[Arguments]	${forSupplier}	${CountryofOrigin}  ${Country_Index}
	${CountryofOriginValueCollection} = 	GetDataProviderColumnValue	${CountryofOrigin}
	${CountryofOriginValue} =  DataProviderSplitterForMultipleValues  ${CountryofOriginValueCollection}  ${Country_Index}
	Click Element 	//a[text()='${forSupplier}']/../following-sibling::td[@data-csi-act='Country::0']
	Wait Until Element Is Visible  //div[@class='dijitReset dijitMenuItem' and text()='${CountryofOriginValue}']
	Click Element  //div[@class='dijitReset dijitMenu dijitComboBoxMenu']//div[@class='dijitReset dijitMenuItem' and text()='${CountryofOriginValue}']

Verify Country For Required Supplier
	[Arguments]	 ${forSupplier}  ${CountryList}  ${CountryIndex}
	Click Element 	//a[text()='${forSupplier}']/../following-sibling::td[@data-csi-act='Country::0']
	Wait.DOMreadyByWaitTime  3
	:FOR  ${i}  IN RANGE  0  2
	\	${CountryIndex} =  Evaluate  ${i} + 1
	\  	${CountryCollection} =  Data_Provider.GetDataProviderColumnValue  ${CountryList}
	\	${Country_Locator} =  Data_Provider.DataProviderSplitterForMultipleValues  ${CountryCollection}  ${CountryIndex} 
	\	Assertions.Assert for the Element in Table DDL  ${Country_Locator}

		
Update Port of Origin For Required Supplier
	[Arguments]  ${forSupplier}  ${PortofOrigin}  ${PortofOrigin_Index}
	${PortofOriginCollection} = 	GetDataProviderColumnValue	${PortofOrigin}
	${PortofOriginValue} =  DataProviderSplitterForMultipleValues  ${PortofOriginCollection}  ${PortofOrigin_Index}
	Click Element 	//a[text()='${forSupplier}']/../following-sibling::td[@data-csi-act='PortOfOrigin::0']	
	Wait.DOMreadyByWaitTime  1
	Click Element		//div[@class='dijitReset dijitMenu dijitComboBoxMenu']/div[@class='dijitReset dijitMenuItem' and text()='${PortofOriginValue}']
	

Set or Unset Agent 
	[Arguments]  ${forSupplier}
 	Click Element 	//a[text()='${forSupplier}']/../following-sibling::td[@data-csi-heading='IsAgent::0']/div/input	
	Wait.DOMreadyByWaitTime  1
	
	
Set or Unset Warehouse
	[Arguments]  ${forSupplier}
	${forSupplierValue} =  GetDataProviderColumnValue  ${forSupplier}
 	Click Element 	//a[text()='${forSupplierValue}']/../following-sibling::td[@data-csi-heading='IsWarehouse::0']/div/input
	
Click on the Required Supplier
	[Arguments]  ${SupplierList}  ${Index}
	${SupplierCollection} =  Data_Provider.GetDataProviderColumnValue  ${SupplierList}
	${Supplier_Identifier} =  DataProviderSplitterForMultipleValues  ${SupplierCollection}  ${Index}
	Click Link  //td[@data-csi-heading='Node Name::0']/a[text()='${Supplier_Identifier}']
	#This locator works need to verify if the above keyword works fine Click Link  //a[text()='${Supplier_Identifier}']

Wait for New Factory option
    Wait Until Element Is Visible  ${Loc_NewFactory_Link}

Click on New Factory option
	Click Element  ${Loc_NewFactory_Link}	

Wait for New Supplier Discount option 
	Wait Until Element Is Visible  ${Loc_New Supplier Discount_Link}
	
Click on New Supplier Discount option
	Click Element  ${Loc_New Supplier Discount_Link}	
	
Enter the Supplier Discount Name	
	[Arguments]  ${DiscountName}
	Input Text  ${Loc_Table_InputText}  ${DiscountName}

/* #The below Keyword clicks checkbox for Node link and the node value and the position of the checkbox 
   #is given directly to the keyword, excel read function is performed within the calling or before the calling function */ 
 Click checkbox in table view   
	[Arguments]  ${RowIdentifier}  ${Position}
	Click Element  //td[a[text()='${RowIdentifier}']]/following::td[${Position}]/div/Input[@class='dijitReset dijitCheckBoxInput']	

Click on required orphan select link
	[Arguments]  ${LinkText}
	Click Link  //a[text()='${LinkText}']	

#This Keyword below is used enter the node name value an link 
# This can be moved to a new PO TABLE ROW and can be reused 
# given directly to the keyword, excel read function is performed within the calling or before the calling function */ 
# Click checkbox in table view   
Enter the Node name value in Text Box
	[Arguments]	${Nodename}	
	Input Text	${Loc_Table_InputText}	${Nodename}

#This Keyword below is used to click on the link in table view 
# This can be moved to a new PO TABLE ROW and can be reused 
# given directly to the keyword, excel read function is performed within the calling or before the calling function */ 
 	
Click on the Node name in table
	[Arguments]  ${Nodename}  ${position}
	Click Link  //td[${position}]/a[text()='${Nodename}']

Click on the Node in table
  [Arguments]  ${Nodename}
  Wait Until Element Is Visible  //td[@data-csi-heading='Node Name::0']/a[text()='${Nodename}']  timeout=120
  Click Element  //td[@data-csi-heading='Node Name::0']/a[text()='${Nodename}']
	
	#This locator works need to verify if the above keyword works fine Click Link  //a[text()='${Supplier_Identifier}']

Update pieces per hour
	${Count} =  Get Element Count  //td[1]/following::a[@class='browse']/following::td[@class='attrNumber iconEditable']
	${PiecesList} =  Get WebElements   //td[@data-csi-act='PcsPerHr::0']
	${iteration} =  Evaluate  ${Count} + 1
	:FOR  ${i}  IN RANGE  0  ${Count}
	\	${Element} =   Get From List   ${PiecesList}  ${i}
	\	Wait.DOMreadyByWaitTime  2
	\	Click Element  ${Element}
	#Commenting for now as the double click of the element causes issue \	Click Element  ${Element}
	\	#Wait Until Page Contains Element  css=[data-csi-automation='edit-RoutingRevision-Items-PcsPerHr:'] .dijitInputField input
	\	#Input Text  css=[data-csi-automation='edit-RoutingRevision-Items-PcsPerHr:'] .dijitInputField input  ${i}
	\	Wait.DOMreadyByWaitTime  2
	\   Input Text  //input[@class='dijitReset dijitInputInner']  ${i} 
	\	Click Element  css=[data-csi-tab='RoutingRevision-TDS']
	Wait.DOMreadyByWaitTime  3
	
Update Factory value in Routing 
	Click Element  //div[text()='Factories']/following::td[@class='attrReflist attrContainer iconEditable']
	Click Element  //div[*]/label[text()=*]
	
Click on the Action link in table 
 	[Arguments]  ${RefElement}  ${index}  ${LinkTitle}
 	${RefElementCollection} =  Data_Provider.GetDataProviderColumnValue  ${RefElement}
	${Ref} =  DataProviderSplitterForMultipleValues  ${RefElementCollection}  ${index}
	Click Element  //td[a[text()='${Ref}']]/following-sibling::td[@class='actionsColumn noexport lastColumn']/div/a[@title='${LinkTitle}']
 	
Click on the Action link in table Mod 
 	[Arguments]  ${Ref}  ${LinkTitle}
 	Click Element  //td[a[text()='${Ref}']]/following-sibling::td[@class='actionsColumn noexport lastColumn']/div/span[@data-csi-act='${LinkTitle}']

Click on the Required Cell in BOM TDS table 
	[Arguments]  ${PlacementName}  ${ColumnIndex}  ${MatchIndex}
	${Count} =  Get Element Count  //span[text()='${PlacementName}']/following::td[${ColumnIndex}]
	${ElementList} =  Get WebElements  //span[text()='${PlacementName}']/following::td[${ColumnIndex}]
	${Element1} =  Get From List  ${ElementList}  ${MatchIndex}
	Click Element  ${Element1}  
Click on the Unit Price Cell in BOM TDS table 
	[Arguments]  ${PlacementName}  ${ColumnIndex}  ${MatchIndex}
	${Count} =  Get Element Count  //span[text()='${PlacementName}']/following::td[@data-csi-heading='UnitPriceAct::0']
	${ElementList} =  Get WebElements  //span[text()='${PlacementName}']/following::td[@data-csi-heading='UnitPriceAct::0']
	${Element1} =  Get From List  ${ElementList}  ${MatchIndex}
	Click Element  ${Element1}
Click on the TDS Cell in BOM TDS table 
	[Arguments]  ${PlacementName}  ${AutoId}  ${MatchIndex}
	${Count} =  Get Element Count  //span[text()='${PlacementName}']/following::td[@data-csi-act='${AutoId}']
	${ElementList} =  Get WebElements  //span[text()='${PlacementName}']/following::td[@data-csi-act='${AutoId}']
	${Element1} =  Get From List  ${ElementList}  ${MatchIndex}
	Click Element  ${Element1}  
	
Select Requrired value from DDL
  [Arguments]  ${ValueList}  ${Index}
  ${ValueCollection} =  Data_Provider.GetDataProviderColumnValue  ${ValueList}
  ${Value} =  DataProviderSplitterForMultipleValues  ${ValueCollection}  ${Index}
  Click Element  //div[@class='dijitReset dijitMenu dijitComboBoxMenu']/div[@class='dijitReset dijitMenuItem' and text()='${Value}']	
	
Click on the Required CheckBox-StyleBOM
	[Arguments]  ${StringName}
	Click Element  //*[@class='attrPrimary' and text()='${StringName}']//ancestor::td[@class='attrString iconEditable firstColumn']//following-sibling::td[@class='attrBoolean iconEditable']//input[@class='dijitReset dijitCheckBoxInput']
	
#Thiru	
Click on the Required cell in the table 
	[Arguments]  ${Refelement}  ${Cellposition}
	Click Element  //td/a[text()='${Refelement}']/following::td[${Cellposition}]

Click on the Required DDL cell in the table 
	[Arguments]  ${Refelement}  ${Cellposition}
	Click Element  //td/a[text()='${Refelement}']/following::td[${Cellposition}]
	
Enter the number value in table 
	[Arguments]  ${Value}
	#Input Text  ${Loc_Table_Input_Number}	${Value}
	#Input Text  //*[@data-csi-automation='edit-Supplier-SupplierDiscounts-DiscountPct:']//div//input[@class='dijitReset dijitInputInner']	${Value}
	Input Text  //input[@class='dijitReset dijitInputInner']  ${Value}
Enter the text value in table 
	[Arguments]  ${Value}
	Input Text  ${Loc_Table_InputText}	${Value} 	
	
Click on Actions link in the table view 
  [Arguments]  ${ReferenceItem}  ${LinkTitle}
  Click Element  //*[@class='attrString iconEditable firstColumn']/a[text()='${ReferenceItem}']/following::td/div/span[@title='${LinkTitle}']
  
Click on the Sourcing SupplierQuote Tab 
  Click Element  ${Loc_SourcingSupplierQuotesTab}
  	
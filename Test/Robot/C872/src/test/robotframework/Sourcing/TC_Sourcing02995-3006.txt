*** Settings ***

Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Assertions.txt
Resource          ../UtilityHelper/Wait.txt
Resource    	  ../PageObjects/PO_MaterialTypePage.txt
Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../PageObjects/PO_SpecificationPage.txt
Resource          ../PageObjects/PO_SourcingPage.txt
Resource          ../Locators/Loc_SourcingSupplierPage.txt
Resource          ../Locators/Loc_StylePage.txt
Resource          ../Locators/Loc_SourcingPage.txt
Resource          ../Locators/Loc_QualityPage.txt
Resource          ../Locators/Loc_Calendar.txt

***Variable***

${Supplier1}  Laces
${SizeLabel1}  Size 2
${SizeLabel2}  Size 3
${SizeLabel3}  Size 4

*** Test Cases ***
Test Setup         
    Setup The Variables
Login-Home-Sourcing- Supplier PO
  Login TestCaseName		SupplierPO
  # Home tab-> Sourcing-> Customers
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads
  PO_HomePage.Click on Sourcing tab 
  DOMready-TabLoads
  Click Element  ${Loc_Home-Sourcing-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  ${Loc_Home-Sourcing-SupplierPO-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  //a[@class='browse' and text()='${SupplierPO1}']
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}

Sourcing-02995-Click on the samples tab and check if the samples related to the data sheets and the Supplier PO are displayed in the table.
 #Home-Sourcing-Supplier PO-Supplier PO-Samples
 #Click on the samples tab and check if the samples related to the data sheets and the Supplier PO are displayed in the table.
  Click Element   ${PurchasedOrder-Samples}
  DOMready-TabLoads
  
#Sourcing-02996
 #Home-Sourcing-Supplier PO-Supplier PO-Samples
 #Edit the values and check if the changes made are displayed correctly
 ####### No Data Available ############## 
 
Sourcing-02997-Click on the 'New' link and make sure two links 'New Shipment' and 'New from Shipment ' are available
 #Home-Sourcing-Supplier PO-Supplier PO-Shipment
 #Click on the 'New' link and make sure two links 'New Shipment' and 'New from Shipment ' are available
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads
  PO_HomePage.Click on Sourcing tab 
  DOMready-TabLoads
  Click Element  ${Loc_Home-Sourcing-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  ${Loc_Home-Sourcing-SupplierPO-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  //a[@class='browse' and text()='${SupplierPO1}']
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}
  Click Element   ${PurchasedOrder-Shipment_Tab}
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}
  Mouse Over   ${SupplierPO_Shipment_Actions}
  Click Element  ${SupplierPO_Shipment_Actions}
  #Wait Until Element Is Visible	  ${NewShipment}
  #Assert for the given element    ${NewShipment}
  Assert for the given element   ${Select_Shipment}
  
Sourcing-02998-Click on 'New Shipment' link and check whether a new Shipment is created. Update editable attributes and check if they are displayed correctly. Also click 'New from Shipment' link and make sure the aggregate displays only PO line Item shipments for selection. Select a few and click Save 
 #Home-Sourcing-Supplier PO-Supplier PO-Shipment
 #Click on 'New Shipment' link and check whether a new Shipment is created. Update editable attributes and check if they are displayed correctly. Also click 'New from Shipment' link and make sure the aggregate displays only PO line Item shipments for selection. Select a few and click Save   
  #Assert for the given element   ${NewShipment}
  #Assert for the given element   ${Select_Shipment}
  Click Element  ${SupplierPO_Shipment_Actions}
  DOMreadyByWaitTime  2
  #Click Element	  ${NewShipment}
  DOMreadyByWaitTime  2
  Click Element   ${PurchasedOrder-Shipment_Tab}
  DOMready-TabLoads
  Execute Javascript    window.document.evaluate("//a[text()='(Unnamed)']/parent::td", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();
  Click Element  //div/textarea
  Input Text  //div/textarea  Shipment1
  #Wait Until Element Is Visible    ${TextArea_Shipment}
  #Input Text   ${TextArea_Shipment}   Shipment1
  Click Element   ${PurchasedOrder-Shipment_Tab}
  DOMready-TabLoads
  
Sourcing-02999-Verify that Shipment Details and Shipment Quantities sections are displayed on Shipment selection
 #Home-Sourcing-Supplier PO-Supplier PO-Shipment
 #Verify that Shipment Details and Shipment Quantities sections are displayed on Shipment selection
  Assert NodeName    Shipment1
  #Select FOB Date
  Click Element   //*[@class='browse' and text()='Shipment1']/following::td[@data-csi-act='FactoryOutBoundDate::0']
  DOMreadyByWaitTime  3
  Click Element  //span[@class='dijitCalendarDateLabel' and text()='1']
  DOMreadyByWaitTime  3
  
Sourcing-03000-Update all editable attributes in Shipment Details section. Verify that it is getting updated properly.
 #Home-Sourcing-Supplier PO-Supplier PO-Shipment
 #Update all editable attributes in Shipment Details section. Verify that it is getting updated properly.
  Click Element  //*[@class='browse' and text()='Shipment1']/following::td[@data-csi-act='VesselName::0']
  Wait Until Element Is Visible   ${TextArea_VesselName}
  Input Text  ${TextArea_VesselName}   VesselName1
  Click Element   ${PurchasedOrder-Shipment_Tab}
  #Click Element   //*[@class='browse' and text()='Shipment1']/following::td[@data-csi-act='Container::0']
  #DOMreadyByWaitTime   3
  #PO_SpecificationPage.Select Requrired value from DDL-StyleBOM    	AUT_Shipping Container-01

Sourcing-03001	  
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads
  PO_HomePage.Click on Sourcing tab 
  DOMready-TabLoads
  Click Element  ${Loc_Home-Sourcing-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  ${Loc_Home-Sourcing-SupplierPO-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  //a[@class='browse' and text()='${SupplierPO1}']
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}
  #Home-Sourcing-Supplier PO-Supplier PO-Shipment	
  #Make sure Orders section is displayed once the Shipment is selected  
  Click Element   ${PurchasedOrder-Shipment_Tab}
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}
  Assert NodeName     Shipment1
  Click Element  //a[@class='browse' and text()='Shipment1']
  DOMready-TabLoads 
Sourcing-03002-Click on 'New Orders shipment' link in the Orders section. Check if the Orders selected in the Home-Sourcing-POs-Orders tab are displayed for selection.	
 #Home-Sourcing-Supplier PO-Supplier PO-Shipment	
 #Click on 'New Orders shipment' link in the Orders section. Check if the Orders selected in the Home-Sourcing-POs-Orders tab are displayed for selection.
  Click Element  ${ShipmentNode-Shipment_Tab}
  DOMready-TabLoads 
  Click Element  css=table[data-csi-automation='plugin-ShipmentTerms-ShipmentOrders-ToolbarNewActions']
  #MouseOver Assert and Click    css=span[data-csi-automation='actions-ShipmentTerms-ShipmentOrders-root']	 css=[data-csi-act='AggregateShipmentOrders']
  DOMready-PopUpAppears
  #Assert Popup Table Text    ${StyleSKUSize2}
  
Sourcing-03003-Select few SKUs & Material SKUs and click on Save	
 #Home-Sourcing-Supplier PO-Supplier PO-Shipment	
 #Select few SKUs & Material SKUs and click on Save
  ${ReqCheckbox} =  Required Element From List  //td[contains(text(),'${SizeLabel2}')]//preceding-sibling::td//div//input  0
  Select Checkbox  ${ReqCheckbox}	
   DOMreadyByWaitTime  3
  Click Element    ${Loc_SaveButton}
  DOMready-PopUpCloses
 
Sourcing-03004-Update the Shipment Quantity value and check whether the Balance value is getting updated based on Order quantity	
 #Home-Sourcing-Supplier PO-Supplier PO-Shipment	
 #Update the Shipment Quantity value and check whether the Balance value is getting updated based on Order quantity
  Click Element    //*[@class='attrString firstColumn' and contains(text(),'${SizeLabel2}')]/following::td[@data-csi-act='Quantity::0']
  #Wait Until Element Is Visible   //*[@data-csi-automation='edit-ShipmentTerms-ShipmentOrders-Quantity:']/div[2]/input
  DOMreadyByWaitTime  3
      Enter The Values on Textarea  //div[contains(@class,'csi-flex-row')]//input[contains(@class,'dijitReset dijitInputInner')]  10
  
  #Input Text  css=[data-csi-automation='edit-ShipmentTerms-ShipmentOrders-Quantity:'] .dijitInputInner  10
  Click Element   ${Loc_Release}
  Element Should be Visible  //td[@data-csi-act='Quantity::0' and contains(text(),'10')] 
      
Sourcing-03005-Click on Freeze link in Shipment table and Check if the record is frozen with a confirmation.	
 #Home-Sourcing-Supplier PO-Supplier PO-Shipment	
 #Click on Freeze link in Shipment table and Check if the record is frozen with a confirmation.
  Click Element   css=[data-csi-automation='plugin-ShipmentTerms-ShipmentTerms-Freeze'] .csi-toolbar-btn-icon-Freeze
  DOMreadyByWaitTime  2
  Click Element   //*[@class='dijitReset dijitInline dijitButtonText' and contains(text(),'Freeze')]
  Wait Until Page Contains Element   ${Loc_Release}   
  DOMready-TabLoads
Sourcing-03006-Click on Unfreeze link in Shipment table and check whether able to edit Shipment values	
 #Home-Sourcing-Supplier PO-Supplier PO-Shipment	
 #Click on Unfreeze link in Shipment table and check whether able to edit Shipment values
  DOMreadyByWaitTime  4
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads
  PO_HomePage.Click on Sourcing tab 
  DOMready-TabLoads
  Click Element  ${Loc_Home-Sourcing-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  ${Loc_Home-Sourcing-SupplierPO-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  //a[@class='browse' and text()='${SupplierPO1}']
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}
  Click Element   ${PurchasedOrder-Shipment_Tab}
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}
  DOMready-TabLoads
  Assert NodeName     Shipment1
  ${Element} =  Get Required Element  //td//a[text()='Shipment1']//following::td//div//span[text()='more_horiz']  0
  Mouse Over  ${Element}
  Click Element  ${Element}
  ${NodeURL} =  Get Element Attribute  //a[text()='Shipment1']//parent::td  data-csi-url
  Wait Until Element Is Visible  css=table[data-csi-automation='actions-PurchasedOrder-Shipment-${NodeURL}'] tr[data-csi-act='Unfreeze'] td.dijitMenuItemLabel
  Click Element  css=table[data-csi-automation='actions-PurchasedOrder-Shipment-${NodeURL}'] tr[data-csi-act='Unfreeze'] td.dijitMenuItemLabel
  Wait Until Element is Visible  ${Loc_Release} 
  DOMready-TabLoads
  Click Element From List  //td//a[text()='Shipment1']//following::td[@data-csi-act='VesselName::0']  0
  Wait Until Element Is Visible  //textarea[@data-csi-automation='edit-PurchasedOrder-Shipment-VesselName:']
  Input Text  //textarea[@data-csi-automation='edit-PurchasedOrder-Shipment-VesselName:']  VesselName2
  Click Element   ${PurchasedOrder-Shipment_Tab}
  DOMready-TabLoads
  Element Should be Visible  //td//a[text()='Shipment1']//following::td[@data-csi-act='VesselName::0' and text()='VesselName2']
 
  SuiteSetUp.Exit Execution
***Keywords*** 
  
Setup The Variables
  ${This_DataProvider} =  Data_Provider.DataProvider  SupplierPO
  Set Suite Variable  ${This_DataProvider}
  Log  ${This_DataProvider}
  
  ${SupplierPOCollection} =  GetDataProviderColumnValue  Data_SupplierPOName
  ${SupplierPO1} =  DataProviderSplitterForMultipleValues  ${SupplierPOCollection}  1 
  Set Suite Variable  ${SupplierPO1}
  
  ${POGroupCollection} =  GetDataProviderColumnValue  Data_POGroupName
  ${POGroup1} =  DataProviderSplitterForMultipleValues  ${POGroupCollection}  1 
  Set Suite Variable  ${POGroup1}

  
  ${ColoredMaterialCollection} =  GetDataProviderColumnValue  Data_ColoredMaterialName
  ${ColoredMaterial1} =  DataProviderSplitterForMultipleValues  ${ColoredMaterialCollection}   1 
  ${ColoredMaterial2} =  DataProviderSplitterForMultipleValues  ${ColoredMaterialCollection}   2
  Set Suite Variable  ${ColoredMaterial1}
  Set Suite Variable  ${ColoredMaterial2}

Enter The Values on Textarea
    [Arguments]   ${Element}  ${InputData}
    DOMreadyByWaitTime  2
    # Press backspace button
    Press Key  ${Element}  \\08
    DOMreadyByWaitTime  2
    Press Key  ${Element}  ${InputData}   
MouseOver Assert and Click
  [Arguments]   ${Loc_MouseOverActLink}    ${Loc_NewElementAftMouseOver}
  Mouse Over   ${Loc_MouseOverActLink}
  Wait Until Element Is Visible	  ${Loc_NewElementAftMouseOver}
  Assert for the given element   ${Loc_NewElementAftMouseOver}
  Click Element	  ${Loc_NewElementAftMouseOver}
  
Click Node
  [Arguments]   ${NodeName}
  Click Element  //*[@class='browse' and text()='${NodeName}']

Assert Table Text
  [Arguments]	${TableText}
   Element Should Be Visible  //span[contains(text(),'${TableText}')]
     
Assert Popup Table Text
   [Arguments]	${TableText}
   Element Should Be Visible  //*[@class='attrString firstColumn' and text()='${TableText}']
***comments

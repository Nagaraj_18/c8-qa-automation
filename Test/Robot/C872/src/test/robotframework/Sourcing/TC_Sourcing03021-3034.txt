*** Settings ***

Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Assertions.txt
Resource          ../UtilityHelper/Wait.txt
Resource    	  ../PageObjects/PO_MaterialTypePage.txt
Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_StylePage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../PageObjects/PO_SpecificationPage.txt
Resource          ../PageObjects/PO_SourcingPage.txt
Resource          ../Locators/Loc_SourcingSupplierPage.txt
Resource          ../Locators/Loc_CollectionManagement.txt
Resource          ../Locators/Loc_StylePage.txt

*** Variables *** 
 ${MaterialName}  MaterialPO
 ${Style2}  POStyle1
 ${ColorSpec1}  blue6
 ${ColorSpec2}  red7
 ${Customer}  Myntra

*** Test Cases ***
Test Setup         
    Setup The Variables
Login-Home-Sourcing- Supplier PO
  Login TestCaseName		SupplierPO
  # Home tab-> Sourcing-> Customers
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads
  PO_HomePage.Click on Sourcing tab 
  DOMready-TabLoads
  Click Element  ${Loc_Home-Sourcing-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  ${Loc_Home-Sourcing-SupplierPO-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  //a[@class='browse' and text()='${SupplierPO2}']
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}
  Click Element   ${PO_Orders_Tab}
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}
  Click Element   ${Loc_SupplierPONodeName_OrdersTab_OrderPropertiesTab}
  DOMready-TabLoads
Sourcing-03021-Edit all editable attributes and check whether it is getting updated properly.
  #Home-Sourcing-Supplier PO-Supplier PO-Orders-Orders
  #Edit all editable attributes and check whether it is getting updated properly.
  ${Element} =  Get Required Element  ${Loc_POSupplierNode_SupplierPOTab_OrdersSection_ProductDDL}  -1
  Click Element  ${Element}
  DOMreadyByWaitTime  2
  Select Value from the DDL  ${Style2}  
  Click Element   ${Loc_SupplierPONodeName_OrdersTab_OrderPropertiesTab}
  DOMready-TabLoads 
  Click Element From List   //td[text()='${ColorSpec1}']//following::td[@data-csi-act="UnitPerPack::0"]  -1
  DOMreadyByWaitTime  3
        Enter The Values on Textarea  //div[contains(@class,'csi-flex-row')]//input[contains(@class,'dijitReset dijitInputInner')]  20
  
  #Selenium2Library.Input Text   css=[data-csi-automation='edit-PurchasedOrder-Orders-UnitPerPack:'] .dijitInputInner  20
  Click Element   ${Loc_SupplierPONodeName_OrdersTab_OrderPropertiesTab}
  DOMready-TabLoads 
  DOMreadyByWaitTime  4
  ${Element} =  Get Required Element  //td[text()="${ColorSpec1}"]//following::td[@data-csi-act="UnitPerPack::0" and contains(text(),"20")]  -1 
  Element Should Be Visible  ${Element}
  Click Element From List   //td[text()="${ColorSpec1}"]//following::td[@data-csi-act="FactoryOutBoundBase::0"]  -1
  DOMreadyByWaitTime  3
        Enter The Values on Textarea  //div[contains(@class,'csi-flex-row')]//input[contains(@class,'dijitReset dijitInputInner')]  15
  
  #Selenium2Library.Input Text   css=[data-csi-automation='edit-PurchasedOrder-Orders-FactoryOutBoundBase:'] .dijitInputInner  15
  Click Element   ${Loc_SupplierPONodeName_OrdersTab_OrderPropertiesTab}
  DOMready-TabLoads 
  ${Element} =  Get Required Element  //td[text()="${ColorSpec1}"]//following::td[@data-csi-act="FactoryOutBoundBase::0" and contains(text(),"15")]  -1 
  Element Should Be Visible  ${Element}
  #Click Element From List   //td[text()="${ColorSpec1}"]//following::td[@data-csi-act="DiscountPct::0"]  -1
  #DOMreadyByWaitTime  3
  #Selenium2Library.Input Text   css=[data-csi-automation='edit-PurchasedOrder-Orders-DiscountPct:'] .dijitInputInner  10
  #Click Element   ${Loc_SupplierPONodeName_OrdersTab_OrderPropertiesTab}
  #DOMready-TabLoads 
  #${Element} =  Get Required Element  //td[text()="${ColorSpec1}"]//following::td[@data-csi-act="DiscountPct::0" and contains(text(),"10")]  -1 
  #Element Should Be Visible  ${Element}
	

Sourcing-03022-Verify that new SKUs are created automatically based on the Colorways selected for the Orders.	
  #Home-Style-Season-Brand-Department-Collection-Style-SKUs
  #Verify that new SKUs are created automatically based on the Colorways selected for the Orders.
  Click on Home Icon
  DOMready-TabLoads
  Click Element  ${HomeStylesTab} 
  DOMready-TabLoads
  Click Element   ${HomeStyle_StylesTab}
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}
  Click Node   ${Style2}
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}
  Click Element  ${Loc_Style-StyleTab}
  DOMready-TabLoads
  Click Element    ${Style-ProductSKU}
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}
  Assert for the given Element  //td[@class="attrString iconEditable firstColumn"]//a[@class="browse" and text()="${ColorSpec1}"]
 
Sourcing-03023-Home-Sourcing-Supplier PO-Supplier PO-Orders-Orders
#Home-Sourcing-Supplier PO-Supplier PO-Orders-Orders
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads
  PO_HomePage.Click on Sourcing tab 
  DOMready-TabLoads
  Click Element  ${Loc_Home-Sourcing-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  ${Loc_Home-Sourcing-SupplierPO-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  //a[@class='browse' and text()='${SupplierPO2}']
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}
  Click Element   ${PO_Orders_Tab}
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}
  Click Element   ${Loc_SupplierPONodeName_OrdersTab_OrderPropertiesTab}
  DOMready-TabLoads
#Click on Select Colored Materials and check whether Select Colored Materials aggregate view displays the list of all Colored Materials
  Click Element  ${Loc_POSupplierNode_SupplierPOTab_OrdersSection_ProductDDL}
  Select Value from the DDL  All
  DOMreadyByWaitTime  6
  Click Element   ${Loc_SupplierPONodeName_OrdersTab_OrderPropertiesTab}
  DOMready-TabLoads
  #MouseOver  ${SupplierPO_OrderTable_Actions} 
  #Click Element  ${SupplierPO_OrderTable_Actions}
  Click Element  css=[data-csi-automation='plugin-PurchasedOrder-Orders-ToolbarNewActions'] td.dijitArrowButton
  Wait Until Page Contains Element  css=[data-csi-automation='plugin-PurchasedOrder-Orders-ToolbarNewActions'] [data-csi-act='AggregateColorMaterialOrders'] .dijitMenuItemLabel  
  Click Element  css=[data-csi-automation='plugin-PurchasedOrder-Orders-ToolbarNewActions'] [data-csi-act='AggregateColorMaterialOrders'] .dijitMenuItemLabel  
  DOMready-PopUpAppears
  #Assert for the given element  ${Loc_POSupplierNode_SupplierPOTab_Actions_NewColorwaysOrderLink}
  #Assert for the given element  ${Loc_POSupplierNode_SupplierPOTab_Actions_NewColoredMaterialsOrderLink}
  #Click Element   ${Loc_POSupplierNode_SupplierPOTab_Actions_NewColoredMaterialsOrderLink}
  DOMready-PopUpAppears
  Assert for the given element   //*[@class='attrString firstColumn' and text()='${ColoredMaterial1}']
  Assert for the given element   //*[@class='attrString firstColumn' and text()='${ColoredMaterial2}']
  
Sourcing-03024-Select few Colored Materials and click on Save. Check whether the Colored Materials gets added to the Orders table	
#Home-Sourcing-Supplier PO-Supplier PO-Orders-Orders
#Select few Colored Materials and click on Save. Check whether the Colored Materials gets added to the Orders table
   #Click Element  css=[data-csi-automation="filter-PurchasedOrder-OrderSelectColoredMaterials-Node Name:Child:__Parent__/Child:ProductSourcing/Child:DefaultQuote/Child:__Parent__/Child:Supplier"] .dijitButtonText
  #Wait Until Element is Visible  //label[@class="dijitMenuItemLabel" and text()="All"]
  #${ReqCheckbox} =  Required Element From List  //label[contains(text(),'All')]/preceding::input[1]  0
  #Select Checkbox  ${ReqCheckbox}
  #Click Element  //span[text()="Agent"]
  #DOMreadyByWaitTime  6
  #${ReqCheckbox} =  Required Element From List  //td[contains(text(),'${ColoredMaterial1}')]/preceding-sibling::td/div/input[@class='dijitReset dijitCheckBoxInput']  0
  #Select Checkbox  ${ReqCheckbox}
  Click Element From List  //td[contains(text(),'${ColoredMaterial1}')]/preceding-sibling::td/div/input[@class='dijitReset dijitCheckBoxInput']  -1
  DOMreadyByWaitTime  3
  #Select Checkbox in popup     ${ColoredMaterial1}
  #DOMreadyByWaitTime  3
  Click Element    ${Loc_SaveButton}
  DOMready-PopUpCloses
  Element Should Be Visible  //td[text()='${ColoredMaterial1}']   
      
Sourcing-03025-Edit all editable attributes and check whether it is getting updated properly.	
#Home-Sourcing-Supplier PO-Supplier PO-Orders-Orders
#Edit all editable attributes and check whether it is getting updated properly.
# Edit The values 
  Click Element  ${Loc_POSupplierNode_SupplierPOTab_OrdersSection_ProductDDL}
  DOMreadyByWaitTime  2
  #Select Value from the DDL  ${Material2}
  Select Value from the DDL  ${MaterialName}
  Click Element   ${Loc_SupplierPONodeName_OrdersTab_OrderPropertiesTab}
  DOMready-TabLoads
  # Edit The values 
  Click Element From List  //td[text()='${ColoredMaterial1}']//following::td[@data-csi-act="Quantity::0"]  0
  DOMreadyByWaitTime  3
        Enter The Values on Textarea  //div[contains(@class,'csi-flex-row')]//input[contains(@class,'dijitReset dijitInputInner')]  20
  
  #Selenium2Library.Input Text   css=[data-csi-automation='edit-PurchasedOrder-Orders-Quantity:'] .dijitInputInner  20
  Click Element   ${Loc_SupplierPONodeName_OrdersTab_OrderPropertiesTab}
  DOMready-TabLoads 
  Element Should Be Visible	//td[text()='${ColoredMaterial1}']//following::td[@data-csi-act="Quantity::0" and contains(text(),"20")]
  Click Element From List   //td[text()='${ColoredMaterial1}']//following::td[@data-csi-act="UnitPerPack::0"]  0
  DOMreadyByWaitTime  3
        Enter The Values on Textarea  //div[contains(@class,'csi-flex-row')]//input[contains(@class,'dijitReset dijitInputInner')]  10
  
  #Selenium2Library.Input Text   css=[data-csi-automation='edit-PurchasedOrder-Orders-UnitPerPack:'] .dijitInputInner  10
  Click Element   ${Loc_SupplierPONodeName_OrdersTab_OrderPropertiesTab}
  DOMready-TabLoads 
  Element Should Be Visible	//td[text()='${ColoredMaterial1}']//following::td[@data-csi-act="UnitPerPack::0" and contains(text(),"10")]
  Click Element From List   //td[text()='${ColoredMaterial1}']//following::td[@data-csi-act="FactoryOutBoundBase::0"]  0
  DOMreadyByWaitTime  3
        Enter The Values on Textarea  //div[contains(@class,'csi-flex-row')]//input[contains(@class,'dijitReset dijitInputInner')]  10
  
  #Selenium2Library.Input Text   css=[data-csi-automation='edit-PurchasedOrder-Orders-FactoryOutBoundBase:'] .dijitInputInner  10
  Click Element   ${Loc_SupplierPONodeName_OrdersTab_OrderPropertiesTab}

  #Element Should Be Visible	//td[text()='${ColoredMaterial1}']//following::td[@data-csi-act="FactoryOutBoundBase::0" and contains(text(),"10")]
  #Click Element From List   //td[text()='${ColoredMaterial1}']//following::td[@data-csi-act="DiscountPct::0"]  -1
  DOMreadyByWaitTime  3
  #Selenium2Library.Input Text   css=[data-csi-automation='edit-PurchasedOrder-Orders-DiscountPct:'] .dijitInputInner  10
  #Click Element   ${Loc_SupplierPONodeName_OrdersTab_OrderPropertiesTab}
  #DOMready-TabLoads 
  #Element Should Be Visible	//td[text()='${ColoredMaterial1}']//following::td[@data-csi-act="DiscountPct::0" and contains(text(),"10")]

Sourcing-03026-Verify that new Material SKUs are created automatically based on the Colored Materials selected for the Orders.
#Home-Specification-Material-Material-Availability/SKUs-Material SKUs
#Verify that new Material SKUs are created automatically based on the Colored Materials selected for the Orders.
  Click on Home Icon
  DOMready-TabLoads  
  PO_HomePage.Click On Material tab 
  DOMready-TabLoads
  Click Element   //*[@class="browse" and text()="${MaterialName}"]
  DOMready-TabLoads
  Click Element   ${Loc_MaterialNodeName_MaterialTab}  
  DOMready-TabLoads 
  Click Element  ${Loc_MaterialNodeName_MaterialTab_SKUTab}
  DOMready-TabLoads 
  ${Check} =  Get Required Element  //td[@class="attrString iconEditable firstColumn"]//a[1][@class="browse" and text()="${ColoredMaterial1}"]  -1
  Assert for the given Element  ${Check}
Sourcing-03027-Home-Sourcing-Supplier PO-Supplier PO-Orders-Orders
#Home-Sourcing-Supplier PO-Supplier PO-Orders-Orders
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads
  PO_HomePage.Click on Sourcing tab 
  DOMready-TabLoads
  Click Element  ${Loc_Home-Sourcing-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  ${Loc_Home-Sourcing-SupplierPO-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  //a[@class='browse' and text()='${SupplierPO2}'] 
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}
  Click Element   ${PO_Orders_Tab}
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}
  Click Element  css=[data-csi-automation="plugin-PurchasedOrder-Orders-CustomViewSelect"] .dijitArrowButtonInner
  DOMreadyByWaitTime  3
  Click Element  //div[@class="dijitReset dijitMenuItem" and text()="Default"]
  DOMreadyByWaitTime  4
#Click on Customer attribute drop down and check whether the list of customers created in Home-Sourcing-Customers tab are displayed. Select a customer and check whether it gets updated properly.
  Click Element From List  ${Loc_SupplierPONodeName_OrdersTab_OrdersTab_CustomerAttribute}  -1
  Wait Until Element Is Visible  //div[text()='${Customer}']
  Assert for the given Element  //div[text()='${Customer}']
  Select Value from the DDL  ${Customer}
  Click Element   ${PO_Orders_Tab}
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}
  Assert for the Given Element  //a[@class="browse" and text()='${Customer}']
  Click Element  css=[data-csi-automation="plugin-PurchasedOrder-Orders-CustomViewSelect"] .dijitArrowButtonInner
  DOMreadyByWaitTime  3
  Click Element  //div[@class="dijitReset dijitMenuItem" and text()="OrdersCV"]
  DOMreadyByWaitTime  4

Sourcing-03028-Check whether PO Product is getting created successfully based on the selection of the Colorways in Orders tab	
#Home-Sourcing-Supplier PO-Supplier PO-Orders-Summary
#Check whether PO Product is getting created successfully based on the selection of the Colorways in Orders tab
  Click Element  ${Order_Summart_Tab}
  DOMready-TabLoads 
  Click Element From List  ${Loc_SupplierPONodeName_OrdersTab_SummaryTab_RefreshButton}  -1
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}
  Assert for the Given Element  //td//a[@class="browse" and text()='${Style2}']
  
Sourcing-03029-Check whether PO Material is getting created successfully based on the selection of the Colored Materials in Orders tab
#Home-Sourcing-Supplier PO-Supplier PO-Orders-Summary
#Check whether PO Material is getting created successfully based on the selection of the Colored Materials in Orders tab
  Assert for the Given Element  //td//a[@class="browse" and text()='${MaterialName}']  
    
Sourcing-03030-Check whether the Product attribute drop down in toolbar displays the list of PO Materials and PO Products.	
#Home-Sourcing-Supplier PO-Supplier PO-Orders-Orders
#Check whether the Product attribute drop down in toolbar displays the list of PO Materials and PO Products.
  Click Element   ${PO_Orders_Tab}
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}
  Click Element   ${Loc_SupplierPONodeName_OrdersTab_OrderPropertiesTab}
  DOMready-TabLoads
  Click Element  ${Loc_POSupplierNode_SupplierPOTab_OrdersSection_ProductDDL}
  Assert for the given Element  //div[text()='${Style2}']
  Assert for the given Element  //div[text()='${MaterialName}']
  Select Value from the DDL  All
  DOMreadyByWaitTime  6
  Click Element   ${Loc_SupplierPONodeName_OrdersTab_OrderPropertiesTab}
  DOMready-TabLoads 

Sourcing-03031-Select a PO Product in Product attribute drop down and check whether the list of the Colorways are displayed
  #Home-Sourcing-Supplier PO-Supplier PO-Orders-Orders
  #Select a PO Product in Product attribute drop down and check whether the list of the Colorways are displayed 
  ${Element} =  Get Required Element  ${Loc_POSupplierNode_SupplierPOTab_OrdersSection_ProductDDL}  -1
  Click Element  ${Element}
  Wait Until Element Is Visible  //div[@class='dijitReset dijitMenuItem' and text()='All']
  Element Should Be Visible  //div[@class='dijitReset dijitMenuItem' and text()='${Style2}']
  #Element Should Be Visible  //div[@class='dijitReset dijitMenuItem' and text()='${MaterialName}']
  Element Should Be Visible  //div[@class='dijitReset dijitMenuItem' and text()='${MaterialName}']
  Click Element   ${Loc_SupplierPONodeName_OrdersTab_OrderPropertiesTab}
  DOMready-TabLoads 
Sourcing-03032-Check whether the Style-Size Range Sizes are displayed in Columns. Verify that the values for the Sizes can be edited
  #Home-Sourcing-Supplier PO-Supplier PO-Orders-Orders
  #Check whether the Style-Size Range Sizes are displayed in Columns. Verify that the values for the Sizes can be edited. Edit all editable values and verify that it is getting updated properly.
  ${Element} =  Get Required Element  ${Loc_POSupplierNode_SupplierPOTab_OrdersSection_ProductDDL}  -1
  Click Element  ${Element}
  Wait Until Element Is Visible  //div[@class='dijitReset dijitMenuItem' and text()='All']
  Select Value from the DDL  ${Style2}  
  Click Element   ${Loc_SupplierPONodeName_OrdersTab_OrderPropertiesTab}
  DOMready-TabLoads 
  Click Element From List   //td[text()='${ColorSpec1}']//following::td[@data-csi-act='QuantityPerSize::0']//span[@class='attrPrimary']  0
  DOMreadyByWaitTime  3

        Enter The Values on Textarea  //div[contains(@class,'csi-flex-row')]//input[contains(@class,'dijitReset dijitInputInner')]  30
  
  #Selenium2Library.Input Text   css=[data-csi-automation='edit-PurchasedOrder-Orders-QuantityPerSize:'] .dijitInputInner  30
  Click Element   ${Loc_SupplierPONodeName_OrdersTab_OrderPropertiesTab}
  DOMready-TabLoads 
  ${Text} =  Selenium2Library.Get Text  //td[text()='${ColorSpec1}']//following::td[@data-csi-act='UnitPerPack::0']
  ${Value} =  Evaluate  ${Text} * 30
  Element Should Be Visible	//td[text()='${ColorSpec1}']//following::td[@data-csi-act='QuantityPerSize::0']//span[contains(text(),'${Value}')]

Sourcing-03033-Home-Sourcing-Supplier PO-Supplier PO-Orders-Orders
#Home-Sourcing-Supplier PO-Supplier PO-Orders-Orders
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads
  PO_HomePage.Click on Sourcing tab 
  DOMready-TabLoads
  Click Element  ${Loc_Home-Sourcing-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  ${Loc_Home-Sourcing-SupplierPO-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  //a[@class='browse' and text()='${SupplierPO2}']
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}
  Click Element   ${PO_Orders_Tab}
  DOMready-TabLoads
  Wait Until Page Contains Element   ${Loc_Release}
  Click Element   ${Loc_SupplierPONodeName_OrdersTab_OrderPropertiesTab}
  DOMready-TabLoads
  #Select a PO Material in Product attribute drop down and check whether the list of the Colored Materials are displayed 
  Click Element  ${Loc_POSupplierNode_SupplierPOTab_OrdersSection_ProductDDL}
  Select Value from the DDL  ${MaterialName}
  DOMreadyByWaitTime  6
  Click Element   ${Loc_SupplierPONodeName_OrdersTab_OrderPropertiesTab}
  DOMready-TabLoads
  Element Should Be Visible   //td[text()='${ColoredMaterial1}']

Sourcing-03034-Check whether the Colored Material-Material-Material Sizes are displayed in Columns. Verify that the values for the Material Sizes can be edited. Edit all editable values and verify that it is getting updated properly.	
  #Home-Sourcing-Supplier PO-Supplier PO-Orders-Orders
  #Check whether the Colored Material-Material-Material Sizes are displayed in Columns. Verify that the values for the Material Sizes can be edited. Edit all editable values and verify that it is getting updated properly.
  Click Element From List   //td[text()='${ColoredMaterial1}']//following::td[@data-csi-act='QuantityPerSize::0']//span[@class='attrPrimary']  0
   DOMreadyByWaitTime  3
        Enter The Values on Textarea  //div[contains(@class,'csi-flex-row')]//input[contains(@class,'dijitReset dijitInputInner')]  30
  
  #Selenium2Library.Input Text   css=[data-csi-automation='edit-PurchasedOrder-Orders-QuantityPerSize:'] .dijitInputInner  30
  Click Element   ${Loc_SupplierPONodeName_OrdersTab_OrderPropertiesTab}
  DOMready-TabLoads 
  ${Text} =  Selenium2Library.Get Text  //td[text()='${ColoredMaterial1}']//following::td[@data-csi-act='UnitPerPack::0']
  ${Value} =  Evaluate  ${Text} * 30
  Element Should Be Visible	//td[text()='${ColoredMaterial1}']//following::td[@data-csi-act='QuantityPerSize::0']//span[contains(text(),'${Value}')]

  SuiteSetUp.Exit Execution
 **Keywords 
Click Node
  [Arguments]   ${NodeName}
  Click Element  //*[@class='browse' and text()='${NodeName}']
Enter The Values on Textarea
    [Arguments]   ${Element}  ${InputData}
    DOMreadyByWaitTime  2
    # Press backspace button
    Press Key  ${Element}  \\08
    DOMreadyByWaitTime  2
    Press Key  ${Element}  ${InputData}   
Setup The Variables
  ${This_DataProvider} =  Data_Provider.DataProvider  SupplierPO
  Set Suite Variable  ${This_DataProvider}
  Log  ${This_DataProvider}
   
  ${MaterialCollection} =  Data_Provider.GetDataProviderColumnValue  Data_MaterialName
  ${Material2} =  DataProviderSplitterForMultipleValues  ${MaterialCollection}  2
  Set Suite Variable  ${Material2} 
   
  ${SupplierPOCollection} =  GetDataProviderColumnValue  Data_SupplierPOName
  ${SupplierPO1} =  DataProviderSplitterForMultipleValues  ${SupplierPOCollection}  1 
  ${SupplierPO2} =  DataProviderSplitterForMultipleValues  ${SupplierPOCollection}  2
  Set Suite Variable  ${SupplierPO1}
  Set Suite Variable  ${SupplierPO2}
  
  ${POGroupCollection} =  GetDataProviderColumnValue  Data_POGroupName
  ${POGroup1} =  DataProviderSplitterForMultipleValues  ${POGroupCollection}  1 
  Set Suite Variable  ${POGroup1}
  
  
    
  ${DocumentNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_DocumentName
  ${Document1} =  DataProviderSplitterForMultipleValues  ${DocumentNameCollection}  1
  Set Suite Variable  ${Document1}
  
  ${CommentNameCollection} =  Data_Provider.GetDataProviderColumnValue  Data_CommentName
  ${Comment1} =  DataProviderSplitterForMultipleValues  ${CommentNameCollection}  1
  Set Suite Variable  ${Comment1}
  
    
  ${ShipmentCollection} =  Data_Provider.GetDataProviderColumnValue  Data_ShipmentName
  ${Shipment1} =  DataProviderSplitterForMultipleValues  ${ShipmentCollection}  1
  Set Suite Variable  ${Shipment1}
  
 
  ${IssueNameCollection} =  GetDataProviderColumnValue  Data_IssueName  
  ${IssueName1} =  DataProviderSplitterForMultipleValues  ${IssueNameCollection}  1 
  ${IssueName2} =  DataProviderSplitterForMultipleValues  ${IssueNameCollection}  2
  Set Suite Variable  ${IssueName1}
  Set Suite Variable  ${IssueName2}  
  
    
  ${ColoredMaterialCollection} =  GetDataProviderColumnValue  Data_ColoredMaterialName
  ${ColoredMaterial1} =  DataProviderSplitterForMultipleValues  ${ColoredMaterialCollection}  1 
  ${ColoredMaterial2} =  DataProviderSplitterForMultipleValues  ${ColoredMaterialCollection}  2
  Set Suite Variable  ${ColoredMaterial1}
  Set Suite Variable  ${ColoredMaterial2}
 ***Comments

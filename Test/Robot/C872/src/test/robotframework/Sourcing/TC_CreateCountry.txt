*** Settings ***
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt

Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_SourcingPage.txt
Resource          ../PageObjects/PO_TablePage.txt
Resource          ../Locators/Loc_SourcingSetupPage.txt

***Variables***
${CountryName}  Burma
${Loc_RegionName}  Burma 
${Loc_CV_SetToDefaultButton}  //span[@id='prefSetDefault_label']
	
*** Test Cases ***
Sourcing-00260
	[tags]  Sanity_DataLoad
	${This_DataProvider} =  Data_Provider.DataProvider  Create Country
	Set Suite Variable  ${This_DataProvider}
	SuiteSetUp.Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    #Wait Until Page Contains Element   ${Loc_Release}
    
    #Reload
    Wait.DOMready
    DOMready-TabLoads
    Click on Home Icon
    Wait.DOMready
    DOMready-TabLoads
    Click Element  ${Loc_Home_SourcingTab}
    DOMready-TabLoads
    Click Element  ${Loc_Home-Sourcing-SetupTab}
    DOMready-TabLoads
    Click Element  ${Loc_Home-Sourcing-Setup-CountryTab}
    DOMready-TabLoads
	
Sourcing-00261
    #Step : Create Country. 
    :FOR  ${i}  IN RANGE   0  1
    \	Log	${i}
    \   #${TableName} =  Get Element Attribute  ${Loc_Home-Sourcing-Setup-CountryTab}  name
    \   #Mouse Over Actions DDL Button-Active  ${TableName}  0
    \	SuiteSetUp.Test Step	${R_Click_NewCountry_Link}
    \	PO_SetupPage.Wait for the required New Creation options on mouse over-ActionsDDL	${Loc_NewCountry}
    \	PO_SetupPage.Click on required option after mouse over-ActionsDDL	${Loc_NewCountry}
    \	Wait.DOMreadyByWaitTime  3
	\	${InputStatus} =  Run Keyword And Return Status  Input Text  //div/textarea  ${CountryName}
    \   Run Keyword If  '${InputStatus}' == 'False'  Click Tab And Update Country Name 
    \	${ClickStatus} =  Run Keyword And Return Status  Click Tab After Entering Value For Inline Row   LibSourcing-Countries
	\   Run Keyword If  '${ClickStatus}' == 'False'  Alternate - Tab name Click  //div/textarea  LibSourcing-Countries
	\   DOMready-TabLoads
	
  [Teardown]    Close Browser 	
***Keywords***
Province Tab Click 
  Click Element    ${Loc_CountryNode-Province/StateTab}	
  Wait Until Element Is Enabled  ${Loc_CountryNode-Province/StateTab_CustomViewTextArea}

Click Tab And Update Country Name
   :FOR  ${i}  IN RANGE  0  5 
   \  Click Element  ${Loc_Home-Sourcing-Setup-CountryTab}
   \  Click Element    ${Loc_Sourcing-Setup-CountriesTab_CustomViewTextArea}
   \  ${Enabled} =  Run Keyword And Return Status  Wait Until Element Is Enabled      ${Loc_Sourcing-Setup-CountriesTab_CustomViewTextArea}
   \  Exit For Loop If  '${Enabled}' == 'True'
   Click Element  //td[@data-csi-act='Node Name::0']/a[text()='(Unnamed)']
   Input Text  ${Loc_TextAreaCountry}  ${CountryName}  

Click Tab And Update Province Name
   ${ClickStatus} =  Run Keyword And Return Status  Province Tab Click
	Run Keyword If  '${ClickStatus}' == 'False'  Alternate - Tab name Click  //div/textarea  9  Country-ProvinceState
	Click Element  css=tr.csiSelected>td[data-csi-act='Node Name::0']
	Wait Until Page Contains Element  ${Loc_Province/StateNodeNameTextArea}  
	Input Text  ${Loc_Province/StateNodeNameTextArea}  ${Loc_State/ProvinceName}
    Click Element  ${Loc_CountryNode-Province/StateTab}
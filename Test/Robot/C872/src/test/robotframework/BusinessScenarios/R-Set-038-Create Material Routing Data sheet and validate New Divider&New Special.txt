*** Settings ***
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../Locators/Loc_SourcingSetupPage.txt
Resource          ../Locators/Loc_MaterialPage.txt
Resource          ../PageObjects/PO_SourcingPage.txt
Resource          ../Locators/Loc_Routing.txt
Resource          ../Locators/Loc_SourcingSetupPage.txt
Resource          ../Locators/Loc_MaterialLibrary.txt

***Variables***
# ${Loc_Specification_RoutingTab}    css=[data-csi-tab='Material-Routing']
# ${Loc_Routing_RoutingItemsTab}    css=[data-csi-tab='RoutingRevision-Items']
${Loc_Routing_AddCapabilityDownButton}    css=[data-csi-automation='plugin-RoutingRevision-Items-ToolbarNewActions'] .dijitArrowButton
# ${Loc_Routing_NewDivider}    css=[data-csi-act='NewDSLineDivider']

*** Test Cases ***
Test Setup        
    DataSetup Create Operation Group
    
Open Existing Material
    [Tags]  Material Specification    
    Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    Wait.DOMready
    DOMready-TabLoads
    Click on Home Icon
    DOMready-TabLoads
    Click Element    ${Loc_MaterialTab}
    DOMready-TabLoads
    Click Element    ${Loc_Material_MaterialTab}
    DOMready-TabLoads
    Click Element    //td[@data-csi-act='Node Name::0']//a[contains(text(),'${MaterialName1}')]
    DOMready-TabLoads
Navigate to Routing Material and Create New Divider and New Special
    [Tags]  Material Specification
    Click Element    ${Loc_MaterialSpecificationTab}
    DOMready-TabLoads
    Click Element    ${Loc_TabSpecificationRouting}
    DOMready-TabLoads 
    Click Element from List    //td[@data-csi-act='Node Name::0']//a[contains(text(),'${MaterialRoutingName1}')]    -1
    DOMready-TabLoads 
    Click Element    ${Loc_TabRoutingItems}
    DOMready-TabLoads
    Click Element From List    ${Loc_Routing_AddCapabilityDownButton}    -1
    Wait.DOMreadyByWaitTime  2
    Click Element    ${Loc_NewDividerLink}    
    Wait.DOMreadyByWaitTime  2
    Input Text    //div/textarea    ${DividerName1}
    Wait.DOMreadyByWaitTime  2
    Click Element From List    ${Loc_Routing_AddCapabilityDownButton}    -1
    Wait.DOMreadyByWaitTime  2
    Click Element    ${Loc_NewSpecialLink}    
    Wait.DOMreadyByWaitTime  2
    Input Text    //div/textarea    Special1
    Wait.DOMreadyByWaitTime  2
    Click Element    ${Loc_TabRoutingItems}
    # Click Element From List   //td//span[text()='Special1']//following::td[@data-csi-heading='PcsPerHr::0' and contains(text(),'0.00')]    -1
    # Press Key  //div/input    \\08
    # DOMreadyByWaitTime  2    
    # Input Text    //div/input   3
    DOMready-TabLoads
    Close Browser 
    
*** Keywords ***
DataSetup Create Operation Group
  ${This_DataProvider} =  Data_Provider.DataProvider    Create Operation Group
  Set Suite Variable  ${This_DataProvider}
  Log  ${This_DataProvider}
  
  ${MaterialNameList} =  Data_Provider.GetDataProviderColumnValue  Data_MaterialName
  ${MaterialName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MaterialNameList}  1
  Set Suite Variable    ${MaterialName1}
  
  ${MaterialRoutingNameList} =     Data_Provider.GetDataProviderColumnValue    Data_Routing
  ${MaterialRoutingName1} =    Data_Provider.DataProviderSplitterForMultipleValues    ${MaterialRoutingNameList}    1
  Set Suite Variable    ${MaterialRoutingName1}    
   
  ${DividerNameList} =    Data_Provider.GetDataProviderColumnValue    Data_DividerName
  ${DividerName1} =    Data_Provider.DataProviderSplitterForMultipleValues   ${DividerNameList}    1
  Set Suite Variable    ${DividerName1}

*** Settings ***
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../Locators/Loc_SourcingSetupPage.txt
Resource          ../Locators/Loc_MaterialPage.txt
Resource          ../PageObjects/PO_SourcingPage.txt
Resource          ../Locators/Loc_Routing.txt
Resource          ../Locators/Loc_SourcingSetupPage.txt
Resource          ../Locators/Loc_MaterialLibrary.txt
Resource          ../Locators/Loc_MaterialBOM.txt


*** Test Cases ***
Test Setup 
    [Tags]  Material Specification
       
    DataSetup CreateMaterialBOM

Login To Application
    [Tags]  Material Specification
	#Data provider: It reads the data from the excel sheet for the test case specified. 
	${This_DataProvider} =  Data_Provider.DataProvider  Create Material BOM
    Set Suite Variable  ${This_DataProvider}
    Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    DOMready-TabLoads
    Click on Home Icon
    DOMready-TabLoads
Open to Eisting Material
    [Tags]  Material Specification
    Click Element    ${Loc_MaterialTab}
    DOMready-TabLoads
    Click Element    ${Loc_Material_MaterialTab}
    DOMready-TabLoads
    Click Element    //td[@data-csi-act='Node Name::0']//a[contains(text(),'${MaterialName3}')]
    DOMready-TabLoads
    Click Element    ${Loc_MaterialSpecificationTab}
    DOMready-TabLoads    
    Click Element    css=[data-csi-tab='Material-BOMs']
    DOMready-TabLoads
Creating New Destination BOM and adding placement to section
    [Tags]  Material Specification
    Click Element    ${NewMaterialBOM_btn}
    DOMreadyByWaitTime  2
    Click Element From List  ${BOMSubType_list}  -1
    Wait.DOMreadyByWaitTime  2
    Select value from the DDL  Trial MaterialBOMSubtype 001
    DOMreadyByWaitTime  1
    Input Text  ${MaterialBOMName_txtbx}    DestinationBOM
    PO_NewCreationPopUpPage.Click on Save Button
    Wait.DOMready-PopUpCloses
    DOMreadyByWaitTime  5
    Click Element from List   //td[@data-csi-act='Node Name::0']//a[contains(text(),'DestinationBOM')]    -1
    DOMready-TabLoads
    Click Element    css=[data-csi-tab='MaterialBOMRevision-TDS']
    DOMready-TabLoads
    Click Element    css=[data-csi-automation='plugin-MaterialBOMRevision-TDS-htmlToolbars']
    DOMready-TabLoads
    # Click Element    css=[data-csi-tab='ProductBOMRevision-PartMaterials']
    # DOMready-TabLoads
    Click Element  css=[data-csi-automation='plugin-ProductBOMRevision-PartMaterials-Actions']         
    DOMready-TabLoads
    Click Element    //td[@class='dijitReset dijitMenuItemLabel' and text()='Sections']            
    Click Element from List    //td[@class='dijitReset dijitMenuItemLabel' and text()='Select Section']    -1
    DOMreadyByWaitTime  5
    PO_SetupPage.Verify Dialog Window  Select BOM Section
    Wait.DOMreadyByWaitTime  5
    Select Single Matching Checkbox elements in the popup-(div-Element)  ${BOMSection3}  
    Select Single Matching Checkbox elements in the popup-(div-Element)  ${BOMSection4}
    Click On Save Button
    Wait.DOMready-PopUpCloses
    DOMready-TabLoads  
    Click Element    css=[data-csi-tab='MaterialBOMRevision-TDS']
    DOMready-TabLoads
    # Click Element    css=[data-csi-tab='ProductBOMRevision-PartMaterials']
    # DOMready-TabLoads  
    Click Element from List    //div[text()='add']    1
    DOMreadyByWaitTime  5
    PO_NewCreationPopUpPage.Select value from the DDL    ${Placement1}
    DOMreadyByWaitTime  2 
Open Source BOM and placement to first section 
    [Tags]  Material Specification
    Click Element    //span[@class='dijit dijitReset dijitInline dijitButton csiAction']//following::a[text()='A_MT_NoSize&HasColor']
    DOMready-TabLoads
    # Click Element from List   //td[@data-csi-act='Node Name::0']//a[contains(text(),'AUT_BOMNSHC')]    -1
    # DOMready-TabLoads
    Click Element    ${NewMaterialBOM_btn}
    DOMreadyByWaitTime  2
    Click Element From List  ${BOMSubType_list}  -1
    Wait.DOMreadyByWaitTime  2
    Select value from the DDL  Trial MaterialBOMSubtype 001
    DOMreadyByWaitTime  1
    Input Text  ${MaterialBOMName_txtbx}    SourceBOM
    PO_NewCreationPopUpPage.Click on Save Button
    Wait.DOMready-PopUpCloses
    DOMreadyByWaitTime  5
    Click Element from List   //td[@data-csi-act='Node Name::0']//a[contains(text(),'SourceBOM')]    -1
    DOMready-TabLoads
    Click Element    css=[data-csi-tab='MaterialBOMRevision-TDS']
    DOMready-TabLoads
    Click Element    css=[data-csi-automation='plugin-MaterialBOMRevision-TDS-htmlToolbars']
    DOMready-TabLoads
    # Click Element    css=[data-csi-tab='ProductBOMRevision-PartMaterials']
    # DOMready-TabLoads
    Click Element   css=[data-csi-automation='plugin-ProductBOMRevision-PartMaterials-Actions']       
    DOMready-TabLoads
    Click Element  //td[@class='dijitReset dijitMenuItemLabel' and text()='Sections']            
    Click Element from List    //td[@class='dijitReset dijitMenuItemLabel' and text()='Select Section']    -1
    DOMreadyByWaitTime  5
    PO_SetupPage.Verify Dialog Window  Select BOM Section
    Wait.DOMreadyByWaitTime  5
    Select Single Matching Checkbox elements in the popup-(div-Element)  ${BOMSection3}  
    Select Single Matching Checkbox elements in the popup-(div-Element)  ${BOMSection4}
    Click On Save Button
    Wait.DOMready-PopUpCloses
    DOMready-TabLoads
    Click Element    css=[data-csi-tab='MaterialBOMRevision-TDS']
    DOMready-TabLoads
    # Click Element    css=[data-csi-tab='ProductBOMRevision-PartMaterials']
    # DOMready-TabLoads
    Click Element from List   //div[text()='add']    1
    DOMreadyByWaitTime  5
    PO_NewCreationPopUpPage.Select value from the DDL    ${Placement3}
    DOMreadyByWaitTime  2 
    Click Element    css=[data-csi-tab='MaterialBOMRevision-TDS']
    DOMready-TabLoads
Click on Push Section and Choose BOM, Material and Destination BOM
    [Tags]  Material Specification
    Click Element  css=[data-csi-automation='plugin-ProductBOMRevision-PartMaterials-Actions']          
    DOMready-TabLoads
    Click Element    //td[@class='dijitReset dijitMenuItemLabel' and text()='Sections']                
    Click Element from List    //td[@class='dijitReset dijitMenuItemLabel' and text()='Push Section']    -1
    DOMready-TabLoads  
    PO_SetupPage.Verify Dialog Window  Push BOM Section
    Wait.DOMreadyByWaitTime  5
    Select Single Matching Checkbox elements in the popup-(div-Element)  ${BOMSection3}
    Select Single Matching Checkbox elements in the popup-(div-Element)  ${BOMSection4}
    Click Element    css=[data-csi-act='NextPage']
    DOMreadyByWaitTime  2
    Select Single Matching Checkbox elements in the popup-(div-Element)  ${MaterialName3}
    Click Element    css=[data-csi-act='NextPage']
    DOMreadyByWaitTime  2
    Select Single Matching Checkbox elements in the popup-(div-Element)  DestinationBOM
    Click Element    css=[data-csi-act='Replace']
    DOMreadyByWaitTime  5
Navigate to Destination BOM and verify placements
    [Tags]  Material Specification
    Click Element    //span[@class='dijit dijitReset dijitInline dijitButton csiAction']//following::a[text()='A_MT_NoSize&HasColor']
    DOMready-TabLoads
    Click Element from List   //td[@data-csi-act='Node Name::0']//a[contains(text(),'DestinationBOM')]    -1
    DOMready-TabLoads
    Click Element    css=[data-csi-tab='MaterialBOMRevision-TDS']
    DOMready-TabLoads
    Click Element    css=[data-csi-automation='plugin-MaterialBOMRevision-TDS-htmlToolbars']
    DOMready-TabLoads
    # Click Element    css=[data-csi-tab='ProductBOMRevision-PartMaterials']
    # DOMready-TabLoads
    Element Should be Visible    //span[text()='APlac_03']
    Close Browser
   
    
*** Keywords ***
DataSetup CreateMaterialBOM
  ${This_DataProvider} =  Data_Provider.DataProvider    Create Material BOM
  Set Suite Variable  ${This_DataProvider}
  Log  ${This_DataProvider}
  
  ${BOMSectionNameList} =  Data_Provider.GetDataProviderColumnValue  Data_BOMSectionName
  ${BOMSection1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${BOMSectionNameList}  1
  ${BOMSection2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${BOMSectionNameList}  2
  ${BOMSection3} =  Data_Provider.DataProviderSplitterForMultipleValues  ${BOMSectionNameList}  3
  ${BOMSection4} =  Data_Provider.DataProviderSplitterForMultipleValues  ${BOMSectionNameList}  4
  Set Suite Variable  ${BOMSection1}
  Set Suite Variable  ${BOMSection2}
  Set Suite Variable  ${BOMSection3}
  Set Suite Variable  ${BOMSection4}

  ${MaterialNameList} =  Data_Provider.GetDataProviderColumnValue  Data_MaterialName
  ${MaterialName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MaterialNameList}  1
   ${MaterialName2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MaterialNameList}  2
  ${MaterialName3} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MaterialNameList}  3
  ${MaterialName4} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MaterialNameList}  4
  Set Suite Variable  ${MaterialName1}
  Set Suite Variable  ${MaterialName2}
  Set Suite Variable  ${MaterialName3}
  Set Suite Variable  ${MaterialName4}

  ${PlacementNameList} =  Data_Provider.GetDataProviderColumnValue  Data_PlacementName
  ${Placement1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${PlacementNameList}  1
  ${Placement2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${PlacementNameList}  2
  ${Placement3} =  Data_Provider.DataProviderSplitterForMultipleValues  ${PlacementNameList}  3
  ${Placement4} =  Data_Provider.DataProviderSplitterForMultipleValues  ${PlacementNameList}  4
  Set Suite Variable  ${Placement1}
  Set Suite Variable  ${Placement2}
  Set Suite Variable  ${Placement3}
  Set Suite Variable  ${Placement4}
*** Settings ***

Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt
Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_StylePage.txt 
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../PageObjects/PO_SourcingPage.txt
Resource          ../PageObjects/PO_SpecificationPage.txt
Resource          ../PageObjects/PO_Calendar.txt
Resource          ../Locators/Loc_MaterialPage.txt
Resource          ../Locators/Loc_StyleSeasonsPage.txt
Resource          ../Locators/Loc_SpecificationSizeRangePage.txt
Resource          ../Locators/Loc_SpecificationSizesPage.txt

***Variables***
${CalendarTemplate2}  CalTempManualA-POG
${POGroup1}  PO_GrpForCal
${SupplierPO1}  SupPOForCal
*** Test Cases ***
#Dependant on R-Set-06-Style Samples scenarios using Supplier, Colors, Sizes.txt
#Use POG Calendar template, create Master Calendar in the POG, Apply to PO and verify all the manual activities 

Test Setup1 
    [Tags]  Calendar       
    DataSetup CreateStyleSamples

Login To Application
    [Tags]  Calendar
	#Data provider: It reads the data from the excel sheet for the test case specified. 
	${This_DataProvider} =  Data_Provider.DataProvider  Create Style Samples
    Set Suite Variable  ${This_DataProvider}
    Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    Wait.DOMready
    DOMready-TabLoads


Create PO Group 
  [Tags]  Calendar 
  Click on Home Icon
  DOMready-TabLoads  
  PO_HomePage.Click on Sourcing tab 
  DOMready-TabLoads
  Click Element  ${Loc_Home-Sourcing-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  ${Loc_Sourcing_POGroupTab}
  DOMready-TabLoads 
           
  Mouse Over  ${Loc_POGroup_ActionsButton} 
  Click Element  ${Loc_POGroup_ActionsButton} 
  #Click Element  ${Loc_POGroup_Actions_NewPOGroupLink}
  Wait Until Element Is Visible  ${Loc_POGroupName_TextArea}
  Input Text  ${Loc_POGroupName_TextArea}  ${POGroup1} 
  Click Element  ${Loc_Sourcing_POGroupTab}
  DOMready-TabLoads   
Add Color based order to Style - Sourcing - Supplier PO tab
   [Tags]  Calendar  
   PO_SetupPage.Click on Site Search Field and Hit Search  Style   ${StyleName1}
   DOMreadyByWaitTime  4 
   PO_SetupPage.Click on Required Value From Site Search Result  1
   Wait.DOMready-TabLoads
   ${NavigationStatus} =  Run Keyword And Return Status  Navigate To Hierarchy Tab  Style    
   Run Keyword If  '${NavigationStatus}'=='True'  Click Element  css=[data-csi-tab='Style-Properties']
   DOMready-TabLoads 
   Click Element  css=[data-csi-tab='Style-Properties']
   DOMready-TabLoads 
   Click Element  ${Collection_Style_Sourcing_Tab}
   DOMready-TabLoads  
   Click Element  css=[data-csi-tab="Product-BasePurchaseOrders"]
   DOMready-TabLoads 
   Click Element  css=[data-csi-tab="Product-PurchasedOrders"]
   DOMready-TabLoads  
   Click Element  ${Loc_StyleNode-POs-SupplierPOsTab_ActionsLink}	
   DOMready-PopUpAppears
  ${Element} =  Get Required Element  ${Loc_SupplierPOTab_NewSupplierWindow_SupplierPOName}  -1
  Input Text  ${Element}  ${SupplierPO1}
  Click on the DDL button2  field-PurchasedOrder-Form-POSupplier
  DOMreadyByWaitTime  3
  Select Value From the DDL  ${Supplier1}
  DOMreadyByWaitTime  3 
  #${Check} =  Get Element Attribute  ${Loc_SupplierPOTab_NewSupplierWindow_ColorBasedOrderingCheckbox}  aria-checked
  #Run Keyword If  str('${Check}') == str('true')  
  Click Element  ${Loc_SupplierPOTab_NewSupplierWindow_ColorBasedOrderingCheckbox}
  Click On Next Button
  DOMreadyByWaitTime  5
  Click Element  //td[@data-csi-heading='Node Name::0' and text()='${ColorWay1}']
  Click On Finish Button
  DOMready-PopUpCloses
  DOMreadyByWaitTime  5
  # Added because teh tab is not loading  
  Reload
  DOMready-TabLoads
  DOMreadyByWaitTime  5
  Wait Until Element is Visible  ${PO_Properties_Tab}
  Click Element  ${PO_Properties_Tab}
  DOMready-TabLoads 
  Click Element From List  css=[data-csi-act="__Parent__::0"]  -1
  DOMreadyByWaitTime  3
  Select Value From the DDL   ${POGroup1}
  DOMreadyByWaitTime  3
  #Click Element  ${PO_Properties_Tab}
  #DOMready-TabLoads 
Create A View
  [Tags]  Calendar
  Click Element  ${Loc_POGroup_SupplierPONode_OrdersTab}
  DOMready-TabLoads
  DOMreadyByWaitTime  5
  Wait Until Element is Visible  ${Loc_SupplierPONodeName_OrdersTab_OrderPropertiesTab}  
  Click Element From List  css=[data-csi-automation='plugin-PurchasedOrder-Orders-CustomViewActions'] .dijitButtonText  -1
  Wait.DOMreadyByWaitTime  2
  Click Element From List  css=[data-csi-automation='plugin-PurchasedOrder-Orders-CustomViewManage'] .dijitMenuItemLabel  -1
  Wait.DOMreadyByWaitTime  5
  Wait.DOMreadyByWaitTime  2	
  PO_SetupPage.Click on Default Custom View
  PO_SetupPage.Click on Copy button
  Clear Element Text  ${Loc_SecurityRoleName}
  Selenium2Library.Input Text  ${Loc_SecurityRoleName}  OrdersCVPO
  Wait.DOMreadyByWaitTime  2
  #Click Element		//div[@class='csiPreferenceDisplay']//div[@class='dijitReset dijitRight dijitButtonNode dijitArrowButton dijitDownArrowButton dijitArrowButtonContainer']         
  #Click Element          //div[@class='dijitReset dijitMenuItem' and text()='Material']      
#Add the  attribute to the view 
  Click Element 		css=.csiPreferenceSelect [value='Description::0']
  Wait.DOMreadyByWaitTime  2
  Click Element         //span[text()='< Remove']     
#Add the attribute to the view
  Wait.DOMreadyByWaitTime  2
  Click Element 		css=.csiPreferenceSelect [value='Customer::0']
  Wait.DOMreadyByWaitTime  2
  Click Element         //span[text()='< Remove']
  Wait.DOMreadyByWaitTime  2
  Click Element 		css=.csiPreferenceSelect [value='PortOfOrigin:Child:Customer:0']
  Wait.DOMreadyByWaitTime  2
  Click Element         //span[text()='< Remove']
  Wait.DOMreadyByWaitTime  2
  Click Element From List  Xpath=//span[text()='Save']  -1 
  Wait.DOMreadyByWaitTime  5
Edit the order values    
  # Edit The values 
  [Tags]  Calendar
  Click Element From List  //td[text()='${ColorWay1}']//following::td[@data-csi-act="UnitPerPack::0"]  0
  DOMreadyByWaitTime  3
  Enter The Values on Textarea  //div[contains(@class,'csi-flex-row')]//input[contains(@class,'dijitReset dijitInputInner')]  5
  Click Element  ${Loc_POGroup_SupplierPONode_OrdersTab}
  DOMready-TabLoads
  Click Element From List	//td[text()='${ColorWay1}']/following::td[@data-csi-heading='UnitsPerSize::0' and @data-csi-heading-vi='0']/span[@class='attrPrimary']  0
  DOMreadyByWaitTime  3
  Enter The Values on Textarea  //div[contains(@class,'csi-flex-row')]//input[contains(@class,'dijitReset dijitInputInner')]  2
  Click Element  ${Loc_POGroup_SupplierPONode_OrdersTab}
  DOMready-TabLoads
  Click Element From List	//td[text()='${ColorWay1}']/following::td[@data-csi-heading='UnitsPerSize::0' and @data-csi-heading-vi='1']/span[@class='attrPrimary']  0
  DOMreadyByWaitTime  3
  Enter The Values on Textarea  //div[contains(@class,'csi-flex-row')]//input[contains(@class,'dijitReset dijitInputInner')]  2
  Click Element  ${Loc_POGroup_SupplierPONode_OrdersTab}
  DOMready-TabLoads 
  Element Should Be Visible	//td[text()='${ColorWay1}']//following::td[@data-csi-act="Quantity::0" and contains(text(),"20")]
   
Go to PO Group and add Calendar from Template
  [Tags]  Calendar
  Click on Home Icon
  Wait.DOMready
  DOMready-TabLoads
  PO_HomePage.Click on Sourcing tab 
  DOMready-TabLoads
  Click Element  ${Loc_Home-Sourcing-SupplierPOTab}
  DOMready-TabLoads 
  Click Element  ${Loc_Sourcing_POGroupTab}
  DOMready-TabLoads 
  Click Element From List  //a[text()='${POGroup1}']  -1
  DOMready-TabLoads 
    Click Element  ${Loc_SeasonNode-MasterCalendarTab}
    DOMready-TabLoads
    Click Element  css=[data-csi-automation="plugin-MasterCalendarContainer-MasterCalendar-ToolbarNewActions"]
    DOMready-PopUpAppears
    Select Checkbox in popup  ${CalendarTemplate2}
    DOMreadyByWaitTime  2
    Click On Save Button
    DOMready-PopUpCloses
    Element Should Be Visible  //a[@class='browse' and text()='${CalendarTemplate2}']
Freeze the Calendar
    [Tags]  Calendar
    ${NodeURL} =  Get Element Attribute  //a[text()='${CalendarTemplate2}']//parent::td  data-csi-url
    ${Element} =  Get Required Element  //td//a[text()='${CalendarTemplate2}']//following::td//div//span[text()="more_horiz"]  0
    Mouse Over  ${Element}
    Click Element  ${Element}
    
    Wait Until Element IS Visible  css=table[data-csi-automation='actions-MasterCalendarContainer-MasterCalendar-${NodeURL}'] tr[data-csi-act='Freeze'] td.dijitMenuItemLabel
    Click Element  css=table[data-csi-automation='actions-MasterCalendarContainer-MasterCalendar-${NodeURL}'] tr[data-csi-act='Freeze'] td.dijitMenuItemLabel
    Wait Until Element is Visible  ${Loc_Release} 
    ${Element} =  Get Required Element  //td//a[text()='${CalendarTemplate2}']//following::td[@data-csi-heading="FreezeStatus::0"]//div//input[@aria-checked="true"]  0
    Element Should Be Visible  ${Element}
Apply to PO Group, PO Color, PO Product, Supplier PO
    [Tags]  Calendar 
    ${Element} =  Get Required Element  //td//a[text()='${CalendarTemplate2}']//following::td//div//span[text()="more_horiz"]  0
    Mouse Over  ${Element}
    Click Element  ${Element}
    ${NodeURL} =  Get Element Attribute  //a[text()='${CalendarTemplate2}']//parent::td  data-csi-url
    Wait Until Element IS Visible  css=table[data-csi-automation='actions-MasterCalendarContainer-MasterCalendar-${NodeURL}'] tr[data-csi-act='AggregateCalendar'] td.dijitMenuItemLabel
    Click Element  css=table[data-csi-automation='actions-MasterCalendarContainer-MasterCalendar-${NodeURL}'] tr[data-csi-act='AggregateCalendar'] td.dijitMenuItemLabel
    DOMready-PopUpAppears
    Element Should be Visible  //td[@data-csi-heading='Node Name::0' and text()="${SupplierPO1}"]  
    Click Element  //td[@data-csi-heading='Node Name::0'and text()='${SupplierPO1}']//preceding-sibling::td[contains(@class,'csi-table-selection-column')]//div//input
    PO_NewCreationPopUpPage.Click on Save Button
    Wait Until Element is Visible  ${Loc_Release}    

Refresh Until Create Succeeded is displayed
    [Tags]  Calendar
    :FOR  ${i}  IN RANGE  0  8
    \  ${State} =  Run Keyword And Return Status  Element Should Be Visible  //td[@class='attrBase' and contains(text(),'Create Succeeded')]
    \  Exit For Loop If  '${State}' == 'True' 
    \  DOMreadyByWaitTime  30
    \  Click Element  css=[data-csi-automation="plugin-MasterCalendarContainer-MasterCalendar-refresh"]
    \  Wait Until Element is Visible  ${Loc_Release}

Navigate to PO Group and Style - Supplier PO-Verify the Manual Activities-Fill The completion dates
    [Tags]  Calendar
     PO_SetupPage.Click on Site Search Field and Hit Search  Supplier PO   ${SupplierPO1}
    DOMreadyByWaitTime  4 
    PO_SetupPage.Click on Required Value From Site Search Result  1
    Wait.DOMready-TabLoads
    Click Element  ${Loc_SeasonHierarchy-CalendarTab}
    Wait.DOMready-TabLoads
    Element Should Be Visible  //a[@class='browse' and text()='${CalendarTemplate2}'] 
    Click Element  //a[@class='browse' and text()='${CalendarTemplate2}']  
    Wait.DOMready-TabLoads
    Click Element  ${Loc_MSG-CalendarNode_ActivityTab}
    DOMready-TabLoads   
   Wait.DOMreadyByWaitTime  3
    Element Should Be Visible  //a[contains(text(),'POG_ManualSupplier PO')]
    Click Element  xpath=//a[text()='POG_ManualSupplier PO']//following::td[@data-csi-act='CompletionDate::0']
    ${CurrentDate} =  Get Current Date  result_format=%#m.%#d.%Y
  Log  ${CurrentDate}
  ${CurrentDateAsList} =  Split String  ${CurrentDate}  .
  ${SystemDate} =  Get From List  ${CurrentDateAsList}  1
  ${SystemMonth}=  Get From List  ${CurrentDateAsList}  0
  ${SystemYear} =  Get From List  ${CurrentDateAsList}  2
  Set Suite Variable  ${CurrentDate}
  Set Test Variable  ${SystemDate}
  Set Test Variable  ${SystemMonth}
  Set Test Variable  ${SystemYear}
  Click Element From List  //span[@class="dijitCalendarDateLabel" and text()='${SystemDate}']  -1
  DOMreadyByWaitTime  2
Go To the PO Colors Tab and Verify the Manual Activities-Fill The completion dates
    [Tags]  Calendar
    Click Element  ${Loc_POGroup-CalendarNode-POColorsTab}
    Wait.DOMready-TabLoads
   Wait.DOMreadyByWaitTime  3
    Element Should Be Visible  //a[contains(text(),'${ColorWay1}')]
    Click Element From List  xpath=//a[text()='${ColorWay1}']//following::td[@data-csi-act='CompletionDate:Child:CalendarRollup:0']  0
    ${CurrentDate} =  Get Current Date  result_format=%#m.%#d.%Y
  Log  ${CurrentDate}
  ${CurrentDateAsList} =  Split String  ${CurrentDate}  .
  ${SystemDate} =  Get From List  ${CurrentDateAsList}  1
  ${SystemMonth}=  Get From List  ${CurrentDateAsList}  0
  ${SystemYear} =  Get From List  ${CurrentDateAsList}  2
  Set Suite Variable  ${CurrentDate}
  Set Test Variable  ${SystemDate}
  Set Test Variable  ${SystemMonth}
  Set Test Variable  ${SystemYear}
  Click Element From List  //span[@class="dijitCalendarDateLabel" and text()='${SystemDate}']  -1
  DOMreadyByWaitTime  2
Go To the PO Products Tab and Verify the Manual Activities-Fill The completion dates
    [Tags]  Calendar 
    Click Element  ${Loc_POGroup-CalendarNode-POProductsTab} 
    Wait.DOMready-TabLoads
   Wait.DOMreadyByWaitTime  3
   #${Element} =  Get Required Element  //a[contains(text(),'${StyleName1}')]  0
    #Element Should Be Visible  ${Element}
   Click Element From List  xpath=//a[text()='${StyleName1}']//following::td[@data-csi-act='CompletionDate:Child:CalendarRollup:0']  1
    ${CurrentDate} =  Get Current Date  result_format=%#m.%#d.%Y
  Log  ${CurrentDate}
  ${CurrentDateAsList} =  Split String  ${CurrentDate}  .
  ${SystemDate} =  Get From List  ${CurrentDateAsList}  1
  ${SystemMonth}=  Get From List  ${CurrentDateAsList}  0
  ${SystemYear} =  Get From List  ${CurrentDateAsList}  2
  Set Suite Variable  ${CurrentDate}
  Set Test Variable  ${SystemDate}
  Set Test Variable  ${SystemMonth}
  Set Test Variable  ${SystemYear}
  Click Element From List  //span[@class="dijitCalendarDateLabel" and text()='${SystemDate}']  -1
  DOMreadyByWaitTime  2          

Go back to POG - Calendar-Fill Completion date for POG
  [Tags]  Calendar
  Click Element  //a[@class='text browse' and text()='${POGroup1}']
  DOMready-TabLoads
 
    Click Element  ${Loc_SeasonNode-MasterCalendarTab}
    DOMready-TabLoads
   Click Element  ${Loc_SeasonHierarchy-CalendarTab}
    Wait.DOMready-TabLoads
    Element Should Be Visible  //a[@class='browse' and text()='${CalendarTemplate2}'] 
    Click Element  //a[@class='browse' and text()='${CalendarTemplate2}']  
    Wait.DOMready-TabLoads
    Click Element  ${Loc_MSG-CalendarNode_ActivityTab}
    DOMready-TabLoads   
    Element Should Be Visible  //a[contains(text(),'POG_ManualPO Group')]
    Click Element  xpath=//a[text()='POG_ManualPO Group']//following::td[@data-csi-act='CompletionDate::0']
    ${CurrentDate} =  Get Current Date  result_format=%#m.%#d.%Y
  Log  ${CurrentDate}
  ${CurrentDateAsList} =  Split String  ${CurrentDate}  .
  ${SystemDate} =  Get From List  ${CurrentDateAsList}  1
  ${SystemMonth}=  Get From List  ${CurrentDateAsList}  0
  ${SystemYear} =  Get From List  ${CurrentDateAsList}  2
  Set Suite Variable  ${CurrentDate}
  Set Test Variable  ${SystemDate}
  Set Test Variable  ${SystemMonth}
  Set Test Variable  ${SystemYear}
  Click Element From List  //span[@class="dijitCalendarDateLabel" and text()='${SystemDate}']  -1       
  [Teardown]    Close Browser 
*** Keywords ***
DataSetup CreateStyleSamples
  ${This_DataProvider} =  Data_Provider.DataProvider    Create Style Samples
  Set Suite Variable  ${This_DataProvider}
  Log  ${This_DataProvider}
  
  ${StyleNameList} =  Data_Provider.GetDataProviderColumnValue  Data_StyleName
  ${StyleName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${StyleNameList}  2
  Set Suite Variable  ${StyleName1}
 
  ${ColorSpecificationList} =  Data_Provider.GetDataProviderColumnValue  Data_ColorSpecificationName
  ${ColorSpecificationName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${ColorSpecificationList}  1
  ${ColorSpecificationName2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${ColorSpecificationList}  2
  Set Suite Variable  ${ColorSpecificationName1}
  Set Suite Variable  ${ColorSpecificationName2}
 
  ${ColorWayList} =  Data_Provider.GetDataProviderColumnValue  Data_ColorWayName
  ${ColorWay1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${ColorWayList}  1 
  ${ColorWay2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${ColorWayList}  2  
  Set Suite Variable  ${ColorWay1}
  Set Suite Variable  ${ColorWay2}
   
  ${SupplierList} =  Data_Provider.GetDataProviderColumnValue  Data_SupplierName
  ${Supplier1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SupplierList}  1 
  ${Supplier2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SupplierList}  2   
  Set Suite Variable  ${Supplier1}
  Set Suite Variable  ${Supplier2}
Navigate To Hierarchy Tab
  [Arguments]  ${Locator}
  Click Element    css=[data-csi-tab='${Locator}-Base']
  DOMready-TabLoads 
  Click Element  css=[data-csi-tab='${Locator}-Hierarchy']  
  DOMready-TabLoads 
**

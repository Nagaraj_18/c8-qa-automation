*** Settings ***

Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt
Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_StylePage.txt 
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../PageObjects/PO_SourcingPage.txt
Resource          ../PageObjects/PO_SpecificationPage.txt
Resource          ../PageObjects/PO_Calendar.txt
Resource          ../Locators/Loc_MaterialPage.txt
Resource          ../Locators/Loc_StyleSeasonsPage.txt
Resource          ../Locators/Loc_SpecificationPage.txt
Resource          ../Locators/Loc_SpecificationSizeRangePage.txt
Resource          ../Locators/Loc_SpecificationSizesPage.txt
Resource          ../PageObjects/PO_SpecificationPage.txt
Resource          ../Locators/Loc_MaterialBOM.txt
Resource          ../Locators/Loc_MaterialPlacementPage.txt
Resource          ../Locators/Loc_SpecificationTemplatesPage.txt


**Variables**

@{DPTemplateTypes}  Material
@{DataSheetType}  Material BOM  Material Data Sheet  Routing
${MDPTemplateName}  AUT_MatDPTemp
${SpecDataSheet}  SpecItems1

*** Test Cases ***

Test Setup        
    DataSetup Create Operation Group
Login To Application
    [Tags]  Material Specification
	#Data provider: It reads the data from the excel sheet for the test case specified. 
	${This_DataProvider} =  Data_Provider.DataProvider  Create Material BOM
    Set Suite Variable  ${This_DataProvider}
    Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    DOMready-TabLoads


Create Material Data Package Template under Home - Specification - Templates - Data Package with BOM, Routing and Material Datasheet
  [Tags]  Material Specification
  Click Element  ${Loc_DataSetupLink}
  DOMreadyByWaitTime    2
  Click Element  ${Loc_ProductSpecificationSetupLink}
  DOMready-TabLoads
  Click Element  ${Loc_ProductSpecificationLink}
  DOMreadyByWaitTime    3
#Filter 
    Click Element  //input[@placeholder="Search Data Setup"] 
    Wait.DOMreadyByWaitTime  3
    Input Text  //input[contains(@class,'MuiInputBase-input MuiInput-input MuiInputBase-inputAdornedStart')]  Data Package Templates
    Click Element  //span[text()='Product Specification Setup']
    Wait.DOMreadyByWaitTime  5 
    Mouse Over  ${Loc_DataPackageTemplatesLink}
    Click Element  ${Loc_DataPackageTemplatesLink} 
     
    Wait.DOMreadyByWaitTime  3
#Step : Create a Material Data Package Template.
  :FOR    ${ProductType}    IN ZIP    ${DPTemplateTypes}
  \  Mouse Over  ${Loc_ActionsDataPackageTab}
  \  Click Element  ${Loc_ActionsDataPackageTab}
  \  PO_SetupPage.Verify Dialog Window  New Data Package Template
  \  DOMreadyByWaitTime  2
  \  Input Text  ${Loc_TextFieldDataPackageTemplate}  DataPackTmp-${ProductType}
  \  Wait.DOMreadyByWaitTime  1
  \  PO_NewCreationPopUpPage.Click on the DDL button  field-DataPackageTemplate-Form-TemplateType
  \  Wait.DOMreadyByWaitTime  1
  \  PO_NewCreationPopUpPage.Select value from the DDL  ${ProductType}
  \  Wait.DOMreadyByWaitTime  1
  \  PO_NewCreationPopUpPage.Click on Save Button
  \  DOMready-PopUpCloses
#Step : Update the Data Package Template.
  \  DOMreadyByWaitTime  3	
  \  PO_SpecificationPage.Click on the Data Package Template  DataPackTmp-${ProductType}
  \  DOMreadyByWaitTime  3
  \  DOMready-TabLoads
  
  :FOR    ${DataSheetType}    IN ZIP    ${DataSheetType}    
  \  Mouse Over  ${Loc_ActionsDataSheetTemplatesTab} 
  \  Click Element   ${Loc_ActionsDataSheetTemplatesTab}
  \  PO_SetupPage.Verify Dialog Window  New Data Sheet Template
  \  DOMreadyByWaitTime  1
  \  PO_NewCreationPopUpPage.Click on the DDL button  field-DataPackageTemplateSheet-Form-Type
  \  Wait.DOMreadyByWaitTime  1
  \  PO_NewCreationPopUpPage.Select value from the DDL  ${DataSheetType}
  \  Wait.DOMreadyByWaitTime  2
  \  PO_NewCreationPopUpPage.Click on the DDL button  field-DataPackageTemplateSheet-Form-State
  \  Wait.DOMreadyByWaitTime  1
  \  PO_NewCreationPopUpPage.Select value from the DDL  APPROVED
  \  Wait.DOMreadyByWaitTime  2
  \  PO_NewCreationPopUpPage.Click on Save Button
  \  DOMready-PopUpCloses

Navigate to existing material which has Material BOM, Material Routing and Material Datasheet
      [Tags]  Material Specification
    PO_SetupPage.Click on Site Search Field and Hit Search  Material   ${MaterialName1}
    DOMreadyByWaitTime  2 
    PO_SetupPage.Click on Required Value From Site Search Result  1
    Wait.DOMready-TabLoads
    Click Element  css=[data-csi-tab='Material-MaterialProperties']
    DOMready-TabLoads
  
Create Material Data Package
      [Tags]  Material Specification
    Click Element  css=[data-csi-tab="Material-ProductSpec"]
    DOMready-TabLoads
    Click Element  css=[data-csi-tab="Product-ProductSpecSummary"]
    DOMready-TabLoads
    Click Element  css=[data-csi-automation="plugin-Product-DataPackages-ToolbarNewActions"]
    DOMreadyByWaitTime  3
    Enter The Values on Textarea  //div/textarea  ${MDPTemplateName}
    DOMreadyByWaitTime  1
    Click Element  css=[data-csi-tab="Product-ProductSpecSummary"]
    DOMready-TabLoads 
Add the existing BOM, Routing to the Data Package
      [Tags]  Material Specification
   Click Element  //a[text()='${MDPTemplateName}']
   DOMready-TabLoads
   Click Element  css=[data-csi-tab="DataPackage-DataPackage"]
   DOMready-TabLoads
   Click Element  css=[data-csi-automation="plugin-DataPackage-DataSheets-ToolbarNewActions"]
   DOMready-PopUpAppears
   Click Element  //td[text()='${MaterialRoutingName1}']
   Click Element  //td[contains(text(),'${SpecDataSheet}')]
   Click On Save Button
   DOMready-PopUpCloses
   
Approve all the datasheets
      [Tags]  Material Specification
   Click Element  //a[text()='${MaterialRoutingName1}']
   DOMready-TabLoads

    Click Element  css=[data-csi-automation="plugin-Routing-Breadcrumb-Actions"] .dijitArrowButtonInner
    DOMreadyByWaitTime  4
    Click Element From List  //td[@class='dijitReset dijitMenuItemLabel' and text()='Approve']  -1
    DOMready-TabLoads
    DOMreadyByWaitTime  4
    Click Element  //a[@class='text browse' and text()='${MaterialName1}']
    DOMready-TabLoads
     Click Element  css=[data-csi-tab="Product-ProductSpecSummary"]
    DOMready-TabLoads
    Click Element  //a[text()='${MDPTemplateName}']
   DOMready-TabLoads
   Click Element  css=[data-csi-tab="DataPackage-DataPackage"]
   DOMready-TabLoads
   Click Element  //a[text()='${SpecDataSheet}']
  DOMready-TabLoads

    Click Element  css=[data-csi-automation="plugin-SpecificationDataSheet-Breadcrumb-Actions"] .dijitArrowButtonInner
    DOMreadyByWaitTime  4
    Click Element From List  //td[@class='dijitReset dijitMenuItemLabel' and text()='Approve']  -1
    DOMready-TabLoads
    DOMreadyByWaitTime  4
Navigate to Data Package
      [Tags]  Material Specification
    PO_SetupPage.Click on Site Search Field and Hit Search  Material   ${MaterialName1}
    DOMreadyByWaitTime  2 
    PO_SetupPage.Click on Required Value From Site Search Result  1
    Wait.DOMready-TabLoads
    Click Element  css=[data-csi-tab="Material-ProductSpec"]
    DOMready-TabLoads
    Click Element  css=[data-csi-tab="Product-ProductSpecSummary"]
    DOMready-TabLoads
    Click Element  //a[text()='${MDPTemplateName}']
   DOMready-TabLoads
   Click Element  css=[data-csi-tab="DataPackage-DataPackage"]
   DOMready-TabLoads

 Click on Actions - Generate PDF in Data Package tab toolbar
   [Tags]  Material Specification
  Click Element  css=[data-csi-automation="plugin-DataPackage-Properties-Actions"] .dijitArrowButtonInner
  DOMreadyByWaitTime  3
  Click Element  css=[data-csi-automation="plugin-DataPackage-Properties-GeneratePDF"] .dijitMenuItemLabel
  DOMready-PopUpAppears
  Click On Save button
  DOMreadyByWaitTime  4
    
   [Teardown]    Close Browser    



*** Keywords ***
DataSetup Create Operation Group
  ${This_DataProvider} =  Data_Provider.DataProvider    Create Operation Group
  Set Suite Variable  ${This_DataProvider}
  Log  ${This_DataProvider}
  
  ${MaterialTypesList} =    Data_Provider.GetDataProviderColumnValue    Data_MaterialTypeName
  ${MaterialTypeName1} =    Data_Provider.DataProviderSplitterForMultipleValues    ${MaterialTypesList}    1
  Set Suite Variable    ${MaterialTypeName1}
  
  ${MaterialNameList} =  Data_Provider.GetDataProviderColumnValue  Data_MaterialName
  ${MaterialName1} =  Data_Provider.DataProviderSplitterForMultipleValues    ${MaterialNameList}     1
  Set Suite Variable  ${MaterialName1}

  ${SpecItemTypeList} =   Data_Provider.GetDataProviderColumnValue    Data_SpecItemName    
  ${SpecItemName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SpecItemTypeList}  1
  Set Suite Variable    ${SpecItemName1}
 
  ${MaterialRoutingNameList} =     Data_Provider.GetDataProviderColumnValue    Data_Routing
  ${MaterialRoutingName1} =    Data_Provider.DataProviderSplitterForMultipleValues    ${MaterialRoutingNameList}    1
  Set Suite Variable    ${MaterialRoutingName1}
***
    
  
  
  
  
  
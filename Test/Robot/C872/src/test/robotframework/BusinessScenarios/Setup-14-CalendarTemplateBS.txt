*** Settings ***

Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt
Resource          ../PageObjects/PO_SetupPage.txt
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_EnumerationPage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../PageObjects/PO_TablePage.txt
Resource          ../Locators/Loc_SetupPage.txt    
Resource          ../Locators/Loc_Calendar.txt
Resource          ../Locators/Loc_BusinessPlanning.txt
Resource          ../Locators/Loc_SpecificationPage.txt

***Variables***
${CalendarTemplate1}  CalendarTempMSG 
${CalendarTemplate2}  CalendarTempPOG
${CalendarTemplate3}  CalendarTempSeason
 
*** Test Cases ***  
Test Setup           
    Setup Calendar Data 
Create Calendar Template 
  SuiteSetUp.Open Browser To Login Page
  PO_LoginPage.Input UserName    Data_UserName
  PO_LoginPage.Input Password    Data_Password
  PO_LoginPage.Submit Credentials
  Wait Until Page Contains Element   ${Loc_Release}
  #Reload
  #Click on Setup Icon
  Wait.DOMready
  DOMready-TabLoads
  Click Element  ${Loc_DataSetupLink}
  DOMreadyByWaitTime    3
  Click Element  ${Loc_CalendarAndScheduleSetupLink}
  DOMreadyByWaitTime    4
  Click Element  ${Loc_CalendarTemplatesLink}
  DOMready-TabLoads


Creating the Material Security Group Calendar Template
  # Click on New Template Calendar link and check if a dialog box appears for creation of a new calendar template. Check if the dialog box has three attributes Calendar Type, Template Calendar and Description.
  Mouse Over  ${Loc_Home-CalendarTemplatesTab_ActionsLink}
  Click Element  ${Loc_Home-CalendarTemplatesTab_ActionsLink}
  DOMReady-PopUpAppears
  Click on the DDL Button  field-ProtoCalendar-Form-Level
  DOMReadyByWaitTime  2
  Select value From the DDL  Material Security Group
  Input Text  ${NewCalendarTemplateDialog_DescriptionTextField}  Material Security Group Calendar Template 
  DOMReadyByWaitTime  2
  Input Text  ${NewCalendarTemplateDialog_TemplateCalendarTextField}  ${CalendarTemplate1}
  DOMReadyByWaitTime  2
  Click Element  ${Dialog_SaveButton}  
  Wait Until element is Visible  ${Msg_ConfirmationCalendarTemplateCreation}
  DOMReadyByWaitTime  4
Creating the PO Group Calendar Template     
  Mouse Over  ${Loc_Home-CalendarTemplatesTab_ActionsLink}
  Click Element  ${Loc_Home-CalendarTemplatesTab_ActionsLink}
  DOMReady-PopUpAppears
  Click on the DDL button2  field-ProtoCalendar-Form-Level
  DOMReadyByWaitTime  2
  Select value From the DDL  PO Group
  DOMReadyByWaitTime  2
  Input Text  ${NewCalendarTemplateDialog_DescriptionTextField}  POGRoup Calendar Template
  DOMReadyByWaitTime  2
  Input Text  ${NewCalendarTemplateDialog_TemplateCalendarTextField}  ${CalendarTemplate2}
  Click Element  ${Dialog_SaveButton}  
  Wait Until element is Visible  ${Msg_ConfirmationCalendarTemplateCreation}
  DOMReadyByWaitTime  4
Creating the Seasons Calendar Template 
  Mouse Over  ${Loc_Home-CalendarTemplatesTab_ActionsLink}
  Click Element  ${Loc_Home-CalendarTemplatesTab_ActionsLink}
  DOMReady-PopUpAppears  
  Click on the DDL button2  field-ProtoCalendar-Form-Level
  DOMReadyByWaitTime  2
  Select value From the DDL  Season
  DOMReadyByWaitTime  2
  Input Text  ${NewCalendarTemplateDialog_DescriptionTextField}  Season Calendar Template
  DOMReadyByWaitTime  2
  Input Text  ${NewCalendarTemplateDialog_TemplateCalendarTextField}  ${CalendarTemplate3}
  Click Element  ${Dialog_SaveButton}  
  Wait Until element is Visible  ${Msg_ConfirmationCalendarTemplateCreation}
  DOMReadyByWaitTime  4 
Creating Activities Under Season Calendar Template
  Click Element  //a[@class='browse' and text()='${CalendarTemplate3}']
  DOMready-TabLoads    
  Wait Until Element Is Visible   ${Loc_Release}
  Element Should Be Visible  ${CalendarTemplate_PropertiesSection}
  Element Should Be Visible  ${CalendarTemplate_ActivitiesSection}
# commmenting this temporary to reduce dependency on calendar data  
#Setting the limit Calendar to Style Types Field
  #Click Element  ${CalendarTemplate_PropertiesSection_LimitCalendarToStyleTypesField} 
  #DOMReadyByWaitTime  3
  #Click Checkbox Item in DDL Within a PopUp  ${StyleType1}
  #Click Element  ${Loc_Home-CalendarTemplatesTab}
  #DOMready-TabLoads
  #Element Should Be Visible  //div[text()='Limit Calendar To Style Types']//following::span[text()='${StyleType1}']
  
Creating Activities in Season For Activity Type Custom and Activity levels Brand Collection Colorway Department Style Style SKU   
  Mouse Over  ${Loc_CalendarTemplatesTab_ActivitiesSection_ActionsLink}
  Wait Until Element Is Visible  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}
  @{CustomActivityLevelList} =  Create List  Brand  Collection  Colorway  Department  Style  Style SKU
  :For  ${Activity}  IN ZIP   ${CustomActivityLevelList} 
   \  Click Element  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}   
   \  DOMready-PopUpAppears 
   \  Input Text  css=[data-csi-automation='field-ProtoActivity-Form-Node Name'] .dijitInputInner  S_Custom${Activity} 
   \  Click on the DDL button2  field-ProtoActivity-Form-ActivityType
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  Custom
   \  DOMReadyByWaitTime  2
   \  Click on the DDL button2  field-ProtoActivity-Form-ActivityLevel 
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  ${Activity}
   \  Input Text  css=[data-csi-automation="field-ProtoActivity-Form-AutocompleteExpression"] .dijitInputInner  Season
   \  Click on Save Button
   \  DOMready-PopUpCloses

Creating Activities in Season For Activity Type Manual and Activity levels Brand Collection Colorway Department Style Style SKU   
  Mouse Over  ${Loc_CalendarTemplatesTab_ActivitiesSection_ActionsLink}
  Wait Until Element Is Visible  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}
  @{ManualActivityLevelList} =  Create List  Brand  Collection  Colorway  Department  Style  Style SKU
  :For  ${Activity}  IN ZIP   ${ManualActivityLevelList} 
   \  Click Element  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}   
   \  DOMready-PopUpAppears 
   \  Input Text  css=[data-csi-automation='field-ProtoActivity-Form-Node Name'] .dijitInputInner  S_Manual${Activity} 
   \  Click on the DDL button2  field-ProtoActivity-Form-ActivityType
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  Manual Activity
   \  DOMReadyByWaitTime  2
   \  Click on the DDL button2  field-ProtoActivity-Form-ActivityLevel 
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  ${Activity}
   \  Click on Save Button
   \  DOMready-PopUpCloses
Creating Activities in Season For Activity Type Milestone
   Click Element  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}   
   DOMready-PopUpAppears 
   Input Text  css=[data-csi-automation='field-ProtoActivity-Form-Node Name'] .dijitInputInner  S_Milestone 
   Click on the DDL button2  field-ProtoActivity-Form-ActivityType
   DOMReadyByWaitTime  2
   Select value From the DDL  Milestone
   DOMReadyByWaitTime  2
   #Click on the DDL button2  field-ProtoActivity-Form-Predecessors 
   Click Element  css=[data-csi-automation='field-ProtoActivity-Form-Predecessors'] .dijitArrowButtonInner
   DOMReadyByWaitTime  5
   Click Element From List  //div[@class='dijitReset dijitMenuItem' and @item='2']//div/input  -1
   Click on Save Button
   DOMready-PopUpCloses

Creating Activities in Season For Activity Type Track Activity and Activity levels Artwork Collection Matrix Image Data Sheet Routing Size Chart Size Label Data Sheet Style BOM Style Color Style Main Material  
  Mouse Over  ${Loc_CalendarTemplatesTab_ActivitiesSection_ActionsLink}
  Wait Until Element Is Visible  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}
  @{TrackActivityLevelList} =  Create List  Artwork  Collection Matrix  Image Data Sheet  Routing  Size Chart  Size Label Data Sheet  Style BOM  Style Color  Style Main Material
  :For  ${Activity}  IN ZIP   ${TrackActivityLevelList} 
   \  Click Element  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}
   \  DOMReadyByWaitTime  2   
   \  DOMready-PopUpAppears 
   \  Input Text  css=[data-csi-automation='field-ProtoActivity-Form-Node Name'] .dijitInputInner  S_Track${Activity}
   \  Click on the DDL button2  field-ProtoActivity-Form-ActivityType
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  Track Activity  
   \  DOMReadyByWaitTime  4 
   \  Click on the DDL button2  field-ProtoActivity-Form-TrackingType
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  ${Activity}
   \  DOMReadyByWaitTime  4
   \  Click on the DDL button2  field-ProtoActivity-Form-TrackingPhase 
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  Sample
   \  Click on the DDL button2  field-ProtoActivity-Form-TrackingState 
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  PENDING
   \  Click on Save Button
   \  DOMready-PopUpCloses
Creating Activities in Season For Activity Type Track Activity and Activity levels Care & Composition Collection Review Colorway Review Size Chart Review SKU Review Spec Data Sheet 
  Mouse Over  ${Loc_CalendarTemplatesTab_ActivitiesSection_ActionsLink}
  Wait Until Element Is Visible  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}
  @{TrackActivityLevelList} =  Create List  Care & Composition  Collection Review  Colorway Review  Size Chart Review  SKU Review  Spec Data Sheet 
  :For  ${Activity}  IN ZIP   ${TrackActivityLevelList} 
   \  Click Element  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}   
   \  DOMready-PopUpAppears 
   \  Input Text  css=[data-csi-automation='field-ProtoActivity-Form-Node Name'] .dijitInputInner  S_Track${Activity}
   \  Click on the DDL button2  field-ProtoActivity-Form-ActivityType
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  Track Activity  
   \  DOMReadyByWaitTime  4 
   \  Click on the DDL button2  field-ProtoActivity-Form-TrackingType
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  ${Activity}
   \  DOMReadyByWaitTime  4
   \  Click on the DDL button2  field-ProtoActivity-Form-TrackingState 
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  APPROVED
   \  Click on Save Button
   \  DOMready-PopUpCloses

Creating Activities in Season For Activity Type Track Activity and Activity levels PO Color PO Product Product Shipment 
  Mouse Over  ${Loc_CalendarTemplatesTab_ActivitiesSection_ActionsLink}
  Wait Until Element Is Visible  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}
  @{TrackActivityLevelList} =  Create List   PO Color  PO Product  Product Shipment 
  :For  ${Activity}  IN ZIP   ${TrackActivityLevelList} 
   \  Click Element  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}   
   \  DOMready-PopUpAppears 
   \  Input Text  css=[data-csi-automation='field-ProtoActivity-Form-Node Name'] .dijitInputInner  S_Track${Activity}
   \  Click on the DDL button2  field-ProtoActivity-Form-ActivityType
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  Track Activity  
   \  DOMReadyByWaitTime  4 
   \  Click on the DDL button2  field-ProtoActivity-Form-TrackingType
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  ${Activity}
   \  DOMReadyByWaitTime  2
   \  Click on Save Button
   \  DOMready-PopUpCloses

Creating Activities in Season For Activity Type Track Activity and Activity levels Style Review Test Run
  Mouse Over  ${Loc_CalendarTemplatesTab_ActivitiesSection_ActionsLink}
  Wait Until Element Is Visible  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}
  @{TrackActivityLevelList} =  Create List  Style Review  Test Run 
  :For  ${Activity}  IN ZIP   ${TrackActivityLevelList} 
   \  Click Element  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}   
   \  DOMready-PopUpAppears 
   \  Input Text  css=[data-csi-automation='field-ProtoActivity-Form-Node Name'] .dijitInputInner  S_Track${Activity}
   \  Click on the DDL button2  field-ProtoActivity-Form-ActivityType
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  Track Activity  
   \  DOMReadyByWaitTime  4 
   \  Click on the DDL button2  field-ProtoActivity-Form-TrackingType
   \  DOMReadyByWaitTime  2
   \  Click Element From List  //div[@class='dijitReset dijitMenu dijitComboBoxMenu']//div[@class="dijitMenuItem dijitMenuNextButton" and text()="More choices"]  -1
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  ${Activity}
   \  DOMReadyByWaitTime  4
   \  Click on the DDL button2  field-ProtoActivity-Form-TrackingState 
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  PENDING
   \  Click on Save Button
   \  DOMready-PopUpCloses   
Creating Activities in Season For Activity Type Track Activity and Activity levels Style Material 
  Mouse Over  ${Loc_CalendarTemplatesTab_ActivitiesSection_ActionsLink}
  Wait Until Element Is Visible  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}
   Click Element  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}   
   DOMready-PopUpAppears 
   Input Text  css=[data-csi-automation='field-ProtoActivity-Form-Node Name'] .dijitInputInner  S_TrackStyle Material
   Click on the DDL button2  field-ProtoActivity-Form-ActivityType
   DOMReadyByWaitTime  2
   Select value From the DDL  Track Activity  
   DOMReadyByWaitTime  4 
   Click on the DDL button2  field-ProtoActivity-Form-TrackingType
   DOMReadyByWaitTime  2
   Click Element From List  //div[@class='dijitReset dijitMenu dijitComboBoxMenu']//div[@class="dijitMenuItem dijitMenuNextButton" and text()="More choices"]  -1
   DOMReadyByWaitTime  2
   Select value From the DDL  Style Material
   DOMReadyByWaitTime  4
   Click on the DDL button2  field-ProtoActivity-Form-TrackingPhase 
   DOMReadyByWaitTime  2
   Select value From the DDL  Sample
   Click on the DDL button2  field-ProtoActivity-Form-TrackingState 
   DOMReadyByWaitTime  2
   Select value From the DDL  PENDING
   Click on Save Button
   DOMready-PopUpCloses
########################################################################################
Creating Activities Under PO Group Calendar Template
  Click Element  ${Loc_CalendarTemplatesLink}
  DOMready-TabLoads
  Click Element  //a[@class='browse' and text()='${CalendarTemplate2}']
  DOMready-TabLoads    
  Wait Until Element IS Visible   ${Loc_Release}
  Element Should Be Visible  ${CalendarTemplate_PropertiesSection}
  Element Should Be Visible  ${CalendarTemplate_ActivitiesSection}

  
Creating Activities in PO Group For Activity Type Custom and Activity levels PO Color PO Group PO Product Supplier PO
  Mouse Over  ${Loc_CalendarTemplatesTab_ActivitiesSection_ActionsLink}
  Wait Until Element Is Visible  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}
  @{CustomActivityLevelList} =  Create List  PO Color  PO Group  PO Product  Supplier PO
  :For  ${Activity}  IN ZIP   ${CustomActivityLevelList} 
   \  Click Element  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}   
   \  DOMready-PopUpAppears 
   \  Input Text  css=[data-csi-automation='field-ProtoActivity-Form-Node Name'] .dijitInputInner  POG_Custom${Activity} 
   \  Click on the DDL button2  field-ProtoActivity-Form-ActivityType
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  Custom
   \  DOMReadyByWaitTime  2
   \  Click on the DDL button2  field-ProtoActivity-Form-ActivityLevel 
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  ${Activity}
   \  Input Text  css=[data-csi-automation="field-ProtoActivity-Form-AutocompleteExpression"] .dijitInputInner  PO Group
   \  Click on Save Button
   \  DOMready-PopUpCloses

Creating Activities in PO Group For Activity Type Manual and Activity levels PO Color PO Group PO Product Supplier PO
  Mouse Over  ${Loc_CalendarTemplatesTab_ActivitiesSection_ActionsLink}
  Wait Until Element Is Visible  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}
  @{ManualActivityLevelList} =  Create List  PO Color  PO Group  PO Product  Supplier PO
  :For  ${Activity}  IN ZIP   ${ManualActivityLevelList} 
   \  Click Element  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}   
   \  DOMready-PopUpAppears 
   \  Input Text  css=[data-csi-automation='field-ProtoActivity-Form-Node Name'] .dijitInputInner  POG_Manual${Activity} 
   \  Click on the DDL button2  field-ProtoActivity-Form-ActivityType
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  Manual Activity
   \  DOMReadyByWaitTime  2
   \  Click on the DDL button2  field-ProtoActivity-Form-ActivityLevel 
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  ${Activity}
   \  Click on Save Button
   \  DOMready-PopUpCloses
Creating Activities in PO Group For Activity Type Milestone
   Click Element  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}   
   DOMready-PopUpAppears 
   Input Text  css=[data-csi-automation='field-ProtoActivity-Form-Node Name'] .dijitInputInner  POG_Milestone 
   Click on the DDL button2  field-ProtoActivity-Form-ActivityType
   DOMReadyByWaitTime  2
   Select value From the DDL  Milestone
   DOMReadyByWaitTime  2
      #Click on the DDL button2  field-ProtoActivity-Form-Predecessors 
   Click Element  css=[data-csi-automation='field-ProtoActivity-Form-Predecessors'] .dijitArrowButtonInner 
   DOMReadyByWaitTime  5
   Click Element From List  //div[@class='dijitReset dijitMenuItem' and @item='2']//div/input  -1
   Click on Save Button
   DOMready-PopUpCloses
Creating Activities in PO Group For Activity Type Track Activity and Activity levels Colorway Review Size Chart Review Style Review 
  Mouse Over  ${Loc_CalendarTemplatesTab_ActivitiesSection_ActionsLink}
  Wait Until Element Is Visible  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}
  @{TrackActivityLevelList} =  Create List  Colorway Review  Size Chart Review  Style Review 
  :For  ${Activity}  IN ZIP   ${TrackActivityLevelList} 
   \  Click Element  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}   
   \  DOMready-PopUpAppears 
   \  Input Text  css=[data-csi-automation='field-ProtoActivity-Form-Node Name'] .dijitInputInner  POG_Track${Activity}
   \  Click on the DDL button2  field-ProtoActivity-Form-ActivityType
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  Track Activity  
   \  DOMReadyByWaitTime  4 
   \  Click on the DDL button2  field-ProtoActivity-Form-TrackingType
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  ${Activity}
   \  DOMReadyByWaitTime  4
   \  Click on the DDL button2  field-ProtoActivity-Form-TrackingState 
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  PENDING
   \  Click on Save Button
   \  DOMready-PopUpCloses   

Creating Activities Under Material Security Group Calendar Template 
  Click Element  ${Loc_CalendarTemplatesLink}
  DOMready-TabLoads
  Click Element  //a[@class='browse' and text()='${CalendarTemplate1}']
  DOMready-TabLoads    
  Wait Until Element IS Visible   ${Loc_Release}
  Element Should Be Visible  ${CalendarTemplate_PropertiesSection}
  Element Should Be Visible  ${CalendarTemplate_ActivitiesSection}

  
Creating Activities in Material Security Group For Activity Type Custom and Activity levels Colored Material Material Material Security Group
  Mouse Over  ${Loc_CalendarTemplatesTab_ActivitiesSection_ActionsLink}
  Wait Until Element Is Visible  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}
  @{CustomActivityLevelList} =  Create List  Colored Material  Material  Material Security Group
  :For  ${Activity}  IN ZIP   ${CustomActivityLevelList} 
   \  Click Element  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}   
   \  DOMready-PopUpAppears 
   \  Input Text  css=[data-csi-automation='field-ProtoActivity-Form-Node Name'] .dijitInputInner  MSG_Custom${Activity} 
   \  Click on the DDL button2  field-ProtoActivity-Form-ActivityType
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  Custom
   \  DOMReadyByWaitTime  2
   \  Click on the DDL button2  field-ProtoActivity-Form-ActivityLevel 
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  ${Activity}
   \  Input Text  css=[data-csi-automation="field-ProtoActivity-Form-AutocompleteExpression"] .dijitInputInner  PO Group
   \  Click on Save Button
   \  DOMready-PopUpCloses

Creating Activities in Material Security Group For Activity Type Manual and Activity levels Colored Material Material Material Security Group
  Mouse Over  ${Loc_CalendarTemplatesTab_ActivitiesSection_ActionsLink}
  Wait Until Element Is Visible  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}
  @{ManualActivityLevelList} =  Create List  Colored Material  Material  Material Security Group
  :For  ${Activity}  IN ZIP   ${ManualActivityLevelList} 
   \  Click Element  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}   
   \  DOMready-PopUpAppears 
   \  Input Text  css=[data-csi-automation='field-ProtoActivity-Form-Node Name'] .dijitInputInner  MSG_Manual${Activity} 
   \  Click on the DDL button2  field-ProtoActivity-Form-ActivityType
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  Manual Activity
   \  DOMReadyByWaitTime  2
   \  Click on the DDL button2  field-ProtoActivity-Form-ActivityLevel 
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  ${Activity}
   \  Click on Save Button
   \  DOMready-PopUpCloses
Creating Activities in Material Security Group For Activity Type Milestone
   Click Element  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}   
   DOMready-PopUpAppears 
   Input Text  css=[data-csi-automation='field-ProtoActivity-Form-Node Name'] .dijitInputInner  MSG_Milestone 
   Click on the DDL button2  field-ProtoActivity-Form-ActivityType
   DOMReadyByWaitTime  2
   Select value From the DDL  Milestone
   DOMReadyByWaitTime  2
      #Click on the DDL button2  field-ProtoActivity-Form-Predecessors 
   Click Element  css=[data-csi-automation='field-ProtoActivity-Form-Predecessors'] .dijitArrowButtonInner 
   DOMReadyByWaitTime  5
   Click Element From List  //div[@class='dijitReset dijitMenuItem' and @item='2']//div/input  -1
   Click on Save Button
   DOMready-PopUpCloses
Creating Activities in Material Security Group For Activity Type Track Activity and Activity levels Material BOM Material Color Data Sheet Material Data Sheet Spec Data Sheet Test Run
  Mouse Over  ${Loc_CalendarTemplatesTab_ActivitiesSection_ActionsLink}
  Wait Until Element Is Visible  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}
  @{TrackActivityLevelList} =  Create List  Material BOM  Material Color Data Sheet  Material Data Sheet  Spec Data Sheet  Test Run 
  :For  ${Activity}  IN ZIP   ${TrackActivityLevelList} 
   \  Click Element  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}   
   \  DOMready-PopUpAppears 
   \  Input Text  css=[data-csi-automation='field-ProtoActivity-Form-Node Name'] .dijitInputInner  MSG_Track${Activity}
   \  Click on the DDL button2  field-ProtoActivity-Form-ActivityType
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  Track Activity  
   \  DOMReadyByWaitTime  4 
   \  Click on the DDL button2  field-ProtoActivity-Form-TrackingType
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  ${Activity}
   \  DOMReadyByWaitTime  4
   \  Click on the DDL button2  field-ProtoActivity-Form-TrackingState 
   \  DOMReadyByWaitTime  2
   \  Select value From the DDL  PENDING
   \  Click on Save Button
   \  DOMready-PopUpCloses   

Creating Activities in Material Security Group For Activity Type Track Activity and Activity level Routing
  Mouse Over  ${Loc_CalendarTemplatesTab_ActivitiesSection_ActionsLink}
  Wait Until Element Is Visible  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}
   Click Element  ${Loc_CalendarTemplatesTab_ActivitiesSection_Actions_NewTemplateActivityLink}   
   DOMready-PopUpAppears 
   Input Text  css=[data-csi-automation='field-ProtoActivity-Form-Node Name'] .dijitInputInner  MSG_TrackRouting
   Click on the DDL button2  field-ProtoActivity-Form-ActivityType
   DOMReadyByWaitTime  2
   Select value From the DDL  Track Activity  
   DOMReadyByWaitTime  4 
   Click on the DDL button2  field-ProtoActivity-Form-TrackingType
   DOMReadyByWaitTime  2
   Select value From the DDL  Routing
   DOMReadyByWaitTime  4
   Click on the DDL button2  field-ProtoActivity-Form-TrackingPhase 
   DOMReadyByWaitTime  2
   Select value From the DDL  Sample
   Click on the DDL button2  field-ProtoActivity-Form-TrackingState 
   DOMReadyByWaitTime  2
   Select value From the DDL  PENDING
   Click on Save Button
   DOMready-PopUpCloses   
***Keywords***
Setup Calendar Data
  ${This_DataProvider} =  Data_Provider.DataProvider  Calendar
  Set Suite Variable  ${This_DataProvider}
  
  ${StyleType_Collection} =  Data_Provider.GetDataProviderColumnValue  Data_StyleTypeName
  ${StyleType1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${StyleType_Collection}  1
  Set Suite Variable  ${StyleType1}
  



  
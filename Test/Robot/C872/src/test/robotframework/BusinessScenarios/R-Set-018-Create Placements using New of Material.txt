*** Settings ***

Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt
Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../PageObjects/PO_HomePage.txt
Resource          ../PageObjects/PO_StylePage.txt 
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt
Resource          ../PageObjects/PO_SourcingPage.txt
Resource          ../PageObjects/PO_SpecificationPage.txt
Resource          ../PageObjects/PO_Calendar.txt
Resource          ../Locators/Loc_MaterialPage.txt
Resource          ../Locators/Loc_StyleSeasonsPage.txt
Resource          ../Locators/Loc_SpecificationSizeRangePage.txt
Resource          ../Locators/Loc_SpecificationSizesPage.txt
Resource          ../PageObjects/PO_SpecificationPage.txt
Resource          ../Locators/Loc_MaterialBOM.txt
Resource          ../Locators/Loc_MaterialPlacementPage.txt

**Variables**
${BOMName}  AUT_BOMHSNC

*** Test Cases ***
Test Setup  
    [Tags]  Material Specification      
    DataSetup CreateMaterialBOM
Login To Application
    [Tags]  Material Specification
	#Data provider: It reads the data from the excel sheet for the test case specified. 
	${This_DataProvider} =  Data_Provider.DataProvider  Create Material BOM
    Set Suite Variable  ${This_DataProvider}
    Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    DOMready-TabLoads

    Click on Home Icon
    DOMready-TabLoads
# The sizes/colorspec/MaterialNames Test Data used from Previous script
  
To check the Main Material Checkbox for the Above Material
    [Tags]  Material Specification
    Click on Home Icon
    DOMready-TabLoads
    PO_SetupPage.Click on Site Search Field and Hit Search  Material   ${MaterialName2}
    DOMreadyByWaitTime  2 
    PO_SetupPage.Click on Required Value From Site Search Result  1
    Wait.DOMready-TabLoads
    ${NavigationStatus} =  Run Keyword And Return Status  Navigate To Hierarchy Tab  Material    
    Run Keyword If  '${NavigationStatus}'=='True'  Click Element  css=[data-csi-tab='Material-MaterialProperties']
    DOMready-TabLoads
    Click Element From List  css=[data-csi-automation='plugin-Material-Properties-CustomViewActions'] .dijitButtonText  -1
    Wait.DOMreadyByWaitTime  2
    Click Element From List  css=[data-csi-automation='plugin-Material-Properties-CustomViewManage'] .dijitMenuItemLabel  -1
    Wait.DOMreadyByWaitTime  5
    Click Element  xpath=//td[@role='gridcell']//span[contains(text(),'MainMat')]
    Wait.DOMreadyByWaitTime  2 
    Click Element From List  Xpath=//span[text()='Save']  -1 
    Wait.DOMreadyByWaitTime  5 
    Click Element  css=[data-csi-act="MainMaterial::0"]
    Wait.DOMreadyByWaitTime  2    
    PO_StylePage.Click on Size Range DDL from properties 
    DOMreadyByWaitTime  4



Create BOM Section in Materials
    [Tags]  Material Specification
     Click on Home Icon
    DOMready-TabLoads
    PO_SetupPage.Click on Site Search Field and Hit Search  Material   ${MaterialName2}
    DOMreadyByWaitTime  2 
    PO_SetupPage.Click on Required Value From Site Search Result  1
    Wait.DOMready-TabLoads
    ${NavigationStatus} =  Run Keyword And Return Status  Navigate To Hierarchy Tab  Material    
    Run Keyword If  '${NavigationStatus}'=='True'  Click Element  css=[data-csi-tab='Material-MaterialProperties']
    DOMready-TabLoads
    Click Element  ${MaterialSpec_tab}
    DOMready-TabLoads
    Click Element  ${MaterialBOM_tab}
    DOMready-TabLoads
    Click Element  ${NewMaterialBOM_btn}
    DOMreadyByWaitTime  1
    Click Element From List  ${BOMSubType_list}  -1
    Wait.DOMreadyByWaitTime  2
    Select value from the DDL  Trial MaterialBOMSubtype 001
    DOMreadyByWaitTime  1
    Input Text  ${MaterialBOMName_txtbx}  ${BOMName}
    PO_NewCreationPopUpPage.Click on Save and Go Button
    DOMready-TabLoads
    Reload
    DOMready-TabLoads
    DOMreadyByWaitTime  5
    Click Element  ${MTDimension_tab}
    DOMready-TabLoads

    Element Should Be Visible  //td[@data-csi-heading='Node Name::0' and text()='${Sizes1}']
    Element Should Be Visible  //td[@data-csi-heading='Node Name::0' and text()='${Sizes2}']

Creation of placement From NewOfMaterial 
    [Tags]  Material Specification 
    Click Element  ${MTPlacement_tab}
    DOMready-TabLoads
    DOMreadyByWaitTime  3
    Click Element From List  ${CreatePlacement_btn}  -1
    DOMreadyByWaitTime  2
    Click Element From List  //table[@data-csi-automation='plugin-ProductBOMRevision-PartMaterials-ToolbarNewActionsCreate']//td[contains(text(),'New of Material')]  -1
    DOMreadyByWaitTime  3
    Select Value From the DDL  ${Placement1}
    DOMreadyByWaitTime  4   
    Click Element  ${MTPlacement_tab}
    DOMready-TabLoads
    
    Click Element From List  ${CreatePlacement_btn}  -1
    DOMreadyByWaitTime  2
    Click Element From List  //table[@data-csi-automation='plugin-ProductBOMRevision-PartMaterials-ToolbarNewActionsCreate']//td[contains(text(),'New of Material')]  -1
    DOMreadyByWaitTime  3
    Select Value From the DDL  ${Placement2}
    DOMreadyByWaitTime  4   
    Click Element  ${MTPlacement_tab}
    DOMready-TabLoads
        

#Fill in Placements name, Common color, size, quantity, unit cost, Loss%  
Select the Default View
   [Tags]  Material Specification
  Click Element  ${MTDimension_tab}
  DOMready-TabLoads
  DOMreadyByWaitTime  4  
  Click Element  ${MTPlacement_tab}
  DOMready-TabLoads 
  Click Element From List  css=[data-csi-automation='plugin-ProductBOMRevision-PartMaterials-CustomViewActions'] .dijitButtonText  -1
  Wait.DOMreadyByWaitTime  2
  Click Element From List  css=[data-csi-automation='plugin-ProductBOMRevision-PartMaterials-CustomViewManage'] .dijitMenuItemLabel  -1
  Wait.DOMreadyByWaitTime  5 
  PO_SetupPage.Click on Default Custom View
  Wait.DOMreadyByWaitTime  2 
  Click Element From List  Xpath=//span[text()='Save']  -1 
  Wait.DOMreadyByWaitTime  5 

Choose Product for each placements
    [Tags]  Material Specification
    Run Keyword And Ignore Error  Click Element  //td[@data-csi-heading='Node Name:']//span[@class='csiAction csiActionIcon tableHierarchy iconCollapsed']
    Wait.DOMreadyByWaitTime  2 
    Click Element From List  css=[data-csi-act="Actual::0"]  0
    DOMreadyByWaitTime  3
    Select Value From the DDL  ${MaterialName1}
    Click Element From List  css=[data-csi-act="Actual::0"]  1
    DOMreadyByWaitTime  3
    Select Value From the DDL  ${MaterialName2}
    Wait.DOMreadyByWaitTime  2  
 #Verify Main Material flag if Source Material has true 
  #[Tags]  Material Specification 
  # Element Should Be Visible   //a[text()='${MaterialName2}']//preceding::td/div[@data-csi-act='MainMaterial::0']/input[@aria-checked='true']      
      
Selecting the View to Update UnitCost/Loss%
  [Tags]  Material Specification
 
  Click Element From List  css=[data-csi-automation='plugin-ProductBOMRevision-PartMaterials-CustomViewActions'] .dijitButtonText  -1
  Wait.DOMreadyByWaitTime  2
  Click Element From List  css=[data-csi-automation='plugin-ProductBOMRevision-PartMaterials-CustomViewManage'] .dijitMenuItemLabel  -1
  Wait.DOMreadyByWaitTime  5
  Click Element  xpath=//td[@role='gridcell']//span[contains(text(),'CVUnitCost')]
  Wait.DOMreadyByWaitTime  2 
  Click Element From List  Xpath=//span[text()='Save']  -1 
  Wait.DOMreadyByWaitTime  5   
  

Adding Unit cost to the Placement
    [Tags]  Material Specification
    Run Keyword And Ignore Error  Click Element  //td[@data-csi-heading='Node Name:']//span[@class='csiAction csiActionIcon tableHierarchy iconCollapsed']
    Wait.DOMreadyByWaitTime  2
    Wait.DOMreadyByWaitTime  2
    Click Element From List  css=[data-csi-act="UnitPriceAct::0"]  0
    Wait.DOMreadyByWaitTime  2
    Enter Text Into Textarea  //*[@data-csi-automation='edit-ProductBOMRevision-PartMaterials-UnitPriceAct:']//div//input[@value='0']  10   
    Wait.DOMreadyByWaitTime  2
    Click Element From List  css=[data-csi-act="UnitPriceAct::0"]  1
    Wait.DOMreadyByWaitTime  2   
    Enter Text Into Textarea  //*[@data-csi-automation='edit-ProductBOMRevision-PartMaterials-UnitPriceAct:']//div//input[@value='0']  20
    Wait.DOMreadyByWaitTime  2
    Click Element  ${MTPlacement_tab}
    DOMready-TabLoads
Adding Loss amount to the Placement  
    [Tags]  Material Specification  
    Reload
    DOMready-TabLoads
    Wait.DOMreadyByWaitTime  2 
    Run Keyword And Ignore Error  Click Element  //td[@data-csi-heading='Node Name:']//span[@class='csiAction csiActionIcon tableHierarchy iconCollapsed']
    Wait.DOMreadyByWaitTime  2
    Click Element From List  css=[data-csi-act="LossPct::0"]  0
    Wait.DOMreadyByWaitTime  2
    Enter Text Into Textarea  //*[@data-csi-automation='edit-ProductBOMRevision-PartMaterials-LossPct:']//div//input[@value='0.0']	 20
    Wait.DOMreadyByWaitTime  2
    Click Element From List  css=[data-csi-act="LossPct::0"]  1
    Wait.DOMreadyByWaitTime  2
    Enter Text Into Textarea  //*[@data-csi-automation='edit-ProductBOMRevision-PartMaterials-LossPct:']//div//input[@value='0.0']	 30
    Wait.DOMreadyByWaitTime  2
Selecting the View to Update CommonColor/Size/Qty
  [Tags]  Material Specification
 
  Click Element From List  css=[data-csi-automation='plugin-ProductBOMRevision-PartMaterials-CustomViewActions'] .dijitButtonText  -1
  Wait.DOMreadyByWaitTime  2
  Click Element From List  css=[data-csi-automation='plugin-ProductBOMRevision-PartMaterials-CustomViewManage'] .dijitMenuItemLabel  -1
  Wait.DOMreadyByWaitTime  5
  Click Element  xpath=//td[@role='gridcell']//span[contains(text(),'CVColSizeQty')]
  Wait.DOMreadyByWaitTime  2 
  Click Element From List  Xpath=//span[text()='Save']  -1
  Wait.DOMreadyByWaitTime  5 
   
Verify the Color, Size cells enabled according to material type
    [Tags]  Material Specification
    Run Keyword And Ignore Error  Click Element  //td[@data-csi-heading='Node Name:']//span[@class='csiAction csiActionIcon tableHierarchy iconCollapsed']
    Wait.DOMreadyByWaitTime  3
 
    ${Element} =  Get Required Element  //*[text()='${MaterialTypeName1}']//following::td[@data-csi-act='CommonColor::0' and contains(@class,'iconEditable')]  0
    Element Should Be Visible  ${Element}
    ${Element} =  Get Required Element  //*[text()='${MaterialTypeName1}']//following::td[@data-csi-act='CommonSize::0' and contains(@class,'iconEditable')]  0
    Element Should Be Visible  ${Element} 
    Element Should Not Be Visible   //*[text()='${MaterialTypeName2}']//following::td[@data-csi-act='CommonColor::0' and contains(@class,'iconEditable')]
    Element Should Be Visible   //*[text()='${MaterialTypeName2}']//following::td[@data-csi-act='CommonSize::0' and contains(@class,'iconEditable')]
   

Adding Common Size to the Placement
    [Tags]  Material Specification
    Wait.DOMreadyByWaitTime  2
    Click Element From List  css=[data-csi-act="CommonSize::0"]  0
    Wait.DOMreadyByWaitTime  2
    Select Value From The DDL  ${Sizes1} 
    Wait.DOMreadyByWaitTime  2
    Click Element  ${MTPlacement_tab}
    DOMready-TabLoads
          
Adding Common Color to the Placement
    [Tags]  Material Specification
    Wait.DOMreadyByWaitTime  2
    Click Element From List  css=[data-csi-act="CommonColor::0"]  0
    Wait.DOMreadyByWaitTime  2
    Select Value From The DDL  ${ColoredMaterial1} 
    Wait.DOMreadyByWaitTime  2
    Click Element  ${MTPlacement_tab}
    DOMready-TabLoads     

Adding Common Quantity to the Placement
    [Tags]  Material Specification
    Wait.DOMreadyByWaitTime  2
    Click Element From List  css=[data-csi-act="QtyDefault::0"]  0
    Wait.DOMreadyByWaitTime  2
    Enter Text Into Textarea  //*[@data-csi-automation='edit-ProductBOMRevision-PartMaterials-QtyDefault:']//div//input[@value='0']  10
    Wait.DOMreadyByWaitTime  2
    Click Element  ${MTPlacement_tab}
    DOMready-TabLoads     
    Click Element From List  css=[data-csi-act="QtyDefault::0"]  1
    Wait.DOMreadyByWaitTime  2   
    Enter Text Into Textarea  //*[@data-csi-automation='edit-ProductBOMRevision-PartMaterials-QtyDefault:']//div//input[@value='0']	 10
    Wait.DOMreadyByWaitTime  2
    Click Element  ${MTPlacement_tab}
    DOMready-TabLoads  
 
  [Teardown]    Close Browser
*** Keywords ***
DataSetup CreateMaterialBOM
  ${This_DataProvider} =  Data_Provider.DataProvider  Create Material BOM
  Set Suite Variable  ${This_DataProvider}
  Log  ${This_DataProvider}
  ${MaterialTypeList} =  Data_Provider.GetDataProviderColumnValue  Data_MaterialTypeName
  ${MaterialTypeName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MaterialTypeList}  1
  ${MaterialTypeName2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MaterialTypeList}  2
  ${MaterialTypeName3} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MaterialTypeList}  3
  ${MaterialTypeName4} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MaterialTypeList}  4
  Set Suite Variable  ${MaterialTypeName1}
  Set Suite Variable  ${MaterialTypeName2}
  Set Suite Variable  ${MaterialTypeName3}
  Set Suite Variable  ${MaterialTypeName4}

  ${MaterialNameList} =  Data_Provider.GetDataProviderColumnValue  Data_MaterialName
  ${MaterialName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MaterialNameList}  1
  ${MaterialName2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MaterialNameList}  2
  ${MaterialName3} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MaterialNameList}  3
  ${MaterialName4} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MaterialNameList}  4
  Set Suite Variable  ${MaterialName1}
  Set Suite Variable  ${MaterialName2}
  Set Suite Variable  ${MaterialName3}
  Set Suite Variable  ${MaterialName4}
  
  ${MaterialDescription} =  GetDataProviderColumnValue  Data_MaterialDescription
  ${MaterialDesc1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MaterialDescription}  1
  ${MaterialDesc2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MaterialDescription}  2
  ${MaterialDesc3} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MaterialDescription}  3
  ${MaterialDesc4} =  Data_Provider.DataProviderSplitterForMultipleValues  ${MaterialDescription}  4
  Set Suite Variable  ${MaterialDesc1}
  Set Suite Variable  ${MaterialDesc2}
  Set Suite Variable  ${MaterialDesc3}
  Set Suite Variable  ${MaterialDesc4}
  
  ${ColorSpecificationList} =  Data_Provider.GetDataProviderColumnValue  Data_ColorSpecificationName
  ${ColorSpecificationName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${ColorSpecificationList}  1
  ${ColorSpecificationName2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${ColorSpecificationList}  2
  Set Suite Variable  ${ColorSpecificationName1}
  Set Suite Variable  ${ColorSpecificationName2}
  
  ${ColorNameCollection} =  GetDataProviderColumnValue  Data_ColorSpecificationName
  ${ColorName1} =  DataProviderSplitterForMultipleValues  ${ColorNameCollection}  1
  ${ColorName2} =  DataProviderSplitterForMultipleValues  ${ColorNameCollection}  2
  Set Suite Variable  ${ColorName1}
  Set Suite Variable  ${ColorName2}

  ${SizesList} =  Data_Provider.GetDataProviderColumnValue  Data_SizeName
  ${Sizes1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SizesList}  1 
  ${Sizes2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SizesList}  2   
  Set Suite Variable  ${Sizes1}
  Set Suite Variable  ${Sizes2}
 
  
  ${SizeRangeList} =  Data_Provider.GetDataProviderColumnValue  Data_SizeRangeName
  ${SizeRange1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SizeRangeList}  1    
  Set Suite Variable  ${SizeRange1}

  ${ColoredMaterialList} =  Data_Provider.GetDataProviderColumnValue  Data_ColoredMaterialName
  ${ColoredMaterial1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${ColoredMaterialList}  1  
  ${ColoredMaterial2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${ColoredMaterialList}  2
  ${ColoredMaterial3} =  Data_Provider.DataProviderSplitterForMultipleValues  ${ColoredMaterialList}  3
  ${ColoredMaterial4} =  Data_Provider.DataProviderSplitterForMultipleValues  ${ColoredMaterialList}  4
  Set Suite Variable  ${ColoredMaterial1}
  Set Suite Variable  ${ColoredMaterial2}
  Set Suite Variable  ${ColoredMaterial3}
  Set Suite Variable  ${ColoredMaterial4}

  ${BOMSectionNameList} =  Data_Provider.GetDataProviderColumnValue  Data_BOMSectionName
  ${BOMSection1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${BOMSectionNameList}  1
  ${BOMSection2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${BOMSectionNameList}  2
  ${BOMSection3} =  Data_Provider.DataProviderSplitterForMultipleValues  ${BOMSectionNameList}  3
  ${BOMSection4} =  Data_Provider.DataProviderSplitterForMultipleValues  ${BOMSectionNameList}  4
  Set Suite Variable  ${BOMSection1}
  Set Suite Variable  ${BOMSection2}
  Set Suite Variable  ${BOMSection3}
  Set Suite Variable  ${BOMSection4}
  
  ${PlacementNameList} =  Data_Provider.GetDataProviderColumnValue  Data_PlacementName
  ${Placement1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${PlacementNameList}  1
  ${Placement2} =  Data_Provider.DataProviderSplitterForMultipleValues  ${PlacementNameList}  2
  ${Placement3} =  Data_Provider.DataProviderSplitterForMultipleValues  ${PlacementNameList}  3
  ${Placement4} =  Data_Provider.DataProviderSplitterForMultipleValues  ${PlacementNameList}  4
  Set Suite Variable  ${Placement1}
  Set Suite Variable  ${Placement2}
  Set Suite Variable  ${Placement3}
  Set Suite Variable  ${Placement4}

Enter Text Into Textarea
    [Arguments]   ${Element}  ${Input}
    DOMreadyByWaitTime  2
    # Press backspace button
    Selenium2Library.Press Key  ${Element}  0
    DOMreadyByWaitTime  2
    Selenium2Library.Press Key  ${Element}  ${Input}
Navigate To Hierarchy Tab
  [Arguments]  ${Locator}
  Click Element    css=[data-csi-tab='${Locator}-Base']
  DOMready-TabLoads 

***

*** Settings ***
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/SuiteSetUp.txt
Resource          ../PageObjects/PO_LoginPage.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../Locators/Loc_StyleSeasonsPage.txt
Resource          ../Locators/Loc_SpecificationPage.txt
Resource          ../PageObjects/PO_NewCreationPopUpPage.txt

*** Test Cases ***
Test Setup
    DataSetup CreateStyleBS
    
Login To Application  
    [Tags]  Hierarchy  
	${This_DataProvider} =  Data_Provider.DataProvider  Create Style BS
    Set Suite Variable  ${This_DataProvider}
    Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    PO_LoginPage.Submit Credentials
    Wait.DOMready
    DOMready-TabLoads

Copying the Collection
    [Tags]  Hierarchy
    PO_SetupPage.Click on Site Search Field and Hit Search  Department   ${DepartmentName1}
    DOMreadyByWaitTime  4 
    PO_SetupPage.Click on Required Value From Site Search Result  1
    DOMready-TabLoads
    Click Element    //a[text()='${CollectionName1}']//following::td//span[@data-csi-act='Copy']
    PO_SetupPage.Verify Dialog Window  Copy
    Wait.DOMreadyByWaitTime  2
    Click On Save Button
    Wait.DOMready-PopUpCloses
    Wait.DOMreadyByWaitTime  5
    
Verify the Destination colection that the Code, Description are not copied
    [Tags]  Hierarchy
    Element Should Be Visible    //td[text()='0']//preceding::td//a[text()='${CollectionName1}']
    Wait.DOMreadyByWaitTime  2
    Click Element from List   //a[@class='browse' and text()='${CollectionName1}']    1
    DOMready-TabLoads
    Element Should Be Visible    //td[@class='attrString iconEditable' and @data-csi-heading='Code::0']
    ${codevalue} =  Get Text    //td[@class='attrString iconEditable' and @data-csi-heading='Code::0']
    ${Descpvalue} =  Get Text    //td[@data-csi-act='Description::0' and @class='attrString iconEditable']
    # Log    ${value}   
    Run Keyword If  str('${codevalue}') == str('')  Log  'Executed'
    Run Keyword If  str('${Descpvalue}') == str('')  Log  'Executed'
    Wait.DOMreadyByWaitTime  2
    Click Element    //a[@title='Department: RF_Dept' and text()='${DepartmentName1}']
    DOMready-TabLoads
    Click Element from List   //a[text()='${CollectionName1}']//following::td//span[@data-csi-act='Delete']    1
    Wait.DOMreadyByWaitTime  5
    # PO_SetupPage.Verify Dialog Window  Delete Brand?
    Click Element   //span[@class='dijitReset dijitInline dijitButtonText' and text()='Delete']    
    # Wait.DOMready-PopUpCloses
    Wait.DOMreadyByWaitTime  5
    Element Should Not Be Visible    //td[text()='0']//preceding::td//a[text()='${CollectionName1}']
    Wait.DOMreadyByWaitTime  2
    Close Browser
    
*** Keywords ***
DataSetup CreateStyleBS
  ${This_DataProvider} =  Data_Provider.DataProvider    Create Style BS
  Set Suite Variable  ${This_DataProvider}
  Log  ${This_DataProvider}

  ${SeasonList} =  Data_Provider.GetDataProviderColumnValue  Data_SeasonName
  ${SeasonName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${SeasonList}  1    
  Set Suite Variable  ${SeasonName1}
  
  ${BrandList} =  Data_Provider.GetDataProviderColumnValue  Data_BrandName
  ${BrandName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${BrandList}  1    
  Set Suite Variable  ${BrandName1}
  
  ${DepartmentList} =  Data_Provider.GetDataProviderColumnValue  Data_DepartmentName
  ${DepartmentName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${DepartmentList}  1    
  Set Suite Variable  ${DepartmentName1}
  
  ${CollectionList} =  Data_Provider.GetDataProviderColumnValue  Data_CollectionName
  ${CollectionName1} =  Data_Provider.DataProviderSplitterForMultipleValues  ${CollectionList}  1    
  Set Suite Variable  ${CollectionName1}
    
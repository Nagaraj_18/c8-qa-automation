*** Settings ***
Resource          ../UtilityHelper/SuiteSetUp.txt    
Resource          ../UtilityHelper/Report_Properties.txt
Resource          ../UtilityHelper/Data_Provider.txt
Resource          ../UtilityHelper/Wait.txt
Resource          ../UtilityHelper/Assertions.txt

Resource          ../PageObjects/PO_SetupPage.txt    
Resource          ../Locators/Loc_SetupPage.txt    
Resource          ../PageObjects/PO_LoginPage.txt
*** Test Cases ***
Run Update Configuration
	[Tags]  Setup
	${This_DataProvider} =  Data_Provider.DataProvider  Run Update Configuration
    Set Test Variable  ${This_DataProvider}
    #Reload
   	#Reload
   	Open Browser To Login Page
    PO_LoginPage.Input UserName    Data_UserName
    PO_LoginPage.Input Password    Data_Password
    SuiteSetUp.Test Step    ${R_Click_Login}
    PO_LoginPage.Submit Credentials
    Wait.DOMready
    DOMready-TabLoads
    Click on Setup Icon
    DOMready-TabLoads
   :FOR  ${i}  IN RANGE  0  5
    #\  PO_SetupPage.Click on Settings Icon
    \  ${Status} =  Run Keyword And Return Status  Wait Until Page Contains Element  ${Loc_UpdateConfigurationLink}
    \  Exit For Loop If  '${Status}'=='True'  
    # Step : Update Configuration
    #PO_SetupPage.Click on Update Configuration Link
    Click Element From List  ${Loc_UpdateConfigurationLink}  -1
    DOMreadyByWaitTime  10
       ${ClickStatus} =  Run Keyword And Return Status  Click Element From List  css=Input[name='UpdateConfiguration']  -1
    
    #Wait for Update Configuration PopUp
    Click Element  //span[text()="Run"] 
    # Waits for Seven minutes for Update Configuration to complete
    :FOR  ${i}  IN RANGE  0  14
    \  #${State} =  Run Keyword And Return Status  Wait Until Element Is Visible  //span[contains(text(),'completed')]
    \  #Exit For Loop If  '${State}' == 'True' 
    \  DOMreadyByWaitTime  30
     #Reload
     #Wait.DOMready
     #DOMready-TabLoads
    #[Teardown]    Close Browser 
    
 
 ***keywords***
  
 Run Update Config 
      Click Element  //span[text()="Run"]
    
   
     
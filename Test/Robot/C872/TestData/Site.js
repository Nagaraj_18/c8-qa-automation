define([
	"csi/api",
	"csi/types",
	"csi/xml"
], function(api,types,xml) {
	// 1) API calls - reference https://centric8.atlassian.net/wiki/display/AAT/Site.js
		api.setSamplesQuantityMax(40);
		api.addCommentPath("Style", "Child:RealizedProducts");
		api.hideRevisionAction("PriceList", "Approve");
		api.hideAction("Collection", "Styles", "MoveFromStyle"); 
		api.hideAction("StructureItem", "AllStyles", "MoveStyleForm"); 
		api.hideAction("StructureItem", "AllStyles", "MoveStyleWizard");
		api.allowNoneOptionForSKUCreation(true);
		api.allow3DDocumentOnType("Style", false);
		api.allow3DDocumentOnType("SKU", true);
		api.allowReferenceDocumentsOnType("Style", false);
		api.allowReferenceDocumentsOnType("Material", false);
		api.allowReferenceDocumentsOnType("ColorMaterial", false);
	return {
		// 2) Functions referenced from Site.xml

	};
});
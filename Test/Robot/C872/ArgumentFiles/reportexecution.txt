..\src\test\robotframework\acceptance\TC_CreateBOMType.txt
..\src\test\robotframework\acceptance\TC_CreateContractualDocumentType.txt
..\src\test\robotframework\acceptance\TC_CreateEnumeration.txt
..\src\test\robotframework\acceptance\TC_CreateImageDirectory.txt
..\src\test\robotframework\acceptance\TC_CreateMaterialType.txt
..\src\test\robotframework\acceptance\TC_CreateMerchandiseType.txt
..\src\test\robotframework\acceptance\TC_CreateMerchBOCustomAttribute.txt
..\src\test\robotframework\acceptance\TC_CreateSpecLibraryItemType.txt
..\src\test\robotframework\acceptance\TC_CreateStyleTypeAndAttribute.txt
..\src\test\robotframework\acceptance\TC_CreateStyleTypeDataSheet.txt
..\src\test\robotframework\acceptance\TC_CreateTestType.txt
..\src\test\robotframework\acceptance\TC_UpdateConfiguration.txt
..\src\test\robotframework\Calendar\CalendarTemplate01645-01655_Config.txt

..\src\test\robotframework\Sourcing\TC_DataSetup_Supplier_PO.txt
..\src\test\robotframework\Sourcing\TC_SourcingCustomerPO03210-3240.txt
..\src\test\robotframework\Sourcing\TC_SourcingCustomerPO03241-3275.txt

..\src\test\robotframework\acceptance\TC_PendingConfigCases.txt
..\src\test\robotframework\acceptance\TC_CreateAdminUserRole.txt
..\src\test\robotframework\WBS\TC_WBS_UsersRoles.txt
# Calendar Cases
..\src\test\robotframework\Calendar\Calendar01656-01680_DataSetupStart1.txt
..\src\test\robotframework\Calendar\Calendar01656-01680_DataSetupStart2.txt
..\src\test\robotframework\Calendar\Calendar01693-1705_DataSetupStart3.txt
..\src\test\robotframework\Calendar\POCalendarDataSetup.txt
# Rest of Data setup test cases
..\src\test\robotframework\acceptance\TC_CreatePlacement.txt
..\src\test\robotframework\acceptance\TC_CreateColorSpecification.txt
..\src\test\robotframework\acceptance\TC_CreateSizeRange.txt
..\src\test\robotframework\acceptance\TC_CreateSizeMap.txt
..\src\test\robotframework\acceptance\TC_CreateDimensions.txt
..\src\test\robotframework\acceptance\TC_CreateIncrements.txt
..\src\test\robotframework\acceptance\TC_CreateProductGroup.txt
..\src\test\robotframework\acceptance\TC_CreateSizeLabel.txt
..\src\test\robotframework\acceptance\TC_CreateSpecItems.txt
..\src\test\robotframework\acceptance\TC_CreateProductAlternativeSpecifications.txt
..\src\test\robotframework\acceptance\TC_CreateOperationGroup.txt
..\src\test\robotframework\acceptance\TC_CreateCapability.txt
..\src\test\robotframework\acceptance\TC_CreateCountry.txt
..\src\test\robotframework\acceptance\TC_CreateHTSCode.txt
..\src\test\robotframework\acceptance\TC_CreateShippingPort.txt
..\src\test\robotframework\acceptance\TC_CreateShippingContainer.txt
..\src\test\robotframework\acceptance\TC_CreateShippingRate.txt
..\src\test\robotframework\acceptance\TC_CreateSupplier.txt
..\src\test\robotframework\acceptance\TC_CreateFactory.txt
..\src\test\robotframework\acceptance\TC_CreateSubRouting.txt
..\src\test\robotframework\Calendar\Calendar01693-1706_DataSetupEnd2.txt
..\src\test\robotframework\Calendar\CalendarPropogation_01773-01784.txt
..\src\test\robotframework\acceptance\TC_CreateRoutingTemplate.txt
..\src\test\robotframework\acceptance\TC_CreateStyleDataPackageTemplates.txt
..\src\test\robotframework\acceptance\TC_CreateMaterialDataPackageTemplates.txt
..\src\test\robotframework\acceptance\TC_CreateSupplierQuoteTemplate.txt
..\src\test\robotframework\acceptance\TC_CreateSupplierRequestTemplate.txt
..\src\test\robotframework\acceptance\TC_CreateSizeChartTemplates.txt
..\src\test\robotframework\acceptance\TC_CreateSpecDataSheetTemplates.txt
..\src\test\robotframework\acceptance\TC_CreateMaterialHierarchy.txt
..\src\test\robotframework\acceptance\TC_CreateStyleBOMTemplate.txt
..\src\test\robotframework\Calendar\BrandCalendarPropogation_01785-01853.txt
..\src\test\robotframework\acceptance\TC_CreateStyleHierarchy.txt
#..\src\test\robotframework\acceptance\TC_CreateHierarchyItemTemplates.txt

#..\src\test\robotframework\acceptance\TC_CreateQualityTestSpec.txt
..\src\test\robotframework\StoryValidation\TC_CreateCompositions.txt
..\src\test\robotframework\acceptance\TC_CreateMaterialProductSymbols.txt
..\src\test\robotframework\acceptance\TC_CreateColorSpecificationLibraries.txt
..\src\test\robotframework\acceptance\TC_CreateCareLabelLibraries.txt
..\src\test\robotframework\acceptance\TC_CreateSizeLabelLibraries.txt
..\src\test\robotframework\acceptance\TC_CreateMaterialLibraries.txt
# Test Cases that are not part of Data Setup cases 
..\src\test\robotframework\Calendar\StyleCalendarFunctionalityValidation_01854-0277.txt
..\src\test\robotframework\Materials\MaterialSupplierRequest.txt
..\src\test\robotframework\acceptance\TC_CreateCurrency.txt
..\src\test\robotframework\Materials\TC_MaterialSourcingTabValidation.txt
# moved from Milestone1
..\src\test\robotframework\ContractualDocuments\Home-Sourcing-Customers.txt
..\src\test\robotframework\Calendar\SKUCalendarPropogation.txt

# moved from independent tests 
..\src\test\robotframework\CreateUsers\TC_CreateOnlineSupplierAdminAndMapSecurityRole.txt
..\src\test\robotframework\CreateUsers\TC_CreateOnlineSupplierGlobalAdminAndMapSecurityRole.txt

#..\src\test\robotframework\Merchandising\MerchandisingHierarchySetup.txt
..\src\test\robotframework\Merchandising\MerchandisingHierarchy.txt
..\src\test\robotframework\ContractualDocuments\Material_Specification787-823.txt

..\src\test\robotframework\WBS\TC_WBS_03473_WBS_03484.txt
..\src\test\robotframework\WBS\TC_WBS_03485_WBS_3510.txt

..\src\test\robotframework\HomeLevelTabs\AssignSecurityRolesToLocalUser.txt
..\src\test\robotframework\HomeLevelTabs\TC_SupplierCollaborationTestData.txt 
..\src\test\robotframework\HomeLevelTabs\TC_OnlineSupplierLogin.txt
..\src\test\robotframework\HomeLevelTabs\TC_SupplierCollaboration3446-3461.txt

..\src\test\robotframework\Sourcing\TC_Sourcing02956-02974.txt
..\src\test\robotframework\Sourcing\TC_Sourcing02976-2994.txt
..\src\test\robotframework\Sourcing\TC_Sourcing02995-3006.txt
..\src\test\robotframework\Sourcing\TC_Sourcing03007-3020.txt
..\src\test\robotframework\Sourcing\TC_Sourcing03021-3034.txt
..\src\test\robotframework\Sourcing\TC_Sourcing03035-3055.txt
..\src\test\robotframework\Sourcing\TC_Sourcing03124-3134.txt
..\src\test\robotframework\Sourcing\TC_Sourcing03135-3149.txt
..\src\test\robotframework\Sourcing\TC_Sourcing03150-3161.txt
..\src\test\robotframework\Sourcing\TC_Sourcing03162-3175.txt
..\src\test\robotframework\Sourcing\TC_Sourcing03176-3195.txt
..\src\test\robotframework\Sourcing\TC_Sourcing03196-03209.txt

..\src\test\robotframework\Style\TC_SizeChartsShape_TestData.txt
..\src\test\robotframework\Style\TC_SizeCharts02291-02349.txt
..\src\test\robotframework\Style\TC_SizeCharts02350-02363.txt
..\src\test\robotframework\Style\TC_SizeChartCompareIncrements.txt
..\src\test\robotframework\Style\TC_SizeCharts.txt

..\src\test\robotframework\acceptance\TC_CreateProductSymbol.txt
..\src\test\robotframework\acceptance\TC_MaterialComposition.txt

..\src\test\robotframework\Materials\TC_MaterialLibrary572-581.txt
..\src\test\robotframework\Materials\TC_MaterialLibrary582-606.txt
..\src\test\robotframework\Materials\TC_MaterialLibrary607-631.txt
..\src\test\robotframework\Materials\TC_MaterialLibrary632-648.txt
..\src\test\robotframework\Materials\TC_MaterialLibrary649-663.txt
..\src\test\robotframework\Materials\TC_MaterialLibrary664-679.txt
..\src\test\robotframework\Materials\TC_MaterialRouting764-775.txt

..\src\test\robotframework\ContractualDocuments\ContractualDocuments.txt
..\src\test\robotframework\ContractualDocuments\Supplier-Contractual Documents.txt
..\src\test\robotframework\ContractualDocuments\Material_Specification703-729.txt
..\src\test\robotframework\ContractualDocuments\Material_Specification730-763.txt
..\src\test\robotframework\Materials\MaterialSupplierRequest1.txt
..\src\test\robotframework\Materials\MaterialSupplierRequest2.txt

..\src\test\robotframework\acceptance\TC_CreateCareLabel.txt
..\src\test\robotframework\Materials\TC_CreateMaterialQuality.txt
..\src\test\robotframework\Materials\TC_MaterialSQT.txt
..\src\test\robotframework\Materials\TC_MaterialSQTCreation.txt
..\src\test\robotframework\Materials\MaterialSQT_BlendedCostscenario.txt

..\src\test\robotframework\CollectionManagement\TC_SalesMarket.txt
..\src\test\robotframework\CollectionManagement\TC_MarketingLooks.txt
..\src\test\robotframework\CollectionManagement\TC_MarketingTools.txt
..\src\test\robotframework\CollectionManagement\TC_MarketingCollections.txt
..\src\test\robotframework\CollectionManagement\TC_Collection_Management2.txt
..\src\test\robotframework\CollectionManagement\TC_CatalogueConfiguration.txt

#..\src\test\robotframework\CollectionManagement\TC_Quality1_TestGroup_Templates.txt
#..\src\test\robotframework\CollectionManagement\TC_Quality2_ColoredMaterial.txt
#..\src\test\robotframework\CollectionManagement\TC_Quality3_Samples_SecurityGroup.txt
#..\src\test\robotframework\CollectionManagement\TC_Quality4_MSG_ColColorwaySample.txt

..\src\test\robotframework\Themes\TC_ShapeAndThemeTab1.txt
..\src\test\robotframework\Themes\TC_ShapeAndThemeThemes2.txt
..\src\test\robotframework\Themes\TC_ShapeAndThemeStyles3.txt
..\src\test\robotframework\Themes\TC_ShapeAndThemeThemeSecurityGroup4.txt
..\src\test\robotframework\Themes\TC_ShapeAndThemeMaterials5.txt
..\src\test\robotframework\Themes\TC_ShapeAndThemeCanvas6.txt
..\src\test\robotframework\Themes\TC_ShapeAndThemeTDS7.txt
..\src\test\robotframework\Themes\TC_ShapeAndThemeReports8.txt

..\src\test\robotframework\Calendar\POCalendar_AddCalendarForPO1.txt
#Quality
..\src\test\robotframework\CollectionManagement\TC_Quality1_TestGroup_Templates.txt
..\src\test\robotframework\CollectionManagement\TC_Quality2_ColoredMaterial.txt
..\src\test\robotframework\CollectionManagement\TC_Quality2_MaterialQuality.txt
..\src\test\robotframework\CollectionManagement\TC_Quality2_StyleQuality.txt
..\src\test\robotframework\CollectionManagement\TC_Quality3_Samples_SecurityGroup.txt
..\src\test\robotframework\CollectionManagement\TC_Quality4_MSG_ColColorwaySample.txt
..\src\test\robotframework\acceptance\TC_CreateQualityTestSpec.txt

#POcalendar
..\src\test\robotframework\Calendar\POCalendar_UpdateCalendarActivities23.txt
..\src\test\robotframework\Calendar\POCalendar_CalendarPropogation4.txt
# business Planning
..\src\test\robotframework\BusinessPlanning\TC_CurrencyTable.txt
..\src\test\robotframework\BusinessPlanning\BusinessCategory.txt
..\src\test\robotframework\BusinessPlanning\BusinessPlanning.txt
..\src\test\robotframework\BusinessPlanning\BusinessPlanning_Material.txt
..\src\test\robotframework\BusinessPlanning\BusinessPlanning_HierarchyBusinessPlans.txt

#PO Calendar 
..\src\test\robotframework\Calendar\POCalendar_CalendarPropogationValidation5.txt
# Style Sourcing Test Cases
..\src\test\robotframework\Sourcing\StyleSourcingPO_026555-02676.txt

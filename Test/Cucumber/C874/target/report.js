$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/Features/A_Config_Setup.feature");
formatter.feature({
  "name": "Configuration and Setup",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@UpdateConfiguration"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Style type  creation",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "User click on update cnfiguration",
  "keyword": "Then "
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.step({
  "name": "User creates style type \"\u003cStyleTypeA\u003e\",\"\u003cStyleTypeB\u003e\",\"\u003cStyleTypeC\u003e\",\"\u003cStyleTypeD\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "User fill the style type attributes fields",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "StyleTypeA",
        "StyleTypeB",
        "StyleTypeC",
        "StyleTypeD"
      ]
    },
    {
      "cells": [
        "Apparel - Color and Size",
        "Accessories - Only Color",
        "Apparel - Only Size",
        "Accessories - No color and Size"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Style type  creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@UpdateConfiguration"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User click on update cnfiguration",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.user_click_on_update_cnfiguration()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.user_Click_on_setup_icon()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates style type \"Apparel - Color and Size\",\"Accessories - Only Color\",\"Apparel - Only Size\",\"Accessories - No color and Size\"",
  "keyword": "When "
});
formatter.match({
  "location": "ConfigurationPageSteps.user_creates_style_type(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User fill the style type attributes fields",
  "keyword": "And "
});
formatter.match({
  "location": "ConfigurationPageSteps.user_fill_the_style_type_attributes_fields()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Material type creation",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.step({
  "name": "User creates material type \"\u003cStandalone\u003e\",\"\u003cStructurecomponent\u003e\",\"\u003cTool\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Standalone",
        "Structurecomponent",
        "Tool"
      ]
    },
    {
      "cells": [
        "Fabric - Stanadlone",
        "Fabric - Structure Component",
        "Fabric - Tool"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Material type creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@UpdateConfiguration"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.user_Click_on_setup_icon()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates material type \"Fabric - Stanadlone\",\"Fabric - Structure Component\",\"Fabric - Tool\"",
  "keyword": "And "
});
formatter.match({
  "location": "ConfigurationPageSteps.user_creates_material_type(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "BOM creation",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.step({
  "name": "User creates BOM type \"\u003cStyleBOM\u003e\",\"\u003cMaterialBOM\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "StyleBOM",
        "MaterialBOM"
      ]
    },
    {
      "cells": [
        "Apparel BOM",
        "Material BOM"
      ]
    }
  ]
});
formatter.scenario({
  "name": "BOM creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@UpdateConfiguration"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.user_Click_on_setup_icon()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates BOM type \"Apparel BOM\",\"Material BOM\"",
  "keyword": "And "
});
formatter.match({
  "location": "ConfigurationPageSteps.user_creates_BOM_type(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Size Chart creation",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.step({
  "name": "User creates size chart type \"\u003cAll\u003e\",\"\u003cList\u003e\",\"\u003cValues\u003e\",\"\u003cTolerance\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "All",
        "List",
        "Values",
        "Tolerance"
      ]
    },
    {
      "cells": [
        "SC-Lock All",
        "SC-Lock Dimension",
        "SC-Lock Values",
        "SC-Lock Tolerance"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Size Chart creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@UpdateConfiguration"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.user_Click_on_setup_icon()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates size chart type \"SC-Lock All\",\"SC-Lock Dimension\",\"SC-Lock Values\",\"SC-Lock Tolerance\"",
  "keyword": "And "
});
formatter.match({
  "location": "ConfigurationPageSteps.user_creates_size_chart_type(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Spec type creation",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.step({
  "name": "User creates spec type \"\u003cSpecTypeA\u003e\",\"\u003cSpecTypeB\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "SpecTypeA",
        "SpecTypeB"
      ]
    },
    {
      "cells": [
        "Spec Type-01",
        "Spec Type-02"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Spec type creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@UpdateConfiguration"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.user_Click_on_setup_icon()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates spec type \"Spec Type-01\",\"Spec Type-02\"",
  "keyword": "And "
});
formatter.match({
  "location": "ConfigurationPageSteps.user_creates_spec_type(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Test type creation",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.step({
  "name": "User creates test type \"\u003cTestTypeA\u003e\",\"\u003cTestTypeB\u003e\",\"\u003cTestTypeC\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User performing delete action",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "TestTypeA",
        "TestTypeB",
        "TestTypeC"
      ]
    },
    {
      "cells": [
        "Wash Test",
        "Chemical Test",
        "Ironing Test"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Test type creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@UpdateConfiguration"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.user_Click_on_setup_icon()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates test type \"Wash Test\",\"Chemical Test\",\"Ironing Test\"",
  "keyword": "And "
});
formatter.match({
  "location": "ConfigurationPageSteps.user_creates_test_type(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User performing delete action",
  "keyword": "And "
});
formatter.match({
  "location": "ConfigurationPageSteps.user_performing_delete_action()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Inspection type creation",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.step({
  "name": "User creates inspection type \"\u003cInspnoLink\u003e\",\"\u003cBOM\u003e\",\"\u003cSizeChart\u003e\",\"\u003cSpecDS\u003e\",\"\u003cStyleReview\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "InspnoLink",
        "BOM",
        "SizeChart",
        "SpecDS",
        "StyleReview"
      ]
    },
    {
      "cells": [
        "Ins Section-No Link",
        "Ins Section-BOM",
        "Ins Section-Size Chart",
        "Ins Section-Spec Data Sheet",
        "Ins Section-Style Review"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Inspection type creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@UpdateConfiguration"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.user_Click_on_setup_icon()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates inspection type \"Ins Section-No Link\",\"Ins Section-BOM\",\"Ins Section-Size Chart\",\"Ins Section-Spec Data Sheet\",\"Ins Section-Style Review\"",
  "keyword": "And "
});
formatter.match({
  "location": "ConfigurationPageSteps.user_creates_inspection_type(String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Contractual documents creation",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.step({
  "name": "User creates contractual documents \"\u003cContrctAll\u003e\",\"\u003cSimple\u003e\",\"\u003cRA\u003e\",\"\u003cHED\u003e\",\"\u003cHRD\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "ContrctAll",
        "Simple",
        "RA",
        "HED",
        "HRD"
      ]
    },
    {
      "cells": [
        "CDOC-ALL",
        "CDOC-simple",
        "CDOC-RA",
        "CDOC-HED",
        "CDOC-HRD"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Contractual documents creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@UpdateConfiguration"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.user_Click_on_setup_icon()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates contractual documents \"CDOC-ALL\",\"CDOC-simple\",\"CDOC-RA\",\"CDOC-HED\",\"CDOC-HRD\"",
  "keyword": "And "
});
formatter.match({
  "location": "ConfigurationPageSteps.user_creates_contractual_documents(String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Look up items creation",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.step({
  "name": "User creates LookUp items \"\u003cLookUpA\u003e\",\"\u003cLookUpB\u003e\",\"\u003cLookUpC\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "LookUpA",
        "LookUpB",
        "LookUpC"
      ]
    },
    {
      "cells": [
        "Cost Factors",
        "Holiday Calendar",
        "User Task Lead Times"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Look up items creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@UpdateConfiguration"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.user_Click_on_setup_icon()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates LookUp items \"Cost Factors\",\"Holiday Calendar\",\"User Task Lead Times\"",
  "keyword": "And "
});
formatter.match({
  "location": "ConfigurationPageSteps.user_creates_LookUp_items(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Theme Master creation",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.step({
  "name": "User creates theme master \"\u003cAll\u003e\",\"\u003csingle\u003e\",\"\u003cmultiple\u003e\",\"\u003cAVsingle\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "All",
        "single",
        "multiple",
        "AVsingle"
      ]
    },
    {
      "cells": [
        "TM-All Season",
        "TM-Single Season",
        "TM-Multiple Season",
        "TM-Allow material variation"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Theme Master creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@UpdateConfiguration"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.user_Click_on_setup_icon()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates theme master \"TM-All Season\",\"TM-Single Season\",\"TM-Multiple Season\",\"TM-Allow material variation\"",
  "keyword": "And "
});
formatter.match({
  "location": "ConfigurationPageSteps.user_creates_theme_master(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Update ColorRole in Enumeration",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Enumeration"
    }
  ]
});
formatter.step({
  "name": "User Creates ColorROle in Enumeration \"\u003cmaterialname\u003e\",\"\u003cThemename\u003e\",\"\u003cfamilyname1\u003e\",\"\u003cfamilyname2\u003e\",\"\u003cBluecolor\u003e\",\"\u003cRedcolor\u003e\",\"\u003cThemevalue1\u003e\",\"\u003cThemevalue2\u003e\",",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "materialname",
        "Themename",
        "familyname1",
        "familyname2",
        "Bluecolor",
        "Redcolor",
        "Themevalue1",
        "Themevalue2"
      ]
    },
    {
      "cells": [
        "MaterialFamily",
        "ThemeMainMaterialGroup",
        "Family-01",
        "Family-02",
        "Family-01",
        "Family-02",
        "Group-01",
        "Group-02"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Update ColorRole in Enumeration",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@UpdateConfiguration"
    },
    {
      "name": "@Enumeration"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User Creates ColorROle in Enumeration \"MaterialFamily\",\"ThemeMainMaterialGroup\",\"Family-01\",\"Family-02\",\"Family-01\",\"Family-02\",\"Group-01\",\"Group-02\",",
  "keyword": "And "
});
formatter.match({
  "location": "ColorRole.user_Creates_ColorROle_in_Enumeration(String,String,String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "FoodTypes and DataSheet creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@FoodTypes"
    }
  ]
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.step({
  "name": "User Creates FoodTypes and FoodlabelDatasheetvalues \"\u003cFoodType\u003e\",\"\u003cFoodLabel\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "FoodType",
        "FoodLabel"
      ]
    },
    {
      "cells": [
        "Pizza,Cat Food,Dog Food,No Data sheets,Fast Food,Fruits and Vegetables",
        "Food Additives, Colours and Flavors,No Data section"
      ]
    }
  ]
});
formatter.scenario({
  "name": "FoodTypes and DataSheet creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@UpdateConfiguration"
    },
    {
      "name": "@FoodTypes"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.user_Click_on_setup_icon()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Creates FoodTypes and FoodlabelDatasheetvalues \"Pizza,Cat Food,Dog Food,No Data sheets,Fast Food,Fruits and Vegetables\",\"Food Additives, Colours and Flavors,No Data section\"",
  "keyword": "And "
});
formatter.match({
  "location": "ConfigurationPageSteps.user_Creates_FoodTypes_and_FoodlabelDatasheetvalues(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "GTIN And Sales Region creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@GTIN"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.step({
  "name": "User Clicks on Company Tab and Create GTIN \"\u003cManufacturerName\u003e\",\"\u003cManufacturerCode\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User Creates Sales Region \"\u003cSalesRegionName\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "ManufacturerName",
        "ManufacturerCode",
        "SalesRegionName"
      ]
    },
    {
      "cells": [
        "JubilientFoods",
        "12345",
        "India-South"
      ]
    }
  ]
});
formatter.scenario({
  "name": "GTIN And Sales Region creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@UpdateConfiguration"
    },
    {
      "name": "@GTIN"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.user_Click_on_setup_icon()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Clicks on Company Tab and Create GTIN \"JubilientFoods\",\"12345\"",
  "keyword": "And "
});
formatter.match({
  "location": "ConfigurationPageSteps.User_Clicks_on_Company_Tab_and_Create_GTIN(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Creates Sales Region \"India-South\"",
  "keyword": "And "
});
formatter.match({
  "location": "ConfigurationPageSteps.User_Clicks_on_Company_Tab_and_Create_GTIN(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "ProjectType creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@ProjectType"
    }
  ]
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to Project Types and Create ProjectType \"\u003cProjectType\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User click on update cnfiguration",
  "keyword": "Then "
});
formatter.step({
  "name": "Verify the Project Type Drop Down \"\u003cProjectType\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User Create WBS projects \"Project\",\"Task\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "ProjectType"
      ]
    },
    {
      "cells": [
        "Project-ALL,Project-Style,Project-Food,Project-Material,Project-Blank,Project-ForDelAct"
      ]
    }
  ]
});
formatter.scenario({
  "name": "ProjectType creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@UpdateConfiguration"
    },
    {
      "name": "@ProjectType"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.user_Click_on_setup_icon()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Project Types and Create ProjectType \"Project-ALL,Project-Style,Project-Food,Project-Material,Project-Blank,Project-ForDelAct\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ConfigurationPageSteps.Navigate_to_Project_Types_and_Create_ProjectType(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User click on update cnfiguration",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.user_click_on_update_cnfiguration()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Verify the Project Type Drop Down \"Project-ALL,Project-Style,Project-Food,Project-Material,Project-Blank,Project-ForDelAct\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ConfigurationPageSteps.Verify_the_Project_Type_Drop_Down(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Create WBS projects \"Project\",\"Task\"",
  "keyword": "And "
});
formatter.match({
  "location": "ProjectSteps.user_Create_WBS_projects(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenario({
  "name": "Update Configuration",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@UpdateConfiguration"
    },
    {
      "name": "@ProjectType"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.user_Click_on_setup_icon()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User click on update cnfiguration",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.user_click_on_update_cnfiguration()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.logout_from_the_Application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenario({
  "name": "Creation of native import",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@UpdateConfiguration"
    },
    {
      "name": "@Nativeimport"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to setup icon and create native import jobs",
  "keyword": "Then "
});
formatter.match({
  "location": "HybridAgentSteps.go_to_setup_icon_and_create_native_import_jobs()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/B_HeirarchyStyleCreation .feature");
formatter.feature({
  "name": "Creation of Season Hierarchy",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@SeasonCreation"
    },
    {
      "name": "@Style"
    }
  ]
});
formatter.scenarioOutline({
  "name": "User creates New season for a product",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@NewseasonCreation"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "Click style tab and get the listed season name in the style tab",
  "keyword": "And "
});
formatter.step({
  "name": "User create New season with mandatory deatails for season creation \"\u003cStyleA\u003e\",\"\u003cScodeA\u003e\",\"\u003cSdescriptionA\u003e\",\"\u003cStyleB\u003e\",\"\u003cScodeB\u003e\",\"\u003cSdescriptionB\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "User performing delete action",
  "keyword": "And "
});
formatter.step({
  "name": "User performing edit action on season",
  "keyword": "And "
});
formatter.step({
  "name": "Click New Brand",
  "keyword": "Then "
});
formatter.step({
  "name": "Create Brand by providing valid and mandatory data \"\u003cBrand\u003e\",\"\u003cBcode\u003e\",\"\u003cBdescription\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Save the New Brand which was created",
  "keyword": "Then "
});
formatter.step({
  "name": "Click New department by providing valid data  \"\u003cDept\u003e\",\"\u003cDcode\u003e\",\"\u003cDdescription\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Save the New department which was created",
  "keyword": "Then "
});
formatter.step({
  "name": "Click New collection",
  "keyword": "Then "
});
formatter.step({
  "name": "Create collection by providing valida and mandatory details \"\u003cCollection\u003e\",\"\u003cCcode\u003e\",\"\u003cCdesription\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Save the collection which was created",
  "keyword": "Then "
});
formatter.step({
  "name": "Click New style",
  "keyword": "Then "
});
formatter.step({
  "name": "Create Style followed by its style Type \"\u003cStyleTypeA\u003e\",\"\u003cStyleTypeB\u003e\",\"\u003cStyleTypeC\u003e\",\"\u003cStyleTypeD\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "StyleA",
        "ScodeA",
        "SdescriptionA",
        "StyleB",
        "ScodeB",
        "SdescriptionB",
        "Brand",
        "Bcode",
        "Bdescription",
        "Dept",
        "Dcode",
        "Ddescription",
        "Collection",
        "Ccode",
        "Cdesription",
        "StyleTypeA",
        "StyleTypeB",
        "StyleTypeC",
        "StyleTypeD"
      ]
    },
    {
      "cells": [
        "summer season",
        "123 summer",
        "Automation",
        "winter season",
        "123 winter",
        "Automation",
        "Denim",
        "Denim Code",
        "Automation",
        "Mens",
        "123 Mens",
        "Automation",
        "Jeans",
        "123 jeans",
        "Automation",
        "Apparel - Color and Size",
        "Accessories - Only Color",
        "Apparel - Only Size",
        "Accessories - No color and Size"
      ]
    }
  ]
});
formatter.scenario({
  "name": "User creates New season for a product",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@SeasonCreation"
    },
    {
      "name": "@Style"
    },
    {
      "name": "@NewseasonCreation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Click style tab and get the listed season name in the style tab",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.click_style_tab_and_get_the_listed_season_name_in_the_style_tab()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User create New season with mandatory deatails for season creation \"summer season\",\"123 summer\",\"Automation\",\"winter season\",\"123 winter\",\"Automation\"",
  "keyword": "When "
});
formatter.match({
  "location": "StylePageSteps.user_create_New_season_with_mandatory_deatails_for_season_creation(String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User performing delete action",
  "keyword": "And "
});
formatter.match({
  "location": "ConfigurationPageSteps.user_performing_delete_action()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User performing edit action on season",
  "keyword": "And "
});
formatter.match({
  "location": "StylePageSteps.user_performing_edit_action_on_season()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Click New Brand",
  "keyword": "Then "
});
formatter.match({
  "location": "StylePageSteps.click_New_Brand()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create Brand by providing valid and mandatory data \"Denim\",\"Denim Code\",\"Automation\"",
  "keyword": "And "
});
formatter.match({
  "location": "StylePageSteps.create_Brand_by_providing_valid_and_mandatory_data(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Save the New Brand which was created",
  "keyword": "Then "
});
formatter.match({
  "location": "StylePageSteps.save_the_New_Brand_which_was_created()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Click New department by providing valid data  \"Mens\",\"123 Mens\",\"Automation\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StylePageSteps.click_New_department_by_providing_valid_data(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Save the New department which was created",
  "keyword": "Then "
});
formatter.match({
  "location": "StylePageSteps.save_the_New_department_which_was_created()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Click New collection",
  "keyword": "Then "
});
formatter.match({
  "location": "StylePageSteps.click_New_collection()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create collection by providing valida and mandatory details \"Jeans\",\"123 jeans\",\"Automation\"",
  "keyword": "And "
});
formatter.match({
  "location": "StylePageSteps.create_collection_by_providing_valida_and_mandatory_details(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Save the collection which was created",
  "keyword": "Then "
});
formatter.match({
  "location": "StylePageSteps.save_the_collection_which_was_created()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Click New style",
  "keyword": "Then "
});
formatter.match({
  "location": "StylePageSteps.click_New_style()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create Style followed by its style Type \"Apparel - Color and Size\",\"Accessories - Only Color\",\"Apparel - Only Size\",\"Accessories - No color and Size\"",
  "keyword": "And "
});
formatter.match({
  "location": "StylePageSteps.create_Style_followed_by_its_style_Type(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.logout_from_the_Application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/C_Material_library.feature");
formatter.feature({
  "name": "Creation of Material Hierarchy",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@MaterialHeirarichy"
    },
    {
      "name": "@Material"
    }
  ]
});
formatter.scenarioOutline({
  "name": "User creates New material for a product",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@NewmaterialCreation"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "Click Libraries tab under specification",
  "keyword": "And "
});
formatter.step({
  "name": "User create New material with mandatory deatails for material creation \"\u003cStandalone-value\u003e\",\"\u003cStandalone-material\u003e\",\"\u003cStandalone-code\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "User create material for Structure Component \"\u003cStrComponent-value\u003e\",\"\u003cStrComponent-material\u003e\",\"\u003cStrComponent-code\u003e\",\"\u003cTool-value\u003e\",\"\u003cTool-material\u003e\",\"\u003cTool-code\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User performing copy and delete action on material",
  "keyword": "And "
});
formatter.step({
  "name": "User creates color and size",
  "keyword": "And "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "User creating suppliers,samples and sku and validates \"\u003cSupplierA\u003e\",\"\u003cSupplierB\u003e\",\"\u003cSupplierC\u003e\",\"\u003cmaterial-Sample\u003e\",\"\u003ccolors/sizes\u003e\",\"\u003cSKU\u003e\",\"\u003cChooseSupplier\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "SupplierA",
        "SupplierB",
        "SupplierC",
        "material-Sample",
        "colors/sizes",
        "SKU",
        "Standalone-value",
        "Standalone-material",
        "Standalone-code",
        "StrComponent-value",
        "StrComponent-material",
        "StrComponent-code",
        "Tool-value",
        "Tool-material",
        "Tool-code",
        "ChooseSupplier"
      ]
    },
    {
      "cells": [
        "Changshu",
        "Frontline",
        "Supplier",
        "100% Cotton Jersey",
        "colors and sizes\u003d1",
        "Material SKU",
        "Fabric - Stanadlone",
        "100% Cotton/Rayon Jersey",
        "123 - Automation",
        "Fabric - Structure Component",
        "100% Polyester Chiffon",
        "123 - Automation",
        "Fabric - Tool",
        "100% Polyester Chiffon (Striped)",
        "123 - Automation",
        "Supplier"
      ]
    }
  ]
});
formatter.scenario({
  "name": "User creates New material for a product",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@MaterialHeirarichy"
    },
    {
      "name": "@Material"
    },
    {
      "name": "@NewmaterialCreation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Click Libraries tab under specification",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.click_Libraries_tab_under_specification()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User create New material with mandatory deatails for material creation \"Fabric - Stanadlone\",\"100% Cotton/Rayon Jersey\",\"123 - Automation\"",
  "keyword": "When "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_create_New_material_with_mandatory_deatails_for_material_creation(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User create material for Structure Component \"Fabric - Structure Component\",\"100% Polyester Chiffon\",\"123 - Automation\",\"Fabric - Tool\",\"100% Polyester Chiffon (Striped)\",\"123 - Automation\"",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_create_material_for_Structure_Component(String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User performing copy and delete action on material",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_performing_copy_and_delete_action_on_material()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates color and size",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_creates_color_and_size()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creating suppliers,samples and sku and validates \"Changshu\",\"Frontline\",\"Supplier\",\"100% Cotton Jersey\",\"colors and sizes\u003d1\",\"Material SKU\",\"Supplier\"",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_creating_suppliers_samples_and_sku_and_validates(String,String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Material Gallery validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@MaterialGallery"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User validates material gallery \"\u003cTest\u003e\",\"\u003cMG-Filter\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Test",
        "MG-Filter"
      ]
    },
    {
      "cells": [
        "QA Automation",
        "Active"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Material Gallery validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@MaterialHeirarichy"
    },
    {
      "name": "@Material"
    },
    {
      "name": "@MaterialGallery"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates material gallery \"QA Automation\",\"Active\"",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_material_gallery(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Material Security Group validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@MaterialSecurity"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User validates material security group \"\u003cTest\u003e\",\"\u003cMSG-Standalone\u003e\",\"\u003cMSG-All\u003e\",\"\u003cMSG-A\u003e\",\"\u003cMSG-B\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User validates security group copy,delete actions",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Test",
        "MSG-Standalone",
        "MSG-All",
        "MSG-A",
        "MSG-B"
      ]
    },
    {
      "cells": [
        "QA Automation",
        "MSG-Standalone",
        "MSG-All",
        "Changshu Materials",
        "Vendor Materials"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Material Security Group validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@MaterialHeirarichy"
    },
    {
      "name": "@Material"
    },
    {
      "name": "@MaterialSecurity"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates material security group \"QA Automation\",\"MSG-Standalone\",\"MSG-All\",\"Changshu Materials\",\"Vendor Materials\"",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_material_security_group(String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates security group copy,delete actions",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_security_group_copy_delete_actions()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenario({
  "name": "Material color validation",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@MaterialHeirarichy"
    },
    {
      "name": "@Material"
    },
    {
      "name": "@MaterialColor"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates colored material",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_colored_material()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates colored material copy,delete actions",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_colored_material_copy_delete_actions()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Material Sample validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@MaterialSample"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User validates and edit material samples \"\u003cmaterial-Sample\u003e\",\"\u003cChooseSupplier\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user validates PDF action in material samples",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "material-Sample",
        "ChooseSupplier"
      ]
    },
    {
      "cells": [
        "100% Cotton Jersey",
        "Supplier"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Material Sample validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@MaterialHeirarichy"
    },
    {
      "name": "@Material"
    },
    {
      "name": "@MaterialSample"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates and edit material samples \"100% Cotton Jersey\",\"Supplier\"",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_and_edit_material_samples(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user validates PDF action in material samples",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_PDF_action_in_material_samples()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenario({
  "name": "Material SKU validation",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@MaterialHeirarichy"
    },
    {
      "name": "@Material"
    },
    {
      "name": "@MaterialSKU"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates material SKU\u0027s",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_material_SKU_s()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates custom view action in material SKU",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_custom_view_action_in_material_SKU()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenario({
  "name": "Material Sourcing validation",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@MaterialHeirarichy"
    },
    {
      "name": "@Material"
    },
    {
      "name": "@MaterialSourcing"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates sourcing",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_sourcing()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Material Care Label validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@MatrialCareLabel"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User validates care label \"\u003cCL-Wash\u003e\",\"\u003cCL-Iron\u003e\",\"\u003cCL-Special\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User validates care label copy,delete action",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "CL-Wash",
        "CL-Iron",
        "CL-Special"
      ]
    },
    {
      "cells": [
        "Do Not Wash",
        "Iron Low Heat",
        "Wash With Like Colors"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Material Care Label validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@MaterialHeirarichy"
    },
    {
      "name": "@Material"
    },
    {
      "name": "@MatrialCareLabel"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates care label \"Do Not Wash\",\"Iron Low Heat\",\"Wash With Like Colors\"",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_care_label(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates care label copy,delete action",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_care_label_copy_delete_action()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Material Composition Placement validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@MaterialComposition"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User validates composition placements \"\u003cCompPlacement-A\u003e\",\"\u003cCompPlacement-B\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User validates composition placements custom view actions",
  "keyword": "And "
});
formatter.step({
  "name": "User validates Composition Placements edit,copy,delete action",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "CompPlacement-A",
        "CompPlacement-B"
      ]
    },
    {
      "cells": [
        "Inner Lining - Placement",
        "Outer Lining - placement"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Material Composition Placement validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@MaterialHeirarichy"
    },
    {
      "name": "@Material"
    },
    {
      "name": "@MaterialComposition"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates composition placements \"Inner Lining - Placement\",\"Outer Lining - placement\"",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_composition_placements(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates composition placements custom view actions",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_composition_placements_custom_view_actions()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates Composition Placements edit,copy,delete action",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_Composition_Placements_edit_copy_delete_action()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Composite Material validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@CompositeMaterial"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User validates composition material \"\u003ccompMaterial-A\u003e\",\"\u003ccompMaterial-B\u003e\",\"\u003ccompMaterial-C\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User validates composition material custom view actions",
  "keyword": "And "
});
formatter.step({
  "name": "User validates composition material edit,copy,delete action",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "compMaterial-A",
        "compMaterial-B",
        "compMaterial-C"
      ]
    },
    {
      "cells": [
        "BRASS",
        "COTTON",
        "LEATHER"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Composite Material validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@MaterialHeirarichy"
    },
    {
      "name": "@Material"
    },
    {
      "name": "@CompositeMaterial"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates composition material \"BRASS\",\"COTTON\",\"LEATHER\"",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_composition_material(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates composition material custom view actions",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_composition_material_custom_view_actions()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates composition material edit,copy,delete action",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_composition_material_edit_copy_delete_action()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Material Placement validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@MaterialPlacement"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User validates placement \"\u003cPlacement-A\u003e\",\"\u003cPlacement-B\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User validates placements custom view actions",
  "keyword": "And "
});
formatter.step({
  "name": "User validates placements edit,copy,delete action",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Placement-A",
        "Placement-B"
      ]
    },
    {
      "cells": [
        "Shoulder - placement",
        "Neck - placement"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Material Placement validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@MaterialHeirarichy"
    },
    {
      "name": "@Material"
    },
    {
      "name": "@MaterialPlacement"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates placement \"Shoulder - placement\",\"Neck - placement\"",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_placement(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates placements custom view actions",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_placements_custom_view_actions()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates placements edit,copy,delete action",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_placements_edit_copy_delete_action()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Material Product Symbols validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@MaterialProductSymbol"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User validates product symbols \"\u003cPS-Brand\u003e\",\"\u003cPS-Feature\u003e\",\"\u003cPS-Function\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User validates product symbols copy,delete actions",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "PS-Brand",
        "PS-Feature",
        "PS-Function"
      ]
    },
    {
      "cells": [
        "Heat",
        "Vibration",
        "Waterproof"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Material Product Symbols validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@MaterialHeirarichy"
    },
    {
      "name": "@Material"
    },
    {
      "name": "@MaterialProductSymbol"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates product symbols \"Heat\",\"Vibration\",\"Waterproof\"",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_product_symbols(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates product symbols copy,delete actions",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_product_symbols_copy_delete_actions()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Material Language validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@MaterialLanguage"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User validates language \"\u003cLanguage-A\u003e\",\"\u003cLanguage-B\u003e\",\"\u003cLanguage-C\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Language-A",
        "Language-B",
        "Language-C"
      ]
    },
    {
      "cells": [
        "English - US",
        "French",
        "Japanese"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Material Language validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@MaterialHeirarichy"
    },
    {
      "name": "@Material"
    },
    {
      "name": "@MaterialLanguage"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates language \"English - US\",\"French\",\"Japanese\"",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_language(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Material Structure Component validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@MaterialStructure"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User validates product structure \"\u003cStrComponent-material\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User validates Product structures custom view actions",
  "keyword": "And "
});
formatter.step({
  "name": "User validates Product structures edit,copy,delete action",
  "keyword": "And "
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "StrComponent-material"
      ]
    },
    {
      "cells": [
        "shoe structure"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Material Structure Component validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@MaterialHeirarichy"
    },
    {
      "name": "@Material"
    },
    {
      "name": "@MaterialStructure"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates product structure \"shoe structure\"",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_product_structure(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates Product structures custom view actions",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_Product_structures_custom_view_actions()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates Product structures edit,copy,delete action",
  "keyword": "And "
});
formatter.match({
  "location": "MaterialSpecificationPageSteps.user_validates_Product_structures_edit_copy_delete_action()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.logout_from_the_Application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/D1_Specification_library.feature");
formatter.feature({
  "name": "Specification validation",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Specification"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Specification Hierarchy creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SpecificationHierarchy"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User Click on specification and creates hierarchy \"\u003cBrand\u003e\",\"\u003cCollection\u003e\",\"\u003cDepartment\u003e\",\"\u003cCode\u003e\",\"\u003cDescription\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "User creates style template \"\u003cStyle\u003e\",\"\u003cStyleType\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Brand",
        "Collection",
        "Department",
        "Code",
        "Description",
        "Style",
        "StyleType"
      ]
    },
    {
      "cells": [
        "Denim - Brand",
        "Mens - collection",
        "Mens - Department",
        "123 - Automation",
        "Automation",
        "Apparel - Template",
        "Apparel - Color and Size"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Specification Hierarchy creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Specification"
    },
    {
      "name": "@SpecificationHierarchy"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Click on specification and creates hierarchy \"Denim - Brand\",\"Mens - collection\",\"Mens - Department\",\"123 - Automation\",\"Automation\"",
  "keyword": "When "
});
formatter.match({
  "location": "SpecificationHierarchyPageSteps.user_Click_on_specification_and_creates_hierarchy(String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates style template \"Apparel - Template\",\"Apparel - Color and Size\"",
  "keyword": "And "
});
formatter.match({
  "location": "SpecificationHierarchyPageSteps.user_creates_style_template(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenario({
  "name": "Specification Classifier creation",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Specification"
    },
    {
      "name": "@SpecificationClassifier"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates Classifier \"Classifier\"",
  "keyword": "And "
});
formatter.match({
  "location": "SpecificationHierarchyPageSteps.user_creates_Classifier(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.logout_from_the_Application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "User create 2d sizes and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@2DSizes"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Setup Enum for size dimensions in setup page \"\u003cEnumeration\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "Create the Enum value for the sizes \"\u003cEnumeration Value\u003e\",\"\u003cDescription\u003e\",\"\u003cEnumerationValue2\u003e\",\"\u003cDescription2\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User navigates to type config",
  "keyword": "And "
});
formatter.step({
  "name": "user creates the size spec data for size label module \"\u003cSizeSpec\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "Update the Enum through partial configuration",
  "keyword": "And "
});
formatter.step({
  "name": "validate and accept the alert",
  "keyword": "Then "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates multiple sizes with different type for TwoDSizes \"\u003cSize\u003e\",\"\u003cSize1\u003e\",\"\u003cSize2\u003e\",\"\u003cSize3\u003e\",\"\u003cSize4\u003e\",\"\u003cSize5\u003e\",\"\u003cSortOrder\u003e\",\"\u003cSortOrder1\u003e\",\"\u003cSortOrder2\u003e\",\"\u003cSortOrder3\u003e\",\"\u003cSortOrder4\u003e\",\"\u003cSortOrder5\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "Create duplicate Sizes and verify the error message \"\u003cDuplicateSize\u003e\",\"\u003cDupSortOrder\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Create twoD sizes with different type of dimension \"\u003cCompSize\u003e\",\"\u003cCompSize2\u003e\",\"\u003cCompSize3\u003e\",\"\u003cCompSize4\u003e\",\"\u003cCompSize5\u003e\",\"\u003cCompSize6\u003e\",\"\u003cDimensionType\u003e\",\"\u003cDimensionType2\u003e\",\"\u003cDimensionType3\u003e\",\"\u003cDimensionType4\u003e\",\"\u003cDimensionType5\u003e\",\"\u003cDimensionType6\u003e\",\"\u003cCompSizeSO1\u003e\",\"\u003cCompSizeSO2\u003e\",\"\u003cCompSizeSO3\u003e\",\"\u003cCompSizeSO4\u003e\",\"\u003cCompSizeSO5\u003e\",\"\u003cCompSizeSO6\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User selects the created sizes for TwoD sizes \"\u003c2DimensionSize1\u003e\",\"\u003c2DimensionSize2\u003e\",\"\u003c2DimensionSize3\u003e\",\"\u003c2DimensionSize4\u003e\",\"\u003c2DimensionSize5\u003e\",\"\u003c2DimensionSize6\u003e\",\"\u003c2DimensionSize7\u003e\",\"\u003c2DimensionSize8\u003e\",\"\u003c2DimensionSize9\u003e\",\"\u003cCompSize\u003e\",\"\u003cCompSize2\u003e\",\"\u003cCompSize3\u003e\",\"\u003cCompSize4\u003e\",\"\u003cCompSize5\u003e\",\"\u003cCompSize6\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "Create size range for the sizes \"\u003cSizeRange\u003e\",\"\u003cDescription\u003e\",\"\u003cEnumerationValue2\u003e\",\"\u003cEnumeration Value\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User selects the sizes for the created size range \"\u003c2DimensionSize1\u003e\",\"\u003c2DimensionSize2\u003e\",\"\u003c2DimensionSize3\u003e\",\"\u003c2DimensionSize4\u003e\",\"\u003c2DimensionSize5\u003e\",\"\u003c2DimensionSize6\u003e\",\"\u003c2DimensionSize7\u003e\",\"\u003c2DimensionSize8\u003e\",\"\u003c2DimensionSize9\u003e\",\"\u003cSizeRange\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User creates simple size range for validation \"\u003cSimpleSizeName1\u003e\",\"\u003cSimpleSizeName2\u003e\",\"\u003cCompSizeName\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Merging of composite size type \"\u003cSize\u003e\",\"\u003cSize1\u003e\",\"\u003cSize2\u003e\",\"\u003cSize3\u003e\",\"\u003cSize4\u003e\",\"\u003cSize5\u003e\",\"\u003cSimpleSizeName1\u003e\",\"\u003cSimpleSizeName2\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Selecting the sub ranges for composite sizes \"\u003cSimpleSizeName1\u003e\",\"\u003cSimpleSizeName2\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Validate the options like copy and delete for size Range \"\u003cCopySizeName\u003e\",\"\u003cCompSizeName\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Create a Size Map and validate the status \"\u003cSizeRange\u003e\",\"\u003cCompSizeName\u003e\",\"\u003cSizeMapName\u003e\",\"\u003cSize\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Enumeration",
        "Enumeration Value",
        "Description",
        "EnumerationValue2",
        "Description2",
        "Size",
        "SortOrder",
        "Size1",
        "SortOrder1",
        "Size2",
        "SortOrder2",
        "Size3",
        "SortOrder3",
        "Size4",
        "SortOrder4",
        "Size5",
        "SortOrder5",
        "DuplicateSize",
        "DupSortOrder",
        "CompSize",
        "DimensionType",
        "CompSize2",
        "DimensionType2",
        "CompSize3",
        "DimensionType3",
        "CompSize4",
        "DimensionType4",
        "CompSize5",
        "DimensionType5",
        "CompSize6",
        "DimensionType6",
        "CompSizeSO1",
        "CompSizeSO2",
        "CompSizeSO3",
        "CompSizeSO4",
        "CompSizeSO5",
        "CompSizeSO6",
        "2DimensionSize1",
        "2DimensionSize2",
        "2DimensionSize3",
        "2DimensionSize4",
        "2DimensionSize5",
        "2DimensionSize6",
        "2DimensionSize7",
        "2DimensionSize8",
        "2DimensionSize9",
        "SizeRange",
        "SimpleSizeName1",
        "SimpleSizeName2",
        "CompSizeName",
        "CopySizeName",
        "SizeMapName",
        "SizeSpec"
      ]
    },
    {
      "cells": [
        "DimensionType (1)",
        "Length",
        "2DSizeModul",
        "Waist",
        "2DSize",
        "small",
        "10",
        "medium",
        "11",
        "large",
        "12",
        "SMALL",
        "13",
        "XL",
        "14",
        "XXL",
        "15",
        "SMALL",
        "16",
        "W1",
        "Waist",
        "W2",
        "Waist",
        "W3",
        "Waist",
        "L1",
        "Length",
        "L2",
        "Length",
        "L3",
        "Length",
        "11",
        "12",
        "13",
        "14",
        "15",
        "16",
        "W1/L1",
        "W1/L2",
        "W1/L3",
        "W2/L1",
        "W2/L2",
        "W2/L3",
        "W3/L1",
        "W3/L2",
        "W3/L3",
        "MensRegular",
        "MensPants",
        "MensJeans",
        "Childrenswear",
        "2DSizesCopy",
        "Elastic",
        "Characteristic,Construction,Packaging"
      ]
    }
  ]
});
formatter.scenario({
  "name": "User create 2d sizes and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Specification"
    },
    {
      "name": "@2DSizes"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Setup Enum for size dimensions in setup page \"DimensionType (1)\"",
  "keyword": "When "
});
formatter.match({
  "location": "SizesTK.setupEnumForSizeDimensionsInSetupPage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create the Enum value for the sizes \"Length\",\"2DSizeModul\",\"Waist\",\"2DSize\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SizesTK.createTheEnumValueForTheSizes(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User navigates to type config",
  "keyword": "And "
});
formatter.match({
  "location": "SizeLabelPage.userNavigatesToTypeConfig()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates the size spec data for size label module \"Characteristic,Construction,Packaging\"",
  "keyword": "When "
});
formatter.match({
  "location": "SizeLabelPage.userCreatesTheSizeSpecDataForSizeLabelModule(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Update the Enum through partial configuration",
  "keyword": "And "
});
formatter.match({
  "location": "SizesTK.updateTheEnumThroughPartialConfiguration()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "validate and accept the alert",
  "keyword": "Then "
});
formatter.match({
  "location": "ConfigurationPageSteps.validate_and_accept_the_alert()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates multiple sizes with different type for TwoDSizes \"small\",\"medium\",\"large\",\"SMALL\",\"XL\",\"XXL\",\"10\",\"11\",\"12\",\"13\",\"14\",\"15\"",
  "keyword": "When "
});
formatter.match({
  "location": "SizesTK.userCreatesMultipleSizesWithDifferentTypeForTwoDSizes(String,String,String,String,String,String,String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create duplicate Sizes and verify the error message \"SMALL\",\"16\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SizesTK.createDuplicateSizesAndVerifyTheErrorMessage(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create twoD sizes with different type of dimension \"W1\",\"W2\",\"W3\",\"L1\",\"L2\",\"L3\",\"Waist\",\"Waist\",\"Waist\",\"Length\",\"Length\",\"Length\",\"11\",\"12\",\"13\",\"14\",\"15\",\"16\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizesTK.createTwoDSizesWithDifferentTypeOfDimension(String,String,String,String,String,String,String,String,String,String,String,String,String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User selects the created sizes for TwoD sizes \"W1/L1\",\"W1/L2\",\"W1/L3\",\"W2/L1\",\"W2/L2\",\"W2/L3\",\"W3/L1\",\"W3/L2\",\"W3/L3\",\"W1\",\"W2\",\"W3\",\"L1\",\"L2\",\"L3\"",
  "keyword": "When "
});
formatter.match({
  "location": "SizesTK.userSelectsTheCreatedSizesForTwoDSizes(String,String,String,String,String,String,String,String,String,String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create size range for the sizes \"MensRegular\",\"2DSizeModul\",\"Waist\",\"Length\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizesTK.createSizeRangeForTheSizes(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User selects the sizes for the created size range \"W1/L1\",\"W1/L2\",\"W1/L3\",\"W2/L1\",\"W2/L2\",\"W2/L3\",\"W3/L1\",\"W3/L2\",\"W3/L3\",\"MensRegular\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SizesTK.userSelectsTheSizesForTheCreatedSizeRange(String,String,String,String,String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates simple size range for validation \"MensPants\",\"MensJeans\",\"Childrenswear\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizesTK.userCreatesSimpleSizeRangeForValidation(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Merging of composite size type \"small\",\"medium\",\"large\",\"SMALL\",\"XL\",\"XXL\",\"MensPants\",\"MensJeans\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SizesTK.mergingOfCompositeSizeType(String,String,String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Selecting the sub ranges for composite sizes \"MensPants\",\"MensJeans\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizesTK.selectingTheSubRangesForCompositeSizes(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Validate the options like copy and delete for size Range \"2DSizesCopy\",\"Childrenswear\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SizesTK.validateTheOptionsLikeCopyAndDeleteForSizeRange(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create a Size Map and validate the status \"MensRegular\",\"Childrenswear\",\"Elastic\",\"small\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizesTK.createASizeMapAndValidateTheStatus(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.logout_from_the_Application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Colour specification Creation and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@ColorSpecification"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "Click on specification tab and create color specification \"\u003cColorSpecificationName1\u003e\",\"\u003cCode\u003e\",\"\u003cDescription\u003e\",\"\u003cColorSpecificationName2\u003e\",\"\u003cCCode\u003e\",\"\u003cCDescription\u003e\",\"\u003cColorSpecificationName3\u003e\",\"\u003cColorSpecificationName4\u003e\",\"\u003cColorSpecificationName5\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User creates the Custom view and add the options to the table of Color spec \"\u003cAvailableAttributes\u003e\",\"\u003cSelectedAttributes\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Merge the color specification and validate the status after merging \"\u003cColorSpecificationName2\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Validate the options like copy and delete \"Reg_colorCopy\",\"\u003cColorSpecificationName1\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User delete the merged colour spec and verify the error message \"\u003cColorSpecificationName1\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "User enters the RBG value and verify the colour in the table \"\u003cColorSpecificationName2\u003e\",\"\u003cRGB Hex\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User enters the RBG value and colour in the table \"\u003cColorSpecificationName1\u003e\",\"\u003cColorSpecificationName3\u003e\",\"\u003cColorSpecificationName4\u003e\",\"\u003cColorSpecificationName5\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "ColorSpecificationName1",
        "Code",
        "Description",
        "ColorSpecificationName2",
        "CCode",
        "CDescription",
        "AvailableAttributes",
        "SelectedAttributes",
        "ColorName",
        "RGB Hex",
        "ColorSpecificationName3",
        "ColorSpecificationName4",
        "ColorSpecificationName5"
      ]
    },
    {
      "cells": [
        "11-0103 EGRET",
        "022",
        "RegressionValidation",
        "11-0104 VANILLA ICE",
        "023",
        "RegressionValidation",
        "RGB Hex",
        "Pantone,Pantone TC,Libraries,Tags",
        "Reg_color2",
        "#323232",
        "11-0105 ANTIQUE WHITE",
        "11-0107 PAPYRUS",
        "11-0205 GLASS GREEN"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Colour specification Creation and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Specification"
    },
    {
      "name": "@ColorSpecification"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Click on specification tab and create color specification \"11-0103 EGRET\",\"022\",\"RegressionValidation\",\"11-0104 VANILLA ICE\",\"023\",\"RegressionValidation\",\"11-0105 ANTIQUE WHITE\",\"11-0107 PAPYRUS\",\"11-0205 GLASS GREEN\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ColorSpecificationTK.clickOnSpecificationTabAndCreateColorSpecification(String,String,String,String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates the Custom view and add the options to the table of Color spec \"RGB Hex\",\"Pantone,Pantone TC,Libraries,Tags\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ColorSpecificationTK.userCreatesTheCustomViewAndAddTheOptionsToTheTable(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Merge the color specification and validate the status after merging \"11-0104 VANILLA ICE\"",
  "keyword": "And "
});
formatter.match({
  "location": "ColorSpecificationTK.mergeTheColorSpecificationAndValidateTheStatusAfterMerging(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Validate the options like copy and delete \"Reg_colorCopy\",\"11-0103 EGRET\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ColorSpecificationTK.validateThOptionsLikeCopyDelete(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User delete the merged colour spec and verify the error message \"11-0103 EGRET\"",
  "keyword": "When "
});
formatter.match({
  "location": "ColorSpecificationTK.userDeleteTheMergedColourSpecAndVerifyTheErrorMessage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User enters the RBG value and verify the colour in the table \"11-0104 VANILLA ICE\",\"#323232\"",
  "keyword": "And "
});
formatter.match({
  "location": "ColorSpecificationTK.userEntersTheRBGValueAndVerifyTheColourInTheTable(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User enters the RBG value and colour in the table \"11-0103 EGRET\",\"11-0105 ANTIQUE WHITE\",\"11-0107 PAPYRUS\",\"11-0205 GLASS GREEN\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ColorSpecificationTK.userEntersTheRBGValueAndColourInTheTable(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.logout_from_the_Application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/D2_Specification_Module.feature");
formatter.feature({
  "name": "The overall scenarios of specification module",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Specification"
    },
    {
      "name": "@overallspecification"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Print design Creation and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@PrintDesign"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user navigates to print design tab",
  "keyword": "When "
});
formatter.step({
  "name": "user creates print design data \"\u003cPrintDesign\u003e\",\"\u003cPrintType\u003e\",\"\u003cNoOfPrintPosition\u003e\",\"\u003cRepeatHeight\u003e\",\"\u003cRepeatWidth\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "user creates the new print color \"\u003cNewPrintColor\u003e\",\"\u003cPrintDesign1\u003e\",\"\u003cNewPrintColor1\u003e\",\"\u003ccode\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user navigates to position tab and create data \"\u003cPrintPositionName\u003e\",\"\u003cPrintTech\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "PrintDesign",
        "PrintType",
        "NoOfPrintPosition",
        "RepeatHeight",
        "RepeatWidth",
        "NewPrintColor",
        "PrintDesign1",
        "NewPrintColor1",
        "code",
        "PrintPositionName",
        "PrintTech"
      ]
    },
    {
      "cells": [
        "Abstract Floral,Batik Print,Lilac Print",
        "All Over,Placement,Logo",
        "3,4,6",
        "12,12,12",
        "12,12,24",
        "Blue Abstract Floral",
        "Abstract Floral",
        "Grey Abstract Floral",
        "PDC 100",
        "ground,leaf,swoosh",
        "Screen Print"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Print design Creation and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Specification"
    },
    {
      "name": "@overallspecification"
    },
    {
      "name": "@PrintDesign"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user navigates to print design tab",
  "keyword": "When "
});
formatter.match({
  "location": "ColorSpecificationTK.userNavigatesToPrintDesignTab()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates print design data \"Abstract Floral,Batik Print,Lilac Print\",\"All Over,Placement,Logo\",\"3,4,6\",\"12,12,12\",\"12,12,24\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ColorSpecificationTK.userCreatesPrintDesignData(String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates the new print color \"Blue Abstract Floral\",\"Abstract Floral\",\"Grey Abstract Floral\",\"PDC 100\"",
  "keyword": "And "
});
formatter.match({
  "location": "ColorSpecificationTK.userCreatesTheNewPrintColor(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user navigates to position tab and create data \"ground,leaf,swoosh\",\"Screen Print\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ColorSpecificationTK.userNavigatesToPositionTabAndCreateData(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Data creation for the size chart and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SizeChartCreation"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user navigates to size chart tab",
  "keyword": "When "
});
formatter.step({
  "name": "create the size charts \"\u003cSizeChart\u003e\",\"\u003cSizeChartDec\u003e\",\"\u003cSizeChartDecAlt\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "SizeChart",
        "SizeChartDec",
        "SizeChartDecAlt"
      ]
    },
    {
      "cells": [
        "A15E,A16S,A25FE,A26FS,A29,C122,C123,U550",
        "NECK WIDTH_EDGE TO EDGE,NECK WIDTH_SEAM TO SEAM,FRONT NECK DROP_EDGE TO EDGE,FRONT NECK DROP_SEAM TO SEAM,HPS DROP TO CLOSURE_LOW,WAIST_RIGID WAISTBAND,WAIST_ELASTIC BAND RELAXED,KNEE",
        "Measure neck opening straight across from inside edge to inside edge,Measure neck opening straight across from seam to seam,Measure from high point shoulder to center front neck edge,Measure from high point shoulder seam to center front neck seam,Measure from high point shoulder to center of first button,Align top edge of waistband Measure straight across from inside edge to inside edge (waist closure must be fastened),Measure with elastic relaxed and top edge of waistband aligned straight across top edge from inside edge to inside edge (waist closure must be fastened),Measure straight across leg grain at point indicated on specification"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Data creation for the size chart and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Specification"
    },
    {
      "name": "@overallspecification"
    },
    {
      "name": "@SizeChartCreation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user navigates to size chart tab",
  "keyword": "When "
});
formatter.match({
  "location": "SizeChartPage.userNavigatesToSizeChartTab()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "create the size charts \"A15E,A16S,A25FE,A26FS,A29,C122,C123,U550\",\"NECK WIDTH_EDGE TO EDGE,NECK WIDTH_SEAM TO SEAM,FRONT NECK DROP_EDGE TO EDGE,FRONT NECK DROP_SEAM TO SEAM,HPS DROP TO CLOSURE_LOW,WAIST_RIGID WAISTBAND,WAIST_ELASTIC BAND RELAXED,KNEE\",\"Measure neck opening straight across from inside edge to inside edge,Measure neck opening straight across from seam to seam,Measure from high point shoulder to center front neck edge,Measure from high point shoulder seam to center front neck seam,Measure from high point shoulder to center of first button,Align top edge of waistband Measure straight across from inside edge to inside edge (waist closure must be fastened),Measure with elastic relaxed and top edge of waistband aligned straight across top edge from inside edge to inside edge (waist closure must be fastened),Measure straight across leg grain at point indicated on specification\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SizeChartPage.createTheSizeCharts(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "SizeChart increment creation and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SizeChartIncrement"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "creates the increment for the size chart \"\u003cSizeIncrement\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "SizeIncrement"
      ]
    },
    {
      "cells": [
        "Womens alpha tops,Pants Grade Rule"
      ]
    }
  ]
});
formatter.scenario({
  "name": "SizeChart increment creation and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Specification"
    },
    {
      "name": "@overallspecification"
    },
    {
      "name": "@SizeChartIncrement"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "creates the increment for the size chart \"Womens alpha tops,Pants Grade Rule\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SizeChartPage.createsTheIncrementForTheSizeChart(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Mapping of values increment Tab and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@MappingofValuesIncrement"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user navigates to sizeRange tab for twoDsize validation \"MensRegular\"",
  "keyword": "When "
});
formatter.step({
  "name": "user navigates to increment tab and create custom view \"\u003cRemoveAttributes\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user maps the size range for the increments \"\u003cSizeIncrement\u003e\",\"\u003cSizeRange\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "user maps the dimensions inside the increments values \"\u003cIncrement1\u003e\",\"\u003cIncrement2\u003e\",\"\u003cdim1\u003e\",\"\u003cdim2\u003e\",\"\u003cdim3\u003e\",\"\u003cdim4\u003e\",\"\u003cdim5\u003e\",\"\u003cdim6\u003e\",\"\u003cdim7\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "SizeRange",
        "SizeIncrement",
        "Increment1",
        "Increment2",
        "dim1",
        "dim2",
        "dim3",
        "dim4",
        "dim5",
        "dim6",
        "dim7",
        "baseSize1",
        "baseSize2",
        "RemoveAttributes"
      ]
    },
    {
      "cells": [
        "Childrenswear,MensRegular",
        "Womens alpha tops,Pants Grade Rule",
        "Womens alpha tops",
        "Pants Grade Rule",
        "C122",
        "C123",
        "A15E",
        "A16S",
        "A25FE",
        "A29",
        "A26FS",
        "small",
        "large",
        "Description,Tags"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Mapping of values increment Tab and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Specification"
    },
    {
      "name": "@overallspecification"
    },
    {
      "name": "@MappingofValuesIncrement"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user navigates to sizeRange tab for twoDsize validation \"MensRegular\"",
  "keyword": "When "
});
formatter.match({
  "location": "SizeChartPage.userNavigatesToSizeRangeTabForTwoDsizeValidation(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user navigates to increment tab and create custom view \"Description,Tags\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizeChartPage.userNavigatesToIncrementTabAndCreateCustomView(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user maps the size range for the increments \"Womens alpha tops,Pants Grade Rule\",\"Childrenswear,MensRegular\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SizeChartPage.userMapsTheSizeRangeForTheIncrements(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user maps the dimensions inside the increments values \"Womens alpha tops\",\"Pants Grade Rule\",\"C122\",\"C123\",\"A15E\",\"A16S\",\"A25FE\",\"A29\",\"A26FS\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizeChartPage.userMapsTheDimensionsInsideTheIncrementsValues(String,String,String,String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "User creates sizeLabel and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SizeLabel"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user navigates to size label tab",
  "keyword": "When "
});
formatter.step({
  "name": "user creates the size label \"\u003cSizeLabel\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Validate the options like copy and delete for size label \"Horizontal Size Label-COPY\",\"\u003cSizeLabelCopy\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "SizeLabel",
        "SizeLabelCopy"
      ]
    },
    {
      "cells": [
        "Horizontal Size Label,Vertical Size Label",
        "Horizontal Size Label"
      ]
    }
  ]
});
formatter.scenario({
  "name": "User creates sizeLabel and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Specification"
    },
    {
      "name": "@overallspecification"
    },
    {
      "name": "@SizeLabel"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user navigates to size label tab",
  "keyword": "When "
});
formatter.match({
  "location": "SizeLabelPage.userNavigatesToSizeLabelTab()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates the size label \"Horizontal Size Label,Vertical Size Label\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizeLabelPage.userCreatesTheSizeLabel(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Validate the options like copy and delete for size label \"Horizontal Size Label-COPY\",\"Horizontal Size Label\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SizeLabelPage.validateTheOptionsLikeCopyAndDeleteForSizeLabel(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "User creates SizeSpecs and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SizeSpecification"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user navigates to size spec tab",
  "keyword": "When "
});
formatter.step({
  "name": "user creates the size spec \"\u003cSizeSpec\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Validate the options like copy and delete for size spec \"\u003cSizeSpecCopy\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "SizeSpec",
        "SizeSpecCopy"
      ]
    },
    {
      "cells": [
        "12,14,16,4*6,8x11",
        "12"
      ]
    }
  ]
});
formatter.scenario({
  "name": "User creates SizeSpecs and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Specification"
    },
    {
      "name": "@overallspecification"
    },
    {
      "name": "@SizeSpecification"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user navigates to size spec tab",
  "keyword": "When "
});
formatter.match({
  "location": "sizeSpecPage.userNavigatesToSizeSpecTab()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates the size spec \"12,14,16,4*6,8x11\"",
  "keyword": "And "
});
formatter.match({
  "location": "sizeSpecPage.userCreatesTheSizeSpec(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Validate the options like copy and delete for size spec \"12\"",
  "keyword": "Then "
});
formatter.match({
  "location": "sizeSpecPage.validateTheOptionsLikeCopyAndDeleteForSizeSpec(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "User creates size items and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SizeItems"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user navigates to size items tab",
  "keyword": "When "
});
formatter.step({
  "name": "user creates the size item \"\u003cSizeItem\u003e\",\"\u003cSizeSpec\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user creates the size item with all the types for Template scenario \"\u003cSizeItems\u003e\",\"\u003cSizeSpec\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "user creates the spec items with different types \"\u003cValueItems\u003e\",\"\u003cSizeSpec\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Validate the options like copy and delete for size item \"Reg_colorCopy\",\"\u003cSizeItemCopy\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "user creation of tags in the size item \"\u003cSizeItem\u003e\",\"\u003cSizeItemTag\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "SizeItem",
        "SizeItemCopy",
        "SizeSpec",
        "SizeItemTag",
        "SizeItems",
        "ValueItems"
      ]
    },
    {
      "cells": [
        "Armhole_Bound,Armhole_Faced,Belt Loops_Clean Finish Make",
        "Armhole_Bound",
        "Characteristic,Construction,Packaging",
        "sleeve,armhole,waistband",
        "Alphanumeric,Barcode,Email Address,Hyperlink,Latest Multi-line Comment,Multi-line Comment,Multi-line Text,Phone,Rich Text",
        "double,integer,ref,reflist"
      ]
    }
  ]
});
formatter.scenario({
  "name": "User creates size items and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Specification"
    },
    {
      "name": "@overallspecification"
    },
    {
      "name": "@SizeItems"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user navigates to size items tab",
  "keyword": "When "
});
formatter.match({
  "location": "SizeItemsSteps.userNavigatesToSizeItemsTab()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates the size item \"Armhole_Bound,Armhole_Faced,Belt Loops_Clean Finish Make\",\"Characteristic,Construction,Packaging\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizeItemsSteps.userCreatesTheSizeItem(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates the size item with all the types for Template scenario \"Alphanumeric,Barcode,Email Address,Hyperlink,Latest Multi-line Comment,Multi-line Comment,Multi-line Text,Phone,Rich Text\",\"Characteristic,Construction,Packaging\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SizeItemsSteps.userCreatesTheSizeItemWithAllTheTypesForTemplateScenario(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates the spec items with different types \"double,integer,ref,reflist\",\"Characteristic,Construction,Packaging\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizeItemsSteps.userCreatesTheSpecItemsWithDifferentTypes(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Validate the options like copy and delete for size item \"Reg_colorCopy\",\"Armhole_Bound\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SizeItemsSteps.validateTheOptionsLikeCopyAndDeleteForSizeItem(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creation of tags in the size item \"Armhole_Bound,Armhole_Faced,Belt Loops_Clean Finish Make\",\"sleeve,armhole,waistband\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizeItemsSteps.userCreationOfTagsInTheSizeItem(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Lookup creation and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@LookUp"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user navigates to lookup tab",
  "keyword": "When "
});
formatter.step({
  "name": "user creates the lookup \"\u003cLookUp\u003e\",\"\u003cLookUpTypes\u003e\",\"\u003cCode\u003e\",\"\u003cdesc\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user creates the lookup \"\u003cLookUp1\u003e\",\"\u003cLookUpTypes2\u003e\",\"\u003cCode\u003e\",\"\u003cdesc\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user creates the lookup \"\u003cLookUp2\u003e\",\"\u003cLookUpTypes3\u003e\",\"\u003cCode\u003e\",\"\u003cdesc\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Validate the options like copy and delete for lookup \"Reg_colorCopy\",\"\u003cLookUpCopy\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "LookUp",
        "LookUpCopy",
        "LookUpTypes",
        "LookUpTypes2",
        "LookUpTypes3",
        "LookUp1",
        "LookUp2",
        "Code",
        "desc"
      ]
    },
    {
      "cells": [
        "01_Low Cost Factor,02_Mid Cost Factor,03_High Cost Factor,04_Very High Cost Factor,05_Speciality Products Only",
        "005a_Create Design BOM_Apparel_Carryover",
        "Cost Factors",
        "Holiday Calendar",
        "User Task Lead Times",
        "IND-Dussehra-2020,IND-Dussehra-2019,IND-Diwali-2020,IND-Diwali-2019,IND-Christmas-2020",
        "001_Fill Set-up details_Apparel_Carryover,002_Review and Approve Style Targets_Apparel_Carryover,003_Create Colorways_Apparel_Carryover,004_Upload Sketches_Apparel_Carryover,005a_Create Design BOM_Apparel_Carryover",
        "1,2,3,4,5",
        "LookupData"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Lookup creation and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Specification"
    },
    {
      "name": "@overallspecification"
    },
    {
      "name": "@LookUp"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user navigates to lookup tab",
  "keyword": "When "
});
formatter.match({
  "location": "LookUpSteps.userNavigatesToLookupTab()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates the lookup \"01_Low Cost Factor,02_Mid Cost Factor,03_High Cost Factor,04_Very High Cost Factor,05_Speciality Products Only\",\"Cost Factors\",\"1,2,3,4,5\",\"LookupData\"",
  "keyword": "And "
});
formatter.match({
  "location": "LookUpSteps.userCreatesTheLookup(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates the lookup \"IND-Dussehra-2020,IND-Dussehra-2019,IND-Diwali-2020,IND-Diwali-2019,IND-Christmas-2020\",\"Holiday Calendar\",\"1,2,3,4,5\",\"LookupData\"",
  "keyword": "And "
});
formatter.match({
  "location": "LookUpSteps.userCreatesTheLookup(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates the lookup \"001_Fill Set-up details_Apparel_Carryover,002_Review and Approve Style Targets_Apparel_Carryover,003_Create Colorways_Apparel_Carryover,004_Upload Sketches_Apparel_Carryover,005a_Create Design BOM_Apparel_Carryover\",\"User Task Lead Times\",\"1,2,3,4,5\",\"LookupData\"",
  "keyword": "And "
});
formatter.match({
  "location": "LookUpSteps.userCreatesTheLookup(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Validate the options like copy and delete for lookup \"Reg_colorCopy\",\"005a_Create Design BOM_Apparel_Carryover\"",
  "keyword": "Then "
});
formatter.match({
  "location": "LookUpSteps.validateTheOptionsLikeCopyAndDeleteForLookup(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Product alternative and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@ProductAlternative"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user navigates to product alternative tab",
  "keyword": "When "
});
formatter.step({
  "name": "user creates the product alternative data \"\u003cProductAlternative\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Validate the options like copy and delete for size label \"China-COPY\",\"\u003cProductAlternativeCopy\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "ProductAlternative",
        "ProductAlternativeCopy"
      ]
    },
    {
      "cells": [
        "China,Italy,Vietnam (no qoute)",
        "China"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Product alternative and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Specification"
    },
    {
      "name": "@overallspecification"
    },
    {
      "name": "@ProductAlternative"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user navigates to product alternative tab",
  "keyword": "When "
});
formatter.match({
  "location": "LookUpSteps.userNavigatesToProductAlternativeTab()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates the product alternative data \"China,Italy,Vietnam (no qoute)\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductAlternativeSteps.userCreatesTheProductAlternativeData(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Validate the options like copy and delete for size label \"China-COPY\",\"China\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SizeLabelPage.validateTheOptionsLikeCopyAndDeleteForSizeLabel(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Libraries Creation and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Libraries"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user navigates to tab",
  "keyword": "When "
});
formatter.step({
  "name": "user creates the data for the libraries \"\u003cCareLibrary\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "CareLibrary"
      ]
    },
    {
      "cells": [
        "Electric Boho"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Libraries Creation and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Specification"
    },
    {
      "name": "@overallspecification"
    },
    {
      "name": "@Libraries"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user navigates to tab",
  "keyword": "When "
});
formatter.match({
  "location": "ColorSpecificationTK.userNavigatesToLibrariesTab()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates the data for the libraries \"Electric Boho\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ColorSpecificationTK.userCreatesTheDataForTheLibraries(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenario({
  "name": "Libraries Creation and validation",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Specification"
    },
    {
      "name": "@overallspecification"
    },
    {
      "name": "@LibrariesValidation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "user creates the data for the color libraries \"Blue Haze\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ColorSpecificationTK.userCreatesTheDataForTheColorLibraries(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenario({
  "name": "Libraries Creation and validation",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Specification"
    },
    {
      "name": "@overallspecification"
    },
    {
      "name": "@LibrariesMaaterial"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates the data for New Material library \"Fall 2020 Materials\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ColorSpecificationTK.userCreatesTheDataForNewMaterialLibrary(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenario({
  "name": "Libraries Creation and validation",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Specification"
    },
    {
      "name": "@overallspecification"
    },
    {
      "name": "@LibrariesPrints"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates the data for print design library \"Fall 2020 Concept Prints\"",
  "keyword": "And "
});
formatter.match({
  "location": "ColorSpecificationTK.userCreatesTheDataForPrintDesignLibrary(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenario({
  "name": "Libraries Creation and validation",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Specification"
    },
    {
      "name": "@overallspecification"
    },
    {
      "name": "@LibrariesWinter"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "user creates the data for New size library \"Winter 2020 3D\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ColorSpecificationTK.userCreatesTheDataForNewSizeLibrary(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "User create templates",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Templates"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user navigates to template tab",
  "keyword": "When "
});
formatter.step({
  "name": "user creates the template \"\u003cTemplate\u003e\",\"\u003cType\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user creates the datasheet template for style \"\u003cDataSheetType\u003e\",\"\u003cTemplateStyle\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "user creates the datasheet template for material \"\u003cDataSheetType1\u003e\",\"\u003cTemplateMaterial\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Template",
        "Type",
        "DataSheetType",
        "TemplateStyle",
        "DataSheetType1",
        "TemplateMaterial"
      ]
    },
    {
      "cells": [
        "Costing Package,DPT Material",
        "Style,Material",
        "Artwork,Routing,Size Chart,Style BOM",
        "Costing Package",
        "Test Run,Material BOM,Routing,Material Data Sheet",
        "DPT Material"
      ]
    }
  ]
});
formatter.scenario({
  "name": "User create templates",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Specification"
    },
    {
      "name": "@overallspecification"
    },
    {
      "name": "@Templates"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user navigates to template tab",
  "keyword": "When "
});
formatter.match({
  "location": "ColorSpecificationTK.userNavigatesToTemplateTab()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates the template \"Costing Package,DPT Material\",\"Style,Material\"",
  "keyword": "And "
});
formatter.match({
  "location": "ColorSpecificationTK.userCreatesTheTemplate(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates the datasheet template for style \"Artwork,Routing,Size Chart,Style BOM\",\"Costing Package\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ColorSpecificationTK.userCreatesTheDatasheetTemplateForStyle(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates the datasheet template for material \"Test Run,Material BOM,Routing,Material Data Sheet\",\"DPT Material\"",
  "keyword": "And "
});
formatter.match({
  "location": "ColorSpecificationTK.userCreatesTheDatasheetTemplateForMaterial(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "User create canvas",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@canvas"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user navigates to canvas tab",
  "keyword": "When "
});
formatter.step({
  "name": "user creates a manage view for the page \"\u003cRemoveAttributes\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "user creates the canvas template \"\u003cTemplate\u003e\",\"\u003cType\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "verify the description of canvas template screen message \"\u003cTemplateStyle\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "create a canvas and select the rectangle box",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Template",
        "Type",
        "TemplateStyle",
        "RemoveAttributes"
      ]
    },
    {
      "cells": [
        "1st Proto Review-Apparel,Fit Correction:Across Back",
        "Artwork,Style Review",
        "1st Proto Review-Apparel",
        "Description,Subtype"
      ]
    }
  ]
});
formatter.scenario({
  "name": "User create canvas",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Specification"
    },
    {
      "name": "@overallspecification"
    },
    {
      "name": "@canvas"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user navigates to canvas tab",
  "keyword": "When "
});
formatter.match({
  "location": "ColorSpecificationTK.userNavigatesToCanvasTab()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates a manage view for the page \"Description,Subtype\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ColorSpecificationTK.userCreatesAManageViewForThePage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates the canvas template \"1st Proto Review-Apparel,Fit Correction:Across Back\",\"Artwork,Style Review\"",
  "keyword": "And "
});
formatter.match({
  "location": "ColorSpecificationTK.userCreatesTheCanvasTemplate(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify the description of canvas template screen message \"1st Proto Review-Apparel\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ColorSpecificationTK.verifyTheDescriptionOfCanvasTemplateScreenMessage(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "create a canvas and select the rectangle box",
  "keyword": "And "
});
formatter.match({
  "location": "ColorSpecificationTK.createACanvasAndSelectTheRectangleBox()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "SizeChart Canvas creation \u0026 validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@canvasCreation"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user navigates to SizeCanvas tab",
  "keyword": "When "
});
formatter.step({
  "name": "user creates the sizeChart canvas \"\u003cSizeChartName\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "user creates the dimension inside the canvas \"Bra\"",
  "keyword": "And "
});
formatter.step({
  "name": "user enters the values in the dimensions \"\u003cDimensions\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "SizeChartName",
        "Dimensions"
      ]
    },
    {
      "cells": [
        "Bra,Womens Basic Knit Tee",
        "A15E"
      ]
    }
  ]
});
formatter.scenario({
  "name": "SizeChart Canvas creation \u0026 validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Specification"
    },
    {
      "name": "@overallspecification"
    },
    {
      "name": "@canvasCreation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user navigates to SizeCanvas tab",
  "keyword": "When "
});
formatter.match({
  "location": "ColorSpecificationTK.userNavigatesToSizeCanvasTab()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates the sizeChart canvas \"Bra,Womens Basic Knit Tee\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ColorSpecificationTK.userCreatesTheSizeChartCanvas(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates the dimension inside the canvas \"Bra\"",
  "keyword": "And "
});
formatter.match({
  "location": "ColorSpecificationTK.userCreatesTheDimensionInsideTheCanvas(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user enters the values in the dimensions \"A15E\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ColorSpecificationTK.userEntersTheValuesInTheDimensions(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "SpecSection definition Canvas creation \u0026 validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@canvas"
    },
    {
      "name": "@SpecificationSizeDefinitions"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user navigates to SpecSizeCanvas tab",
  "keyword": "When "
});
formatter.step({
  "name": "user creates the Spec Section definitions canvas \"\u003cSpecSectionName\u003e\",\"\u003cSizeSpec\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "user creates the data inside the characteristics value \"\u003cNameOfNewSpecSectionInsideChar\u003e\",\"\u003cSpecSectionName\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user selects the values from the options of spec items \"\u003cNameOfNewSpecSectionInsideChar\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "user creates a custom view to verify the data output \"\u003cAttributes\u003e\",\"\u003cDeselectAttributes\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user validate the special created data",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "SpecSectionName",
        "SizeSpec",
        "NameOfNewSpecSectionInsideChar",
        "Attributes",
        "DeselectAttributes"
      ]
    },
    {
      "cells": [
        "Model of Characteristics,KnitBottoms,Lighting Specifications",
        "Characteristic,Construction,Packaging",
        "Mount holder,General Construction",
        "Value Type",
        "Description,Image (Spec Item)"
      ]
    }
  ]
});
formatter.scenario({
  "name": "SpecSection definition Canvas creation \u0026 validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Specification"
    },
    {
      "name": "@overallspecification"
    },
    {
      "name": "@canvas"
    },
    {
      "name": "@SpecificationSizeDefinitions"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user navigates to SpecSizeCanvas tab",
  "keyword": "When "
});
formatter.match({
  "location": "SizeItemsSteps.userNavigatesToSpecSizeCanvasTab()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates the Spec Section definitions canvas \"Model of Characteristics,KnitBottoms,Lighting Specifications\",\"Characteristic,Construction,Packaging\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SizeItemsSteps.userCreatesTheSpecSectionDefinitionsCanvas(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates the data inside the characteristics value \"Mount holder,General Construction\",\"Model of Characteristics,KnitBottoms,Lighting Specifications\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizeItemsSteps.userCreatesTheDataInsideTheCharacteristicsValue(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user selects the values from the options of spec items \"Mount holder,General Construction\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SizeItemsSteps.userSelectsTheValuesFromTheOptionsOfSpecItems(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates a custom view to verify the data output \"Value Type\",\"Description,Image (Spec Item)\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizeItemsSteps.userCreatesACustomViewToVerifyTheDataOutput(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user validate the special created data",
  "keyword": "Then "
});
formatter.match({
  "location": "SizeItemsSteps.userValidateTheSpecialCreatedData()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/D3_SizeValidation.feature");
formatter.feature({
  "name": "validation scenario of size \u0026 style",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Specification"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Size validation \u0026 Colour validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Size\u0026ColorValidation"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "Click style tab and get the listed season name in the style tab",
  "keyword": "And "
});
formatter.step({
  "name": "User create New season with mandatory deatails for season creation \"\u003cseason\u003e\",\"\u003cscode\u003e\",\"\u003csdescription\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "Click New Brand",
  "keyword": "Then "
});
formatter.step({
  "name": "Save the New Brand which was created",
  "keyword": "Then "
});
formatter.step({
  "name": "Click New department by providing valid data \"\u003cdept\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Save the New department which was created",
  "keyword": "Then "
});
formatter.step({
  "name": "Click New collection",
  "keyword": "Then "
});
formatter.step({
  "name": "Create collection by providing valida and mandatory details \"\u003ccollection\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Save the collection which was created",
  "keyword": "Then "
});
formatter.step({
  "name": "Click New style",
  "keyword": "Then "
});
formatter.step({
  "name": "Create Style followed by its style Type \"\u003cStyleTypeA\u003e\",\"\u003cStyleTypeA\u003e\",\"\u003cStyleTypeA\u003e\",\"\u003cStyleTypeA\u003e\",\"\u003cStyleName1\u003e\",\"\u003cStyleName2\u003e\",\"\u003cStyleName3\u003e\",\"\u003cStyleName4\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Click New style",
  "keyword": "Then "
});
formatter.step({
  "name": "Create Style followed by its style Type \"\u003cStyleTypeB\u003e\",\"\u003cStyleTypeB1\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Create Style followed by its style Type \"\u003cStyleTypeC\u003e\",\"\u003cStyleTypeC1\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Create Style followed by its style Type \"\u003cStyleTypeD\u003e\",\"\u003cStyleTypeD1\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User creates the Custom view and add the options to the table \"Colorway\",\"Color Specification\"",
  "keyword": "When "
});
formatter.step({
  "name": "User deletes the unwanted attributes from custom views column for styles \"\u003cSelectedAttributes1\u003e\",\"\u003cSelectedAttributes2\u003e\",\"\u003cSelectedAttributes3\u003e\",\"\u003cSelectedAttributes4\u003e\",\"\u003cSelectedAttributes5\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User selects the size range for the styles \"\u003cStyleName1\u003e\",\"\u003cStyleName2\u003e\",\"\u003cStyleName3\u003e\",\"\u003cStyleName4\u003e\",\"\u003cSizeRange\u003e\",\"\u003cSimpleSizeName1\u003e\",\"\u003cSimpleSizeName2\u003e\",\"\u003cCompSizeName\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User selects the colorway through option from the styles page itself \"\u003cStyleName4\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "user selects the colorway for the styles \"\u003cStyleName1\u003e\",\"\u003cNavignSearchtype\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User creates the colorway for the style and maps it \"\u003cColorSpecificationName2\u003e\",\"\u003cColorSpecificationName3\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "user selects the colorway for the styles \"\u003cStyleName2\u003e\",\"\u003cNavignSearchtype\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User creates the colorway for the style and maps it \"\u003cColorSpecificationName4\u003e\",\"\u003cColorSpecificationName5\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "user selects the colorway for the styles \"\u003cStyleName3\u003e\",\"\u003cNavignSearchtype\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User creates the colorway for the style and maps it \"\u003cColorSpecificationName4\u003e\",\"\u003cColorSpecificationName1\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "StyleTypeA",
        "StyleTypeB",
        "StyleTypeC",
        "StyleTypeD",
        "season",
        "scode",
        "sdescription",
        "brand",
        "bcode",
        "bdescription",
        "dept",
        "dcode",
        "ddescription",
        "collection",
        "ccode",
        "cdesription",
        "SelectedAttributes1",
        "SelectedAttributes2",
        "SelectedAttributes3",
        "SelectedAttributes4",
        "StyleName1",
        "StyleName2",
        "StyleName3",
        "StyleName4",
        "SizeRange",
        "SimpleSizeName1",
        "SimpleSizeName2",
        "CompSizeName",
        "NavignSearchtype",
        "ColorSpecificationName2",
        "ColorSpecificationName3",
        "ColorSpecificationName4",
        "ColorSpecificationName5",
        "ColorSpecificationName1",
        "SelectedAttributes5",
        "StyleTypeB1",
        "StyleTypeC1",
        "StyleTypeD1"
      ]
    },
    {
      "cells": [
        "Apparel - Color and Size",
        "Accessories - Only Color",
        "Apparel - Only Size",
        "Accessories - No color and Size",
        "01 - Spring 2020",
        "SP20",
        "Spring 2020 Development Season",
        "Brand X",
        "B03",
        "SizeValidation",
        "Accessories",
        "105",
        "pant",
        "Belts",
        "110",
        "cotton",
        "Description",
        "Tool (Style Specification)",
        "Development Type",
        "Theme",
        "HC\u0026S1",
        "HC\u0026S2",
        "HC\u0026S3",
        "HC\u0026S4",
        "MensRegular",
        "MensPants",
        "MensJeans",
        "Childrenswear",
        "Style",
        "11-0104 VANILLA ICE",
        "11-0105 ANTIQUE WHITE",
        "11-0107 PAPYRUS",
        "11-0205 GLASS GREEN",
        "11-0103 EGRET",
        "Code",
        "HomeAssortmentOC2",
        "HardgoodsOS1",
        "FootwearNC\u0026NS1"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Size validation \u0026 Colour validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Specification"
    },
    {
      "name": "@Size\u0026ColorValidation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Click style tab and get the listed season name in the style tab",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.click_style_tab_and_get_the_listed_season_name_in_the_style_tab()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User create New season with mandatory deatails for season creation \"01 - Spring 2020\",\"SP20\",\"Spring 2020 Development Season\"",
  "keyword": "When "
});
formatter.match({
  "location": "StylePageSteps.user_create_New_season_with_mandatory_deatails_for_season_creation(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Click New Brand",
  "keyword": "Then "
});
formatter.match({
  "location": "StylePageSteps.click_New_Brand()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Save the New Brand which was created",
  "keyword": "Then "
});
formatter.match({
  "location": "StylePageSteps.save_the_New_Brand_which_was_created()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Click New department by providing valid data \"Accessories\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SizesTK.clickNewDepartmentByProvidingValidData(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Save the New department which was created",
  "keyword": "Then "
});
formatter.match({
  "location": "StylePageSteps.save_the_New_department_which_was_created()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Click New collection",
  "keyword": "Then "
});
formatter.match({
  "location": "StylePageSteps.click_New_collection()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create collection by providing valida and mandatory details \"Belts\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizesTK.createCollectionByProvidingValidaAndMandatoryDetails(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Save the collection which was created",
  "keyword": "Then "
});
formatter.match({
  "location": "StylePageSteps.save_the_collection_which_was_created()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Click New style",
  "keyword": "Then "
});
formatter.match({
  "location": "StylePageSteps.click_New_style()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create Style followed by its style Type \"Apparel - Color and Size\",\"Apparel - Color and Size\",\"Apparel - Color and Size\",\"Apparel - Color and Size\",\"HC\u0026S1\",\"HC\u0026S2\",\"HC\u0026S3\",\"HC\u0026S4\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizesTK.createStyleFollowedByItsStyleType(String,String,String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Click New style",
  "keyword": "Then "
});
formatter.match({
  "location": "StylePageSteps.click_New_style()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create Style followed by its style Type \"Accessories - Only Color\",\"HomeAssortmentOC2\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizesTK.createStyleFollowedByItsStyleType(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create Style followed by its style Type \"Apparel - Only Size\",\"HardgoodsOS1\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizesTK.createStyleFollowedByItsStyleType(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create Style followed by its style Type \"Accessories - No color and Size\",\"FootwearNC\u0026NS1\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizesTK.createStyleFollowedByItsStyleType(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates the Custom view and add the options to the table \"Colorway\",\"Color Specification\"",
  "keyword": "When "
});
formatter.match({
  "location": "SizesTK.userCreatesTheCustomViewAndAddTheOptionsToTheTable(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User deletes the unwanted attributes from custom views column for styles \"Description\",\"Tool (Style Specification)\",\"Development Type\",\"Theme\",\"Code\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizesTK.userDeletesTheUnwantedAttributesFromCustomViewsColumnForStyles(String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User selects the size range for the styles \"HC\u0026S1\",\"HC\u0026S2\",\"HC\u0026S3\",\"HC\u0026S4\",\"MensRegular\",\"MensPants\",\"MensJeans\",\"Childrenswear\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SizesTK.userSelectsTheSizeRangeForTheStyles(String,String,String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User selects the colorway through option from the styles page itself \"HC\u0026S4\"",
  "keyword": "When "
});
formatter.match({
  "location": "SizesTK.userSelectsTheColorwayThroughOptionFromTheStylesPageItself(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user selects the colorway for the styles \"HC\u0026S1\",\"Style\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizesTK.userSelectsTheColorwayForTheStyles(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates the colorway for the style and maps it \"11-0104 VANILLA ICE\",\"11-0105 ANTIQUE WHITE\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SizesTK.userCreatesTheColorwayForTheStyleAndMapsIt(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user selects the colorway for the styles \"HC\u0026S2\",\"Style\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizesTK.userSelectsTheColorwayForTheStyles(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates the colorway for the style and maps it \"11-0107 PAPYRUS\",\"11-0205 GLASS GREEN\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SizesTK.userCreatesTheColorwayForTheStyleAndMapsIt(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user selects the colorway for the styles \"HC\u0026S3\",\"Style\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizesTK.userSelectsTheColorwayForTheStyles(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates the colorway for the style and maps it \"11-0107 PAPYRUS\",\"11-0103 EGRET\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SizesTK.userCreatesTheColorwayForTheStyleAndMapsIt(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.logout_from_the_Application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/E_Sourcing_library_Creation.feature");
formatter.feature({
  "name": "Sourcing Validation",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Sourcing"
    }
  ]
});
formatter.scenarioOutline({
  "name": "capability and operation group validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@CapabilityandOperation"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates capability \"\u003cShipping Port\u003e\",\"\u003cFactory\u003e\",\"\u003cSupplier\u003e\" and Operation group \"\u003cOperationGroup\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "User creates sub routing \"\u003cSubRoutingA\u003e\",\"\u003cSubRoutingB\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Shipping Port",
        "Factory",
        "Supplier",
        "OperationGroup",
        "SubRoutingA",
        "SubRoutingB"
      ]
    },
    {
      "cells": [
        "Cut - Shipping Port",
        "Pack - Factory",
        "Ship - Supplier",
        "MAKE",
        "MAKE Subrouting",
        "SHIP Subrouting"
      ]
    }
  ]
});
formatter.scenario({
  "name": "capability and operation group validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Sourcing"
    },
    {
      "name": "@CapabilityandOperation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates capability \"Cut - Shipping Port\",\"Pack - Factory\",\"Ship - Supplier\" and Operation group \"MAKE\"",
  "keyword": "When "
});
formatter.match({
  "location": "SourcingPageSteps.user_creates_capability_and_Operation_group(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates sub routing \"MAKE Subrouting\",\"SHIP Subrouting\"",
  "keyword": "And "
});
formatter.match({
  "location": "SourcingPageSteps.user_creates_sub_routing(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Sales Region and country creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SalesRegion"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates Country \"\u003cCountryA\u003e\",\"\u003cCountryB\u003e\",\"\u003cCountryC\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User creates sales region \"\u003cSalesRegionA\u003e\",\"\u003cSalesRegionB\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "CountryA",
        "CountryB",
        "CountryC",
        "SalesRegionA",
        "SalesRegionB"
      ]
    },
    {
      "cells": [
        "India-IND-Asia",
        "China-CHN-Asia",
        "United States-USA-North America",
        "CAD SR",
        "USA SR"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Sales Region and country creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Sourcing"
    },
    {
      "name": "@SalesRegion"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates Country \"India-IND-Asia\",\"China-CHN-Asia\",\"United States-USA-North America\"",
  "keyword": "And "
});
formatter.match({
  "location": "SourcingPageSteps.user_creates_Country(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates sales region \"CAD SR\",\"USA SR\"",
  "keyword": "And "
});
formatter.match({
  "location": "SourcingPageSteps.user_creates_sales_region(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "HTS code creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@HtsCode"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates HTS code \"\u003cHTS_A\u003e\",\"\u003cHTS_B\u003e\",\"\u003cDescription\u003e\",\"\u003cFrom\u003e\",\"\u003cTo\u003e\",\"\u003cDuty\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "HTS_A",
        "HTS_B",
        "Description",
        "From",
        "To",
        "Duty"
      ]
    },
    {
      "cells": [
        "India HTS Code",
        "USA HTS Code",
        "Automation",
        "India/United States",
        "United States/India",
        "15/20"
      ]
    }
  ]
});
formatter.scenario({
  "name": "HTS code creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Sourcing"
    },
    {
      "name": "@HtsCode"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates HTS code \"India HTS Code\",\"USA HTS Code\",\"Automation\",\"India/United States\",\"United States/India\",\"15/20\"",
  "keyword": "And "
});
formatter.match({
  "location": "SourcingPageSteps.user_creates_HTS_code(String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Container creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@ContainerCreation"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates container \"\u003ccontainer\u003e\",\"\u003cVolume\u003e\",\"\u003cWeight\u003e\",\"\u003cEfficiency\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "container",
        "Volume",
        "Weight",
        "Efficiency"
      ]
    },
    {
      "cells": [
        "Container",
        "30/40",
        "15/10",
        "70/80"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Container creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Sourcing"
    },
    {
      "name": "@ContainerCreation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates container \"Container\",\"30/40\",\"15/10\",\"70/80\"",
  "keyword": "And "
});
formatter.match({
  "location": "SourcingPageSteps.user_creates_container(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Shipping Port creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@ShippingPort"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates shipping port \"\u003cportA\u003e\",\"\u003cportB\u003e\",\"\u003cportC\u003e\",\"\u003cportD\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "portA",
        "portB",
        "portC",
        "portD"
      ]
    },
    {
      "cells": [
        "Chennai Port/India/231",
        "Mumbai Port/India/123",
        "SFS port/United States/456",
        "New York Port/United States/7855"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Shipping Port creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Sourcing"
    },
    {
      "name": "@ShippingPort"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates shipping port \"Chennai Port/India/231\",\"Mumbai Port/India/123\",\"SFS port/United States/456\",\"New York Port/United States/7855\"",
  "keyword": "And "
});
formatter.match({
  "location": "SourcingPageSteps.user_creates_shipping_port(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Shpping Rate creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@ShippingRateCreation"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates shipping rate \"\u003crateA\u003e\",\"\u003crateB\u003e\",\"\u003cFreightrate\u003e\",\"\u003corigin\u003e\",\"\u003cdestination\u003e\",\"\u003ccontainer\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "rateA",
        "rateB",
        "Freightrate",
        "origin",
        "destination",
        "container"
      ]
    },
    {
      "cells": [
        "India - United States",
        "United States - India",
        "30000/40000",
        "Mumbai/SFS",
        "SFS/Mumbai",
        "Container"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Shpping Rate creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Sourcing"
    },
    {
      "name": "@ShippingRateCreation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates shipping rate \"India - United States\",\"United States - India\",\"30000/40000\",\"Mumbai/SFS\",\"SFS/Mumbai\",\"Container\"",
  "keyword": "And "
});
formatter.match({
  "location": "SourcingPageSteps.user_creates_shipping_rate(String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Review creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@ReviewCreation"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates and validates Questions subsection\"\u003cPercentage\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User validates review templates \"\u003cReview\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Review",
        "Percentage"
      ]
    },
    {
      "cells": [
        "Sourcing Review Template - Automation",
        "25/55"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Review creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Sourcing"
    },
    {
      "name": "@ReviewCreation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates and validates Questions subsection\"25/55\"",
  "keyword": "And "
});
formatter.match({
  "location": "SourcingPageSteps.user_creates_and_validates_Questions_subsection(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates review templates \"Sourcing Review Template - Automation\"",
  "keyword": "And "
});
formatter.match({
  "location": "SourcingPageSteps.user_validates_review_templates(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Template creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@NewTemplateCreation"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates routing template \"\u003cSRoutingTemp\u003e\",\"\u003cDescription\u003e\",\"Divider\",\"\u003cFactory\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "User creates quote template \"\u003cSQuoteTemp\u003e\",\"\u003cPlacement\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Placement",
        "SQuoteTemp",
        "SRoutingTemp",
        "Description",
        "Factory"
      ]
    },
    {
      "cells": [
        "Shoulder - placement",
        "Supplier Quote - Template",
        "Supplier Routing - Template",
        "Automation",
        "Chennai Factory"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Template creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Sourcing"
    },
    {
      "name": "@NewTemplateCreation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates routing template \"Supplier Routing - Template\",\"Automation\",\"Divider\",\"Chennai Factory\"",
  "keyword": "When "
});
formatter.match({
  "location": "SourcingPageSteps.user_creates_routing_template(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates quote template \"Supplier Quote - Template\",\"Shoulder - placement\"",
  "keyword": "And "
});
formatter.match({
  "location": "SourcingPageSteps.user_creates_quote_template(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "SR Template creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SRTemplateCreation"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates data package template in specification \"DPT - Style\",\"DPT - Material\"",
  "keyword": "And "
});
formatter.step({
  "name": "User setup enumeration configurtion for Sample \"\u003cStyle\u003e\",\"\u003cMaterial\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User update configuration",
  "keyword": "And "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "User creates supplier request templates \"\u003cTemplateA\u003e\",\"\u003cTemplateB\u003e\",\"\u003cSupplier\u003e\",\"\u003cStyle\u003e\",\"\u003cMaterial\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Supplier",
        "Style",
        "Material",
        "TemplateA",
        "TemplateB"
      ]
    },
    {
      "cells": [
        "Supplier",
        "Style - Proto",
        "Material - Proto",
        "SRT - Style",
        "SRT - Material"
      ]
    }
  ]
});
formatter.scenario({
  "name": "SR Template creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Sourcing"
    },
    {
      "name": "@SRTemplateCreation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates data package template in specification \"DPT - Style\",\"DPT - Material\"",
  "keyword": "And "
});
formatter.match({
  "location": "SourcingPageSteps.user_creates_data_package_template_in_specification(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User setup enumeration configurtion for Sample \"Style - Proto\",\"Material - Proto\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_setup_enumeration_configurtion_for_Sample(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User update configuration",
  "keyword": "And "
});
formatter.match({
  "location": "SpecificationHierarchyPageSteps.user_update_configuration()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates supplier request templates \"SRT - Style\",\"SRT - Material\",\"Supplier\",\"Style - Proto\",\"Material - Proto\"",
  "keyword": "And "
});
formatter.match({
  "location": "SourcingPageSteps.user_creates_supplier_request_templates(String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Factory creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@FactoryCreation"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates factory \"\u003cFactoryA\u003e\",\"\u003cFactoryB\u003e\",\"\u003cPercentage\u003e\",\"\u003cmin-order\u003e\",\"\u003cre-order\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User validates factory reviews \"\u003cFactoryA\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User validates supplier reviews",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "FactoryA",
        "FactoryB",
        "Percentage",
        "min-order",
        "re-order"
      ]
    },
    {
      "cells": [
        "Chennai Factory",
        "Los Angels Factory",
        "20",
        "1000",
        "100"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Factory creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Sourcing"
    },
    {
      "name": "@FactoryCreation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates factory \"Chennai Factory\",\"Los Angels Factory\",\"20\",\"1000\",\"100\"",
  "keyword": "And "
});
formatter.match({
  "location": "SourcingPageSteps.user_creates_factory(String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates factory reviews \"Chennai Factory\"",
  "keyword": "And "
});
formatter.match({
  "location": "SourcingPageSteps.user_validates_factory_reviews(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates supplier reviews",
  "keyword": "And "
});
formatter.match({
  "location": "SourcingPageSteps.user_validates_supplier_reviews()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Customer creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@CustomerCreation"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates customer \"\u003cCustomerA\u003e\",\"\u003cCustomerB\u003e\",\"\u003cCustomerC\u003e\"",
  "keyword": "When "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "CustomerA",
        "CustomerB",
        "CustomerC"
      ]
    },
    {
      "cells": [
        "H\u0026M - India",
        "Target - China",
        "Zara - United States"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Customer creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Sourcing"
    },
    {
      "name": "@CustomerCreation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates customer \"H\u0026M - India\",\"Target - China\",\"Zara - United States\"",
  "keyword": "When "
});
formatter.match({
  "location": "SourcingPageSteps.user_creates_customer(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Shipment creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@ShipmentCreation"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates Shipment \"\u003cShipment\u003e\",\"\u003cPortFrom\u003e\",\"\u003cPortTo\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Shipment",
        "PortFrom",
        "PortTo"
      ]
    },
    {
      "cells": [
        "Fedex",
        "Mumbai Port",
        "SFS port"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Shipment creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Sourcing"
    },
    {
      "name": "@ShipmentCreation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates Shipment \"Fedex\",\"Mumbai Port\",\"SFS port\"",
  "keyword": "When "
});
formatter.match({
  "location": "SourcingPageSteps.user_creates_Shipment(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.logout_from_the_Application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/I_Quality.feature");
formatter.feature({
  "name": "Quality validation",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Quality"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Quality setup creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@QualitySetup"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates test spec under quality \"\u003cTestSpecA\u003e\",\"\u003cTestSpecB\u003e\",\"\u003cCode\u003e\",\"\u003cDescription\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "User creates test group \"\u003cTestSpecA\u003e\",\"\u003cTestSpecC\u003e\",\"\u003cCode\u003e\",\"\u003cDescription\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User creates test templates \"\u003cTestSpecA\u003e\",\"\u003cTestSpecB\u003e\",\"\u003cTestSpecC\u003e\",\"\u003cUser\u003e\",\"\u003cSupplier\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "TestSpecA",
        "TestSpecB",
        "TestSpecC",
        "Code",
        "Description",
        "User",
        "Supplier"
      ]
    },
    {
      "cells": [
        "Chemical Test",
        "Ironing Test",
        "Wash Test",
        "123 - Automation",
        "Automation",
        "Administrator",
        "Supplier"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Quality setup creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Quality"
    },
    {
      "name": "@QualitySetup"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates test spec under quality \"Chemical Test\",\"Ironing Test\",\"123 - Automation\",\"Automation\"",
  "keyword": "When "
});
formatter.match({
  "location": "QualityPageSteps.user_creates_test_spec_under_quality(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates test group \"Chemical Test\",\"Wash Test\",\"123 - Automation\",\"Automation\"",
  "keyword": "And "
});
formatter.match({
  "location": "QualityPageSteps.user_creates_test_group(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates test templates \"Chemical Test\",\"Ironing Test\",\"Wash Test\",\"Administrator\",\"Supplier\"",
  "keyword": "And "
});
formatter.match({
  "location": "QualityPageSteps.user_creates_test_templates(String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.logout_from_the_Application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/I_StyleUpdate.feature");
formatter.feature({
  "name": "Style Update validation",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@StyleSample"
    },
    {
      "name": "@Style"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Style ColorSpec creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@StyleColorSpec"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User update property table with \"\u003cCode\u003e\",\"\u003cDescription\u003e\",\"\u003cSizeRange\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "User creates colorway \"\u003cColorSpec\u003e\",\"\u003cColorwayA\u003e\",\"\u003cColorwayB\u003e\",\"\u003cDescription\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Code",
        "Description",
        "SizeRange",
        "ColorSpec",
        "ColorwayA",
        "ColorwayB"
      ]
    },
    {
      "cells": [
        "123",
        "Automation",
        "MensJeans",
        "GLASS GREEN",
        "Blue Color faded",
        "Green Color faded"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Style ColorSpec creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@StyleSample"
    },
    {
      "name": "@Style"
    },
    {
      "name": "@StyleColorSpec"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User update property table with \"123\",\"Automation\",\"MensJeans\"",
  "keyword": "When "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_update_property_table_with(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates colorway \"GLASS GREEN\",\"Blue Color faded\",\"Green Color faded\",\"Automation\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_creates_colorway(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Style Sample Creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@StyleSample"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates style sample combination \"\u003cStyleSample\u003e\",\"\u003cStyle\u003e\",\"\u003cDimensionA\u003e\",\"\u003cDimensionB\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "StyleSample",
        "DimensionA",
        "DimensionB",
        "Style"
      ]
    },
    {
      "cells": [
        "Jeans Sample - Style",
        "Colors",
        "Colors and Sizes",
        "Style - Proto"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Style Sample Creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@StyleSample"
    },
    {
      "name": "@Style"
    },
    {
      "name": "@StyleSample"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates style sample combination \"Jeans Sample - Style\",\"Style - Proto\",\"Colors\",\"Colors and Sizes\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_creates_style_sample_combination(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Style Quality Creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@StyleQuality"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User update quality types",
  "keyword": "When "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "User creates Style quality \"\u003cTestrunA\u003e\",\"\u003cTestRunB\u003e\",\"\u003cTestGrpA\u003e\",\"\u003cTestGrpB\u003e\",\"\u003cSampleA\u003e\",\"\u003cSampleB\u003e\",\"\u003cUser\u003e\",\"\u003cSupplier\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User Creates test run templates",
  "keyword": "And "
});
formatter.step({
  "name": "User approve test run \"\u003cCode\u003e\",\"\u003cDescription\u003e\",\"\u003cComment\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "TestrunA",
        "TestRunB",
        "TestGrpA",
        "TestGrpB",
        "SampleA",
        "SampleB",
        "User",
        "Supplier",
        "Code",
        "Description",
        "Comment"
      ]
    },
    {
      "cells": [
        "Acid Test - Style",
        "Wash Test - Style",
        "Chemical Test Group",
        "Wash Test Group",
        "small",
        "EGRET",
        "Administrator",
        "Supplier",
        "123",
        "Automation",
        "Test Run"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Style Quality Creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@StyleSample"
    },
    {
      "name": "@Style"
    },
    {
      "name": "@StyleQuality"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User update quality types",
  "keyword": "When "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_update_quality_types()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates Style quality \"Acid Test - Style\",\"Wash Test - Style\",\"Chemical Test Group\",\"Wash Test Group\",\"small\",\"EGRET\",\"Administrator\",\"Supplier\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_creates_Style_quality(String,String,String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Creates test run templates",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_Creates_test_run_templates()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User approve test run \"123\",\"Automation\",\"Test Run\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_approve_test_run(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Supplier Quote Creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@StyleSupplierQuote"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates Style sourcing price list",
  "keyword": "When "
});
formatter.step({
  "name": "User Creates Style Supplier Quotes \"\u003cSupplier\u003e\",\"\u003cSQ-Template\u003e\",\"\u003cSet\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User creates Style product blended cost \"\u003cProduct\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Supplier",
        "SQ-Template",
        "Set",
        "Product"
      ]
    },
    {
      "cells": [
        "Supplier",
        "Supplier Quote - Template",
        "SS - Supplier",
        "Product Blend Cost"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Supplier Quote Creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@StyleSample"
    },
    {
      "name": "@Style"
    },
    {
      "name": "@StyleSupplierQuote"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates Style sourcing price list",
  "keyword": "When "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_creates_Style_sourcing_price_list()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Creates Style Supplier Quotes \"Supplier\",\"Supplier Quote - Template\",\"SS - Supplier\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_Creates_Style_Supplier_Quotes(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates Style product blended cost \"Product Blend Cost\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_creates_Style_product_blended_cost(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Supplier Request Creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@StyleSupplierRequest"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates Supplier Requests \"\u003cRequestTemplate\u003e\",\"\u003cRequestName\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "User creates copy templates \"\u003cCopyTemp\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "User add new inspiration products \"\u003cInspA\u003e\",\"\u003cInspB\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "RequestTemplate",
        "RequestName",
        "InspA",
        "InspB",
        "CopyTemp"
      ]
    },
    {
      "cells": [
        "SRT - Style",
        "Apparel - SR",
        "Auto Inspiration",
        "New Inspiration",
        "Copy Template - Inspiration"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Supplier Request Creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@StyleSample"
    },
    {
      "name": "@Style"
    },
    {
      "name": "@StyleSupplierRequest"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates Supplier Requests \"SRT - Style\",\"Apparel - SR\"",
  "keyword": "When "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_creates_Supplier_Requests(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates copy templates \"Copy Template - Inspiration\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_creates_copy_templates(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User add new inspiration products \"Auto Inspiration\",\"New Inspiration\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_add_new_inspiration_products(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Supplier PO",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@StyleSupplierPO"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "Create PO Group and Supplier PO \"\u003cPoGroup\u003e\",\"\u003csupplierpovalue1\u003e\",\"\u003csupplierpovalue2\u003e\",\"\u003cquotevalueBlue\u003e\",\"\u003cquotevaluegreen\u003e\",\"\u003cEditValue\u003e\",\"\u003cdispoint\u003e\",\"\u003cdisvalue\u003e\",\"\u003callowancepoint\u003e\",\"\u003callowancevalue\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "user creates shipment qc and set \"\u003cunitvalueb1\u003e\",\"\u003cunitvalueg1\u003e\",\"\u003cbshipvalue\u003e\",\"\u003cqshipvalue\u003e\",\"\u003cqcissue\u003e\",\"\u003cselectset\u003e\",\"\u003cgshipqty\u003e\",\"\u003cbshipqty\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "PoGroup",
        "supplierpovalue1",
        "supplierpovalue2",
        "EditValue",
        "quotevalueBlue",
        "quotevaluegreen",
        "dispoint",
        "disvalue",
        "allowancepoint",
        "allowancevalue",
        "unitvalueb1",
        "unitvalueg1",
        "bshipvalue",
        "qshipvalue",
        "qcissue",
        "selectset",
        "gshipqty",
        "bshipqty"
      ]
    },
    {
      "cells": [
        "order po group",
        "PO-AP1321-Not color",
        "PO-AP1123-colorbased",
        "Automation",
        "blue color faded-large",
        "green color faded-large",
        "1",
        "10",
        "2",
        "20",
        "3",
        "5",
        "50",
        "100",
        "qc issue",
        "SS-Shipment",
        "60",
        "30"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Supplier PO",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@StyleSample"
    },
    {
      "name": "@Style"
    },
    {
      "name": "@StyleSupplierPO"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create PO Group and Supplier PO \"order po group\",\"PO-AP1321-Not color\",\"PO-AP1123-colorbased\",\"blue color faded-large\",\"green color faded-large\",\"Automation\",\"1\",\"10\",\"2\",\"20\"",
  "keyword": "When "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.create_PO_Group_and_Supplier_PO(String,String,String,String,String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates shipment qc and set \"3\",\"5\",\"50\",\"100\",\"qc issue\",\"SS-Shipment\",\"60\",\"30\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_creates_shipment_qc_and_set(String,String,String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Customer PO",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@StyleCustomerPO"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates customerpo and issue \"\u003cpovalue\u003e\",\"\u003cB_baseprice\u003e\",\"\u003cG_baseprice\u003e\",\"\u003cB_orderqty\u003e\",\"\u003cG_orderqty\u003e\",\"\u003cdispnt\u003e\",\"\u003cdisvalue\u003e\",\"\u003callowanpct\u003e\",\"\u003callowvalue\u003e\",\"\u003ccommission\u003e\",\"\u003cpayment\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "povalue",
        "B_baseprice",
        "G_baseprice",
        "B_orderqty",
        "G_orderqty",
        "dispnt",
        "disvalue",
        "allowanpct",
        "allowvalue",
        "commission",
        "payment"
      ]
    },
    {
      "cells": [
        "Target PO123#-color",
        "200",
        "350",
        "30",
        "40",
        "1",
        "100",
        "2",
        "50",
        "3",
        "cheque"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Customer PO",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@StyleSample"
    },
    {
      "name": "@Style"
    },
    {
      "name": "@StyleCustomerPO"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates customerpo and issue \"Target PO123#-color\",\"200\",\"350\",\"30\",\"40\",\"1\",\"100\",\"2\",\"50\",\"3\",\"cheque\"",
  "keyword": "Then "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_creates_customerpo_and_issue(String,String,String,String,String,String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Style Sample Update",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@StyleSampleUpdate"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user update delete style samples \"\u003cSample\u003e\",\"\u003cQuantity\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "User modify the sample names",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Sample",
        "Quantity"
      ]
    },
    {
      "cells": [
        "Sample",
        "5"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Style Sample Update",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@StyleSample"
    },
    {
      "name": "@Style"
    },
    {
      "name": "@StyleSampleUpdate"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user update delete style samples \"Sample\",\"5\"",
  "keyword": "When "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_update_delete_style_samples(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User modify the sample names",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_modify_the_sample_names()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Delete Supplier Quotes \u0026 Samples",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@DeleteStyleSupplierQuotes\u0026Samples"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user creates delete supplier quotes \"\u003cDelete\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "user creates supplier request \"\u003cTemplate\u003e\",\"\u003cRequest\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User Selects quotes and modifying states",
  "keyword": "And "
});
formatter.step({
  "name": "User select Supplier request samples",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Delete",
        "Template",
        "Request"
      ]
    },
    {
      "cells": [
        "Delete SQ",
        "SRT - Style",
        "SR - Delete Quotes \u0026 Samples - True"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Delete Supplier Quotes \u0026 Samples",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@StyleSample"
    },
    {
      "name": "@Style"
    },
    {
      "name": "@DeleteStyleSupplierQuotes\u0026Samples"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates delete supplier quotes \"Delete SQ\"",
  "keyword": "When "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_creates_delete_supplier_quotes(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates supplier request \"SRT - Style\",\"SR - Delete Quotes \u0026 Samples - True\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_creates_supplier_request(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Selects quotes and modifying states",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_Selects_quotes_and_modifying_states()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User select Supplier request samples",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_select_Supplier_request_samples()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenario({
  "name": "Validating quotes \u0026 samples",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@StyleSample"
    },
    {
      "name": "@Style"
    },
    {
      "name": "@ValidatingStyleQuotes\u0026Samples"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user removing supplier under supplier request setup",
  "keyword": "When "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_removing_supplier_under_supplier_request_setup()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user validating supplier request and samples displayed datas are correct or not",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_validating_supplier_request_and_samples_displayed_datas_are_correct_or_not()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user validating supplier quotes displayed datas are correct or not",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_validating_supplier_quotes_displayed_datas_are_correct_or_not()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validating style sample displayed datas are correct or not",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_validating_style_sample_displayed_datas_are_correct_or_not()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Copy Supplier Request",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@StyleCopyRequest"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user creating new inspiration \"\u003cInspiration\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "User copy supplier request \"\u003cSR\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Inspiration",
        "SR"
      ]
    },
    {
      "cells": [
        "Ins - 01",
        "Apparel SR - Inspiration"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Copy Supplier Request",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@StyleSample"
    },
    {
      "name": "@Style"
    },
    {
      "name": "@StyleCopyRequest"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creating new inspiration \"Ins - 01\"",
  "keyword": "When "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_creating_new_inspiration(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User copy supplier request \"Apparel SR - Inspiration\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_copy_supplier_request(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Issuing Supplier Request",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@IssuingStyleSupplierRequest"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User issuing apparel SR",
  "keyword": "When "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "user creates issue supplier request \"\u003cTemplate\u003e\",\"\u003cRequest\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Template",
        "Request"
      ]
    },
    {
      "cells": [
        "SRT - Style",
        "SR - with two products"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Issuing Supplier Request",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@StyleSample"
    },
    {
      "name": "@Style"
    },
    {
      "name": "@IssuingStyleSupplierRequest"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User issuing apparel SR",
  "keyword": "When "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_issuing_apparel_SR()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates issue supplier request \"SRT - Style\",\"SR - with two products\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_creates_issue_supplier_request(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Supplier Quotes Update",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@StyleSupplierQuote"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user enters designated supplier \"Supplier\" and samples details",
  "keyword": "When "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "User Creates supplier request template without style \"\u003cTemplate\u003e\",\"\u003cvalue\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "validating copy and delete action \"\u003cvalue\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User Creates supplier request template with style \"\u003cTemplate\u003e\",\"\u003cStylevalue\u003e\",\"\u003cStyle\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "user validating style template updated correctly or not",
  "keyword": "And "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "User issuing without style template",
  "keyword": "And "
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Template",
        "value",
        "Stylevalue",
        "Style"
      ]
    },
    {
      "cells": [
        "SRT - Style",
        "Style SR",
        "Style Home - Jeans",
        "Apparel - Color and Size"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Supplier Quotes Update",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@StyleSample"
    },
    {
      "name": "@Style"
    },
    {
      "name": "@StyleSupplierQuote"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user enters designated supplier \"Supplier\" and samples details",
  "keyword": "When "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_enters_designated_supplier_and_samples_details(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Creates supplier request template without style \"SRT - Style\",\"Style SR\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_Creates_supplier_request_template_without_style(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "validating copy and delete action \"Style SR\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.validating_copy_and_delete_action(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Creates supplier request template with style \"SRT - Style\",\"Style Home - Jeans\",\"Apparel - Color and Size\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_Creates_supplier_request_template_with_style(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user validating style template updated correctly or not",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_validating_style_template_updated_correctly_or_not()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User issuing without style template",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_issuing_without_style_template()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.logout_from_the_Application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/JJ_Inspection.feature");
formatter.feature({
  "name": "Inspection_creation",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Inspection"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Inspection Setup",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@InspectionSetCreation"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates Enumeration \"\u003cDefectsCategory\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "update configuration for inspection",
  "keyword": "And "
});
formatter.step({
  "name": "user creates Defects in Inspection \"\u003cDefects\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "user creates AccLimit in Inspection \"\u003cAccLimit\u003e\",\"\u003csortOrder\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "user creates TestSpecification and sampling in Inspection \"\u003cTestSpecification\u003e\",\"\u003cSamplingValue\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User creates ErrorType in Inspection \"\u003cErrorType\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "user creates TemplateName BomName sizechart and Dimension \"\u003cTemplateName\u003e\",\"\u003cBOMName\u003e\",\"\u003cSizeChart\u003e\",\"\u003cDimensioName\u003e\",\"\u003cTestSpecification\u003e\",\"\u003cSamplingValue\u003e\",\"\u003cErrorType\u003e\",\"\u003cInspectionGrpName\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Defects",
        "AccLimit",
        "sortOrder",
        "TestSpecification",
        "SamplingValue",
        "ErrorType",
        "TemplateName",
        "BOMName",
        "SizeChart",
        "DimensioName",
        "InspectionGrpName",
        "DefectsCategory"
      ]
    },
    {
      "cells": [
        "Size Defect,Sewing Defect,Fabric Defect,Color Defect",
        "2.5%AQL,4.5%AQL,ACL 3.0",
        "0,1,2",
        "No Link,BOM,Size Chart,Spec Data Sheet,Style Review",
        "Single SP,Multiple SP",
        "Critical,High,Check Error",
        "auto-inspection",
        "Inspection BOM",
        "Ins Special",
        "New Dimension",
        "New Inspection Group",
        "High,Medium,Low"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Inspection Setup",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Inspection"
    },
    {
      "name": "@InspectionSetCreation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates Enumeration \"High,Medium,Low\"",
  "keyword": "Then "
});
formatter.match({
  "location": "InspectionPageSteps.user_creates_Enumeration(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "update configuration for inspection",
  "keyword": "And "
});
formatter.match({
  "location": "InspectionPageSteps.update_configuration_for_inspection()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates Defects in Inspection \"Size Defect,Sewing Defect,Fabric Defect,Color Defect\"",
  "keyword": "Then "
});
formatter.match({
  "location": "InspectionPageSteps.user_creates_Defects_in_Inspection(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates AccLimit in Inspection \"2.5%AQL,4.5%AQL,ACL 3.0\",\"0,1,2\"",
  "keyword": "Then "
});
formatter.match({
  "location": "InspectionPageSteps.user_creates_AccLimit_in_Inspection(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates TestSpecification and sampling in Inspection \"No Link,BOM,Size Chart,Spec Data Sheet,Style Review\",\"Single SP,Multiple SP\"",
  "keyword": "Then "
});
formatter.match({
  "location": "InspectionPageSteps.user_creates_TestSpecification_and_sampling_in_Inspection(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates ErrorType in Inspection \"Critical,High,Check Error\"",
  "keyword": "Then "
});
formatter.match({
  "location": "InspectionPageSteps.user_creates_ErrorType_in_Inspection(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates TemplateName BomName sizechart and Dimension \"auto-inspection\",\"Inspection BOM\",\"Ins Special\",\"New Dimension\",\"No Link,BOM,Size Chart,Spec Data Sheet,Style Review\",\"Single SP,Multiple SP\",\"Critical,High,Check Error\",\"New Inspection Group\"",
  "keyword": "And "
});
formatter.match({
  "location": "InspectionPageSteps.user_creates_TemplateName_BomName_sizechart_and_Dimension(String,String,String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Inspection Groupname",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@InspectionGroupName"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user creates Inspection group name\"\u003cInspectionGrpName\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "InspectionGrpName"
      ]
    },
    {
      "cells": [
        "New Inspection Group"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Inspection Groupname",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Inspection"
    },
    {
      "name": "@InspectionGroupName"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates Inspection group name\"New Inspection Group\"",
  "keyword": "Then "
});
formatter.match({
  "location": "InspectionPageSteps.user_creates_Inspection_group_name(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Inspection style Specification creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@InspectionStyleCreation"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates Style for inspection \"\u003cBOMName\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User creates ImageDatasheet \"\u003cImageName\u003e\",\"\u003cDescription\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User creates Review \"\u003cReviewName\u003e\",\"\u003cReviewDesc\u003e\",\"\u003ccanvasNote\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User creates sizechart \"\u003cInsSize\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User creates spec \"\u003cspecvalue\u003e\",\"\u003cdatasheet\u003e\",\"\u003cspecdesc\u003e\",\"\u003ccustomspecvalue\u003e\",\"\u003cspecialvalue\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User Copy BOM Name \"\u003cBomcopy1\u003e\",\"\u003cBomcopy2\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User creates Inspection and verify the created items in Style Specification \"\u003cInspectionName\u003e\",\"\u003cSamplingvalue\u003e\",\"\u003cSupplierValue\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "BOMName",
        "ImageName",
        "Description",
        "ReviewName",
        "ReviewDesc",
        "canvasNote",
        "InsSize",
        "specvalue",
        "datasheet",
        "specdesc",
        "customspecvalue",
        "specialvalue",
        "Bomcopy1",
        "Bomcopy2",
        "InspectionName",
        "Samplingvalue",
        "SupplierValue"
      ]
    },
    {
      "cells": [
        "Ins-BOM",
        "INS-IDS",
        "Image Inspection Testing",
        "Ins-Style Review",
        "Style Inspection testing",
        "Automation Testing",
        "Ins-SizeChart",
        "character",
        "Ins-spec datasheet",
        "Inspection Testing",
        "Custom spec section",
        "Characteristic",
        "Ins-BOM COPY",
        "Ins-BOM COPY2",
        "Apparel-Inspection",
        "Single",
        "Supplier"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Inspection style Specification creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Inspection"
    },
    {
      "name": "@InspectionStyleCreation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates Style for inspection \"Ins-BOM\"",
  "keyword": "Then "
});
formatter.match({
  "location": "InspectionPageSteps.User_creates_Style_for_inspection(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates ImageDatasheet \"INS-IDS\",\"Image Inspection Testing\"",
  "keyword": "And "
});
formatter.match({
  "location": "InspectionPageSteps.User_creates_ImageDatasheet(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates Review \"Ins-Style Review\",\"Style Inspection testing\",\"Automation Testing\"",
  "keyword": "Then "
});
formatter.match({
  "location": "InspectionPageSteps.User_creates_Review(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates sizechart \"Ins-SizeChart\"",
  "keyword": "And "
});
formatter.match({
  "location": "InspectionPageSteps.User_creates_sizechart(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates spec \"character\",\"Ins-spec datasheet\",\"Inspection Testing\",\"Custom spec section\",\"Characteristic\"",
  "keyword": "Then "
});
formatter.match({
  "location": "InspectionPageSteps.User_creates_spec(String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Copy BOM Name \"Ins-BOM COPY\",\"Ins-BOM COPY2\"",
  "keyword": "And "
});
formatter.match({
  "location": "InspectionPageSteps.User_Copy_BOM_Name(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates Inspection and verify the created items in Style Specification \"Apparel-Inspection\",\"Single\",\"Supplier\"",
  "keyword": "Then "
});
formatter.match({
  "location": "InspectionPageSteps.User_creates_Inspection_and_verify_the_created_items_in_Style_Specification(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Inspection NewInspectionSecurityGroup creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@InspectionContinuation"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "create NewInspectionSecurityGroup \"\u003cErrorType\u003e\",\"\u003cAccLimit\u003e\",\"\u003cSampleQtyMethod\u003e\",\"\u003cInsBatchName\u003e\",\"\u003cAttribute\u003e\",\"\u003cBatchcolorway\u003e\",\"\u003cBatchSize\u003e\",\"\u003cDefectvalue\u003e\",\"\u003cDefectCounter\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User verify the PDF and InspectionBatch status",
  "keyword": "And "
});
formatter.step({
  "name": "User waits for PDF and verify it",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "ErrorType",
        "AccLimit",
        "SampleQtyMethod",
        "InsBatchName",
        "Attribute",
        "Batchcolorway",
        "BatchSize",
        "Defectvalue",
        "DefectCounter"
      ]
    },
    {
      "cells": [
        "High,Critical",
        "2.5,4.5",
        "Static,Percentage",
        "Size chart batch",
        "Inspection Size Chart Dimension,Size Chart Dimension",
        "Vanilla",
        "small",
        "size",
        "5"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Inspection NewInspectionSecurityGroup creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Inspection"
    },
    {
      "name": "@InspectionContinuation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "create NewInspectionSecurityGroup \"High,Critical\",\"2.5,4.5\",\"Static,Percentage\",\"Size chart batch\",\"Inspection Size Chart Dimension,Size Chart Dimension\",\"Vanilla\",\"small\",\"size\",\"5\"",
  "keyword": "Then "
});
formatter.match({
  "location": "InspectionPageSteps.create_NewInspectionSecurityGroup(String,String,String,String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User verify the PDF and InspectionBatch status",
  "keyword": "And "
});
formatter.match({
  "location": "InspectionPageSteps.user_verify_the_PDF_and_InspectionBatch_status()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User waits for PDF and verify it",
  "keyword": "Then "
});
formatter.match({
  "location": "InspectionPageSteps.user_waits_for_PDF_and_verify_it()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Inspection Shipment Style Order creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@InspectionShipment"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User Creates Style Inspection \"\u003cProductionQuote\u003e\",\"\u003cstyleInspValue\u003e\",\"\u003cTemplateValue\u003e\",\"\u003cErrorType\u003e\",\"\u003cDefectValue\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User creates Shipment Inspection \"\u003cShipmentInspValue\u003e\",\"\u003cTemplateValue\u003e\",\"\u003cBatchShipment\u003e\",\"\u003cLevel\u003e\",\"\u003cDefectValue\u003e\",\"\u003cErrorType\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User Creates Order Inspection \"\u003cErrorType\u003e\",\"\u003cTemplateValue\u003e\",\"\u003cLevel\u003e\",\"\u003cOrderInspection\u003e\",\"\u003cDefectCounter\u003e\",\"\u003cDefectValue\u003e\",\"\u003cBatchSize\u003e\",\"\u003cOrderBatchName\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User Create Tempalte under Inspection \"\u003cTemplateName\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "verify Template has created or not \"\u003cTemplateName\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "ErrorType",
        "ProductionQuote",
        "styleInspValue",
        "TemplateValue",
        "ShipmentInspValue",
        "Level",
        "BatchShipment",
        "OrderInspection",
        "TemplateName",
        "DefectCounter",
        "DefectValue",
        "BatchSize",
        "OrderBatchName"
      ]
    },
    {
      "cells": [
        "High,Critical",
        "Supplier",
        "Style-Inspection",
        "inspection",
        "Shipment-Inspection",
        "Shipment,Order",
        "Batch-shipment",
        "Order-Inspection",
        "order-inspection template",
        "1,2",
        "fabric,color",
        "small",
        "Batch for Order"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Inspection Shipment Style Order creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Inspection"
    },
    {
      "name": "@InspectionShipment"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Creates Style Inspection \"Supplier\",\"Style-Inspection\",\"inspection\",\"High,Critical\",\"fabric,color\"",
  "keyword": "And "
});
formatter.match({
  "location": "InspectionPageSteps.user_Creates_Style_Inspection(String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates Shipment Inspection \"Shipment-Inspection\",\"inspection\",\"Batch-shipment\",\"Shipment,Order\",\"fabric,color\",\"High,Critical\"",
  "keyword": "Then "
});
formatter.match({
  "location": "InspectionPageSteps.user_creates_Shipment_Inspection(String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Creates Order Inspection \"High,Critical\",\"inspection\",\"Shipment,Order\",\"Order-Inspection\",\"1,2\",\"fabric,color\",\"small\",\"Batch for Order\"",
  "keyword": "And "
});
formatter.match({
  "location": "InspectionPageSteps.user_Creates_Order_Inspection(String,String,String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Create Tempalte under Inspection \"order-inspection template\"",
  "keyword": "Then "
});
formatter.match({
  "location": "InspectionPageSteps.user_Create_Tempalte_under_Inspection(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify Template has created or not \"order-inspection template\"",
  "keyword": "And "
});
formatter.match({
  "location": "InspectionPageSteps.verify_Template_has_created_or_not(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/J_MaterialUpdate.feature");
formatter.feature({
  "name": "Material Update validation",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@MaterialSample"
    },
    {
      "name": "@Material"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Material ColorSpec creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@MaterialColorSpecification"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User update material property table with \"\u003cSizeRange\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "User creates material colorway \"\u003cColorwayA\u003e\",\"\u003cDescription\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "SizeRange",
        "Description",
        "ColorwayA"
      ]
    },
    {
      "cells": [
        "MensJeans",
        "Automation",
        "Green CM - 01"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Material ColorSpec creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@MaterialSample"
    },
    {
      "name": "@Material"
    },
    {
      "name": "@MaterialColorSpecification"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User update material property table with \"MensJeans\"",
  "keyword": "When "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_update_material_property_table_with(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates material colorway \"Green CM - 01\",\"Automation\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_creates_material_colorway(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Material Sample Creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@MaterialSampleCreation"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates material sample combination \"\u003cMaterialSample\u003e\",\"\u003cMaterial\u003e\",\"\u003cDimensionA\u003e\",\"\u003cDimensionB\u003e\",\"\u003cCode\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Code",
        "Material",
        "MaterialSample",
        "DimensionA",
        "DimensionB"
      ]
    },
    {
      "cells": [
        "123",
        "Material - Proto",
        "Jeans Sample - Material",
        "Colors",
        "Sample per active color"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Material Sample Creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@MaterialSample"
    },
    {
      "name": "@Material"
    },
    {
      "name": "@MaterialSampleCreation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates material sample combination \"Jeans Sample - Material\",\"Material - Proto\",\"Colors\",\"Sample per active color\",\"123\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_creates_material_sample_combination(String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Material Quality Creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@MaterialQuality"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates Material quality \"\u003cTestrunA\u003e\",\"\u003cTestRunB\u003e\",\"\u003cTestGrpA\u003e\",\"\u003cTestGrpB\u003e\",\"\u003cSampleA\u003e\",\"\u003cSampleB\u003e\",\"\u003cUser\u003e\",\"\u003cSupplier\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User approve material test run",
  "keyword": "And "
});
formatter.step({
  "name": "User Creates material test run templates",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "TestrunA",
        "TestRunB",
        "TestGrpA",
        "TestGrpB",
        "SampleA",
        "SampleB",
        "User",
        "Supplier"
      ]
    },
    {
      "cells": [
        "Acid Test - Material",
        "Wash Test - Material",
        "Chemical Test Group",
        "Wash Test Group",
        "Green",
        "EGRET",
        "Administrator",
        "Supplier"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Material Quality Creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@MaterialSample"
    },
    {
      "name": "@Material"
    },
    {
      "name": "@MaterialQuality"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates Material quality \"Acid Test - Material\",\"Wash Test - Material\",\"Chemical Test Group\",\"Wash Test Group\",\"Green\",\"EGRET\",\"Administrator\",\"Supplier\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_creates_Material_quality(String,String,String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User approve material test run",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_approve_material_test_run()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Creates material test run templates",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_Creates_material_test_run_templates()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Material Supplier Quotes \u0026 Samples",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@MaterialSupplierQuotes\u0026Samples"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user creates material supplier quotes",
  "keyword": "When "
});
formatter.step({
  "name": "user creates material supplier request \"\u003cTemplate\u003e\",\"\u003cRequest\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user removing supplier under supplier request setup",
  "keyword": "When "
});
formatter.step({
  "name": "user validating supplier quotes \u0026 samples",
  "keyword": "And "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "user validating material supplier quotes",
  "keyword": "And "
});
formatter.step({
  "name": "user issuing material supplier request",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Template",
        "Request"
      ]
    },
    {
      "cells": [
        "SRT - Material",
        "SR - Material Cotton"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Material Supplier Quotes \u0026 Samples",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@MaterialSample"
    },
    {
      "name": "@Material"
    },
    {
      "name": "@MaterialSupplierQuotes\u0026Samples"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates material supplier quotes",
  "keyword": "When "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_creates_material_supplier_quotes()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates material supplier request \"SRT - Material\",\"SR - Material Cotton\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_creates_material_supplier_request(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user removing supplier under supplier request setup",
  "keyword": "When "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_removing_supplier_under_supplier_request_setup()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user validating supplier quotes \u0026 samples",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_removing_supplier_under_material_request_setup()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user validating material supplier quotes",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_validating_material_supplier_quotes()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user issuing material supplier request",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_issuing_material_supplier_request()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Supplier Quotes Update",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@MaterialSupplierQuotes"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user enters designated supplier \"Supplier\" and template details",
  "keyword": "When "
});
formatter.step({
  "name": "User creates material product blended cost \"\u003cProduct\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Product"
      ]
    },
    {
      "cells": [
        "Material - PBC"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Supplier Quotes Update",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@MaterialSample"
    },
    {
      "name": "@Material"
    },
    {
      "name": "@MaterialSupplierQuotes"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user enters designated supplier \"Supplier\" and template details",
  "keyword": "When "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_enters_designated_supplier_and_template_details(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates material product blended cost \"Material - PBC\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleMaterialSamplePageSteps.user_creates_material_product_blended_cost(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.logout_from_the_Application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/KA_ShapeCreation.feature");
formatter.feature({
  "name": "Shape creation and validation",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Shape"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Shape creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@ShapeCreation"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates product group in size chart \"\u003cProductGrp\u003e\",\"\u003cDescription\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "User update dimension,increments \"\u003cIncrements\u003e\" and tolerance",
  "keyword": "And "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "User creates shape \"Circle Shape\",\"winter season\" and update shape master \"Object Shape Master\"",
  "keyword": "And "
});
formatter.step({
  "name": "User is performing copy,edit and delete action",
  "keyword": "And "
});
formatter.step({
  "name": "User creates Security group \"\u003cSecurityGrp\u003e\" and update values under shape \"MensJeans\"",
  "keyword": "And "
});
formatter.step({
  "name": "User creates shape sample \"\u003cSample\u003e\",\"Supplier\"",
  "keyword": "And "
});
formatter.step({
  "name": "User is performing sample copy,edit and delete action",
  "keyword": "And "
});
formatter.step({
  "name": "User Creates shape size chart for \"Circle SC-Lock All\",\"Circle SC-Lock Dimension\",\"Circle SC-Lock Tolerance\",\"Circle SC-Lock Values\"",
  "keyword": "And "
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "ProductGrp",
        "Description",
        "Increments",
        "SecurityGrp",
        "Sample"
      ]
    },
    {
      "cells": [
        "PG - 01",
        "Automation",
        "Pants Grade Rule",
        "Security Group - Shape",
        "Circle Shape Sample"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Shape creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Shape"
    },
    {
      "name": "@ShapeCreation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates product group in size chart \"PG - 01\",\"Automation\"",
  "keyword": "When "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_creates_product_group_in_size_chart(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User update dimension,increments \"Pants Grade Rule\" and tolerance",
  "keyword": "And "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_update_dimension_increments_and_tolerance(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates shape \"Circle Shape\",\"winter season\" and update shape master \"Object Shape Master\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_creates_shape_and_update_shape_master(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User is performing copy,edit and delete action",
  "keyword": "And "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_is_performing_copy_edit_and_delete_action()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates Security group \"Security Group - Shape\" and update values under shape \"MensJeans\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_creates_Security_group_and_update_values_under_shape(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates shape sample \"Circle Shape Sample\",\"Supplier\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_creates_shape_sample(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User is performing sample copy,edit and delete action",
  "keyword": "And "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_is_performing_sample_copy_edit_and_delete_action()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Creates shape size chart for \"Circle SC-Lock All\",\"Circle SC-Lock Dimension\",\"Circle SC-Lock Tolerance\",\"Circle SC-Lock Values\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_Creates_shape_size_chart_for(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.logout_from_the_Application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/KB_BomCreation.feature");
formatter.feature({
  "name": "BOMCreation",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@BOMCreation"
    },
    {
      "name": "@BOM"
    }
  ]
});
formatter.scenarioOutline({
  "name": "BOM Creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@BomCreate"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user creates NewBOMSection \"\u003cBomstylesection\u003e\",\"\u003cStylesortvalue\u003e\",\"\u003cfilterName\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "user Creates NewMaterialBomSection \"\u003cBomMaterialSection\u003e\",\"\u003cMaterialsortvalue\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Bomstylesection",
        "Stylesortvalue",
        "filterName",
        "BomMaterialSection",
        "Materialsortvalue"
      ]
    },
    {
      "cells": [
        "Apparel section,fabric section,All section,Delete section",
        "10,12,100,50,15",
        "Material",
        "fabric section,All section,Delete section",
        "23,24,25"
      ]
    }
  ]
});
formatter.scenario({
  "name": "BOM Creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@BOMCreation"
    },
    {
      "name": "@BOM"
    },
    {
      "name": "@BomCreate"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates NewBOMSection \"Apparel section,fabric section,All section,Delete section\",\"10,12,100,50,15\",\"Material\"",
  "keyword": "Then "
});
formatter.match({
  "location": "BOMCreationSteps.user_creates_NewBOMSection(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user Creates NewMaterialBomSection \"fabric section,All section,Delete section\",\"23,24,25\"",
  "keyword": "Then "
});
formatter.match({
  "location": "BOMCreationSteps.user_Creates_NewMaterialBomSection(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "BOM Creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@BomTemplate"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user created NewBOMTemplate \"\u003cSubtype\u003e\",\"\u003cTemplateName\u003e\",\"\u003ccreatesection\u003e\",\"\u003cEditComment\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Subtype",
        "TemplateName",
        "createsection",
        "EditComment"
      ]
    },
    {
      "cells": [
        "Apparel BOM",
        "Apparel BOM Template",
        "auto adhoc section,Delete section",
        "automation test"
      ]
    }
  ]
});
formatter.scenario({
  "name": "BOM Creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@BOMCreation"
    },
    {
      "name": "@BOM"
    },
    {
      "name": "@BomTemplate"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user created NewBOMTemplate \"Apparel BOM\",\"Apparel BOM Template\",\"auto adhoc section,Delete section\",\"automation test\"",
  "keyword": "Then "
});
formatter.match({
  "location": "BOMCreationSteps.user_created_NewBOMTemplate(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.logout_from_the_Application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/KB_ThemeCreation.feature");
formatter.feature({
  "name": "Theme creation and validation",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Theme"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Theme creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@ThemeCreation"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates theme for single season \"TM-Single Season\",\"Wild Theme Master\",\"Wild Theme\",\"winter season\"",
  "keyword": "When "
});
formatter.step({
  "name": "User creates new theme from style \"\u003cStyleSubType\u003e\",\"\u003cStyleTM\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User creates new theme from material \"\u003cMaterialSubType\u003e\",\"\u003cMaterialTM\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User creates theme for All season \"TM-All Season\",\"All season TM\",\"All season - Theme\"",
  "keyword": "And "
});
formatter.step({
  "name": "User is performing theme copy,edit and delete action",
  "keyword": "And "
});
formatter.step({
  "name": "User creates Security group \"\u003cSecurityGrp\u003e\" and update values under Theme \"TSG\"",
  "keyword": "And "
});
formatter.step({
  "name": "User creates color inside theme \"Blue Theme\",\"White Theme\",\"Glass Theme\"",
  "keyword": "And "
});
formatter.step({
  "name": "User creates product alternative \"Italy\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "StyleSubType",
        "StyleTM",
        "MaterialSubType",
        "MaterialTM",
        "SecurityGrp"
      ]
    },
    {
      "cells": [
        "TM-Allow material variation",
        "Style Theme Master",
        "TM-Multiple Season",
        "Material Theme Master",
        "TSG -All"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Theme creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Theme"
    },
    {
      "name": "@ThemeCreation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates theme for single season \"TM-Single Season\",\"Wild Theme Master\",\"Wild Theme\",\"winter season\"",
  "keyword": "When "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_creates_theme_for_single_season(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates new theme from style \"TM-Allow material variation\",\"Style Theme Master\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_creates_new_theme_from_style(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates new theme from material \"TM-Multiple Season\",\"Material Theme Master\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_creates_new_theme_from_material(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates theme for All season \"TM-All Season\",\"All season TM\",\"All season - Theme\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_creates_theme_for_All_season(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User is performing theme copy,edit and delete action",
  "keyword": "And "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_is_performing_theme_copy_edit_and_delete_action()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates Security group \"TSG -All\" and update values under Theme \"TSG\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_creates_Security_group_and_update_values_under_Theme(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates color inside theme \"Blue Theme\",\"White Theme\",\"Glass Theme\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_creates_color_inside_theme(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates product alternative \"Italy\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_creates_product_alternative(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Theme BOM creation and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@BOMTypeThemeCreation"
    }
  ]
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.step({
  "name": "User creates theme BOM type \"\u003cBOMSubType\u003e\",\"No Theme Lock\"",
  "keyword": "And "
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.step({
  "name": "User click on update cnfiguration",
  "keyword": "Then "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "User creates new theme placement \"\u003cPlacementA\u003e\",\"Theme BOM Value\"",
  "keyword": "When "
});
formatter.step({
  "name": "User creates new theme placement from material",
  "keyword": "When "
});
formatter.step({
  "name": "User creates new from theme placement",
  "keyword": "When "
});
formatter.step({
  "name": "User creates select section and update color value \"\u003cPlacementB\u003e\",\"\u003cColorA\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "User creates adhoc section \"\u003cAdhoc\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "User is performing select,create,arrange and delete section activities under sections",
  "keyword": "And "
});
formatter.step({
  "name": "User creates style BOM \"\u003cBOMSubType\u003e\",\"\u003cBOM_Value\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "User creates and update style BOM placements \"\u003cBOMSubType\u003e\",\"\u003cPlacementB\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Validating under style placements",
  "keyword": "And "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "user updates existing style BOM \"\u003cColorB\u003e\",\"\u003cBOMSubType\u003e\" and validate style placements",
  "keyword": "When "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "User performing delete style BOM placements and validate style",
  "keyword": "And "
});
formatter.step({
  "name": "User performing unlink and synch from theme validation",
  "keyword": "And "
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "BOMSubType",
        "PlacementA",
        "PlacementB",
        "ColorA",
        "ColorB",
        "BOM_Value",
        "Adhoc"
      ]
    },
    {
      "cells": [
        "Theme Lock",
        "Shoulder - placement",
        "Neck - placement",
        "Blue",
        "Red",
        "Apparel BOM - Theme Lock",
        "Theme Adhoc Section"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Theme BOM creation and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@Theme"
    },
    {
      "name": "@BOMTypeThemeCreation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.user_Click_on_setup_icon()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates theme BOM type \"Theme Lock\",\"No Theme Lock\"",
  "keyword": "And "
});
formatter.match({
  "location": "ConfigurationPageSteps.user_creates_theme_BOM_type(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.user_Click_on_setup_icon()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User click on update cnfiguration",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.user_click_on_update_cnfiguration()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates new theme placement \"Shoulder - placement\",\"Theme BOM Value\"",
  "keyword": "When "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_creates_new_theme_placement(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates new theme placement from material",
  "keyword": "When "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_creates_new_theme_placement_from_material()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates new from theme placement",
  "keyword": "When "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_creates_new_from_theme_placement()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates select section and update color value \"Neck - placement\",\"Blue\"",
  "keyword": "When "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_creates_select_section_and_update_color_value(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates adhoc section \"Theme Adhoc Section\"",
  "keyword": "When "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_creates_adhoc_section(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User is performing select,create,arrange and delete section activities under sections",
  "keyword": "And "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_is_performing_select_create_arrange_and_delete_section_activities_under_sections()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates style BOM \"Theme Lock\",\"Apparel BOM - Theme Lock\"",
  "keyword": "When "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_creates_style_BOM(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates and update style BOM placements \"Theme Lock\",\"Neck - placement\"",
  "keyword": "And "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_creates_and_update_style_BOM_placements(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Validating under style placements",
  "keyword": "And "
});
formatter.match({
  "location": "StyleandThemePageSteps.validating_under_style_placements()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user updates existing style BOM \"Red\",\"Theme Lock\" and validate style placements",
  "keyword": "When "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_updates_existing_style_BOM_and_validate_style_placements(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User performing delete style BOM placements and validate style",
  "keyword": "And "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_performing_delete_style_BOM_placements_and_validate_style()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User performing unlink and synch from theme validation",
  "keyword": "And "
});
formatter.match({
  "location": "StyleandThemePageSteps.user_performing_unlink_and_synch_from_theme_validation()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.logout_from_the_Application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/KC_ThemeColorRule.feature");
formatter.feature({
  "name": "ThemeColorRule  validation",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@ThemeColorRule"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Theme Color Rule Data Setup",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@ThemeColorRuleData"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to Theme – Color Matching type, Create Color Matching type with Delete option  \"\u003cColorMatchingType\u003e\",\"11-0103 EGRET\"",
  "keyword": "When "
});
formatter.step({
  "name": "Click style tab and get the listed season name in the style tab",
  "keyword": "And "
});
formatter.step({
  "name": "Create a New Season \"\u003cSeason\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Create three Material with Colored Material and Enable “Ok for Color Specification” – True and False \"\u003cMaterial\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Create New Theme value with above created Season \"\u003cSeason\u003e\",\"\u003cSubType\u003e\",\"Color Rule\"",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate into the Theme Master and Create Custom view in properties level",
  "keyword": "And "
});
formatter.step({
  "name": "Select the \"Contrast\" value in Color Matching type",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to above create Theme node value",
  "keyword": "And "
});
formatter.step({
  "name": "Add two Theme Colorway values \"Theme Blue\",\"Theme Egret\"",
  "keyword": "And "
});
formatter.step({
  "name": "Go \u0026 Check the Theme – Material tab \u003e Color Rule should be displayed after refreshing the page",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "ColorMatchingType",
        "Season",
        "SubType",
        "Material"
      ]
    },
    {
      "cells": [
        "Contrast,Tone-Tone,Dummy",
        "Theme Color Rule Season",
        "TM-Single Season",
        "Linen,Cotton,Polyster"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Theme Color Rule Data Setup",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@ThemeColorRule"
    },
    {
      "name": "@ThemeColorRuleData"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Theme – Color Matching type, Create Color Matching type with Delete option  \"Contrast,Tone-Tone,Dummy\",\"11-0103 EGRET\"",
  "keyword": "When "
});
formatter.match({
  "location": "ThemeColorRuleSteps.navigate_to_Theme_Color_Matching_type_Create_Color_Matching_type_with_Delete_option(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Click style tab and get the listed season name in the style tab",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.click_style_tab_and_get_the_listed_season_name_in_the_style_tab()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create a New Season \"Theme Color Rule Season\"",
  "keyword": "And "
});
formatter.match({
  "location": "ThemeColorRuleSteps.create_a_New_Season(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create three Material with Colored Material and Enable “Ok for Color Specification” – True and False \"Linen,Cotton,Polyster\"",
  "keyword": "And "
});
formatter.match({
  "location": "ThemeColorRuleSteps.create_three_Material_with_Colored_Material_and_Enable_Ok_for_Color_Specification_True_and_False(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create New Theme value with above created Season \"Theme Color Rule Season\",\"TM-Single Season\",\"Color Rule\"",
  "keyword": "And "
});
formatter.match({
  "location": "ThemeColorRuleSteps.create_New_Theme_value_with_above_created_Season(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate into the Theme Master and Create Custom view in properties level",
  "keyword": "And "
});
formatter.match({
  "location": "ThemeColorRuleSteps.navigate_into_the_Theme_Master_and_Create_Custom_view_in_properties_level()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Select the \"Contrast\" value in Color Matching type",
  "keyword": "And "
});
formatter.match({
  "location": "ThemeColorRuleSteps.select_the_value_in_Color_Matching_type(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to above create Theme node value",
  "keyword": "And "
});
formatter.match({
  "location": "ThemeColorRuleSteps.navigate_to_above_create_Theme_node_value()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Add two Theme Colorway values \"Theme Blue\",\"Theme Egret\"",
  "keyword": "And "
});
formatter.match({
  "location": "ThemeColorRuleSteps.add_two_Theme_Colorway_values(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go \u0026 Check the Theme – Material tab \u003e Color Rule should be displayed after refreshing the page",
  "keyword": "And "
});
formatter.match({
  "location": "ThemeColorRuleSteps.go_Check_the_Theme_Material_tab_Color_Rule_should_be_displayed_after_refreshing_the_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenario({
  "name": "Verify Generate Color Rule",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@ThemeColorRule"
    },
    {
      "name": "@VerifyGenerateColor"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Add above created two Material placements",
  "keyword": "When "
});
formatter.match({
  "location": "ThemeColorRuleSteps.add_above_created_two_Material_placements()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Set Main Material for both Material Placements",
  "keyword": "And "
});
formatter.match({
  "location": "ThemeColorRuleSteps.set_Main_Material_for_both_Material_Placements()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Set a Color Specification common value for 1 placement",
  "keyword": "And "
});
formatter.match({
  "location": "ThemeColorRuleSteps.set_a_Color_Specification_common_value_for_placement(Integer)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Set a Colored Material common value for 1 placement",
  "keyword": "And "
});
formatter.match({
  "location": "ThemeColorRuleSteps.set_a_Colored_Material_common_value_for_placement(Integer)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Now Click Generate Color Rule verify \u003e It will Generate two Theme Color rule – Check the Status message status with condition",
  "keyword": "Then "
});
formatter.match({
  "location": "ThemeColorRuleSteps.now_Click_Generate_Color_Rule_verify_It_will_Generate_two_Theme_Color_rule_Check_the_Status_message_status_with_condition()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Theme Placement Change the Colored Material value and Check again Generate Color Rule",
  "keyword": "And "
});
formatter.match({
  "location": "ThemeColorRuleSteps.navigate_to_Theme_Placement_Change_the_Colored_Material_value_and_Check_again_Generate_Color_Rule()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenario({
  "name": "Verify Apply Color Rules",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@ThemeColorRule"
    },
    {
      "name": "@VerifyApplyColor"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to same Theme used above scenario",
  "keyword": "When "
});
formatter.match({
  "location": "ThemeColorRuleSteps.navigate_to_same_Theme_used_above_scenario()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Change the Main Material Set to True for 1st placements and Set false for 2nd placements",
  "keyword": "And "
});
formatter.match({
  "location": "ThemeColorRuleSteps.change_the_Main_Material_Set_to_True_for_st_placements_and_Set_false_for_nd_placements(Integer,Integer)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Add Material Family from the Custom view in Theme – Placement table",
  "keyword": "And "
});
formatter.match({
  "location": "ThemeColorRuleSteps.add_Material_Family_from_the_Custom_view_in_Theme_Placement_table()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Set Colored Material common value for both Material Placements",
  "keyword": "And "
});
formatter.match({
  "location": "ThemeColorRuleSteps.set_Colored_Material_common_value_for_both_Material_Placements()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Set Theme Main Material Group value for both placements “Set – Group”",
  "keyword": "And "
});
formatter.match({
  "location": "ThemeColorRuleSteps.set_Theme_Main_Material_Group_value_for_both_placements_Set_Group()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Set Material Family for 2nd placements",
  "keyword": "And "
});
formatter.match({
  "location": "ThemeColorRuleSteps.set_Material_Family_for_nd_placements(Integer)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Click Action – Generate Color Rule",
  "keyword": "And "
});
formatter.match({
  "location": "ThemeColorRuleSteps.click_Action_Generate_Color_Rule()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Season – Theme – Color Rule, Click Action \u003e Apply Color for Theme value and set option is displayed window and click save \u003e Verify it is applied color rule theme correctly",
  "keyword": "And "
});
formatter.match({
  "location": "ThemeColorRuleSteps.navigate_to_Season_Theme_Color_Rule_Click_Action_Apply_Color_for_Theme_value_and_set_option_is_displayed_window_and_click_save_Verify_it_is_applied_color_rule_theme_correctly()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Theme – Placements tab \u003e\u003e Add “Applied Color Rule” in Custom view Color Matrix and check values are updated correctly",
  "keyword": "And "
});
formatter.match({
  "location": "ThemeColorRuleSteps.navigate_to_Theme_Placements_tab_Add_Applied_Color_Rule_in_Custom_view_Color_Matrix_and_check_values_are_updated_correctly()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Change the Color, Material Family Color Rule name, State in season level and verify it updated on theme level",
  "keyword": "And "
});
formatter.match({
  "location": "ThemeColorRuleSteps.change_the_Color_Material_Family_Color_Rule_name_State_in_season_level_and_verify_it_updated_on_theme_level()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenario({
  "name": "Verify Generate Color Rules from Availability",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@ThemeColorRule"
    },
    {
      "name": "@VerifyGenerateColorAvailability"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Delete Existing ColorRule in ThemeRuleSeason",
  "keyword": "Then "
});
formatter.match({
  "location": "ThemeColorRuleSteps.Delete_Existing_ColorRule_in_ThemeRuleSeason()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Approve Red Fmaily and Blule Family",
  "keyword": "And "
});
formatter.match({
  "location": "ThemeColorRuleSteps.Approve_Red_Fmaily_and_Blule_Family()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Material and Set Main Material \u003d True for 3 Materials i.e. created above scenario",
  "keyword": "Then "
});
formatter.match({
  "location": "ThemeColorRuleSteps.navigate_to_Material_and_Set_Main_Material_True_for_Materials_i_e_created_above_scenario(Integer)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Style – Season, Create a Season Color Rule – Availability",
  "keyword": "And "
});
formatter.match({
  "location": "ThemeColorRuleSteps.navigate_to_Style_Season_Create_a_Season_Color_Rule_Availability()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Season – Specification, Add 3 Materials",
  "keyword": "And "
});
formatter.match({
  "location": "ThemeColorRuleSteps.navigate_to_Season_Specification_Add_Materials(Integer)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to Season – Theme – Color Rule and Click Action - Generate Color rules from Availability",
  "keyword": "Then "
});
formatter.match({
  "location": "ThemeColorRuleSteps.go_to_Season_Theme_Color_Rule_and_Click_Action_Generate_Color_rules_from_Availability()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Verify Color Rule generate from availability",
  "keyword": "Then "
});
formatter.match({
  "location": "ThemeColorRuleSteps.verify_Color_Rule_generate_from_availability()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.logout_from_the_Application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/L_CollectionManagement.feature");
formatter.feature({
  "name": "Creation of Collection Management",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@CollectionManagement"
    }
  ]
});
formatter.scenarioOutline({
  "name": "User creates enumeration and currency",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@EnumerationandCurrency"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user creates enumeration \"Segment - 01\",\"Segment - 02\"",
  "keyword": "When "
});
formatter.step({
  "name": "user run update configuration",
  "keyword": "And "
});
formatter.step({
  "name": "user creates currencies in general setup \"\u003cCurrencyA\u003e\",\"\u003cCurrencyB\u003e\",\"\u003cCurrencyC\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "user performing delete action",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "CurrencyA",
        "CurrencyB",
        "CurrencyC"
      ]
    },
    {
      "cells": [
        "Rupee,R",
        "Dollar,$",
        "Euro,E"
      ]
    }
  ]
});
formatter.scenario({
  "name": "User creates enumeration and currency",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@CollectionManagement"
    },
    {
      "name": "@EnumerationandCurrency"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates enumeration \"Segment - 01\",\"Segment - 02\"",
  "keyword": "When "
});
formatter.match({
  "location": "CollectionMangSteps.user_creates_enumeration(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user run update configuration",
  "keyword": "And "
});
formatter.match({
  "location": "CollectionMangSteps.user_run_update_configuration()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates currencies in general setup \"Rupee,R\",\"Dollar,$\",\"Euro,E\"",
  "keyword": "When "
});
formatter.match({
  "location": "CollectionMangSteps.user_creates_currencies_in_general_setup(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user performing delete action",
  "keyword": "And "
});
formatter.match({
  "location": "CollectionMangSteps.user_performing_delete_action()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenario({
  "name": "User creates curreny table",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@CollectionManagement"
    },
    {
      "name": "@CurrencyTable"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "user creates currency table in general setup \"CT - 01\"",
  "keyword": "When "
});
formatter.match({
  "location": "CollectionMangSteps.user_creates_currency_table_in_general_setup(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user updating currency exchange rates",
  "keyword": "And "
});
formatter.match({
  "location": "CollectionMangSteps.user_updating_currency_exchange_rates()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "User creates sales market",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@CreateSaslesMarket"
    }
  ]
});
formatter.step({
  "name": "user creates Sales market in general setup \"\u003cSalesMarketA\u003e\",\"\u003cSalesMarketB\u003e\"",
  "keyword": "When "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "SalesMarketA",
        "SalesMarketB"
      ]
    },
    {
      "cells": [
        "Indian Market,Rupee",
        "US Market,Dollar"
      ]
    }
  ]
});
formatter.scenario({
  "name": "User creates sales market",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@CollectionManagement"
    },
    {
      "name": "@CreateSaslesMarket"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "user creates Sales market in general setup \"Indian Market,Rupee\",\"US Market,Dollar\"",
  "keyword": "When "
});
formatter.match({
  "location": "CollectionMangSteps.user_creates_Sales_market_in_general_setup(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "User creates sales division",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@salesDivision"
    }
  ]
});
formatter.step({
  "name": "user creates Sales division in general setup \"\u003cSalesDivA\u003e\",\"\u003cSalesDivB\u003e\",\"\u003cSalesDivC\u003e\",\"\u003cSalesDivD\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "user updating contact inside sales division",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "SalesDivA",
        "SalesDivB",
        "SalesDivC",
        "SalesDivD"
      ]
    },
    {
      "cells": [
        "US - North",
        "US - South",
        "India - South",
        "India - North"
      ]
    }
  ]
});
formatter.scenario({
  "name": "User creates sales division",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@CollectionManagement"
    },
    {
      "name": "@salesDivision"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "user creates Sales division in general setup \"US - North\",\"US - South\",\"India - South\",\"India - North\"",
  "keyword": "When "
});
formatter.match({
  "location": "CollectionMangSteps.user_creates_Sales_division_in_general_setup(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user updating contact inside sales division",
  "keyword": "And "
});
formatter.match({
  "location": "CollectionMangSteps.user_updating_contact_inside_sales_division()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "User creates collection management",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@CollectionManagementWithoutSKU"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user creates marketing looks \"Avengers\"",
  "keyword": "When "
});
formatter.step({
  "name": "user creates marketing tools \"\u003cToolA\u003e\",\"\u003cToolB\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "performing delete action",
  "keyword": "And "
});
formatter.step({
  "name": "user creates collection management \"\u003cCollectionValue\u003e\",\"\u003cCurrency\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user creates style marketing products \"TShirts Style\",\"123 Avengers\",\"smoke\"",
  "keyword": "And "
});
formatter.step({
  "name": "user creates new marketing product \"MP - TShirt\",\"123 - Marketing\",\"Apparel - Color and Size\"",
  "keyword": "And "
});
formatter.step({
  "name": "user creates colorway inside marketing product \"\u003cColorA\u003e\",\"\u003cColorB\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user creates marketing material and looks \"\u003cLookA\u003e\",\"\u003cLookB\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user update material value in material product and segments",
  "keyword": "And "
});
formatter.step({
  "name": "user click on release to markets and release to customers",
  "keyword": "Then "
});
formatter.step({
  "name": "user creates sales market in collection management",
  "keyword": "And "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "creates sales order \"TShirt Sales Order\",\"Indian Market\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "ToolA",
        "ToolB",
        "CollectionValue",
        "Currency",
        "ColorA",
        "ColorB",
        "LookA",
        "LookA"
      ]
    },
    {
      "cells": [
        "Banner",
        "Canoply",
        "TShirts Collection - without SKU",
        "Dollar",
        "Blue",
        "Green",
        "Spider Man",
        "Iron Man"
      ]
    }
  ]
});
formatter.scenario({
  "name": "User creates collection management",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@CollectionManagement"
    },
    {
      "name": "@CollectionManagementWithoutSKU"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates marketing looks \"Avengers\"",
  "keyword": "When "
});
formatter.match({
  "location": "CollectionMangSteps.user_creates_marketing_looks(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates marketing tools \"Banner\",\"Canoply\"",
  "keyword": "And "
});
formatter.match({
  "location": "CollectionMangSteps.user_creates_marketing_tools(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "performing delete action",
  "keyword": "And "
});
formatter.match({
  "location": "CollectionMangSteps.performing_delete_action()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates collection management \"TShirts Collection - without SKU\",\"Dollar\"",
  "keyword": "And "
});
formatter.match({
  "location": "CollectionMangSteps.user_creates_collection_management(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates style marketing products \"TShirts Style\",\"123 Avengers\",\"smoke\"",
  "keyword": "And "
});
formatter.match({
  "location": "CollectionMangSteps.user_creates_style_marketing_products(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates new marketing product \"MP - TShirt\",\"123 - Marketing\",\"Apparel - Color and Size\"",
  "keyword": "And "
});
formatter.match({
  "location": "CollectionMangSteps.user_creates_new_marketing_product(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates colorway inside marketing product \"Blue\",\"Green\"",
  "keyword": "And "
});
formatter.match({
  "location": "CollectionMangSteps.user_creates_colorway_inside_marketing_product(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates marketing material and looks \"Spider Man\",\"\u003cLookB\u003e\"",
  "keyword": "And "
});
formatter.match({
  "location": "CollectionMangSteps.user_creates_marketing_material_and_looks(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user update material value in material product and segments",
  "keyword": "And "
});
formatter.match({
  "location": "CollectionMangSteps.user_update_material_value_in_material_product_and_segments()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user click on release to markets and release to customers",
  "keyword": "Then "
});
formatter.match({
  "location": "CollectionMangSteps.user_click_on_release_to_markets_and_release_to_customers()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates sales market in collection management",
  "keyword": "And "
});
formatter.match({
  "location": "CollectionMangSteps.user_creates_sales_market_in_collection_management()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "creates sales order \"TShirt Sales Order\",\"Indian Market\"",
  "keyword": "And "
});
formatter.match({
  "location": "CollectionMangSteps.creates_sales_order(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "User creates collection management with SKU",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@CollectionManagementWithSKU"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user creates collection management with SKU \"\u003cCollectionValue\u003e\",\"\u003cCurrency\u003e\",\"123 Automation\",\"CT - 01\",\"Stripped Jeans - SKU\"",
  "keyword": "When "
});
formatter.step({
  "name": "user creates styles marketing products \"Apparel - Jeans\",\"123 Automation\",\"smoke\"",
  "keyword": "And "
});
formatter.step({
  "name": "verify material SKU values",
  "keyword": "And "
});
formatter.step({
  "name": "user creates marketing material and update values in marketing product \"\u003cMaterialA\u003e\",\"\u003cMaterialB\u003e\",\"\u003cColor\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user creates SKU sales market in collection management \"\u003cCustomer\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User creates promotion and sales order \"New Promo\",\"Indian Sales Order\",\"India - North\",\"\u003cCustomer\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user creates marketing tools and update sales order",
  "keyword": "And "
});
formatter.step({
  "name": "user creates sales order group and update catalog configuration",
  "keyword": "And "
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "CollectionValue",
        "Currency",
        "MaterialA",
        "MaterialB",
        "Color",
        "Customer"
      ]
    },
    {
      "cells": [
        "Jeans",
        "Rupee",
        "100% Cotton/Rayon Jersey",
        "100% Cotton/Rayon Jersey - Copy",
        "Blue",
        "H\u0026M"
      ]
    }
  ]
});
formatter.scenario({
  "name": "User creates collection management with SKU",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@CollectionManagement"
    },
    {
      "name": "@CollectionManagementWithSKU"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates collection management with SKU \"Jeans\",\"Rupee\",\"123 Automation\",\"CT - 01\",\"Stripped Jeans - SKU\"",
  "keyword": "When "
});
formatter.match({
  "location": "CollectionMangSteps.user_creates_collection_management_with_SKU(String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates styles marketing products \"Apparel - Jeans\",\"123 Automation\",\"smoke\"",
  "keyword": "And "
});
formatter.match({
  "location": "CollectionMangSteps.user_creates_styles_marketing_products(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify material SKU values",
  "keyword": "And "
});
formatter.match({
  "location": "CollectionMangSteps.verify_material_SKU_values()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates marketing material and update values in marketing product \"100% Cotton/Rayon Jersey\",\"100% Cotton/Rayon Jersey - Copy\",\"Blue\"",
  "keyword": "And "
});
formatter.match({
  "location": "CollectionMangSteps.user_creates_marketing_material_and_update_values_in_marketing_product(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates SKU sales market in collection management \"H\u0026M\"",
  "keyword": "And "
});
formatter.match({
  "location": "CollectionMangSteps.user_creates_SKU_sales_market_in_collection_management(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates promotion and sales order \"New Promo\",\"Indian Sales Order\",\"India - North\",\"H\u0026M\"",
  "keyword": "And "
});
formatter.match({
  "location": "CollectionMangSteps.user_creates_promotion_and_sales_order(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates marketing tools and update sales order",
  "keyword": "And "
});
formatter.match({
  "location": "CollectionMangSteps.user_creates_marketing_tools_and_update_sales_order()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates sales order group and update catalog configuration",
  "keyword": "And "
});
formatter.match({
  "location": "CollectionMangSteps.user_creates_sales_order_group_and_update_catalog_configuration()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.logout_from_the_Application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/M_BusinessPlanning.feature");
formatter.feature({
  "name": "Creation of Business Planning",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@BusinessPlanning"
    }
  ]
});
formatter.scenarioOutline({
  "name": "User creates BusinessCategory for  Style and market",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates Business category for Style  \"\u003cstylecategory\u003e\",\"\u003cstyle\u003e\",\"\u003ccurrency\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User creates Business category for Material \"\u003cmaterialcategory\u003e\",\"\u003cmaterial\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User Edit and copy Business category \"\u003ccopymaterial\u003e\",\"\u003ceditmaterial\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "create BusinessMarket for Style and Material \"\u003cBusinessmarketvalue1\u003e\",\"\u003cBusinessCode1\u003e\",\"\u003cBusinessmarketvalue2\u003e\",\"\u003cBusinessCode2\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "user validating Business category custom view",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "stylecategory",
        "style",
        "currency",
        "materialcategory",
        "material",
        "copymaterial",
        "editmaterial",
        "Businessmarketvalue1",
        "BusinessCode1",
        "Businessmarketvalue2",
        "BusinessCode2",
        ""
      ]
    },
    {
      "cells": [
        "BC-Style Category",
        "style",
        "dollar",
        "BC-Material category",
        "Material",
        "BC-Material Category Copied",
        "BC-Material Category EditedUS Market",
        "US Market",
        "US Automation",
        "Indian Market",
        "IND Automation",
        ""
      ]
    }
  ]
});
formatter.scenario({
  "name": "User creates BusinessCategory for  Style and market",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@BusinessPlanning"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates Business category for Style  \"BC-Style Category\",\"style\",\"dollar\"",
  "keyword": "Then "
});
formatter.match({
  "location": "BusinessPlanningSteps.user_creates_Business_category_for_Style(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates Business category for Material \"BC-Material category\",\"Material\"",
  "keyword": "Then "
});
formatter.match({
  "location": "BusinessPlanningSteps.user_creates_Business_category_for_Material(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Edit and copy Business category \"BC-Material Category Copied\",\"BC-Material Category EditedUS Market\"",
  "keyword": "And "
});
formatter.match({
  "location": "BusinessPlanningSteps.user_Edit_and_copy_Business_category(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "create BusinessMarket for Style and Material \"US Market\",\"US Automation\",\"Indian Market\",\"IND Automation\"",
  "keyword": "Then "
});
formatter.match({
  "location": "BusinessPlanningSteps.create_BusinessMarket_for_Style_and_Material(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user validating Business category custom view",
  "keyword": "And "
});
formatter.match({
  "location": "BusinessPlanningSteps.user_validating_Business_category_custom_view()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "User creates BusinessPlan for  Style and market",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@BusinessPlan"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates Business Plan for Style \"\u003cBPValue1\u003e\",\"\u003cBPCategory1\u003e\",\"\u003ccurrency\u003e\",\"\u003csupplier\u003e\",\"\u003cseason\u003e\",\"\u003cBrand\u003e\",\"\u003cDpmt\u003e\",\"\u003cCollection\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "verify the datas in marketplan",
  "keyword": "And "
});
formatter.step({
  "name": "User creates Business Plan for Material \"\u003cBPValue2\u003e\",\"\u003cBPCategory2\u003e\",\"\u003ccurrency\u003e\",\"\u003csupplier\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "user verify custom view actions",
  "keyword": "And "
});
formatter.step({
  "name": "user validating volume and margin datas are editable",
  "keyword": "And "
});
formatter.step({
  "name": "User Verifying copy, edit and delete action",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "BPValue1",
        "BPCategory1",
        "currency",
        "supplier",
        "season",
        "Brand",
        "Dpmt",
        "Collection",
        "BPValue2",
        "BPCategory2"
      ]
    },
    {
      "cells": [
        "Plan for style",
        "BC-Style Category",
        "CT",
        "supplier",
        "winter",
        "Denim",
        "Mens",
        "Jeans",
        "Plan for material",
        "BC-Material Category"
      ]
    }
  ]
});
formatter.scenario({
  "name": "User creates BusinessPlan for  Style and market",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@BusinessPlanning"
    },
    {
      "name": "@BusinessPlan"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates Business Plan for Style \"Plan for style\",\"BC-Style Category\",\"CT\",\"supplier\",\"winter\",\"Denim\",\"Mens\",\"Jeans\"",
  "keyword": "Then "
});
formatter.match({
  "location": "BusinessPlanningSteps.user_creates_Business_Plan_for_Style(String,String,String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify the datas in marketplan",
  "keyword": "And "
});
formatter.match({
  "location": "BusinessPlanningSteps.verify_the_datas_in_marketplan()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates Business Plan for Material \"Plan for material\",\"BC-Material Category\",\"CT\",\"supplier\"",
  "keyword": "Then "
});
formatter.match({
  "location": "BusinessPlanningSteps.user_creates_Business_Plan_for_Material(String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user verify custom view actions",
  "keyword": "And "
});
formatter.match({
  "location": "BusinessPlanningSteps.user_verify_custom_view_actions()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user validating volume and margin datas are editable",
  "keyword": "And "
});
formatter.match({
  "location": "BusinessPlanningSteps.user_validating_volume_and_margin_datas_are_editable()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Verifying copy, edit and delete action",
  "keyword": "And "
});
formatter.match({
  "location": "BusinessPlanningSteps.user_Verifying_copy_edit_and_delete_action()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "User verify datas in BusinessPlan for Style",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@BusinessHierarchy"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User verify business plan for season  \"\u003ccopy\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User verify business plan for brand",
  "keyword": "And "
});
formatter.step({
  "name": "User verify business plan for collection\"\u003ccopy1\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User verify business plan for department\"\u003ccopy2\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "copy",
        "copy1",
        "copy2"
      ]
    },
    {
      "cells": [
        "Copy of Default",
        "Copy of Default",
        "Copy of Default"
      ]
    }
  ]
});
formatter.scenario({
  "name": "User verify datas in BusinessPlan for Style",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@BusinessPlanning"
    },
    {
      "name": "@BusinessHierarchy"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User verify business plan for season  \"Copy of Default\"",
  "keyword": "Then "
});
formatter.match({
  "location": "BusinessPlanningSteps.user_verify_business_plan_for_season(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User verify business plan for brand",
  "keyword": "And "
});
formatter.match({
  "location": "BusinessPlanningSteps.user_verify_business_plan_for_brand()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User verify business plan for collection\"Copy of Default\"",
  "keyword": "Then "
});
formatter.match({
  "location": "BusinessPlanningSteps.user_verify_business_plan_for_collection(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User verify business plan for department\"Copy of Default\"",
  "keyword": "And "
});
formatter.match({
  "location": "BusinessPlanningSteps.user_verify_business_plan_for_department(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/N_BusinessObject.feature");
formatter.feature({
  "name": "Creation of Business Object",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@BusinessObjectCreation"
    }
  ]
});
formatter.scenarioOutline({
  "name": "User creates Business Object",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@BusinesssObject"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates Enumeration value in Config_Setup \"\u003cEnumValue1\u003e\",\"\u003cEnumValue2\u003e\",\"\u003cEnumValue3\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "User select Style from Business Object \"Style\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User create New Attribute for Enum values \"\u003cAttributeName\u003e\",\"\u003cAttributeType\u003e\",\"\u003cDisplayName\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User Navigates to formDefinition",
  "keyword": "Then "
});
formatter.step({
  "name": "Select the Business Object Attributes",
  "keyword": "And "
});
formatter.step({
  "name": "User click on update cnfiguration",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "EnumValue1",
        "EnumValue2",
        "EnumValue3",
        "AttributeName",
        "AttributeType",
        "DisplayName"
      ]
    },
    {
      "cells": [
        "Enum A,Enum A1,Enum A2",
        "Enum B,Enum B1,Enum B2",
        "Enum C,Enum C1",
        "Auto_String,Auto_Test,Enum_A,Enum_B,Enum_C",
        "enum",
        "Tested by Automation"
      ]
    }
  ]
});
formatter.scenario({
  "name": "User creates Business Object",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@BusinessObjectCreation"
    },
    {
      "name": "@BusinesssObject"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates Enumeration value in Config_Setup \"Enum A,Enum A1,Enum A2\",\"Enum B,Enum B1,Enum B2\",\"Enum C,Enum C1\"",
  "keyword": "When "
});
formatter.match({
  "location": "BusinessPlanningSteps.user_creates_Enumeration_value_in_Config_Setup(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User select Style from Business Object \"Style\"",
  "keyword": "Then "
});
formatter.match({
  "location": "BusinessPlanningSteps.user_select_Style_from_Business_Object(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User create New Attribute for Enum values \"Auto_String,Auto_Test,Enum_A,Enum_B,Enum_C\",\"enum\",\"Tested by Automation\"",
  "keyword": "And "
});
formatter.match({
  "location": "BusinessPlanningSteps.user_create_New_Attribute_for_Enum_values(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Navigates to formDefinition",
  "keyword": "Then "
});
formatter.match({
  "location": "BusinessPlanningSteps.user_Navigates_to_formDefinition()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Select the Business Object Attributes",
  "keyword": "And "
});
formatter.match({
  "location": "BusinessPlanningSteps.select_the_Business_Object_Attributes()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User click on update cnfiguration",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.user_click_on_update_cnfiguration()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "User Creates Style-Apparel BO",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@StyleApparelBO"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User Create New Style in Season \"\u003cseason\u003e\",\"\u003cstylename\u003e\",\"\u003cEnumvalue\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User Clicks NewStyle and verify created values in BusinessObject",
  "keyword": "Then "
});
formatter.step({
  "name": "User Clicks ApparelBO and manage Views",
  "keyword": "And "
});
formatter.step({
  "name": "User Verify that AutoString \"\u003cAutoString\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "season",
        "stylename",
        "Enumvalue",
        "AutoString"
      ]
    },
    {
      "cells": [
        "winter season",
        "Apparel-BO",
        "Enum A1",
        "Tested by Automation"
      ]
    }
  ]
});
formatter.scenario({
  "name": "User Creates Style-Apparel BO",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@BusinessObjectCreation"
    },
    {
      "name": "@StyleApparelBO"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Create New Style in Season \"winter season\",\"Apparel-BO\",\"Enum A1\"",
  "keyword": "Then "
});
formatter.match({
  "location": "BusinessPlanningSteps.user_Create_New_Style_in_Season(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Clicks NewStyle and verify created values in BusinessObject",
  "keyword": "Then "
});
formatter.match({
  "location": "BusinessPlanningSteps.user_Clicks_NewStyle_and_verify_created_values_in_BusinessObject()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Clicks ApparelBO and manage Views",
  "keyword": "And "
});
formatter.match({
  "location": "BusinessPlanningSteps.user_Clicks_ApparelBO_and_manage_Views()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Verify that AutoString \"Tested by Automation\"",
  "keyword": "Then "
});
formatter.match({
  "location": "BusinessPlanningSteps.user_Verify_that_AutoString(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.logout_from_the_Application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/O_BOMStyle.feature");
formatter.feature({
  "name": "Creation of BOMStyle",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@BOMStyle"
    }
  ]
});
formatter.scenarioOutline({
  "name": "User creates NewStyleBOM-Set1",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@NewStyleBOM1"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user creates NeStyleBOM  under Apparelcolorandsize \"\u003cBOMName\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "user creates BOM validation in manage-views",
  "keyword": "And "
});
formatter.step({
  "name": "user validates Color under NewStyleBOM-placements",
  "keyword": "And "
});
formatter.step({
  "name": "user creates NewOfMaterial",
  "keyword": "And "
});
formatter.step({
  "name": "user select season availability for cotton jersey-copy",
  "keyword": "And "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "user creates NewFromMaterial \"\u003cBOMValue\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "user select Materialsection also draganddrop the created material",
  "keyword": "And "
});
formatter.step({
  "name": "user select Apparel and fabric section",
  "keyword": "And "
});
formatter.step({
  "name": "user creates Newofstyle and newfromstyle apparel section \"\u003cStyle\u003e\",\"\u003cColor\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user creates NewSpecial in fabric section \"\u003cMaterialNew\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "BOMName",
        "BOMValue",
        "Style",
        "Color",
        "MaterialNew",
        "Commoncolor",
        "unitcost"
      ]
    },
    {
      "cells": [
        "Apparel BOM Validation",
        "Material section",
        "New of style",
        "Only Color",
        "SpecialMaterial",
        "BOM RED,EGRET",
        "3400"
      ]
    }
  ]
});
formatter.scenario({
  "name": "User creates NewStyleBOM-Set1",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@BOMStyle"
    },
    {
      "name": "@NewStyleBOM1"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates NeStyleBOM  under Apparelcolorandsize \"Apparel BOM Validation\"",
  "keyword": "When "
});
formatter.match({
  "location": "BOMCreationSteps.user_creates_NeStyleBOM_under_Apparelcolorandsize(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates BOM validation in manage-views",
  "keyword": "And "
});
formatter.match({
  "location": "BOMCreationSteps.user_creates_BOM_validation_in_manage_views()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user validates Color under NewStyleBOM-placements",
  "keyword": "And "
});
formatter.match({
  "location": "BOMCreationSteps.user_validates_Color_under_NewStyleBOM_placements()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates NewOfMaterial",
  "keyword": "And "
});
formatter.match({
  "location": "BOMCreationSteps.user_creates_NewOfMaterial()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user select season availability for cotton jersey-copy",
  "keyword": "And "
});
formatter.match({
  "location": "BOMCreationSteps.user_select_season_availability_for_cotton_jersey_copy()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates NewFromMaterial \"Material section\"",
  "keyword": "When "
});
formatter.match({
  "location": "BOMCreationSteps.user_creates_NewFromMaterial(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user select Materialsection also draganddrop the created material",
  "keyword": "And "
});
formatter.match({
  "location": "BOMCreationSteps.user_select_Materialsection_also_draganddrop_the_created_material()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user select Apparel and fabric section",
  "keyword": "And "
});
formatter.match({
  "location": "BOMCreationSteps.user_select_Apparel_and_fabric_section()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates Newofstyle and newfromstyle apparel section \"New of style\",\"Only Color\"",
  "keyword": "And "
});
formatter.match({
  "location": "BOMCreationSteps.user_creates_Newofstyle_and_newfromstyle_apparel_section(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates NewSpecial in fabric section \"SpecialMaterial\"",
  "keyword": "Then "
});
formatter.match({
  "location": "BOMCreationSteps.user_creates_NewSpecial_in_fabric_section(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.logout_from_the_Application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/P_Converstion.feature");
formatter.feature({
  "name": "Creation of Conversation Categories",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Conversation"
    }
  ]
});
formatter.scenarioOutline({
  "name": "User Conversation Categories creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@ConversationCategory"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "User creates converstion value in config_setup \"\u003cconversation1\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Edit and delete the existing conversation data \"\u003cconversation2\u003e\",\"\u003cconversation3\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User select Style from Business Object \"Style\"",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to to conversation cateogries within style",
  "keyword": "When "
});
formatter.step({
  "name": "User click on update cnfiguration",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "conversation1",
        "conversation2",
        "conversation3"
      ]
    },
    {
      "cells": [
        "Planning the style,Planning the Material,Designing the Style,Plan for Seasons",
        "Test Running,Design Style,Plan Material",
        "Tested Runner"
      ]
    }
  ]
});
formatter.scenario({
  "name": "User Conversation Categories creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Conversation"
    },
    {
      "name": "@ConversationCategory"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates converstion value in config_setup \"Planning the style,Planning the Material,Designing the Style,Plan for Seasons\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ConversationCategoriesSteps.user_creates_converstion_value_in_config_setup(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Edit and delete the existing conversation data \"Test Running,Design Style,Plan Material\",\"Tested Runner\"",
  "keyword": "And "
});
formatter.match({
  "location": "ConversationCategoriesSteps.edit_and_delete_the_existing_conversation_data(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User select Style from Business Object \"Style\"",
  "keyword": "And "
});
formatter.match({
  "location": "BusinessPlanningSteps.user_select_Style_from_Business_Object(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to to conversation cateogries within style",
  "keyword": "When "
});
formatter.match({
  "location": "ConversationCategoriesSteps.navigate_to_to_conversation_cateogries_within_style()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User click on update cnfiguration",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.user_click_on_update_cnfiguration()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Conversation with season hierarchy",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@ConversationHierarchy"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to season create conversation",
  "keyword": "And "
});
formatter.step({
  "name": "Edit and Delete the created conversation",
  "keyword": "When "
});
formatter.step({
  "name": "Verify the conversation by sort order",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to Material create conversation",
  "keyword": "And "
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "conversation1",
        "conversation2",
        "conversation3"
      ]
    },
    {
      "cells": [
        "Planning the style,Planning the Material,Designing the Style,Plan for Seasons",
        "Test Running,Design Style,Plan Material",
        "Tested Runner"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Conversation with season hierarchy",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Conversation"
    },
    {
      "name": "@ConversationHierarchy"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to season create conversation",
  "keyword": "And "
});
formatter.match({
  "location": "ConversationCategoriesSteps.navigate_to_season_create_conversation()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Edit and Delete the created conversation",
  "keyword": "When "
});
formatter.match({
  "location": "ConversationCategoriesSteps.edit_and_Delete_the_created_conversation()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Verify the conversation by sort order",
  "keyword": "And "
});
formatter.match({
  "location": "ConversationCategoriesSteps.verify_the_conversation_by_sort_order()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Material create conversation",
  "keyword": "And "
});
formatter.match({
  "location": "ConversationCategoriesSteps.navigate_to_Material_create_conversation()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.logout_from_the_Application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/Q_Calendar.feature");
formatter.feature({
  "name": "Data Setup for Calendar",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Calendar"
    }
  ]
});
formatter.scenarioOutline({
  "name": "calendar setup for season",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@CalendarSetup"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User moves into Calendar Template setup",
  "keyword": "Then "
});
formatter.step({
  "name": "User create Calendar Type for season \"\u003ccalendartype\u003e\",\"\u003ctemplatecalendar\u003e\",\"\u003ccalendardescription\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user create season Template activity under Calendar Type \"\u003cTemplateactivity\u003e\",\"\u003cActivityType\u003e\",\"\u003cActivityLevel\u003e\",\"\u003cTrackingType\u003e\",\"\u003cTrackingPhase\u003e\",\"\u003cTrackingstate\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "calendartype",
        "templatecalendar",
        "calendardescription",
        "Templateactivity",
        "ActivityType",
        "ActivityLevel",
        "TrackingType",
        "TrackingPhase",
        "Trackingstate"
      ]
    },
    {
      "cells": [
        "Season",
        "Calendar_Season",
        "Season calendar Template",
        "Manual,Milestone,Artwork Track,IDS Track",
        "Manual Activity,Track Activity",
        "Brand,Collection,Style",
        "Artwork,Image Data Sheet",
        "Production,sample",
        "APPROVED"
      ]
    }
  ]
});
formatter.scenario({
  "name": "calendar setup for season",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Calendar"
    },
    {
      "name": "@CalendarSetup"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User moves into Calendar Template setup",
  "keyword": "Then "
});
formatter.match({
  "location": "CalendarSetupSteps.user_moves_into_Calendar_Template_setup()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User create Calendar Type for season \"Season\",\"Calendar_Season\",\"Season calendar Template\"",
  "keyword": "And "
});
formatter.match({
  "location": "CalendarSetupSteps.user_create_Calendar_Type_for_season(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user create season Template activity under Calendar Type \"Manual,Milestone,Artwork Track,IDS Track\",\"Manual Activity,Track Activity\",\"Brand,Collection,Style\",\"Artwork,Image Data Sheet\",\"Production,sample\",\"APPROVED\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CalendarSetupSteps.user_create_season_Template_activity_under_Calendar_Type(String,String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "calendar setup for material",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@CalendarSetupMaterial"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User moves into Calendar Template setup",
  "keyword": "Then "
});
formatter.step({
  "name": "User create Calendar Type for material \"\u003ccalendartype\u003e\",\"\u003ctemplatecalendar\u003e\",\"\u003ccalendardescription\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user create material Template activity under Calendar Type \"\u003cTemplateactivity\u003e\",\"\u003cActivityType\u003e\",\"\u003cActivityLevel\u003e\",\"\u003cTrackingType\u003e\",\"\u003cTrackingstate\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "calendartype",
        "templatecalendar",
        "calendardescription",
        "Templateactivity",
        "ActivityType",
        "ActivityLevel",
        "TrackingType",
        "Trackingstate"
      ]
    },
    {
      "cells": [
        "Material Security Group",
        "Calendar_MSG",
        "MSG Calendar Template",
        "Manual,BOM Track,Color Data Sheet Track,Test Run Track,Milestone",
        "Manual Activity,Track Activity,Milestone",
        "Material Security Group,Material BOM",
        "Material BOM,Material Color Data Sheet,Test Run",
        "APPROVED,PENDING"
      ]
    }
  ]
});
formatter.scenario({
  "name": "calendar setup for material",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Calendar"
    },
    {
      "name": "@CalendarSetupMaterial"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User moves into Calendar Template setup",
  "keyword": "Then "
});
formatter.match({
  "location": "CalendarSetupSteps.user_moves_into_Calendar_Template_setup()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User create Calendar Type for material \"Material Security Group\",\"Calendar_MSG\",\"MSG Calendar Template\"",
  "keyword": "And "
});
formatter.match({
  "location": "CalendarSetupSteps.user_create_Calendar_Type_for_material(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user create material Template activity under Calendar Type \"Manual,BOM Track,Color Data Sheet Track,Test Run Track,Milestone\",\"Manual Activity,Track Activity,Milestone\",\"Material Security Group,Material BOM\",\"Material BOM,Material Color Data Sheet,Test Run\",\"APPROVED,PENDING\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CalendarSetupSteps.user_create_material_Template_activity_under_Calendar_Type(String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "master calendar creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Calendarmaster"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User moves into Calendar Template setup",
  "keyword": "Then "
});
formatter.step({
  "name": "User create Calendar Type for PO \"\u003ccalendartype\u003e\",\"\u003ctemplatecalendar\u003e\",\"\u003ccalendardescription\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user create PO Template activity under Calendar Type \"\u003cTemplateactivity\u003e\",\"\u003cActivityType\u003e\",\"\u003cActivityLevel\u003e\",\"\u003cTrackingType\u003e\",\"\u003cTrackingstate\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Go to homepage and create master calendar",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "calendartype",
        "templatecalendar",
        "calendardescription",
        "Templateactivity",
        "ActivityType",
        "ActivityLevel",
        "TrackingType",
        "Trackingstate"
      ]
    },
    {
      "cells": [
        "PO Group",
        "Calendar_PO",
        "PO Calendar Template",
        "Manual,Style Review Track,Size Chart Review,Mile",
        "Manual Activity,Track Activity,Milestone",
        "PO Group,PO Color,PO Product,Supplier PO",
        "Style Review,Size Chart Review",
        "APPROVED"
      ]
    }
  ]
});
formatter.scenario({
  "name": "master calendar creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Calendar"
    },
    {
      "name": "@Calendarmaster"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User moves into Calendar Template setup",
  "keyword": "Then "
});
formatter.match({
  "location": "CalendarSetupSteps.user_moves_into_Calendar_Template_setup()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User create Calendar Type for PO \"PO Group\",\"Calendar_PO\",\"PO Calendar Template\"",
  "keyword": "And "
});
formatter.match({
  "location": "CalendarSetupSteps.user_create_Calendar_Type_for_PO(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user create PO Template activity under Calendar Type \"Manual,Style Review Track,Size Chart Review,Mile\",\"Manual Activity,Track Activity,Milestone\",\"PO Group,PO Color,PO Product,Supplier PO\",\"Style Review,Size Chart Review\",\"APPROVED\"",
  "keyword": "Then "
});
formatter.match({
  "location": "CalendarSetupSteps.user_create_PO_Template_activity_under_Calendar_Type(String,String,String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage and create master calendar",
  "keyword": "And "
});
formatter.match({
  "location": "CalendarSetupSteps.go_to_homepage_and_create_master_calendar()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "calendar date selection and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Calendardate"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user enter date for calendar season",
  "keyword": "And "
});
formatter.step({
  "name": "user modify the manual date and approves it",
  "keyword": "And "
});
formatter.step({
  "name": "user navigates into Artwork tab and creates \"\u003cArtworkname\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user navigates into Image tab and creates \"\u003cIDSName\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user select calendar season under style denim and verify the screen",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Artworkname",
        "IDSName"
      ]
    },
    {
      "cells": [
        "Calendar Artwork",
        "Calendar IDS"
      ]
    }
  ]
});
formatter.scenario({
  "name": "calendar date selection and validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Calendar"
    },
    {
      "name": "@Calendardate"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user enter date for calendar season",
  "keyword": "And "
});
formatter.match({
  "location": "CalendarSetupSteps.user_enter_date_for_calendar_season()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user modify the manual date and approves it",
  "keyword": "And "
});
formatter.match({
  "location": "CalendarSetupSteps.user_modify_the_manual_date_and_approves_it()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user navigates into Artwork tab and creates \"Calendar Artwork\"",
  "keyword": "And "
});
formatter.match({
  "location": "CalendarSetupSteps.user_navigates_into_Artwork_tab_and_creates(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user navigates into Image tab and creates \"Calendar IDS\"",
  "keyword": "And "
});
formatter.match({
  "location": "CalendarSetupSteps.user_navigates_into_Image_tab_and_creates(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user select calendar season under style denim and verify the screen",
  "keyword": "Then "
});
formatter.match({
  "location": "CalendarSetupSteps.user_select_calendar_season_under_style_denim_and_verify_the_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "calendar date selection in material",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Calendarmaterial"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user create material sku under material \"\u003cmaterailsku\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user select calendar enter date for the calendar season",
  "keyword": "And "
});
formatter.step({
  "name": "user freeze and apply calendar",
  "keyword": "And "
});
formatter.step({
  "name": "user edit manual date under material activities",
  "keyword": "And "
});
formatter.step({
  "name": "user navigates MSGSTANDALONE creates BOM and approves it \"\u003cBomvalue\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user approves created MDS and MCDS \"\u003cmdsvalue\u003e\",\"\u003ccolorvalue\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "materailsku",
        "Bomvalue",
        "mdsvalue",
        "colorvalue"
      ]
    },
    {
      "cells": [
        "MT SKU",
        "Calendar BOM",
        "Calendar MDS",
        "11-0103 EGRET"
      ]
    }
  ]
});
formatter.scenario({
  "name": "calendar date selection in material",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Calendar"
    },
    {
      "name": "@Calendarmaterial"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user create material sku under material \"MT SKU\"",
  "keyword": "And "
});
formatter.match({
  "location": "CalendarSetupSteps.user_create_material_sku_under_material(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user select calendar enter date for the calendar season",
  "keyword": "And "
});
formatter.match({
  "location": "CalendarSetupSteps.user_select_calendar_enter_date_for_the_calendar_season()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user freeze and apply calendar",
  "keyword": "And "
});
formatter.match({
  "location": "CalendarSetupSteps.user_freeze_and_apply_calendar()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user edit manual date under material activities",
  "keyword": "And "
});
formatter.match({
  "location": "CalendarSetupSteps.user_edit_manual_date_under_material_activities()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user navigates MSGSTANDALONE creates BOM and approves it \"Calendar BOM\"",
  "keyword": "And "
});
formatter.match({
  "location": "CalendarSetupSteps.user_navigates_MSGSTANDALONE_creates_BOM_and_approves_it(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user approves created MDS and MCDS \"Calendar MDS\",\"11-0103 EGRET\"",
  "keyword": "And "
});
formatter.match({
  "location": "CalendarSetupSteps.user_approves_created_MDS_and_MCDS(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "calendar sourcing and po order validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Calendarsourcing"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "create calendar po template for order po group",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to calendarpo and enter date for the calendar",
  "keyword": "And "
});
formatter.step({
  "name": "user freeze and apply calendar for the data",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to style create review \"\u003cReview\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Review"
      ]
    },
    {
      "cells": [
        "Calendar Style Review"
      ]
    }
  ]
});
formatter.scenario({
  "name": "calendar sourcing and po order validation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Calendar"
    },
    {
      "name": "@Calendarsourcing"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "create calendar po template for order po group",
  "keyword": "And "
});
formatter.match({
  "location": "CalendarSetupSteps.create_calendar_po_template_for_order_po_group()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to calendarpo and enter date for the calendar",
  "keyword": "And "
});
formatter.match({
  "location": "CalendarSetupSteps.navigate_to_calendarpo_and_enter_date_for_the_calendar()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user freeze and apply calendar for the data",
  "keyword": "And "
});
formatter.match({
  "location": "CalendarSetupSteps.user_freeze_and_apply_calendar_for_the_data()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to style create review \"Calendar Style Review\"",
  "keyword": "And "
});
formatter.match({
  "location": "CalendarSetupSteps.navigate_to_style_create_review(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.logout_from_the_Application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/Q_SizeChartCreation.feature");
formatter.feature({
  "name": "Creation and evaluation of sizechart",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@SizeChart"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Sizechart creation setup",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SizeChartIncrement"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates setup page in Increment \"\u003cIncrementvalue\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User sets grainline in ProductGroup",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Incrementvalue"
      ]
    },
    {
      "cells": [
        "34,30,45,55,67,45,34,32,56,66"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Sizechart creation setup",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SizeChart"
    },
    {
      "name": "@SizeChartIncrement"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates setup page in Increment \"34,30,45,55,67,45,34,32,56,66\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SizeChartPage.user_creates_setup_page_in_Increment_and_ProductGroup(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User sets grainline in ProductGroup",
  "keyword": "And "
});
formatter.match({
  "location": "SizeChartPage.User_sets_grainline_in_ProductGroup()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Sizechart creation setup",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SizeSetup"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User Navigates to apparelColorandsize under season",
  "keyword": "Then "
});
formatter.step({
  "name": "User creates NewSizeChart \"\u003csizechartvalue\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User manageviews for Dimension and TDS",
  "keyword": "And "
});
formatter.step({
  "name": "Select the Warp, weave, grainlinevalues \"\u003cwarpShrinkagevalue\u003e\",\"\u003cweaveShrinkagevalue\u003e\",\"\u003cgralinevalue\"\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "user fills the pattern value for large, medium and small \"\u003csmall\u003e\",\"\u003cmedium\u003e\",\"\u003clarge\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User restore increments and restore tolerance",
  "keyword": "And "
});
formatter.step({
  "name": "user enter Shrinkagevalue \"\u003cShrinkagevalue\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user restoreshrinkage",
  "keyword": "Then "
});
formatter.step({
  "name": "user Resetpattern",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "sizechartvalue",
        "small",
        "medium",
        "large",
        "Shrinkagevalue",
        "warpShrinkagevalue",
        "weaveShrinkagevalue",
        "gralinevalue"
      ]
    },
    {
      "cells": [
        "Apparel SC - 01",
        "45,46,32,45",
        "45,56,70,56",
        "34,34,24,34",
        "15,20",
        "5",
        "10",
        "Cut With Grainline,Cut Against Grainline"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Sizechart creation setup",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SizeChart"
    },
    {
      "name": "@SizeSetup"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Navigates to apparelColorandsize under season",
  "keyword": "Then "
});
formatter.match({
  "location": "SizeChartPage.user_Navigates_to_apparelColorandsize_under_season()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates NewSizeChart \"Apparel SC - 01\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SizeChartPage.user_creates_NewSizeChart(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User manageviews for Dimension and TDS",
  "keyword": "And "
});
formatter.match({
  "location": "SizeChartPage.User_manageviews_for_Dimension_and_TDS()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Select the Warp, weave, grainlinevalues \"5\",\"10\",\"\u003cgralinevalue\"\u003e",
  "keyword": "And "
});
formatter.match({
  "location": "SizeChartPage.select_the_Warp_weave_grainlinevalues(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user fills the pattern value for large, medium and small \"45,46,32,45\",\"45,56,70,56\",\"34,34,24,34\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SizeChartPage.user_fills_the_pattern_value_for_large_medium_and_small(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User restore increments and restore tolerance",
  "keyword": "And "
});
formatter.match({
  "location": "SizeChartPage.user_restore_increments_and_restore_tolerance()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user enter Shrinkagevalue \"15,20\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizeChartPage.user_enter_Shrinkagevalue(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user restoreshrinkage",
  "keyword": "Then "
});
formatter.match({
  "location": "SizeChartPage.user_restoreshrinkage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user Resetpattern",
  "keyword": "Then "
});
formatter.match({
  "location": "SizeChartPage.user_Resetpattern()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Sizechart setup for changeproductgroup",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SizeChangeproduct"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User create New Increment value",
  "keyword": "Then "
});
formatter.step({
  "name": "select Dimension from list \"\u003cIncrementvalue\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User create New product group value",
  "keyword": "Then "
});
formatter.step({
  "name": "User enters value in newfromdimension \"\u003cTolerancevalue\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Incrementvalue",
        "Tolerancevalue"
      ]
    },
    {
      "cells": [
        "8,3,9,2,10,11",
        "1,2,3"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Sizechart setup for changeproductgroup",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SizeChart"
    },
    {
      "name": "@SizeChangeproduct"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User create New Increment value",
  "keyword": "Then "
});
formatter.match({
  "location": "SizeChartPage.user_create_New_Increment_value()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "select Dimension from list \"8,3,9,2,10,11\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizeChartPage.select_Dimension_from_list(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User create New product group value",
  "keyword": "Then "
});
formatter.match({
  "location": "SizeChartPage.user_create_New_product_group_value()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User enters value in newfromdimension \"1,2,3\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SizeChartPage.user_enters_value_in_newfromdimension(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "changeproductgroup in Sizechart",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SizeChangeProduct1"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User Navigates to apparelColorandsize under season",
  "keyword": "Then "
});
formatter.step({
  "name": "user changeproductgroup under sizechart \"\u003cnewProdgrp\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user changeincrement under sizechart \"\u003cnewProdgrp\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user change sizerange",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "newProdgrp"
      ]
    },
    {
      "cells": [
        "NEW PG - 01"
      ]
    }
  ]
});
formatter.scenario({
  "name": "changeproductgroup in Sizechart",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SizeChart"
    },
    {
      "name": "@SizeChangeProduct1"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Navigates to apparelColorandsize under season",
  "keyword": "Then "
});
formatter.match({
  "location": "SizeChartPage.user_Navigates_to_apparelColorandsize_under_season()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user changeproductgroup under sizechart \"NEW PG - 01\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizeChartPage.user_changeproductgroup_under_sizechart(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user changeincrement under sizechart \"NEW PG - 01\"",
  "keyword": "And "
});
formatter.match({
  "location": "SizeChartPage.user_changeincrement_under_sizechart(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user change sizerange",
  "keyword": "Then "
});
formatter.match({
  "location": "SizeChartPage.user_change_sizerange()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Evaluate Sizechart for Apparel SC - 01",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@EvaluateSizeChartApparel"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User Navigates to apparelColorandsize under season",
  "keyword": "Then "
});
formatter.step({
  "name": "user selects the created sizechart uncheck size medium",
  "keyword": "And "
});
formatter.step({
  "name": "Evalaute the Sizechart \"\u003csupplierrequest\u003e\",\"\u003csampledimension\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "verify values under Evaluation tab",
  "keyword": "And "
});
formatter.step({
  "name": "User pass the Evaluation as Expected value refelcted in page",
  "keyword": "Then "
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "supplierrequest",
        "sampledimension"
      ]
    },
    {
      "cells": [
        "Apparel - SR",
        "Colors and Sizes"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Evaluate Sizechart for Apparel SC - 01",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@SizeChart"
    },
    {
      "name": "@EvaluateSizeChartApparel"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Navigates to apparelColorandsize under season",
  "keyword": "Then "
});
formatter.match({
  "location": "SizeChartPage.user_Navigates_to_apparelColorandsize_under_season()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user selects the created sizechart uncheck size medium",
  "keyword": "And "
});
formatter.match({
  "location": "SizeChartPage.user_selects_the_created_sizechart_uncheck_size_medium()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Evalaute the Sizechart \"Apparel - SR\",\"Colors and Sizes\"",
  "keyword": "Then "
});
formatter.match({
  "location": "SizeChartPage.evalaute_the_Sizechart(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify values under Evaluation tab",
  "keyword": "And "
});
formatter.match({
  "location": "SizeChartPage.verify_values_under_Evaluation_tab()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User pass the Evaluation as Expected value refelcted in page",
  "keyword": "Then "
});
formatter.match({
  "location": "SizeChartPage.user_pass_the_Evaluation_as_Expected_value_refelcted_in_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.logout_from_the_Application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/Q_Wizard.feature");
formatter.feature({
  "name": "Creation of New Wizard",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Wizard"
    }
  ]
});
formatter.scenarioOutline({
  "name": "User Create New style with wizard",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@WizardNewStyle"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to style tab and create new style with name as wizard \"\u003cStyleName\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to new from style",
  "keyword": "When "
});
formatter.step({
  "name": "Create Move Style from within the style tab",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to apparel color and size style add image",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to new apparel wizard colorway verify data visible",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Style Name"
      ]
    },
    {
      "cells": [
        "New Apparel - Wizard"
      ]
    }
  ]
});
formatter.scenario({
  "name": "User Create New style with wizard",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Wizard"
    },
    {
      "name": "@WizardNewStyle"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to style tab and create new style with name as wizard \"\u003cStyleName\u003e\"",
  "keyword": "And "
});
formatter.match({
  "location": "WizardPageSteps.navigate_to_style_tab_and_create_new_style_with_name_as_wizard(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to new from style",
  "keyword": "When "
});
formatter.match({
  "location": "WizardPageSteps.navigate_to_new_from_style()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create Move Style from within the style tab",
  "keyword": "And "
});
formatter.match({
  "location": "WizardPageSteps.create_Move_Style_from_within_the_style_tab()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to apparel color and size style add image",
  "keyword": "And "
});
formatter.match({
  "location": "WizardPageSteps.navigate_to_apparel_color_and_size_style_add_image()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to new apparel wizard colorway verify data visible",
  "keyword": "And "
});
formatter.match({
  "location": "WizardPageSteps.navigate_to_new_apparel_wizard_colorway_verify_data_visible()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/R_FoodSetup.feature");
formatter.feature({
  "name": "Creating Food Setup",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@FoodSetupHeirarchy"
    },
    {
      "name": "@FoodTypes"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Creation for Food",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@FoodSetup"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to Food Setup",
  "keyword": "And "
});
formatter.step({
  "name": "Create food config \"\u003cAllergens\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Create claims \"\u003cClaims\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "create food ingredients \"\u003cIngredients\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Create food nutrients \"\u003cNutrients\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "user create Datasheet Template \"\u003cDSallergen\u003e\",\"\u003cDSnutrient\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Allergens",
        "Claims",
        "Ingredients",
        "Nutrients",
        "DSallergen",
        "DSnutrient"
      ]
    },
    {
      "cells": [
        "Fish,Milk,Eggs,Soybean,Tree Nuts,Peanuts,ShellFish",
        "structure/function claims,nutrient content claims,health claims",
        "caramelized onions,Oils,Vinegars,Chilli paste,Soy sauce,cheddar cheese,green pepper,grilled chicken,Pepper",
        "Proteins,Vitamins,Minerals,Calories,Sodium,Sugars,Carbohydrates",
        "Eggs,Fish,Milk,Peanuts,ShellFish,Soybean,Tree Nuts",
        "Calories,Carbohydrates,Minerals,Proteins,Sodium,Sugars,Vitamins"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Creation for Food",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@FoodSetupHeirarchy"
    },
    {
      "name": "@FoodTypes"
    },
    {
      "name": "@FoodSetup"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Food Setup",
  "keyword": "And "
});
formatter.match({
  "location": "FoodSetupSteps.Navigate_to_Food_Setup()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create food config \"Fish,Milk,Eggs,Soybean,Tree Nuts,Peanuts,ShellFish\"",
  "keyword": "Then "
});
formatter.match({
  "location": "FoodSetupSteps.Create_food_config(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create claims \"structure/function claims,nutrient content claims,health claims\"",
  "keyword": "Then "
});
formatter.match({
  "location": "FoodSetupSteps.Create_claims(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "create food ingredients \"caramelized onions,Oils,Vinegars,Chilli paste,Soy sauce,cheddar cheese,green pepper,grilled chicken,Pepper\"",
  "keyword": "Then "
});
formatter.match({
  "location": "FoodSetupSteps.create_food_ingredients(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create food nutrients \"Proteins,Vitamins,Minerals,Calories,Sodium,Sugars,Carbohydrates\"",
  "keyword": "Then "
});
formatter.match({
  "location": "FoodSetupSteps.create_food_nutrients(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user create Datasheet Template \"Eggs,Fish,Milk,Peanuts,ShellFish,Soybean,Tree Nuts\",\"Calories,Carbohydrates,Minerals,Proteins,Sodium,Sugars,Vitamins\"",
  "keyword": "Then "
});
formatter.match({
  "location": "FoodSetupSteps.user_create_Datasheet_Template(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Creation for Food",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@CompetitiveFoodcreation"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user create foodtype and competitivefood \"\u003ccompetitive\u003e\",\"\u003cFood\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User select Salesregion and perform delete action",
  "keyword": "Then "
});
formatter.step({
  "name": "select Competitive food",
  "keyword": "And "
});
formatter.step({
  "name": "create Food SKU \"\u003csku\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "competitive",
        "Food",
        "sku"
      ]
    },
    {
      "cells": [
        "Cheese Pizza,1000,TEST",
        "Pizza,veg pizza",
        "FOOD SKU,CAD"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Creation for Food",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@FoodSetupHeirarchy"
    },
    {
      "name": "@FoodTypes"
    },
    {
      "name": "@CompetitiveFoodcreation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user create foodtype and competitivefood \"Cheese Pizza,1000,TEST\",\"Pizza,veg pizza\"",
  "keyword": "And "
});
formatter.match({
  "location": "FoodSetupSteps.user_create_foodtype_and_competitivefood(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User select Salesregion and perform delete action",
  "keyword": "Then "
});
formatter.match({
  "location": "FoodSetupSteps.user_select_Salesregion_and_perform_delete_action()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "select Competitive food",
  "keyword": "And "
});
formatter.match({
  "location": "FoodSetupSteps.select_Competitive_food()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "create Food SKU \"FOOD SKU,CAD\"",
  "keyword": "And "
});
formatter.match({
  "location": "FoodSetupSteps.create_Food_SKU(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Creating datas under created Food",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@FoodDataCreation"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user create and validate datapackge",
  "keyword": "Then "
});
formatter.step({
  "name": "create Lables \"\u003clabels\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "under labels select values for Allergen Ingredient Nutrient Claim \"\u003cfooddata\u003e\",\"\u003callergens\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "user create Artwork values",
  "keyword": "Then "
});
formatter.step({
  "name": "create value for Review also verify delete function",
  "keyword": "And "
});
formatter.step({
  "name": "user verify copy edit and delte in spec creation \"\u003cspec\u003e\",\"\u003cspecedit\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "labels",
        "fooddata",
        "spec",
        "specedit",
        "allergens"
      ]
    },
    {
      "cells": [
        "Food,Food Additives  Colours and Flavors Datasheet",
        "New Ingredient,New Allergen,Eggs,New Nutrient,Calories,New Claim",
        "spec",
        "specedit",
        "Fish,Milk,Eggs,Soybean,Tree Nuts,Peanuts,ShellFish"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Creating datas under created Food",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@FoodSetupHeirarchy"
    },
    {
      "name": "@FoodTypes"
    },
    {
      "name": "@FoodDataCreation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user create and validate datapackge",
  "keyword": "Then "
});
formatter.match({
  "location": "FoodSetupSteps.user_create_and_validate_datapackge()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "create Lables \"Food,Food Additives  Colours and Flavors Datasheet\"",
  "keyword": "And "
});
formatter.match({
  "location": "FoodSetupSteps.create_Lables(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "under labels select values for Allergen Ingredient Nutrient Claim \"New Ingredient,New Allergen,Eggs,New Nutrient,Calories,New Claim\",\"Fish,Milk,Eggs,Soybean,Tree Nuts,Peanuts,ShellFish\"",
  "keyword": "Then "
});
formatter.match({
  "location": "FoodSetupSteps.under_labels_select_values_for_Allergen_Ingredient_Nutrient_Claim(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user create Artwork values",
  "keyword": "Then "
});
formatter.match({
  "location": "FoodSetupSteps.user_create_Artwork_values()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "create value for Review also verify delete function",
  "keyword": "And "
});
formatter.match({
  "location": "FoodSetupSteps.create_value_for_Review_also_verify_delete_function()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user verify copy edit and delte in spec creation \"spec\",\"specedit\"",
  "keyword": "Then "
});
formatter.match({
  "location": "FoodSetupSteps.user_verify_copy_edit_and_delte_in_spec_creation(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Creating data Package Templates under Data Packages",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@DataPackage"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to Data packages",
  "keyword": "And "
});
formatter.step({
  "name": "Create Data Package Templates \"\u003cDPTemplates\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Go to the Data Packages with cover sheet and create the Data Sheets And Verify Edit Delete Copy Functionality \"\u003cDPTemplates\u003e\",\"\u003cData SheetTypes\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Go to the Data Packages without cover sheet and create the Data Sheets \"\u003cDPTemplates\u003e\",\"\u003cData SheetTypes\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Create View Under the Cover Sheet View for Data Package Template With Cover Sheet \"\u003cDPTemplates\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Create View Under the Cover Sheet View for Data Package Template With Out Cover Sheet \"\u003cDPTemplates\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "DPTemplates",
        "Data SheetTypes"
      ]
    },
    {
      "cells": [
        "DPTemp-WithCoverSheet,DPTemp-WithOutCoverSheet",
        "Artwork,Food Label Data Sheet,Food Review,Routing,Spec Data Sheet,Test Run"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Creating data Package Templates under Data Packages",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@FoodSetupHeirarchy"
    },
    {
      "name": "@FoodTypes"
    },
    {
      "name": "@DataPackage"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Data packages",
  "keyword": "And "
});
formatter.match({
  "location": "FoodSetupSteps.Navigate_to_Data_packages()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create Data Package Templates \"DPTemp-WithCoverSheet,DPTemp-WithOutCoverSheet\"",
  "keyword": "And "
});
formatter.match({
  "location": "FoodSetupSteps.Create_Data_Package_Templates(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to the Data Packages with cover sheet and create the Data Sheets And Verify Edit Delete Copy Functionality \"DPTemp-WithCoverSheet,DPTemp-WithOutCoverSheet\",\"Artwork,Food Label Data Sheet,Food Review,Routing,Spec Data Sheet,Test Run\"",
  "keyword": "Then "
});
formatter.match({
  "location": "FoodSetupSteps.Go_to_the_Data_Packages_with_cover_sheet_and_create_the_Data_Sheets_And_Verify_Edit_Delete_Copy_Functionality(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to the Data Packages without cover sheet and create the Data Sheets \"DPTemp-WithCoverSheet,DPTemp-WithOutCoverSheet\",\"Artwork,Food Label Data Sheet,Food Review,Routing,Spec Data Sheet,Test Run\"",
  "keyword": "Then "
});
formatter.match({
  "location": "FoodSetupSteps.Go_to_the_Data_Packages_without_cover_sheet_and_create_the_Data_Sheets(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create View Under the Cover Sheet View for Data Package Template With Cover Sheet \"DPTemp-WithCoverSheet,DPTemp-WithOutCoverSheet\"",
  "keyword": "Then "
});
formatter.match({
  "location": "FoodSetupSteps.Create_View_Under_the_Cover_Sheet_View_for_Data_Package_Template_Witht_Cover_Sheet(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create View Under the Cover Sheet View for Data Package Template With Out Cover Sheet \"DPTemp-WithCoverSheet,DPTemp-WithOutCoverSheet\"",
  "keyword": "Then "
});
formatter.match({
  "location": "FoodSetupSteps.Create_View_Under_the_Cover_Sheet_View_for_Data_Package_Template_With_Out_Cover_Sheet(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Creating datas under created Food",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Artwork"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user create food type \"\u003cFood\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user create Artwork template and verify edit,copy and delete function",
  "keyword": "Then "
});
formatter.step({
  "name": "user create canvas template for artwork",
  "keyword": "And "
});
formatter.step({
  "name": "select template in foodtab and approves it",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Food"
      ]
    },
    {
      "cells": [
        "Fast Food, J-Noodles,Fruits and vegetables,Apple"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Creating datas under created Food",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@FoodSetupHeirarchy"
    },
    {
      "name": "@FoodTypes"
    },
    {
      "name": "@Artwork"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user create food type \"Fast Food, J-Noodles,Fruits and vegetables,Apple\"",
  "keyword": "And "
});
formatter.match({
  "location": "FoodSetupSteps.user_create_food_type(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user create Artwork template and verify edit,copy and delete function",
  "keyword": "Then "
});
formatter.match({
  "location": "FoodSetupSteps.user_create_Artwork_template_and_verify_edit_copy_and_delete_function()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user create canvas template for artwork",
  "keyword": "And "
});
formatter.match({
  "location": "FoodSetupSteps.user_create_canvas_template_for_artwork()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "select template in foodtab and approves it",
  "keyword": "And "
});
formatter.match({
  "location": "FoodSetupSteps.select_template_in_foodtab_and_approves_it()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "create routing template under Food",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Routing"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user create routing template and verify edit,copy and delete function",
  "keyword": "Then "
});
formatter.step({
  "name": "user create newsubrouting \"\u003cSubrouting\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user creates newcapability in subrouting",
  "keyword": "And "
});
formatter.step({
  "name": "user copy delete approve the capabilities and subrouting",
  "keyword": "Then "
});
formatter.step({
  "name": "user created Routing template tab \"\u003crouting template\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "goes to foodtab and select new from template \"\u003crouting template\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user copies approve revise and adandon",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Subrouting",
        "routing template"
      ]
    },
    {
      "cells": [
        "Subrouting001,CMT",
        "Div001"
      ]
    }
  ]
});
formatter.scenario({
  "name": "create routing template under Food",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@FoodSetupHeirarchy"
    },
    {
      "name": "@FoodTypes"
    },
    {
      "name": "@Routing"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user create routing template and verify edit,copy and delete function",
  "keyword": "Then "
});
formatter.match({
  "location": "FoodSetupSteps.user_create_routing_template_and_verify_edit_copy_and_delete_function()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user create newsubrouting \"Subrouting001,CMT\"",
  "keyword": "And "
});
formatter.match({
  "location": "FoodSetupSteps.user_create_newsubrouting(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates newcapability in subrouting",
  "keyword": "And "
});
formatter.match({
  "location": "FoodSetupSteps.user_creates_newcapability_in_subrouting()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user copy delete approve the capabilities and subrouting",
  "keyword": "Then "
});
formatter.match({
  "location": "FoodSetupSteps.user_copy_delete_approve_the_capabilities_and_subrouting()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user created Routing template tab \"Div001\"",
  "keyword": "And "
});
formatter.match({
  "location": "FoodSetupSteps.user_created_Routing_template_tab(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "goes to foodtab and select new from template \"Div001\"",
  "keyword": "And "
});
formatter.match({
  "location": "FoodSetupSteps.goes_to_foodtab_and_select_new_from_template(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user copies approve revise and adandon",
  "keyword": "Then "
});
formatter.match({
  "location": "FoodSetupSteps.user_copies_approve_revise_and_adandon()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Creating Food Label Data Sheet Templates",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@FoodLabelDataSheet"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to Food Label Data Sheet Templates",
  "keyword": "And "
});
formatter.step({
  "name": "Create Food Label Data Sheet Templates \"\u003cFoodLabelDataSheetTemplates\u003e\",\"\u003cFoodLabelDataSheetSubtype\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Go to the Food Label Data Sheet Template Node and Verify the Tab names \"\u003cFoodLabelDataSheetTemplates\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Go to the Ingredient Items tab and Create Ingredient Item and Verify the Main Ingredient Checkbox \"\u003cIngredientItemName\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Go to the Nutrient Items tab and Create Nutrient Items and Verify UOM DV and Amount Columns \"\u003cNutrientItemName\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "FoodLabelDataSheetTemplates",
        "FoodLabelDataSheetSubtype",
        "IngredientItemName",
        "NutrientItemName"
      ]
    },
    {
      "cells": [
        "FLDataSheet",
        "Food Additives, Colours and Flavors",
        "CornFlour",
        "WheyProtein"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Creating Food Label Data Sheet Templates",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@FoodSetupHeirarchy"
    },
    {
      "name": "@FoodTypes"
    },
    {
      "name": "@FoodLabelDataSheet"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Food Label Data Sheet Templates",
  "keyword": "And "
});
formatter.match({
  "location": "FoodSetupSteps.Navigate_to_Food_Label_Data_Sheet_Templates()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create Food Label Data Sheet Templates \"FLDataSheet\",\"Food Additives, Colours and Flavors\"",
  "keyword": "And "
});
formatter.match({
  "location": "FoodSetupSteps.Create_Food_Label_Data_Sheet_Templates(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to the Food Label Data Sheet Template Node and Verify the Tab names \"FLDataSheet\"",
  "keyword": "Then "
});
formatter.match({
  "location": "FoodSetupSteps.Go_to_the_Food_Label_Data_Sheet_Template_Node_and_Verify_the_Tab_names(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to the Ingredient Items tab and Create Ingredient Item and Verify the Main Ingredient Checkbox \"CornFlour\"",
  "keyword": "Then "
});
formatter.match({
  "location": "FoodSetupSteps.Go_to_the_Ingredient_Items_tab_and_Create_Ingredient_Item_and_Verify_the_Main_Ingredient_Checkbox(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to the Nutrient Items tab and Create Nutrient Items and Verify UOM DV and Amount Columns \"WheyProtein\"",
  "keyword": "Then "
});
formatter.match({
  "location": "FoodSetupSteps.Go_to_the_Nutrient_Items_tab_and_Create_Nutrient_Items_and_Verify_UOM_DV_and_Amount_Columns(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "create Document under Food",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@FoodDocument"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user create food types \"\u003cFood\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user create Document and verify approve function\"\u003cDocument\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Food",
        "Document"
      ]
    },
    {
      "cells": [
        "Pizza, veg pizza,Dog Food,pedigree,Fast Food,Noodles",
        "New Document,New Document1"
      ]
    }
  ]
});
formatter.scenario({
  "name": "create Document under Food",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@FoodSetupHeirarchy"
    },
    {
      "name": "@FoodTypes"
    },
    {
      "name": "@FoodDocument"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user create food types \"Pizza, veg pizza,Dog Food,pedigree,Fast Food,Noodles\"",
  "keyword": "And "
});
formatter.match({
  "location": "FoodSetupSteps.user_create_food_types(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user create Document and verify approve function\"New Document,New Document1\"",
  "keyword": "Then "
});
formatter.match({
  "location": "FoodSetupSteps.user_create_Document_and_verify_approve_delete_function(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "create GTIn under Food SKU",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@FoodSKUGTIN"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user create foodtype and competitivefood \"\u003cCompFoodName\u003e\",\"\u003cFoodNames\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User select Salesregion and create SKU \"\u003cSalesRegionName\u003e\",\"\u003cFoodSKU\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User Creates and Verify the GTIN Under Food SKU \"\u003cGTINForFoodSKU\u003e\",\"\u003cFoodSKU\u003e\",\"\u003cManufacturerName\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "ManufacturerName",
        "ManufacturerCode",
        "SalesRegionName",
        "GTINForFoodSKU",
        "FoodNames",
        "CompFoodName",
        "FoodSKU"
      ]
    },
    {
      "cells": [
        "JubilientFoods",
        "12345",
        "India-South",
        "12345",
        "Pizza, FoodForGTINVerf",
        "CompFoodName,200,CompetitiveFood",
        "FoodSKU"
      ]
    }
  ]
});
formatter.scenario({
  "name": "create GTIn under Food SKU",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@FoodSetupHeirarchy"
    },
    {
      "name": "@FoodTypes"
    },
    {
      "name": "@FoodSKUGTIN"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user create foodtype and competitivefood \"CompFoodName,200,CompetitiveFood\",\"Pizza, FoodForGTINVerf\"",
  "keyword": "And "
});
formatter.match({
  "location": "FoodSetupSteps.user_create_foodtype_and_competitivefood(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User select Salesregion and create SKU \"India-South\",\"FoodSKU\"",
  "keyword": "Then "
});
formatter.match({
  "location": "FoodSetupSteps.User_select_Salesregion_and_create_SKU(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Creates and Verify the GTIN Under Food SKU \"12345\",\"FoodSKU\",\"JubilientFoods\"",
  "keyword": "And "
});
formatter.match({
  "location": "FoodSetupSteps.User_Creates_and_Verify_the_GTIN_Under_Food_SKU(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "create BOM under style",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@FoodBOM"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user create new food under food placement in style\"\u003cBOM\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user create BOM under food placement in style\"\u003cFood\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "BOM",
        "Food"
      ]
    },
    {
      "cells": [
        "Apparel BOM,Food BOM",
        "New Food,veg pizza"
      ]
    }
  ]
});
formatter.scenario({
  "name": "create BOM under style",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@FoodSetupHeirarchy"
    },
    {
      "name": "@FoodTypes"
    },
    {
      "name": "@FoodBOM"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user create new food under food placement in style\"Apparel BOM,Food BOM\"",
  "keyword": "And "
});
formatter.match({
  "location": "FoodSetupSteps.user_create_food_under_food_placement_in_style(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user create BOM under food placement in style\"New Food,veg pizza\"",
  "keyword": "Then "
});
formatter.match({
  "location": "FoodSetupSteps.user_create_BOM_under_food_placement_in_style(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.logout_from_the_Application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenario({
  "name": "create BOM under style",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@FoodSetupHeirarchy"
    },
    {
      "name": "@FoodTypes"
    },
    {
      "name": "@Food_Data_Package_PDF"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User select food data package templates",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "User generating food data package pdf",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/R_MerchandisingSetup.feature");
formatter.feature({
  "name": "Creating Merchandising Setup",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Merchandise"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Create Secondary type",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@CreateSecondaryType"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to Secondary Type tab create data \"\u003cSecondaryType\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to Option Type and Product type create data \"\u003cOptionType\u003e\",\"\u003cProductType\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to Collection type and create data \"\u003cCollectionType\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to Folder type \"\u003cFolderType\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "Create plan type data \"\u003cPlanType\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Enumeration creation",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to Business Object",
  "keyword": "And "
});
formatter.step({
  "name": "Create style attribute for merchandise product version",
  "keyword": "Then "
});
formatter.step({
  "name": "Navigate to style push template and add mapping expression",
  "keyword": "And "
});
formatter.step({
  "name": "Create style copy templates",
  "keyword": "And "
});
formatter.step({
  "name": "User select Style from Business Object \"Style\"",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to style create atrributes",
  "keyword": "And "
});
formatter.step({
  "name": "Again navigate to copy template and create new style copy template",
  "keyword": "And "
});
formatter.step({
  "name": "User click on update cnfiguration",
  "keyword": "And "
});
formatter.step({
  "name": "Create Merchandise copy template",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "SecondaryType",
        "OptionType",
        "ProductType",
        "CollectionType",
        "FolderType",
        "PlanType"
      ]
    },
    {
      "cells": [
        "Segment,Monthly",
        "Option",
        "Product",
        "Product Group",
        "Department,Gender,Region",
        "Merch Plan"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Create Secondary type",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Merchandise"
    },
    {
      "name": "@CreateSecondaryType"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.user_Click_on_setup_icon()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Secondary Type tab create data \"Segment,Monthly\"",
  "keyword": "And "
});
formatter.match({
  "location": "MerchandisingSteps.navigate_to_Secondary_Type_tab_create_data(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Option Type and Product type create data \"Option\",\"Product\"",
  "keyword": "And "
});
formatter.match({
  "location": "MerchandisingSteps.navigate_to_Option_Type_and_Product_type_create_data(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Collection type and create data \"Product Group\"",
  "keyword": "And "
});
formatter.match({
  "location": "MerchandisingSteps.navigate_to_Collection_type_and_create_data(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Folder type \"Department,Gender,Region\"",
  "keyword": "When "
});
formatter.match({
  "location": "MerchandisingSteps.navigate_to_Folder_type(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create plan type data \"Merch Plan\"",
  "keyword": "And "
});
formatter.match({
  "location": "MerchandisingSteps.create_plan_type_data(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Enumeration creation",
  "keyword": "And "
});
formatter.match({
  "location": "MerchandisingSteps.enumeration_creation()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Business Object",
  "keyword": "And "
});
formatter.match({
  "location": "MerchandisingSteps.navigate_to_Business_Object()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create style attribute for merchandise product version",
  "keyword": "Then "
});
formatter.match({
  "location": "MerchandisingSteps.create_style_attribute_for_merchandise_product_version()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to style push template and add mapping expression",
  "keyword": "And "
});
formatter.match({
  "location": "MerchandisingSteps.navigate_to_style_push_template_and_add_mapping_expression()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create style copy templates",
  "keyword": "And "
});
formatter.match({
  "location": "MerchandisingSteps.create_style_copy_templates()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User select Style from Business Object \"Style\"",
  "keyword": "And "
});
formatter.match({
  "location": "BusinessPlanningSteps.user_select_Style_from_Business_Object(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to style create atrributes",
  "keyword": "And "
});
formatter.match({
  "location": "MerchandisingSteps.navigate_to_style_create_atrributes()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Again navigate to copy template and create new style copy template",
  "keyword": "And "
});
formatter.match({
  "location": "MerchandisingSteps.again_navigate_to_copy_template_and_create_new_style_copy_template()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User click on update cnfiguration",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.user_click_on_update_cnfiguration()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create Merchandise copy template",
  "keyword": "And "
});
formatter.match({
  "location": "MerchandisingSteps.create_Merchandise_copy_template()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Setup for hierarchy creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@HierarchySetup"
    }
  ]
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to Folder type to check Brand",
  "keyword": "And "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "Navigate to Home Page and setup template for hierarchy \"\u003cSeasonName1\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "Create new season choose existing hierarchy value \"\u003cSeasonName2\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "SeasonName1",
        "SeasonName2"
      ]
    },
    {
      "cells": [
        "Summer 2021",
        "Summer 2022"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Setup for hierarchy creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Merchandise"
    },
    {
      "name": "@HierarchySetup"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User Click on setup icon",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.user_Click_on_setup_icon()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Folder type to check Brand",
  "keyword": "And "
});
formatter.match({
  "location": "MerchandisingSteps.navigate_to_Folder_type_to_check_Brand()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Home Page and setup template for hierarchy \"Summer 2021\"",
  "keyword": "And "
});
formatter.match({
  "location": "MerchandisingSteps.navigate_to_Home_Page_and_setup_template_for_hierarchy(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create new season choose existing hierarchy value \"Summer 2022\"",
  "keyword": "And "
});
formatter.match({
  "location": "MerchandisingSteps.create_new_season_choose_existing_hierarchy_value(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Brand Creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@BrandCreation"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user creates brand under Heirarchy Template \"\u003cBrandValue\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Click New Brand",
  "keyword": "Then "
});
formatter.step({
  "name": "create Brand for merchandise \"\u003cBrandValue\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "BrandValue"
      ]
    },
    {
      "cells": [
        "Brand X,Brand Y,Childrenswear,Tops"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Brand Creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Merchandise"
    },
    {
      "name": "@BrandCreation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates brand under Heirarchy Template \"Brand X,Brand Y,Childrenswear,Tops\"",
  "keyword": "Then "
});
formatter.match({
  "location": "MerchandisingSteps.user_creates_brand_under_Heirarchy_Template(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Click New Brand",
  "keyword": "Then "
});
formatter.match({
  "location": "StylePageSteps.click_New_Brand()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "create Brand for merchandise \"Brand X,Brand Y,Childrenswear,Tops\"",
  "keyword": "And "
});
formatter.match({
  "location": "MerchandisingSteps.create_Brand_for_merchandise(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Create Plan in Merchandise",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@MerhcnadisePlan"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user creates new Plan \"\u003cPlanName\u003e\",\"\u003cseasonname\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "user creates season under version \"\u003cseasonname\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "under secondarysetup created monthly and segment setup \"\u003cmonth\u003e\",\"\u003csegment\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "user selects monthly and segment value under Secondary",
  "keyword": "Then "
});
formatter.step({
  "name": "user click PlanTab, adds created monthly",
  "keyword": "Then "
});
formatter.step({
  "name": "user moves to created season",
  "keyword": "Then "
});
formatter.step({
  "name": "user selects monthly and segment value under Secondary for summer2022",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "PlanName",
        "seasonname",
        "month",
        "segment"
      ]
    },
    {
      "cells": [
        "Plan 1",
        "Summer 2021,Summer 2022",
        "monthly1,monthly2",
        "segment1,segment2"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Create Plan in Merchandise",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Merchandise"
    },
    {
      "name": "@MerhcnadisePlan"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates new Plan \"Plan 1\",\"Summer 2021,Summer 2022\"",
  "keyword": "Then "
});
formatter.match({
  "location": "MerchandisingSteps.user_creates_new_Plan(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user creates season under version \"Summer 2021,Summer 2022\"",
  "keyword": "Then "
});
formatter.match({
  "location": "MerchandisingSteps.user_creates_season_under_version(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "under secondarysetup created monthly and segment setup \"monthly1,monthly2\",\"segment1,segment2\"",
  "keyword": "And "
});
formatter.match({
  "location": "MerchandisingSteps.under_secondarysetup_created_monthly_and_segment_setup(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user selects monthly and segment value under Secondary",
  "keyword": "Then "
});
formatter.match({
  "location": "MerchandisingSteps.user_selects_monthly_and_segment_value_under_Secondary()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user click PlanTab, adds created monthly",
  "keyword": "Then "
});
formatter.match({
  "location": "MerchandisingSteps.user_click_PlanTab_adds_created_monthly()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user moves to created season",
  "keyword": "Then "
});
formatter.match({
  "location": "MerchandisingSteps.user_moves_to_created_season()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user selects monthly and segment value under Secondary for summer2022",
  "keyword": "And "
});
formatter.match({
  "location": "MerchandisingSteps.user_selects_monthly_and_segment_value_under_Secondary_for_summer2022()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Folder creation and validation in merchandise",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@MerchandiseSeason"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user navigates into created season \"\u003cseason\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "create newfolder under Plan \"\u003cfoldername\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "create NewProducts and verify the attribute value of quantity created \"\u003cproductvalue\u003e\",\"\u003coptionNamevalue\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "user copy, delete and edit the created product \"\u003ccopygreenvalue\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Navigate to Product tab and validate Tops available or not",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "season",
        "foldername",
        "productvalue",
        "optionNamevalue",
        "copygreenvalue"
      ]
    },
    {
      "cells": [
        "Summer 2021",
        "Americas,Childrenswear,Boys,Tops",
        "Tops-01,Tops-02,Tops-03",
        "Option,Blue,Green",
        "Greenshade"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Folder creation and validation in merchandise",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Merchandise"
    },
    {
      "name": "@MerchandiseSeason"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user navigates into created season \"Summer 2021\"",
  "keyword": "Then "
});
formatter.match({
  "location": "MerchandisingSteps.user_navigates_into_created_season(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "create newfolder under Plan \"Americas,Childrenswear,Boys,Tops\"",
  "keyword": "And "
});
formatter.match({
  "location": "MerchandisingSteps.create_newfolder_under_Plan(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "create NewProducts and verify the attribute value of quantity created \"Tops-01,Tops-02,Tops-03\",\"Option,Blue,Green\"",
  "keyword": "Then "
});
formatter.match({
  "location": "MerchandisingSteps.create_NewProducts_and_verify_the_attribute_value_of_quantity_created(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user copy, delete and edit the created product \"Greenshade\"",
  "keyword": "Then "
});
formatter.match({
  "location": "MerchandisingSteps.user_copy_delete_and_edit_the_created_product(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Product tab and validate Tops available or not",
  "keyword": "Then "
});
formatter.match({
  "location": "MerchandisingSteps.navigate_to_Product_tab_and_validate_Tops_available_or_not()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Product Creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@ProductCreation"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user navigate to merch collection create product\"\u003cProduct\u003e\"\"\u003cSeason\u003e\"\"\u003cBrand\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "create product with candidate\"\u003cProduct1\u003e\"\"\u003cSeason\u003e\"\"\u003cBrand\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "create product for summer2022 with new from style \"\u003cProduct\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "create product for summer with new from product and carryover",
  "keyword": "Then "
});
formatter.step({
  "name": "Pushing the style attribute price point and gender",
  "keyword": "Then "
});
formatter.step({
  "name": "create product for summer with carryover style",
  "keyword": "Then "
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Product",
        "Season",
        "Brand",
        "Product1"
      ]
    },
    {
      "cells": [
        "Round Neck T-Shirt",
        "Summer 2021",
        "Brand X",
        "RNT1"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Product Creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Merchandise"
    },
    {
      "name": "@ProductCreation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user navigate to merch collection create product\"Round Neck T-Shirt\"\"Summer 2021\"\"Brand X\"",
  "keyword": "Then "
});
formatter.match({
  "location": "MerchandisingSteps.user_navigate_to_merch_collection_create_product(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "create product with candidate\"RNT1\"\"Summer 2021\"\"Brand X\"",
  "keyword": "Then "
});
formatter.match({
  "location": "MerchandisingSteps.create_product_with_candidate(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "create product for summer2022 with new from style \"Round Neck T-Shirt\"",
  "keyword": "Then "
});
formatter.match({
  "location": "MerchandisingSteps.create_product_for_summer_with_new_from_style(Integer,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "create product for summer with new from product and carryover",
  "keyword": "Then "
});
formatter.match({
  "location": "MerchandisingSteps.create_product_for_summer_with_new_from_product_and_carryover()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Pushing the style attribute price point and gender",
  "keyword": "Then "
});
formatter.match({
  "location": "MerchandisingSteps.pushing_the_style_attribute_price_point_and_gender()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "create product for summer with carryover style",
  "keyword": "Then "
});
formatter.match({
  "location": "MerchandisingSteps.create_product_for_summer_with_carryover_style()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.logout_from_the_Application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/R_Project.feature");
formatter.feature({
  "name": "Project",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Projects"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Project creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@ProjectCreation"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "User creates Projects with different Project Types \"\u003cProjectType\u003e\",\"\u003cProjectNames\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Verify Edit And Delete Options \"\u003cProjectNames\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Verify the Tabs Under Each Project Type \"\u003cProjectNames\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "ProjectType",
        "ProjectNames"
      ]
    },
    {
      "cells": [
        "Project-ALL,Project-Style,Project-Food,Project-Material,Project-Blank,Project-All",
        "Project-With All ProductTypes Selected,Project With Style As ProductType,Project With Food As Product Type,Project With Material As Product Type,Project With No Product Type Selected,ProjectForDeletion"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Project creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Projects"
    },
    {
      "name": "@ProjectCreation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User creates Projects with different Project Types \"Project-ALL,Project-Style,Project-Food,Project-Material,Project-Blank,Project-All\",\"Project-With All ProductTypes Selected,Project With Style As ProductType,Project With Food As Product Type,Project With Material As Product Type,Project With No Product Type Selected,ProjectForDeletion\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProjectSteps.User_creates_Projects_with_different_Project_Types(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Verify Edit And Delete Options \"Project-With All ProductTypes Selected,Project With Style As ProductType,Project With Food As Product Type,Project With Material As Product Type,Project With No Product Type Selected,ProjectForDeletion\"",
  "keyword": "And "
});
formatter.match({
  "location": "ProjectSteps.Verify_Edit_And_Delete_Options(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Verify the Tabs Under Each Project Type \"Project-With All ProductTypes Selected,Project With Style As ProductType,Project With Food As Product Type,Project With Material As Product Type,Project With No Product Type Selected,ProjectForDeletion\"",
  "keyword": "And "
});
formatter.match({
  "location": "ProjectSteps.Verify_the_Tabs_Under_Each_Project_Type(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenario({
  "name": "",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Projects"
    },
    {
      "name": "@WBS_Validation"
    },
    {
      "name": "@ProjectType"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User select and start wbs projects in schedule",
  "keyword": "When "
});
formatter.match({
  "location": "ProjectSteps.user_select_and_start_wbs_projects_in_schedule()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User verify gantt tab is apperaed",
  "keyword": "And "
});
formatter.match({
  "location": "ProjectSteps.user_verify_gantt_tab_is_apperaed()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User validates milestone and deliever reports in project",
  "keyword": "Then "
});
formatter.match({
  "location": "ProjectSteps.user_validates_milestone_and_deliever_reports_in_project()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenario({
  "name": "",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Projects"
    },
    {
      "name": "@ProjectAllSKUColorCreation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user select MaterialInprojectType modify manageview and select colorways and sku",
  "keyword": "Then "
});
formatter.match({
  "location": "ProjectSteps.user_select_MaterialInprojectType_modify_manageview_and_select_colorways_and_sku()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user select StyleInprojectType modify manageview and select colorways and sku",
  "keyword": "Then "
});
formatter.match({
  "location": "ProjectSteps.user_select_StyleInprojectType_modify_manageview_and_select_colorways_and_sku()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user select Project SKU and project color for MaterialInprojectType and StyleInprojectType",
  "keyword": "Then "
});
formatter.match({
  "location": "ProjectSteps.user_select_Project_SKU_and_project_color_for_MaterialInprojectType_and_StyleInprojectType()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user clicks Allproject under project and setup manageviews",
  "keyword": "Then "
});
formatter.match({
  "location": "ProjectSteps.user_clicks_Allproject_under_project_and_setup_manageviews()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user copies Material and style in AllProject",
  "keyword": "Then "
});
formatter.match({
  "location": "ProjectSteps.user_copies_Material_and_style_in_AllProject()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@ProjectAllDocumentshare"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user clicks Allproject under project and navigates to DocumentTab  \"\u003cDocName\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "user share and validate the document \"\u003cProjectName\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "user Navigates into user management under configuration \"\u003cSharedTeam\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "user moves into AllProject and Manage Team",
  "keyword": "Then "
});
formatter.step({
  "name": "user verify the summary in Presentation Tab",
  "keyword": "Then "
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "DocName",
        "ProjectName",
        "SharedTeam"
      ]
    },
    {
      "cells": [
        "All Project Document",
        "Material,All Product",
        "Project Shared Team,Project"
      ]
    }
  ]
});
formatter.scenario({
  "name": "",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Projects"
    },
    {
      "name": "@ProjectAllDocumentshare"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user clicks Allproject under project and navigates to DocumentTab  \"All Project Document\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProjectSteps.user_clicks_Allproject_under_project_and_navigates_to_DocumentTab(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user share and validate the document \"Material,All Product\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProjectSteps.user_share_and_validate_the_document(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user Navigates into user management under configuration \"Project Shared Team,Project\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProjectSteps.user_Navigates_into_user_management_under_configuration(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user moves into AllProject and Manage Team",
  "keyword": "Then "
});
formatter.match({
  "location": "ProjectSteps.user_moves_into_AllProject_and_Manage_Team()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user verify the summary in Presentation Tab",
  "keyword": "Then "
});
formatter.match({
  "location": "ProjectSteps.user_verify_the_summary_in_Presentation_Tab()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.logout_from_the_Application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/S_HybridAgent.feature");
formatter.feature({
  "name": "Creation of Hybrid Agent",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@HybridAgent"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Create Roles, Assign supplier user, update home supplier",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@RolesCreation"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "Update the suppliers",
  "keyword": "Then "
});
formatter.step({
  "name": "User click on setup icon and roles under user management",
  "keyword": "And "
});
formatter.step({
  "name": "Update user assignment for Global users \"\u003cSupplier\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Update Local User Security Roles",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Supplier"
      ]
    },
    {
      "cells": [
        "h1,i1"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Create Roles, Assign supplier user, update home supplier",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@HybridAgent"
    },
    {
      "name": "@RolesCreation"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Update the suppliers",
  "keyword": "Then "
});
formatter.match({
  "location": "HybridAgentSteps.update_the_suppliers()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User click on setup icon and roles under user management",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.user_click_on_setup_icon_and_roles_under_user_management()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Update user assignment for Global users \"h1,i1\"",
  "keyword": "Then "
});
formatter.match({
  "location": "HybridAgentSteps.update_user_assignment_for_Global_users(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Update Local User Security Roles",
  "keyword": "And "
});
formatter.match({
  "location": "HybridAgentSteps.update_Local_User_Security_Roles()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Create Data for portal use case",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@PortalUseCases"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "Create HTS Code \"\u003cHTSvalue\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Create supplier quotes templates and data package template \"\u003cSupQuoTemp\u003e\",\"\u003cDPtemp\u003e\",\"\u003cDatasheettype\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "create supplier request template \"\u003cSamptype\u003e\",\"\u003cDPtemp\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Test Data to Create Material Security Group and Assign Context Security Role For Material Security Group \"\u003cMSGMatr\u003e\",\"\u003cMSGUsers\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Test Data to Create Materials Under MSG-Fig  and MSG-Grape \"\u003cMaterialsForMSGFig\u003e\",\"\u003cMaterialsForMSGGrape\u003e\",\"\u003cMSGMatr\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "HTSvalue",
        "SupQuoTemp",
        "DPtemp",
        "Datasheettype",
        "Samptype",
        "MSGMatr",
        "MSGUsers",
        "MaterialsForMSGFig",
        "MaterialsForMSGGrape"
      ]
    },
    {
      "cells": [
        "HTSCode1,HTSCode2",
        "SupQteTmp1,SupQteTmp2",
        "Style,Material",
        "Artwork,Routing",
        "Style - Proto,Material - Proto",
        "MSG1-Fig_Materials,MSG2-Grape_Materials",
        "f1,g1",
        "Mat 3 Fig,Mat 4 Fig",
        "Mat 5 Grape,Mat 6 Grape"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Create Data for portal use case",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@HybridAgent"
    },
    {
      "name": "@PortalUseCases"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create HTS Code \"HTSCode1,HTSCode2\"",
  "keyword": "And "
});
formatter.match({
  "location": "HybridAgentSteps.create_HTS_Code(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create supplier quotes templates and data package template \"SupQteTmp1,SupQteTmp2\",\"Style,Material\",\"Artwork,Routing\"",
  "keyword": "And "
});
formatter.match({
  "location": "HybridAgentSteps.create_supplier_quotes_templates_and_data_package_template(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "create supplier request template \"Style - Proto,Material - Proto\",\"Style,Material\"",
  "keyword": "And "
});
formatter.match({
  "location": "HybridAgentSteps.create_supplier_request_template(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Test Data to Create Material Security Group and Assign Context Security Role For Material Security Group \"MSG1-Fig_Materials,MSG2-Grape_Materials\",\"f1,g1\"",
  "keyword": "And "
});
formatter.match({
  "location": "HybridAgentSteps.test_Data_to_Create_Material_Security_Group_and_Assign_Context_Security_Role_For_Material_Security_Group(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Test Data to Create Materials Under MSG-Fig  and MSG-Grape \"Mat 3 Fig,Mat 4 Fig\",\"Mat 5 Grape,Mat 6 Grape\",\"MSG1-Fig_Materials,MSG2-Grape_Materials\"",
  "keyword": "And "
});
formatter.match({
  "location": "HybridAgentSteps.test_Data_to_Create_Materials_Under_MSG_Fig_and_MSG_Grape(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Verify Tab Level fot h1 and i1 user",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@VerifyTablevel"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "user verify the tabs for the user \"\u003cuserdata\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "user",
        "userdata"
      ]
    },
    {
      "cells": [
        "h1",
        "My Home,Styles,Materials,Projects,Supplier POs,Supplier"
      ]
    },
    {
      "cells": [
        "i1",
        "My Home,Styles,Materials,Projects,Supplier POs,Supplier"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Verify Tab Level fot h1 and i1 user",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@HybridAgent"
    },
    {
      "name": "@VerifyTablevel"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user verify the tabs for the user \"My Home,Styles,Materials,Projects,Supplier POs,Supplier\"",
  "keyword": "Then "
});
formatter.match({
  "location": "HybridAgentSteps.user_verify_the_tabs_for_the_user(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenario({
  "name": "Verify Tab Level fot h1 and i1 user",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@HybridAgent"
    },
    {
      "name": "@VerifyTablevel"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user verify the tabs for the user \"My Home,Styles,Materials,Projects,Supplier POs,Supplier\"",
  "keyword": "Then "
});
formatter.match({
  "location": "HybridAgentSteps.user_verify_the_tabs_for_the_user(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Verify Tab Level fot h1 and i1 user",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@VerifysubTablevel"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "user verify the specific tab and sub tab for the user \"\u003cuserdata\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "user",
        "userdata"
      ]
    },
    {
      "cells": [
        "f1",
        "My Home,Style,Material,Shape and Theme,Food,Sourcing,Projects,Documents,Issues,Reports,Select Sets,Business Planning,Quality,Inspection,Schedule,Engineering Change,Product Presentations,Collection Management,Merchandising"
      ]
    },
    {
      "cells": [
        "g1",
        "My Home,Style,Material,Shape and Theme,Food,Sourcing,Projects,Documents,Issues,Reports,Select Sets,Business Planning,Quality,Inspection,Schedule,Engineering Change,Product Presentations,Collection Management,Merchandising"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Verify Tab Level fot h1 and i1 user",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@HybridAgent"
    },
    {
      "name": "@VerifysubTablevel"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user verify the specific tab and sub tab for the user \"My Home,Style,Material,Shape and Theme,Food,Sourcing,Projects,Documents,Issues,Reports,Select Sets,Business Planning,Quality,Inspection,Schedule,Engineering Change,Product Presentations,Collection Management,Merchandising\"",
  "keyword": "Then "
});
formatter.match({
  "location": "HybridAgentSteps.user_verify_the_specific_tab_and_sub_tab_for_the_user(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenario({
  "name": "Verify Tab Level fot h1 and i1 user",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@HybridAgent"
    },
    {
      "name": "@VerifysubTablevel"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user verify the specific tab and sub tab for the user \"My Home,Style,Material,Shape and Theme,Food,Sourcing,Projects,Documents,Issues,Reports,Select Sets,Business Planning,Quality,Inspection,Schedule,Engineering Change,Product Presentations,Collection Management,Merchandising\"",
  "keyword": "Then "
});
formatter.match({
  "location": "HybridAgentSteps.user_verify_the_specific_tab_and_sub_tab_for_the_user(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Create Style supplier request as host user",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@StyleSupplierReqHost"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "Create Style Supplier Request for h1 users\"\u003cSupplierRequest\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Create Style Supplier Quotes and Issued for h1 user\"\u003cSupplierQuotes\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Create Style Supplier Request for i1 user\"\u003cSupplierRequest\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Create Style Supplier Quotes and Issued for i1 user\"\u003cSupplierQuotes\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "SupplierRequest",
        "SupplierQuotes"
      ]
    },
    {
      "cells": [
        "AUT_SR_001,AUT_SR_003",
        "AUT_SQ_SR1_001,AUT_SQ_SR3_001"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Create Style supplier request as host user",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@HybridAgent"
    },
    {
      "name": "@StyleSupplierReqHost"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create Style Supplier Request for h1 users\"AUT_SR_001,AUT_SR_003\"",
  "keyword": "Then "
});
formatter.match({
  "location": "HybridAgentSteps.create_Style_Supplier_Request_for_h_users(Integer,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create Style Supplier Quotes and Issued for h1 user\"AUT_SQ_SR1_001,AUT_SQ_SR3_001\"",
  "keyword": "Then "
});
formatter.match({
  "location": "HybridAgentSteps.create_Style_Supplier_Quotes_and_Issued_for_h_user(Integer,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create Style Supplier Request for i1 user\"AUT_SR_001,AUT_SR_003\"",
  "keyword": "And "
});
formatter.match({
  "location": "HybridAgentSteps.create_Style_Supplier_Request_for_i_user(Integer,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create Style Supplier Quotes and Issued for i1 user\"AUT_SQ_SR1_001,AUT_SQ_SR3_001\"",
  "keyword": "And "
});
formatter.match({
  "location": "HybridAgentSteps.create_Style_Supplier_Quotes_and_Issued_for_i_user(Integer,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Create Material supplier request as host user",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@MaterialSupplierReqhost"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "Create Material Supplier Request for h1 user\"\u003cSupplierRequest\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Creat Material Supplier Quotes and Issue for h1 user\"\u003cSupplierQuotes\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Create Material Supplier Request for i1 user\"\u003cSupplierRequest\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Create Material Supplier Quotes and issued for i1 User\"\u003cSupplierQuotes\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "SupplierRequest",
        "SupplierQuotes"
      ]
    },
    {
      "cells": [
        "AUT_MSR_001,AUT_MSR_003",
        "AUT_MSQ_SR1_001,AUT_MSQ_SR3_001"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Create Material supplier request as host user",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@HybridAgent"
    },
    {
      "name": "@MaterialSupplierReqhost"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create Material Supplier Request for h1 user\"AUT_MSR_001,AUT_MSR_003\"",
  "keyword": "Then "
});
formatter.match({
  "location": "HybridAgentSteps.create_Material_Supplier_Request_for_h_user(Integer,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Creat Material Supplier Quotes and Issue for h1 user\"AUT_MSQ_SR1_001,AUT_MSQ_SR3_001\"",
  "keyword": "Then "
});
formatter.match({
  "location": "HybridAgentSteps.creat_Material_Supplier_Quotes_and_Issue_for_h_user(Integer,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create Material Supplier Request for i1 user\"AUT_MSR_001,AUT_MSR_003\"",
  "keyword": "And "
});
formatter.match({
  "location": "HybridAgentSteps.create_Material_Supplier_Request_for_i_user(Integer,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create Material Supplier Quotes and issued for i1 User\"AUT_MSQ_SR1_001,AUT_MSQ_SR3_001\"",
  "keyword": "And "
});
formatter.match({
  "location": "HybridAgentSteps.create_Material_Supplier_Quotes_and_issued_for_i_User(Integer,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Verify SR and Style,Material products as supplier user",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@VerifySRUser"
    }
  ]
});
formatter.step({
  "name": "Login as h1 User and Verify Supplier Request Name, Type and State for Style\"\u003cSupplierRequest\u003e\",\"\u003cSupplierQuotes\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User launches centric application for local user \"\u003cuser1\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Login as i1 User and Verify Supplier Request Name, Type and State for Style\"\u003cSupplierRequest\u003e\",\"\u003cSupplierQuotes\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User launches centric application for local user \"\u003cuser\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Login as h1 User and Verify Supplier Request Name, Type and State for Material\"\u003cSupplierRequest\u003e\",\"\u003cSupplierQuotes\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "User launches centric application for local user \"\u003cuser1\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Login as i1 User and Verify Supplier Request Name, Type and State for Material\"\u003cSupplierRequest\u003e\",\"\u003cSupplierQuotes\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "SupplierRequest",
        "SupplierQuotes",
        "user",
        "user1"
      ]
    },
    {
      "cells": [
        "AUT_SR_001,AUT_SR_003,AUT_MSR_001,AUT_MSR_003",
        "AUT_SQ_SR1_001,AUT_SQ_SR3_001,AUT_MSQ_SR1_001,AUT_MSQ_SR3_001",
        "h1",
        "i1"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Verify SR and Style,Material products as supplier user",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@HybridAgent"
    },
    {
      "name": "@VerifySRUser"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Login as h1 User and Verify Supplier Request Name, Type and State for Style\"AUT_SR_001,AUT_SR_003,AUT_MSR_001,AUT_MSR_003\",\"AUT_SQ_SR1_001,AUT_SQ_SR3_001,AUT_MSQ_SR1_001,AUT_MSQ_SR3_001\"",
  "keyword": "Then "
});
formatter.match({
  "location": "HybridAgentSteps.login_as_h_User_and_Verify_Supplier_Request_Name_Type_and_State_for_Style(Integer,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application for local user \"i1\"",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application_for_local_user(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Login as i1 User and Verify Supplier Request Name, Type and State for Style\"AUT_SR_001,AUT_SR_003,AUT_MSR_001,AUT_MSR_003\",\"AUT_SQ_SR1_001,AUT_SQ_SR3_001,AUT_MSQ_SR1_001,AUT_MSQ_SR3_001\"",
  "keyword": "Then "
});
formatter.match({
  "location": "HybridAgentSteps.login_as_i_User_and_Verify_Supplier_Request_Name_Type_and_State_for_Style(Integer,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application for local user \"h1\"",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application_for_local_user(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Login as h1 User and Verify Supplier Request Name, Type and State for Material\"AUT_SR_001,AUT_SR_003,AUT_MSR_001,AUT_MSR_003\",\"AUT_SQ_SR1_001,AUT_SQ_SR3_001,AUT_MSQ_SR1_001,AUT_MSQ_SR3_001\"",
  "keyword": "Then "
});
formatter.match({
  "location": "HybridAgentSteps.login_as_h_User_and_Verify_Supplier_Request_Name_Type_and_State_for_Material(Integer,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application for local user \"i1\"",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application_for_local_user(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Login as i1 User and Verify Supplier Request Name, Type and State for Material\"AUT_SR_001,AUT_SR_003,AUT_MSR_001,AUT_MSR_003\",\"AUT_SQ_SR1_001,AUT_SQ_SR3_001,AUT_MSQ_SR1_001,AUT_MSQ_SR3_001\"",
  "keyword": "Then "
});
formatter.match({
  "location": "HybridAgentSteps.login_as_i_User_and_Verify_Supplier_Request_Name_Type_and_State_for_Material(Integer,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "create supplier request for h1 and i1 user",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Stylesamplecreate"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user navigates into supplier request under soucring for  \"\u003cuser\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "user"
      ]
    },
    {
      "cells": [
        "AUT_HSSRS_001,AUT_ISRS_003"
      ]
    }
  ]
});
formatter.scenario({
  "name": "create supplier request for h1 and i1 user",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@HybridAgent"
    },
    {
      "name": "@Stylesamplecreate"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user navigates into supplier request under soucring for  \"AUT_HSSRS_001,AUT_ISRS_003\"",
  "keyword": "Then "
});
formatter.match({
  "location": "HybridAgentSteps.user_navigates_into_supplier_request_under_soucring_for(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Create Material Samples with Huckleberry and Iceplant user",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@MaterialSamplecreate\u0026VerifySamples"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "Create sample to material for the Huckleberry user\"\u003cSupplierRequest\u003e\",\"\u003cSupplierQuotes\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Create sample to material for the Iceplant user\"\u003cSupplierRequest\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User launches centric application for local user \"\u003cuser\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "name": "Login as Huckleberry and verify the sample data is visible for style  and material",
  "keyword": "And "
});
formatter.step({
  "name": "User launches centric application for local user \"\u003cuser1\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Login as Iceplant and verify the sample data is visible for style  and material tab",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "SupplierRequest",
        "user",
        "user1"
      ]
    },
    {
      "cells": [
        "AUT_MSR_001,AUT_MSR_003",
        "h1",
        "i1"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Create Material Samples with Huckleberry and Iceplant user",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@HybridAgent"
    },
    {
      "name": "@MaterialSamplecreate\u0026VerifySamples"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create sample to material for the Huckleberry user\"AUT_MSR_001,AUT_MSR_003\",\"\u003cSupplierQuotes\u003e\"",
  "keyword": "Then "
});
formatter.match({
  "location": "HybridAgentSteps.create_sample_to_material_for_the_Huckleberry_user(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create sample to material for the Iceplant user\"AUT_MSR_001,AUT_MSR_003\"",
  "keyword": "And "
});
formatter.match({
  "location": "HybridAgentSteps.create_sample_to_material_for_the_Iceplant_user(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application for local user \"h1\"",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application_for_local_user(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Login as Huckleberry and verify the sample data is visible for style  and material",
  "keyword": "And "
});
formatter.match({
  "location": "HybridAgentSteps.login_as_Huckleberry_and_verify_the_sample_data_is_visible_for_style_and_material()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application for local user \"i1\"",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application_for_local_user(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Login as Iceplant and verify the sample data is visible for style  and material tab",
  "keyword": "Then "
});
formatter.match({
  "location": "HybridAgentSteps.login_as_Iceplant_and_verify_the_sample_data_is_visible_for_style_and_material_tab()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Verify Supplier Request value in f1 and g1 user",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@StyleMaterialvalidationH1I1"
    }
  ]
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user navigates into styles and validate the values \"\u003cuser\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "user navigates into material and validate the supplier quotes \"\u003cuser\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "user"
      ]
    },
    {
      "cells": [
        "f1"
      ]
    },
    {
      "cells": [
        "g1"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Verify Supplier Request value in f1 and g1 user",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@HybridAgent"
    },
    {
      "name": "@StyleMaterialvalidationH1I1"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user navigates into styles and validate the values \"f1\"",
  "keyword": "Then "
});
formatter.match({
  "location": "HybridAgentSteps.user_navigates_into_styles_and_validate_the_values(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user navigates into material and validate the supplier quotes \"f1\"",
  "keyword": "Then "
});
formatter.match({
  "location": "HybridAgentSteps.user_navigates_into_material_and_validate_the_supplier_quotes(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenario({
  "name": "Verify Supplier Request value in f1 and g1 user",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@HybridAgent"
    },
    {
      "name": "@StyleMaterialvalidationH1I1"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user navigates into styles and validate the values \"g1\"",
  "keyword": "Then "
});
formatter.match({
  "location": "HybridAgentSteps.user_navigates_into_styles_and_validate_the_values(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user navigates into material and validate the supplier quotes \"g1\"",
  "keyword": "Then "
});
formatter.match({
  "location": "HybridAgentSteps.user_navigates_into_material_and_validate_the_supplier_quotes(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.uri("src/test/resources/Features/S_ProductCustomization.feature");
formatter.feature({
  "name": "Product Customization",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@ProductCustomizations"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Verifying the Maximum Quantity field",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to Existing Style \"\u003cStyleName\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Verify the Quantity field cannot take values more than Forty \"\u003cSupplier\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Verify Forty Samples are created \"\u003cSample\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "StyleName",
        "Supplier",
        "Sample"
      ]
    },
    {
      "cells": [
        "Apparel - Color and Size",
        "Supplier",
        "AUTSamp"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Verifying the Maximum Quantity field",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@ProductCustomizations"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Existing Style \"Apparel - Color and Size\"",
  "keyword": "And "
});
formatter.match({
  "location": "ProductCustomizationSteps.Navigate_to_Existing_Style(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Verify the Quantity field cannot take values more than Forty \"Supplier\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.Verify_the_Quantity_field_cannot_take_values_more_than_Forty(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Verify Forty Samples are created \"AUTSamp\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.Verify_Forty_Samples_are_created(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Add Comments Path",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to Existing Style \"\u003cStyleName\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Create Custom View in Style Properties",
  "keyword": "Then "
});
formatter.step({
  "name": "Navigate to style and add Recent comment from custom view",
  "keyword": "Then "
});
formatter.step({
  "name": "Navigate to SKU tab and Create two SKUs and custom view for SKU \"\u003cSKUName\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Create Recent comment for two SKUs \"\u003cStyleName\u003e\",\"\u003cSKUName\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Navigate to Style properties and check recent comments reflected in properties \"\u003cStyleName\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Delete the one comment SKU in style",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "StyleName",
        "Supplier",
        "Sample",
        "SKUName"
      ]
    },
    {
      "cells": [
        "Apparel - Color and Size",
        "Ship - Supplier",
        "AUTSamp",
        "StyleSKU1,StyleSKU2"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Add Comments Path",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@ProductCustomizations"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Existing Style \"Apparel - Color and Size\"",
  "keyword": "And "
});
formatter.match({
  "location": "ProductCustomizationSteps.Navigate_to_Existing_Style(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create Custom View in Style Properties",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.Create_Custom_View_in_Style_Properties()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to style and add Recent comment from custom view",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.Navigate_to_style_and_add_Recent_comment_from_custom_view()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to SKU tab and Create two SKUs and custom view for SKU \"StyleSKU1,StyleSKU2\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.Navigate_to_SKU_tab_and_Create_SKU_and_custom_view_for_SKU(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create Recent comment for two SKUs \"Apparel - Color and Size\",\"StyleSKU1,StyleSKU2\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.Create_Recent_comment_for_SKU(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Style properties and check recent comments reflected in properties \"Apparel - Color and Size\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.Navigate_to_Style_properties_and_check_recent_comments_reflected_in_properties(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Delete the one comment SKU in style",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.Delete_the_one_comment_SKU_in_style()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Hide Action",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Navigate to collection verify move from style action is visible as Admin \"\u003cCollection\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Navigate to Style and verify move from style action is visible as Admin \"\u003cStyle\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to Hierarchy and verify move styles from present in season brand and department \"\u003cSeason\u003e\",\"\u003cBrand\u003e\",\"\u003cDepartment\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "User click on setup icon and roles under user management",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to Role and assign User \"\u003cUser\u003e\",\"\u003cRole\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Logout Login as Non Admin \"\u003cUser\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to collection verify move from style action not visible as NonAdmin \"\u003cCollection\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Navigate to style and verify move from style action not visible as NonAdmin \"\u003cStyle\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Navigate to Hierarchy and verify move styles from not present in season, brand and department as NonAdmin \"\u003cSeason\u003e\",\"\u003cBrand\u003e\",\"\u003cDepartment\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Season",
        "Brand",
        "Department",
        "Collection",
        "Style",
        "Role",
        "User"
      ]
    },
    {
      "cells": [
        "winter season",
        "Denim",
        "Mens",
        "Jeans",
        "Apparel - Color and Size",
        "Non Admin",
        "NA"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Hide Action",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@ProductCustomizations"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to collection verify move from style action is visible as Admin \"Jeans\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.Navigate_to_collection_verify_move_from_style_action_is_visible_as_Admin(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Style and verify move from style action is visible as Admin \"Apparel - Color and Size\"",
  "keyword": "And "
});
formatter.match({
  "location": "ProductCustomizationSteps.Navigate_to_style_and_verify_move_from_style_action_is_visible_as_Admin(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Hierarchy and verify move styles from present in season brand and department \"winter season\",\"Denim\",\"Mens\"",
  "keyword": "And "
});
formatter.match({
  "location": "ProductCustomizationSteps.Navigate_to_Hierarchy_and_verify_move_styles_from_present_in_season_brand_and_department(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User click on setup icon and roles under user management",
  "keyword": "And "
});
formatter.match({
  "location": "HomePageSteps.user_click_on_setup_icon_and_roles_under_user_management()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Role and assign User \"NA\",\"Non Admin\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.Navigate_to_Role_and_assign_User(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout Login as Non Admin \"NA\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.Login_as_Non_Admin(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to collection verify move from style action not visible as NonAdmin \"Jeans\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.Navigate_to_collection_verify_move_from_style_action_not_visible_as_NonAdmin(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to style and verify move from style action not visible as NonAdmin \"Apparel - Color and Size\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.Navigate_to_style_and_verify_move_from_style_action_not_visible_as_NonAdmin(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Hierarchy and verify move styles from not present in season, brand and department as NonAdmin \"winter season\",\"Denim\",\"Mens\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.Navigate_to_Hierarchy_and_verify_move_styles_from_not_present_in_season_brand_and_department_as_NonAdmin(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout from the Application",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageSteps.logout_from_the_Application()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Hide Revision Action",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "Navigate to Existing Style \"\u003cStyle\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "For Style Create price list, verify Approve action is visible \"\u003cPriceList\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Logout Login as Non Admin \"\u003cUser\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to Existing Style \"\u003cStyle\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "For Style Create price list, verify Approve action is not visible \"\u003cPriceList\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Style",
        "Role",
        "User",
        "PriceList"
      ]
    },
    {
      "cells": [
        "Apparel - Color and Size",
        "Non Admin",
        "NA",
        "A_Style_Price List"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Hide Revision Action",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@ProductCustomizations"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Existing Style \"Apparel - Color and Size\"",
  "keyword": "And "
});
formatter.match({
  "location": "ProductCustomizationSteps.Navigate_to_Existing_Style(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "For Style Create price list, verify Approve action is visible \"A_Style_Price List\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.For_Style_Create_price_list_verify_Approve_action_is_visible(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Logout Login as Non Admin \"NA\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.Login_as_Non_Admin(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Existing Style \"Apparel - Color and Size\"",
  "keyword": "And "
});
formatter.match({
  "location": "ProductCustomizationSteps.Navigate_to_Existing_Style(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "For Style Create price list, verify Approve action is not visible \"A_Style_Price List\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.For_Style_Create_price_list_verify_Approve_action_is_not_visible(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "AllowNoneOptionForSKU",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "Navigate to Existing Style \"\u003cStyle\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Create New SKU and verify the None option is available \"\u003cSKUName\u003e\",\"\u003cStyleColorway\u003e\",\"\u003cStyleSize\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Create another SKU and verify in status bar shows duplicate ignored \"\u003cSKUName\u003e\",\"\u003cStyleColorway\u003e\",\"\u003cStyleSize\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "Navigate to Existing Material \"\u003cMaterial\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Create New SKU for Material and Verify None option is visible in dropdown \"\u003cSKUName\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Style",
        "SKUName",
        "Material",
        "StyleColorway",
        "StyleSize"
      ]
    },
    {
      "cells": [
        "Apparel - Color and Size",
        "Style_SKU1,Style_SKUDup,Mat_SKU1",
        "100% Cotton/Rayon Jersey",
        "Blue Color faded",
        "small"
      ]
    }
  ]
});
formatter.scenario({
  "name": "AllowNoneOptionForSKU",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@ProductCustomizations"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Existing Style \"Apparel - Color and Size\"",
  "keyword": "And "
});
formatter.match({
  "location": "ProductCustomizationSteps.Navigate_to_Existing_Style(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create New SKU and verify the None option is available \"Style_SKU1,Style_SKUDup,Mat_SKU1\",\"Blue Color faded\",\"small\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.Create_New_SKU_and_verify_the_None_option_is_available(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create another SKU and verify in status bar shows duplicate ignored \"Style_SKU1,Style_SKUDup,Mat_SKU1\",\"Blue Color faded\",\"small\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.Create_another_SKU_and_verify_in_status_bar_shows_duplicate_ignored(String,String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Existing Material \"100% Cotton/Rayon Jersey\"",
  "keyword": "And "
});
formatter.match({
  "location": "ProductCustomizationSteps.Navigate_to_Existing_Material(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Create New SKU for Material and Verify None option is visible in dropdown \"Style_SKU1,Style_SKUDup,Mat_SKU1\"",
  "keyword": "And "
});
formatter.match({
  "location": "ProductCustomizationSteps.Create_New_SKU_for_Material_and_verify_the_None_option_is_visible_in_dropdown(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Block 3D Document On Style",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "Navigate to Existing Style \"\u003cStyle\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Add document properties through custom view in Style",
  "keyword": "Then "
});
formatter.step({
  "name": "Verify the Add 3D Document not visible in menu list for Style",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Style"
      ]
    },
    {
      "cells": [
        "Apparel - Color and Size"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Block 3D Document On Style",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@ProductCustomizations"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Existing Style \"Apparel - Color and Size\"",
  "keyword": "And "
});
formatter.match({
  "location": "ProductCustomizationSteps.Navigate_to_Existing_Style(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Add document properties through custom view in Style",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.Add_document_properties_through_custom_view_in_Style()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Verify the Add 3D Document not visible in menu list for Style",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.Verify_the_Add_3D_Document_not_visible_in_menu_list_for_Style()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Block 3D Document On SKU",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "Navigate to Existing Style \"\u003cStyle\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "Navigate to SKU of the style and Add document properties through custom view \"\u003cSKUName\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "Verify the Add 3D Document is visible in menu list for Style SKU",
  "keyword": "Then "
});
formatter.step({
  "name": "Navigate to Document tab and verify fields are visible",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Style",
        "SKUName"
      ]
    },
    {
      "cells": [
        "Apparel - Color and Size",
        "StyleSKU1,StyleSKU2"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Block 3D Document On SKU",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@ProductCustomizations"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Existing Style \"Apparel - Color and Size\"",
  "keyword": "And "
});
formatter.match({
  "location": "ProductCustomizationSteps.Navigate_to_Existing_Style(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to SKU of the style and Add document properties through custom view \"StyleSKU1,StyleSKU2\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.Navigate_to_SKU_of_the_style_and_Add_document_properties_through_custom_view(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Verify the Add 3D Document is visible in menu list for Style SKU",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.Verify_the_Add_3D_Document_is_visible_in_menu_list_for_Style_SKU()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Document tab and verify fields are visible",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.Navigate_to_Document_tab_and_verify_fields_are_visible()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
formatter.scenarioOutline({
  "name": "Block Reference Document on Style",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "Navigate to Existing Style \"\u003cStyle\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "In Style verify reference document should not be visible",
  "keyword": "Then "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "Navigate to Existing Material \"\u003cMaterialName\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "name": "In Material verify reference document should not be visible",
  "keyword": "Then "
});
formatter.step({
  "name": "Navigate to Colored Material and verify reference document should not be visible \"\u003cColoredMaterialName\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Style",
        "MaterialName",
        "ColoredMaterialName"
      ]
    },
    {
      "cells": [
        "Apparel - Color and Size",
        "100% Cotton/Rayon Jersey",
        "Green CM - 01"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Block Reference Document on Style",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@ProductCustomizations"
    }
  ]
});
formatter.before({
  "status": "skipped"
});
formatter.before({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Existing Style \"Apparel - Color and Size\"",
  "keyword": "And "
});
formatter.match({
  "location": "ProductCustomizationSteps.Navigate_to_Existing_Style(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "In Style verify reference document should not be visible",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.In_Style_verify_reference_document_should_not_be_visible()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Existing Material \"100% Cotton/Rayon Jersey\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.Navigate_to_Existing_Material(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "In Material verify reference document should not be visible",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.In_Material_verify_reference_document_should_not_be_visible()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "Navigate to Colored Material and verify reference document should not be visible \"Green CM - 01\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ProductCustomizationSteps.Navigate_to_Colored_Material_and_verify_reference_document_should_not_be_visible(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "skipped"
});
});
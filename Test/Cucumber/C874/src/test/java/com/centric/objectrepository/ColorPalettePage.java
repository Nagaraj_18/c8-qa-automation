package com.centric.objectrepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.centric.resources.Commonactions;

public class ColorPalettePage extends Commonactions{

	public ColorPalettePage() {
		PageFactory.initElements(Commonactions.driver, this);
	}

	@FindBy(xpath="//span[@data-csi-tab-name='Color Palette Types']")
	private WebElement ColorPaletteTab;

	@FindBy(xpath="//table[@data-csi-automation='plugin-Site-ColorPaletteSubtypes-ToolbarNewActions']")
	private WebElement NewColorPaletteBtn;

	@FindBy(xpath="(//div[contains(@data-csi-automation,'field-ColorPaletteSubtype-Form-Node Name')]/div)/div/input")
	private WebElement ColorPaletteSubTypeName;

	@FindBy(xpath="//div[@data-csi-automation='field-ColorPaletteSubtype-Form-PaletteToSeason']/div[3]/input[1]")
	private WebElement ColorPaletteToSeasonName;

	@FindBy(xpath="//span[text()='Color Palettes']")
	private WebElement ColorPalette;
	
	@FindBy(xpath="//div[@data-csi-automation='field-ColorPalette-Form-Node Name']//div/div/input")
	private WebElement CoPaletteName;
	
	@FindBy(xpath="//div[@data-csi-automation='field-ColorPalette-Form-Code']//div/div/input")
	private WebElement CoPaletteDesc;
	
	@FindBy(xpath="//div[@data-csi-automation='field-ColorPalette-Form-Description']//div/div/input")
	private WebElement CoPaletteCode;
	
	@FindBy(xpath="//div[@data-csi-automation='field-ColorPalette-Form-Subtype']/div[3]/input[1]")
	private WebElement CoPaletteSubType;
	
	@FindBy(xpath="//div[@data-csi-automation='field-ColorPalette-Form-Seasons']/div[3]/input[1]")
	private WebElement CoPaletteSeason;
	
	@FindBy(xpath="//div[@data-csi-automation='field-ColorPalette-Form-Category1Names']/div[3]/input[1]")
	private WebElement CoPaletteBrand;
	
	@FindBy(xpath="//div[@data-csi-automation='field-ColorPalette-Form-CollectionNames']/div[3]/input[1]")
	private WebElement CoPaletteCollec;
	
	@FindBy(xpath="//div[@data-csi-automation='field-ColorPalette-Form-Category2Names']/div[3]/input[1]")
	private WebElement CoPaletteDpt;
	
	@FindBy(xpath="//span[text()='Cancel']")
	private WebElement cancelbutton;

	@FindBy(xpath="//table[@data-csi-automation='plugin-ApparelViews-ColorPalettes-ToolbarNewActions']//div/div[2]")
	private WebElement CoPaletteAction;
	
	@FindBy(xpath="//div[@data-csi-automation='field-ColorPalette-Form-Seasons']/div[3]/input[@aria-invalid='false']")
	private WebElement DisabledSeasonField;
	
	@FindBy(xpath="//span[@data-csi-automation='plugin-BusinessObject-StyleTypeAttributes-refresh']")
	private WebElement StyleTypePageRef;
	
	@FindBy(xpath="//span[@data-csi-automation='filter-BusinessObject-StyleTypeAttributes-Node Name']")
	private WebElement StyleTypeFilter;
	
	@FindBy(xpath="//span[@data-csi-automation='plugin-Site-MaterialTypes-refresh']")
	private WebElement MatTypePageRef;
	
	@FindBy(xpath="//span[@data-csi-automation='filter-Site-MaterialTypes-Node Name']")
	private WebElement MatTypeFilter;
	
	@FindBy(xpath="//label[@class='dijitMenuItemLabel' and text()='All']")
	private WebElement LabelAll;
	
	public WebElement getStyleTypePageRef() {
		return StyleTypePageRef;
	}
	public WebElement getStyleTypeFilter() {
		return StyleTypeFilter;
	}
	public WebElement getLabelAll() {
		return LabelAll;
	}
	
	public WebElement getMatTypePageRef() {
		return MatTypePageRef;
	}
	public WebElement getMatTypeFilter() {
		return MatTypeFilter;
	}

	public WebElement getColorPaletteTab() {
		return ColorPaletteTab;
	}
	public WebElement getNewColorPaletteBtn() {
		return NewColorPaletteBtn;
	}
	public WebElement getColorPaletteSubTypeName() {
		return ColorPaletteSubTypeName;
	}
	
	public WebElement getColorPaletteToSeasonName() {
		return ColorPaletteToSeasonName;
	}

	public WebElement getColorPalette() {
		return ColorPalette;
	}
	public WebElement getCoPaletteName() {
		return CoPaletteName;
	}
	public WebElement getCoPaletteDesc() {
		return CoPaletteDesc;
	}
	
	public WebElement getCoPaletteCode() {
		return CoPaletteCode;
	}
	
	public WebElement getCoPaletteSubType() {
		return CoPaletteSubType;
	}
	public WebElement getCoPaletteSeason() {
		return CoPaletteSeason;
	}
	public WebElement getCoPaletteBrand() {
		return CoPaletteBrand;
	}
	
	public WebElement getCoPaletteCollec() {
		return CoPaletteCollec;
	}
	
	public WebElement getCoPaletteDpt() {
		return CoPaletteDpt;
	}
	
	public WebElement getcancelbutton() {
		return cancelbutton;
	}
	
	public WebElement getCoPaletteAction() {
		return CoPaletteAction;
	}
	
	public WebElement getDisabledSeasonField() {
		return DisabledSeasonField;
	}
	
	
}
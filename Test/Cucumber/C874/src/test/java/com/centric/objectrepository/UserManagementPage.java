package com.centric.objectrepository;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.centric.resources.Commonactions;

public class UserManagementPage extends Commonactions{

	public UserManagementPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="//span[@data-csi-tab-name='User Management' or text()='User Management']")
	private WebElement UsrManagement;

	@FindBy(xpath="//span[@data-csi-tab-name='Roles' or text()='Roles']")
	private WebElement usrMgmt_Roles;
	
	@FindBy(xpath="//span[@data-csi-tab-name='Users' or text()='Users']")
	private WebElement usrMgmt_Users;
	
	@FindBy(xpath="(//table[contains(@data-csi-automation,'Users')]//div/div)[2]")
	private WebElement new_user_btn;

	@FindBy(xpath="(//table[contains(@data-csi-automation,'Roles')]//div/div)[2]")
	private WebElement new_role_Btn;
	
	@FindBy(xpath="//div[@data-csi-automation='field-Role-NewRole-ManagePreferences']/input")
	private WebElement manage_preference_chkbox;
	

	public WebElement getManage_preference_chkbox() {
		return manage_preference_chkbox;
	}

	@FindBy(xpath="(//span[contains(@data-csi-automation,'actions-Site-Roles')]/span/span/span)[1]")
	private WebElement usrMgmt_MoreOptions;
	@FindBy(xpath="(//span[contains(@data-csi-automation,'actions-Site-Roles')]/span/span)[2]")
	private WebElement usrMgmt_MoreOptions1;
	@FindBy(xpath="(//span[contains(@data-csi-automation,'actions-Site-Roles')]/span/span)[3]")
	private WebElement usrMgmt_MoreOptions2;
	@FindBy(xpath="(//span[contains(@data-csi-automation,'actions-Site-Roles')]/span)[4]")
	private WebElement usrMgmt_MoreOptions3;
	
	@FindBy(xpath="(//div[contains(@data-csi-automation,'NewUser')]/div/div/input)[1]")
	private WebElement user_login;
	
	@FindBy(xpath="(//div[contains(@data-csi-automation,'NewUser')]/input)[1]")
	private WebElement Active_chkbox;
	
	@FindBy(xpath="(//div[contains(@data-csi-automation,'NewUser')]/div/div/input)[2]")
	private WebElement firstname;


	
	@FindBy(xpath="((//table[contains(@data-csi-automation,'actions-Site-Roles')]/tbody/tr)[1]/td)[2]")
	private WebElement usrMgmt_SecurityRoles;
	//@FindBy(xpath="(//table[contains(@data-csi-automation,'actions-Site-Roles')]/tbody/tr/td[contains(text(),'Select Security Roles')])[1]")
	//private WebElement usrMgmt_SecurityRoles1;
	@FindBy(xpath="(//table[contains(@data-csi-automation,'actions-Site-Roles-')]/tbody/tr[1]/td[contains(text(),'Select Security Roles')])[1]")
	private WebElement usrMgmt_SecurityRoles2;
	@FindBy(xpath="(//table[contains(@data-csi-automation,'actions-Site-Roles-C10533')]/tbody/tr/td[contains(text(),'Select Security Roles')])[1]")
	private WebElement usrMgmt_SecurityRoles3;
	@FindBy(xpath="(//table[contains(@data-csi-automation,'actions-Site-Roles-C10534')]/tbody/tr/td[contains(text(),'Select Security Roles')])[1]")
	private WebElement usrMgmt_SecurityRoles4;
	@FindBy(xpath="(//div[contains(@data-csi-automation,'SecurityRolesAndGroups')]/div[3]//input)[3]")
	private WebElement selectbtn;
	@FindBy(xpath="//th[contains(@class,'csi-table-selection-column noexport firstColumn')]/div/input")
	private WebElement securityroleschkbox;
	@FindBy(xpath="(//td[text()='EC Admin']/parent::tr/td/div/input)[1]")
	private WebElement selectmemebersAdmin;
	@FindBy(xpath="(//td[text()='EC Edit']/parent::tr/td/div/input)[1]")
	private WebElement selectmemebersEdit;
	@FindBy(xpath="(//td[text()='EC User']/parent::tr/td/div/input)[1]")
	private WebElement selectmemebersUser;
	@FindBy(xpath="(//td[text()='EC View']/parent::tr/td/div/input)[1]")
	private WebElement selectmemebersView;
	
	public WebElement getSelectmemebersAdmin() {
		return selectmemebersAdmin;
	}

	public WebElement getSelectmemebersEdit() {
		return selectmemebersEdit;
	}

	public WebElement getSelectmemebersUser() {
		return selectmemebersUser;
	}

	public WebElement getSelectmemebersView() {
		return selectmemebersView;
	}

	public WebElement getSecurityroleschkbox() {
		return securityroleschkbox;
	}

	public WebElement getSelectbtn() {
		return selectbtn;
	}

	@FindBy(xpath="((//table[contains(@data-csi-automation,'actions-Site-Roles')]/tbody/tr)[2]/td)[2]")
	private WebElement select_members1;
	@FindBy(xpath="(//table[contains(@data-csi-automation,'actions-Site-Roles-C10532')]/tbody/tr/td[contains(text(),'Select Members')])[1]")
	private WebElement select_members2;
	@FindBy(xpath="(//table[contains(@data-csi-automation,'actions-Site-Roles-C10533')]/tbody/tr/td[contains(text(),'Select Members')])[1]")
	private WebElement select_members3;
	@FindBy(xpath="(//table[contains(@data-csi-automation,'actions-Site-Roles-C10534')]/tbody/tr/td[contains(text(),'Select Members')])[1]")
	private WebElement select_members4;
	@FindBy(xpath="//span[@data-csi-tab-name='Enumerations']")
	private WebElement Enumeration_tab;
	@FindBy(xpath="(//input[@type='button']/ancestor::tr/th/span[contains(@data-csi-automation,'filter-Site-Enumerations-Node Name')])[1]")
	private WebElement Enumeration_selecttab;
	@FindBy(xpath="//div[text()='New Enumeration Value']")
	private WebElement Enumeration_Value;
	

	public WebElement getEnumeration_Value() {
		return Enumeration_Value;
	}

	public WebElement getEnumeration_tab() {
		return Enumeration_tab;
	}

	public WebElement getEnumeration_selecttab() {
		return Enumeration_selecttab;
	}

	public WebElement getUsrMgmt_MoreOptions1() {
		return usrMgmt_MoreOptions1;
	}

	public WebElement getUsrMgmt_MoreOptions2() {
		return usrMgmt_MoreOptions2;
	}

	public WebElement getUsrMgmt_MoreOptions3() {
		return usrMgmt_MoreOptions3;
	}

	//public WebElement getUsrMgmt_SecurityRoles1() {
		//return usrMgmt_SecurityRoles1;
	//}

	public WebElement getUsrMgmt_SecurityRoles2() {
		return usrMgmt_SecurityRoles2;
	}

	public WebElement getUsrMgmt_SecurityRoles3() {
		return usrMgmt_SecurityRoles3;
	}

	public WebElement getUsrMgmt_SecurityRoles4() {
		return usrMgmt_SecurityRoles4;
	}

	public WebElement getSelect_members1() {
		return select_members1;
	}

	public WebElement getSelect_members2() {
		return select_members2;
	}

	public WebElement getSelect_members3() {
		return select_members3;
	}

	public WebElement getSelect_members4() {
		return select_members4;
	}

	@FindBy(xpath="//span[contains(@data-csi-automation,'SecurityRolesAndGroups-CustomViewActions')]")
	private WebElement usrMgmt_Views;

	@FindBy(xpath="//table[@data-csi-automation='plugin-Role-SecurityRolesAndGroups-CustomViewActions']//tr[contains(@data-csi-act,'CustomViewManage')]")
	private WebElement usrMgmt_Manage_Views;

	@FindBy(xpath="//span[@widgetid='prefCopy']/span")
	private WebElement usrMgmt_Copy;

	@FindBy(xpath="//input[@id='prefName']")
	private WebElement usrMgmt_Copy_Value;

	@FindBy(xpath="(((//div[@id='customViewsTabContainer']/div)[1]/div)[4]/div/div)[2]")
	private WebElement usrMgmt_Filter;

	@FindBy(xpath="(//span[@title='Add'])[1]")
	private WebElement usrMgmt_Add;

	@FindBy(xpath="(//td[@role='gridcell']//input[@aria-autocomplete='both'])[1]")
	private WebElement usrMgmt_Attribute;

	@FindBy(xpath="(//td[@role='gridcell']//input[@aria-autocomplete='both'])[2]")
	private WebElement usrMgmt_Operator;

	@FindBy(xpath="(//td[@role='gridcell']//input[@aria-autocomplete='list'])")
	private WebElement usrMgmt_Value;

	@FindBy(xpath="(((//div[@id='customViewsTabContainer']/div)[1]/div)[4]/div/div)[6]")
	private WebElement usrMgmt_Options;

	@FindBy(xpath="(//input[@id='querySize'])[1]")
	private WebElement usrMgmt_ResultsSize;

	@FindBy(xpath="//input[@title='Toggle selections of all displayed items']")
	private WebElement usrMgmt_All;

	@FindBy(xpath="//span[@data-csi-tab-name='Announcements' or text()='Announcements']")
	private WebElement usrMgmt_Ancmnts;

	@FindBy(xpath="(//table[contains(@data-csi-automation,'Announcement')]//div/div)[2]")
	private WebElement new_Ancmnt_Btn;

	@FindBy(xpath="//td[contains(@class,'firstColumn') and @data-csi-heading='Message::0']")
	private WebElement usrMgmt_Ancmnts_Msg;

	@FindBy(xpath="//a[@data-csi-context-parent-attr='Announcements']")
	private WebElement usrMgmt_Ancmnts_Edit;

	@FindBy(xpath="//div[@class='fr-element fr-view']")
	private WebElement usrMgmt_Ancmnts_EditMsg_value;

	@FindBy(xpath="//button[@data-cmd='bold']")
	private WebElement usrMgmt_Ancmnts_Bold;

	@FindBy(xpath="//button[@data-cmd='fontSize']")
	private WebElement usrMgmt_Ancmnts_FontSize;

	@FindBy(xpath="//a[@data-cmd='fontSize' and @title='30']")
	private WebElement usrMgmt_Ancmnts_FontSize_30;

	@FindBy(xpath="//button[@data-cmd='textColor']")
	private WebElement usrMgmt_Ancmnts_Color;

	@FindBy(xpath="(//span[@data-cmd='applytextColor'])[5]")
	private WebElement usrMgmt_Ancmnts_purpleColor;

	@FindBy(xpath="//td[@data-csi-heading='EffectiveTo::0']")
	private WebElement usrMgmt_Ancmnts_EffectiveTo;

	@FindBy(xpath="//td[contains(@class,'CurrentDate')]")
	private WebElement usrMgmt_Ancmnts_CurrentDate;

	@FindBy(xpath="//td[@data-csi-heading='Companies::0']")
	private WebElement usrMgmt_Ancmnts_Companies;

	@FindBy(xpath="//input[contains(@value,'CompanyInfo')]")
	private WebElement usrMgmt_Ancmnts_HostCompanies;


	public WebElement getUsrManagement() {
		return UsrManagement;
	}

	public WebElement getUsrMgmt_Roles() {
		return usrMgmt_Roles;
	}

	public WebElement getUsrMgmt_Users() {
		return usrMgmt_Users;
	}

	public WebElement getNew_role_Btn() {
		return new_role_Btn;
	}
	
	public WebElement getNew_user_btn() {
		return new_user_btn;
	}

	public WebElement getUser_login() {
		return user_login;
	}

	public WebElement getActive_chkbox() {
		return Active_chkbox;
	}

	public WebElement getFirstname() {
		return firstname;
	}


	public WebElement getUsrMgmt_MoreOptions() {
		return usrMgmt_MoreOptions;
	}

	public WebElement getUsrMgmt_SecurityRoles() {
		return usrMgmt_SecurityRoles;
	}

	public WebElement getUsrMgmt_Views() {
		return usrMgmt_Views;
	}

	public WebElement getUsrMgmt_Manage_Views() {
		return usrMgmt_Manage_Views;
	}

	public WebElement getUsrMgmt_Copy() {
		return usrMgmt_Copy;
	}

	public WebElement getUsrMgmt_Copy_Value() {
		return usrMgmt_Copy_Value;
	}

	public WebElement getUsrMgmt_Filter() {
		return usrMgmt_Filter;
	}

	public WebElement getUsrMgmt_Add() {
		return usrMgmt_Add;
	}

	public WebElement getUsrMgmt_Attribute() {
		return usrMgmt_Attribute;
	}

	public WebElement getUsrMgmt_Operator() {
		return usrMgmt_Operator;
	}

	public WebElement getUsrMgmt_Value() {
		return usrMgmt_Value;
	}

	public WebElement getUsrMgmt_Options() {
		return usrMgmt_Options;
	}

	public WebElement getUsrMgmt_ResultsSize() {
		return usrMgmt_ResultsSize;
	}

	public WebElement getUsrMgmt_All() {
		return usrMgmt_All;
	}

	public WebElement getUsrMgmt_Ancmnts() {
		return usrMgmt_Ancmnts;
	}

	public WebElement getNew_Ancmnt_Btn() {
		return new_Ancmnt_Btn;
	}

	public WebElement getUsrMgmt_Ancmnts_Msg() {
		return usrMgmt_Ancmnts_Msg;
	}

	public WebElement getUsrMgmt_Ancmnts_Edit() {
		return usrMgmt_Ancmnts_Edit;
	}

	public WebElement getUsrMgmt_Ancmnts_EditMsg_value() {
		return usrMgmt_Ancmnts_EditMsg_value;
	}

	public WebElement getUsrMgmt_Ancmnts_Bold() {
		return usrMgmt_Ancmnts_Bold;
	}

	public WebElement getUsrMgmt_Ancmnts_FontSize() {
		return usrMgmt_Ancmnts_FontSize;
	}

	public WebElement getUsrMgmt_Ancmnts_FontSize_30() {
		return usrMgmt_Ancmnts_FontSize_30;
	}

	public WebElement getUsrMgmt_Ancmnts_Color() {
		return usrMgmt_Ancmnts_Color;
	}

	public WebElement getUsrMgmt_Ancmnts_purpleColor() {
		return usrMgmt_Ancmnts_purpleColor;
	}

	public WebElement getUsrMgmt_Ancmnts_EffectiveTo() {
		return usrMgmt_Ancmnts_EffectiveTo;
	}

	public WebElement getUsrMgmt_Ancmnts_CurrentDate() {
		return usrMgmt_Ancmnts_CurrentDate;
	}

	public WebElement getUsrMgmt_Ancmnts_Companies() {
		return usrMgmt_Ancmnts_Companies;
	}

	public WebElement getUsrMgmt_Ancmnts_HostCompanies() {
		return usrMgmt_Ancmnts_HostCompanies;
	}

	//project type
	@FindBy(xpath="//span[@data-csi-tab-name='Shared Teams' or text()='Shared Teams']")
	private WebElement sharedteamtab;

	@FindBy(xpath="(//table[contains(@data-csi-automation,'HierarchySecurityGroups-ToolbarNewActions')]//div/div)[2]")
	private WebElement Newsharedbtn;

	@FindBy(xpath="((//div[contains(@data-csi-automation,'field-HierarchySecurityGroup-Form-Node Name')]/div)/div/input)[1]")
	private WebElement sharedname;

	@FindBy(xpath="((//div[contains(@data-csi-automation,'field-HierarchySecurityGroup-Form-ForLevel')]/div)[3]//input)[1]")
	private WebElement sharedtype;


	public WebElement getSharedteamtab() {
		return sharedteamtab;
	}

	public WebElement getNewsharedbtn() {
		return Newsharedbtn;
	}

	public WebElement getSharedname() {
		return sharedname;
	}

		public WebElement getSharedtype() {
		return sharedtype;
	}
//Engineering Change
	@FindBy(xpath="//span[text()='Engineering Change' or @data-csi-tab-name='Engineering']")
	private WebElement Engineering_Change_Tab;
	
	@FindBy(xpath="//span[text()='All Engineering Changes' or @data-csi-tab-name='Engineering']")
	private WebElement AllEngineering_Changes_Tab;
	
	@FindBy(xpath="//table[contains(@data-csi-automation,'AllECs')]//div[text()='New Engineering Change']")
	private WebElement New_Engineering_Changebtn;
	
	@FindBy(xpath="//a[text() = 'New Style']")
	private WebElement NeWStylebtn;
	
	@FindBy(xpath="(//a[text()='New Style']//following::span[text()='mode_edit'])[1]")
	private WebElement NewStyle_Edit_btn;
	
	@FindBy(xpath="(//a[text()='Style1']//following::span[text()='close'])[1]")
	private WebElement NewStyle_Close_btn;
	
		@FindBy(xpath="//span[text()='Views']/parent::span[1]")
	private WebElement Viewbtn;
	
	@FindBy(xpath="(//tr[contains(@data-csi-automation,'EngineeringChange-Properties-CustomViewManage')]//td[text()='Manage Views'])")
	private WebElement ManageViewbtn;
	
	@FindBy(xpath="//div[text()='Allowed Revision State']/parent::th/following-sibling::td")
	private WebElement Allrevisionbtn;
	
	@FindBy(xpath="//div[text()='Effective End']/parent::th/following-sibling::td")
	private WebElement Effectiveend_date;
	
	@FindBy(xpath="//span[text()='7']")
	private WebElement Startdate;
	
	@FindBy(xpath="//div[text()='Effective Start']/parent::th/following-sibling::td")
	private WebElement Effectivestart_date;
	
	
	@FindBy(xpath="//span[text()='8']")
	private WebElement Enddate;
	
	@FindBy(xpath="//span[text()='Affected Items' or @data-csi-tab-name='Affected Items']")
	private WebElement Affected_Items;
	
	@FindBy(xpath="//table[contains(@data-csi-automation,'AffectedItemsView')]//div[text()='New Affected Item']")
	private WebElement NewAffected_Items;
	
	@FindBy(xpath="(//td[text()='Apparel - Color and Size']//parent::tr//div/input)[7]")
	private WebElement Aparel_colorandsizebtn;
	
	
	@FindBy(xpath="//td[@data-csi-act='AffectedColors::0']")
	private WebElement Affected_Colors;
	
	@FindBy(xpath="//td[@data-csi-act='AffectedSKUs::0']")
	private WebElement Affected_SKU;
	
	@FindBy(xpath="//td[@data-csi-act='AffectedIssues::0']")
	private WebElement Affected_Issue;
	
	@FindBy(xpath="(//label[contains(text(),'EC_Issue')]/parent::div/div)[1]")
	private WebElement Issue_chkbox;
	
	
	@FindBy(xpath="(//label[contains(text(),'11-0103 EGRET')]//parent::div/div[1])[1]")
	private WebElement EGRETCheckbox;
	
	@FindBy(xpath="(//label[contains(text(),'Blue Color faded')]//parent::div/div[1])[1]")
	private WebElement AffectedSku_chkbox;
	
	@FindBy(xpath="//span[text()='Style' or @data-csi-tab-name='Style']")
	private WebElement Style_tab;
	
	@FindBy(xpath="//span[text()='SKUs' or @data-csi-tab-name='SKUs']")
	private WebElement SKUs_tab;
	
	@FindBy(xpath="//span[text()='Issues' or @data-csi-tab-name='Issues']")
	private WebElement Issue_tab;
	
	@FindBy(xpath="//table[contains(@data-csi-automation,'Issues')]//div[text()='New Issue']")
	private WebElement NewIssue_btn;
	
	@FindBy(xpath="//table[contains(@data-csi-automation,'ProductSKU')]//div[text()='New Style SKU']")
	private WebElement NewStyleSKU;
	
	@FindBy(xpath="(//th[text()='Style SKU']//parent::tr//div/input)[2]")
	private WebElement Style_Skuvalue;
	
	@FindBy(xpath="(//th[text()='Issue']//parent::tr//div/input)[2]")
	private WebElement IssueValue;
	
	
	@FindBy(xpath="(//div[contains(@data-csi-automation,'Matrix')]/input)[1]")
	private WebElement Matrixchkbox;
	
	@FindBy(xpath="(//div[contains(@data-csi-automation,'IssueProductColors')]/div)[1]")
	private WebElement ProductColors_btn;

	@FindBy(xpath="(//label[contains(text(),'All')]//parent::div/div/input[1])[1]")
	private WebElement All_chkbox;

	@FindBy(xpath="(//div[contains(@data-csi-automation,'IssueProductSize')]/div)[1]")
	private WebElement Product_size;

	
	@FindBy(xpath="(//label[contains(text(),'All')]//parent::div/div/input[1])[2]")
	private WebElement All_chkbox2;

	@FindBy(xpath="(//div[contains(@data-csi-automation,'field-Issue-Form-CreateOneIssuePerColorSize')]/input)[1]")
	private WebElement issuesize_chkbox;

	
	public WebElement getStyle_Skuvalue() {
		return Style_Skuvalue;
	}

	public WebElement getStyle_tab() {
		return Style_tab;
	}

	public WebElement getSKUs_tab() {
		return SKUs_tab;
	}

	public WebElement getIssue_tab() {
		return Issue_tab;
	}

	public WebElement getNewIssue_btn() {
		return NewIssue_btn;
	}

	public WebElement getNewStyleSKU() {
		return NewStyleSKU;
	}

	public WebElement getMatrixchkbox() {
		return Matrixchkbox;
	}

	public WebElement getProductColors_btn() {
		return ProductColors_btn;
	}

	public WebElement getAffectedSku_chkbox() {
		return AffectedSku_chkbox;
	}

	public WebElement getAllrevisionbtn() {
		return Allrevisionbtn;
	}

	public WebElement getEngineering_Change_Tab() {
		return Engineering_Change_Tab;
	}

	public WebElement getAllEngineering_Changes_Tab() {
		return AllEngineering_Changes_Tab;
	}

	public WebElement getNew_Engineering_Changebtn() {
		return New_Engineering_Changebtn;
	}

	public WebElement getNeWStylebtn() {
		return NeWStylebtn;
	}

	public WebElement getNewStyle_Edit_btn() {
		return NewStyle_Edit_btn;
	}

	public WebElement getNewStyle_Close_btn() {
		return NewStyle_Close_btn;
	}

	public WebElement getViewbtn() {
		return Viewbtn;
	}

	public WebElement getManageViewbtn() {
		return ManageViewbtn;
	}

	public WebElement getEffectiveend_date() {
		return Effectiveend_date;
	}

	public WebElement getStartdate() {
		return Startdate;
	}

	public WebElement getEffectivestart_date() {
		return Effectivestart_date;
	}

	public WebElement getEnddate() {
		return Enddate;
	}

	public WebElement getAffected_Items() {
		return Affected_Items;
	}

	public WebElement getNewAffected_Items() {
		return NewAffected_Items;
	}

	public WebElement getAparel_colorandsizebtn() {
		return Aparel_colorandsizebtn;
	}

	public WebElement getAffected_Colors() {
		return Affected_Colors;
	}

	public WebElement getAffected_SKU() {
		return Affected_SKU;
	}

	public WebElement getEGRETCheckbox() {
		return EGRETCheckbox;
	}

	public WebElement getAll_chkbox() {
		return All_chkbox;
	}

	public WebElement getProduct_size() {
		return Product_size;
	}

	public WebElement getAll_chkbox2() {
		return All_chkbox2;
	}

	public WebElement getIssuesize_chkbox() {
		return issuesize_chkbox;
	}

	public WebElement getIssueValue() {
		return IssueValue;
	}

	public WebElement getAffected_Issue() {
		return Affected_Issue;
	}

	public WebElement getIssue_chkbox() {
		return Issue_chkbox;
	}


	
	//New Engineering change Style
	
	@FindBy(xpath="//span[@data-csi-tab-name='Engineering Change']")
	private WebElement EngineeringChange_Tab;
	
	@FindBy(xpath="(//table[contains(@data-csi-automation,'plugin-Data-AllECs-ToolbarNewActions')]//div/div)[2]")
	private WebElement NewEngineeringChange_Button;
	
	@FindBy(xpath="((//div[contains(@data-csi-automation,'field-EngineeringChange-Form-Node Name')]/div)/div/input)[1]")
	private WebElement EC_Creation;
	
	@FindBy(xpath="(//span[contains(@class,'Button') or text()='●']//following-sibling::span[text()='Save & New'])[1]")
	private WebElement SaveandNew;

	@FindBy(xpath="(//span[contains(@class,'Button') or text()='●']//following-sibling::span[text()='Save & Go'])[1]")
	private WebElement SaveandGo;
	
	@FindBy(xpath="//span[@data-csi-tab-name='Properties']")
	private WebElement PropertiesTab;
	
	@FindBy(xpath="//span[text()='home']")
	private WebElement HomeIcon;
	
	@FindBy(xpath="//td[text() = 'Production Approved']")
	private WebElement prodApprve;
	
	
	@FindBy(xpath="//a[text()='EC-LATEST REVISION']/parent::td//following-sibling::td[@data-csi-heading='AllowedRevisionState::0']")
	private WebElement prodApprve1;



	@FindBy(xpath="(//span[contains(@class,'Button') or text()='●']//following-sibling::span[text()='Save'])[1]")
	private WebElement SaveBtn;
	
	@FindBy(xpath="//span[@data-csi-act='Edit' or text()='mode_edit'][1]")
	private WebElement Edit_EC001;
	
	@FindBy(xpath="//span[contains(@class,'csi-icon-delete') or text()='Delete'][1]")
	private WebElement DeleteIcon;
	
	@FindBy(xpath="//span[contains(@class,'ButonText') or text()='Delete']")
	private WebElement DeleteBtn;
	
	@FindBy(xpath="//a[text()='EC-LATEST APPROVED']/parent::td//following-sibling::td[@data-csi-heading='IssueSeverity::0']")
	private WebElement issueSeverity1;
	
	@FindBy(xpath="//a[text()='EC-LATEST REVISION']/parent::td//following-sibling::td[@data-csi-heading='IssueSeverity::0']")
	private WebElement issueSeverity2;
	
	@FindBy(xpath="//a[text() = 'EC-Product Approved']")
	private WebElement ProductApproved;
	
	@FindBy(xpath="(//table[contains(@data-csi-automation,'AffectedItemsView')]//div/div)[2]")
	private WebElement NewAffectedItemBtn;
	
	@FindBy(xpath="((//div[contains(@data-csi-automation,'NewAffectedItemForm')]/div)[3]//input)[1]")
	private WebElement ItemType;
	
	@FindBy(xpath="//span[text()='Next']")
	private WebElement NextBtn;
	
	@FindBy(xpath="//td[text()='Apparel - Color and Size']//parent::tr//input[@type='checkbox'][1]")
	private WebElement colorandsizecheckbox;
	
	@FindBy(xpath="//span[text()='Finish']")
	private WebElement FinishBtn;
	
	@FindBy(xpath="//span[@data-csi-tab-name='Style']")
	private WebElement Styletab;
	
	@FindBy(xpath="//span[@data-csi-tab-name='Affected Items']")
	private WebElement affectedTab;
	
	
	
	public WebElement getAffectedTab() {
		return affectedTab;
	}

	@FindBy(xpath="//span[@data-csi-tab-name='Styles']")
	private WebElement Stylestab;
	
	@FindBy(xpath="//a[text() = 'Apparel - Color and Size'][1]")
	private WebElement StyleApparelclr;
	
	@FindBy(xpath="//span[@data-csi-tab-name='Colorways']")
	private WebElement colorwayTab;
	
	@FindBy(xpath="(//span[contains(@data-csi-automation,'actions-Style-ProductColors')])[1]")
	private WebElement ThreeDot;
	
	@FindBy(xpath="//td[text()='Add to Engineering Change']")
	private WebElement AddToEngineeringChange;
	
	@FindBy(xpath="//td[text()='EC-Product Approved']//parent::tr//input[@type='radio'][1]")
	private WebElement productCheckbox;
	
	@FindBy(xpath="//span[@data-csi-tab-name='SKUs']")
	private WebElement SKUTabs;
	
	@FindBy(xpath="(//span[contains(@data-csi-automation,'actions-Style-ProductSKU')])[1]")
	private WebElement ThreeDotSku;
	
	@FindBy(xpath="//td[text()='Add to Engineering Change']")
	private WebElement AddtoEngineeringSku;

	@FindBy(xpath="//span[@data-csi-tab-name='Issues']")
	private WebElement issuestab;
	
	@FindBy(xpath="(//span[contains(@data-csi-automation,'actions-IssueLog-Issues')])[1]")
	private WebElement ThreeDotsIssues;
	
	@FindBy(xpath="//td[text()='Add to Engineering Change']")
	private WebElement AddToEngineeringChangeIssue;
	
	@FindBy(xpath="(//span[contains(@data-csi-automation,'actions-EngineeringChange-AffectedItemsView')])[1]")
	private WebElement dotAffectspecs;

	@FindBy(xpath="//td[text()='Add Affected Specs']")
	private WebElement AddAffectedSpecs;
	
	@FindBy(xpath="//td[text()='Apparel BOM - Theme Lock']//parent::tr//input[@type='checkbox'][1]")
	private WebElement styleBOMSelect;
	
	@FindBy(xpath="//td[text()='Calendar Artwork']//parent::tr//input[@type='checkbox'][1]")
	private WebElement ArtworkSelection;


	@FindBy(xpath="//span[text()='Master']/parent::div/parent::th/preceding-sibling::th//input")
	private WebElement MasterCheck;
	
	@FindBy(xpath="//span[@data-csi-tab-name='Affected Specs']")
	private WebElement AffectedSPecsTabs;
	
	@FindBy(xpath="//a[text()='EC-Product Approved-2'][1]")
	private WebElement ECApproveArtwork;
	
	
	

	@FindBy(xpath="//span[text()='Create a new Canvas']")
	private WebElement CreateCanvas;
	
	@FindBy(xpath="//div[@title='Rectangle']")
	private WebElement rectangle;
	
	@FindBy(xpath="(//span[contains(@class,'Button') or text()='●']//following-sibling::span[text()='Save & Finish'])[1]")
	private WebElement SaveandFinish;
	
	@FindBy(xpath="(//span[text()='arrow_forward'])[1]")
	private WebElement arrowforward;
	
	@FindBy(xpath="//span[text()='Proceed']")
	private WebElement Proceed;
	
	@FindBy(xpath="(//span[text()='arrow_back'])[2]")
	private WebElement arrowBack;
	
	@FindBy(xpath="//span[@data-csi-tab-name='Affected Items']")
	private WebElement AffectedITemsTab;
	
	@FindBy(xpath="(//table[contains(@data-csi-automation,'plugin-EngineeringChange-AllAffectedSpecsView-ToolbarNewActions')]//div/div)[2]")
	private WebElement NewAffectedSpec;
	
	@FindBy(xpath="//a[text()='Calendar Artwork']")
	private WebElement CalendarArtworkSelect;
	
	@FindBy(xpath="//a[text()='Calendar IDS']")
	private WebElement CalendarIDS;
	
	
	public WebElement getCalendarIDS() {
		return CalendarIDS;
	}

	@FindBy(xpath="(//a[text()='EC-Product Approved-3']/ancestor::table/tbody/tr/td/a)[2]")
	private WebElement CalendarArtwork; 
	

	
	@FindBy(xpath="//span[text()='Actions']")
	private WebElement ActionsTab;
	
	@FindBy(xpath="//span[@data-csi-act='Edit' or text()='Edit'][1]")
	private WebElement editbtn;
	
	@FindBy(xpath="//td[text()='Approve']")
	private WebElement Approvebtn;
	
	@FindBy(xpath="//td[text()='DRAFT']")
	private WebElement DraftBTn;
	
	@FindBy(xpath="//span[text()='home']")
	private WebElement home;
	
	
	public WebElement getHome() {
		return home;
	}
	
	@FindBy(xpath="//td[text()='Reopen']")
	private WebElement ReOpen;
	
	
	public WebElement getEngineeringChange_Tab() {
		return EngineeringChange_Tab;
	}
	
	@FindBy(xpath="((//div[contains(@data-csi-automation,'Engineering')]/div)/div/input)[1]")
	private WebElement ECValue;
	
	
	public WebElement getECValue() {
		return ECValue;
	}

	public WebElement getNewEngineeringChange_Button() {
		return NewEngineeringChange_Button;
	}

	public WebElement getEC_Creation() {
		return EC_Creation;
	}

	public WebElement getSaveandNew() {
		return SaveandNew;
	}

	public WebElement getSaveandGo() {
		return SaveandGo;
	}

	public WebElement getPropertiesTab() {
		return PropertiesTab;
	}
	
	public WebElement getProdApprve() {
		return prodApprve;
	}
	
	
	public WebElement getProdApprve1() {
		return prodApprve1;
	}
	
	
	public WebElement getHomeIcon() {
		return HomeIcon;
	}

	public WebElement getSaveBtn() {
		return SaveBtn;
	}

	public WebElement getEdit_EC001() {
		return Edit_EC001;
	}

	public WebElement getDeleteIcon() {
		return DeleteIcon;
	}

	public WebElement getDeleteBtn() {
		return DeleteBtn;
	}

	public WebElement getIssueSeverity1() {
		return issueSeverity1;
	}

	public WebElement getIssueSeverity2() {
		return issueSeverity2;
	}

	public WebElement getProductApproved() {
		return ProductApproved;
	}

	public WebElement getNewAffectedItemBtn() {
		return NewAffectedItemBtn;
	}

	public WebElement getItemType() {
		return ItemType;
	}

	public WebElement getNextBtn() {
		return NextBtn;
	}

	public WebElement getColorandsizecheckbox() {
		return colorandsizecheckbox;
	}

	public WebElement getFinishBtn() {
		return FinishBtn;
	}

	public WebElement getStyletab() {
		return Styletab;
	}

	public WebElement getStylestab() {
		return Stylestab;
	}

	public WebElement getStyleApparelclr() {
		return StyleApparelclr;
	}

	public WebElement getColorwayTab() {
		return colorwayTab;
	}

	public WebElement getThreeDot() {
		return ThreeDot;
	}

	public WebElement getAddToEngineeringChange() {
		return AddToEngineeringChange;
	}

	public WebElement getProductCheckbox() {
		return productCheckbox;
	}

	public WebElement getSKUTabs() {
		return SKUTabs;
	}

	public WebElement getThreeDotSku() {
		return ThreeDotSku;
	}

	public WebElement getAddtoEngineeringSku() {
		return AddtoEngineeringSku;
	}

	public WebElement getIssuestab() {
		return issuestab;
	}

	public WebElement getThreeDotsIssues() {
		return ThreeDotsIssues;
	}

	public WebElement getAddToEngineeringChangeIssue() {
		return AddToEngineeringChangeIssue;
	}

	public WebElement getDotAffectspecs() {
		return dotAffectspecs;
	}

	public WebElement getAddAffectedSpecs() {
		return AddAffectedSpecs;
	}

	public WebElement getStyleBOMSelect() {
		return styleBOMSelect;
	}

	public WebElement getArtworkSelection() {
		return ArtworkSelection;
	}

	public WebElement getAffectedSPecsTabs() {
		return AffectedSPecsTabs;
	}

	public WebElement getECApproveArtwork() {
		return ECApproveArtwork;
	}

	public WebElement getCreateCanvas() {
		return CreateCanvas;
	}

	public WebElement getRectangle() {
		return rectangle;
	}

	public WebElement getSaveandFinish() {
		return SaveandFinish;
	}

	public WebElement getArrowforward() {
		return arrowforward;
	}

	public WebElement getProceed() {
		return Proceed;
	}

	public WebElement getArrowBack() {
		return arrowBack;
	}

	public WebElement getAffectedITemsTab() {
		return AffectedITemsTab;
	}

	public WebElement getNewAffectedSpec() {
		return NewAffectedSpec;
	}

	public WebElement getCalendarArtworkSelect() {
		return CalendarArtworkSelect;
	}

	public WebElement getActionsTab() {
		return ActionsTab;
	}

	public WebElement getEditbtn() {
		return editbtn;
	}

	public WebElement getApprovebtn() {
		return Approvebtn;
	}

	public WebElement getDraftBTn() {
		return DraftBTn;
	}

	public WebElement getReOpen() {
		return ReOpen;
	}
	
	public WebElement getMasterCheck() {
		return MasterCheck;
	}
	//New Engineering change Material
	
	
	@FindBy(xpath="//span[@data-csi-tab-name='Material']")
	private WebElement MaterialTab;
	
	@FindBy(xpath="//span[@data-csi-tab-name='Specification']")
	private WebElement MaterialSpecificationTab;
	
	@FindBy(xpath="//span[@data-csi-tab-name='BOM']")
	private WebElement MaterialBOM;
	

	
	
	@FindBy(xpath="(//table[contains(@data-csi-automation,'plugin-Material-BOMs-ToolbarNewActions')]//div/div)[2]")
	private WebElement NewMaterialBOM;
	
	@FindBy(xpath="(//a[text()='Routing']/ancestor::table/tbody/tr/td//span[text()='check_circle'])")
	private WebElement bomapprve;
	
	@FindBy(xpath="(//a[text()='Material BOM']/ancestor::table/tbody/tr/td//span[text()='check_circle'])[1]")
	private WebElement bomapproveorignal;
	
	
	public WebElement getBomapproveorignal() {
		return bomapproveorignal;
	}

	@FindBy(xpath="//span[@data-csi-tab-name='Routing']")
	private WebElement RoutingTab;

	@FindBy(xpath="(//table[contains(@data-csi-automation,'plugin-Material-Routing-ToolbarNewActions')]//div/div)[2]")
	private WebElement NewRouting;
	
	

	@FindBy(xpath="//span[@data-csi-tab-name='Material Data Sheets']")
	private WebElement MaterialDataSheets;
	
	@FindBy(xpath="(//table[contains(@data-csi-automation,'plugin-Material-MaterialDataSheets-ToolbarNewActions')]//div/div)[2]")
	private WebElement NewMaterialDataSheets;
	
	
	@FindBy(xpath="//span[@data-csi-tab-name='Spec']")
	private WebElement SpecTab;

	@FindBy(xpath="(//table[contains(@data-csi-automation,'plugin-Material-SpecificationDataSheets-ToolbarNewActions')]//div/div)[2]")
	private WebElement NewSpec;
	
	@FindBy(xpath="//a[text() = '100% Cotton/Rayon Jersey']")
	private WebElement rayonJersey;
	
	@FindBy(xpath="//span[@data-csi-tab-name='Issues']")
	private WebElement IssuesTabs;
	
	@FindBy(xpath="//a[text()='100% Cotton/Rayon Jersey']/parent::td//following-sibling::td[@data-csi-heading='AffectedColors::0']")
	private WebElement ColorMaterial;
	
	@FindBy(xpath="//a[text()='100% Cotton/Rayon Jersey']/parent::td//following-sibling::td[@data-csi-heading='AffectedSKUs::0']")
	private WebElement AffectedSkuselect;
	
	@FindBy(xpath="//a[text()='100% Cotton/Rayon Jersey']/parent::td//following-sibling::td[@data-csi-heading='AffectedIssues::0']")
	private WebElement AffectedISseuesSelect;
	
	@FindBy(xpath="//span[contains(@data-csi-automation,'actions-EngineeringChange-AffectedItemsView')][1]")
	private WebElement ThreeDotMaterial;
	
	@FindBy(xpath="//td[text()='Calendar BOM']//parent::tr//input[@type='checkbox'][1]")
	private WebElement CalendarBOM;
	
	@FindBy(xpath="//td[text()='Calendar MDS']//parent::tr//input[@type='checkbox'][1]")
	private WebElement CalendarMDS;
	
	@FindBy(xpath="//span[@data-csi-tab-name='Affected Specs']")
	private WebElement AffectedSpecsTab;
	
	@FindBy(xpath="//a[text() = 'EC-PRODUCT APPROVED-1']")
	private WebElement ECArtwork;
	
	@FindBy(xpath="//span[@data-csi-tab-name='Canvas']")
	private WebElement CanvasTab;
	
	@FindBy(xpath="(//a[text()='EC-PRODUCT APPROVED-2']/ancestor::table/tbody/tr/td//a[text()='Calendar BOM'])[2]")
	private WebElement CalendarBOMLink;

	@FindBy(xpath="//td[text()='100% Cotton/Rayon Jersey']//parent::tr//input[@type='checkbox'][1]")
	private WebElement rayonCheckBox;
	
	@FindBy(xpath="//label[contains(text(),'11-0103 EGRET')]//parent::div/div/input")
	private WebElement checkLabel;
	
	@FindBy(xpath="//label[contains(text(),'EC-ISSUE')]//parent::div/div/input")
	private WebElement checkIssue;
	
	@FindBy(xpath="//label[contains(text(),'MT SKU-11-0103 EGRET-large')]//parent::div/div/input")
	private WebElement checkSku;
	@FindBy(xpath="(//div[contains(@class,'dijitSliderIncrementIconH')]//span[text()='+']/parent::div)[1]")
	private WebElement RightScroll;	
	@FindBy(xpath="((//div[contains(@data-csi-automation,'field-MaterialBOM-Form-Subtype')]/div)[3]//input)[1]")
	private WebElement BomField;
	
	@FindBy(xpath="//span[@data-csi-tab-name='Material']")
	private WebElement MaterialTabbb;
	 
	@FindBy(xpath="((//div[contains(@data-csi-automation,'field-SpecificationDataSheet-Form-Subtype')]/div)[3]//input)[1]")
	private WebElement specDropdown;
	
	@FindBy(xpath="((//div[contains(@data-csi-automation,'field-EngineeringChange-NewAffectedSpecsForm-AffectedItems')]/div)[3]//input)[1]")
	private WebElement Affectedfinal;
	
	
	public WebElement getAffectedfinal() {
		return Affectedfinal;
	}

	public WebElement getMaterialTabbb() {
		return MaterialTabbb;
	}

	public WebElement getSpecDropdown() {
		return specDropdown;
	}

	public WebElement getBomField() {
		return BomField;
	}

	public WebElement getRightScroll() {
		return RightScroll;
	}

	public WebElement getCheckLabel() {
		return checkLabel;
	}

	public WebElement getCheckIssue() {
		return checkIssue;
	}

	public WebElement getCheckSku() {
		return checkSku;
	}

	public WebElement getRayonCheckBox() {
		return rayonCheckBox;
	}

	public WebElement getCalendarArtwork() {
		return CalendarArtwork;
	}

	public WebElement getMaterialTab() {
		return MaterialTab;
	}

	public WebElement getRayonJersey() {
		return rayonJersey;
	}

	public WebElement getIssuesTabs() {
		return IssuesTabs;
	}

	public WebElement getColorMaterial() {
		return ColorMaterial;
	}

	public WebElement getAffectedSkuselect() {
		return AffectedSkuselect;
	}

	public WebElement getAffectedISseuesSelect() {
		return AffectedISseuesSelect;
	}

	public WebElement getThreeDotMaterial() {
		return ThreeDotMaterial;
	}

	public WebElement getCalendarBOM() {
		return CalendarBOM;
	}

	public WebElement getCalendarMDS() {
		return CalendarMDS;
	}

	public WebElement getAffectedSpecsTab() {
		return AffectedSpecsTab;
	}

	public WebElement getECArtwork() {
		return ECArtwork;
	}

	public WebElement getCanvasTab() {
		return CanvasTab;
	}

	public WebElement getCalendarBOMLink() {
		return CalendarBOMLink;
	}
	

	public WebElement getBomapprve() {
		return bomapprve;
	}

	public WebElement getMaterialSpecificationTab() {
		return MaterialSpecificationTab;
	}

	public WebElement getMaterialBOM() {
		return MaterialBOM;
	}

	public WebElement getNewMaterialBOM() {
		return NewMaterialBOM;
	}
	
	
	public WebElement getRoutingTab() {
		return RoutingTab;
	}

	public WebElement getNewRouting() {
		return NewRouting;
	}

	public WebElement getMaterialDataSheets() {
		return MaterialDataSheets;
	}

	public WebElement getNewMaterialDataSheets() {
		return NewMaterialDataSheets;
	}

	public WebElement getSpecTab() {
		return SpecTab;
	}

	public WebElement getNewSpec() {
		return NewSpec;
	}

	
}

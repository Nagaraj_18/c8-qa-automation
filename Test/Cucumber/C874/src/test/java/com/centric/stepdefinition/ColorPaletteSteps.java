package com.centric.stepdefinition;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.centric.objectrepository.ColorPalettePage;
import com.centric.objectrepository.ConfigurationPage;
import com.centric.objectrepository.HomePage;
import com.centric.objectrepository.Locators;
import com.centric.objectrepository.MaterialSpecificationPage;
import com.centric.objectrepository.PopupPage;
import com.centric.objectrepository.SetupPageTK;
import com.centric.resources.Commonactions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ColorPaletteSteps extends Commonactions {
	Commonactions ca = new Commonactions();
	HomePage hp = new HomePage();
	SetupPageTK su = new SetupPageTK();
	MaterialSpecificationPage mp = new MaterialSpecificationPage();
	ConfigurationPage co = new ConfigurationPage();
	PopupPage pp=new PopupPage();
	ColorPalettePage cp = new ColorPalettePage();
	Locators lo = new Locators();
	static String style1,style2,style3,style4=null;
	
	   @Then("User Creates Color Palette SubTypes {string},{string}")
	    public void User_Creates_Raw_Material_Types(String SubTypeName,String SeasonDDLValue) throws Throwable {
	    	String[] SubTypeNames = SubTypeName.split(",");
	    	String[] SeasonDDLValues = SeasonDDLValue.split(",");
	    	
	    	for (int i = 0; i < SubTypeNames.length; i++) 
	    	{
	    	Commonactions.isElementPresent(cp.getColorPaletteTab());
	    	ca.click(cp.getColorPaletteTab());
	     	Commonactions.isElementPresent(cp.getNewColorPaletteBtn());
	    	ca.click(cp.getNewColorPaletteBtn());
	     	Commonactions.isElementPresent(cp.getColorPaletteSubTypeName());
	    	ca.insertText(cp.getColorPaletteSubTypeName(),SubTypeNames[i]);
	     	Commonactions.isElementPresent(cp.getColorPaletteToSeasonName());
	    	ca.insertText(cp.getColorPaletteToSeasonName(),SeasonDDLValues[i]);
	   		ca.click(mp.getSave_btn1());
			ca.eleToBeClickable();
			//Setting the Color palette types to active
			Commonactions.isElementPresent(driver.findElement(By.xpath("//td[text()='"+SubTypeNames[i]+"']//following::td/div[@data-csi-act='Active:Child:SetupSettings:0']/input[1]")));
			Commonactions.clickjs(driver.findElement(By.xpath("//td[text()='"+SubTypeNames[i]+"']//following::td/div[@data-csi-act='Active:Child:SetupSettings:0']/input[1]")));
	    	}
	 
		    //deleting the Color Palette
			Commonactions.isElementPresent(driver.findElement(By.xpath("(//td[text()='"+SubTypeNames[3]+"']//following::td/div/span[@data-csi-act='Delete'])[1]")));
			Commonactions.clickjs(driver.findElement(By.xpath("(//td[text()='"+SubTypeNames[3]+"']//following::td/div/span[@data-csi-act='Delete'])[1]")));
			ca.eleToBeClickable();
		    ca.click(co.getDelBtn());
			ca.eleToBeClickable();
		    System.out.println("The Delete button works and the Color palette is Deleted");
	    }
		@Then("Click on specification tab and Verify color palette {string},{string},{string},{string},{string}")
		public void clickOnSpecificationTabAndCreateColorSpecification(String SubtypeName, String Season, String Brand, String Collection, String Dpt) throws Throwable {
	    	String[] SubTypeNames = SubtypeName.split(",");
	    	String[] SeasonDDLValues = Season.split(",");
	    	
			Commonactions.isElementPresent(hp.getUser_settingsBtn1());
			ca.click(hp.getUser_settingsBtn1()); 

			System.out.println("Setup tab clicked successfully");

			Commonactions.isElementPresent(hp.getData_Spec());
			ca.click(hp.getData_Spec());
			ca.eleToBeClickable();
			Commonactions.isElementPresent(hp.getSetupSearch());
			ca.insertText(hp.getSetupSearch(), "Color Palette" +Keys.ENTER);
			ca.eleToBeClickable();

			Commonactions.isElementPresent(cp.getCoPaletteAction());
			ca.click(cp.getCoPaletteAction());
			ca.eleToBeClickable();
			Commonactions.isElementPresent(cp.getCoPaletteName());
			ca.insertText(cp.getCoPaletteName(),"CoPalette1");
			ca.eleToBeClickable();
			System.out.println("Color Palette Name Field is Editable");
			ca.eleToBeClickable();
			Commonactions.isElementPresent(cp.getCoPaletteDesc());
			ca.insertText(cp.getCoPaletteDesc(),"123");
			ca.eleToBeClickable();
			System.out.println("Color Palette Description Field is Editable");
			
			Commonactions.isElementPresent(cp.getCoPaletteCode());
			ca.insertText(cp.getCoPaletteCode(),"TestAut CoPlalette");
			ca.eleToBeClickable();
			System.out.println("Color Palette Code Field is Editable");
			
			//Select Subtype All Seasons  and verify the values in Seasons Brand Collections and Department
			Commonactions.isElementPresent(cp.getCoPaletteSubType());
			ca.click(cp.getCoPaletteSubType());
			ca.eleToBeClickable();
			ca.insertText(cp.getCoPaletteSubType(),SubTypeNames[0]);
			ca.eleToBeClickable();
			ca.jsMouseOver();
			ca.eleToBeClickable();
			Commonactions.isElementPresent(cp.getDisabledSeasonField());
			System.out.println("The Seasons  Field is Disabled When All seasons are selected");
			
			Commonactions.isElementPresent(cp.getCoPaletteBrand());
			//ca.click(cp.getCoPaletteBrand());
			ca.click(driver.findElement(By.xpath("//div[@data-csi-automation='field-ColorPalette-Form-Category1Names']")));
			ca.eleToBeClickable();
			Commonactions.isElementPresent(driver.findElement(By.xpath("//label[@class='dijitMenuItemLabel' and text()='"+Brand+"']")));
			System.out.println("The Brands are shown in the Brand Field");
			WebElement a2 = ca.activeElement();
			ca.eleToBeClickable();
			a2.sendKeys(Brand);
			ca.eleToBeClickable();
			//ca.insertText(cp.getCoPaletteBrand(),Brand);
			ca.eleToBeClickable();
			ca.jsMouseOver();
			ca.click(lo.getSm2());
			System.out.println("The User Is able to select the Brand Field");
			
			Commonactions.isElementPresent(cp.getCoPaletteDpt());
			//ca.click(cp.getCoPaletteDpt());
			ca.eleToBeClickable();
			Commonactions.isElementPresent(driver.findElement(By.xpath("//label[@class='dijitMenuItemLabel' and text()='"+Dpt+"']")));
			System.out.println("The Departments are shown in the Department Field");
			ca.insertText(cp.getCoPaletteDpt(),Dpt);
			ca.eleToBeClickable();
			ca.jsMouseOver();
			ca.click(lo.getSm2());
			System.out.println("The User Is able to select the Department Field");
			
			Commonactions.isElementPresent(cp.getCoPaletteCollec());
			//ca.click(cp.getCoPaletteCollec());
			ca.eleToBeClickable();
			Commonactions.isElementPresent(driver.findElement(By.xpath("//label[@class='dijitMenuItemLabel' and text()='"+Collection+"']")));
			System.out.println("The Collections are shown in the Collection Field");
			ca.insertText(cp.getCoPaletteCollec(),Collection);
			ca.eleToBeClickable();
			ca.jsMouseOver();
			ca.click(lo.getSm2());
			System.out.println("The User Is able to select the Collections Field");
				
			Commonactions.isElementPresent(cp.getcancelbutton());
			ca.click(cp.getcancelbutton());
			ca.eleToBeClickable();

	
		}

		@When("User creates style types For Color Palette {string}")
		public void user_creates_style_types_For_Color_Palette (String StyleType) throws Throwable {
			String[] StyleTypes = StyleType.split(",");
			
			for(int i=0; i < StyleTypes.length; i++){	
			Commonactions.clickjs(co.getStyle_Types());
			Commonactions.jsWaitForPageLoad();
			ca.eleToBeClickable();
			ca.click(cp.getStyleTypePageRef());
			ca.eleToBeClickable();
			
			Commonactions.isElementPresent(co.getNew_styleType_Btn());
			Commonactions.clickjs(co.getNew_styleType_Btn());
			Commonactions.jsWaitForPageLoad();
			Commonactions.isElementPresent(pp.getConfig_styleType_ValueA());
			ca.insertText(pp.getConfig_styleType_ValueA(), StyleTypes[i]);
			Commonactions.clickjs(pp.getConfig_save_Btn());
			Commonactions.jsWaitForPageLoad();
			ca.eleToBeClickable();
		    //filter the value
			ca.click(cp.getStyleTypeFilter());
			ca.eleToBeClickable();
			ca.click(cp.getLabelAll());
			ca.eleToBeClickable();
			//ca.click(driver.findElement(By.xpath("//input[@data-csi-automation='values-BusinessObject-StyleTypeAttributes-Node Name']")));
			//ca.eleToBeClickable();
	
			try {
				WebElement b1 = ca.activeElement();
				ca.eleToBeClickable();
			b1.sendKeys(Keys.DELETE);
			ca.eleToBeClickable();
			b1.sendKeys(StyleTypes[i]);
			ca.eleToBeClickable();
			ca.click(lo.getSm74());
			ca.eleToBeClickable();
			b1.sendKeys(Keys.TAB);
			} 

	        catch (Exception e) {
	    		ca.click(cp.getStyleTypeFilter());
				ca.eleToBeClickable();
				WebElement c1 = ca.activeElement();
				ca.eleToBeClickable();
				c1.sendKeys(Keys.DELETE);
				ca.eleToBeClickable();
				c1.sendKeys(StyleTypes[i]);
				ca.eleToBeClickable();
				ca.click(lo.getSm74());
				ca.eleToBeClickable();
				c1.sendKeys(Keys.TAB);

	        }
	
    		Commonactions.isElementPresent(co.getStyle_Colour());
			Commonactions.clickjs(co.getStyle_Colour());
			ca.eleToBeClickable();
			Commonactions.isElementPresent(driver.findElement(By.xpath("(//td[text()='"+StyleTypes[i]+"']//following::div[@data-csi-act='Available:Child:Config:0'])[1]")));
			ca.click(driver.findElement(By.xpath("(//td[text()='"+StyleTypes[i]+"']//following::div[@data-csi-act='Available:Child:Config:0'])[1]")));
			ca.eleToBeClickable();
			Commonactions.isElementPresent(driver.findElement(By.xpath("(//td[text()='"+StyleTypes[i]+"']//following::td[@data-csi-act='ColorPaletteRule:Child:Config:0'])[1]")));
			ca.click(driver.findElement(By.xpath("(//td[text()='"+StyleTypes[i]+"']//following::td[@data-csi-act='ColorPaletteRule:Child:Config:0'])[1]")));
			ca.eleToBeClickable();
			WebElement a1 = ca.activeElement();
			ca.eleToBeClickable();
			a1.sendKeys(Keys.DELETE);
			ca.eleToBeClickable();
			a1.sendKeys(StyleTypes[i]);
			//ca.insertText(a1, "Enabled");
			ca.eleToBeClickable();
			ca.jsMouseOver();
			ca.eleToBeClickable();
			try{
			for(int j=0; j < 30; j++){
				ca.click(driver.findElement(By.xpath("//div[contains(@class,'Style')]//span[text()='+']/parent::div")));
			}}catch (Exception e) {
				// TODO: handle exception
			}
			Commonactions.clickjs(co.getStyle_Brands());
			Commonactions.isElementPresent(co.getStyle_Features());
			Commonactions.clickjs(co.getStyle_Features());
			Commonactions.isElementPresent(co.getStyle_Functions());
			Commonactions.clickjs(co.getStyle_Functions());
			Commonactions.isElementPresent(co.getStyle_InAssortment());
			Commonactions.clickjs(co.getStyle_InAssortment());
			Commonactions.isElementPresent(co.getStyle_IsAssortment());
			Commonactions.clickjs(co.getStyle_IsAssortment());
			Commonactions.isElementPresent(co.getStyle_FoodAssortment());
			Commonactions.clickjs(co.getStyle_FoodAssortment());
			Commonactions.isElementPresent(co.getStyle_EnableSKU());
			Commonactions.clickjs(co.getStyle_EnableSKU());
			ca.eleToBeClickable();
			try{
			for(int k=0; k < 20; k++){
				ca.click(driver.findElement(By.xpath("//div[contains(@class,'Style')]//span[text()='-']/parent::div")));
			}}catch (Exception e) {
				// TODO: handle exception
			}
			Commonactions.isElementPresent(co.getStyle_EnableStyle());
			Commonactions.clickjs(co.getStyle_EnableStyle());
			Commonactions.isElementPresent(co.getStyle_Size());
			Commonactions.clickjs(co.getStyle_Size());
			Commonactions.isElementPresent(co.getStyle_Validate_MCM());
			Commonactions.clickjs(co.getStyle_Validate_MCM());
			Commonactions.isElementPresent(co.getStyle_Reference_Img());
			Commonactions.clickjs(co.getStyle_Reference_Img());
			try {
			Commonactions.isElementPresent(co.getStyle_Allow_ColourSpec());
			Commonactions.clickjs(co.getStyle_Allow_ColourSpec());
			ca.eleToBeClickable();
			}
			catch (Exception e) {
				// TODO: handle exception
			}
			try{
				for(int k=0; k < 60; k++){
					ca.click(driver.findElement(By.xpath("//div[contains(@class,'Style')]//span[text()='-']/parent::div")));
				}}catch (Exception e) {
					// TODO: handle exception
				}
	  		System.out.println("style type created successfully");
	  		System.out.println("checkbox clicked successfully");
		//	ca.clickjs(driver.findElement(By.xpath("//span[@title='Exit Full Screen']")));
			}
		}


		@When("User creates material types For Color Palette {string},{string}")
		public void user_creates_material_types_For_Color_Palette (String MatType,String MatTypeName) throws Throwable {
			String[] MatTypes = MatType.split(",");
			String[] MatTypeNames = MatTypeName.split(",");
			
			for(int i=0; i < MatTypes.length; i++){

			Commonactions.clickjs(co.getMaterial_Types());
			Commonactions.jsWaitForPageLoad();
			ca.eleToBeClickable();
			Commonactions.isElementPresent(cp.getMatTypePageRef());
			ca.click(cp.getMatTypePageRef());
			ca.eleToBeClickable();
			
			Commonactions.isElementPresent(co.getNew_material_TypeBtn());
			Commonactions.clickjs(co.getNew_material_TypeBtn());
			Commonactions.jsWaitForPageLoad();
			Commonactions.isElementPresent(pp.getConfig_materialType_ValueA());
			ca.insertText(pp.getConfig_materialType_ValueA(), MatTypeNames[i]);
			ca.eleToBeClickable();
	 		Commonactions.isElementPresent(pp.getConfig_material_DropDown());
	   		ca.click(pp.getConfig_material_DropDown());
	   		ca.eleToBeClickable();
	   		WebElement s1 = ca.activeElement();
			ca.eleToBeClickable();
		    ca.insertText(s1, "Standalone");
			ca.eleToBeClickable();
			ca.jsMouseOver();
			ca.eleToBeClickable();
			Commonactions.clickjs(pp.getConfig_save_Btn());
			Commonactions.jsWaitForPageLoad();
			ca.eleToBeClickable();
		    //filter the value
			ca.click(cp.getMatTypeFilter());
			ca.eleToBeClickable();
			ca.click(cp.getLabelAll());
			ca.eleToBeClickable();
	
			try {
				WebElement b1 = ca.activeElement();
				ca.eleToBeClickable();
			b1.sendKeys(Keys.DELETE);
			ca.eleToBeClickable();
			b1.sendKeys(MatTypeNames[i]);
			ca.eleToBeClickable();
			ca.click(lo.getSm74());
			ca.eleToBeClickable();
			b1.sendKeys(Keys.TAB);
			} 

	        catch (Exception e) {
	    		ca.click(cp.getMatTypeFilter());
				ca.eleToBeClickable();
				WebElement c1 = ca.activeElement();
				ca.eleToBeClickable();
				c1.sendKeys(Keys.DELETE);
				ca.eleToBeClickable();
				c1.sendKeys(MatTypeNames[i]);
				ca.eleToBeClickable();
				ca.click(lo.getSm74());
				ca.eleToBeClickable();
				c1.sendKeys(Keys.TAB);

	        }
	
    		Commonactions.isElementPresent(co.getMaterial_Colour());
			Commonactions.clickjs(co.getMaterial_Colour());
			ca.eleToBeClickable();
			Commonactions.isElementPresent(driver.findElement(By.xpath("(//td[text()='"+MatTypeNames[i]+"']//following::div[@data-csi-act='Available:Child:Config:0'])[1]")));
			ca.click(driver.findElement(By.xpath("(//td[text()='"+MatTypeNames[i]+"']//following::div[@data-csi-act='Available:Child:Config:0'])[1]")));
			ca.eleToBeClickable();
			Commonactions.isElementPresent(driver.findElement(By.xpath("(//td[text()='"+MatTypeNames[i]+"']//following::td[@data-csi-act='ColorPaletteRule:Child:Config:0'])[1]")));
			ca.click(driver.findElement(By.xpath("(//td[text()='"+MatTypeNames[i]+"']//following::td[@data-csi-act='ColorPaletteRule:Child:Config:0'])[1]")));
			ca.eleToBeClickable();
			WebElement a1 = ca.activeElement();
			ca.eleToBeClickable();
			a1.sendKeys(Keys.DELETE);
			ca.eleToBeClickable();
			a1.sendKeys(MatTypes[i]);
			ca.eleToBeClickable();
			ca.jsMouseOver();
			ca.eleToBeClickable();
			for(int j=0; j < 5; j++){
				Thread.sleep(1000);
				ca.click(driver.findElement(By.xpath("//div[contains(@class,'Material')]//span[text()='+']/parent::div")));
				if(co.getMaterial_BOM().isDisplayed()){
					
			//	ca.click(driver.findElement(By.xpath("//div[contains(@class,'Material')]//span[text()='+']/parent::div")));
				break;
				}}
			Commonactions.clickjs(co.getMaterial_BOM());
			
			for(int j=0; j < 5; j++){
				Thread.sleep(1000);
				ca.click(driver.findElement(By.xpath("//div[contains(@class,'Material')]//span[text()='+']/parent::div")));
				if(co.getMaterial_Routing().isDisplayed()){
			//	ca.click(driver.findElement(By.xpath("//div[contains(@class,'Material')]//span[text()='+']/parent::div")));
				break;
			}}
			Commonactions.isElementPresent(co.getMaterial_Routing());
			Commonactions.clickjs(co.getMaterial_Routing());
			
			for(int j=0; j < 5; j++){
				Thread.sleep(1000);
				ca.click(driver.findElement(By.xpath("//div[contains(@class,'Material')]//span[text()='+']/parent::div")));
				if(co.getMaterial_DataSheet().isDisplayed()){
			//	ca.click(driver.findElement(By.xpath("//div[contains(@class,'Material')]//span[text()='+']/parent::div")));
				break;
			}}
			Commonactions.isElementPresent(co.getMaterial_DataSheet());
			Commonactions.clickjs(co.getMaterial_DataSheet());
			
			for(int j=0; j < 5; j++){
				Thread.sleep(1000);
				ca.click(driver.findElement(By.xpath("//div[contains(@class,'Material')]//span[text()='+']/parent::div")));
				if(co.getMaterial_TestRun().isDisplayed()){
				//ca.click(driver.findElement(By.xpath("//div[contains(@class,'Material')]//span[text()='+']/parent::div")));
				break;
			}}
			Commonactions.isElementPresent(co.getMaterial_TestRun());
			Commonactions.clickjs(co.getMaterial_TestRun());
			Thread.sleep(1000);
			
			for(int j=0; j < 7; j++){
				Thread.sleep(1000);
				ca.click(driver.findElement(By.xpath("//div[contains(@class,'Material')]//span[text()='-']/parent::div")));
				if(co.getMaterial_Highest_Cost().isDisplayed()){
			//	ca.click(driver.findElement(By.xpath("//div[contains(@class,'Material')]//span[text()='-']/parent::div")));
				break;
			}}
			Commonactions.clickjs(co.getMaterial_Highest_Cost());
			
			for(int j=0; j < 4; j++){
				Thread.sleep(1000);
				ca.click(driver.findElement(By.xpath("//div[contains(@class,'Material')]//span[text()='-']/parent::div")));
				ca.eleToBeClickable();
				if(co.getMaterial_Default_Season().isDisplayed()){
				//ca.click(driver.findElement(By.xpath("//div[contains(@class,'Material')]//span[text()='-']/parent::div")));
				break;
			}}
			Commonactions.isElementPresent(co.getMaterial_Default_Season());
			Commonactions.clickjs(co.getMaterial_Default_Season());
			
			for(int j=0; j < 4; j++){
				Thread.sleep(1000);
				ca.click(driver.findElement(By.xpath("//div[contains(@class,'Material')]//span[text()='-']/parent::div")));
				if(co.getMaterial_EnableSKU().isDisplayed()){
			//	ca.click(driver.findElement(By.xpath("//div[contains(@class,'Material')]//span[text()='-']/parent::div")));
				break;
			}}
			Commonactions.isElementPresent(co.getMaterial_EnableSKU());
			Commonactions.clickjs(co.getMaterial_EnableSKU());
			
			for(int j=0; j < 4; j++){
				Thread.sleep(1000);
				ca.click(driver.findElement(By.xpath("//div[contains(@class,'Material')]//span[text()='-']/parent::div")));
				if(co.getMaterial_Size().isDisplayed()){
				//ca.click(driver.findElement(By.xpath("//div[contains(@class,'Material')]//span[text()='-']/parent::div")));
				break;
			}}
			Commonactions.isElementPresent(co.getMaterial_Size());
			Commonactions.clickjs(co.getMaterial_Size());
			
			for(int j=0; j < 10; j++){
				Thread.sleep(1000);
				ca.click(driver.findElement(By.xpath("//div[contains(@class,'Material')]//span[text()='-']/parent::div")));
				//if(co.getMaterial_Allow_Coloured().isDisplayed()){
				//ca.click(driver.findElement(By.xpath("//div[contains(@class,'Material')]//span[text()='-']/parent::div")));
				//break;}
			}
			try
			{
			Commonactions.isElementPresent(co.getMaterial_Allow_Coloured());
			Commonactions.clickjs(co.getMaterial_Allow_Coloured());
			ca.eleToBeClickable();
				}
			catch (Exception e) {
			}
			
			Thread.sleep(1000);
			ca.click(driver.findElement(By.xpath("//div[contains(@class,'Material')]//span[text()='-']/parent::div")));
			ca.click(driver.findElement(By.xpath("//div[contains(@class,'Material')]//span[text()='-']/parent::div")));
			try{
				for(int k=0; k < 20; k++){
					ca.click(driver.findElement(By.xpath("//div[contains(@class,'Material')]//span[text()='-']/parent::div")));
				}}catch (Exception e) {
					// TODO: handle exception
				}

		}
	}
}

		
		

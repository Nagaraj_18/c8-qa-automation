package com.centric.stepdefinition;

import com.centric.objectrepository.BusinessPlanningPage;
import com.centric.objectrepository.ConfigurationPage;
import com.centric.objectrepository.HomePage;
import com.centric.objectrepository.MaterialSpecificationPage;
import com.centric.objectrepository.PopupPage;
import com.centric.resources.Commonactions;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ConfigurationPageStepsTK extends Commonactions {
  Commonactions ca=new Commonactions();
    ConfigurationPage cp= new ConfigurationPage();
    PopupPage pp= new PopupPage();
	MaterialSpecificationPage mp = new MaterialSpecificationPage();
	HomePage hp = new  HomePage();
	ConfigurationPage co = new  ConfigurationPage();
	BusinessPlanningPage bp = new  BusinessPlanningPage();
	
    static String style1,style2,style3,style4=null;

    @When("User creates style type in setup page {string},{string},{string},{string}")
    public void user_creates_style_type(String A, String B, String C, String D) throws Throwable {
        style1=A;
        style2=B;
        style3=C;
        style4=D;
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_Types());
        Commonactions.jsWaitForPageLoad();
        ca.eleToBeClickable();
        //	ca.clickjs(driver.findElement(By.xpath("(//span[contains(@class,'fullScreen')])[3]")));
        colourAndSizeTK(A);
        onlyColoursTK(B);
        onlySizeTK(C);
        noDatasTK(D);

        getStyleActiveTK();

        System.out.println("style type created successfully");
        System.out.println("checkbox clicked successfully");
        //	ca.clickjs(driver.findElement(By.xpath("//span[@title='Exit Full Screen']")));

    }

    @When("User fills all the style type attributes fields")
    public void user_fill_the_style_type_attributes_fieldsTK() throws Throwable {
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_Type_DataSheets());
        ca.eleToBeClickable();
        try{
            Commonactions.clickjs(cp.getStyle_Type_DataSheets_RefreshBtn());
        }catch(Exception e){

        }
        ca.eleToBeClickable();
        //ca.clickjs(driver.findElement(By.xpath("(//span[contains(@class,'fullScreen')])[3]")));
        List<WebElement> nRows = driver.findElements(By.xpath("((//div[contains(@class,'TDS')])/div[@class='csiDialogScroll']//table)//tr[@data-csi-act='ViewSelect']"));
        int i = nRows.size();
        for (int j = 1; j <=i; j++) {
            ca.eleToBeClickable();
            WebElement text = driver.findElement(By.xpath("((((//div[contains(@class,'TDS')])/div[@class='csiDialogScroll']//table)//tr[@data-csi-act='ViewSelect'])["+j+"]/td)[1]"));
            //String s = String.valueOf(j);
            //style=null;
            String text2 = text.getText();
            System.out.println("text is :"+text2);

            if(text2.equals(style1)){
                for (int j2 = 1; j2 <= 12; j2++) {
                    if(j2<=5){
                        ca.eleToBeClickable();
                        Commonactions.clickjs(driver.findElement(By.xpath("(//tr["+j+"]//div[@data-csi-act='TDSMap:Child:Config:0']/input[@tabindex='0'])["+j2+"]")));
                        //ca.eleToBeClickable(cp.getStyle_Colour());
                    }
                    else{
                        //ca.eleToBeClickable();
                        //ca.clickjs(driver.findElement(By.xpath("(//span[text()='+']/parent::div)[3]")));
                        ca.eleToBeClickable();
                        Commonactions.clickjs(driver.findElement(By.xpath("(//tr["+j+"]//div[@data-csi-act='TDSMap:Child:Config:0']/input[@tabindex='0'])["+j2+"]")));

                    }

                }
            }else if(text2.equals(style2)){
                for (int j2 = 1; j2 <= 12; j2++) {

                    if(j2<=5){
                        ca.eleToBeClickable();
                        Commonactions.clickjs(driver.findElement(By.xpath("(//tr["+j+"]//div[@data-csi-act='TDSMap:Child:Config:0']/input[@tabindex='0'])["+j2+"]")));
                        //ca.eleToBeclickjsable(cp.getStyle_Colour());
                    }else{
                       // ca.eleToBeClickable();
                       // ca.clickjs(driver.findElement(By.xpath("(//span[text()='+']/parent::div)[3]")));
                        ca.eleToBeClickable();
                        Commonactions.clickjs(driver.findElement(By.xpath("(//tr["+j+"]//div[@data-csi-act='TDSMap:Child:Config:0']/input[@tabindex='0'])["+j2+"]")));

                    }

                }
            }else if(text2.equals(style3)){
                for (int j2 = 1; j2 <= 12; j2++) {

                    if(j2<=5){
                        ca.eleToBeClickable();
                        Commonactions.clickjs(driver.findElement(By.xpath("(//tr["+j+"]//div[@data-csi-act='TDSMap:Child:Config:0']/input[@tabindex='0'])["+j2+"]")));
                        //ca.eleToBeclickjsable(cp.getStyle_Colour());
                    }else{
                       // ca.eleToBeClickable();
                       // ca.clickjs(driver.findElement(By.xpath("(//span[text()='+']/parent::div)[3]")));
                        ca.eleToBeClickable();
                        Commonactions.clickjs(driver.findElement(By.xpath("(//tr["+j+"]//div[@data-csi-act='TDSMap:Child:Config:0']/input[@tabindex='0'])["+j2+"]")));
                    }

                }
            }else if(text2.equals(style4)){
                for (int j2 = 1; j2 <= 12; j2++) {

                    if(j2<=5){
                        ca.eleToBeClickable();
                        Commonactions.clickjs(driver.findElement(By.xpath("(//tr["+j+"]//div[@data-csi-act='TDSMap:Child:Config:0']/input[@tabindex='0'])["+j2+"]")));
                        //ca.eleToBeclickjsable(cp.getStyle_Colour());
                    }else{
                        //ca.eleToBeClickable();
                       // ca.clickjs(driver.findElement(By.xpath("(//span[text()='+']/parent::div)[3]")));
                        ca.eleToBeClickable();
                        Commonactions.clickjs(driver.findElement(By.xpath("(//tr["+j+"]//div[@data-csi-act='TDSMap:Child:Config:0']/input[@tabindex='0'])["+j2+"]")));
                    }

                }
            }else{
            }
            System.out.println("name doesn't matched");
        }


        System.out.println("style type attributes created successfully");
        System.out.println("checkbox clicked successfully");
    }
    //ca.clickjs(driver.findElement(By.xpath("//span[@title='Exit Full Screen']")));

    public void colourAndSizeTK(String value) throws Throwable {

        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getNew_styleType_Btn());
        Commonactions.jsWaitForPageLoad();
        ca.eleToBeClickable();
        ca.insertText(pp.getConfig_styleType_ValueA(), value);
        Commonactions.clickjs(pp.getConfig_save_Btn());
        Commonactions.jsWaitForPageLoad();
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_Colour());
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_Active());
        ca.eleToBeClickable();
        try{
            for(int i=0; i < 8; i++){
                Commonactions.clickjs(driver.findElement(By.xpath("(//span[text()='+']/parent::div)[2]")));
            }}catch (Exception e) {
            // TODO: handle exception
        }
        Commonactions.clickjs(cp.getStyle_Brands());
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_Features());
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_Functions());
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_InAssortment());
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_IsAssortment());
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_EnableSKU());
        ca.eleToBeClickable();
        try{
            for(int i=0; i < 7; i++){
                Commonactions.clickjs(driver.findElement(By.xpath("(//span[text()='-']/parent::div)[2]")));
            }}catch (Exception e) {
            // TODO: handle exception
        }
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_EnableStyle());
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_Size());
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_Validate_MCM());
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_Reference_Img());
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_Allow_ColourSpec());
        ca.eleToBeClickable();

        try{

            Commonactions.clickjs(driver.findElement(By.xpath("(//span[text()='-']/parent::div)[2]")));
        }catch (Exception e) {
            // TODO: handle exception
        }
    }

    public void onlyColoursTK(String value) throws Throwable {

        ca.eleToBeClickable();
        Commonactions.jsWaitForPageLoad();
        Commonactions.clickjs(cp.getNew_styleType_Btn());
        Commonactions.jsWaitForPageLoad();
        ca.eleToBeClickable();
        ca.insertText(pp.getConfig_styleType_ValueB(), value);
        Commonactions.jsWaitForPageLoad();
        Commonactions.clickjs(pp.getConfig_save_Btn());
        Commonactions.jsWaitForPageLoad();
        ca.eleToBeClickable();
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_Colour());
        ca.eleToBeClickable();
        //	ca.clickjs(cp.getStyle_Active());
        //	ca.eleToBeClickable();
        try{
            for(int i=0; i < 8; i++){
                Commonactions.clickjs(driver.findElement(By.xpath("(//span[text()='+']/parent::div)[2]")));
            }}catch (Exception e) {
            // TODO: handle exception
        }
        Commonactions.clickjs(cp.getStyle_Brands());
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_Features());
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_Functions());
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_InAssortment());
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_IsAssortment());
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_EnableSKU());
        ca.eleToBeClickable();
        try{
            for(int i=0; i < 7; i++){
                Commonactions.clickjs(driver.findElement(By.xpath("(//span[text()='-']/parent::div)[2]")));
            }}catch (Exception e) {
            // TODO: handle exception
        }
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_EnableStyle());
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_Validate_MCM());
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_Reference_Img());
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_Allow_ColourSpec());
        ca.eleToBeClickable();
        try{

            Commonactions.clickjs(driver.findElement(By.xpath("(//span[text()='-']/parent::div)[2]")));
        }catch (Exception e) {
            // TODO: handle exception
        }
    }

    public void onlySizeTK(String value) throws Throwable {

        ca.eleToBeClickable();
        Commonactions.jsWaitForPageLoad();
        Commonactions.clickjs(cp.getNew_styleType_Btn());
        Commonactions.jsWaitForPageLoad();
        ca.eleToBeClickable();
        ca.insertText(pp.getConfig_styleType_ValueB(), value);
        Commonactions.jsWaitForPageLoad();
        Commonactions.clickjs(pp.getConfig_save_Btn());
        Commonactions.jsWaitForPageLoad();
        //ca.eleToBeClickable();
        //	ca.eleToBeClickable();
        //	ca.clickjs(cp.getStyle_Active());
        //ca.eleToBeClickable();
        ca.eleToBeClickable();
        try{
            for(int i=0; i < 8; i++){
                Commonactions.clickjs(driver.findElement(By.xpath("(//span[text()='+']/parent::div)[2]")));
            }}catch(Exception e){

        }
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_Size());
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_Brands());
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_Features());
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_Functions());
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_InAssortment());
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_IsAssortment());
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_EnableSKU());
        ca.eleToBeClickable();
        try{
            for(int i=0; i < 7; i++){
                Commonactions.clickjs(driver.findElement(By.xpath("(//span[text()='-']/parent::div)[2]")));
            }}catch (Exception e) {
            // TODO: handle exception
        }
        ca.eleToBeClickable();
        Commonactions.clickjs(cp.getStyle_EnableStyle());
        ca.eleToBeClickable();
        try{

            Commonactions.clickjs(driver.findElement(By.xpath("(//span[text()='-']/parent::div)[2]")));
        }catch (Exception e) {
            // TODO: handle exception
        }
    }
    public void noDatasTK(String value) throws Throwable {

        ca.eleToBeClickable();
        Commonactions.jsWaitForPageLoad();
        Commonactions.clickjs(cp.getNew_styleType_Btn());
        Commonactions.jsWaitForPageLoad();
        ca.eleToBeClickable();
        ca.insertText(pp.getConfig_styleType_ValueB(), value);
        Commonactions.jsWaitForPageLoad();
        Commonactions.clickjs(pp.getConfig_save_Btn());
        Commonactions.jsWaitForPageLoad();
        //	ca.eleToBeClickable();
        //	ca.clickjs(cp.getStyle_Active());

    }
    public void getStyleActiveTK() throws Throwable{
        List<WebElement> actives = driver.findElements(By.xpath("(//div[@data-csi-act='Available:Child:Config:0']/input[@tabindex='0'])"));
        int i=actives.size();
        System.out.println("size :"+i);
        int  j=i-1;
        int  k=i-2;
        int  l=i-3;
        int  m=i-4;
        try{
            ca.eleToBeClickable();
            ca.jsScrollPageDown(driver.findElement(By.xpath("(//div[@data-csi-act='Available:Child:Config:0']/input[@tabindex='0'])["+i+"]")));

        }catch (Exception e) {
            // TODO: handle exception

        }

        Commonactions.clickjs(driver.findElement(By.xpath("(//div[@data-csi-act='Available:Child:Config:0']/input[@tabindex='0'])["+i+"]")));
        ca.eleToBeClickable();
        Commonactions.clickjs(driver.findElement(By.xpath("(//div[@data-csi-act='Available:Child:Config:0']/input[@tabindex='0'])["+j+"]")));
        ca.eleToBeClickable();
        Commonactions.clickjs(driver.findElement(By.xpath("(//div[@data-csi-act='Available:Child:Config:0']/input[@tabindex='0'])["+k+"]")));
        ca.eleToBeClickable();



    }


    @And("User deselects the colour specification option {string}")
    public void userDeselectsTheColourSpecificationOption(String a) throws Throwable {
        Thread.sleep(500);
        WebElement allowColorSpec = driver.findElement(By.xpath("(//td[contains(text(),'" + a + "')]//following::div[@data-csi-act='AllowCreateColorSpecOnColorway:Child:Config:0'])[1]"));
        ca.eleToBeClickable();
        ca.click(allowColorSpec);
    }
    @And("Go To Business Objects Tab")
    public void Go_To_Business_Objects_Tab() throws Throwable {
            Commonactions.isElementPresent(hp.getUser_settingsBtn());
    		ca.click(hp.getUser_settingsBtn());
    		Commonactions.isElementPresent(hp.getSystem_config());
    		ca.click(hp.getSystem_config());
    		Commonactions.isElementPresent(co.getUser_config());
    		ca.click(co.getUser_config());
       	    Commonactions.isElementPresent(cp.getBusinessObjectsBtn());
        	ca.click(cp.getBusinessObjectsBtn());
        	ca.eleToBeClickable();
        } 
    
    @Then("Create Optional Attribute for Raw Material and Raw Material Attributes {string},{string}")
    public void create_Optional_Attribute_for_Raw_Material_and_Raw_Material_Attributes(String RawMatName, String RawMatDispName) throws Throwable {
    	String[] RawMatNames = RawMatName.split(",");
    	String[] RawMatDispNames = RawMatDispName.split(",");
      	String str1 = new String("RawMaterial");
      	
     	Commonactions.isElementPresent(cp.getBusinessObjectsFilter());
    	ca.click(cp.getBusinessObjectsFilter());
    	Commonactions.isElementPresent(bp.getBusinessObject());
		ca.click(bp.getBusinessObject());
		
		Commonactions.isElementPresent(bp.getBusinessobj_Dropdown());
		ca.click(bp.getBusinessobj_Dropdown());
		try{
		
		WebElement a2 = ca.activeElement();
		ca.eleToBeClickable();
		a2.sendKeys(str1);
		ca.eleToBeClickable();
		Commonactions.isElementPresent(cp.getRawMatcheckbox());
		ca.click(cp.getRawMatcheckbox());
		ca.eleToBeClickable();
		WebElement a3 = ca.activeElement();
		ca.eleToBeClickable();
		a3.sendKeys(Keys.TAB);
		ca.eleToBeClickable();
		}catch (Exception e) {
			ca.eleToBeClickable();
			ca.click(driver.findElement(By.xpath("(//div[contains(@data-csi-automation,'filter-Reflection-BusinessObjects')]//div[contains(@class,'ArrowButton')])[1]")));
			WebElement a4 = ca.activeElement();
			ca.eleToBeClickable();
			a4.sendKeys(str1);
			ca.eleToBeClickable();
			Commonactions.isElementPresent(cp.getRawMatcheckbox());
			ca.click(cp.getRawMatcheckbox());
			ca.eleToBeClickable();
			WebElement a3 = ca.activeElement();
			ca.eleToBeClickable();
			a3.sendKeys(Keys.TAB);
			ca.eleToBeClickable();
		}
	
 
		Commonactions.isElementPresent(driver.findElement(By.xpath("//a[@class='browse' and text()='RawMaterial']")));
		ca.click(driver.findElement(By.xpath("//a[@class='browse' and text()='RawMaterial']")));
		//Adding the Attributes
     	Commonactions.isElementPresent(cp.getRawMatAttributesTab());
    	ca.click(cp.getRawMatAttributesTab());
     	Commonactions.isElementPresent(cp.getNewBtn());
    	ca.click(cp.getNewBtn());
    	ca.eleToBeClickable();
     	Commonactions.isElementPresent(cp.getAttributeName());
     	ca.insertText(cp.getAttributeName(), RawMatNames[0]);
    	Commonactions.isElementPresent(cp.getDisplayName());
     	ca.insertText(cp.getDisplayName(), RawMatDispNames[0]);
       	Commonactions.isElementPresent(cp.getAttributeType());
    	ca.click(cp.getAttributeType());
		WebElement N2 = ca.activeElement();
		N2.sendKeys(Keys.DELETE);
		ca.eleToBeClickable();
		N2.sendKeys("boolean");
		ca.eleToBeClickable();
		ca.jsMouseOver();
		ca.eleToBeClickable();
		Commonactions.mouseOver(driver.findElement(By.xpath("//div[@data-csi-automation='field-ConfigurableAttribute-Form-Optional']/input")));
		ca.click(driver.findElement(By.xpath("//div[@data-csi-automation='field-ConfigurableAttribute-Form-Optional']/input")));
		ca.eleToBeClickable();
		ca.click(mp.getSave_btn1());
		ca.eleToBeClickable();
		System.out.println("First Attribute is created ");

		//Adding the Attributes Under Form Definition
     	Commonactions.isElementPresent(cp.getRawMatFormDefTab());
    	ca.click(cp.getRawMatFormDefTab());
     	Commonactions.isElementPresent(cp.getSelectAttributeBtn());
    	ca.click(cp.getSelectAttributeBtn());
    	// Deleting the Default selections
     	Commonactions.isElementPresent(cp.getSelectAllCheckbox());
    	ca.click(cp.getSelectAllCheckbox());
    	ca.eleToBeClickable();
    	ca.click(cp.getSelectAllCheckbox());
    	ca.eleToBeClickable();
		ca.click(mp.getSave_btn1());
		ca.eleToBeClickable();
		// Selecting the required attributes in form defination
	  	Commonactions.isElementPresent(cp.getSelectAttributeBtn());
    	ca.click(cp.getSelectAttributeBtn());
    	ca.eleToBeClickable();
    	ca.click(driver.findElement(By.xpath("//td[@class='attrString' and text()='RawMaterial/Node Name']")));
    	ca.click(driver.findElement(By.xpath("//td[@class='attrString' and text()='RawMaterial/Subtype']")));
      	ca.click(mp.getSave_btn1());
		ca.eleToBeClickable();	
		System.out.println("Node name and Subtype are added under form definition");

		//Adding the other Attribute
    	Commonactions.isElementPresent(cp.getAtrributeNameSelecBreadCrumb());
    	ca.click(cp.getAtrributeNameSelecBreadCrumb());
    	WebElement N3 = ca.activeElement();
		ca.eleToBeClickable();
		N3.sendKeys(Keys.DELETE);
		ca.eleToBeClickable();
		N3.sendKeys("RawMaterialAttributes");
		ca.eleToBeClickable();
		N3.sendKeys(Keys.ENTER);
		ca.eleToBeClickable();
     	Commonactions.isElementPresent(cp.getNewBtn());
    	ca.click(cp.getNewBtn());
    	ca.eleToBeClickable();
     	Commonactions.isElementPresent(cp.getAttributeName());
     	ca.insertText(cp.getAttributeName(), RawMatNames[1]);
    	Commonactions.isElementPresent(cp.getDisplayName());
     	ca.insertText(cp.getDisplayName(), RawMatDispNames[1]);
       	Commonactions.isElementPresent(cp.getAttributeType());
    	ca.click(cp.getAttributeType());
		WebElement N4 = ca.activeElement();
		N4.sendKeys(Keys.DELETE);
		ca.eleToBeClickable();
		N4.sendKeys("string");
		ca.eleToBeClickable();
		ca.jsMouseOver();
		ca.eleToBeClickable();
		Commonactions.mouseOver(driver.findElement(By.xpath("//div[@data-csi-automation='field-ConfigurableAttribute-Form-Optional']/input")));
		ca.click(driver.findElement(By.xpath("//div[@data-csi-automation='field-ConfigurableAttribute-Form-Optional']/input")));
		ca.click(mp.getSave_btn1());
		ca.eleToBeClickable();
		System.out.println("Second Attribute is created ");
    }
    @Then("User Creates Raw Material Types {string}")
    public void User_Creates_Raw_Material_Types(String RawMatType) throws Throwable {
    	String[] RawMatTypes = RawMatType.split(",");
    	
    	List<WebElement> index = driver.findElements(By.xpath("//button[@aria-label='Scroll Right']"));
		int j = index.size();
		System.out.println(j);
		try{
		ca.eleToBeClickable();
		Commonactions.clickjs(driver.findElement(By.xpath("(//button[@aria-label='Scroll Right'])["+j+"]")));
	     }catch(Exception e){
	     }
		ca.eleToBeClickable();
    	for (int i = 0; i < RawMatTypes.length; i++) 
    	{
    	Commonactions.isElementPresent(cp.getRawMatTypeTab());
    	ca.click(cp.getRawMatTypeTab());
     	Commonactions.isElementPresent(cp.getNewRawMatTypeBtn());
    	ca.click(cp.getNewRawMatTypeBtn());
     	Commonactions.isElementPresent(cp.getRawMaterialTypeName());
    	ca.insertText(cp.getRawMaterialTypeName(),RawMatTypes[i]);
   		ca.click(mp.getSave_btn1());
		ca.eleToBeClickable();
    	}
    	//Setting the raw material types to active and checking the checkboxes for both Optional types for Raw material1
		Commonactions.isElementPresent(driver.findElement(By.xpath("//td[text()='"+RawMatTypes[0]+"']//following::td/div[@data-csi-act='Active:Child:SetupSettings:0']/input[1]")));
		Commonactions.clickjs(driver.findElement(By.xpath("//td[text()='"+RawMatTypes[0]+"']//following::td/div[@data-csi-act='Active:Child:SetupSettings:0']/input[1]")));
		
		//Commonactions.isElementPresent(driver.findElement(By.xpath("//td[text()='"+RawMatTypes[0]+"']//following::td[@data-csi-heading='Published::0'][1]/div/input[1]")));
		//ca.click(driver.findElement(By.xpath("//td[text()='"+RawMatTypes[0]+"']//following::td[@data-csi-heading='Published::0'][1]/div/input[1]")));
		
		Commonactions.isElementPresent(driver.findElement(By.xpath("(//td[text()='"+RawMatTypes[0]+"']//following::td/div[@data-csi-act='RawMaterialOptionalAttributes:Child:SetupSettings:0'][1]/input[1])[1]")));
		Commonactions.clickjs(driver.findElement(By.xpath("(//td[text()='"+RawMatTypes[0]+"']//following::td/div[@data-csi-act='RawMaterialOptionalAttributes:Child:SetupSettings:0'][1]/input[1])[1]")));
			
		Commonactions.isElementPresent(driver.findElement(By.xpath("(//td[text()='"+RawMatTypes[0]+"']//following::td/div[@data-csi-act='RawMaterialAttributesOptionalAttributes:Child:SetupSettings:0'][1]/input[1])[1]")));
		Commonactions.clickjs(driver.findElement(By.xpath("(//td[text()='"+RawMatTypes[0]+"']//following::td/div[@data-csi-act='RawMaterialAttributesOptionalAttributes:Child:SetupSettings:0'][1]/input[1])[1]")));

    	//Setting the raw material types to active and checking the checkboxes for first Optional types for Raw material2
		Commonactions.isElementPresent(driver.findElement(By.xpath("//td[text()='"+RawMatTypes[1]+"']//following::td/div[@data-csi-act='Active:Child:SetupSettings:0']/input[1]")));
		Commonactions.clickjs(driver.findElement(By.xpath("//td[text()='"+RawMatTypes[1]+"']//following::td/div[@data-csi-act='Active:Child:SetupSettings:0']/input[1]")));
		
		Commonactions.isElementPresent(driver.findElement(By.xpath("(//td[text()='"+RawMatTypes[1]+"']//following::td/div[@data-csi-act='RawMaterialOptionalAttributes:Child:SetupSettings:0'][1]/input[1])[1]")));
		Commonactions.clickjs(driver.findElement(By.xpath("(//td[text()='"+RawMatTypes[1]+"']//following::td/div[@data-csi-act='RawMaterialOptionalAttributes:Child:SetupSettings:0'][1]/input[1])[1]")));
		
		//Setting the raw material types to active and checking the checkboxes for second Optional types for Raw material3
		Commonactions.isElementPresent(driver.findElement(By.xpath("//td[text()='"+RawMatTypes[2]+"']//following::td/div[@data-csi-act='Active:Child:SetupSettings:0']/input[1]")));
		Commonactions.clickjs(driver.findElement(By.xpath("//td[text()='"+RawMatTypes[2]+"']//following::td/div[@data-csi-act='Active:Child:SetupSettings:0']/input[1]")));

		Commonactions.isElementPresent(driver.findElement(By.xpath("(//td[text()='"+RawMatTypes[2]+"']//following::td/div[@data-csi-act='RawMaterialAttributesOptionalAttributes:Child:SetupSettings:0'][1]/input[1])[1]")));
		Commonactions.clickjs(driver.findElement(By.xpath("(//td[text()='"+RawMatTypes[2]+"']//following::td/div[@data-csi-act='RawMaterialAttributesOptionalAttributes:Child:SetupSettings:0'][1]/input[1])[1]")));
		
		//Setting the raw material types to active and not checking the checkboxes for Optional types for Raw material4
		Commonactions.isElementPresent(driver.findElement(By.xpath("//td[text()='"+RawMatTypes[3]+"']//following::td/div[@data-csi-act='Active:Child:SetupSettings:0']/input[1]")));
		Commonactions.clickjs(driver.findElement(By.xpath("//td[text()='"+RawMatTypes[3]+"']//following::td/div[@data-csi-act='Active:Child:SetupSettings:0']/input[1]")));

		//Setting the raw material types to active and checking both the checkboxes for  Optional types for Raw material5
		Commonactions.isElementPresent(driver.findElement(By.xpath("//td[text()='"+RawMatTypes[4]+"']//following::td/div[@data-csi-act='Active:Child:SetupSettings:0']/input[1]")));
		Commonactions.clickjs(driver.findElement(By.xpath("//td[text()='"+RawMatTypes[4]+"']//following::td/div[@data-csi-act='Active:Child:SetupSettings:0']/input[1]")));
	
		Commonactions.isElementPresent(driver.findElement(By.xpath("(//td[text()='"+RawMatTypes[4]+"']//following::td/div[@data-csi-act='RawMaterialOptionalAttributes:Child:SetupSettings:0'][1]/input[1])[1]")));
		Commonactions.clickjs(driver.findElement(By.xpath("(//td[text()='"+RawMatTypes[4]+"']//following::td/div[@data-csi-act='RawMaterialOptionalAttributes:Child:SetupSettings:0'][1]/input[1])[1]")));
		
		Commonactions.isElementPresent(driver.findElement(By.xpath("(//td[text()='"+RawMatTypes[4]+"']//following::td/div[@data-csi-act='RawMaterialAttributesOptionalAttributes:Child:SetupSettings:0'][1]/input[1])[1]")));
		Commonactions.clickjs(driver.findElement(By.xpath("(//td[text()='"+RawMatTypes[4]+"']//following::td/div[@data-csi-act='RawMaterialAttributesOptionalAttributes:Child:SetupSettings:0'][1]/input[1])[1]")));
		
			//deleting the raw Material 5
		Commonactions.isElementPresent(driver.findElement(By.xpath("(//td[text()='"+RawMatTypes[4]+"']//following::td/div/span[@data-csi-act='Delete'])[1]")));
		Commonactions.clickjs(driver.findElement(By.xpath("(//td[text()='"+RawMatTypes[4]+"']//following::td/div/span[@data-csi-act='Delete'])[1]")));
		ca.eleToBeClickable();
	    ca.click(cp.getDelBtn());
		ca.eleToBeClickable();
	    System.out.println("The Delete button works and the Issue is Deleted");
    }
    @Then("User Clicks on ExternalImage Sources and Create Folder {string}")
    public void User_Clicks_on_ExternalImage_Sources_and_Create_Folder (String ImagePath) throws Throwable {
		Commonactions.isElementPresent(co.getUser_config());
		ca.click(co.getUser_config());
    	Commonactions.isElementPresent(cp.getExternalImageBtn());
    	ca.click(cp.getExternalImageBtn());
      	Commonactions.isElementPresent(cp.getNewFolderBtn());
    	ca.click(cp.getNewFolderBtn());
      	Commonactions.isElementPresent(cp.getFolderName());
    	ca.insertText(cp.getFolderName(),"Test");
     	Commonactions.isElementPresent(cp.getFolderPath());
      	ca.insertText(cp.getFolderPath(),ImagePath);
   		ca.click(mp.getSave_btn1());
		ca.eleToBeClickable();
    }  
    
}    
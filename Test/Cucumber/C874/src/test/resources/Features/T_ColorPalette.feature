#Kompal
@Selenium @ColorPalette

Feature: ColorPalette
   Scenario Outline: Create the ColorPalette Subtypes Under Configuration
    Given User launches centric application
		And User Click on setup icon
	  Then User Creates Color Palette SubTypes "<ColorPaletteType>","<Color Palette To Season>"
	  When User creates style types For Color Palette "<StyleTypes>"
	  When User creates material types For Color Palette "<MatTypes>","<MatTypesNames>"
		Then User click on update cnfiguration
		Then Logout from the Application
		

    Examples: 
      | ColorPaletteType            								|  Color Palette To Season  |	StyleTypes	|	MatTypesNames |		MatTypes |																  
      | All Seasons,Multi Seasons,Single Season,Test|All Seasons,Multiple Seasons,Single Season,Single Season| Enabled,Enforced,Off |EnabledM,EnforcedM,OffM | Enabled,Enforced,Off |
      

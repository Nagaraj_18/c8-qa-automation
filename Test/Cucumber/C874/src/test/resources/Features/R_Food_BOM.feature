@Selenium @FoodBOMSection @FoodNew
Feature: Food BOM and Datapackage creation




@FoodBOMNew1  
   Scenario Outline: create BOM under configuration  
     Given User launches centric application
     Then Go to homepage
      And user create new food bom types and create rolls,Recipe under configurtion "<Food>","<FoodType>","<RawMaterial>"
     Then User click on update cnfiguration
     Then Go to homepage
     Then user create new food in homepage"<NewFood>","<BOM1>"
    
    Examples: 
      |    Food   | FoodType                    | RawMaterial      |NewFood             |BOM1                                        |
      |Food BOM   | Rolls,Rolls1,New Recipe     |RecipeRawMaterial |Rolls,French Roast  |Food BOM,French Roast BOM1,French Roast BOM2|
      
   @FoodBOM_Datapackage 
   Scenario Outline: create Datapackage Under FoodBOM 
    Given User launches centric application
    Then Go to homepage
    And verify user screen
    Then user create Datapackage under FoodBOM "<Datapackage>"
    Then user create datapackage under Datapackagetemplate"<DPT>"
    Then user create FoodBoM Type under BOM Sections "<FoodBOMsection>","<Food1>"
    
    Examples:
    |Datapackage                          |DPT                       |FoodBOMsection|Food1|
    |Datapackage Manual,TDS View2,TDS View|DPT with Food BOM,Food BOM|Food Type     |Rolls,French Roast1,Food BOM,French Roast BOM3|

     @Recipe
   Scenario Outline: create Recipe under food in Homepage
    Given User launches centric application
    Then Go to homepage
    And verify user screen
    Then user create newfood and create recipe under food  "<Recipe>","<RawMaterial1>"
  Examples:
  |Recipe             | RawMaterial1|      
  |New Recipe,Margherita Pizza|RecipeRawMaterial,Wheat,Tomato,Milk|
@WizAndStyleBO @StyleUpdate @Calendar @Merchandise @HybridAgent @CollectionManagement @Conversation @Specification @StyleType 
Feature: Configuration Style Type Creation 
@StyleBOM @MatHierarchy @ShapeAndTheme @PC @BusinessPlanning @FoodSetupHeirarchy @Sourcing @Project @createissue
Scenario Outline: Style type  creation
    Given User launches centric application
    And User Click on setup icon
    When User creates style type "<StyleTypeA>","<StyleTypeB>","<StyleTypeC>","<StyleTypeD>"
    And User fill the style type attributes fields
    Then Logout from the Application

    Examples: 
      | StyleTypeA               | StyleTypeB               | StyleTypeC          | StyleTypeD                      |
      | Apparel - Color and Size | Accessories - Only Color | Apparel - Only Size | Accessories - No color and Size |

@BOMSizeSpecTestDoc @Quality @StyleUpdate 

Feature: Creation for BOM size spec  doc and test type
@StyleBOM @ShapeAndTheme @Calendar @FoodSetupHeirarchy @MaterialSpecifBOM @BOMType
Scenario Outline: BOM creation
    Given User launches centric application
    And User Click on setup icon
    And User creates BOM type "<StyleBOM>","<MaterialBOM>"
   # Then Logout from the Application
    Examples: 
      | StyleBOM    | MaterialBOM  |
      | Apparel BOM | Material BOM |

  #-----------------------------------------------------------------------------------------------------------------
@ShapeAndTheme @MatHierarchy 
  Scenario Outline: Size Chart creation
    Given User launches centric application
    And User Click on setup icon
    And User creates size chart type "<All>","<List>","<Values>","<Tolerance>"
	Then Logout from the Application
	

    Examples: 
      | All         | List              | Values         | Tolerance         |
      | SC-Lock All | SC-Lock Dimension | SC-Lock Values | SC-Lock Tolerance |

  #-----------------------------------------------------------------------------------------------------------------
  @MatHierarchy @FoodSetupHeirarchy @SpecType
  Scenario Outline: Spec type creation
    Given User launches centric application
    And User Click on setup icon
    And User creates spec type "<SpecTypeA>","<SpecTypeB>"
	Then Logout from the Application

    Examples: 
      | SpecTypeA    | SpecTypeB    |
      | Spec Type-01 | Spec Type-02 |

  #-----------------------------------------------------------------------------------------------------------------
  @MatHierarchy 
  Scenario Outline: Test type creation
    Given User launches centric application
    And User Click on setup icon
    And User creates test type "<TestTypeA>","<TestTypeB>","<TestTypeC>"
    And User performing delete action
	Then Logout from the Application

    Examples: 
      | TestTypeA | TestTypeB     | TestTypeC    |
      | Wash Test | Chemical Test | Ironing Test |

	#-----------------------------------------------------------------------------------------------------------------
  Scenario Outline: Contractual documents creation
    Given User launches centric application
    And User Click on setup icon
    And User creates contractual documents "<ContrctAll>","<Simple>","<RA>","<HED>","<HRD>"
	Then Logout from the Application

    Examples: 
      | ContrctAll | Simple      | RA      | HED      | HRD      |
      | CDOC-ALL   | CDOC-simple | CDOC-RA | CDOC-HED | CDOC-HRD |

 @Specification
 Scenario Outline: Look up items creation
    Given User launches centric application
    And User Click on setup icon
    And User creates LookUp items "<LookUpA>","<LookUpB>","<LookUpC>"
	Then Logout from the Application

    Examples: 
      | LookUpA      | LookUpB          | LookUpC              |
      | Cost Factors | Holiday Calendar | User Task Lead Times |

  #-----------------------------------------------------------------------------------------------------------------
  @ShapeAndTheme @StyleBOM
  Scenario Outline: Theme Master creation
    Given User launches centric application
    And User Click on setup icon
    And User creates theme master "<All>","<single>","<multiple>","<AVsingle>"
	Then Logout from the Application

    Examples: 
      | All           | single           | multiple           | AVsingle                    |
      | TM-All Season | TM-Single Season | TM-Multiple Season | TM-Allow material variation |

  #-----------------------------------------------------------------------------------------------------------------
  @ShapeAndTheme @Enumeration @StyleBOM
  Scenario Outline: Update ColorRole in Enumeration
    Given User launches centric application
    And User Creates ColorROle in Enumeration "<materialname>","<Themename>","<familyname1>","<familyname2>","<Bluecolor>","<Redcolor>","<Themevalue1>","<Themevalue2>",
	Then Logout from the Application
    Examples: 
      | materialname   | Themename              | familyname1 | familyname2 | Bluecolor | Redcolor  | Themevalue1 | Themevalue2 |
      | MaterialFamily | ThemeMainMaterialGroup | Family-01   | Family-02   | Family-01 | Family-02 | Group-01    | Group-02    |

  
  @ColorwayAttributegrp @StyleBOM
  Scenario:Create colorway attribute group 
 	Given User launches centric application
    And User Click on setup icon
    Then Navigate to business object and create colorway attribute group
    Then Logout from the Application
  
  
  
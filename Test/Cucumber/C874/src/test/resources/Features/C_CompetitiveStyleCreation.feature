#Author: Kompal@kripya.com
@Selenium @CompetitiveStyle @StyleUpdate
Feature: Creation of CompetitiveStyle

  Scenario Outline: User creates New Competitive Style
    Given User launches centric application
    Then Go to homepage
    And verify user screen
    And Click style tab and get the listed season name in the style tab
    And Create a Competitive Style and Verify the Save Cancel SaveCopy SaveGo on Pop Up with Edit Delete and Copy options for Node "<CompStyleName>"
    And Verify the Tabs under Competitive Styles 
    And Add Documents Under Documents Tab 
    Then Logout from the Application

    Examples:
      | CompStyleName        | 
      | Aut_CompetitiveStyleName1,Aut_CompetitiveStyleName2 |
    

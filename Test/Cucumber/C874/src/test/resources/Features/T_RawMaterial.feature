@Selenium @RawMaterial @Tag2

Feature: RawMaterial
   Scenario Outline: Create the Raw Material Under Foods Tabs using the Raw Material Types Created
    Given User launches centric application
    Then Go to homepage
    And Navigate to RawMaterial Tab 
    And Create A Custom View to Add Optional Attributes
    And Create the Raw Materials "<RawMaterials>","<Raw Material Types>"
    And Editing and Deleting the Raw Materials "<RawMaterials>"
    And Verifying the Properties Tab for Raw Materials "<RawMaterials>"
    And Adding Image in Properties Tab for Raw Material "<RawMaterials>"
    And Verifying the Documents Tab for Raw Materials and Delete "<RawMaterials>"
    Then user share and validate the document for RawMaterials "<RawMaterials>"
    Then user Add Referenced Document the document Tab "<RawMaterials>"
    Then user Add Conversations in the Raw Material "<RawMaterials>"
 
 
    Examples: 
      | RawMaterials            										            |  Raw Material Types  |																						  
      | RawMatUPandStr,RawMatUP,RawMatStr,RawMatNone,RawForDele |RawMaterialType1,RawMaterialType2,RawMaterialType3,RawMaterialType4,RawMaterialType1 | 
  
  
@MaterialType @Calendar @MatHierarchy @Conversation @HybridAgent @CollectionManagement  
Feature: Configuration Material Type Creation
@StyleBOM @ShapeAndTheme @PC @BusinessPlanning @Project @MaterialSpecifBOM @StyleUpdate @Projects
Scenario Outline: Material type creation
    Given User launches centric application
    And User Click on setup icon
    And User creates material type "<Standalone>","<Structurecomponent>","<Tool>"
    Then Logout from the Application

    Examples: 
      | Standalone          | Structurecomponent           | Tool          |
      | Fabric - Stanadlone | Fabric - Structure Component | Fabric - Tool |

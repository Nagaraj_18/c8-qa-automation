#Author: karthick@kripya.com
@Selenium @UpdateConfiguration @Tag1 @Tag2 @Group1 @Group2 @Group3 @Group4
Feature: Configuration and Setup


  Scenario Outline: Style type  creation
    Given User launches centric application
    Then User click on update cnfiguration
    And User Click on setup icon
    When User creates style type "<StyleTypeA>","<StyleTypeB>","<StyleTypeC>","<StyleTypeD>"
    And User fill the style type attributes fields

    Examples: 
      | StyleTypeA               | StyleTypeB               | StyleTypeC          | StyleTypeD                      |
      | Apparel - Color and Size | Accessories - Only Color | Apparel - Only Size | Accessories - No color and Size |

  #-----------------------------------------------------------------------------------------------------------------
  Scenario Outline: Material type creation
    #Given User launches centric application
    And User Click on setup icon
    And User creates material type "<Standalone>","<Structurecomponent>","<Tool>"

    Examples: 
      | Standalone          | Structurecomponent           | Tool          |
      | Fabric - Stanadlone | Fabric - Structure Component | Fabric - Tool |

  #-----------------------------------------------------------------------------------------------------------------
  Scenario Outline: BOM creation
     #Given User launches centric application
    And User Click on setup icon
    And User creates BOM type "<StyleBOM>","<MaterialBOM>"

    Examples: 
      | StyleBOM    | MaterialBOM  |
      | Apparel BOM | Material BOM |

  #-----------------------------------------------------------------------------------------------------------------
  Scenario Outline: Size Chart creation
      #Given User launches centric application
    And User Click on setup icon
    And User creates size chart type "<All>","<List>","<Values>","<Tolerance>"

    Examples: 
      | All         | List              | Values         | Tolerance         |
      | SC-Lock All | SC-Lock Dimension | SC-Lock Values | SC-Lock Tolerance |

  #-----------------------------------------------------------------------------------------------------------------
  Scenario Outline: Spec type creation
     #Given User launches centric application
    And User Click on setup icon
    And User creates spec type "<SpecTypeA>","<SpecTypeB>"

    Examples: 
      | SpecTypeA    | SpecTypeB    |
      | Spec Type-01 | Spec Type-02 |

  #-----------------------------------------------------------------------------------------------------------------
  Scenario Outline: Test type creation
     #Given User launches centric application
    And User Click on setup icon
    And User creates test type "<TestTypeA>","<TestTypeB>","<TestTypeC>"
    And User performing delete action

    Examples: 
      | TestTypeA | TestTypeB     | TestTypeC    |
      | Wash Test | Chemical Test | Ironing Test |

  #-----------------------------------------------------------------------------------------------------------------
  Scenario Outline: Inspection type creation
     #Given User launches centric application
    And User Click on setup icon
    And User creates inspection type "<InspnoLink>","<BOM>","<SizeChart>","<SpecDS>","<StyleReview>"

    Examples: 
      | InspnoLink          | BOM             | SizeChart              | SpecDS                      | StyleReview              |
      | Ins Section-No Link | Ins Section-BOM | Ins Section-Size Chart | Ins Section-Spec Data Sheet | Ins Section-Style Review |

  #-----------------------------------------------------------------------------------------------------------------
  Scenario Outline: Contractual documents creation
     #Given User launches centric application
    And User Click on setup icon
    And User creates contractual documents "<ContrctAll>","<Simple>","<RA>","<HED>","<HRD>"

    Examples: 
      | ContrctAll | Simple      | RA      | HED      | HRD      |
      | CDOC-ALL   | CDOC-simple | CDOC-RA | CDOC-HED | CDOC-HRD |

  #-----------------------------------------------------------------------------------------------------------------
  Scenario Outline: Look up items creation
     #Given User launches centric application
    And User Click on setup icon
    And User creates LookUp items "<LookUpA>","<LookUpB>","<LookUpC>"

    Examples: 
      | LookUpA      | LookUpB          | LookUpC              |
      | Cost Factors | Holiday Calendar | User Task Lead Times |

  #-----------------------------------------------------------------------------------------------------------------
  Scenario Outline: Theme Master creation
     #Given User launches centric application
    And User Click on setup icon
    And User creates theme master "<All>","<single>","<multiple>","<AVsingle>"

    Examples: 
      | All           | single           | multiple           | AVsingle                    |
      | TM-All Season | TM-Single Season | TM-Multiple Season | TM-Allow material variation |

  #-----------------------------------------------------------------------------------------------------------------
  @Enumeration
  Scenario Outline: Update ColorRole in Enumeration
     #Given User launches centric application
    And User Creates ColorROle in Enumeration "<materialname>","<Themename>","<familyname1>","<familyname2>","<Bluecolor>","<Redcolor>","<Themevalue1>","<Themevalue2>",

    Examples: 
      | materialname   | Themename              | familyname1 | familyname2 | Bluecolor | Redcolor  | Themevalue1 | Themevalue2 |
      | MaterialFamily | ThemeMainMaterialGroup | Family-01   | Family-02   | Family-01 | Family-02 | Group-01    | Group-02    |

  @FoodTypeConfig
  Scenario Outline: FoodTypes and DataSheet creation
    Given User launches centric application
    And User Click on setup icon
    And User Creates FoodTypes and FoodlabelDatasheetvalues "<FoodType>","<FoodLabel>"
    Then User click on update cnfiguration
    Examples: 
      | FoodType                                                               | FoodLabel                                           |
      | Pizza,Cat Food,Dog Food,Fruits and Vegetables,Fast Food,No Data sheets | Food Additives, Colours and Flavors,No Data section |
 
 @GTIN
   Scenario Outline: GTIN And Sales Region creation
    #Given User launches centric application
    And User Click on setup icon
    And User Clicks on Company Tab and Create GTIN "<ManufacturerName>","<ManufacturerCode>"
    And User Creates Sales Region "<SalesRegionName>"
 Examples: 
      | ManufacturerName  | ManufacturerCode | SalesRegionName |           
      | JubilientFoods    | 12345            | India-South     | 
 
 @ProjectType
  Scenario Outline: ProjectType creation
    #Given User launches centric application
    And User Click on setup icon
     Then Navigate to Project Types and Create ProjectType "<ProjectType>"
     Then User click on update cnfiguration
     Then Verify the Project Type Drop Down "<ProjectType>"
    And User Create WBS projects "Project","Task"

    Examples: 
      | ProjectType                                                                             |
      | Project-ALL,Project-Style,Project-Food,Project-Material,Project-Blank,Project-ForDelAct |

@ColorwayAttributegrp
 Scenario:Create colorway attribute group 
 	  Given User launches centric application
    And User Click on setup icon
    Then Navigate to business object and create colorway attribute group
  #-----------------------------------------------------------------------------------------------------------------
  @ProjectType
  Scenario: Update Configuration
    #Given User launches centric application
    And User Click on setup icon
    Then User click on update cnfiguration
    
  #-----------------------------------------------------------------------------------------------------------------
@AddingImageDirectory
  Scenario Outline: Creation of Business Objects and Raw Material Types
    Given User launches centric application
    And User Click on setup icon
    And User Clicks on ExternalImage Sources and Create Folder "<Image Path>"
Examples:
      | Image Path  |
      |  C:\Test\Images |

@RawMaterialconfiguration
  Scenario Outline: Creation of Business Objects and Raw Material Types
    Given User launches centric application
    And Go To Business Objects Tab
    Then Create Optional Attribute for Raw Material and Raw Material Attributes "<Raw Material Attributes Name>","<Raw Material Attributes Display Name>"
		Then User click on update cnfiguration
		Then Go to homepage
		And User Click on setup icon
		Then User Creates Raw Material Types "<Raw Material Types>"
		Then User click on update cnfiguration
		
   Examples:
      | Raw Material Attributes Name  |Raw Material Attributes Display Name|Raw Material Types 																																	|
      | rm_UnitPrice,rmAttr_String    |OptAttrUnitPrice,OptAttrString      |RawMaterialType1,RawMaterialType2,RawMaterialType3,RawMaterialType4,RawMaterialType5 |

  @Nativeimport
  Scenario: Creation of native import
    #Given User launches centric application
    Then Go to homepage
    Then Go to setup icon and create native import jobs
    Then Logout from the Application
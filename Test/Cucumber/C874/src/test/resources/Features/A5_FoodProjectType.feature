@FoodAndProjectTypeConfigCreation

Feature: Config creation for food and project
@FoodTypeConfig @FoodSetupHeirarchy @Project
  Scenario Outline: FoodTypes and DataSheet creation
    Given User launches centric application
    And User Click on setup icon
    And User Creates FoodTypes and FoodlabelDatasheetvalues "<FoodType>","<FoodLabel>"
    Then User click on update cnfiguration
    Examples: 
      | FoodType                                                               | FoodLabel                                           |
      | Pizza,Cat Food,Dog Food,Fruits and Vegetables,Fast Food,No Data sheets | Food Additives, Colours and Flavors,No Data section |
 
 @GTIN @FoodSetupHeirarchy @Project
   Scenario Outline: GTIN And Sales Region creation
    #Given User launches centric application
    And User Click on setup icon
    And User Clicks on Company Tab and Create GTIN "<ManufacturerName>","<ManufacturerCode>"
    And User Creates Sales Region "<SalesRegionName>"
 Examples: 
      | ManufacturerName  | ManufacturerCode | SalesRegionName |           
      | JubilientFoods    | 12345            | India-South     | 
 
 @ProjectType @FoodSetupHeirarchy @Project
  Scenario Outline: ProjectType creation
    #Given User launches centric application
    And User Click on setup icon
     Then Navigate to Project Types and Create ProjectType "<ProjectType>"
     Then User click on update cnfiguration
     Then Verify the Project Type Drop Down "<ProjectType>"
     And User Create WBS projects "Project","Task"
     Then Logout from the Application

    Examples: 
      | ProjectType                                                                             |
      | Project-ALL,Project-Style,Project-Food,Project-Material,Project-Blank,Project-ForDelAct |

@AddingImageDirectory
  Scenario Outline: Creation of Business Objects and Raw Material Types
    Given User launches centric application
    And User Click on setup icon
    And User Clicks on ExternalImage Sources and Create Folder "<Image Path>"
Examples:
      | Image Path  |
      |  C:\Test\Images |

@RawMaterialconfiguration
  Scenario Outline: Creation of Business Objects and Raw Material Types
    Given User launches centric application
    And Go To Business Objects Tab
    Then Create Optional Attribute for Raw Material and Raw Material Attributes "<Raw Material Attributes Name>","<Raw Material Attributes Display Name>"
		Then User click on update cnfiguration
		Then Go to homepage
		And User Click on setup icon
		Then User Creates Raw Material Types "<Raw Material Types>"
		Then User click on update cnfiguration
		
   Examples:
      | Raw Material Attributes Name  |Raw Material Attributes Display Name|Raw Material Types 																																	|
      | rm_UnitPrice,rmAttr_String    |OptAttrUnitPrice,OptAttrString      |RawMaterialType1,RawMaterialType2,RawMaterialType3,RawMaterialType4,RawMaterialType5 |


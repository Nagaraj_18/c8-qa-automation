$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/Features/JJ_Inspection.feature");
formatter.feature({
  "name": "Inspection_creation",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Selenium"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Inspection Creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@inspection"
    },
    {
      "name": "@JenkinsStyle"
    }
  ]
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.step({
  "name": "user creates setup in Inspection \"\u003cDefects\u003e\",\"\u003cAccLimit\u003e\",\"\u003csortOrder\u003e\",\"\u003cTestSpecification\u003e\",\"\u003cSamplingValue\u003e\",\"\u003cErrorType\u003e\",\"\u003cTemplateName\u003e\",\"\u003cBOMName\u003e\",\"\u003cSizeChart\u003e\",\"\u003cDimensioName\u003e\",\"\u003cInspectionGrpName\u003e\"",
  "keyword": "When "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Defects",
        "AccLimit",
        "sortOrder",
        "TestSpecification",
        "SamplingValue",
        "ErrorType",
        "TemplateName",
        "BOMName",
        "SizeChart",
        "DimensioName",
        "InspectionGrpName"
      ]
    },
    {
      "cells": [
        "High,Medium,Low,VeryLow",
        "ACL 1.0,ACL 2.0,ACL 3.0",
        "0,1,2",
        "No Link,BOM,Size Chart,Spec Data Sheet,Style Review",
        "SP 1.0,SP 2.0,SP 3.0",
        "Full Error,Batch Error,Check Error",
        "auto-inspection",
        "Inspection BOM",
        "Ins Special",
        "New Dimension",
        "New Inspection Group"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Inspection Creation",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@Selenium"
    },
    {
      "name": "@inspection"
    },
    {
      "name": "@JenkinsStyle"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User launches centric application",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginPageSteps.user_launches_centric_application()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Go to homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "HomePageSteps.go_to_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "verify user screen",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageSteps.verify_user_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user creates setup in Inspection \"High,Medium,Low,VeryLow\",\"ACL 1.0,ACL 2.0,ACL 3.0\",\"0,1,2\",\"No Link,BOM,Size Chart,Spec Data Sheet,Style Review\",\"SP 1.0,SP 2.0,SP 3.0\",\"Full Error,Batch Error,Check Error\",\"auto-inspection\",\"Inspection BOM\",\"Ins Special\",\"New Dimension\",\"New Inspection Group\"",
  "keyword": "When "
});
formatter.match({
  "location": "InspectionPageSteps.user_creates_setup_in_Inspection(String,String,String,String,String,String,String,String,String,String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.embedding("image/png", "embedded0.png");
formatter.after({
  "status": "passed"
});
});
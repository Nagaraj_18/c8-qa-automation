cd C:\Test\Import\C872
@set CENTRIC_DIR=C:\Program Files\Centric Software\C8
@set JAVA_HOME=C:\PROGRA~1\CENTRI~1\C8\jdk-11.0.7+10

@set LIB_DIR=%CENTRIC_DIR%\Wildfly-20.0.0\modules\com\centricsoftware\pi\main
@set classpath=%LIB_DIR%\main\pi-module-export.jar
@set classpath=%classpath%;%LIB_DIR%\libs\opencsv-2.3.jar
@set classpath=%classpath%;%CENTRIC_DIR%\Wildfly-20.0.0\modules\com\oracle\db\main\jdbc-driver-18.3-jdk8.jar
@set classpath=%classpath%;%LIB_DIR%\libs\aspectjrt-1.9.6.jar
@set classpath=%classpath%;%LIB_DIR%\libs\commons-fileupload-1.3.2.jar
@set classpath=%classpath%;%LIB_DIR%\libs\jdom2-2.0.6.jar
@set classpath=%classpath%;%LIB_DIR%\libs\rhino-1.7.7.jar
@set classpath=%classpath%;%LIB_DIR%\libs\commons-lang-2.4.jar
@set classpath=%classpath%;%LIB_DIR%\main\pi-base.jar
@set classpath=%classpath%;%LIB_DIR%\main\pi-service-base.jar
@set classpath=%classpath%;%LIB_DIR%\main\pi-modelinterface.jar
@set classpath=%classpath%;%LIB_DIR%\main\pi-module-siteadmin.jar

@"%JAVA_HOME%\bin\java" com.centricsoftware.pi.export.impl.run.Importer files.cfg localhost:8080 Administrator centric8 CSIDBA CSIDBA CSIC8 localhost:1521 Oracle false

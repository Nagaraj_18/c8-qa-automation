'use strict';
const puppeteer = require('puppeteer');
var fs = require("fs");
var path = require('path');

function delay(time) {
    return new Promise(function (resolve) {
        setTimeout(resolve, time)
    });
}

var checkThePageTitle = async function (page) {
    console.log('Checking the Webpage Title');
    return page.evaluate(() => {
        return new Promise((resolve, reject) => {
            try {
                const interval = setInterval(() => {
                    if (document.title == 'Done' || document.title == 'Error') {
                        clearInterval(interval);
                        resolve(document.title);
                    }
                }, 100);
            } catch (error) {
                resolve('Unknown');
            }
        });
    });
};

function decodeHTMLEntities(text) {
    var entities = [
        ['amp', '&'],
        ['apos', '\''],
        ['#x27', '\''],
        ['#x2F', '/'],
        ['#39', '\''],
        ['#47', '/'],
        ['lt', '<'],
        ['gt', '>'],
        ['nbsp', ' '],
        ['quot', '"']
    ];

    for (var i = 0, max = entities.length; i < max; ++i) {
        text = text.replace(new RegExp('&' + entities[i][0] + ';', 'g'), entities[i][1]);
    }

    return text;
}

var writeXMLContent = function (xmlContent, xmlFileName) {
    return new Promise(function (resolve, reject) {
        fs.writeFile(xmlFileName, '<?xml version="1.0" encoding="UTF-8"?>' + decodeHTMLEntities(xmlContent), function (err) {
            if (err) reject(err.message);
            else resolve(xmlFileName);
        });
    });
};

(async () => {

    console.log('URL LAUNCHER');

    var start = new Date();
    console.log('Current Time : ' + start.getHours() + ':' + start.getMinutes() + ':' + start.getSeconds());

    if (process.argv.length < 7) {
        console.log('Missing/Invalid Arguments!');
        console.log('Usage : node urlLauncher.js "Server URL" "Username" "Password" "Launch URL" "XML Filename" ');
        process.exit(0);
    }

    var serverName = process.argv[2];
    var userName = process.argv[3];
    var passWord = process.argv[4];
    var launchUrl = process.argv[5];
    var xmlFileName = process.argv[6];

    var puppeteerSettings = {
        dumpio: true,
        headless: true,
        scrollPage: true,
        timeout: 300000,
        args: ['--no-sandbox', '--disable-setuid-sandbox', '--start-maximized', '--auto-open-devtools-for-tabs']
    };

    console.log('Launching Chromium!');
    var chromium = await puppeteer.launch(puppeteerSettings);

    console.log('Creating a new Chromium Page!');
    const page = await chromium.newPage();

    console.log('Loading the Login URL');
    var data = 'Module=DataSource&Operation=SimpleLogin&Fmt.Node.Info=Min&Fmt.Attr.Info=ValueOnly&Fmt.Attr.Format=Yes&Fmt.Attr.Locale=en&LoginID=' + userName + '&Password=' + passWord;
    await page.goto(serverName + '/csi-requesthandler/RequestHandler?' + data, {
        waitUntil: 'networkidle2'
    });

    console.log('Loading the Given URL : ' + launchUrl);
    await page.goto(launchUrl, {
        waitLoad: true,
        waitNetworkIdle: true
    });

    console.log('Awaiting for the Images to Load');
    const imgs = await page.$$eval('img', imgs => Promise.all(
        imgs.map(img => new Promise(resolve => img.onload = resolve))
    ));

    const webPageTitle = await checkThePageTitle(page);
    console.log('WebPage Title : ' + webPageTitle);
    if (webPageTitle != 'Done') {
        await page.waitFor(1000);
        var csiError = await page.$eval('body',
            body => body.hasAttribute('csi_error') ? body.getAttribute('csi_error') : 'Failed to load the page!');
        console.log('CSI Error : %s', csiError);

        if (await page.$('#csiErrorStackTrace') !== null) {
            var csiErrorTrace = await page.$eval('#csiErrorStackTrace', csiErrorTrace => csiErrorTrace.innerHTML);
            console.log('CSI Error Trace : %s', csiErrorTrace);
        }
    }

    console.log('Writing the XML');
    if (await page.$('#requestContent') !== null) {
        var requestContent = await page.$eval('#requestContent', requestContent => requestContent.innerHTML);
        await writeXMLContent(requestContent, xmlFileName);
    }

    console.log('Closing Chrome...');
    await chromium.close();

    var finish = new Date();
    console.log('Current Time : ' + finish.getHours() + ':' + finish.getMinutes() + ':' + finish.getSeconds());
})()
.catch(error => {
    console.error(error.message);
    process.exit(1);
});

$ProgressPreference = 'SilentlyContinue'
$artifactUrlBase = 'http://jenkins01.centricsoftware.com:8080/job/c8-installer/lastSuccessfulBuild/artifact/media/'
#$artifactUrlBase = 'http://jenkins01.centricsoftware.com:8080/job/c8-installer-release/'

function checkmd5 {
  param([string]$f)
  $fileHash = $regex.Matches((Get-FileHash -Algorithm MD5 $f | Format-List | Out-String)).Groups[1].Value.Trim().toLower()
  $pat = $fileHash + " *" + $f
  if (Select-String -Path md5sums -Pattern $pat -SimpleMatch -Quiet) {
    return $true
  } else {
    return $false
  }
}

if (-not (Test-Path "media")) {
 New-Item -Path "media" -ItemType Directory -ErrorAction stop
}
Set-Location -Path media -ErrorAction stop

foreach ($a in @("latest_release", "md5sums", "output.txt", "updates.xml")) {
  Invoke-WebRequest $artifactUrlBase$a -OutFile $a -ErrorAction stop
}

$release = Get-Content -Path latest_release -ErrorAction Stop
Write-Output "Latest release is $release"
[regex]$regex = 'Hash.*:\s+(.*)'

foreach ($f in @("c8apps_${release}.exe", "c8server_${release}.exe")) {
  if (Test-Path $f) {
    Write-Output "Found $f"
    if (checkmd5($f)) {
      continue
    } else {
      Write-Output "File is corrupt, downloading it again"
      Remove-Item $f
    }
  }
  Write-Output "Downloading $f"
  Invoke-WebRequest $artifactUrlBase$f -OutFile $f -ErrorAction stop
  $fileHash = $regex.Matches((Get-FileHash -Algorithm MD5 $f | Format-List | Out-String)).Groups[1].Value.Trim().toLower()
  $pat = $fileHash + " *" + $f
  if (-not (checkmd5($f))) {
    Write-Error "$f has an invalid md5sum of $fileHash" -Category InvalidData
    cd ..
    exit 1
  }
}

cd ..

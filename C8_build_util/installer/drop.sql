set serveroutput on;
DECLARE 
    v_csiUser       VARCHAR2(50) := UPPER('&&1');
	  v_SQL           VARCHAR2(500);
BEGIN

    DBMS_OUTPUT.PUT_LINE('Prepare to drop a user: ' || v_csiUser);
    
    FOR cur_rec IN ( select sid, serial# from v$session where username = v_csiUser ) LOOP
        DBMS_OUTPUT.PUT_LINE('sid:' || cur_rec.sid || '        serial#:' || cur_rec.serial#);    
        v_SQL := 'ALTER SYSTEM KILL SESSION ''' || cur_rec.sid || ', ' || cur_rec.serial# || '''' ;  
        DBMS_OUTPUT.PUT_LINE(v_SQL);   
        EXECUTE IMMEDIATE v_SQL;		
		
    END LOOP;
    
    v_SQL := 'drop user ' || v_csiUser||  ' cascade';
	EXECUTE IMMEDIATE v_SQL;
  
END;
/
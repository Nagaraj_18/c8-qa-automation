import os
import platform
import pywintypes
import shlex
import shutil
import subprocess
import sys
import time
import win32service
import win32serviceutil

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

deployDir = 'C:\Program Files\Centric Software\C8\Wildfly-20.0.0\standalone\deployments'
dirsToCreate = ('logs', 'D:\SQLDATA', 'L:\SQLLOG', 'T:\SQLTEMP', 'F:\FILEVAULT')
httpServer = 'localhost'
httpPort = '8080'
logDir = "logs"
serviceName = 'WFAS20SVC'
sqlUser = 'sa'
sqlPass = 'csisa'
adminPassword = 'centric8'
timeStamp = time.strftime('%y%m%d_%H%M%S')


def runCommand(cmd, communicate=False):
    r = 0
    if (communicate):
        #Doesn't really work with install4j apps
        p = subprocess.Popen(shlex.split(cmd), 0, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p.communicate()
        r = p.returncode
    else:
        try:
            cmdOutput = subprocess.check_output(cmd)
        except subprocess.CalledProcessError as e:
            sys.stderr.write('"%s" failed with return code %d\n' % (cmd, e.returncode))
            r = e.returncode
    return(r)

def waitForStatus(serviceName, desiredStatus, timeout=30):
    waitTime = 0.0
    while True:
        serviceStatus = win32serviceutil.QueryServiceStatus(serviceName, None)
    if serviceStatus[1] == desiredStatus:
        return(0)
    time.sleep(0.5)
    waitTime += 0.5
    if waitTime >= timeout:
        return(1)

print('\nStarting apps installation at %s' % time.strftime('%c', time.localtime()))

#if platform.system() != 'Windows':
#    sys.stderr.write("This script only runs on Windows\n")
#    exit(1)

if len(sys.argv) > 1:
    release = sys.argv[1]
else:
    with open('media/latest_release') as f:
        release = f.read().rstrip()
print('\nUsing release %s' % release)
if not os.path.exists('media/c8apps_%s.exe' % release):
    print('\nUnable to find application installer c8apps_%s.exe' % release)
    exit(1)

print("\nWaiting for server")
time.sleep(20)
#driver = webdriver.Chrome()
#driver.implicitly_wait(10)
#driver.get('http://%s:%s/WebAccess/login.html' % (httpServer, httpPort))
#
#try:
#    element = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "LoginID")))
#except Exception as e:
#    print(e)
#    driver.quit()
#    exit(1)
#loginInput = driver.find_element_by_xpath('//*[@id="LoginID"]')
#passwordInput = driver.find_element_by_xpath('//*[@id="Password"]')
#loginButton = driver.find_element_by_xpath('/html/body/div[1]/div[1]/form/div/div[2]/div[3]/div/span[2]/span')
#loginInput.send_keys('Administrator');
#passwordInput.send_keys(adminPassword);
#time.sleep(1)
#
#loginButton.click();
#time.sleep(10)
#
#driver.quit()

#print("\nSkipping application install")
#exit(0)

print('\nInstalling Applications')
dirpath = os.getcwd()
print("current directory is : " + dirpath)
os.chdir('media')
#retval = runCommand('c8apps_%s -q -console -overwrite -Dinstall4j.keepLog=true -Dinstall4j.alternativeLogfile=../logs/c8apps.log -Dinstall4j.logToStderr=true -varfile ../c8apps.varfile' % release, True)
print('\nrun apps installer')
dirpath = os.getcwd()
print("current directory is : " + dirpath)
retval = runCommand('c8apps_%s -q -console -overwrite -Dinstall4j.keepLog=true -Dinstall4j.logToStderr=true -varfile ../c8apps.varfile' % release, True)
os.chdir('..')
print('\nexit ...')
if retval != 0: exit(retval)

time.sleep(5)
print('\nApplication installation complete at %s' % time.strftime('%c', time.localtime()))

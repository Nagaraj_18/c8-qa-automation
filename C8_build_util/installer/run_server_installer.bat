@echo off
cd %SYSTEMROOT%/../C8_build_util/installer

if exist logs\ (
  del /q /f logs\*
) else (
  mkdir logs
)
powershell -File download.ps1
python install_server.py

rem move /y %TEMP%\i4j_log_c8server*.log logs
rem move /y %TEMP%\i4j_log_c8apps*.log logs
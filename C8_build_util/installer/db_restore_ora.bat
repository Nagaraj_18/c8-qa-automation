set user=CSIDBA
set file=CSIDBA.DMP
echo import Oracle database %user%


sqlplus sys/Manager7@csic8 as sysdba @user.sql %user%

impdp system/Manager7@csic8 schemas=CSIDBA directory=DMPDIR transform=oid:N  dumpfile=%file% logfile=IMP_%file%.log
sqlplus %user%/%user%@csic8 @updateSequence.sql

sqlplus system/Manager7@csic8 @dbms.sql %user%



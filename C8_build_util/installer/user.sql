SET SERVEROUTPUT ON

/*drop user*/
DECLARE

    CURSOR c1 IS
		SELECT s.sid, s.serial# FROM gv$session s JOIN gv$process p ON p.addr = s.paddr AND p.inst_id = s.inst_id
		WHERE   s.username =UPPER('&&1');
		
BEGIN
	FOR c1_rec IN c1
    LOOP
        
        EXECUTE IMMEDIATE 'ALTER SYSTEM KILL SESSION ''' || c1_rec.sid || ',' || c1_rec.serial# || '''';
    END LOOP;
	
END;
/

DROP USER "&&1"  CASCADE;


CREATE USER "&&1" PROFILE "DEFAULT" IDENTIFIED BY "&&1" DEFAULT TABLESPACE "CENTRICHUB" TEMPORARY TABLESPACE "TEMP" ACCOUNT UNLOCK;
GRANT ALTER SYSTEM TO "&&1";
GRANT CREATE PROCEDURE TO "&&1";
GRANT CREATE SEQUENCE TO "&&1";
GRANT CREATE TABLE TO "&&1";
GRANT CREATE TABLESPACE TO "&&1";
GRANT CREATE TRIGGER TO "&&1";
GRANT CREATE TYPE TO "&&1";
GRANT CREATE VIEW TO "&&1";
GRANT DROP TABLESPACE TO "&&1";
GRANT FORCE ANY TRANSACTION TO "&&1";
GRANT SELECT ANY DICTIONARY TO "&&1";
GRANT UNLIMITED TABLESPACE TO "&&1";
GRANT "CONNECT" TO "&&1";
GRANT "RESOURCE" TO "&&1";
grant execute on sys.dbms_lock TO "&&1";
grant debug connect session to "&&1";
grant debug any procedure to "&&1";
grant EXECUTE any procedure to "&&1";

conn &&1/&&1@csic8

CREATE OR REPLACE
FUNCTION cf_safeToNumber
(   str     VARCHAR2
)
RETURN NUMBER DETERMINISTIC
AS
    num NUMBER(37,10);
BEGIN
    num := CASE WHEN INSTR(str, 'E', 1, 1) > 0 THEN TO_NUMBER(str, '90.99999999999999999EEEE') ELSE TO_NUMBER(str, '99999999999999999999.99999999999999999999') END;
    RETURN num;
EXCEPTION
    WHEN OTHERS THEN
        RETURN NULL;
END;
/

CREATE OR REPLACE
FUNCTION cf_safeToBoolean
(   str     VARCHAR2
)
RETURN NUMBER DETERMINISTIC
AS
BEGIN
    RETURN
        CASE
            WHEN str = 'true'  THEN 1
            WHEN str = 'false' THEN 0
                           ELSE NULL
        END
    ;
END cf_safeToBoolean;
/

	
exit;
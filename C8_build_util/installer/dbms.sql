/*
alter table &&1..ATTRIBUTE_VARIANT_ELEMENT drop COLUMN VALUENUMBER; 
Alter table &&1..ATTRIBUTE_VARIANT_ELEMENT drop COLUMN VALUEBOOLEAN;

ALTER TABLE &&1..ATTRIBUTE_VARIANT_ELEMENT ADD (VALUENUMBER  NUMBER(37,10) AS (&&1..cf_safeToNumber(ValueString)));

ALTER TABLE &&1..ATTRIBUTE_VARIANT_ELEMENT ADD (VALUEBOOLEAN NUMBER(1,0) AS (&&1..cf_safeToBoolean(ValueString)));
*/


--update &&1..UM_IDENTITY set Password = 'kY7wnVwh56dSCQrZpoh0oHpAy18=' where Username='Administrator';





PROMPT running dbms_utility.compile_schema
exec dbms_utility.compile_schema('&&1');

--ALTER INDEX &&1..IX_AVE_BOOLEAN ENABLE;


PROMPT running gather_schema_stats
exec dbms_stats.gather_schema_stats('&&1');


PROMPT check invalid objects


select * from all_objects where OWNER='&&1' AND status <> 'VALID';


exit;
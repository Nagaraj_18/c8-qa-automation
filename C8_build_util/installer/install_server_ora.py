import os
import platform
import pywintypes
import shlex
import shutil
import subprocess
import sys
import time
import win32service
import win32serviceutil

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

deployDir = 'C:\Program Files\Centric Software\C8\Wildfly-20.0.0\standalone\deployments'
dirsToCreate = ('logs', 'D:\ORADATA', 'L:\ORALOG', 'T:\ORATEMP', 'F:\FILEVAULT')
httpServer = 'localhost'
httpPort = '8080'
logDir = "logs"
serviceName = 'WFAS20SVC'
user = 'CSIDBA'
password = 'CSIDBA'
adminPassword = 'centric8'
timeStamp = time.strftime('%y%m%d_%H%M%S')


def runCommand(cmd, communicate=False):
    r = 0
    if (communicate):
        #Doesn't really work with install4j apps
        p = subprocess.Popen(shlex.split(cmd), 0, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p.communicate()
        r = p.returncode
    else:
        try:
            cmdOutput = subprocess.check_output(cmd)
        except subprocess.CalledProcessError as e:
            sys.stderr.write('"%s" failed with return code %d\n' % (cmd, e.returncode))
            r = e.returncode
    return(r)

def waitForStatus(serviceName, desiredStatus, timeout=30):
    waitTime = 0.0
    while True:
        serviceStatus = win32serviceutil.QueryServiceStatus(serviceName, None)
    if serviceStatus[1] == desiredStatus:
        return(0)
    time.sleep(0.5)
    waitTime += 0.5
    if waitTime >= timeout:
        return(1)

print('\nStarting server installation at %s' % time.strftime('%c', time.localtime()))

if len(sys.argv) > 1:
    release = sys.argv[1]
else:
    with open('media/latest_release') as f:
        release = f.read().rstrip()
print('\nUsing release %s' % release)
if not os.path.exists('media/c8server_%s.exe' % release):
    print('\nUnable to find server installer c8server_%s.exe' % release)
    exit(1)
if not os.path.exists('media/c8apps_%s.exe' % release):
    print('\nUnable to find application installer c8apps_%s.exe' % release)
    exit(1)

#retval = runCommand('osql -U %s -P %s -Q quit' % (user, password))
#if retval != 0:
#    sys.stderr.write('Unable to log in to MS SQL Server\n')
#    exit(retval)

for p in dirsToCreate:
    if not os.path.exists(p): os.mkdir(p)

print('\nInstalling Server')
os.chdir('media')
print('\n... run server installer')
dirpath = os.getcwd()
print("current directory is : " + dirpath)
retval = runCommand('c8server_%s -q -console -overwrite -Dinstall4j.keepLog=true -Dinstall4j.logToStderr=true -varfile ../c8server_ora.varfile' % release, True)
if retval != 0:
    sys.stderr.write("Server installation failed with error code %d\n" % retval)
    exit(retval)
os.chdir('..')

while True:
    if os.path.exists(deployDir + '\csi-requesthandler.war.deployed'):
        print("\nDeployment successful")
        break
    elif os.path.exists(deployDir + '\csi-requesthandler.war.failed'):
        print("\nDeployment failed")
    exit(1)
    time.sleep(0.5)

print('\nServer installation complete at %s' % time.strftime('%c', time.localtime()))

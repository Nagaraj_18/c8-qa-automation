--select * from all_objects where object_type = 'SEQUENCE' AND owner = 'CSIDBA';
SET SERVEROUTPUT ON;



DECLARE 
    v_value             NUMBER(37) := 0;
    v_realvalue         NUMBER(37); 
    v_tempvalue         NUMBER(37);   
    v_SQL               VARCHAR2(500);
    v_seq               VARCHAR2(35);
    v_tbl               VARCHAR2(35);
    v_tb2               VARCHAR2(35);
    v_col               VARCHAR2(35);
    v_newAdd            NUMBER(1) := 0;
    
BEGIN
    DBMS_OUTPUT.PUT_LINE('UPDATING SEQUENCE -- BEGIN');

    FOR cur_rec IN (SELECT object_name FROM all_objects where object_type = 'SEQUENCE' AND owner = USER and object_name NOT LIKE '%SEQ_DEPENDENCY_LEVELS%') LOOP
        --get current val
        --
        v_SQL := 'SELECT ' || cur_rec.object_name|| '.nextval FROM DUAL ' ;
        EXECUTE IMMEDIATE   v_SQL INTO v_value;
        -- DBMS_OUTPUT.PUT_LINE(v_SQL);
        DBMS_OUTPUT.PUT_LINE(cur_rec.object_name || ' SEQUENCE ' || v_value);
         
        CASE cur_rec.object_name 
                
             
            
                
            WHEN 'SEQ_AC_PATTERN_ID' THEN  -- 1
                v_tbl := 'AC_PATTERN';
                v_col := 'AC_PATTERN_ID';
                
            WHEN 'SEQ_ALL_URL_ID' THEN    --  2
                v_tbl := 'ALL_URL';
                v_col := 'URL_ID';
                
            WHEN 'SEQ_ATTRIBUTE_ID' THEN    -- 3
                v_tbl := 'ATTRIBUTE_NAME';
                v_col := 'ATTRIBUTE_ID';
                
            WHEN 'SEQ_ATTRIBUTE_VARIANT_ID' THEN  -- 4 
                v_tbl := 'ATTRIBUTE2';
                v_col := 'ATTRIBUTE_VARIANT_ID';
                
            WHEN 'SEQ_AVE_ID' THEN                        -- 5
                v_tbl := 'ATTRIBUTE_VARIANT_ELEMENT';
                v_col := 'ATTRIBUTE_VARIANT_ELEMENT_ID';
                
            /*
			WHEN 'SEQ_DEPENDENCY_LEVELS' THEN   -- 6
                v_tbl := 'Dependency_Levels';
                v_col := 'SEQ';
            */    
            WHEN 'SEQ_EXTRACT_ATTRIBUTE_ID' THEN   -- 7
                v_tbl := 'EXTRACT_ATTRIBUTE';
                v_col := 'EXTRACT_ATTRIBUTE_ID';

            WHEN 'SEQ_EXTRACT_STORE_ID' THEN   -- 8
                v_tbl := 'EXTRACT_STORE';
                v_col := 'STORE_ID';

            WHEN 'SEQ_ROLLUP_EXPRESSION' THEN   -- 9
                v_tbl := 'ROLLUP_EXPRESSION';
                v_col := 'ROLLUP_EXPRESSION_ID';

            WHEN 'SEQ_UT_TX_ID' THEN   -- 10
                v_tbl := 'UT_Transaction';
                v_col := 'TX_Begin_ID';

            WHEN 'SEQ_ROLLUP_QUEUE' THEN   -- 11
                v_tbl := 'ROLLUP_QUEUE';
                v_col := 'ROLLUP_QUEUE_ID';
				
            ELSE 
                v_newAdd := 1;
            
        END CASE;
        
        IF (v_newAdd = 0) THEN
        
            v_SQL := 'SELECT MAX('||v_col || ') FROM ' || v_tbl;
            EXECUTE IMMEDIATE   v_SQL INTO v_realvalue;
            
            IF (v_value < v_realvalue) THEN
                FOR i IN 1..(v_realvalue - v_value + 5000) LOOP
                    v_SQL := 'SELECT '|| cur_rec.object_name ||'.nextval FROM DUAL';
                    EXECUTE IMMEDIATE   v_SQL INTO v_tempvalue;
                END LOOP;
            END IF;
        
        ELSE
            DBMS_OUTPUT.PUT_LINE('========================================================================');
            DBMS_OUTPUT.PUT_LINE('========================================================================');
            DBMS_OUTPUT.PUT_LINE('WARMING: NEW SEQUENCE '|| cur_rec.object_name  || ' Added, catch this exception');
            DBMS_OUTPUT.PUT_LINE('=========================================================================');
            DBMS_OUTPUT.PUT_LINE('========================================================================');
            v_newAdd := 0 ;
            
        END IF;
        
        
            
    END LOOP;  
   
END;
/
COMMIT;

EXIT;
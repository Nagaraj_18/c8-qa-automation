import argparse
import os
from multiprocessing import Process

def jmeterCall (serverName,userName,password, javaPath):
	os.system('%s -cp pi-tools.jar com.centricsoftware.pi.tools.jmeter.Generator %s %s %s ViewRequests.xml OUTPUT_PROJECT_NAME.jmx RESULTS.csv' % (javaPath, serverName, userName, password))
	print("Jmeter Call ran")
def neoloadCall (serverName, userName):
	os.system('c:\\PROGRA~1\\"NeoLoad 5.0"\\bin\\NeoLoadCmd -project "C:\\NeoLoad\\Prototypes\\Prototypes.nlp" -launch "NavigateOnly" -noGUI  -NTS "http://neoload:8080" -NTSLogin "admin:2oRsKlNRrbY2LHHH/Qrj9g==" -leaseLicense "MCwCFFZePcKMAbSXTVibAwISYPYta+hCAhQWuyMpF9yS71BNS2b44J2df6j/ug==:50:1" -report "C:\\NeoLoad\\Perf_57_2\\results\\report.xml"')
	print("Neoload Call ran")
if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument( dest="serverName",	type=str, help="Enter the Server name")
	parser.add_argument( dest="userName", 	type=str, help="Enter the Admin Username")
	parser.add_argument( dest="password",	type=str, help="Enter the Admin Password")

	args = parser.parse_args()

	server = args.serverName
	username = args.userName
	password = args.password
	#javaPath = "C:\Program Files\\Centric Software\\C8\\jdk-11.0.7+10\\bin\\java.exe"
	javaPath = "C:\Program Files\\Centric Software\\C8\\jdk-11.0.11+9\\bin\\java.exe"
	os.system('%s -cp pi-tools.jar com.centricsoftware.pi.tools.http.CentricRequest %s %s %s Module=NodeProcessor Operation=Execute Report=false ReturnNodes=false OmitResults=false UpdatedNodes:File="SystemConfiguration.operation.AutoUpdate.xml"' % (javaPath, server, username, password))
	print("Config file ran")
	os.system('%s -cp pi-tools.jar com.centricsoftware.pi.tools.http.CentricRequest %s %s %s Module=NodeProcessor Operation=Execute Report=false ReturnNodes=false OmitResults=false UpdatedNodes:File="SystemConfiguration2.operation.xml"' % (javaPath, server, username, password))
	print("config2 ran")
	p1 = Process(target=jmeterCall,args=(server, username, password, javaPath))
	p2 = Process(target=neoloadCall,args=(server, username))
	#starts process
	p1.start()
	p2.start()
	#waits for proccess to finish
	p1.join()
	p2.join()

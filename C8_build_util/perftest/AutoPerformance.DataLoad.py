import argparse
import os
from multiprocessing import Process
import time

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument( dest="serverName",	type=str, help="Enter the Server name")
	parser.add_argument( dest="c8_userName", 	type=str, help="Enter the Admin Username")
	parser.add_argument( dest="c8_password",	type=str, help="Enter the Admin Password")
	parser.add_argument( dest="system_username",	type=str, help="Enter the RDP Username")
	parser.add_argument( dest="system_password",	type=str, help="Enter the RDP Password")
	parser.add_argument( dest="db_username",	type=str, help="Enter the DB SA User")
	parser.add_argument( dest="db_password",	type=str, help="Enter the DB SA Password")

	args = parser.parse_args()

	server = args.serverName
	username = args.c8_userName
	password = args.c8_password
	rdpUser = args.system_username
	rdpPass = args.system_password
	dbUser = args.db_username
	dbPass = args.db_password
	#javaPath= "C:\\PROGRA~1\\CENTRI~1\\C8\\jdk1.8.0_191\\bin\\java.exe"
	#javaPath= "C:\\PROGRA~1\\CENTRI~1\\C8\\jdk-11.0.7+10\\bin\\java.exe"
	javaPath= "C:\\PROGRA~1\\CENTRI~1\\C8\\jdk-11.0.11+9\\bin\\java.exe"
	neoloadCmdPath = "c:\\PROGRA~1\\\"NeoLoad 5.0\"\\bin\\NeoLoadCmd"
	sqlcmdPath = "C:\\PROGRA~1\\MICROS~1\\CLIENT~1\\ODBC\\130\\Tools\\Binn\SQLCMD.EXE"
	# Use Neoload to Generate Users os.system()
	#os.system('%s -project "%s\\perftest\\UserLoad\\UserLoad.nlp" -launch "CreateUser" -noGUI  -NTS "http://neoload:8080" -NTSLogin "admin:2oRsKlNRrbY2LHHH/Qrj9g==" -leaseLicense "MCwCFFZePcKMAbSXTVibAwISYPYta+hCAhQWuyMpF9yS71BNS2b44J2df6j/ug==:100:7"' % (os.environ['WORKSPACE'], neoloadCmdPath))

	#Call to start library populatin and updateconfig
	print("Starting Library Population")
	os.system('%s -cp pi-tools.jar com.centricsoftware.pi.tools.http.CentricRequest %s:8080 %s %s Module=NodeProcessor Operation=Execute Report=false ReturnNodes=false OmitResults=false UpdatedNodes:File="SystemConfiguration.operation.AutoUpdate.xml"' % (javaPath, server, username, password))
	print("Library Population Complete")

	#Call to start dataload
	print("Starting Data Population")
	os.system('%s -cp pi-tools.jar com.centricsoftware.pi.tools.http.CentricRequest %s:8080 %s %s Module=NodeProcessor Operation=Execute Report=false ReturnNodes=false OmitResults=false UpdatedNodes:File="SystemConfiguration2.operation.xml"' % (javaPath, server, username, password))
	print("Data Population complete")

	#Stop wildfly service
	#print("Stopping wildfly service")
	#os.system('psservice \\\\%s -u %s -p %s stop WFAS13SVC' % (server, rdpUser, rdpPass))
	#os.system('sc stop WFAS13SVC')
	#time.sleep(30)
	#Take MSSQL backup.
	#print("Backing up database")

	#os.remove("T:\\AutoDataBackUp\\PrototypesDataGen.bak")
	#os.system('%s -S %s -U %s -P %s -d master -Q "BACKUP DATABASE c8 TO DISK=\'T:\\AutoDataBackUp\\PrototypesDataGen.bak\'" ' % (sqlcmdPath, server, dbUser, dbPass))

	#Start Wildfly
	#print("Starting wildfly service")
	#os.system('psservice \\\\%s -u %s -p %s start WFAS13SVC' % (server, rdpUser, rdpPass))
	#os.system('sc start WFAS13SVC')
	print("Autoperformance.dataload done")

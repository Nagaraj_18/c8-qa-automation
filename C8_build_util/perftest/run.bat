rem @echo off

echo Starting run.bat (Data Generator)

cd %SYSTEMROOT%/../C8_build_util/perftest

cd perftest
if exist logs\ (
  del /q /f logs\*
) else (
  mkdir logs
)
powershell -File setplan.ps1
python AutoPerformance.dataload.py localhost Administrator centric8 Administrator csidemo csidba csidba
echo Test complete

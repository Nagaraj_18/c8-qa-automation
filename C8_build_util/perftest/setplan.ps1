$confFile = 'SystemConfiguration2.operation.xml'
$label = '#DEBUG_PLAN#'
echo Data plan: $label
$replace =  $env:WORKSPACE + '\perftest\DataGenerator\Debug.plan.xml'
$contents = Get-Content -Path $confFile -ErrorAction stop
if ($contents -match $label) {
  $contents.replace($label, $replace) | Set-Content $confFile -ErrorAction stop
}
